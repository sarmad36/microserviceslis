import { Component, ViewChild, TemplateRef } from '@angular/core';
declare var $ :any;
import { RouteConfigLoadEnd } from "@angular/router";
import { RouteConfigLoadStart } from "@angular/router";
import { Event as RouterEvent } from "@angular/router";
import { Router } from "@angular/router";
import { UserIdleService } from 'angular-user-idle';
import { AuthService } from '../services/auth/auth.service';
import { AuthGuard } from '../services/auth/auth.guard';
import { DataService } from "../services/data/data.service";
// import { Idle, DEFAULT_INTERRUPTSOURCES } from '@ng-idle/core';
// import { MatDialog, MatDialogRef } from '@angular/material';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'container';
  // title = 'Panda';
idleState = 'Not started.';
timedOut = false;
callofFunc = 0;
public logedInUserRoles :any = {};
// @ViewChild('timeoutdialog') timeoutDialog: TemplateRef<any>;
// private timeoutDialogRef: MatDialogRef<TemplateRef<any>>;
  // public isShowingRouteLoadIndicator: boolean;

	// I initialize the app view component.
	constructor(
    private authService: AuthService,
    private authGaurd : AuthGuard,
    private sharedData: DataService,
    router: Router, private userIdle: UserIdleService) {



	}

callOnce(call){
  let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
  var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
  var originalText = bts.toString(CryptoJS.enc.Utf8);
  this.logedInUserRoles = JSON.parse(originalText)
  console.log("this.logedInUserRoles",this.logedInUserRoles);

  if (call == 1) {
    console.log("innnnnnn");
    setTimeout(()=>{
      // this.sharedData.checkUSerActivity('null');
    },5000);



  }
  else{
    // this.sharedData.checkUSerActivity('null');
  }
}
  ngOnInit() {
    console.log("Page is refreshed");

    // this.sharedData.checkUSerActivity('null');
    // $('.modal').modal('hide');
    // $('body').removeClass('')
     this.bindJQueryFuncs();
     //Start watching for user inactivity.
    this.userIdle.startWatching();

    // Start watching when user idle is starting.
    this.userIdle.onTimerStart().subscribe(count => {
      // this.userIdle.startWatching();
      // localStorage.removeItem("activity");
      // this.sharedData.checkUSerActivity('null');
      this.callOnce(count);
      console.log("count",count)
    });

    // Start watch when time is up.
    this.userIdle.onTimeout().subscribe(() =>{
       console.log('Time is up!')
         this.sharedData.checkActivity.subscribe(atcivity => {
           console.log("atcivity from service app component---",atcivity);
             if (atcivity == 'null') {
               this.logout()
             }


         })


     });
  }
  // timeoutFunction(){
  //   this.sharedData.checkActivity.subscribe(atcivity => {
  //     console.log("atcivity from service app component---",atcivity);
  //
  //      if (atcivity == 'null') {
  //        this.logout()
  //      }
  //
  //
  //   })
  // }
  ngAfterViewInit() {
    // console.log("Page loaded completely");
    //
    // this.sharedData.checkUSerActivity('null');
    }

  bindJQueryFuncs() {
    // Scroll to top button appear
    $(document).on('scroll', function() {
      var scrollDistance = $(this).scrollTop();
      if (scrollDistance > 100) {
        $('.scroll-to-top').fadeIn();
      } else {
        $('.scroll-to-top').fadeOut();
      }
    });

    // Smooth scrolling using jQuery easing
    $(document).on('click', 'a.scroll-to-top', function(e) {
      var $anchor = $(this);
      $('html, body').stop().animate({
        scrollTop: ($($anchor.attr('href')).offset().top)
      }, 1000, 'easeInOutExpo');
      e.preventDefault();
    });
  }
  gotoTop() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }

  stop() {
    console.log("stop timmer");
    // this.sharedData.checkUSerActivity('null');

    this.userIdle.stopTimer();
  }

  stopWatching() {
    this.userIdle.stopWatching();
  }

  startWatching() {
    // console.log('------In start watching');

    this.userIdle.startWatching();
  }

  restart() {
    this.userIdle.resetTimer();

  }
  logout(){
    this.authService.logout("null");
  }

}
