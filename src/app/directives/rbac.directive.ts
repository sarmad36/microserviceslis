import { Directive, ElementRef, HostListener, Input, OnInit, Output, NgModule } from '@angular/core';
import * as CryptoJS from 'crypto-js';

@Directive({
  selector: '[appRbac]'
})
export class RbacDirective implements OnInit {

  @Input() featureCode: string;
  @Output() visibilityAllow: Boolean;
  public logedInUserRoles :any = {};


  constructor(private el: ElementRef) {
   }

  ngOnInit() {
    if (localStorage.getItem('userRolesInfo') != null) {
      let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
      var originalText = bts.toString(CryptoJS.enc.Utf8);
      this.logedInUserRoles = JSON.parse(originalText)
    }
    else{
      this.logedInUserRoles = null
    }

    // console.log("ON LOAD  featureCode *******************",this.featureCode);
    // console.log("this.logedInUserRoles['allowedRoles'].indexOf(this.featureCode) *******************",this.logedInUserRoles['allowedRoles'].indexOf(this.featureCode));
    if (this.logedInUserRoles != null) {
      if(this.logedInUserRoles['allowedRoles'].indexOf(this.featureCode) == -1){
        // this.visibilityAllow = true;
        // this.el.nativeElement.style.display = 'none';


        this.el.nativeElement.remove();

      }
    }



  }

}

// @NgModule({
//   declarations: [ RbacDirective ],
//   exports: [ RbacDirective ]
// })
//
// export class RbacDirectiveModule {}
