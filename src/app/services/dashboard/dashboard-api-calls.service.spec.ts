import { TestBed } from '@angular/core/testing';

import { DashboardApiCallsService } from './dashboard-api-calls.service';

describe('DashboardApiCallsService', () => {
  let service: DashboardApiCallsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DashboardApiCallsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
