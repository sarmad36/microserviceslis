import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import { of } from 'rxjs';
import { BehaviorSubject, Observable, throwError, Subject } from 'rxjs';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
// import { catchError, retry, finalize, tap, map, takeUntil, delay } from 'rxjs/operators';
import { catchError, debounceTime, distinctUntilChanged, switchMap, tap, delay, map } from "rxjs/operators";
import { environment } from '../../../environments/environment';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class DashboardApiCallsService {

  public searchClientName = {
    header:{
      uuid               :"",
      partnerCode        :"",
      userCode           :"",
      referenceNumber    :"",
      systemCode         :"",
      moduleCode         :"CLIM",
      functionalityCode  :"",
      systemHostAddress  :"",
      remoteUserAddress  :"",
      dateTime           :""
    },
    data:{
      sortColumn  :"name",
      sortingOrder:"asc",
      searchString:""
    }
  }
  public logedInUserRoles :any = {};
  public corsHeaders: any = new HttpHeaders({
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Authorization': ""
  });

  constructor(
    private http: HttpClient,
    private sanitizer       : DomSanitizer
  ) {
    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
    var originalText = bts.toString(CryptoJS.enc.Utf8);
    // console.log("originalText",originalText);
    this.logedInUserRoles = JSON.parse(originalText)
    // console.log('this.logedInUserRoles',this.logedInUserRoles);
    // console.log('this.logedInUserRoles',this.logedInUserRoles);

    // this.corsHeaders['Authorization'] = 'Bearer '+this.logedInUserRoles.token;
  }

  totalUsers(url: any,saveData: any, logedInUserRoles: any){
    // console.log('logedInUserRoles.token',logedInUserRoles.token);

    var headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization':'Bearer '+logedInUserRoles.token,
    });


    return this.http.put(url,saveData,{headers})
  }
  totalClients(url: any,saveData: any, logedInUserRoles: any){
    // console.log('logedInUserRoles.token',logedInUserRoles.token);
    var headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization':'Bearer '+logedInUserRoles.token,
    });


    return this.http.put(url,saveData,{headers})
  }

  todayCasesLoadFirstGraph(url, saveData, logedInUserRoles: any){
    var headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization':'Bearer '+logedInUserRoles.token,
    });
    return this.http.post<any>(url, saveData,{headers})
    .pipe(
      catchError(this.errorHandler)
    )
  }
  todayCasesLoadBetweenDates(url, saveData, logedInUserRoles: any){
    var headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization':'Bearer '+logedInUserRoles.token,
    });
    return this.http.post<any>(url, saveData,{headers})
    .pipe(
      catchError(this.errorHandler)
    )
  }

  todayCasesLoadSecondGraph(url, saveData, logedInUserRoles: any){
    var headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization':'Bearer '+logedInUserRoles.token,
    });
    return this.http.post<any>(url, saveData,{headers})
    .pipe(
      catchError(this.errorHandler)
    )
  }
  todayCasesLoadBetweenDatesSecondGraph(url, saveData, logedInUserRoles: any){
    var headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization':'Bearer '+logedInUserRoles.token,
    });
    return this.http.post<any>(url, saveData,{headers})
    .pipe(
      catchError(this.errorHandler)
    )
  }

  CountCasesThirdGraph(url, saveData, logedInUserRoles: any){
    var headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization':'Bearer '+logedInUserRoles.token,
    });
    return this.http.post<any>(url, saveData,{headers})
    .pipe(
      catchError(this.errorHandler)
    )
  }
  CountCasesBetweenDatesThirdGraph(url, saveData, logedInUserRoles: any){
    var headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization':'Bearer '+logedInUserRoles.token,
    });
    return this.http.post<any>(url, saveData,{headers})
    .pipe(
      catchError(this.errorHandler)
    )
  }
  getClientByName(url, saveData, logedInUserRoles: any){
    var headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization':'Bearer '+logedInUserRoles.token,
    });
    return this.http.post(url,saveData,{headers})
    .pipe()
    .toPromise()
    .then( resp => {
      return resp;
    })
    .catch(error => {
      catchError(this.errorHandler)
    });
  }

  updatePassword(url: any,saveData: any, logedInUserRoles: any){
    var headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization':'Bearer '+logedInUserRoles.token,
    });
    return this.http.put(url,saveData,{headers})
  }
  getAgreement(url: any,saveData: any, logedInUserRoles: any){
    var headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization':'Bearer '+logedInUserRoles.token,
    });
    return this.http.put(url,saveData,{headers})
  }

  acceptAgreement(url: any,saveData: any, logedInUserRoles: any){
    var headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization':'Bearer '+logedInUserRoles.token,
    });
    return this.http.put(url,saveData,{headers})
  }

  errorHandler(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
