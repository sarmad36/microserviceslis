import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './includes/dashboard/dashboard.component';
import { LoginComponent } from './includes/login/login.component';
import { LoginLayoutComponent } from './includes/layouts/login-layout.component';
import { HomeLayoutComponent } from './includes/layouts/home-layout.component';

// import { BreadcrumbComponent } from './includes/breadcrumb/breadcrumb.component';
import { AuthGuard } from '../services/auth/auth.guard';
import { ChangePasswordComponent } from './includes/change-password/change-password.component';
import { NglisClumClientSharedModule} from '../../projects/nglis_clum_client/src/app/app.module';
import { NglisPntmClientSharedModule} from '../../projects/nglis_pntm_client/src/app/app.module';
import { NglisSpcmClientSharedModule } from '../../projects/nglis_spcm_client/src/app/app.module';
import { NglisMrmmClientSharedModule } from '../../projects/nglis_mrmm_client/src/app/app.module';
import { NglisTwwmClientSharedModule } from '../../projects/nglis_twmm_client/src/app/app.module';
import { AppComponent } from './app.component';
import { AppGuard } from "../services/gaurd/app.guard";
import { ResetPasswordComponent } from './includes/reset-password/reset-password.component';
import { ForgotPasswordComponent } from './includes/forgot-password/forgot-password.component';
import { PageRestrictedComponent } from './includes/page-restricted/page-restricted.component'
import { DownloadComponent } from './includes/download/download.component'


const routes: Routes = [

  {
    path: '',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: DashboardComponent,

      },
      // {
      //   path      : 'download/:id',
      //   component : DownloadComponent,
      //
      // },
      {
        path      : 'change-password',
        component : ChangePasswordComponent,

      },
      // {
      //   path      : 'change-password',
      //   component : ChangePasswordComponent,
      //
      // },

      {
        path: 'add-user',

        loadChildren: () => import('../../projects/nglis_clum_client/src/app/app.module').then(m => m.NglisClumClientSharedModule),

        data: { app: 'nglis_clum_client' }

      },
      {
        path: 'all-users',
        loadChildren: () => import('../../projects/nglis_clum_client/src/app/app.module').then(m => m.NglisClumClientSharedModule),
        data: { app: 'nglis_clum_client' }
      },
      {
        path: 'edit-user/:id',
        loadChildren: () => import('../../projects/nglis_clum_client/src/app/app.module').then(m => m.NglisClumClientSharedModule),
        data: { app: 'nglis_clum_client' }
      },
      {
        path      : 'add-client',
        loadChildren: () => import('../../projects/nglis_clum_client/src/app/app.module').then(m => m.NglisClumClientSharedModule),
        data: { app: 'nglis_clum_client' }
      },
      {
        path      : 'all-clients',
        loadChildren: () => import('../../projects/nglis_clum_client/src/app/app.module').then(m => m.NglisClumClientSharedModule),
        data: { app: 'nglis_clum_client' }
      },
      {
        path      : 'edit-client/:id',
        loadChildren: () => import('../../projects/nglis_clum_client/src/app/app.module').then(m => m.NglisClumClientSharedModule),
        data: { app: 'nglis_clum_client' }
      },
      {
        path      : 'add-patient',
        loadChildren: () => import('../../projects/nglis_pntm_client/src/app/app.module').then(m => m.NglisPntmClientSharedModule),
        data: { app: 'nglis_pntm_client' }
      },
      {
        path      : 'all-patients',
        loadChildren: () => import('../../projects/nglis_pntm_client/src/app/app.module').then(m => m.NglisPntmClientSharedModule),
        data: { app: 'nglis_pntm_client' }
      },

      {
        path      : 'edit-patient/:id/:from',
        loadChildren: () => import('../../projects/nglis_pntm_client/src/app/app.module').then(m => m.NglisPntmClientSharedModule),
        data: { app: 'nglis_pntm_client' }
      },
      {
        path      : 'add-case',
        loadChildren: () => import('../../projects/nglis_spcm_client/src/app/app.module').then(m => m.NglisSpcmClientSharedModule),
        data: { app: 'nglis_spcm_client' }
      },
      {
        path      : 'edit-case/:id/:with',
        loadChildren: () => import('../../projects/nglis_spcm_client/src/app/app.module').then(m => m.NglisSpcmClientSharedModule),
        data: { app: 'nglis_spcm_client' }
      },
      {
        path      : 'create-intake',
        loadChildren: () => import('../../projects/nglis_spcm_client/src/app/app.module').then(m => m.NglisSpcmClientSharedModule),
        data: { app: 'nglis_spcm_client' }
      },
      {
        path      : 'edit-intake/:id',
        loadChildren: () => import('../../projects/nglis_spcm_client/src/app/app.module').then(m => m.NglisSpcmClientSharedModule),
        data: { app: 'nglis_spcm_client' }
      },
      {
        path      : 'manage-cases',
        loadChildren: () => import('../../projects/nglis_spcm_client/src/app/app.module').then(m => m.NglisSpcmClientSharedModule),
        data: { app: 'nglis_spcm_client' }
      },
      {
        path      : 'manage-reports',
        loadChildren: () => import('../../projects/nglis_mrmm_client/src/app/app.module').then(m => m.NglisMrmmClientSharedModule),
        data: { app: 'nglis_mrmm_client'}
      },
      {
        path      : 'add-batch/:with',
        // canActivate: [AppGuard],
        loadChildren: () => import('../../projects/nglis_mrmm_client/src/app/app.module').then(m => m.NglisMrmmClientSharedModule),

        data: { app: 'nglis_twmm_client'}
      },
      {
        path      : 'manage-batches',
        // canActivate: [AppGuard],
        loadChildren: () => import('../../projects/nglis_mrmm_client/src/app/app.module').then(m => m.NglisMrmmClientSharedModule),
        data: { app: 'nglis_twmm_client'}
      },
      {
        path      : 'edit-batch/:id',
        loadChildren: () => import('../../projects/nglis_mrmm_client/src/app/app.module').then(m => m.NglisMrmmClientSharedModule),
        data: { app: 'nglis_twmm_client'}
      },
      {
        path      : 'worksheet/:id',
        loadChildren: () => import('../../projects/nglis_mrmm_client/src/app/app.module').then(m => m.NglisMrmmClientSharedModule),
        canActivate: [AppGuard],
        data: { app: 'nglis_twmm_client'}
      }

    ]
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login',
    }
  },
  {
    path: 'reset-password',
    component: ResetPasswordComponent,
    data: {
      title: 'Reset',
    }
  },
  {
    path: 'forgot-password/:id',
    component: ForgotPasswordComponent,
    data: {
      title: 'Forgot',
    }
  },
  {
    path: 'page-restrict',
    component: PageRestrictedComponent,
    data: {
      title: 'PageNotFound',
    }
  },
  {
    path      : 'download/:id/:clientname',
    component : DownloadComponent,
    data: {
      title: 'download',
    }
  },
  { path: '**', redirectTo: '' },

// {
//     path       : '',
//     redirectTo : '/dashboard',
//     pathMatch  : 'full'
//   }
];

@NgModule({
  imports: [RouterModule.forRoot(routes),
    NglisClumClientSharedModule.forRoot(),
    NglisPntmClientSharedModule.forRoot(),
    NglisSpcmClientSharedModule.forRoot(),
    NglisMrmmClientSharedModule.forRoot(),
    NglisTwwmClientSharedModule.forRoot()
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
