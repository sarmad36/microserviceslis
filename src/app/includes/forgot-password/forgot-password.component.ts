import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';

declare var $ :any;
import Swal from 'sweetalert2';
import { NotifierService } from 'angular-notifier';
import * as CryptoJS from 'crypto-js';
import { GlobalySharedModule } from '../../sharedmodules/globaly-shared/globaly-shared.module';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  submitted              = false;
  confirmNewPassword     = null;
  public passwordForm      : FormGroup;
  public updatePasswordData: any ={
    header               : {
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"",
      functionalityCode:"",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data                 : {
      userCode:"1136",
      otp:"",
      newPassword:""
    }
  }
  selectedUserCode;
  otp;
  ressetSuccess = false;
  passwordError = false;


  constructor(
    private formBuilder  : FormBuilder,
    private http         : HttpClient,
    private route        : ActivatedRoute,
    private router       : Router ,
    private ngxLoader    : NgxUiLoaderService,
    private  notifier    : NotifierService,
    private encryptDecrypt  : GlobalySharedModule
  ) {
    this.notifier        = notifier;

  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      console.log("params",params);
      if (typeof params['id'] == "undefined") {
        this.router.navigate(['login']);

      }
      else{
        // var base64Key = 'em5MMEZlOEt6Y05mRHc2Zg==';
        // var key = CryptoJS.enc.Base64.parse(base64Key);
        // var decryptedText = CryptoJS.AES.decrypt(params['id'], key, {
        //   mode: CryptoJS.mode.ECB,
        //   padding: CryptoJS.pad.Pkcs7
        // });
        // decryptedText.toString(CryptoJS.enc.Utf8);
        // this.selectedUserCode = decryptedText;
        this.selectedUserCode = params['id'];
      }
      // console.log("this.pageType",this.pageType);

    })
    this.passwordForm  = this.formBuilder.group({
      otp        : ['', [Validators.required]],
      NewPassword            : ['', [Validators.required, Validators.maxLength(50), Validators.minLength(8)]],
      confirmPassword        : ['', [Validators.required, Validators.maxLength(50), Validators.minLength(8)]]

     }, {validator: this.checkPasswords });
  }

  get f() { return this.passwordForm.controls; }

  onSubmit(){
    // // stop here if form is invalid
    this.submitted = true;
    this.ngxLoader.start();
    if (this.passwordForm.invalid) {
      // alert('form invalid');
      this.ngxLoader.stop();
      return;
    }

    var url                = environment.API_USER_ENDPOINT + 'changepassowrdbyotp';
    var requestData         = {...this.updatePasswordData};
    requestData.data.otp         = this.otp;
    console.log("selectedUserCode",this.selectedUserCode);
   
    var key = CryptoJS.enc.Base64.parse("em5MMEZlOEt6Y05mRHc2Zg==");

    var encryptedData = CryptoJS.AES.encrypt(this.selectedUserCode, key, {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7
    }).toString();
    var jsonEncypted = CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(encryptedData));
    
    requestData.data.userCode    = jsonEncypted;
    // requestData.data.userCode    = this.encryptDecrypt.encryptString(this.selectedUserCode);

    this.http.put(url,requestData).subscribe(otpResp=>{
      console.log('otpResp',otpResp);
      this.ngxLoader.stop();

      if (otpResp['result'].codeType == "S") {
        // this.router.navigate(['forgot-password']);
          this.notifier.notify("success",otpResp['result'].description);
          this.ressetSuccess = true;
          // setTimeout(function(){
          //   this.router.navigate(['login']);
          // },3000);
      }
      else{
        this.notifier.notify("error","Please try again with correct credentials");
      }
    },error=>{
      this.notifier.notify("error","Please try again");
    })
    // requestData.data.newPassword =
    // return;



  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    let pass = group.get('NewPassword').value;
    let confirmPass = group.get('confirmPassword').value;

    return pass === confirmPass ? null : { notSame: true }
  }

  passwordRegex(e){
    var regexp = new RegExp(/^(?=.*[A-Z])(?=.*[!@#$&*_])(?=.*[0-9])(?=.*[a-z]).{8,100}$/);
    var reg = regexp.test(e.target.value);
    console.log('reg',reg);

    if (reg == true) {
      this.passwordError = false;
    }
    else{
      this.passwordError = true;
    }
  }

}
