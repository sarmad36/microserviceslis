import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth/auth.service';
// import { AuthGuard } from '../../../services/auth/auth.guard';
declare var $ :any;
import { NgxUiLoaderService } from 'ngx-ui-loader';
import * as CryptoJS from 'crypto-js';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, ActivatedRoute  } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  // private formSubmitAttempt: boolean;
  submitted = false;
  loginFailed = false;
  returnUrl: string;
  downloadString: string;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private ngxLoader    : NgxUiLoaderService,
    private router: Router,
    private route: ActivatedRoute
    // private authGaurd : AuthGuard
  ) {
    // console.log("this.authService.isLoggedIn",this.authService.isLoggedIn);
  }

  ngOnInit(): void {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.downloadString = this.route.snapshot.queryParams['string'] || '/';
    // console.log("this.returnUrl",this.returnUrl);

    this.authService.isLoggedIn.subscribe(resp=>{
      // console.log("resp",resp);
      if (resp == true) {

        this.router.navigate(['']);
        return;

      }


    })
    this.ngxLoader.stop();
    $('.modal-backdrop').css('display','none');
    this.form = this.fb.group({
      userName: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  get f() { return this.form.controls; }
  onSubmit() {
    this.ngxLoader.start();
    this.submitted = true;
    if (this.form.valid) {

      this.authService.login(this.form.value,this.returnUrl, this.downloadString);


    }
    else{
      this.ngxLoader.stop();
      // this.submitted = false;
      return;
    }
    // this.formSubmitAttempt = true;
  }

}
