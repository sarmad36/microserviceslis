import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Router, NavigationEnd } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
// import { AppSettings } from '../../services/_services/app.setting';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
// import { HttpRequestsService } from '../../services/_services/http-requests/http-requests.service';
import { DataTableDirective } from 'angular-datatables';
import { NgxUiLoaderService } from 'ngx-ui-loader';
declare var $ :any;
import Swal from 'sweetalert2';
import { NotifierService } from 'angular-notifier';
// import { PartnerApiEndpointsService } from '../../services/partner/partner-api-endpoints.service';
// import { DashboardApiCallsService } from '../../services/dashboard/dashboard-api-calls.service';
import { filter } from 'rxjs/operators';
import { DataService } from "../../../services/data/data.service";
import * as CryptoJS from 'crypto-js';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  public getAgreement   =
  {
    header              : {
      uuid              : "",
      partnerCode       : "",
      userCode          : "",
      referenceNumber   : "",
      systemCode        : "",
      moduleCode        : "",
      functionalityCode : "",
      systemHostAddress : "",
      remoteUserAddress : "",
      dateTime          : ""
    },
    data                :{
      partnerCode       : 1
    }
  }
  agreementText         ;
  termsConditions       = false;
  swalStyle              : any;
  previousUrl: string = null;
  currentUrl: string = null;

  addUserAction = false;
  // addUserExternalAction = false;
  manageUserAction = false;
  addClientAction = false;
  manageClientAction = false;
  addPatientAction = false;
  managePatientAction = false;
  createDetailCaseAction = false;
  manageCaseAction = false;
  manageIntakeAction = false;
  createBatchAction = false;
  manageBatchAction = false;
  manageReportAction = false;
  viewClientAction = false;
  manageUserExternalAction = false;
  viewExternalUserAction = false;
  manageAndViewAction = false;

  // allowedActions = ['addUser','manageUser'];


  constructor(
    private formBuilder  : FormBuilder,
    private http         : HttpClient,
    private router       : Router ,
    private route       : ActivatedRoute ,
    // private httpRequest  : HttpRequestsService,
    private ngxLoader    : NgxUiLoaderService,
    // private partnerService  : PartnerApiEndpointsService,
    private  notifier    : NotifierService,
    // private dashboardService : DashboardApiCallsService,
    private sharedData: DataService
  ) {
    this.swalStyle       =  Swal.mixin({
      customClass : {
        confirmButton : 'btn btn-success my-swal-success',
        cancelButton  : 'btn btn-danger'
      },
      buttonsStyling  : true
    })
    this.notifier        = notifier;
   }
  public logedInUserRoles :any = {};
  ngOnInit(): void {
    // this.route.params.subscribe(params => {
    //   console.log('params',params);
    // });
    // this.router.events.pipe(filter(event => event instanceof NavigationEnd))
    // .subscribe((event: any) => {
    //
    //   this.previousUrl = this.currentUrl;
    //   this.currentUrl = event.url;
    //
    // });




    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
var originalText = bts.toString(CryptoJS.enc.Utf8);
this.logedInUserRoles = JSON.parse(originalText)
    console.log("ROles -------------" , this.logedInUserRoles );

    this.RolesBaseAuth();



    this.bindJQueryFuncs();
  }

  showAgreement(){
    // let url  = environment.API_PARTNER_ENDPOINT + 'agreement?partnerCode=1';
    this.ngxLoader.start();
    let url  = environment.API_PARTNER_ENDPOINT + 'agreement';
    this.getAgreement.data.partnerCode = this.logedInUserRoles.partnerCode;
    this.http.put(url,this.getAgreement,this.logedInUserRoles).subscribe(resp => {
      console.log("resp",resp);
      this.ngxLoader.stop();
      if (resp['result'].codeType =="E" || resp['result'].data == null) {
        // alert("Agreement already accepted");
        this.router.navigate(['dashboard']);
        this.notifier.notify( "info", "Agreement Already Accepted");
      }
      else{
        this.agreementText = resp['data'].text;
        this.ngxLoader.stop();
        $('#wizardsModal').modal('show');
      }

      // this.router.navigate(['/editlisting', JSON.stringify(resp)]);
    })
  }
  acceptAgreement(){
    this.ngxLoader.start();
    if (this.termsConditions) {

      // let url  = environment.API_PARTNER_ENDPOINT + 'acceptagreement?partnerCode=1';
      let url  = environment.API_PARTNER_ENDPOINT + 'acceptagreement';
      this.http.put(url, this.getAgreement,this.logedInUserRoles).subscribe(resp => {
        console.log("resp",resp);
        this.ngxLoader.stop();
        if (resp['result'].codeType == 'S') {
          this.swalStyle.fire({
            icon: 'success',
            type: 'sucess',
            title: 'Thankyou for accepting terms',
            showConfirmButton: true,
            timer: 3000
          })
        }
        else{
          this.ngxLoader.stop();
          this.swalStyle.fire({
            icon: 'error',
            type: 'error',
            title: 'There was an error while accepting terms',
            showConfirmButton: true,
            timer: 3000
          })

        }
        // this.router.navigate(['/editlisting', JSON.stringify(resp)]);
      })
      this.ngxLoader.stop();
      $('#wizardsModal').modal('hide');
    }
    else{
      alert("Please Accept The Terms and COnditions");
    }

  }

  bindJQueryFuncs() {
    $("#sidebarToggle, #sidebarToggleTop").on('click', function(e) {
      // console.log("here99999999999");
      $("body").toggleClass("sidebar-toggled");
      $(".sidebar").toggleClass("toggled");
      if ($(".sidebar").hasClass("toggled")) {
        $('.sidebar .collapse').collapse('hide');
      };
    });

    // Close any open menu accordions when window is resized below 768px
    $(window).resize(function() {
      if ($(window).width() < 768) {
        $('.sidebar .collapse').collapse('hide');
      };
    });

    // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
    $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function(e) {
      if ($(window).width() > 768) {
        var e0           = e.originalEvent,
        delta            = e0.wheelDelta || -e0.detail;
        this.scrollTop   += (delta < 0 ? 1 : -1) * 30;
        e.preventDefault();
      }
    });
  }

  RolesBaseAuth(){
    if(this.logedInUserRoles['allowedRoles'].indexOf("USRA-AUI") != -1){
      this.addUserAction = true;
    }
    // if(this.logedInUserRoles['allowedRoles'].indexOf("USRA-AUE") != -1){
    //   this.addUserExternalAction = true;
    // }
    if(this.logedInUserRoles['allowedRoles'].indexOf("USRA-MUI") != -1){
      this.manageUserAction = true;
    }
    if(this.logedInUserRoles['allowedRoles'].indexOf("USRA-MUE") != -1){
      this.manageUserExternalAction = true;
    }
    if(this.logedInUserRoles['allowedRoles'].indexOf("CLTA-AC") != -1){
      this.addClientAction = true;
    }
    if(this.logedInUserRoles['allowedRoles'].indexOf("CLTA-MC") != -1){
      this.manageClientAction = true;
    }
    if(this.logedInUserRoles['allowedRoles'].indexOf("PNTA-AP") != -1){
      this.addPatientAction = true;
    }
    if(this.logedInUserRoles['allowedRoles'].indexOf("PNTA-MP") != -1){
      this.managePatientAction = true;
    }
    if(this.logedInUserRoles['allowedRoles'].indexOf("SPCA-CD") != -1){
      this.createDetailCaseAction = true;
    }
    if(this.logedInUserRoles['allowedRoles'].indexOf("SPCA-MC") != -1){
      this.manageCaseAction = true;
    }
    if(this.logedInUserRoles['allowedRoles'].indexOf("SPCA-MI") != -1){
      this.manageIntakeAction = true;
    }
    if(this.logedInUserRoles['allowedRoles'].indexOf("TWMA-CNB") != -1){
      this.createBatchAction = true;
    }
    if(this.logedInUserRoles['allowedRoles'].indexOf("TWMA-MBC") != -1){
      this.manageBatchAction = true;
    }
    if(this.logedInUserRoles['allowedRoles'].indexOf("MRMA-MR") != -1){
      this.manageReportAction = true;
    }
    if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-VC") != -1) {
      this.viewClientAction = true;
    }
    if(this.logedInUserRoles['allowedRoles'].indexOf("USRA-VUE") != -1 && this.logedInUserRoles['userType'] == 2){
      this.viewExternalUserAction = true;

    }

    // if (this.manageClientAction == false && this.viewClientAction == false) {
    //   this.manageAndViewAction = false;
    // }
    // else if (this.manageClientAction == true || this.viewClientAction == true) {
    //   this.manageAndViewAction = true;
    // }

    console.log("this.manageUserAction",this.manageUserAction);
    console.log("this.manageUserExternalAction",this.manageUserExternalAction);
    // console.log("this.manageAndViewAction",this.manageAndViewAction);



  }
  gotoAddPatient(){
    // $('.nav-item .collapse-item').on('click', function(){
      $('.nav-item  .collapse-item').collapse('hide');
      for (let i = 0; i < $('.nav-item .collapse-item').length; i++) {
        // console.log("$('.nav-item  .collapse-item')[i].classList",$('.nav-item  .collapse-item')[i].classList);

        if (typeof $('.nav-item  .collapse-item')[i].classList != 'undefined') {

          $('.nav-item .collapse-item')[i].parentElement.parentElement.parentElement.children[1].classList.remove("show")
          // $('.nav-item  .collapse-item').collapse('hide');
        }
      }
    console.log('localStorage=',  localStorage.getItem("ptatientLoadedFromClient"));

    if (localStorage.getItem("ptatientLoadedFromClient") == "true") {
        localStorage.removeItem("ptatientLoadedFromClient")
        location.reload();
    }
    else{
      this.router.navigate(['/add-patient']);
    }


    // this.sharedData.currentClientId.subscribe(client => {
    //       // this.patientForm.reset();
    //       console.log("Side Bar ",client);
    //       // if (client != 'null') {
    //       //   location.reload();
    //       // }
    //       // else{
    //       //   this.router.navigate(['/add-patient']);
    //       // }
    //     })





  }
  removeStorage(){
    // console.log("removeStorage");

    localStorage.removeItem("ptatientLoadedFromClient");
    // this.sharedData.checkUSerActivity('null');
    $('.nav-item  .collapse-item').collapse('hide');
    for (let i = 0; i < $('.nav-item .collapse-item').length; i++) {
      // console.log("$('.nav-item  .collapse-item')[i].classList",$('.nav-item  .collapse-item')[i].classList);

      if (typeof $('.nav-item  .collapse-item')[i].classList != 'undefined') {

        $('.nav-item .collapse-item')[i].parentElement.parentElement.parentElement.children[1].classList.remove("show")
        // $('.nav-item  .collapse-item').collapse('hide');
      }
    }
  }

  // gotoPatient(){
  // localStorage.setItem("from","sidebar")
  // this.router.navigateByUrl('/all-patients')
  // }

}
