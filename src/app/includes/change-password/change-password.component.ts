import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';

declare var $ :any;
import Swal from 'sweetalert2';
import { NotifierService } from 'angular-notifier';
import { DashboardApiCallsService } from '../../services/dashboard/dashboard-api-calls.service';
import { GlobalySharedModule } from '../../sharedmodules/globaly-shared/globaly-shared.module';
import * as CryptoJS from 'crypto-js';


@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css'],
  host       : {
    class    :'mainComponentStyle'
  }
})
export class ChangePasswordComponent implements OnInit {
  submitted              = false;
  selctedUserID          = 1;
  isPasswordChange       = false;
  CNewPassword           = '';
  newPassword            = '';
  minLength              = false;
  maxLength              = false;
  passMisMatch           = false;
  passEmpty              = false
  CPassEmpty             = false;
  confirmNewPassword     ;
  notSame                = true;
  wrongCurrentPass       = false;

  public updatePasswordData: any ={
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode  : "USRA-CP",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 : {
      user: {
        userCode           : 1,
        password         : ''
      },
      currentPassword    : ""
    }
  }
  oldPassword ;
  public logedInUserRoles :any = {};
  swalStyle              : any;
  public passwordForm      : FormGroup;
  logedInUserPassword;
  logedInUserLoginInfo;
  logedInUserCode;
  passwordError = false;
  newPasswordUser;

  constructor(
    private formBuilder  : FormBuilder,
    private http         : HttpClient,
    private route        : ActivatedRoute,
    private router       : Router ,
    private ngxLoader    : NgxUiLoaderService,
    private  notifier    : NotifierService,
    private dashboardService : DashboardApiCallsService,
    private encryptDecrypt  : GlobalySharedModule
  ) {
    this.notifier        = notifier;
    this.swalStyle       =  Swal.mixin({
      customClass : {
        confirmButton : 'btn btn-success my-swal-success',
        cancelButton  : 'btn btn-danger'
      },
      buttonsStyling  : true
    })
   }

  ngOnInit(): void {
    // this.logedInUserRoles     = JSON.parse(localStorage.getItem('userRolesInfo'));
    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
    var originalText = bts.toString(CryptoJS.enc.Utf8);
    this.logedInUserRoles = JSON.parse(originalText)
    // this.logedInUserLoginInfo = JSON.parse(localStorage.getItem('userLoginInfo'))
    var codeData              = this.logedInUserRoles;
    // var codeData              = JSON.parse(localStorage.getItem('userRolesInfo'));
    // console.log("codeData",codeData);
    // console.log("logedInUserLoginInfo",this.logedInUserLoginInfo);

    this.logedInUserCode      = codeData.userCode;
    // this.logedInUserPassword  = this.logedInUserLoginInfo.password;
    // console.log("logedInUserLoginInfo",this.logedInUserLoginInfo);
    // this.updatePasswordData.data.currentPassword = this.logedInUserPassword;

    // this.notifier.notify( "error", "Password is updated successfully");
    this.passwordForm  = this.formBuilder.group({
      currentPassword        : ['', [Validators.required, Validators.maxLength(50), Validators.minLength(8)]],
      NewPassword            : ['', [Validators.required, Validators.maxLength(50), Validators.minLength(8)]],
      confirmPassword        : ['', [Validators.required, Validators.maxLength(50), Validators.minLength(8)]]

     }, {validator: this.checkPasswords });
  }

  get f() { return this.passwordForm.controls; }

  updatePassword(){
    this.wrongCurrentPass = false;
    // // stop here if form is invalid
    this.submitted = true;
    this.ngxLoader.start();
    if (this.passwordForm.invalid) {
      // alert('form invalid');
      this.ngxLoader.stop();
      return;
    }
    // console.log("logedInUserCode",this.logedInUserCode);

    this.updatePasswordData.data.user.userCode          = this.logedInUserCode;
    this.updatePasswordData.data.currentPassword        = this.encryptDecrypt.encryptString(this.oldPassword);
    this.updatePasswordData.data.user.password               = this.encryptDecrypt.encryptString(this.newPasswordUser);
    // console.log("updatePasswordData",this.updatePasswordData);
    // return;

    // this.updatePasswordData.data.password  = this.newPassword;
    // var url                = environment.API_USER_ENDPOINT + 'updatepassword';
    var url                = environment.API_USER_ENDPOINT + 'updatepassword';
    // return;
    this.dashboardService.updatePassword(url,this.updatePasswordData,this.logedInUserRoles).subscribe(resp => {
      console.log("resp",resp);
      this.submitted = false;
      this.ngxLoader.stop();
      if (resp['result'].codeType == "S") {
        console.log('Password Updated succesfully add response divs');
        this.notifier.notify( "sucess", "Password is updated successfully");
        this.wrongCurrentPass = false;

      }
      else if(resp['result'].codeType == "T" && resp['result'].description == "Provided Current Password of the user is wrong"){
        this.notifier.notify( "error", "Provided password is wrong");
        this.wrongCurrentPass = true;

      }
      else{
        this.notifier.notify( "error", "Error while updating password");
        this.wrongCurrentPass = false;
      }

      setTimeout(function(){
        this.wrongCurrentPass = false;
      }, 1000);

    })


  }


checkPasswords(group: FormGroup) { // here we have the 'passwords' group
  let pass = group.get('NewPassword').value;
  let confirmPass = group.get('confirmPassword').value;

  return pass === confirmPass ? null : { notSame: true }
}


  passwordCheckRealTime(){
    if (this.newPassword.length < 8) {
      this.minLength    = true;
    }
    else if (this.newPassword.length > 50) {
      this.maxLength    = true;
    }
    else{
      this.passMisMatch  = false;
      this.passEmpty     = false;
      this.CPassEmpty    = false;
      this.minLength     = false;
      this.maxLength     = false;
    }

    if (this.CNewPassword != '') {
      if (this.newPassword != this.CNewPassword) {
        this.passMisMatch  = true;
      }
      else{
        this.passMisMatch  = false;
        this.passEmpty     = false;
        this.CPassEmpty    = false;
        this.minLength     = false;
        this.maxLength     = false;
      }

    }


  }
  passwordRegex(e){
    var regexp = new RegExp(/^(?=.*[A-Z])(?=.*[!@#$&*_])(?=.*[0-9])(?=.*[a-z]).{8,100}$/);
    var reg = regexp.test(e.target.value);
    if (reg == true) {
      this.passwordError = false;
    }
    else{
      this.passwordError = true;
    }
  }

}
