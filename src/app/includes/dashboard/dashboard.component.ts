import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import Swal from 'sweetalert2'
declare var $ :any;
import { NotifierService } from 'angular-notifier';
// import { DashboardApiCallsService } from '../../services/dashboard/dashboard-api-calls.service';
import * as CanvasJS from '../../../assets/js/canvasjs.min.js';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';
import * as CryptoJS from 'crypto-js';
import { ChangeDetectorRef } from '@angular/core';

@Component({
  selector   : 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls  : ['./dashboard.component.css'],
  host       : {
    class    :'mainComponentStyle'
  }
})
export class DashboardComponent implements OnInit {

  totalUsers            ;
  totalClients          ;
  public requestData    =
  {
    header              : {
      uuid              : "",
      partnerCode       : "",
      userCode          : "",
      referenceNumber   : "",
      systemCode        : "",
      moduleCode        : "",
      functionalityCode: "USRA-CP",
      systemHostAddress : "",
      remoteUserAddress : "",
      dateTime          : ""
    },
    data                : {
      partnerCode         : 1,
      userType          : 1
    }
  };

  public clientRequestData    =
  {
    header              : {
      uuid              : "",
      partnerCode       : "",
      userCode          : "",
      referenceNumber   : "",
      systemCode        : "",
      moduleCode        : "CLIM",
      functionalityCode: "USRA-CP",
      systemHostAddress : "",
      remoteUserAddress : "",
      dateTime          : ""
    },
    data                : {}
  };
  // logedInUserRoles : any;
  firstGraphData        : any;
  footerDraftTotal      = 0;
  footerAccessTotal     = 0;
  footerPrintedTotal    = 0;
  footernotPrintedTotal = 0;
  firstSelected         ={startDate: moment(), endDate: moment()};
  secondSelected        ={startDate: moment(), endDate: moment()};
  manageCaseAction      =  false;
  swalStyle              : any;
  public logedInUserRoles :any = {};
  maxDate: moment.Moment = moment();

  firstGraphFrom = null;
  firstGraphTo   = null;
  secondGraphFrom = null;
  secondGraphTo   = null;
  hidefourthGraph  = false;
  hideThirdGraph  = false;
  hideSecondGraph  = false;
  hideFirstGraph  = false;
  totalForthGraphCases = null;

  constructor(
    // private formBuilder  : FormBuilder,
    private http         : HttpClient,
    private router       : Router ,
    // private httpRequest  : HttpRequestsService,
    // private userService  : UserApiEndpointsService,

    // private route           : ActivatedRoute,
    // private sanitizer       : DomSanitizer,
    private ngxLoader       : NgxUiLoaderService,
    private notifier        : NotifierService,
    // private dashboardService : DashboardApiCallsService,
    private datePipe: DatePipe,
    private ref: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    $('#wrapper').css('visibility','hidden');
    $('.navbar').css('visibility','hidden');
    $('#accordionSidebar').css('visibility','hidden');

    // var url = 'http://localhost:8080/encrypt';
    // this.http.get(url)
    // .toPromise()
    // .then( result => {
    //   console.log('result----',result);
    //   // let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
    //   var bts  = CryptoJS.AES.decrypt(result);
    //   var originalText = bts.toString(CryptoJS.enc.Utf8);
    //   console.log('originalText',originalText);
    //
    //
    // })

    console.log("localStorage.getItem('fromD')",localStorage.getItem('fromD'));
    //
    if (typeof localStorage.getItem('fromD') != 'undefined') {
      if (localStorage.getItem('fromD') != null) {
        this.router.navigate(['/download/',localStorage.getItem('fromD')]);
        return;

      }
    }
    // else{
    //
    // }


    // console.log("JSON.parse(localStorage.getItem('userRolesInfo'))",JSON.parse(localStorage.getItem('userRolesInfo')));

    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
    var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
    var originalText = bts.toString(CryptoJS.enc.Utf8);
    this.logedInUserRoles = JSON.parse(originalText)
    // console.log("logedInUserRoles",this.logedInUserRoles);
    if(this.logedInUserRoles['allowedRoles'].indexOf("USRA-CP") != -1 ){
      this.manageCaseAction = true;
      // this.rbac.checkROle('SPCA-UC');
    }

    // this.ngxLoader.start();
    // setTimeout (() => {
    // console.log("JSON.parse(localStorage.getItem('userCodeInfo'))",JSON.parse(localStorage.getItem('userCodeInfo')));
    // console.log("JSON.parse(localStorage.getItem('userLoginInfo'))",JSON.parse(localStorage.getItem('userLoginInfo')));


    ////////////////////////////////////////////////////////////
    ////////////USER CLIENT DASHBOARD START//////////////////////
    /////////////////////////////////////////////////////////
    this.requestData.header.userCode    = this.logedInUserRoles.userCode
    this.requestData.header.partnerCode = this.logedInUserRoles.partnerCode
    this.requestData.data.partnerCode = this.logedInUserRoles.partnerCode
    // var url                = environment.API_USER_ENDPOINT + 'totalusers';
    // this.dashboardService.totalUsers(url, this.requestData, this.logedInUserRoles).subscribe(resp => {
    //   console.log('total Users',resp)
    //   this.totalUsers = resp['data'];
    // },
    // error=>{
    //   this.ngxLoader.stop();
    //   this.notifier.notify( "error", "Error while loading Total Users Backend Connection")
    // })


    // var url                = environment.API_USER_ENDPOINT + 'totalusers?partnerCode=1';
    // var url                = environment.API_CLIENT_ENDPOINT + 'totalclients';
    // this.dashboardService.totalClients(url,this.clientRequestData, this.logedInUserRoles).subscribe(resp => {
    //   console.log('total Clients',resp)
    //   this.totalClients = resp['data'];
    // },
    // error=>{
    //   this.ngxLoader.stop();
    //   this.notifier.notify( "error", "Error while loading Total Clients Backend Connection")
    // })
    ////////////////////////////////////////////////////////////
    ////////////USER CLIENT DASHBOARD END//////////////////////
    /////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////
    ////////////SPECIMEN DASHBOARD START//////////////////////
    /////////////////////////////////////////////////////////
    var today = new Date();
    var ageDB = this.convertAge(today) ;

    console.log("ageDB",ageDB);
    var myDate = new Date();
    var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy');
    var currentTimeStamp = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');

    var todayCaseFirstReq={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "SPCA-MC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{searchDate:currentTimeStamp,toDate:currentTimeStamp,statuses:[1,3]}}
    var firstReqUrl      = environment.API_SPECIMEN_ENDPOINT + 'dashboardtoday';
    this.loadFirstGraph(todayCaseFirstReq, firstReqUrl);

    var todayCaseSecondReq={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "SPCA-MC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{searchDate:currentTimeStamp}}
    var secondReqUrl      = environment.API_SPECIMEN_ENDPOINT + 'casestoday';
    this.loadSecondGraph(todayCaseSecondReq,secondReqUrl);

    var todayCaseThirdReq={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "SPCA-MC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{}}
    var thirdReqUrl      = environment.API_SPECIMEN_ENDPOINT + 'casesforlastsevendays';
    this.loadThirdGraph(todayCaseThirdReq, thirdReqUrl);

    var todayCasefourthReq={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "SPCA-MC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{searchDate:"",toDate:"",statuses:[]}}
    var fourthReqUrl      = environment.API_SPECIMEN_ENDPOINT + 'piechartdata';
    this.loadfourthGraph(todayCasefourthReq, fourthReqUrl);


    ////////////////////////////////////////////////////////////
    ////////////SPECIMEN DASHBOARD END//////////////////////
    /////////////////////////////////////////////////////////

  }

  loadFirstGraph(todayCaseFirstReq, firstReqUrl){
    this.ngxLoader.start();
    this.footerDraftTotal       = 0;
    this.footerAccessTotal      = 0;
    this.footerPrintedTotal     = 0;
    this.footernotPrintedTotal  = 0;

    this.http.post(firstReqUrl,todayCaseFirstReq, this.logedInUserRoles).subscribe(firstGraph=>{
      console.log("firstGraph",firstGraph);
      if (firstGraph['data'] == null) {
        this.ngxLoader.stop();
        // this.notifier.notify("error",firstGraph['result']['description'])
        this.hideFirstGraph = true;
        return;
      }
      else{
        this.hideThirdGraph  = false;
        this.hideSecondGraph  = false;
        this.hideFirstGraph  = false;
        this.hidefourthGraph  = false;
        if (firstGraph['data'].length > 0) {
          const clientId = [];
          var tableData = [];
          const map = new Map();
          for (const item of firstGraph['data']) {
            if(!map.has(item.clientId)){
              map.set(item.clientId, true);    // set any value to Map
              clientId.push(item.clientId);
              tableData.push(item);
            }
          }

          var clURL           = environment.API_CLIENT_ENDPOINT + "clientidsnames"
          var clReqData       = {
            header:{
              uuid               :"",
              partnerCode        :this.logedInUserRoles.partnerCode,
              userCode           :this.logedInUserRoles.userCode,
              referenceNumber    :"",
              systemCode         :"",
              moduleCode         :"CLIM",
              functionalityCode: "CLTA-VC",
              systemHostAddress  :"",
              remoteUserAddress  :"",
              dateTime           :""
            },
            data:{
              clientIds:clientId
            }

          }

          this.http.post(clURL,clReqData, this.logedInUserRoles).subscribe(selectedClient => {
            $('#wrapper').css('visibility','visible');
            $('.navbar').css('visibility','visible');
            $('#accordionSidebar').css('visibility','visible');
            //console.log("selectedClient",selectedClient);
            for (let i = 0; i < firstGraph['data'].length; i++) {
              for (let j = 0; j < selectedClient['data']['length']; j++) {
                if (firstGraph['data'][i].clientId == selectedClient['data'][j].clientId) {
                  firstGraph['data'][i].clientName = selectedClient['data'][j].clientName;
                }
                else{
                  ////////////////// client not exist
                  if (typeof firstGraph['data'][i].clientName == 'undefined') {
                    firstGraph['data'][i].clientName = 'no name';
                  }

                }

              }

            }



            ///////////////// draw table
            // var tableData = [];
            var tableHold = {};
            this.drawTable(tableData,firstGraph).then(tableReturn=>{
              console.log("tableReturn",tableReturn);

              this.firstGraphData = tableReturn;
              this.ngxLoader.stop();
            })

          });
        }
        else{
          this.ngxLoader.stop();
          this.firstGraphData = []
          this.footerDraftTotal       = 0;
          this.footerAccessTotal      = 0;
          this.footerPrintedTotal     = 0;
          this.footernotPrintedTotal  = 0;
        }
      }


    },
    error=>{
      this.ngxLoader.stop();
      this.notifier.notify( "error", "Error while loading First Graph Backend Connection")
    })
  }

  loadSecondGraph(todayCaseFirstReq, firstReqUrl){
    this.ngxLoader.start();

    this.http.post(firstReqUrl,todayCaseFirstReq, this.logedInUserRoles).subscribe(secondGraph=>{
      console.log("secondGraph",secondGraph);
      if (secondGraph['data'] == null) {
        this.ngxLoader.stop();
        // this.notifier.notify("error",secondGraph['result']['description'])
        this.hideSecondGraph = true;
        return;
      }
      else{
        this.hideThirdGraph  = false;
        this.hideSecondGraph  = false;
        this.hideFirstGraph  = false;
        this.hidefourthGraph  = false;

        if (secondGraph['data'].length > 0) {
          const clientId = [];
          const map = new Map();
          for (const item of secondGraph['data']) {
            if(!map.has(item.clientId)){
              map.set(item.clientId, true);    // set any value to Map
              clientId.push(item.clientId);
            }
          }
          var clURL           = environment.API_CLIENT_ENDPOINT + "clientidsnames"
          var clReqData       = {
            header:{
              uuid               :"",
              partnerCode        :this.logedInUserRoles.partnerCode,
              userCode           :this.logedInUserRoles.userCode,
              referenceNumber    :"",
              systemCode         :"",
              moduleCode         :"CLIM",
              functionalityCode: "CLTA-VC",
              systemHostAddress  :"",
              remoteUserAddress  :"",
              dateTime           :""
            },
            data:{
              clientIds:clientId}

            }

            // var clReqData       = {clientIds:clientId}
            var dataPoints      = [];
            var dpHolder        = {};
            this.http.post(clURL,clReqData, this.logedInUserRoles).subscribe(selectedClient => {
              $('#wrapper').css('visibility','visible');
              $('.navbar').css('visibility','visible');
              $('#accordionSidebar').css('visibility','visible');
              //console.log("selectedClient",selectedClient);
              for (let i = 0; i < secondGraph['data'].length; i++) {
                for (let j = 0; j < selectedClient['data']['length']; j++) {
                  if (secondGraph['data'][i].clientId == selectedClient['data'][j].clientId) {
                    secondGraph['data'][i].clientName = selectedClient['data'][j].clientName;
                  }
                  else{
                    ////////////////// client not exist
                    // secondGraph['data'][i].clientName = 'no name'
                    if (typeof secondGraph['data'][i].clientName == 'undefined') {
                      secondGraph['data'][i].clientName = 'no name';
                    }

                  }

                }
                dpHolder['y']          = secondGraph['data'][i].totalCount;
                dpHolder['label']      = secondGraph['data'][i].clientName;
                dpHolder['legendText'] = secondGraph['data'][i].clientName;

                dataPoints.push(dpHolder);
                dpHolder = {};

              }
              // console.log("dpHolder",dpHolder);

              let chart = new CanvasJS.Chart("secondChartContainer", {
                animationEnabled: true,
                // exportEnabled: true,
                axisY:{
                  interlacedColor: "rgb(255,250,250)",
                  gridColor: "#FFBFD5"
                },

                dataPointWidth: 30,
                legend: {
                  cursor: "pointer",
                  // itemclick: this.toggleDataSeries,
                  horizontalAlign: "right", // left, center ,right
                  verticalAlign: "top",  // top, center, bottom
                },
                // title: {
                //   text: "Basic Column Chart in Angular"
                // },
                data: [{
                  color: "#3366cc",
                  type: "column",
                  showInLegend: true,
                  legendText: "Cases",
                  // indexLabel: "{label}, {y}",
                  indexLabel: "{y}",
                  indexLabelPlacement: "inside",
                  indexLabelOrientation: "horizontal",
                  indexLabelFontColor  : "#fff",
                  indexLabelFontStyle  : "bold",
                  // dataPoints: dataPoints
                  dataPoints: dataPoints
                }]
              });

              chart.render();
              this.ngxLoader.stop();




            });
          }
          else{
            dataPoints = [{'y':0,"label":'',"legendText":''}]
            let chart = new CanvasJS.Chart("secondChartContainer", {
              animationEnabled: true,
              // exportEnabled: true,
              axisY:{
                interlacedColor: "rgb(255,250,250)",
                gridColor: "#FFBFD5"
              },

              dataPointWidth: 30,
              legend: {
                cursor: "pointer",
                // itemclick: this.toggleDataSeries,
                horizontalAlign: "right", // left, center ,right
                verticalAlign: "top",  // top, center, bottom
              },
              // title: {
              //   text: "Basic Column Chart in Angular"
              // },
              data: [{
                color: "#3366cc",
                type: "column",
                showInLegend: true,
                legendText: "Cases",
                // indexLabel: "{label}, {y}",
                indexLabel: "{y}",
                indexLabelPlacement: "inside",
                indexLabelOrientation: "horizontal",
                indexLabelFontColor  : "#fff",
                indexLabelFontStyle  : "bold",
                // dataPoints: dataPoints
                dataPoints: dataPoints
              }]
            });

            chart.render();
            this.ngxLoader.stop();
          }
        }



      },
      error=>{
        this.ngxLoader.stop();
        this.notifier.notify( "error", "Error while loading First Graph Backend Connection")
      })
    }

    loadThirdGraph(todayCaseFirstReq, firstReqUrl){

      this.http.post(firstReqUrl,todayCaseFirstReq, this.logedInUserRoles).subscribe(thirdGraph=>{
        $('#wrapper').css('visibility','visible');
        $('.navbar').css('visibility','visible');
        $('#accordionSidebar').css('visibility','visible');
        console.log("thirdGraph",thirdGraph);
        if (thirdGraph['data'] == null) {
          this.ngxLoader.stop();
          // this.notifier.notify("error",thirdGraph['result']['description'])
          this.hideThirdGraph = true;
          return;
        }
        else{
          this.hideThirdGraph  = false;
          this.hideSecondGraph  = false;
          this.hideFirstGraph  = false;
          this.hidefourthGraph  = false;

          const clientId = [];
          const map = new Map();
          for (const item of thirdGraph['data']) {
            if(!map.has(item.clientId)){
              map.set(item.clientId, true);    // set any value to Map
              clientId.push(item.clientId);
            }
          }
          var clURL           = environment.API_CLIENT_ENDPOINT + "clientidsnames"
          var clReqData       = {
            header:{
              uuid               :"",
              partnerCode        :this.logedInUserRoles.partnerCode,
              userCode           :this.logedInUserRoles.userCode,
              referenceNumber    :"",
              systemCode         :"",
              moduleCode         :"CLIM",
              functionalityCode: "CLTA-VC",
              systemHostAddress  :"",
              remoteUserAddress  :"",
              dateTime           :""
            },
            data:{
              clientIds:clientId}

            }
            // var clReqData       = {clientIds:clientId}
            var dataPoints      = [];
            var dpHolder        = {};
            this.http.post(clURL,clReqData, this.logedInUserRoles).subscribe(selectedClient => {

              //console.log("selectedClient",selectedClient);
              for (let i = 0; i < thirdGraph['data'].length; i++) {
                // if (typeof selectedClient['data'] != 'undefined') {
                //   for (let j = 0; j < selectedClient['data']['length']; j++) {
                //     if (thirdGraph['data'][i].clientId == selectedClient['data'][j].clientId) {
                //       thirdGraph['data'][i].clientName = selectedClient['data'][j].clientName;
                //     }
                //     else{
                //       ////////////////// client not exist
                //       // thirdGraph['data'][i].clientName = 'no name'
                //       if (typeof thirdGraph['data'][i].clientName == 'undefined') {
                //         thirdGraph['data'][i].clientName = 'no name';
                //       }
                //
                //     }
                //
                //   }
                // }

                var today = new Date(thirdGraph['data'][i].date);
                var date = this.convertAge(today) ;

                dpHolder['y']          = thirdGraph['data'][i].totalCount;
                dpHolder['label']      = date;
                dpHolder['legendText'] = date;

                dataPoints.push(dpHolder);
                dpHolder = {};

              }
              // console.log("dataPoints",dataPoints);

              // console.log("thirdGraph['data']",thirdGraph['data']);
              var chart = new CanvasJS.Chart("thirdChartContainer", {
                animationEnabled: true,
                // theme: "light2",
                // title: {
                //   text: "Monthly Sales Data"
                // },
                // axisX: {
                //   valueFormatString: "MMM"
                // },
                axisY: {
                  interlacedColor: "rgb(255,250,250)",
                  gridColor: "#FFBFD5"
                  // prefix: "$",
                  // labelFormatter: this.addSymbols
                },
                dataPointWidth: 30,
                toolTip: {
                  shared: false,
                  content: "{label}, Cases: {y}"
                },
                legend: {
                  cursor: "pointer",
                  itemclick: this.toggleDataSeries,
                  horizontalAlign: "right", // left, center ,right
                  verticalAlign: "top",  // top, center, bottom
                },
                data: [
                  {
                    color: "#3366cc",
                    type: "column",
                    showInLegend: true,
                    legendText: "Cases",
                    // indexLabel: "{label}, {y}",
                    indexLabel: "{y}",
                    indexLabelPlacement: "inside",
                    indexLabelOrientation: "horizontal",
                    indexLabelFontColor  : "#fff",
                    indexLabelFontStyle  : "bold",
                    // xValueFormatString: "MMMM YYYY",
                    // yValueFormatString: "$#,##0",
                    dataPoints        : dataPoints
                    // dataPoints: [
                    //   { x: new Date(2016, 0), y: 20000 },
                    //   { x: new Date(2016, 1), y: 30000 },
                    //   { x: new Date(2016, 2), y: 25000 },
                    //   { x: new Date(2016, 3), y: 70000, indexLabel: "High Renewals" },
                    //   { x: new Date(2016, 4), y: 50000 },
                    //   { x: new Date(2016, 5), y: 35000 },
                    //   { x: new Date(2016, 6), y: 30000 },
                    //   { x: new Date(2016, 7), y: 43000 },
                    //   { x: new Date(2016, 8), y: 35000 },
                    //   { x: new Date(2016, 9), y:  30000},
                    //   { x: new Date(2016, 10), y: 40000 },
                    //   { x: new Date(2016, 11), y: 50000 }
                    // ]
                  },
                  {
                    type: "line",
                    name: "Average",
                    showInLegend: true,
                    legendText: "Average",
                    // yValueFormatString: "$#,##0",
                    dataPoints: dataPoints
                  },
                  // {
                  //   type: "area",
                  //   name: "Profit",
                  //   markerBorderColor: "white",
                  //   markerBorderThickness: 2,
                  //   showInLegend: true,
                  //   yValueFormatString: "$#,##0",
                  //   dataPoints: dataPoints
                  // }
                ]
              });
              chart.render();




            });
          }


        },
        error=>{
          this.ngxLoader.stop();
          this.notifier.notify( "error", "Error while loading First Graph Backend Connection")
        })
      }
      loadfourthGraph(todayCaseFirstReq, firstReqUrl){

        this.http.post(firstReqUrl,todayCaseFirstReq, this.logedInUserRoles).subscribe(fourthGraph=>{
          $('#wrapper').css('visibility','visible');
          $('.navbar').css('visibility','visible');
          $('#accordionSidebar').css('visibility','visible');
          console.log("fourthGraph",fourthGraph);
          if (fourthGraph['data'] == null) {
            this.ngxLoader.stop();
            // this.notifier.notify("error",fourthGraph['result']['description'])
            this.hidefourthGraph  = true;
            return;
          }
          else{
            this.hideThirdGraph  = false;
            this.hideSecondGraph  = false;
            this.hideFirstGraph  = false;
            this.hidefourthGraph  = false;

            // var clReqData       = {clientIds:clientId}
            var dataPoints      = [];
            var dpHolder        = {};
            this.totalForthGraphCases = fourthGraph['data']['totalCases'];
            for (let i = 0; i < fourthGraph['data']['casesCountStatusDtos'].length; i++) {

              // dpHolder['y']          = fourthGraph['data']['casesCountStatusDtos'][i].totalCount;
              // dpHolder['label']      = date;
              // dpHolder['legendText'] = date;
              if (fourthGraph['data']['casesCountStatusDtos'][i]['caseStatus'] == 1) {
                dpHolder['y']          = fourthGraph['data']['casesCountStatusDtos'][i]['caseCount'];
                dpHolder['name']       = "Draft";
                dpHolder['click']      = this.byPieTotal;
              }
              if (fourthGraph['data']['casesCountStatusDtos'][i]['caseStatus'] == 2) {
                dpHolder['y']          = fourthGraph['data']['casesCountStatusDtos'][i]['caseCount'];
                dpHolder['name']        = "Accessioned";
                dpHolder['click']      = this.byPieTotal;
              }
              if (fourthGraph['data']['casesCountStatusDtos'][i]['caseStatus'] == 3) {
                dpHolder['y']          = fourthGraph['data']['casesCountStatusDtos'][i]['caseCount'];
                dpHolder['name']        = "Triaged";
                dpHolder['exploded']        = "true";
                dpHolder['click']      = this.byPieTotal;
              }
              if (fourthGraph['data']['casesCountStatusDtos'][i]['caseStatus'] == 4) {
                dpHolder['y']          = fourthGraph['data']['casesCountStatusDtos'][i]['caseCount'];
                dpHolder['name']        = "In Progress";
                dpHolder['click']      = this.byPieTotal;

              }
              if (fourthGraph['data']['casesCountStatusDtos'][i]['caseStatus'] == 5) {
                dpHolder['y']          = fourthGraph['data']['casesCountStatusDtos'][i]['caseCount'];
                dpHolder['name']        = "Partial Under Reporting";
                dpHolder['click']      = this.byPieTotal;

              }
              if (fourthGraph['data']['casesCountStatusDtos'][i]['caseStatus'] == 6) {
                dpHolder['y']          = fourthGraph['data']['casesCountStatusDtos'][i]['caseCount'];
                dpHolder['name']        = "Under Reporting";
                dpHolder['click']      = this.byPieTotal;

              }
              if (fourthGraph['data']['casesCountStatusDtos'][i]['caseStatus'] == 7) {
                dpHolder['y']          = fourthGraph['data']['casesCountStatusDtos'][i]['caseCount'];
                dpHolder['name']        = "Partial Signed Off";
                dpHolder['click']      = this.byPieTotal;

              }
              if (fourthGraph['data']['casesCountStatusDtos'][i]['caseStatus'] == 8) {
                dpHolder['y']          = fourthGraph['data']['casesCountStatusDtos'][i]['caseCount'];
                dpHolder['name']        = "Signed Off";
                dpHolder['click']      = this.byPieTotal;

              }
              if (fourthGraph['data']['casesCountStatusDtos'][i]['caseStatus'] == 9) {
                dpHolder['y']          = fourthGraph['data']['casesCountStatusDtos'][i]['caseCount'];
                dpHolder['name']        = "Partial Delivered";
                dpHolder['click']      = this.byPieTotal;

              }
              if (fourthGraph['data']['casesCountStatusDtos'][i]['caseStatus'] == 10) {
                dpHolder['y']          = fourthGraph['data']['casesCountStatusDtos'][i]['caseCount'];
                dpHolder['name']        = "Delivered";
                dpHolder['click']      = this.byPieTotal;

              }
              if (fourthGraph['data']['casesCountStatusDtos'][i]['caseStatus'] == 11) {
                dpHolder['y']          = fourthGraph['data']['casesCountStatusDtos'][i]['caseCount'];
                dpHolder['name']        = "Cancelled";
                dpHolder['click']      = this.byPieTotal;

              }
              if (fourthGraph['data']['casesCountStatusDtos'][i]['caseStatus'] == 12) {
                dpHolder['y']          = fourthGraph['data']['casesCountStatusDtos'][i]['caseCount'];
                dpHolder['name']        = "Not Delivered";
                dpHolder['click']      = this.byPieTotal;

              }



              dataPoints.push(dpHolder);
              dpHolder = {};

            }
            console.log("dataPoints",dataPoints);


            // console.log("thirdGraph['data']",thirdGraph['data']);
            var chart = new CanvasJS.Chart("fourthChartContainer", {
              exportEnabled: false,
              animationEnabled: true,
              theme: "light3",
              // title:{
              // 	text: "Total no of cases : "
              // },
              legend:{
                cursor: "pointer",
                horizontalAlign: "left", // "center" , "right"
                verticalAlign: "center",  // "top" , "bottom"
                itemclick: this.explodePie
              },
              data: [{
                type: "pie",
                indexLabelFontSize: 10,
                showInLegend: true,
                toolTipContent: "{name}: <strong>{y}</strong>",
                indexLabel: "{y}",
                dataPoints: dataPoints
                // dataPoints: [
                // 	{ y: 26, name: "Accessioned", exploded: true },
                // 	{ y: 20, name: "Triaged" },
                // 	{ y: 5, name: "In progress" },
                // 	{ y: 3, name: "Under Reporting" },
                // 	{ y: 7, name: "Delivered" },
                // 	{ y: 17, name: "Not Delivered" },
                // 	{ y: 22, name: "Cancelled"}
                // ]
              }]
            });
            chart.render();

          }


        },
        error=>{
          this.ngxLoader.stop();
          this.notifier.notify( "error", "Error while loading First Graph Backend Connection")
        })
      }

      explodePie (e) {
        if(typeof (e.dataSeries.dataPoints[e.dataPointIndex].exploded) === "undefined" || !e.dataSeries.dataPoints[e.dataPointIndex].exploded) {
          e.dataSeries.dataPoints[e.dataPointIndex].exploded = true;
        } else {
          e.dataSeries.dataPoints[e.dataPointIndex].exploded = false;
        }
        e.chart.render();

      }
      addSymbols(e) {
        var suffixes = ["", "K", "M", "B"];
        var order = Math.max(Math.floor(Math.log(e.value) / Math.log(1000)), 0);

        if(order > suffixes.length - 1)
        order = suffixes.length - 1;

        var suffix = suffixes[order];
        return CanvasJS.formatNumber(e.value / Math.pow(1000, order)) + suffix;
      }

      toggleDataSeries(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
          e.dataSeries.visible = false;
        } else {
          e.dataSeries.visible = true;
        }
        e.chart.render();
      }

      convertAge(today){
        var monthDB ;
        var dayDB ;
        if ((today.getMonth()+1) < 10) {
          monthDB = "0"+(today.getMonth()+1);
        }
        else{
          monthDB = (today.getMonth()+1);
        }
        if (today.getDate() < 10) {
          dayDB = "0"+today.getDate();
        }
        else{
          dayDB = today.getDate();
        }
        return (today.getFullYear()  + "-" + monthDB + "-" + dayDB)
      }

      firstGraphBetween(e){
        console.log("----e",e);
        if (e.startDate == null || e.endDate == null) {
          return;
        }



        var date = new Date(e.startDate);
        var end  = new Date(e.endDate);
        // var startDate = this.convertAge(date);
        // var endDate   = this.convertAge(end);
        var startDate = this.datePipe.transform(date, 'MM-dd-yyyy HH:mm:ss');
        var endDate   = this.datePipe.transform(end, 'MM-dd-yyyy HH:mm:ss');
        // startDate = startDate +' 00:00:00';
        // endDate = endDate +' 00:00:00';
        this.firstGraphFrom = startDate;
        this.firstGraphTo   = endDate;
        var todayCaseFirstReq={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "SPCA-MC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{searchDate:startDate,toDate:endDate,statuses:[1,3]}}

        var firstReqUrl      = environment.API_SPECIMEN_ENDPOINT + 'dashboardtoday';
        this.loadFirstGraph(todayCaseFirstReq, firstReqUrl);
        //
        // // this.firstSelected.startDate

      }

      secondGraphBetween(e){
        console.log("----SEcond",e);
        if (e.startDate == null || e.endDate == null) {
          return;
        }

        var date = new Date(e.startDate);
        var end  = new Date(e.endDate);
        // var startDate = this.convertAge(date);
        // var endDate   = this.convertAge(end);
        var startDate = this.datePipe.transform(date, 'MM-dd-yyyy HH:mm:ss');
        var endDate   = this.datePipe.transform(end, 'MM-dd-yyyy HH:mm:ss');
        // startDate = startDate +' 00:00:00';
        // endDate = endDate +' 00:00:00';
        this.secondGraphFrom = startDate;
        this.secondGraphTo   = endDate;
        console.log('this.secondGraphFrom',this.secondGraphFrom);
        console.log('this.secondGraphFrom',this.secondGraphFrom);

        var todayCaseSecondReq={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "SPCA-MC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{searchDate:startDate,toDate:endDate}}
        var secondReqUrl      = environment.API_SPECIMEN_ENDPOINT + 'countcasesbetweendates';
        this.loadSecondGraph(todayCaseSecondReq,secondReqUrl);
        //
        // // this.firstSelected.startDate

      }

      byName(clientId){
        // var data = {searchBy:'clientId',id:clientId}
        // this.router.navigateByUrl('/manage-cases', { state: data });
      }
      caseDraft(clientId, clientName){
        var myDate = new Date();
        var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy');
        var data = {searchBy:'Draft',id:clientId, clientName: clientName, creationDate:currentDate}
        this.router.navigateByUrl('/manage-cases', { state: data });
      }
      caseAccessioned(clientId, clientName){
        var myDate = new Date();
        var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy');
        var data = {searchBy:'Triaged',id:clientId, clientName: clientName, creationDate:currentDate}
        this.router.navigateByUrl('/manage-cases', { state: data });
      }
      byPrinted(clientId, clientName){
        var myDate = new Date();
        var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy');
        var data = {searchBy:'printed',id:clientId, clientName: null, creationDate:currentDate}
        this.router.navigateByUrl('/manage-cases', { state: data });
      }
      byNotPrinted(clientId, clientName){
        var myDate = new Date();
        var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy');
        var data = {searchBy:'notPrinted',id:clientId, clientName: null, creationDate:currentDate}
        console.log('data',data);

        this.router.navigateByUrl('/manage-cases', { state: data });
      }
      byTotalDraft(clientId){
        var myDate = new Date();
        var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy');
        var data = {searchBy:'Draft', id:null, clientName:null, creationDate:currentDate}
        this.router.navigateByUrl('/manage-cases', { state: data });
      }
      byTotalAccessioned(clientId){
        var myDate = new Date();
        var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy');
        var data = {searchBy:'Accessioned', id:null, clientName:null, creationDate:currentDate}
        this.router.navigateByUrl('/manage-cases', { state: data });
      }
      byTotalPrinted(clientId){
        var myDate = new Date();
        var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy');
        var data = {searchBy:'printed',id:null, clientName: null, creationDate:currentDate}
        this.router.navigateByUrl('/manage-cases', { state: data });
      }
      byTotalNotPrinted(clientId){
        var myDate = new Date();
        var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy');
        var data = {searchBy:'notPrinted',id:null, clientName: null, creationDate:currentDate}
        this.router.navigateByUrl('/manage-cases', { state: data });
      }
        byPieTotal = (e: any) => {
          // console.log("e['dataPoint']['name']",e);
        // return;
        if(this.logedInUserRoles['allowedRoles'].indexOf("SPCA-MC") !== -1 ){
          var data = {searchBy:e['dataPoint']['name'], id:null, clientName:null}
          this.router.navigateByUrl('/manage-cases', { state: data });
       }
       else{
         console.log("notallowed");

       }

      }

      drawTable(tableData,firstGraph){
        console.log("tableData",tableData);
        console.log("firstGraph",firstGraph);

        return new Promise((resolve, reject) => {
          var temp = [];
          var completeData = {};
          this.footernotPrintedTotal = 0;
          this.footerPrintedTotal = 0;
          for (let i = 0; i < tableData.length; i++) {
            var pendingCount = 0;
            var printedCount = 0;
            var accessCount = 0;
            var draftCount = 0;

            for (let j = 0; j < firstGraph['data'].length; j++) {
              if (tableData[i]['clientId'] == firstGraph['data'][j]['clientId']) {
                tableData[i]['clientId'] = firstGraph['data'][j]['clientId'];
                tableData[i]['clientName'] = firstGraph['data'][j]['clientName'];
                switch (firstGraph['data'][j]['status']) {
                  case 1: /////////////printed labels
                  draftCount = draftCount + firstGraph['data'][j]['totalCases'];
                  this.footerDraftTotal = this.footerDraftTotal + draftCount;
                  printedCount = firstGraph['data'][i]['isLabelPrintedCount'];
                  pendingCount = firstGraph['data'][i]['pending'];
                  // if (firstGraph['data'][i]['isLabelPrinted'] == 0) {
                  //   pendingCount = pendingCount + 1;
                  //   // pendingCount = pendingCount + firstGraph['data'][j]['isLabelPrintedCount'];
                  //
                  // }
                  // else if (firstGraph['data'][i]['isLabelPrinted'] == 1) {
                  //   printedCount = printedCount + firstGraph['data'][j]['isLabelPrintedCount'];
                  // }
                  break;

                  case 3: /////////////not printed labels
                  accessCount = accessCount + firstGraph['data'][j]['totalCases'];
                  this.footerAccessTotal = this.footerAccessTotal + accessCount;
                  printedCount = firstGraph['data'][i]['isLabelPrintedCount'];
                  pendingCount = firstGraph['data'][i]['pending'];
                  // if (firstGraph['data'][i]['isLabelPrinted'] == 0) {
                  //   pendingCount = pendingCount + 1;
                  //   // pendingCount = pendingCount + firstGraph['data'][j]['isLabelPrintedCount'];
                  //
                  // }
                  // else if (firstGraph['data'][i]['isLabelPrinted'] == 1) {
                  //   printedCount = printedCount + firstGraph['data'][j]['isLabelPrintedCount'];
                  //   // tableData[i]['pendingCount'] = printedCount;
                  //   // this.footerPrintedTotal = this.footerPrintedTotal + printedCount;
                  // }
                  break;

                  default:
                  break;
                }




                // if (tableData[i]['isLabelPrinted'] == 0) {
                //   tableData[i]['pendingCount'] = 1;
                //   tableData[i]['printedCount'] = 0;
                //   this.footernotPrintedTotal++;
                // }
                // else if (tableData[i]['isLabelPrinted'] == 1) {
                //   tableData[i]['printedCount'] = tableData[i]['isLabelPrintedCount'];
                //   tableData[i]['pendingCount'] = 0;
                //   this.footerPrintedTotal = this.footerPrintedTotal + tableData[i]['isLabelPrintedCount'];
                // }
                //
                // if (tableData[i]['status'] == 1) {
                //   tableData[i]['draftCount'] = tableData[i]['totalCases'];
                //   tableData[i]['accessionedCount'] = 0;
                //   this.footerDraftTotal = this.footerDraftTotal + tableData[i]['totalCases'];
                // }
                // else if (tableData[i]['status'] == 3) {
                //   tableData[i]['accessionedCount'] = tableData[i]['totalCases'];
                //   tableData[i]['draftCount'] = 0;
                //   this.footerAccessTotal = this.footerAccessTotal + tableData[i]['totalCases'];
                // }

              }
            }
            if (printedCount == 0) {
              if (accessCount > 0) {
                tableData[i]['pendingCount']       = accessCount;
              }
              else{
                tableData[i]['pendingCount']       = pendingCount;

              }
            }
            else{
              tableData[i]['pendingCount']       = pendingCount;
            }
            tableData[i]['accessionedCount']   = accessCount;
            tableData[i]['draftCount']         = draftCount;
            tableData[i]['printedCount']       = printedCount;
            // tableData[i]['pendingCount']       = pendingCount;
            this.footernotPrintedTotal = this.footernotPrintedTotal + pendingCount;
            this.footerPrintedTotal    = this.footerPrintedTotal + printedCount;

          }

          // console.log('pendingCount',pendingCount);
          // console.log('printedCount',printedCount);
          // console.log('this.footernotPrintedTotal',this.footernotPrintedTotal);
          // console.log('this.footerPrintedTotal',this.footerPrintedTotal);
          //
          // console.log("tableData",tableData);



          resolve(tableData);
        })
      }

    }
