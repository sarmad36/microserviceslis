import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageRestrictedComponent } from './page-restricted.component';

describe('PageRestrictedComponent', () => {
  let component: PageRestrictedComponent;
  let fixture: ComponentFixture<PageRestrictedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageRestrictedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageRestrictedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
