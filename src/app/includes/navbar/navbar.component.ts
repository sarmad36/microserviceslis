import { Component, OnInit } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../../../services/auth/auth.service';
import { AuthGuard } from '../../../services/auth/auth.guard';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { RouteConfigLoadEnd } from "@angular/router";
import { RouteConfigLoadStart } from "@angular/router";
import { Event as RouterEvent } from "@angular/router";
import * as CryptoJS from 'crypto-js';

declare var $ :any;
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  public logedInUserRoles :any = {};
  orgName;
  userName = '';
  fName = '';
  lName = '';

  constructor(
    private authService: AuthService,
    private authGaurd : AuthGuard,
    private router: Router
  ) {
    this.authService.isLoggedIn.subscribe(resp=>{
      // console.log("resp",resp);
      if (resp == true) {

        // console.log("----------------------");

      }
      else{
        this.router.navigate(['/login']);
      }

    })



  }

  ngOnInit(): void {
  // this.logedInUserRoles     = JSON.parse(localStorage.getItem('userRolesInfo'))
  let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
  var originalText = bts.toString(CryptoJS.enc.Utf8);
  this.logedInUserRoles = JSON.parse(originalText)
  // var name = JSON.parse(localStorage.getItem('userCodeInfo'))
  this.orgName = this.logedInUserRoles.organizationName;


  this.userName = this.logedInUserRoles.userName;
  this.fName = this.logedInUserRoles.firstName;
  this.lName = this.logedInUserRoles.lastName;

  }

  logout(){
    // console.log("logout----",this.logedInUserRoles);
    $('#logoutModal').modal('hide');

    this.authService.logout(this.logedInUserRoles.token);
  }

}
