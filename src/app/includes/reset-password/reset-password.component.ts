import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth/auth.service';
// import { AuthGuard } from '../../../services/auth/auth.guard';
import { environment } from '../../../environments/environment';
declare var $ :any;
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NotifierService } from 'angular-notifier';

// import { ReCaptcha2Component } from 'ngx-captcha';
// import { RecaptchaModule } from 'ng-recaptcha';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  form: FormGroup;
  // private formSubmitAttempt: boolean;
  submitted = false;
  loginFailed = false;
 //  @ViewChild('captchaElem') captchaElem: ReCaptcha2Component;
 // @ViewChild('langInput') langInput: ElementRef;
 // captchaSuccess = false;

 public captchaIsLoaded = false;
 public captchaSuccess = false;
 public captchaIsExpired = false;
 public captchaResponse?: string;

 public theme: 'light' | 'dark' = 'light';
 public size: 'compact' | 'normal' = 'normal';
 public lang = 'en';
 public type: 'image' | 'audio';

 public forgotRequest = {
   header:{
     uuid:"",
     partnerCode:"",
     userCode:"",
     referenceNumber:"",
     systemCode:"",
     moduleCode:"",
     functionalityCode:"",
     systemHostAddress:"",
     remoteUserAddress:"",
     dateTime:""
   },
   data:{
     userEmail:null
   }
 }
 errorResponseFromServer = false;
 errorMessage ="";

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private ngxLoader    : NgxUiLoaderService,
    private http            : HttpClient,
    private notifier        : NotifierService,
  ) {
    this.notifier        = notifier;
  }

  ngOnInit(): void {
    this.ngxLoader.stop();
    $('.modal-backdrop').css('display','none');

    this.form = this.fb.group({
      userName: ['', [Validators.required, Validators.email]]
      // password: ['', Validators.required]
    });
  }

  get f() { return this.form.controls; }
  onSubmit() {
    this.ngxLoader.start();
    this.submitted = true;
    // console.log("this.form.valid",this.form.valid);

    if (this.form.valid) {
      // if (this.captchaSuccess == true) {
        var url = environment.API_USER_ENDPOINT+'sendotp';
        var otpRequest = {...this.forgotRequest}
        this.http.put(url,otpRequest).subscribe(otpResp=>{
          this.ngxLoader.stop();
          this.submitted = false;
          console.log('otpResp',otpResp);
          if (typeof otpResp['message'] != 'undefined') {

            this.notifier.notify("error",otpResp['message']);
            this.errorResponseFromServer = false;
            return;
          }
          if (otpResp['result'].codeType == "S") {
            // this.router.navigate(['forgot-password']);
              this.notifier.notify("success",otpResp['result'].description);
          }
          else{
            // this.errorResponseFromServer = true;
            this.errorMessage ="";
            // this.notifier.notify("error","Please try again");
            this.notifier.notify("error",otpResp['result'].description);

          }
        },error=>{
          this.ngxLoader.stop();
          this.notifier.notify("error","Please try again");
        })
      // }
      // else{
      //   this.ngxLoader.stop();
      //   alert('please check captcha')
      //   // this.submitted = false;
      //   return;
      // }

      // this.authService.login(this.form.value);


    }
    else{
      this.ngxLoader.stop();
      // this.submitted = false;
      return;
    }
    // this.formSubmitAttempt = true;
  }

  public resolved(captchaResponse: string) {
   console.log(`Resolved captcha with response: ${captchaResponse}`);
   this.captchaSuccess = true;
 }
 public onError(errorDetails: any): void {
   console.log(`reCAPTCHA error encountered; details:`, errorDetails);
 }

}
