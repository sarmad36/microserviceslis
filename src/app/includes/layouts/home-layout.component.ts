import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth/auth.service';
import { RouteConfigLoadEnd } from "@angular/router";
import { RouteConfigLoadStart } from "@angular/router";
import { Event as RouterEvent } from "@angular/router";
import { Router } from "@angular/router";
declare var $ :any;
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-home-layout',
  templateUrl: `./home-layout.component.html`,
  styles: []
})
export class HomeLayoutComponent  implements OnInit {
  public isShowingRouteLoadIndicator: boolean;
  constructor(
    private authService: AuthService,
    router: Router,
    private ngxLoader    : NgxUiLoaderService,
  ) {
    this.isShowingRouteLoadIndicator = false;
    // console.log("constatructor -------------");


    // As the router loads modules asynchronously (via loadChildren), we're going to
    // keep track of how many asynchronous requests are currently active. If there is
    // at least one pending load request, we'll show the indicator.
    var asyncLoadCount = 0;

    // The Router emits special events for "loadChildren" configuration loading. We
    // just need to listen for the Start and End events in order to determine if we
    // have any pending configuration requests.
    router.events.subscribe(
      ( event: RouterEvent ) : void => {


        if ( event instanceof RouteConfigLoadStart ) {
          // $('.nav-link').addClass('collapsed');
          // $('.nav-link').attr('aria-expanded','false');
          // $('.collapse').addClass('hide');
          // console.log("$('.nav-link')",$('.nav-link'));

    // $('.navbar-toggler').click(); //bootstrap 4.x

          this.ngxLoader.start();
          $('#LoaderModal1').modal('show')
          asyncLoadCount++;
          // console.log("SARMADNAZIRABBASI222222",history.state);
          if (history.state != 'null') {
            if (typeof history.state.fromCreate != "undefined") {
              localStorage.setItem("FromCreate",'true');
            }
            else{
              localStorage.setItem("FromCreate",'false');
            }
          }


          // console.log("asyncLoadCount",asyncLoadCount);
          // this.isShowingRouteLoadIndicator = true;


        } else if ( event instanceof RouteConfigLoadEnd ) {

          asyncLoadCount--;

          this.ngxLoader.stop();
          $('#LoaderModal1').modal('hide')
          // console.log("asyncLoadCount22222",asyncLoadCount);
          // this.isShowingRouteLoadIndicator =false;

        }

        // If there is at least one pending asynchronous config load request,
        // then let's show the loading indicator.
        // --
        // CAUTION: I'm using CSS to include a small delay such that this loading
        // indicator won't be seen by people with sufficiently fast connections.
        this.isShowingRouteLoadIndicator = !! asyncLoadCount;
        // console.log("this.isShowingRouteLoadIndicator",this.isShowingRouteLoadIndicator);


      }
    );
  }

  ngOnInit() {
     this.bindJQueryFuncs();
     this.isShowingRouteLoadIndicator = false;
    
     
  }
 
  bindJQueryFuncs() {
    // $('.sidebar .collapse').collapse('hide');
    $('.nav-item .collapse-item').on('click', function(){
      $('.nav-item  .collapse-item').collapse('hide');
      for (let i = 0; i < $('.nav-item .collapse-item').length; i++) {
        // console.log("$('.nav-item  .collapse-item')[i].classList",$('.nav-item  .collapse-item')[i].classList);

        if (typeof $('.nav-item  .collapse-item')[i].classList != 'undefined') {

          $('.nav-item .collapse-item')[i].parentElement.parentElement.parentElement.children[1].classList.remove("show")
          // $('.nav-item  .collapse-item').collapse('hide');
        }
      }

    });
    // Scroll to top button appear
    $(document).on('scroll', function() {
      var scrollDistance = $(this).scrollTop();
      if (scrollDistance > 100) {
        $('.scroll-to-top').fadeIn();
      } else {
        $('.scroll-to-top').fadeOut();
      }
    });

    // Smooth scrolling using jQuery easing
    $(document).on('click', 'a.scroll-to-top', function(e) {
      var $anchor = $(this);
      $('html, body').stop().animate({
        scrollTop: ($($anchor.attr('href')).offset().top)
      }, 1000, 'easeInOutExpo');
      e.preventDefault();
    });
  }
  gotoTop() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }
  logout(){
    // console.log("logout");

    this.authService.logout("null");
  }
}
