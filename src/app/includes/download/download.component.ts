import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse, HttpEvent } from '@angular/common/http';
import { ActivatedRoute, RouterStateSnapshot } from '@angular/router';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import Swal from 'sweetalert2'
declare var $ :any;
import { NotifierService } from 'angular-notifier';
import * as CanvasJS from '../../../assets/js/canvasjs.min.js';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';
import * as CryptoJS from 'crypto-js';
import { AuthService } from '../../../services/auth/auth.service';
import { AuthGuard } from '../../../services/auth/auth.guard';
import { catchError, debounceTime, distinctUntilChanged, switchMap, tap, delay, map } from "rxjs/operators";
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.css']
})
export class DownloadComponent implements OnInit {
  public logedInUserRoles :any = {};
  state ;
  string;
  downloadSuccess = false
  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);


  constructor(
    private formBuilder  : FormBuilder,
    private http         : HttpClient,
    private router       : Router ,
    private route           : ActivatedRoute,
    private sanitizer       : DomSanitizer,
    private ngxLoader       : NgxUiLoaderService,
    private notifier        : NotifierService,
    private datePipe: DatePipe,
    private authService: AuthService,
    private authGaurd : AuthGuard,
  ) {
  }

  ngOnInit(): void {
  //   this.route.params.subscribe(params => {
  //     console.log('params',params);
  //     console.log("this.route.snapshot.url[0].path",this.route.snapshot.url[0].path);
  //
  //   // return
  // })
  // return;
    //   // console.log("routes");
    //   // console.log(this.route.snapshot.url); // array of states
    //   // console.log(this.route.snapshot.url[0].path);
    //   this.state = this.route.snapshot.url[0].path;
    //
    //
    //   if (typeof params['id'] != "undefined") {
    //     this.string = params['id'];

    this.authService.isLoggedIn.subscribe(resp=>{
      console.log("resp",resp);
      if (resp == true) {
        this.route.params.subscribe(params => {
          // console.log('params',params);
          // return;
          // console.log("routes");
          // console.log(this.route.snapshot.url); // array of states
          // console.log(this.route.snapshot.url[0].path);
          this.state = this.route.snapshot.url[0].path;


          if (typeof params['id'] != "undefined") {
            this.string = params['id'];
            var corsHeaders = new HttpHeaders({
              'Content-Type': 'application/octet-stream',
              'Access-Control-Expose-Headers':"Content-Diposition",
              Authorization: `Bearer ${this.logedInUserRoles.token}`,
            });
            // console.log('corsHeaders',corsHeaders);

            var url = environment.API_REPORTING_ENDPOINT + 'download/'+params['id']+'/'+params['clientname'];
            this.http.get(url, {headers: corsHeaders, responseType: 'blob', observe:"response"})
            .subscribe( result => {
              this.downloadSuccess = true;
              var a = result.headers.get('Content-Disposition');
              var b = a.split(';');
              var c= b[1].split('"');
              // console.log("c",c);

              var type = c[1].split(".");
              // localStorage.getItem('fromD')

              this.downLoadFile(result['body'],type[1],c[1])
              localStorage.removeItem('fromD')
              // resp;
            },error=>{
              this.downloadSuccess = false;
              console.log("error",error);

            })

          }
          else{
            // this.router.navigate(['/login']);
            this.router.navigate(['/login'], { queryParams: { returnUrl: this.state, string:  this.string }});
          }
        })


      }
      else{
        // this.router.navigate(['/login']);
        this.router.navigate(['/login'], { queryParams: { returnUrl: this.state, string:  this.string}});
      }

    })
    //   }
    //   else{
    //     // this.router.navigate(['/login']);
    //     this.router.navigate(['/login'], { queryParams: { returnUrl: this.state, string:  this.string }});
    //   }
    // })
  }

  downLoadFile(data: any, type: any, name: any) {
    // let blob = new Blob([data], { type: type});\
    console.log("type",type);
    if (type != 'pdf' && type != 'zip') {
      const blob = new Blob([data], { type: 'image/'+type });
      let url = window.URL.createObjectURL(blob);
      // window.open(url);
      const downloadLink = document.createElement("a");
      downloadLink.download = name;

      downloadLink.href = url;
      downloadLink.click();
    }
    else{

      const blob = new Blob([data], { type: 'application/'+type });
      console.log("blob",blob);

      let url = window.URL.createObjectURL(blob);
      const downloadLink = document.createElement("a");
      downloadLink.download = name;

      downloadLink.href = url;
      downloadLink.click();
      // this.notifier.notify('success','file downloaded successfull')
    }

  }

}
