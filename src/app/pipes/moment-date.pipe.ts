import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import * as moment from 'moment-timezone';
/**
* A moment timezone pipe to support parsing based on time zone abbreviations
* covers all cases of offset variation due to daylight saving.
*
* Same API as DatePipe with additional timezone abbreviation support
* Official date pipe dropped support for abbreviations names from
Angular V5
*/
@Pipe({
  name: 'momentDate'
})
export class MomentDatePipe extends DatePipe implements PipeTransform {
  currentZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
  offset = new Date().getTimezoneOffset();


  transform(
    value: any,
    format: string = 'MM/dd/yyyy hh:mm a',
    timezone: string = this.currentZone
  ): string {
    const offset = new Date().getTimezoneOffset();
    // console.log("offset",offset);
    // console.log("this.currentZone",this.currentZone);
    // console.log("value",value);
    var newval = [];
    if (typeof value != 'undefined') {
      if (value != null) {
        newval = value.split('.');
      }

    }
    // var check = '2021-03-29T07:51:43';
    // const timezoneOffset1 = moment(check).tz(this.currentZone).format();
    // // console.log("super.transform(newval[0], format, timezoneOffset1);",super.transform(check, format, timezoneOffset1));

    const timezoneOffset = moment(newval[0]).tz(this.currentZone).format();
    // console.log("super.transform(newval[0], format, timezoneOffset);",super.transform(newval[0], format, timezoneOffset));
    return super.transform(newval[0], format, timezoneOffset);
  }
}
