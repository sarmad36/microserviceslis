import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import * as moment from 'moment-timezone';

@Pipe({
  name: 'momentDateWithTz'
})
export class MomentDateWithTzPipe extends DatePipe implements PipeTransform {

  currentZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
  offset = new Date().getTimezoneOffset();


  transform(
    value: any,
    format: string = 'MM/dd/yyyy hh:mm a',
    timezone: string = this.currentZone
  ): string {
    const offset = new Date().getTimezoneOffset();
    // console.log("offset",offset);
    // console.log("this.currentZone",this.currentZone);
    // console.log("value",value);



    const timezoneOffset = moment(value).tz(this.currentZone).format();
    // console.log("super.transform(newval[0], format, timezoneOffset);",super.transform(newval[0], format, timezoneOffset));
    return super.transform(value, format, timezoneOffset);
  }

}
