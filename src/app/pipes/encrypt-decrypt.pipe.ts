import { Pipe, PipeTransform } from '@angular/core';
import * as CryptoJS from 'crypto-js';

@Pipe({
  name: 'encrypted'
})
export class EncryptPipe implements PipeTransform {
  // base64Key = 'em5MMEZlOEt6Y05mRHc2Zg==';
  // key = CryptoJS.enc.Base64.parse(this.base64Key);
  public logedInUserRoles :any = {};


  transform(value: string): unknown {
    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
    var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
    var originalText = bts.toString(CryptoJS.enc.Utf8);
    this.logedInUserRoles = JSON.parse(originalText);
    var base64Key = this.logedInUserRoles.keyToken;
    var key = CryptoJS.enc.Base64.parse(base64Key);

    var encryptedData = CryptoJS.AES.encrypt(value, key, {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7
    }).toString();
    var jsonEncypted = CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(encryptedData));
    return jsonEncypted;
  }

}

@Pipe({
  name: 'decrypted'
})
export class DecryptPipe implements PipeTransform {
  // base64Key = 'em5MMEZlOEt6Y05mRHc2Zg==';
  // key = CryptoJS.enc.Base64.parse(this.base64Key);
  public logedInUserRoles :any = {};

  transform(value: string): unknown {
    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
    var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
    var originalText = bts.toString(CryptoJS.enc.Utf8);
    this.logedInUserRoles = JSON.parse(originalText);
    var base64Key = this.logedInUserRoles.keyToken;
    var key = CryptoJS.enc.Base64.parse(base64Key);
    if (value != null && value != '') {
      var parsedValue = CryptoJS.enc.Base64.parse(value).toString(CryptoJS.enc.Utf8);
      var decryptedText = CryptoJS.AES.decrypt(parsedValue, key, {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
          });
      return decryptedText.toString(CryptoJS.enc.Utf8);
    }
    else{
      return '';
    }


  }

}
