import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RbacDirective } from '../../directives/rbac.directive';
import { MomentDatePipe } from '../../pipes/moment-date.pipe';
import { MomentDateWithTzPipe } from '../../pipes/moment-date-with-tz.pipe';
import { EncryptPipe, DecryptPipe} from '../../pipes/encrypt-decrypt.pipe';
import * as CryptoJS from 'crypto-js';



@NgModule({
  declarations: [RbacDirective, MomentDatePipe, MomentDateWithTzPipe, EncryptPipe, DecryptPipe],
  imports: [
    CommonModule
  ],
  exports:    [RbacDirective, MomentDatePipe, MomentDateWithTzPipe, EncryptPipe, DecryptPipe]
})
export class GlobalySharedModule {
  // base64Key = 'em5MMEZlOEt6Y05mRHc2Zg==';
  // key = CryptoJS.enc.Base64.parse(this.base64Key);
  public logedInUserRoles :any = {};


  encryptString(value: any): string {
    // console.log("value",value);
    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
    var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
    var originalText = bts.toString(CryptoJS.enc.Utf8);
    this.logedInUserRoles = JSON.parse(originalText);
    var base64Key = this.logedInUserRoles.keyToken;
    var key = CryptoJS.enc.Base64.parse(base64Key);

    var encryptedData = CryptoJS.AES.encrypt(value, key, {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7
    }).toString();
    var jsonEncypted = CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(encryptedData));
    return jsonEncypted;
    // return encryptedData;
  }

  decryptString(value: any): string {
    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
    var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
    var originalText = bts.toString(CryptoJS.enc.Utf8);
    this.logedInUserRoles = JSON.parse(originalText);
    var base64Key = this.logedInUserRoles.keyToken;
    var key = CryptoJS.enc.Base64.parse(base64Key);
    if (value != null && value != '') {
      var parsedValue = CryptoJS.enc.Base64.parse(value).toString(CryptoJS.enc.Utf8);
      var decryptedText = CryptoJS.AES.decrypt(parsedValue, key, {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
          });
      return decryptedText.toString(CryptoJS.enc.Utf8);
    }
    else{
      return '';
    }


  }
  // static forRoot(){
  //     return {
  //       ngModule: GlobalySharedModule,
  //       providers: [RbacDirective],
  //     }
  //   }
}
