import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouteReuseStrategy } from '@angular/router';
// import { MicroFrontendRouteReuseStrategy } from 'src/services/route-reuse-strategy';
import { DashboardComponent } from './includes/dashboard/dashboard.component';
import { NavbarComponent } from './includes/navbar/navbar.component';
import { SidebarComponent } from './includes/sidebar/sidebar.component';
import { FooterComponent } from './includes/footer/footer.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { AppRoutingModule } from './app-routing.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { DataTablesModule } from 'angular-datatables';
import { NgxPopper } from 'angular-popper';
import { NgxUiLoaderModule } from  'ngx-ui-loader';
import { NotifierModule, NotifierOptions } from 'angular-notifier';
import { LoginComponent } from './includes/login/login.component';
import { HomeLayoutComponent } from './includes/layouts/home-layout.component';
import { LoginLayoutComponent } from './includes/layouts/login-layout.component';
import { RouterModule } from '@angular/router';

import { AuthGuard } from '../services/auth/auth.guard';
import { AuthService } from '../services/auth/auth.service';
import { AuthApiCallsService } from '../services/auth/api-calls/api-calls';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { ChangePasswordComponent } from './includes/change-password/change-password.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { AppGuard } from "../services/gaurd/app.guard";
import { DataService } from "../services/data/data.service";
// import { BreadcrumbComponent } from './includes/breadcrumb/breadcrumb.component';
// import { NgIdleKeepaliveModule } from '@ng-idle/core';
// import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { UserIdleModule } from 'angular-user-idle';
import { ResetPasswordComponent } from './includes/reset-password/reset-password.component';
// import { NgxCaptchaModule } from 'ngx-captcha';
// import {
//   RECAPTCHA_SETTINGS,
//   RecaptchaModule,
//   RecaptchaSettings,
//   RecaptchaFormsModule
// } from 'ng-recaptcha';
// const globalSettings: RecaptchaSettings = { siteKey: '6LcOuyYTAAAAAHTjFuqhA52fmfJ_j5iFk5PsfXaU' };
import { RecaptchaModule } from 'ng-recaptcha';
import { ForgotPasswordComponent } from './includes/forgot-password/forgot-password.component';
// import { SortablejsModule } from 'ngx-sortablejs'
import { DragDropModule } from "@angular/cdk/drag-drop";
import { BasicAuthInterceptor } from '../services/auth/basic-auth.interceptor';
import { TokenInterceptor } from '../services/auth/response-token.interceptor';
import { ErrorInterceptor } from '../services/auth/error.interceptor';
// import { RbacDirective } from './directives/rbac.directive';

import { GlobalySharedModule } from './sharedmodules/globaly-shared/globaly-shared.module';
import { PageRestrictedComponent } from './includes/page-restricted/page-restricted.component';

import { MomentDatePipe } from './pipes/moment-date.pipe';
import { MomentDateWithTzPipe } from './pipes/moment-date-with-tz.pipe';
import { DownloadComponent } from './includes/download/download.component';
// import { EncryptDecryptPipe } from './pipes/encrypt-decrypt.pipe'
// import { EncryptPipe, DecryptPipe} from './pipes/encrypt-decrypt.pipe';
/**
 * Custom angular notifier options
 */
const customNotifierOptions: NotifierOptions = {
  position: {
		horizontal: {
			position: 'left',
			distance: 110
		},
		vertical: {
			position: 'top',
			distance: 100,
			gap: 10
		}
	},
  theme: 'material',
  behaviour: {
    autoHide: 3000,
    onClick: 'hide',
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 300,
      easing: 'ease'
    },
    hide: {
      preset: 'fade',
      speed: 300,
      easing: 'ease',
      offset: 50
    },
    shift: {
      speed: 300,
      easing: 'ease'
    },
    overlap: 150
  }
};


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    NavbarComponent,
    SidebarComponent,
    FooterComponent,
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    FooterComponent,
    DashboardComponent,
    LoginComponent,
    HomeLayoutComponent,
   LoginLayoutComponent,
   ChangePasswordComponent,
   ResetPasswordComponent,
   ForgotPasswordComponent,
   PageRestrictedComponent,
   DownloadComponent,


   // MomentDateWithTzPipe,

   // MomentDatePipe,
   // RbacDirective,
  //  CreateIntakeComponent,

   // NgxCaptchaModule
  //  BreadcrumbComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NgSelectModule,
    DataTablesModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    NgxUiLoaderModule,
    NgxPopper,
    RouterModule,
    // NgxUiLoaderModule,
    NotifierModule.withConfig(customNotifierOptions),
    NgxDaterangepickerMd.forRoot(),
    UserIdleModule.forRoot({idle: 1800, timeout: 10, ping: 120}),
    // UserIdleModule.forRoot({idle: 20, timeout: 10, ping: 120}),
    RecaptchaModule,
    DragDropModule,
    GlobalySharedModule,
    // EncryptPipe,
    // DecryptPipe

    // RecaptchaFormsModule
  ],
  // {provide: LocationStrategy, useClass: HashLocationStrategy}
  providers: [AuthService, AuthGuard,AuthApiCallsService,AppGuard,DataService,
    { provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor , multi: true },
    // {
    //   provide: RECAPTCHA_SETTINGS,
    //   useValue: globalSettings,
    // },
  ],
  // providers: [AuthService, AuthGuard,AuthApiCallsService,{
  //   provide: RouteReuseStrategy,
  //   useClass: MicroFrontendRouteReuseStrategy
  // }],
  bootstrap: [AppComponent],
  // exports:    [RbacDirective]
})
export class AppModule { }
// export class NglisRbaceDIrextive{
//
//   static forRoot(){
//     return {
//       ngModule: AppModule,
//       providers: RbacDirective,
//     }
//   }
// }
