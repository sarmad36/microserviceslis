// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  API_USER_ENDPOINT      : 'http://localhost:8088/apigateway/nglis/usrm/',
  API_PARTNER_ENDPOINT   : 'http://localhost:8088/apigateway/nglis/usrm/',
  API_ROLE_ENDPOINT      : 'http://localhost:8088/apigateway/nglis/usrm/',
  API_CLIENT_ENDPOINT    : 'http://localhost:8088/apigateway/nglis/cltm/',
  API_PATIENT_ENDPOINT   : 'http://localhost:8088/apigateway/nglis/pntm/',
  API_SPECIMEN_ENDPOINT  : 'http://localhost:8088/apigateway/nglis/spcm/',
  API_COVID_ENDPOINT     : 'http://localhost:8088/apigateway/nglis/twmm/',
  API_REPORTING_ENDPOINT : 'http://localhost:8088/apigateway/nglis/mrmm/',
  API_LOOKUP_ENDPOINT    : 'http://localhost:8088/apigateway/nglis/sadm/',
  API_AUTHN_ENDPOINT     : 'http://localhost:8088/apigateway/',
  API_AUTHZ_ENDPOINT     : 'http://localhost:8088/apigateway/nglis/authz/',
  API_RTL_ENDPOINT       : 'http://localhost:8088/apigateway/nglis/rtal/',
  API_ACDM_ENDPOINT       : 'http://localhost:8088/apigateway/nglis/acdm/',
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
