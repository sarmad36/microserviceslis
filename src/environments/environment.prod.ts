export const environment = {
  production: true,
  API_USER_ENDPOINT      : 'http://192.168.2.8:8088/apigateway/nglis/usrm/',
  API_PARTNER_ENDPOINT   : 'http://192.168.2.8:8088/apigateway/nglis/usrm/',
  API_ROLE_ENDPOINT      : 'http://192.168.2.8:8088/apigateway/nglis/usrm/',
  API_CLIENT_ENDPOINT    : 'http://192.168.2.8:8088/apigateway/nglis/cltm/',
  API_PATIENT_ENDPOINT   : 'http://192.168.2.8:8088/apigateway/nglis/pntm/',
  API_SPECIMEN_ENDPOINT  : 'http://192.168.2.8:8088/apigateway/nglis/spcm/',
  API_COVID_ENDPOINT     : 'http://192.168.2.8:8088/apigateway/nglis/twmm/',
  API_REPORTING_ENDPOINT : 'http://192.168.2.8:8088/apigateway/nglis/mrmm/',
  API_AUTHN_ENDPOINT     : 'http://192.168.2.8:8088/apigateway/',
  API_AUTHZ_ENDPOINT     : 'http://192.168.2.8:8088/apigateway/nglis/authz/',
  API_LOOKUP_ENDPOINT    : 'http://192.168.2.8:8088/apigateway/nglis/sadm/',
  API_RTL_ENDPOINT       : 'http://192.168.2.8:8088/apigateway/nglis/rtal/',
  API_ACDM_ENDPOINT       : 'http://192.168.2.8:8088/apigateway/nglis/acdm/',
};

if(window){
  window.console.log = window.console.warn = window.console.info = function(){
    // Don't log anything.
  }
}
