import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot ,CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AppGuard implements CanActivate , CanDeactivate<any>{
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

      // console.log('activate gaurd', {next,state});
    return true;
  }
  canDeactivate(component: any, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot)

    {
      // console.log("component.diasbleBack",component.diasbleBack);
      console.log("component",component);

      if (component != null) {
        if(component.diasbleBack){
          if (confirm("Changes You made will not be saved, Do you still want to move?")) {
            return true;
          } else {
            return false;
          }

        }
      }
      else{
        return true;
      }
       // console.log('deactivate gaurd', {component,currentRoute ,currentState , nextState});
      return true;
    }
}
