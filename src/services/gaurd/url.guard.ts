import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot ,CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from "@angular/router";
import * as CryptoJS from 'crypto-js';

@Injectable({ providedIn: 'root' })
export class UrlGuard {
  public logedInUserRoles :any = {};
  constructor(
    private router       : Router ,

  ) {}
  checkROle(actionCode) {
    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
var originalText = bts.toString(CryptoJS.enc.Utf8);
this.logedInUserRoles = JSON.parse(originalText)
    console.log("Sarmad",actionCode);
    if (this.logedInUserRoles != null) {
      if(this.logedInUserRoles['allowedRoles'].indexOf(actionCode) == -1){
        this.router.navigate(['page-restrict']);
      }
    }



  }
}
