import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { User } from './user';
import { AuthApiCallsService } from './api-calls/api-calls'
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { DataService } from "../data/data.service";
import * as CryptoJS from 'crypto-js';
import { environment } from '../../environments/environment';


@Injectable()
export class AuthService {
  // private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public loginRequest : any ={
    partnerCode:"sip",
    userName:"",
    password: ""
  }
  public rolesRequest : any ={
    partnerCode:"1",
    userCode:"",
  }

  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }
  // public logedInUserRoles :any = {};

  constructor(
    private router: Router,
    private ngxLoader    : NgxUiLoaderService,
    private authCalls : AuthApiCallsService,
    private sharedData: DataService
  ) {
    if (localStorage.getItem('userRolesInfo') !== null) {
      // this.authService.isLoggedIn.

      // console.log("In IFFFFFFFFFFF ",localStorage.getItem('userRolesInfo'));

      this.loggedIn.next(true);
    }
    else{
      console.log("else");

      this.loggedIn.next(false);
    }
  }

  login(user: User, returnUrl: String, downloadString: string) {

    if (user.userName !== '' && user.password !== '' ) {
      var request = {...this.loginRequest};
      request.userName = user.userName;
      request.password = user.password;
      return this.authCalls.login(request).then(loginResponse =>{
        console.log("loginResponse",loginResponse);
        if (typeof loginResponse['error'] != 'undefined') {
          if (loginResponse['error']['message'].indexOf('Unable to acquire JDBC Connection') != -1) {
            alert("No response from server please check your connection")
            // return false;
            this.ngxLoader.stop();
            this.router.navigate(['/login']);
            return;
          }
          else if (loginResponse['error']['message'].indexOf('UNAUTHORIZED') != -1) {
            alert("Invalid username or password")
            // return false;
            this.ngxLoader.stop();
            this.router.navigate(['/login']);
            return;
          }
          else{
            alert(loginResponse['error']['message'])
            this.ngxLoader.stop();
            this.router.navigate(['/login']);
            return;
          }

        }

        if (typeof loginResponse['token'] != 'undefined') {
        var rolesHolder = [];
        var count = 0;

          loginResponse['allowedRoles'] = loginResponse['roleActions'];
          // localStorage.setItem('userLoginInfo',JSON.stringify(request));
          // localStorage.setItem('userCodeInfo',JSON.stringify(loginResponse));

          var a = JSON.stringify(loginResponse);
          // var pc = loginResponse['email'] + loginResponse['userType'] + loginResponse['token'].substring(0, 4);

          // var ax = CryptoJS.AES.encrypt(a, 'nglisSarmad').toString();

          let encJson = CryptoJS.AES.encrypt(a, 'nglisSarmad').toString()
          let encData = CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(encJson))
          console.log('encData',encData);

          localStorage.setItem("userRolesInfo",encData);
          // localStorage.setItem("userRolesInfo",JSON.stringify(loginResponse));
          // this.loggedIn.next(true);
          if (returnUrl == 'download') {
            localStorage.setItem('fromD',downloadString);
          }
          setTimeout (() => {

            this.ngxLoader.stop();
            this.loggedIn.next(true);
            this.router.navigate(['/']);
            // console.log('returnUrl',returnUrl);
            // console.log('downloadString',downloadString);
            // return;

            // if (returnUrl == 'download') {
            //   // localStorage.setItem('fromD',downloadString);
            //   // this.router.navigate(['/download/',downloadString]);
            //   this.loggedIn.next(true);
            // }
            // else{
            //   this.loggedIn.next(true);
            //   this.router.navigate(['/']);
            // }
          }, 1000)

        }
        else{
          alert("No response from server please check your connection")
          // return false;
          this.ngxLoader.stop();
          this.router.navigate(['/login']);
        }
      })
      .catch(error=>{
        // console.log("error",error);

        alert("No response from server please check your connection")
        this.ngxLoader.stop();
        this.router.navigate(['/login']);
      })
      // this.loggedIn.next(true);
      // this.router.navigate(['/']);
    }
    else{
      this.ngxLoader.stop();
      alert("Please provide username and password")
      return;
    }
  }

  logout(token: any) {


    this.authCalls.logout(token).then(reps=>{
      console.log("reps.message",reps.message);

      if (reps.message == "200") {
        // console.log("Before",JSON.parse(localStorage.getItem('userRolesInfo')));

        localStorage.removeItem('userLoginInfo');
        localStorage.removeItem('userCodeInfo');
        localStorage.removeItem('userRolesInfo');


        localStorage.clear();
        // this.sharedData.checkUSerActivity('null');
        // console.log("after",JSON.parse(localStorage.getItem('userRolesInfo')));
        this.loggedIn.next(false);
        this.router.navigate(['/login']);
      }
      else{
        this.loggedIn.next(false);
        this.router.navigate(['/login']);
      }

    })

  }
}
