import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import { of } from 'rxjs';
import { BehaviorSubject, Observable, throwError, Subject } from 'rxjs';
import { catchError, retry, finalize, tap, map, takeUntil, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';
// import { DataService } from "../../data/data.service";
import { NgxUiLoaderService } from 'ngx-ui-loader';
import * as CryptoJS from 'crypto-js';

@Injectable()
export class AuthApiCallsService {
  public logedInUserRoles :any = {};
  public corsHeaders: any = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'application/json',
  });

  constructor(private http: HttpClient,
    private router: Router,
    private ngxLoader    : NgxUiLoaderService,) {
      if (localStorage.getItem('userRolesInfo') != null) {
        let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
        var originalText = bts.toString(CryptoJS.enc.Utf8);
        this.logedInUserRoles = JSON.parse(originalText)
      }

    // console.log('this.logedInUserRoles',this.logedInUserRoles);
    // if (this.logedInUserRoles != null) {
    //   this.corsHeaders['Authorization'] = this.logedInUserRoles.token;
    //   // console.log('this.corsHeaders',this.corsHeaders);
    //   // console.log('this.corsHeaders[] ',this.corsHeaders['Authorization'] );
    //
    // }
   }

  login(loginData){
    var url = environment.API_AUTHN_ENDPOINT+'authenticate'
    return this.http.post(url,loginData)
    .pipe( )
    .toPromise()
    .then( resp => {
      return resp;
    })
    .catch(error => {
      // alert("No response from server please check your connection")
      // this.ngxLoader.stop();
      // this.router.navigate(['/login']);
      return error;
    });
  }
  getRoles(loginData){
    var url = environment.API_AUTHZ_ENDPOINT+'logout'
    return this.http.post(url,loginData)
    .pipe( )
    .toPromise()
    .then( resp => {
      return resp;
    })
    .catch(error => {
      return error;
    });
  }
  logout(token){
    var authToken = ''
    // console.log('this.corsHeaders',this.corsHeaders);
    // return;
    if (this.logedInUserRoles == null) {
      authToken = this.logedInUserRoles;
    }
    else{
      authToken = token;
    }
    const headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer '+authToken
    });
    var url = environment.API_AUTHN_ENDPOINT+'logout'
    return this.http.get(url, {headers})
    .pipe( )
    .toPromise()
    .then( resp => {
      return resp;
    })
    .catch(error => {
      return error;
    });
  }
}
