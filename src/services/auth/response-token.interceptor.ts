import { Injectable,Injector } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse } from '@angular/common/http';
import { Observable, } from 'rxjs';
import { AuthService } from './auth.service';
import { map, catchError, } from 'rxjs/operators';
import { UserIdleService } from 'angular-user-idle';
import { DataService } from "../data/data.service";
import * as CryptoJS from 'crypto-js';
import { Router, NavigationEnd } from "@angular/router";
import { NotifierService } from 'angular-notifier';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  public logedInUserRoles :any = {};
  constructor(private injector: Injector, private notifier : NotifierService, private router : Router ,
    private authService: AuthService, private userIdle: UserIdleService,private sharedData: DataService) {
    if (localStorage.getItem('userRolesInfo') != null) {
      let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
      var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
      var originalText = bts.toString(CryptoJS.enc.Utf8);
      this.logedInUserRoles = JSON.parse(originalText)
    }
    else{
      this.logedInUserRoles = null
    }
   }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
          map((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {



              if (this.logedInUserRoles != null) {
                if(event.headers.get('Authorization') == '' || event.headers.get('Authorization') == null){
                  // this.authService.logout("null");
                }
                else {
                  // console.log("event['body']",event['body']);

                  if (typeof event['body']['result'] != 'undefined') {
                    if (typeof event['body']['result']['description'] != 'undefined') {
                      if (event['body']['result']['description'].indexOf('UnAuthorized') != -1) {
                        // this.notifier.notify('warning',event['body']['result']['description'])
                        // this.router.navigateByUrl('/');
                      }
                    }
                  }


                  this.sharedData.checkUSerActivity('true');
                  // console.log("event.headers.get('Authorization')",event.headers.get('Authorization'));
                  // console.log("this.logedInUserRoles.token",this.logedInUserRoles.token);
                  // var temp = event.headers.get('Authorization').split(' ');
                  // console.log('temp',temp);
                  // console.log('temp[1]',temp[1]);

                  if (event.headers.get('Authorization') != `Bearer ${this.logedInUserRoles.token}`) {
                    // console.log("mis match --- 1");


                  // this.logedInUserRoles.token = event.headers.get('Authorization');
                  // console.log("Inside event.headers.get('Authorization')",event.headers.get('Authorization'));
                  // console.log("this.logedInUserRoles.token",this.logedInUserRoles.token);

                  var temp = event.headers.get('Authorization').split(' ');
                  this.logedInUserRoles.token = temp[1];
                  // this.logedInUserRoles.token = event.headers.get('Authorization');
                }
              }
              }



              // let camelCaseObject = mapKeys(event.body, (v, k) => camelCase(k));
              const modEvent = event.clone({ body: event.body });

              return modEvent;
            }
          })
          // console.log("token interceptor",resp);
          // console.log("token interceptor request",request);
          // console.log("token interceptor next",next);

            // return resp;

            // const error = err.error.message || err.statusText;
            // return throwError(error);
        )
    }
}
