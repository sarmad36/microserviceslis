import { Injectable,Injector } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { UserIdleService } from 'angular-user-idle';
import { DataService } from "../data/data.service";
import * as CryptoJS from 'crypto-js';


// import { AuthenticationService } from '../_services';

@Injectable()
export class BasicAuthInterceptor implements HttpInterceptor {
  public logedInUserRoles :any = {};
  constructor(private injector: Injector, private userIdle: UserIdleService,private sharedData: DataService) {
    if (localStorage.getItem('userRolesInfo') != null) {
      let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
      var originalText = bts.toString(CryptoJS.enc.Utf8);
      this.logedInUserRoles = JSON.parse(originalText)
    }
    else{
      this.logedInUserRoles = null
    }
   }
    // constructor(private authenticationService: AuthenticationService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      // const auth = this.injector.get(AuthService);
        // add authorization header with basic auth credentials if available
        // const currentUser = this.authenticationService.currentUserValue;

        if (localStorage.getItem('userRolesInfo') != null) {
          let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
          var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
          var originalText = bts.toString(CryptoJS.enc.Utf8);
          this.logedInUserRoles = JSON.parse(originalText)
        }
        else{
          this.logedInUserRoles = null
        }

        // console.log('this.logedInUserRoles',this.logedInUserRoles);

        if (this.logedInUserRoles != null) {
          this.userIdle.stopTimer();
          localStorage.setItem("activity",'true');
          // console.log("activity",localStorage.getItem("activity"));
          this.sharedData.checkUSerActivity('true');
          // console.log('request',request);
          // request['body'][]



            request = request.clone({
                setHeaders: {
                  Authorization: `Bearer ${this.logedInUserRoles.token}`,
                  'Cache-Control':  'no-cache, no-store, must-revalidate, post-check=0, pre-check=0',
                  'Pragma': 'no-cache',
                  'Expires': '0',
                  'Strict-Transport-Security': 'max-age=7776000; includeSubDomains',
                  'X-Permitted-Cross-Domain-Policies': 'none',
                  'Content-Security-Policy': "default-src 'none'; script-src 'self'; connect-src 'self'; img-src 'self'; style-src 'self'",
                  'X-Frame-Options': 'Deny',
                  'X-Content-Type-Options': 'nosniff',
                  'Referrer-Policy': 'strict-origin-when-cross-origin',
                  'Permissions-Policy': 'geolocation=(), midi=(), notifications=(), push=(), sync-xhr=(), microphone=(), camera=(), magnetometer=(), gyroscope=(), speaker=(), vibrate=(), fullscreen=(), payment=()'

                    // 'Expires': 'Sat, 01 jan 2000 00:00:00 GMT'
                }
            });
            // console.log("request['body']",request);
            if (typeof request['body'] != 'undefined') {
              if (request['body'] != null) {
                if (typeof request['body']['header']!= 'undefined') {
                  request['body']['header']['remoteUserAddress'] = '';
                  request['body']['header']['systemHostAddress'] = '';
                  request['body']['header']['userCode'] = this.logedInUserRoles.userCode;
                }

              }
            }



        }

        return next.handle(request);
    }
}
