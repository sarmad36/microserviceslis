import { Injectable,Injector } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor,HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError,map } from 'rxjs/operators'
import { AuthService } from './auth.service';
import * as CryptoJS from 'crypto-js';


@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

    public logedInUserRoles :any = {};
    constructor(private injector: Injector, private authService: AuthService) {
      if (localStorage.getItem('userRolesInfo') != null) {
        let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
        var originalText = bts.toString(CryptoJS.enc.Utf8);
        this.logedInUserRoles = JSON.parse(originalText)
      }
      else{
        this.logedInUserRoles = null
      }
     }

      intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
          return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
              if (event instanceof HttpResponse) {
                // console.log("event",event);
                if (event.body.statusCode == "401 UNAUTHORIZED") {
                  // alert('token expired please login agian to continue')
                  this.authService.logout("null");

                }



                // let camelCaseObject = mapKeys(event.body, (v, k) => camelCase(k));
                const modEvent = event.clone({ body: event.body });

                return modEvent;
              }
            })
          )
      }

    // intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    //     return next.handle(request).pipe(catchError(err => {
    //         if (err.status === 401) {
    //             // auto logout if 401 response returned from api
    //             this.authService.logout("null");
    //             location.reload(true);
    //         }
    //
    //         const error = err.error.message || err.statusText;
    //         return throwError(error);
    //     }))
    // }
}
