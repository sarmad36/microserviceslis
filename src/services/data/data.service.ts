import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class DataService {

  private messageSource = new BehaviorSubject('null');
  private patientSource = new BehaviorSubject('null');
  private clientCode = new BehaviorSubject('null');
  private activity = new BehaviorSubject('null');
  currentClientId = this.messageSource.asObservable();
  currentPatientId = this.patientSource.asObservable();
  clientCodeForPatient = this.clientCode.asObservable();
  checkActivity = this.activity.asObservable();

  constructor() { }

  changeMessage(message: string) {
    this.messageSource.next(message)
  }

  changePatientId(message: string) {
    this.patientSource.next(message)
  }
  selectedClientCode(message: string) {
    this.patientSource.next(message)
  }
  checkUSerActivity(message: string) {
    // console.log('oooooooooooooooooooooooooooo3oooooooooooooooo',message);

    this.activity.next(message)
  }

}
