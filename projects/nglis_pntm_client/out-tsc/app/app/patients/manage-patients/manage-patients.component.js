import { __decorate } from "tslib";
import { Component, ViewChildren } from '@angular/core';
import { environment } from '../../../environments/environment';
import { DataTableDirective } from 'angular-datatables';
import { Subject, forkJoin } from 'rxjs';
import Swal from 'sweetalert2';
let ManagePatientsComponent = class ManagePatientsComponent {
    constructor(formBuilder, http, route, router,
    // private httpRequest  : HttpRequestsService,
    ngxLoader, sanitizer, notifier, patientnService, globalService) {
        this.formBuilder = formBuilder;
        this.http = http;
        this.route = route;
        this.router = router;
        this.ngxLoader = ngxLoader;
        this.sanitizer = sanitizer;
        this.notifier = notifier;
        this.patientnService = patientnService;
        this.globalService = globalService;
        this.mergeView = false;
        this.dtTrigger = new Subject();
        // dtTrigger2             : Subject<ManagePatientsComponent> = new Subject();
        this.triggerHistoryTable = false;
        this.dtOptions = [];
        this.paginatePage = 0;
        this.paginatePage2 = 0;
        this.paginatePage3 = 0;
        this.paginatePage4 = 0;
        this.paginateLength = 20;
        this.oldCharacterCount = 0;
        this.oldCharacterCount2 = 0;
        this.oldCharacterCount3 = 0;
        this.oldCharacterCount4 = 0;
        this.requestData = {
            header: {
                uuid: "",
                partnerCode: "",
                userCode: "",
                referenceNumber: "",
                systemCode: "",
                moduleCode: "",
                functionalityCode: "",
                systemHostAddress: "",
                remoteUserAddress: "",
                dateTime: ""
            },
            data: {
                username: "danish",
                patientType: "",
                pageNumber: 0,
                pageSize: 20,
                sortColumn: "patientId",
                sortType: "desc"
            }
        };
        this.searchData = {
            header: {
                uuid: "",
                partnerCode: "",
                userCode: "",
                referenceNumber: "",
                systemCode: "",
                moduleCode: "",
                functionalityCode: "",
                systemHostAddress: "",
                remoteUserAddress: "",
                dateTime: ""
            },
            data: {
                username: "danish",
                searchCriteria: "",
                eligibility: null,
                pageNumber: 0,
                pageSize: 20,
                searchType: "",
                sortColumn: "patientId",
                sortType: "desc"
            }
        };
        this.allPatients = [];
        this.eligiblePatients = [];
        this.nonEligiblePatients = [];
        this.searchCreateriaHolderall = '';
        this.searchCreateriaHoldereligible = '';
        this.searchCreateriaHoldernoneligible = '';
        this.exportRequestData = {
            header: {
                uuid: "",
                partnerCode: "",
                userCode: "",
                referenceNumber: "",
                systemCode: "",
                moduleCode: "",
                functionalityCode: "",
                systemHostAddress: "",
                remoteUserAddress: "",
                dateTime: ""
            },
            data: {
                username: "danish",
                searchCriteria: "",
                eligibility: null
            }
        };
        this.fileBase64 = '';
        this.fileName = '';
        this.successCount = null;
        this.failureCount = null;
        this.saveRequest = {
            header: {
                uuid: "",
                partnerCode: "",
                userCode: "",
                referenceNumber: "",
                systemCode: "",
                moduleCode: "SPCM",
                functionalityCode: "",
                systemHostAddress: "",
                remoteUserAddress: "",
                dateTime: ""
            },
            data: {
                caseCategoryPrefix: "MA",
                caseNumber: "",
                caseStatusId: 1,
                caseCategoryId: null,
                clientId: null,
                attendingPhysicianId: "376",
                patientId: null,
                createdTimestamp: "2020-10-22T11:02:03.523+00:00",
                receivingDate: "2020-10-22T07:54:10.485+00:00",
                collectedBy: "",
                collectionDate: null,
                accessionTimestamp: "2020-10-22T07:54:10.485+00:00",
                createdBy: "abc",
                caseSpecimen: [
                    {
                        // caseSpecimenId:1,
                        specimenTypeId: 2,
                        specimenSourceId: 1,
                        bodySiteId: 1,
                        procedureId: 2,
                        createdBy: "abc",
                        createdTimestamp: "2020-10-22T07:54:10.485+00:00",
                        updatedBy: "",
                        updatedTimestamp: ""
                    }
                ]
            }
        };
        this.printedCaseNumber = null;
        this.caseUrl = '';
        this.printedCaseCategory = '';
        this.slectedPatientCases = [];
        this.selectedpatientId = null;
        this.selectedpFname = null;
        this.selectedpLname = null;
        this.selectedpDob = null;
        this.allSpecimenTypes = [];
    }
    ngOnInit() {
        this.getLookups().then(lookupsLoaded => {
        });
        this.dtOptions[0] = {
            pagingType: 'full_numbers',
            pageLength: this.paginateLength,
            serverSide: true,
            processing: true,
            ordering: false,
            "lengthMenu": [20, 50, 75, 100],
            ajax: (dataTablesParameters, callback) => {
                this.dtElement.forEach((dtElement, index) => {
                    dtElement.dtInstance.then((dtInstance) => {
                        if (dtInstance.table().node().id == 'all-patient') {
                            this.paginatePage = dtElement['dt'].page();
                            this.paginateLength = dtElement['dt'].page.len();
                            if (dataTablesParameters.search.value == "" && (this.oldCharacterCount == 0 || this.oldCharacterCount == 3)) {
                                if (this.oldCharacterCount == 3) {
                                }
                                this.searchCreateriaHolderall = dataTablesParameters.search.value;
                                var reqAllPatients = Object.assign({}, this.requestData);
                                reqAllPatients.data.pageNumber = this.paginatePage;
                                reqAllPatients.data.pageSize = this.paginateLength;
                                reqAllPatients.data.patientType = "";
                                // reqAllPatients.data.patientType   = 1;
                                this.oldCharacterCount = 3;
                                // let url    = environment.API_USER_ENDPOINT + 'partnerusers?partnerId=1&pageNumber='+ this.paginatePage +'&pageSize=' + this.paginateLength;
                                let url = environment.API_PATIENT_ENDPOINT + 'allpatients';
                                this.http.put(url, reqAllPatients).toPromise()
                                    .then(resp => {
                                    console.log('resp all Patient: ', resp);
                                    const map = new Map();
                                    var uniqueCLIds = [];
                                    var uniquePIds = [];
                                    for (const item of resp['data']['patients']) {
                                        if (!map.has(item.clientId)) {
                                            map.set(item.clientId, true); // set any value to Map
                                            uniqueCLIds.push(item.clientId);
                                        }
                                        uniquePIds.push(item.patientId);
                                    }
                                    // console.log("Patient",uniquePIds);
                                    var clURL = environment.API_CLIENT_ENDPOINT + "clientsname";
                                    var clReqData = {
                                        header: {
                                            uuid: "",
                                            partnerCode: "",
                                            userCode: "",
                                            referenceNumber: "",
                                            systemCode: "",
                                            moduleCode: "CLIM",
                                            functionalityCode: "",
                                            systemHostAddress: "",
                                            remoteUserAddress: "",
                                            dateTime: ""
                                        },
                                        data: {
                                            clientIds: uniqueCLIds
                                        }
                                    };
                                    // var clReqData       = {clientIds:uniqueCLIds}
                                    this.popClientSpecData(clURL, clReqData, resp, uniquePIds).then(resCS => {
                                        console.log("allPatients", this.allPatients);
                                        callback({
                                            recordsTotal: this.totalPatientsCount,
                                            recordsFiltered: this.totalPatientsCount,
                                            data: []
                                        });
                                    });
                                });
                            }
                            else {
                                if (dataTablesParameters.search.value.length >= 3) {
                                    this.searchCreateriaHolderall = dataTablesParameters.search.value;
                                    var seachAllPatients = Object.assign({}, this.searchData);
                                    seachAllPatients.data.pageNumber = this.paginatePage;
                                    seachAllPatients.data.pageSize = this.paginateLength;
                                    seachAllPatients.data.eligibility = null;
                                    let url = environment.API_PATIENT_ENDPOINT + 'searchpatients';
                                    seachAllPatients.data.searchCriteria = dataTablesParameters.search.value;
                                    this.http.put(url, seachAllPatients).toPromise()
                                        .then(resp => {
                                        console.log('resp Search All Patient: ', resp);
                                        const map = new Map();
                                        var uniqueCLIds = [];
                                        for (const item of resp['data']['patients']) {
                                            if (!map.has(item.clientId)) {
                                                map.set(item.clientId, true); // set any value to Map
                                                uniqueCLIds.push(item.clientId);
                                            }
                                        }
                                        // console.log("uniqueCLIds",uniqueCLIds);
                                        var clURL = environment.API_CLIENT_ENDPOINT + "clientsname";
                                        var clReqData = {
                                            header: {
                                                uuid: "",
                                                partnerCode: "",
                                                userCode: "",
                                                referenceNumber: "",
                                                systemCode: "",
                                                moduleCode: "CLIM",
                                                functionalityCode: "",
                                                systemHostAddress: "",
                                                remoteUserAddress: "",
                                                dateTime: ""
                                            },
                                            data: {
                                                clientIds: uniqueCLIds
                                            }
                                        };
                                        // var clReqData       = {clientIds:uniqueCLIds}
                                        this.patientnService.getClientByIds(clURL, clReqData).then(selectedClient => {
                                            // console.log("this.selectedClient",selectedClient);
                                            for (let i = 0; i < resp['data']['patients'].length; i++) {
                                                for (let j = 0; j < selectedClient['data']['length']; j++) {
                                                    if (resp['data']['patients'][i].clientId == selectedClient['data'][j].clientId) {
                                                        resp['data']['patients'][i].clientName = selectedClient['data'][j].name;
                                                    }
                                                }
                                            }
                                            this.allPatients = resp['data']['patients'];
                                            this.totalPatientsCount = resp['data']['totalPatients'];
                                            callback({
                                                recordsTotal: this.totalPatientsCount,
                                                recordsFiltered: this.totalPatientsCount,
                                                data: []
                                            });
                                        });
                                    })
                                        .catch(error => {
                                        // console.log("error: ",error);
                                        this.ngxLoader.stop();
                                        this.notifier.notify("error", "Error while loading all patiens");
                                    });
                                    this.oldCharacterCount = 3;
                                }
                                else {
                                    $('.dataTables_processing').css('display', "none");
                                    if (this.oldCharacterCount == 3) {
                                        this.oldCharacterCount = 3;
                                    }
                                    else {
                                        this.oldCharacterCount = 2;
                                    }
                                }
                            }
                        }
                        // console.log(`The DataTable ${index} instance ID is: ${dtInstance.table().node().id}`);
                    });
                });
            },
        };
        //////////////////////////// Eligible Patients
        this.dtOptions[1] = {
            pagingType: 'full_numbers',
            pageLength: this.paginateLength,
            serverSide: true,
            processing: true,
            ordering: false,
            responsive: true,
            "lengthMenu": [20, 50, 75, 100],
            ajax: (dataTablesParameters, callback) => {
                this.dtElement.forEach((dtElement, index) => {
                    dtElement.dtInstance.then((dtInstance) => {
                        if (dtInstance.table().node().id == 'patient-eligible') {
                            // console.log("External",dtElement['dt']);
                            // console.log("External page",dtElement['dt'].page());
                            // console.log("External page len",dtElement['dt'].page.len());
                            this.paginatePage2 = dtElement['dt'].page();
                            this.paginateLength = dtElement['dt'].page.len();
                            if (dataTablesParameters.search.value == "" && (this.oldCharacterCount2 == 0 || this.oldCharacterCount2 == 3)) {
                                this.searchCreateriaHoldereligible = dataTablesParameters.search.value;
                                var reqEligiblePatients = Object.assign({}, this.requestData);
                                reqEligiblePatients.data.pageNumber = dtElement['dt'].page();
                                reqEligiblePatients.data.pageSize = this.paginateLength;
                                reqEligiblePatients.data.patientType = "eligible";
                                // reqEligiblePatients.data.userType   = 2;
                                // let url    = environment.API_USER_ENDPOINT + 'partnerusers?partnerId=1&pageNumber='+ this.paginatePage +'&pageSize=' + this.paginateLength;
                                let url = environment.API_PATIENT_ENDPOINT + 'allpatients';
                                // dataTablesParameters.pageNumber = this.paginatePage;
                                this.http.put(url, reqEligiblePatients).toPromise()
                                    .then(resp => {
                                    console.log('resp ELigible: ', resp);
                                    if (resp['data']['patients'] != null) {
                                        const map = new Map();
                                        var uniqueCLIds = [];
                                        for (const item of resp['data']['patients']) {
                                            if (!map.has(item.clientId)) {
                                                map.set(item.clientId, true); // set any value to Map
                                                uniqueCLIds.push(item.clientId);
                                            }
                                        }
                                        // console.log("uniqueCLIds",uniqueCLIds);
                                        var clURL = environment.API_CLIENT_ENDPOINT + "clientsname";
                                        var clReqData = {
                                            header: {
                                                uuid: "",
                                                partnerCode: "",
                                                userCode: "",
                                                referenceNumber: "",
                                                systemCode: "",
                                                moduleCode: "CLIM",
                                                functionalityCode: "",
                                                systemHostAddress: "",
                                                remoteUserAddress: "",
                                                dateTime: ""
                                            },
                                            data: {
                                                clientIds: uniqueCLIds
                                            }
                                        };
                                        // var clReqData       = {clientIds:uniqueCLIds}
                                        this.patientnService.getClientByIds(clURL, clReqData).then(selectedClient => {
                                            // console.log("this.selectedClient",selectedClient);
                                            for (let i = 0; i < resp['data']['patients'].length; i++) {
                                                for (let j = 0; j < selectedClient['data']['length']; j++) {
                                                    if (resp['data']['patients'][i].clientId == selectedClient['data'][j].clientId) {
                                                        resp['data']['patients'][i].clientName = selectedClient['data'][j].name;
                                                    }
                                                }
                                            }
                                            this.eligiblePatients = resp['data']['patients'];
                                            this.totalPatientsCoun2 = resp['data']['totalPatients'];
                                            callback({
                                                recordsTotal: this.totalPatientsCoun2,
                                                recordsFiltered: this.totalPatientsCoun2,
                                                data: []
                                            });
                                        });
                                    }
                                    else {
                                        this.eligiblePatients = resp['data']['patients'];
                                        this.totalPatientsCoun2 = resp['data']['totalPatients'];
                                        callback({
                                            recordsTotal: this.totalPatientsCoun2,
                                            recordsFiltered: this.totalPatientsCoun2,
                                            data: []
                                        });
                                    }
                                })
                                    .catch(error => {
                                    // Set loader false
                                    // this.loading = false;
                                    this.ngxLoader.stop();
                                    this.notifier.notify("error", "Error while loading eligible patiens");
                                });
                                this.oldCharacterCount2 = 3;
                            }
                            else {
                                if (dataTablesParameters.search.value.length >= 3) {
                                    this.searchCreateriaHoldereligible = dataTablesParameters.search.value;
                                    var searchEligiblePatients = Object.assign({}, this.searchData);
                                    searchEligiblePatients.data.pageNumber = this.paginatePage2;
                                    searchEligiblePatients.data.pageSize = this.paginateLength;
                                    searchEligiblePatients.data.eligibility = 1;
                                    searchEligiblePatients.data.searchCriteria = dataTablesParameters.search.value;
                                    // let url    = environment.API_USER_ENDPOINT + 'partnerusers?partnerId=1&pageNumber='+ this.paginatePage +'&pageSize=' + this.paginateLength;
                                    let url = environment.API_PATIENT_ENDPOINT + 'searchpatients';
                                    // console.log('this.paginatePage: ',this.paginatePage);
                                    // dataTablesParameters.pageNumber = this.paginatePage;
                                    this.http.put(url, searchEligiblePatients).toPromise()
                                        .then(resp => {
                                        console.log('resp Eligible Patient Search: ', resp);
                                        if (resp['data']['patients'] != null) {
                                            const map = new Map();
                                            var uniqueCLIds = [];
                                            for (const item of resp['data']['patients']) {
                                                if (!map.has(item.clientId)) {
                                                    map.set(item.clientId, true); // set any value to Map
                                                    uniqueCLIds.push(item.clientId);
                                                }
                                            }
                                            // console.log("uniqueCLIds",uniqueCLIds);
                                            var clURL = environment.API_CLIENT_ENDPOINT + "clientsname";
                                            var clReqData = {
                                                header: {
                                                    uuid: "",
                                                    partnerCode: "",
                                                    userCode: "",
                                                    referenceNumber: "",
                                                    systemCode: "",
                                                    moduleCode: "CLIM",
                                                    functionalityCode: "",
                                                    systemHostAddress: "",
                                                    remoteUserAddress: "",
                                                    dateTime: ""
                                                },
                                                data: {
                                                    clientIds: uniqueCLIds
                                                }
                                            };
                                            // var clReqData       = {clientIds:uniqueCLIds}
                                            this.patientnService.getClientByIds(clURL, clReqData).then(selectedClient => {
                                                // console.log("this.selectedClient",selectedClient);
                                                for (let i = 0; i < resp['data']['patients'].length; i++) {
                                                    for (let j = 0; j < selectedClient['data']['length']; j++) {
                                                        if (resp['data']['patients'][i].clientId == selectedClient['data'][j].clientId) {
                                                            resp['data']['patients'][i].clientName = selectedClient['data'][j].name;
                                                        }
                                                    }
                                                }
                                                this.eligiblePatients = resp['data']['patients'];
                                                this.totalPatientsCoun2 = resp['data']['totalPatients'];
                                                callback({
                                                    recordsTotal: this.totalPatientsCoun2,
                                                    recordsFiltered: this.totalPatientsCoun2,
                                                    data: []
                                                });
                                            });
                                        }
                                        else {
                                            this.eligiblePatients = resp['data']['patients'];
                                            this.totalPatientsCoun2 = resp['data']['totalPatients'];
                                            callback({
                                                recordsTotal: this.totalPatientsCoun2,
                                                recordsFiltered: this.totalPatientsCoun2,
                                                data: []
                                            });
                                        }
                                    })
                                        .catch(error => {
                                        // Set loader false
                                        // this.loading = false;
                                        this.ngxLoader.stop();
                                        this.notifier.notify("error", "Error while loading eligible patiens");
                                    });
                                }
                                else {
                                    $('.dataTables_processing').css('display', "none");
                                    if (this.oldCharacterCount2 == 3) {
                                        this.oldCharacterCount2 = 3;
                                    }
                                    else {
                                        this.oldCharacterCount2 = 2;
                                    }
                                }
                            }
                        }
                        // console.log(`The DataTable ${index} instance ID is: ${dtInstance.table().node().id}`);
                    });
                });
            },
            columns: [
                { data: 'SPD-002' },
                { data: 'firstName' },
                { data: 'lastName' },
                { data: 'username' },
                { data: 'phone' },
                { data: 'email' },
                { data: 'action' }
            ],
        };
        //////////////////////////// Non Eligible Patients
        this.dtOptions[2] = {
            pagingType: 'full_numbers',
            pageLength: this.paginateLength,
            serverSide: true,
            processing: true,
            ordering: false,
            responsive: true,
            "lengthMenu": [20, 50, 75, 100],
            ajax: (dataTablesParameters, callback) => {
                this.dtElement.forEach((dtElement, index) => {
                    dtElement.dtInstance.then((dtInstance) => {
                        if (dtInstance.table().node().id == 'patient-noneligible') {
                            this.paginatePage3 = dtElement['dt'].page();
                            this.paginateLength = dtElement['dt'].page.len();
                            if (dataTablesParameters.search.value == "" && (this.oldCharacterCount3 == 0 || this.oldCharacterCount3 == 3)) {
                                this.searchCreateriaHoldernoneligible = dataTablesParameters.search.value;
                                var reqEligiblePatients = Object.assign({}, this.requestData);
                                reqEligiblePatients.data.pageNumber = dtElement['dt'].page();
                                reqEligiblePatients.data.pageSize = this.paginateLength;
                                reqEligiblePatients.data.patientType = "ineligible";
                                // reqEligiblePatients.data.userType   = 2;
                                // let url    = environment.API_USER_ENDPOINT + 'partnerusers?partnerId=1&pageNumber='+ this.paginatePage +'&pageSize=' + this.paginateLength;
                                let url = environment.API_PATIENT_ENDPOINT + 'allpatients';
                                // dataTablesParameters.pageNumber = this.paginatePage;
                                this.http.put(url, reqEligiblePatients).toPromise()
                                    .then(resp => {
                                    console.log('**** resp ELigible: ', resp);
                                    if (resp['data']['patients'] != null) {
                                        const map = new Map();
                                        var uniqueCLIds = [];
                                        for (const item of resp['data']['patients']) {
                                            if (!map.has(item.clientId)) {
                                                map.set(item.clientId, true); // set any value to Map
                                                uniqueCLIds.push(item.clientId);
                                            }
                                        }
                                        // console.log("uniqueCLIds",uniqueCLIds);
                                        var clURL = environment.API_CLIENT_ENDPOINT + "clientsname";
                                        var clReqData = {
                                            header: {
                                                uuid: "",
                                                partnerCode: "",
                                                userCode: "",
                                                referenceNumber: "",
                                                systemCode: "",
                                                moduleCode: "CLIM",
                                                functionalityCode: "",
                                                systemHostAddress: "",
                                                remoteUserAddress: "",
                                                dateTime: ""
                                            },
                                            data: {
                                                clientIds: uniqueCLIds
                                            }
                                        };
                                        // var clReqData       = {clientIds:uniqueCLIds}
                                        this.patientnService.getClientByIds(clURL, clReqData).then(selectedClient => {
                                            // console.log("this.selectedClient",selectedClient);
                                            for (let i = 0; i < resp['data']['patients'].length; i++) {
                                                for (let j = 0; j < selectedClient['data']['length']; j++) {
                                                    if (resp['data']['patients'][i].clientId == selectedClient['data'][j].clientId) {
                                                        resp['data']['patients'][i].clientName = selectedClient['data'][j].name;
                                                    }
                                                }
                                            }
                                            this.nonEligiblePatients = resp['data']['patients'];
                                            this.totalPatientsCoun3 = resp['data']['totalPatients'];
                                            callback({
                                                recordsTotal: this.totalPatientsCoun3,
                                                recordsFiltered: this.totalPatientsCoun3,
                                                data: []
                                            });
                                        });
                                    }
                                    else {
                                        this.nonEligiblePatients = resp['data']['patients'];
                                        this.totalPatientsCoun3 = resp['data']['totalPatients'];
                                        callback({
                                            recordsTotal: this.totalPatientsCoun3,
                                            recordsFiltered: this.totalPatientsCoun3,
                                            data: []
                                        });
                                    }
                                })
                                    .catch(error => {
                                    // Set loader false
                                    // this.loading = false;
                                    this.ngxLoader.stop();
                                    this.notifier.notify("error", "Error while loading non-eligible patiens");
                                });
                                this.oldCharacterCount3 = 3;
                            }
                            else {
                                if (dataTablesParameters.search.value.length >= 3) {
                                    this.searchCreateriaHoldernoneligible = dataTablesParameters.search.value;
                                    var searchEligiblePatients = Object.assign({}, this.searchData);
                                    searchEligiblePatients.data.pageNumber = this.paginatePage2;
                                    searchEligiblePatients.data.pageSize = this.paginateLength;
                                    searchEligiblePatients.data.eligibility = 0;
                                    searchEligiblePatients.data.searchCriteria = dataTablesParameters.search.value;
                                    // let url    = environment.API_USER_ENDPOINT + 'partnerusers?partnerId=1&pageNumber='+ this.paginatePage +'&pageSize=' + this.paginateLength;
                                    let url = environment.API_PATIENT_ENDPOINT + 'searchpatients';
                                    // console.log('this.paginatePage: ',this.paginatePage);
                                    // dataTablesParameters.pageNumber = this.paginatePage;
                                    this.http.put(url, searchEligiblePatients).toPromise()
                                        .then(resp => {
                                        console.log('resp Non Eligibel patients Search: ', resp);
                                        if (resp['data']['patients'] != null) {
                                            const map = new Map();
                                            var uniqueCLIds = [];
                                            for (const item of resp['data']['patients']) {
                                                if (!map.has(item.clientId)) {
                                                    map.set(item.clientId, true); // set any value to Map
                                                    uniqueCLIds.push(item.clientId);
                                                }
                                            }
                                            // console.log("uniqueCLIds",uniqueCLIds);
                                            var clURL = environment.API_CLIENT_ENDPOINT + "clientsname";
                                            var clReqData = {
                                                header: {
                                                    uuid: "",
                                                    partnerCode: "",
                                                    userCode: "",
                                                    referenceNumber: "",
                                                    systemCode: "",
                                                    moduleCode: "CLIM",
                                                    functionalityCode: "",
                                                    systemHostAddress: "",
                                                    remoteUserAddress: "",
                                                    dateTime: ""
                                                },
                                                data: {
                                                    clientIds: uniqueCLIds
                                                }
                                            };
                                            // var clReqData       = {clientIds:uniqueCLIds}
                                            this.patientnService.getClientByIds(clURL, clReqData).then(selectedClient => {
                                                // console.log("this.selectedClient",selectedClient);
                                                for (let i = 0; i < resp['data']['patients'].length; i++) {
                                                    for (let j = 0; j < selectedClient['data']['length']; j++) {
                                                        if (resp['data']['patients'][i].clientId == selectedClient['data'][j].clientId) {
                                                            resp['data']['patients'][i].clientName = selectedClient['data'][j].name;
                                                        }
                                                    }
                                                }
                                                this.nonEligiblePatients = resp['data']['patients'];
                                                this.totalPatientsCoun3 = resp['data']['totalPatients'];
                                                callback({
                                                    recordsTotal: this.totalPatientsCoun3,
                                                    recordsFiltered: this.totalPatientsCoun3,
                                                    data: []
                                                });
                                            });
                                        }
                                        else {
                                            this.nonEligiblePatients = resp['data']['patients'];
                                            this.totalPatientsCoun3 = resp['data']['totalPatients'];
                                            callback({
                                                recordsTotal: this.totalPatientsCoun3,
                                                recordsFiltered: this.totalPatientsCoun3,
                                                data: []
                                            });
                                        }
                                    })
                                        .catch(error => {
                                        // Set loader false
                                        // this.loading = false;
                                        this.ngxLoader.stop();
                                        this.notifier.notify("error", "Error while loading non-eligible patiens");
                                    });
                                }
                                else {
                                    $('.dataTables_processing').css('display', "none");
                                    if (this.oldCharacterCount3 == 3) {
                                        this.oldCharacterCount3 = 3;
                                    }
                                    else {
                                        this.oldCharacterCount3 = 2;
                                    }
                                }
                            }
                        }
                        // console.log(`The DataTable ${index} instance ID is: ${dtInstance.table().node().id}`);
                    });
                });
            },
            columns: [
                { data: 'SPD-002' },
                { data: 'firstName' },
                { data: 'lastName' },
                { data: 'username' },
                { data: 'phone' },
                { data: 'email' },
                { data: 'action' }
            ],
        };
        this.dtOptions[3] = {
            pagingType: 'full_numbers',
            pageLength: this.paginateLength,
            serverSide: true,
            processing: true,
            ordering: false,
            "lengthMenu": [20, 50, 75, 100],
            ajax: (dataTablesParameters, callback) => {
                if (this.triggerHistoryTable == true) {
                    this.dtElement.forEach((dtElement, index) => {
                        dtElement.dtInstance.then((dtInstance) => {
                            if (dtInstance.table().node().id == 'spec-col-history') {
                                this.paginatePage4 = dtElement['dt'].page();
                                this.paginateLength = dtElement['dt'].page.len();
                                if (dataTablesParameters.search.value == "" && (this.oldCharacterCount4 == 0 || this.oldCharacterCount4 == 3)) {
                                    if (this.oldCharacterCount4 == 3) {
                                    }
                                    var reqURL = environment.API_SPECIMEN_ENDPOINT + 'findcasesbypatientid';
                                    var reqData = { header: { uuid: "", partnerCode: "", userCode: "", referenceNumber: "", systemCode: "", moduleCode: "SPCM", functionalityCode: "", systemHostAddress: "", remoteUserAddress: "", dateTime: "" }, data: { page: this.paginatePage4, size: "20", sortColumn: "caseNumber", sortingOrder: "asc", patientIds: [this.selectedpatientId] } };
                                    this.patientnService.caseByPatientId(reqURL, reqData).subscribe(respCases => {
                                        console.log("respCases", respCases);
                                        var uniqueClientId = [];
                                        const map = new Map();
                                        for (const item of respCases['data']['caseDetails']) {
                                            if (!map.has(item.clientId)) {
                                                map.set(item.clientId, true); // set any value to Map
                                                uniqueClientId.push(item.clientId);
                                            }
                                        }
                                        var clURL = environment.API_CLIENT_ENDPOINT + "clientsname";
                                        var clReqData = {
                                            header: {
                                                uuid: "",
                                                partnerCode: "",
                                                userCode: "",
                                                referenceNumber: "",
                                                systemCode: "",
                                                moduleCode: "CLIM",
                                                functionalityCode: "",
                                                systemHostAddress: "",
                                                remoteUserAddress: "",
                                                dateTime: ""
                                            },
                                            data: {
                                                clientIds: uniqueClientId
                                            }
                                        };
                                        // var clReqData       = {clientIds:uniqueClientId}
                                        this.patientnService.getPatientsByIds(clURL, clReqData).then(selectedClient => {
                                            console.log("selectedClient", selectedClient);
                                            for (let i = 0; i < respCases['data']['caseDetails'].length; i++) {
                                                for (let j = 0; j < selectedClient['data']['length']; j++) {
                                                    if (respCases['data']['caseDetails'][i].clientId == selectedClient['data'][j].clientId) {
                                                        respCases['data']['caseDetails'][i].clientName = selectedClient['data'][j].name;
                                                    }
                                                }
                                                if (respCases['data']['caseDetails'][i].caseStatusId == 1 || respCases['data']['caseDetails'][i].caseNumber == null || respCases['data']['caseDetails'][i].caseNumber == '') {
                                                  var a = respCases['data']['caseDetails'][i].caseId+';2';
                                                  var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
                                                  var bx = ax.replace('+','xsarm');
                                                  var cx = bx.replace('/','ydan');
                                                  var ciphertext = cx.replace('=','zhar');
                                                  console.log('ciphertext',ciphertext);
                                                  var ur = location.origin+'/edit-case/'+ciphertext+'/1';
                                                  // respCases['data']['caseDetails'][i].caseUrl = environment.UI_SPECIMEN_ENDPOINT + "/edit-case/" + respCases['data']['caseDetails'][i].caseId + "/1;from=2";
                                                    respCases['data']['caseDetails'][i].caseUrl = ur;
                                                }
                                                else {
                                                  var a = respCases['data']['caseDetails'][i].caseNumber+';0';
                                                  var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
                                                  var bx = ax.replace('+','xsarm');
                                                  var cx = bx.replace('/','ydan');
                                                  var ciphertext = cx.replace('=','zhar');
                                                  console.log('ciphertext',ciphertext);
                                                  var ur = location.origin+'/edit-case/'+ciphertext+'/1';
                                                  respCases['data']['caseDetails'][i].caseUrl = ur;
                                                    // respCases['data']['caseDetails'][i].caseUrl = environment.UI_SPECIMEN_ENDPOINT + "/edit-case/" + respCases['data']['caseDetails'][i].caseNumber+'/1';
                                                }
                                                respCases['data']['caseDetails'][i].pFname = this.selectedpFname;
                                                respCases['data']['caseDetails'][i].pLname = this.selectedpLname;
                                                respCases['data']['caseDetails'][i].pDob = this.selectedpDob;
                                                for (let sp = 0; sp < this.allSpecimenTypes.length; sp++) {
                                                    if (this.allSpecimenTypes[sp].specimenTypeId == respCases['data']['caseDetails'][i]['caseSpecimen'][0].specimenTypeId) {
                                                        respCases['data']['caseDetails'][i]['caseSpecimen'][0].specimenPrefix = this.allSpecimenTypes[sp].specimenTypePrefix;
                                                    }
                                                }
                                            }
                                            this.slectedPatientCases = respCases['data']['caseDetails'];
                                            // $('#spec-collectionhistory-Modal').modal('show');
                                            this.ngxLoader.stop();
                                            this.totalSpecimenCount = respCases['data']['totalCount'];
                                            callback({
                                                recordsTotal: this.totalSpecimenCount,
                                                recordsFiltered: this.totalSpecimenCount,
                                                data: []
                                            });
                                        });
                                    });
                                    // reqAllPatients.data.patientType   = 1;
                                    this.oldCharacterCount4 = 3;
                                }
                                else {
                                    if (dataTablesParameters.search.value.length >= 3) {
                                        var searchUrl = environment.API_SPECIMEN_ENDPOINT + 'searchcases';
                                        var searchReq = { header: { uuid: "", partnerCode: "", userCode: "", referenceNumber: "", systemCode: "", moduleCode: "SPCM", functionalityCode: "", systemHostAddress: "", remoteUserAddress: "", dateTime: "" }, data: { page: "0", size: "20", sortColumn: "caseNumber", sortingOrder: "desc", searchDate: "20", caseNumber: dataTablesParameters.search.value, caseStatus: 0 } };
                                        this.patientnService.caseByPatientId(searchUrl, searchReq).subscribe(respCases => {
                                            console.log("search cases response", respCases);
                                            var uniqueClientId = [];
                                            const map = new Map();
                                            for (const item of respCases['data']['caseDetails']) {
                                                if (!map.has(item.clientId)) {
                                                    map.set(item.clientId, true); // set any value to Map
                                                    uniqueClientId.push(item.clientId);
                                                }
                                            }
                                            var clURL = environment.API_CLIENT_ENDPOINT + "clientsname";
                                            var clReqData = {
                                                header: {
                                                    uuid: "",
                                                    partnerCode: "",
                                                    userCode: "",
                                                    referenceNumber: "",
                                                    systemCode: "",
                                                    moduleCode: "CLIM",
                                                    functionalityCode: "",
                                                    systemHostAddress: "",
                                                    remoteUserAddress: "",
                                                    dateTime: ""
                                                },
                                                data: {
                                                    clientIds: uniqueClientId
                                                }
                                            };
                                            // var clReqData       = {clientIds:uniqueClientId}
                                            this.patientnService.getPatientsByIds(clURL, clReqData).then(selectedClient => {
                                                console.log("selectedClient", selectedClient);
                                                for (let i = 0; i < respCases['data']['caseDetails'].length; i++) {
                                                    for (let j = 0; j < selectedClient['data']['length']; j++) {
                                                        if (respCases['data']['caseDetails'][i].clientId == selectedClient['data'][j].clientId) {
                                                            respCases['data']['caseDetails'][i].clientName = selectedClient['data'][j].name;
                                                        }
                                                    }
                                                    if (respCases['data']['caseDetails'][i].caseStatusId == 1 || respCases['data']['caseDetails'][i].caseNumber == null || respCases['data']['caseDetails'][i].caseNumber == '') {
                                                      var a = respCases['data']['caseDetails'][i].caseId+';2';
                                                      var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
                                                      var bx = ax.replace('+','xsarm');
                                                      var cx = bx.replace('/','ydan');
                                                      var ciphertext = cx.replace('=','zhar');
                                                      console.log('ciphertext',ciphertext);
                                                      var ur = location.origin+'/edit-case/'+ciphertext+'/1';
                                                      respCases['data']['caseDetails'][i].caseUrl = ur;
                                                        // respCases['data']['caseDetails'][i].caseUrl = environment.UI_SPECIMEN_ENDPOINT + "/edit-case/" + respCases['data']['caseDetails'][i].caseId + "/1;from=2";
                                                    }
                                                    else {
                                                      var a = respCases['data']['caseDetails'][i].caseNumber+';0';
                                                      var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
                                                      var bx = ax.replace('+','xsarm');
                                                      var cx = bx.replace('/','ydan');
                                                      var ciphertext = cx.replace('=','zhar');
                                                      console.log('ciphertext',ciphertext);
                                                      var ur = location.origin+'/edit-case/'+ciphertext+'/1';
                                                      respCases['data']['caseDetails'][i].caseUrl = ur;
                                                        // respCases['data']['caseDetails'][i].caseUrl = environment.UI_SPECIMEN_ENDPOINT + "/edit-case/" + respCases['data']['caseDetails'][i].caseNumber+'/1';
                                                    }
                                                    respCases['data']['caseDetails'][i].pFname = this.selectedpFname;
                                                    respCases['data']['caseDetails'][i].pLname = this.selectedpLname;
                                                    respCases['data']['caseDetails'][i].pDob = this.selectedpDob;
                                                }
                                                this.slectedPatientCases = respCases['data']['caseDetails'];
                                                // $('#spec-collectionhistory-Modal').modal('show');
                                                this.ngxLoader.stop();
                                                this.totalSpecimenCount = respCases['data']['totalCount'];
                                                callback({
                                                    recordsTotal: this.totalSpecimenCount,
                                                    recordsFiltered: this.totalSpecimenCount,
                                                    data: []
                                                });
                                            });
                                        });
                                        ///////////// goes here
                                        // this.oldCharacterCount4    = 3;
                                    }
                                    else {
                                        $('.dataTables_processing').css('display', "none");
                                        if (this.oldCharacterCount4 == 3) {
                                            this.oldCharacterCount4 = 3;
                                        }
                                        else {
                                            this.oldCharacterCount4 = 2;
                                        }
                                    }
                                }
                            }
                            // console.log(`The DataTable ${index} instance ID is: ${dtInstance.table().node().id}`);
                        });
                    });
                }
            },
        };
    }
    ngAfterViewInit() {
        this.dtTrigger.next();
    }
    ngOnDestroy() {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
    }
    popClientSpecData(clURL, clReqData, resp, uniquePIds) {
        return new Promise((resolve, reject) => {
            var byPidURL = environment.API_SPECIMEN_ENDPOINT + 'findbypatientid';
            var reqPid = { header: { uuid: "", partnerCode: "", userCode: "", referenceNumber: "", systemCode: "", moduleCode: "SPCM", functionalityCode: "", systemHostAddress: "", remoteUserAddress: "", dateTime: "" }, data: { patientIds: uniquePIds } };
            var patientout = this.patientnService.getPatientsByIds(byPidURL, reqPid).then(patient => {
                return patient;
            });
            var clientsout = this.patientnService.getClientByIds(clURL, clReqData).then(respClient => {
                return respClient['data'];
            });
            forkJoin([clientsout, patientout]).subscribe(bothresults => {
                var selectedClient = bothresults[0];
                var selectedPatient = bothresults[1];
                for (let i = 0; i < resp['data']['patients'].length; i++) {
                    for (let j = 0; j < selectedClient['length']; j++) {
                        if (resp['data']['patients'][i].clientId == selectedClient[j].clientId) {
                            resp['data']['patients'][i].clientName = selectedClient[j].name;
                        }
                    }
                }
                for (let i = 0; i < resp['data']['patients'].length; i++) {
                    for (let j = 0; j < selectedPatient['data']['length']; j++) {
                        if (resp['data']['patients'][i].patientId == selectedPatient['data'][j].patientId) {
                            // console.log("selectedPatient['data'][j]",selectedPatient['data'][j]);
                            if (selectedPatient['data'][j]['caseSpecimen'].length > 0) {
                                var cbyname = "";
                                var specPre = "";
                                if (selectedPatient['data'][j].collectedBy == 1) {
                                    cbyname = "Sip";
                                }
                                else if (selectedPatient['data'][j].collectedBy == 2) {
                                    cbyname = "Client";
                                }
                                for (let sp = 0; sp < this.allSpecimenTypes.length; sp++) {
                                    if (this.allSpecimenTypes[sp].specimenTypeId == selectedPatient['data'][j]['caseSpecimen'][0].specimenTypeId) {
                                        specPre = this.allSpecimenTypes[sp].specimenTypePrefix;
                                    }
                                }
                                resp['data']['patients'][i].collectedBy = selectedPatient['data'][j].collectedBy;
                                resp['data']['patients'][i].collectionDate = selectedPatient['data'][j].collectionDate;
                                resp['data']['patients'][i].lastCollected = specPre + "Collected By" + cbyname + ' AT ' + selectedPatient['data'][j].collectionDate;
                            }
                            else {
                                resp['data']['patients'][i].lastCollected = "No Case Created";
                            }
                            resp['data']['patients'][i].caseId = selectedPatient['data'][j].caseId;
                        }
                    }
                }
                this.allPatients = resp['data']['patients'];
                this.totalPatientsCount = resp['data']['totalPatients'];
                resolve();
            });
            //   this.patientnService.getClientByIds(clURL,clReqData).then(selectedClient => {
            //     // console.log("this.selectedClient",selectedClient);
            //
            //
            //   })
            // .catch(error => {
            //
            //   console.log("error: ",error);
            //   this.notifier.notify( "error", "Error while loading all patiens");
            //   resolve();
            // });
        });
    }
    deactivatePatient() {
        console.log("patientToDisable", this.patientToDisable);
        var requestTemplate = { header: { uuid: "", partnerCode: "", userCode: "", referenceNumber: "", systemCode: "", moduleCode: "", functionalityCode: "", systemHostAddress: "", remoteUserAddress: "", dateTime: "" }, data: { username: "danish", patientId: this.patientToDisable, status: true } };
        var url = environment.API_PATIENT_ENDPOINT + 'changestatus';
        this.patientnService.togglePatientStatus(url, requestTemplate).subscribe(resp => {
            $('#deactivateModal').modal('hide');
            if (resp['result']['codeType'] == 'S') {
                this.notifier.notify("success", "Patient deactivated successfully");
            }
            else {
                this.notifier.notify("error", resp['result']['description']);
            }
        }, error => {
            this.notifier.notify("error", "Error while changing status check DB connection");
        });
    }
    exportPatient(from) {
        // console.log("from", from);
        var url = environment.API_PATIENT_ENDPOINT + 'exportpatients';
        if (from == 'all') {
            this.ngxLoader.start();
            var reqDataBP = Object.assign({}, this.exportRequestData);
            reqDataBP.data.searchCriteria = this.searchCreateriaHolderall;
            reqDataBP.data.eligibility = "";
            // console.log("searchCreateria",this.searchCreateriaHolderall);
            this.patientnService.exportPatients(url, reqDataBP).subscribe(allresp => {
                console.log("export all resp", allresp);
                this.ngxLoader.stop();
                if (allresp['result'].codeType == "S") {
                    window.open(allresp['data']['exportedPatients'], "_blank");
                }
                else {
                    this.notifier.notify("error", allresp['result'].description);
                }
            }, error => {
                this.notifier.notify("error", "Error while exporting Patients check DB connection");
            });
        }
        else if (from == 'el') {
            this.ngxLoader.start();
            var reqDataBP = Object.assign({}, this.exportRequestData);
            reqDataBP.data.searchCriteria = this.searchCreateriaHoldereligible;
            reqDataBP.data.eligibility = 1;
            this.patientnService.exportPatients(url, reqDataBP).subscribe(allresp => {
                console.log("export eligible resp", allresp);
                this.ngxLoader.stop();
                if (allresp['result'].codeType == "S") {
                    window.open(allresp['data']['exportedPatients'], "_blank");
                }
                else {
                    this.notifier.notify("error", allresp['result'].description);
                }
            }, error => {
                this.notifier.notify("error", "Error while exporting Patients check DB connection");
            });
        }
        else if (from == 'non') {
            this.ngxLoader.start();
            var reqDataBP = Object.assign({}, this.exportRequestData);
            reqDataBP.data.searchCriteria = this.searchCreateriaHoldernoneligible;
            reqDataBP.data.eligibility = 0;
            this.patientnService.exportPatients(url, reqDataBP).subscribe(allresp => {
                console.log("export non eligible resp", allresp);
                this.ngxLoader.stop();
                if (allresp['result'].codeType == "S") {
                    window.open(allresp['data']['exportedPatients'], "_blank");
                }
                else {
                    this.notifier.notify("error", allresp['result'].description);
                }
            }, error => {
                this.notifier.notify("error", "Error while exporting Patients check DB connection");
            });
        }
    }
    onFileSelect(input) {
        console.log('size', input.files[0]);
        this.fileName = input.files[0].name;
        var mb = input.files[0].size / 1048576;
        console.log("mb", mb);
        if (mb <= 1) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = (e) => {
                    console.log('Got here: ', e.target.result);
                    // console.log('input.files[0]: ', input.files[0]);
                    this.fileBase64 = e.target.result;
                    // this.obj.photoUrl = e.target.result;
                };
                reader.readAsDataURL(input.files[0]);
            }
            // console.log('this.imageBase64: ', this.imageBase64);
            if (input.files && input.files.length > 0) {
                this.filePayLoad = input.files[0];
            }
        }
        else {
            alert('File Size must be less then 1MB');
        }
    }
    importPatient() {
        var url = environment.API_PATIENT_ENDPOINT + 'importpatients';
        if (this.fileBase64 == "" || typeof this.fileBase64 != "undefined") {
            this.ngxLoader.start();
            var reqDataBP = { header: { uuid: "", partnerCode: "", userCode: "", referenceNumber: "", systemCode: "", moduleCode: "", functionalityCode: "", systemHostAddress: "", remoteUserAddress: "", dateTime: "" }, data: { userName: "danish", fileName: this.fileName, importedFile: this.fileBase64 } };
            console.log("this.fileBase64", this.fileBase64);
            this.patientnService.importPatients(url, reqDataBP).subscribe(allresp => {
                console.log("import resp", allresp);
                this.ngxLoader.stop();
                if (allresp['result'].codeType == "S") {
                    $('#importModal').modal('hide');
                    $('#import-successModal').modal('show');
                    this.successFileURL = this.sanitizer.bypassSecurityTrustUrl(allresp['data'].exportedPatients);
                    this.successCount = allresp['data'].successCount;
                    this.failureCount = allresp['data'].failedCount;
                    // window.open(allresp['data']['exportedPatients'], "_blank");
                }
                else {
                    $('#importModal').modal('hide');
                    this.notifier.notify("error", allresp['result'].description);
                }
            }, error => {
                this.notifier.notify("error", "Error while exporting Patients check DB connection");
            });
        }
        else {
            this.ngxLoader.stop();
            alert("Please Select a file first");
        }
    }
    createMinCase(patientId, clientId, collectedBy, collectionDate, prifix, pLName, pFName, pDob, typeId, clientName) {
        this.ngxLoader.start();
        var d = new Date();
        var defaultPhysician = 0;
        if (typeof collectionDate == "undefined") {
            collectionDate = this.convertAge(d);
        }
        if (typeof collectedBy == "undefined") {
            collectedBy = 1;
        }
        // this.printLabel("cd123",pLName,pFName,pDob)
        var atdPhyUrl = environment.API_CLIENT_ENDPOINT + "clientsname";
        var reqTemplate = {
            header: {
                uuid: "",
                partnerCode: "",
                userCode: "",
                referenceNumber: "",
                systemCode: "",
                moduleCode: "CLIM",
                functionalityCode: "",
                systemHostAddress: "",
                remoteUserAddress: "",
                dateTime: ""
            },
            data: {
                clientIds: [clientId]
            }
        };
        // var reqTemplate = {clientIds:[clientId]}
        this.patientnService.getClientByIds(atdPhyUrl, reqTemplate).then(respClient => {
            console.log("respClient", respClient);
            if (respClient['data'][0]['clientUsers'].length == 0) {
                alert('No Physician Associated to this Client');
                this.ngxLoader.stop();
                return;
            }
            else {
                var physCount = 0; /////// if there is no default physician then slect 1s as physician
                var arrCount = 0;
                for (let i = 0; i < respClient['data'][0]['clientUsers'].length; i++) {
                    if (respClient['data'][0]['clientUsers'][i]['defaultPhysician'] == 1) {
                        defaultPhysician = respClient['data'][0]['clientUsers'][i]['defaultPhysician'];
                        physCount++;
                    }
                    arrCount = arrCount + 1;
                    if (arrCount == respClient['data'][0]['clientUsers'].length) {
                        if (physCount == 0) {
                            defaultPhysician = respClient['data'][0]['clientUsers'][0]['defaultPhysician'];
                        }
                    }
                }
            }
            // console.log('patientId',patientId);
            // console.log('clientId',clientId);
            // console.log('collectedBy',collectedBy);
            // console.log('collectionDate',collectionDate);
            // console.log('collectionDate',defaultPhysician);
            // console.log('collectionDate',pLName);
            // console.log('collectionDate',pFName);
            // console.log('collectionDate',pDob);
            // this.printLabel("cd123",pLName,pFName,pDob)
            this.saveMinCase(patientId, clientId, collectedBy, collectionDate, defaultPhysician, prifix, pLName, pFName, pDob, typeId, clientName);
        }, error => {
            this.notifier.notify('error', 'Error while fetching default physicians');
        });
    }
    saveMinCase(patientId, clientId, collectedBy, collectionDate, defaultPhysician, prifix, pLName, pFName, pDob, typeId, clientName) {
        this.ngxLoader.start();
        this.saveRequest.data.caseStatusId = 2;
        this.saveRequest.data.patientId = patientId;
        this.saveRequest.data.clientId = clientId;
        this.saveRequest.data.collectionDate = collectionDate;
        this.saveRequest.data.collectedBy = collectedBy;
        this.saveRequest.data.attendingPhysicianId = defaultPhysician;
        // this.saveRequest.data.caseCategoryPrefix = prifix;
        this.saveRequest.data.caseCategoryId = 1;
        this.saveRequest.data.caseCategoryPrefix = 'NH';
        this.saveRequest.data.caseSpecimen['specimenTypeId'] = typeId;
        console.log("this.saveRequest", this.saveRequest);
        var saveminUrl = environment.API_SPECIMEN_ENDPOINT + "minaccession";
        this.patientnService.saveMinAccession(saveminUrl, this.saveRequest).subscribe(saveResp => {
            console.log("saveResp", saveResp);
            this.notifier.notify("success", "Case saved successfully");
            this.printedCaseNumber = saveResp['data'].caseNumber;
            this.printedCaseCategory = prifix;
            this.printLabel(this.printedCaseNumber, pLName, pFName, pDob, prifix, clientName);
            var a = this.printedCaseNumber+';3';
            var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
            var bx = ax.replace('+','xsarm');
            var cx = bx.replace('/','ydan');
            var ciphertext = cx.replace('=','zhar');
            console.log('ciphertext',ciphertext);
            var ur = location.origin+'/edit-case/'+ciphertext+'/1';
            this.caseUrl = ur;
            // this.caseUrl = environment.UI_SPECIMEN_ENDPOINT + "/edit-case/" + this.printedCaseNumber + "/1;from=3";
            this.ngxLoader.stop();
            $('#successcreate-modal').modal('show');
        }, error => {
            this.notifier.notify("error", "Error while saving case backend exception");
        });
    }
    caseBypatientId(patientId, pFname, pLname, pDob) {
        this.selectedpatientId = patientId;
        this.selectedpFname = pFname;
        this.selectedpLname = pLname;
        this.selectedpDob = pDob;
        this.triggerHistoryTable = true;
        this.dtElement.forEach((dtElement, index) => {
            dtElement.dtInstance.then((dtInstance) => {
                if (dtInstance.table().node().id == 'spec-col-history') {
                    // dtInstance.destroy();
                    // dtInstance.draw();
                    dtInstance.ajax.reload();
                    // dtInstance.columns().every(function () {
                    //   $(this.header())
                    //     .search()
                    //     .draw();
                    // })
                    // dtInstance.destroy();
                }
                // dtInstance.destroy();
            });
        });
        $('#spec-collectionhistory-Modal').modal('show');
        // this.dtTrigger.next();
        // this.ngxLoader.start();
        // var reqURL = environment.API_SPECIMEN_ENDPOINT + 'findcasesbypatientid';
        // var reqData={header:{uuid:"",partnerCode:"",userCode:"",referenceNumber:"",systemCode:"",moduleCode:"",functionalityCode:"",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{page:"0",size:"2",sortColumn:"caseNumber",sortingOrder:"asc",patientIds:[patientId]}};
        // this.patientnService.caseByPatientId(reqURL, reqData).subscribe(respCases =>{
        //   console.log("respCases",respCases);
        //   var uniqueClientId = [];
        //   const map = new Map();
        //   for (const item of respCases['data']['caseDetails']) {
        //     if(!map.has(item.clientId)){
        //       map.set(item.clientId, true);    // set any value to Map
        //       uniqueClientId.push(item.clientId);
        //
        //     }
        //   }
        //   var clURL           = environment.API_CLIENT_ENDPOINT + "clientsname"
        //   var clReqData       = {clientIds:uniqueClientId}
        //   this.patientnService.getPatientsByIds(clURL,clReqData).then(selectedClient => {
        //     console.log("selectedClient",selectedClient);
        //     for (let i = 0; i < respCases['data']['caseDetails'].length; i++) {
        //       for (let j = 0; j <  selectedClient['length']; j++) {
        //         if (respCases['data']['caseDetails'][i].clientId == selectedClient[j].clientId) {
        //           respCases['data']['caseDetails'][i].clientName = selectedClient[j].name;
        //         }
        //
        //       }
        //       if (respCases['data']['caseDetails'][i].caseStatusId == 1 || respCases['data']['caseDetails'][i].caseNumber == null || respCases['data']['caseDetails'][i].caseNumber == '') {
        //         respCases['data']['caseDetails'][i].caseUrl = environment.UI_SPECIMEN_ENDPOINT + "/edit-case/"+respCases['data']['caseDetails'][i].caseId+";from=2";
        //       }
        //       else{
        //         respCases['data']['caseDetails'][i].caseUrl = environment.UI_SPECIMEN_ENDPOINT + "/edit-case/"+respCases['data']['caseDetails'][i].caseNumber;
        //       }
        //       respCases['data']['caseDetails'][i].pFname = pFname;
        //       respCases['data']['caseDetails'][i].pLname = pLname;
        //       respCases['data']['caseDetails'][i].pDob   = pDob;
        //
        //     }
        //
        //     this.slectedPatientCases = respCases['data']['caseDetails'];
        //     $('#spec-collectionhistory-Modal').modal('show');
        //     this.ngxLoader.stop();
        //
        //   });
        //
        // })
    }
    printLabel(caseNumber, lName, fName, dob, prifix, clientName) {
        // console.log("caseNumber",caseNumber);
        // console.log("fName",fName);
        // console.log("lName",lName);
        // console.log("dob",dob);
        // console.log("prifix",prifix);
        // console.log("clientName",clientName);
        this.ngxLoader.start();
        console.log("caseNumber", caseNumber);
        var labelXml = '<DieCutLabel Version="8.0" Units="twips" MediaType="Default"> <PaperOrientation>Landscape</PaperOrientation> <Id>Small30332</Id> <IsOutlined>false</IsOutlined> <PaperName>30332 1 in x 1 in</PaperName> <DrawCommands> <RoundRectangle X="0" Y="0" Width="1440" Height="1440" Rx="180" Ry="180" /> </DrawCommands> <ObjectInfo> <TextObject> <Name>FirstLast</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <HorizontalAlignment>Left</HorizontalAlignment> <VerticalAlignment>Top</VerticalAlignment> <TextFitMode>ShrinkToFit</TextFitMode> <UseFullFontHeight>True</UseFullFontHeight> <Verticalized>False</Verticalized> <StyledText> <Element> <String xml:space="preserve">First Last</String> <Attributes> <Font Family="Arial" Size="5" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" HueScale="100" /> </Attributes> </Element> </StyledText> </TextObject> <Bounds X="144" Y="199.393133236425" Width="1186.49076398256" Height="120" /> </ObjectInfo> <ObjectInfo> <BarcodeObject> <Name>BARCODE</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <Text>12345</Text> <Type>QRCode</Type> <Size>Medium</Size> <TextPosition>None</TextPosition> <TextFont Family="Arial" Size="8" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <CheckSumFont Family="Arial" Size="8" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <TextEmbedding>None</TextEmbedding> <ECLevel>0</ECLevel> <HorizontalAlignment>Center</HorizontalAlignment> <QuietZonesPadding Left="0" Top="0" Right="0" Bottom="0" /> </BarcodeObject> <Bounds X="381.704497509707" Y="771.219004098847" Width="541.292842170495" Height="526.701837189909" /> </ObjectInfo> <ObjectInfo> <TextObject> <Name>Facility</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <HorizontalAlignment>Left</HorizontalAlignment> <VerticalAlignment>Top</VerticalAlignment> <TextFitMode>ShrinkToFit</TextFitMode> <UseFullFontHeight>True</UseFullFontHeight> <Verticalized>False</Verticalized> <StyledText> <Element> <String xml:space="preserve">Facility</String> <Attributes> <Font Family="Arial" Size="4" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" HueScale="100" /> </Attributes> </Element> </StyledText> </TextObject> <Bounds X="164.897098682172" Y="651.471024372647" Width="1183.87862664729" Height="120" /> </ObjectInfo> <ObjectInfo> <TextObject> <Name>DOB</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <HorizontalAlignment>Left</HorizontalAlignment> <VerticalAlignment>Top</VerticalAlignment> <TextFitMode>ShrinkToFit</TextFitMode> <UseFullFontHeight>True</UseFullFontHeight> <Verticalized>False</Verticalized> <StyledText> <Element> <String xml:space="preserve">DOB</String> <Attributes> <Font Family="Arial" Size="5" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" HueScale="100" /> </Attributes> </Element> </StyledText> </TextObject> <Bounds X="144" Y="310.963055930226" Width="1210" Height="120" /> </ObjectInfo> <ObjectInfo> <TextObject> <Name>Client</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <HorizontalAlignment>Left</HorizontalAlignment> <VerticalAlignment>Top</VerticalAlignment> <TextFitMode>ShrinkToFit</TextFitMode> <UseFullFontHeight>True</UseFullFontHeight> <Verticalized>False</Verticalized> <StyledText> <Element> <String xml:space="preserve">Client</String> <Attributes> <Font Family="Arial" Size="5" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" HueScale="100" /> </Attributes> </Element> </StyledText> </TextObject> <Bounds X="144" Y="418.060686676358" Width="1210" Height="120" /> </ObjectInfo> <ObjectInfo> <TextObject> <Name>CaseCode</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <HorizontalAlignment>Left</HorizontalAlignment> <VerticalAlignment>Top</VerticalAlignment> <TextFitMode>ShrinkToFit</TextFitMode> <UseFullFontHeight>True</UseFullFontHeight> <Verticalized>False</Verticalized> <StyledText> <Element> <String xml:space="preserve">Case Code</String> <Attributes> <Font Family="Arial" Size="5" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" HueScale="100" /> </Attributes> </Element> </StyledText> </TextObject> <Bounds X="144" Y="525.15831742249" Width="1210" Height="120" /> </ObjectInfo> </DieCutLabel>';
        var label = dymo.label.framework.openLabelXml(labelXml);
        // var barcodeData = 'Si Paradigm'
        label.setObjectText('BARCODE', caseNumber);
        // set label text
        label.setObjectText("FirstLast", lName + ',' + fName);
        label.setObjectText("DOB", dob);
        label.setObjectText("Facility", prifix);
        label.setObjectText("Client", clientName.substring(0, 10));
        label.setObjectText("CaseCode", caseNumber);
        var printers = dymo.label.framework.getPrinters();
        if (printers.length == 0)
            throw "No DYMO printers are installed. Install DYMO printers.";
        var printerName = "";
        for (var i = 0; i < printers.length; ++i) {
            var printer = printers[i];
            if (printer.printerType == "LabelWriterPrinter") {
                printerName = printer.name;
                break;
            }
        }
        label.print(printerName);
        this.printedCaseNumber = caseNumber;
        console.log("this.printedCaseNumber--", this.printedCaseNumber);
        // $('#pecimenlabel-Modal').modal('show');
        // $('#pecimenlabel-Modal').modal('show');
        this.ngxLoader.stop();
    }
    toggleActiveClient(type, patientId, index) {
        var mes = '';
        if (type == true) {
            mes = "activate";
        }
        else {
            mes = "deactivate";
        }
        console.log("patientToDisable", patientId);
        console.log("type", type);
        var requestTemplate = { header: { uuid: "", partnerCode: "", userCode: "", referenceNumber: "", systemCode: "", moduleCode: "", functionalityCode: "", systemHostAddress: "", remoteUserAddress: "", dateTime: "" }, data: { username: "danish", patientId: patientId, status: type } };
        var url = environment.API_PATIENT_ENDPOINT + 'changestatus';
        Swal.fire({
            title: 'Are you sure?',
            text: "Are you sure to " + mes + " the patient!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, do it!'
        }).then((result) => {
            this.ngxLoader.start();
            this.patientnService.togglePatientStatus(url, requestTemplate).subscribe(resp => {
                $('#deactivateModal').modal('hide');
                this.ngxLoader.stop();
                if (resp['result']['codeType'] == 'S') {
                    this.notifier.notify("success", "Patient deactivated successfully");
                    if (type == true) {
                        this.allPatients[index].status = 1;
                    }
                    else {
                        this.allPatients[index].status = 0;
                    }
                }
                else {
                    this.notifier.notify("error", resp['result']['description']);
                }
            }, error => {
                this.notifier.notify("error", "Error while changing status check DB connection");
            });
        });
    }
    showMergeTable(firstPatientID) {
        this.mergeView = true;
    }
    //////////////////////////////////////////////////////////
    ////////// MERGE PATIENT FUNCTIONS //////////////////////
    ////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////
    ////////// MERGE PATIENT FUNCTIONS END //////////////////////
    ////////////////////////////////////////////////////////
    convertAge(today) {
        var monthDB;
        var dayDB;
        if ((today.getMonth() + 1) < 10) {
            monthDB = "0" + (today.getMonth() + 1);
        }
        else {
            monthDB = (today.getMonth() + 1);
        }
        if (today.getDate() < 10) {
            dayDB = "0" + today.getDate();
        }
        else {
            dayDB = today.getDate();
        }
        return (today.getFullYear() + "-" + monthDB + "-" + dayDB);
    }
    getLookups() {
        // this.ngxLoader.start();
        return new Promise((resolve, reject) => {
            var specType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=SPECIMEN_TYPE_CACHE&partnerCode=sip').then(response => {
                return response;
            });
            forkJoin([specType]).subscribe(allLookups => {
                console.log("allLookups", allLookups);
                // this.allpattypes    = allLookups[0];
                this.allSpecimenTypes = allLookups[0];
                resolve();
            });
        });
    }
};
__decorate([
    ViewChildren(DataTableDirective)
], ManagePatientsComponent.prototype, "dtElement", void 0);
ManagePatientsComponent = __decorate([
    Component({
        selector: 'app-manage-patients',
        templateUrl: './manage-patients.component.html',
        styleUrls: ['./manage-patients.component.css'],
        host: {
            class: 'mainComponentStyle'
        }
    })
], ManagePatientsComponent);
export { ManagePatientsComponent };
//# sourceMappingURL=manage-patients.component.js.map
