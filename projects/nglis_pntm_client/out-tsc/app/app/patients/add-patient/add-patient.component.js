import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { Subject, forkJoin } from "rxjs";
let AddPatientComponent = class AddPatientComponent {
    constructor(formBuilder, http, router, route, sanitizer,
    // private httpRequest  : HttpRequestsService,
    ngxLoader, notifier, patientnService, globalService, sortPipe) {
        this.formBuilder = formBuilder;
        this.http = http;
        this.router = router;
        this.route = route;
        this.sanitizer = sanitizer;
        this.ngxLoader = ngxLoader;
        this.notifier = notifier;
        this.patientnService = patientnService;
        this.globalService = globalService;
        this.sortPipe = sortPipe;
        this.searchTerm$ = new Subject();
        this.immuteable = {
            name: 'Dzon',
            age: 25,
            address: 'Sunny street 34'
        };
        this.clientsDropdown = [];
        this.allpattypes = [];
        this.allBillingypes = [];
        this.insuranceDropdown = [
            { id: 1, name: 'Fidelis Care NewYork' },
            { id: 2, name: '----------' },
            { id: 3, name: '----------' }
        ];
        this.allEthnicities = [];
        this.allRaces = [];
        this.allCountry = [];
        this.allState = [];
        this.allCities = [];
        this.submitted = false;
        this.isInsurance = 0;
        this.pageType = 'add';
        this.mobileNumber = null;
        this.homePhone = null;
        this.saveRequest = {
            header: {
                uuid: "",
                partnerCode: "",
                userCode: "",
                referenceNumber: "",
                systemCode: "",
                moduleCode: "",
                functionalityCode: "",
                systemHostAddress: "",
                remoteUserAddress: "",
                dateTime: ""
            },
            data: {
                accountNumber: "ABC-21",
                patientId: null,
                clientId: null,
                status: true,
                createdSource: 1,
                patientInsurance: 123423234,
                patientDemographics: {
                    patientTypeId: 1,
                    patientTypeStr: null,
                    paymentTypeId: 0,
                    firstName: null,
                    middleName: null,
                    lastName: null,
                    gender: "m",
                    dateOfBirth: "",
                    ssn: "",
                    ethnicityId: null,
                    raceId: null,
                    address: {
                        street: "37",
                        suiteFloorBuilding: null,
                        city: null,
                        zip: null,
                        stateId: null,
                        stateOther: "ISB",
                        countryId: 1,
                        moduleCode: "PNTM",
                        createdBy: "sip-C-0011",
                        createdTimestamp: "09-15-2020"
                    },
                    contacts: [{
                            contactTypeId: 1,
                            value: "+92 345 1234567",
                            moduleCode: "PNTM",
                            createdBy: "sip-C-0011",
                            createdTimestamp: "09-15-2020"
                        }
                    ],
                    createdBy: "sip-C-0011",
                    createdTimestamp: "09-15-2020"
                },
                createdBy: "danish",
                createdTimestamp: "09-15-2020",
                userName: "danish"
            }
        };
        this.patientExistReq = {
            header: {
                uuid: "",
                partnerCode: "",
                userCode: "",
                referenceNumber: "",
                systemCode: "",
                moduleCode: "",
                functionalityCode: "",
                systemHostAddress: "",
                remoteUserAddress: "",
                dateTime: ""
            },
            data: {
                username: "",
                firstName: "",
                lastName: "",
                dateOfBirth: "",
                clientId: 2
            }
        };
        this.ssnExistReq = {
            header: {
                uuid: "",
                partnerCode: "",
                userCode: "",
                referenceNumber: "",
                systemCode: "",
                moduleCode: "",
                functionalityCode: "",
                systemHostAddress: "",
                remoteUserAddress: "",
                dateTime: ""
            },
            data: {
                username: "danish",
                ssn: ""
            }
        };
        this.searchClientName = {
            header: {
                uuid: "",
                partnerCode: "",
                userCode: "",
                referenceNumber: "",
                systemCode: "",
                moduleCode: "CLIM",
                functionalityCode: "",
                systemHostAddress: "",
                remoteUserAddress: "",
                dateTime: ""
            },
            data: {
                sortColumn: "name",
                sortingOrder: "asc",
                searchString: "",
                userCode    : ""
            }
        };
        this.clientLoading = false;
        this.clientExist = false;
        this.ssnExist = false;
        this.contactrequestBP = {
            contactTypeId: 1,
            value: "",
            moduleCode: "",
            createdBy: "",
            createdTimestamp: ""
        };
        this.contactrequestBPUpdate = {
            contactTypeId: 1,
            contactId: 1,
            value: "",
            moduleCode: "",
            createdBy: "",
            createdTimestamp: ""
        };
        this.selectedPatient = [];
        this.contactID1 = null;
        this.contactID2 = null;
        this.contactID3 = null;
        this.update_patientId = null;
        this.notifier = notifier;
        this.swalStyle = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success my-swal-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: true
        });
    }
    ngOnInit() {
        this.ngxLoader.start();
        this.getLookups().then(lookupsLoaded => {
            this.ngxLoader.stop();
        });
        this.patientnService.search(this.searchTerm$)
            .subscribe(results => {
            console.log("results", results);
            this.clientsDropdown = results['data'];
            this.clientLoading = false;
        }, error => {
            this.ngxLoader.stop();
            this.notifier.notify("error", "Error while fetching clients backend connect aborted");
            this.clientLoading = false;
            return;
            // console.log("error",error);
        });
        this.patientForm = this.formBuilder.group({
            client: ['', [Validators.required]],
            ssn: [''],
            patientID: [{ value: '', disabled: true }],
            lastName: ['', [Validators.required, Validators.maxLength(50)]],
            firstName: ['', [Validators.required, Validators.maxLength(50)]],
            middleName: [''],
            dob: ['', [Validators.required]],
            gender: ['', [Validators.required]],
            email: ['', [Validators.email]],
            mobile: ['', [Validators.maxLength(50)]],
            phone: ['', [Validators.maxLength(50)]],
            patientType: ['', [Validators.required]],
            billingInfo: ['', [Validators.required]],
            insuranceRadio: ['0', [Validators.required]],
            insuranceName: [''],
            ethnicity: ['', [Validators.required]],
            race: ['', [Validators.required]],
            insuranceID: [''],
            street: ['', [Validators.maxLength(100)]],
            suite: ['', [Validators.maxLength(100)]],
            country: [''],
            state: [''],
            city: [''],
            zip: ['', [Validators.maxLength(10)]],
            activedeactive: ['']
        });
        this.route.params.subscribe(params => {
            // console.log('params',params['id']);
            if (typeof params['id'] == "undefined") {
                this.pageType = 'add';
            }
            else {
                this.pageType = 'edit';
                let getPatientReq = {};
                var selectURL = environment.API_PATIENT_ENDPOINT + 'patientbyid';
                getPatientReq = { header: { uuid: "", partnerCode: "", userCode: "", referenceNumber: "", systemCode: "", moduleCode: "", functionalityCode: "", systemHostAddress: "", remoteUserAddress: "", dateTime: "" }, data: { username: "danish", patientId: params['id'] } };
                this.getSelectedRecord(selectURL, getPatientReq).then(selectedPatient => {
                    // console.log("Selected Case response",selectedPatient);
                    // this.chec();
                    console.log("selectedPatient", this.selectedPatient);
                });
            }
            console.log('pageType:  ', this.pageType);
        });
    }
    ngAfterViewInit() {
    }
    check(e) {
        console.log("date", e.target.value);
    }
    zeroPadded(val) {
        if (val >= 10)
            return val;
        else
            return '0' + val;
    }
    getSelectedRecord(url, data) {
        this.ngxLoader.start();
        return new Promise((resolve, reject) => {
            this.patientnService.getPatientbyID(url, data).subscribe(resp => {
                this.getAge(resp['data'].patientDemographics.dateOfBirth);
                // var ar = resp['data'].patientDemographics.dateOfBirth.split('-');
                // console.log("ar",ar);
                // this.dobToShow = ar[2]+"-"+ar[0]+"-"+ar[1];
                // console.log("dobToShow",this.dobToShow);
                if (typeof resp['data'].patientDemographics.contacts[0] != "undefined") {
                    if (resp['data'].patientDemographics.contacts[0].contactTypeId == 1) {
                        this.contactID1 = resp['data'].patientDemographics.contacts[0].contactId;
                        this.mobileNumber = resp['data'].patientDemographics.contacts[0].value;
                    }
                    else if (resp['data'].patientDemographics.contacts[0].contactTypeId == 4) {
                        this.contactID2 = resp['data'].patientDemographics.contacts[0].contactId;
                        this.homePhone = resp['data'].patientDemographics.contacts[0].value;
                    }
                    else if (resp['data'].patientDemographics.contacts[0].contactTypeId == 2) {
                        this.contactID3 = resp['data'].patientDemographics.contacts[0].contactId;
                        this.emailAddress = resp['data'].patientDemographics.contacts[0].value;
                    }
                }
                if (typeof resp['data'].patientDemographics.contacts[1] != "undefined") {
                    if (resp['data'].patientDemographics.contacts[1].contactTypeId == 1) {
                        this.contactID1 = resp['data'].patientDemographics.contacts[1].contactId;
                        this.mobileNumber = resp['data'].patientDemographics.contacts[1].value;
                    }
                    else if (resp['data'].patientDemographics.contacts[1].contactTypeId == 4) {
                        this.contactID2 = resp['data'].patientDemographics.contacts[1].contactId;
                        this.homePhone = resp['data'].patientDemographics.contacts[1].value;
                    }
                    else if (resp['data'].patientDemographics.contacts[1].contactTypeId == 2) {
                        this.contactID3 = resp['data'].patientDemographics.contacts[1].contactId;
                        this.emailAddress = resp['data'].patientDemographics.contacts[1].value;
                    }
                }
                if (typeof resp['data'].patientDemographics.contacts[2] != "undefined") {
                    if (resp['data'].patientDemographics.contacts[2].contactTypeId == 1) {
                        this.contactID1 = resp['data'].patientDemographics.contacts[2].contactId;
                        this.mobileNumber = resp['data'].patientDemographics.contacts[2].value;
                    }
                    else if (resp['data'].patientDemographics.contacts[2].contactTypeId == 4) {
                        this.contactID2 = resp['data'].patientDemographics.contacts[2].contactId;
                        this.homePhone = resp['data'].patientDemographics.contacts[2].value;
                    }
                    else if (resp['data'].patientDemographics.contacts[2].contactTypeId == 2) {
                        this.contactID3 = resp['data'].patientDemographics.contacts[2].contactId;
                        this.emailAddress = resp['data'].patientDemographics.contacts[2].value;
                    }
                }
                this.update_patientId = resp['data'].patientId;
                if (typeof resp['data'].patientDemographics.address == "undefined") {
                    resp['data'].patientDemographics.address = {
                        street: "",
                        suiteFloorBuilding: "",
                        city: "",
                        zip: "",
                        stateId: null,
                        stateOther: "",
                        countryId: "",
                        moduleCode: "PNTM",
                        createdBy: "sip-C-0011",
                        createdTimestamp: "09-15-2020"
                    };
                }
                else {
                    if (typeof resp['data'].patientDemographics.address.street == 'undefined' || resp['data'].patientDemographics.address.street == '') {
                        resp['data'].patientDemographics.address.street = '';
                    }
                    if (typeof resp['data'].patientDemographics.address.suiteFloorBuilding == 'undefined' || resp['data'].patientDemographics.address.suiteFloorBuilding == '') {
                        resp['data'].patientDemographics.address.suiteFloorBuilding = '';
                    }
                    if (typeof resp['data'].patientDemographics.address.city == 'undefined' || resp['data'].patientDemographics.address.city == '') {
                        resp['data'].patientDemographics.address.city = '';
                    }
                    if (typeof resp['data'].patientDemographics.address.zip == 'undefined' || resp['data'].patientDemographics.address.zip == '') {
                        resp['data'].patientDemographics.address.zip = '';
                    }
                    if (typeof resp['data'].patientDemographics.address.stateId == 'undefined' || resp['data'].patientDemographics.address.stateId == '') {
                        resp['data'].patientDemographics.address.stateId = '';
                    }
                    if (typeof resp['data'].patientDemographics.address.countryId == 'undefined' || resp['data'].patientDemographics.address.countryId == '') {
                        resp['data'].patientDemographics.address.countryId = '';
                    }
                }
                if (typeof resp['data'].patientDemographics.middleName == 'undefined') {
                    resp['data'].patientDemographics.middleName = '';
                }
                if (typeof resp['data'].patientDemographics.ssn == 'undefined') {
                    resp['data'].patientDemographics.ssn = '';
                }
                var getCLurl = environment.API_CLIENT_ENDPOINT + "clientsname";
                var reqTemplate = {
                    header: {
                        uuid: "",
                        partnerCode: "",
                        userCode: "",
                        referenceNumber: "",
                        systemCode: "",
                        moduleCode: "CLIM",
                        functionalityCode: "",
                        systemHostAddress: "",
                        remoteUserAddress: "",
                        dateTime: ""
                    },
                    data: {
                        clientIds: [resp['data'].clientId]
                    }
                };
                // var reqTemplate = {clientIds:[resp['data'].clientId]}
                this.patientnService.getClientByName(getCLurl, reqTemplate).subscribe(selectedClient => {
                    console.log("selectedClient", selectedClient);
                    this.clientsDropdown = selectedClient['data'];
                    this.clientSearchString = selectedClient['data'][0].clientId;
                    this.ngxLoader.stop();
                }, error => {
                    this.ngxLoader.stop();
                    this.notifier.notify("error", "Error while fetching clients backend connect aborted");
                    return;
                    // console.log("error",error);
                });
                this.selectedPatient = resp['data'];
                this.saveRequest.data = resp['data'];
                console.log("this.saveRequest", this.saveRequest);
                // return true;
                // this.ngxLoader.stop();
                resolve();
            }, error => {
                // return false;
                // console.log("error: ",error);
                this.ngxLoader.stop();
                this.notifier.notify("error", "Error while loading case number search results");
                reject();
            });
            // resolve();
        });
    }
    get f() { return this.patientForm.controls; }
    addPatient() {
        this.ngxLoader.start();
        this.submitted = true;
        if (this.patientForm.invalid) {
            this.ngxLoader.stop();
            // alert('form invalid');
            this.scrollToError();
            return;
        }
        if (this.pageType == 'add') {
            this.savePatientRequest();
        }
        else if (this.pageType == 'edit') {
            this.updatePatient();
        }
        else {
            this.ngxLoader.start();
        }
    }
    savePatientRequest() {
        var contactholder = [];
        this.saveRequest.data.patientDemographics.contacts = [];
        if (this.mobileNumber != null) {
            var contactBP = Object.assign({}, this.contactrequestBP);
            contactBP.contactTypeId = 1;
            contactBP.value = this.mobileNumber;
            contactBP.moduleCode = "PNTM";
            contactBP.createdBy = "sip-C-0011";
            contactBP.createdTimestamp = "09-15-2020";
            contactholder.push(contactBP);
        }
        if (this.homePhone != null) {
            var contactBP = Object.assign({}, this.contactrequestBP);
            contactBP.contactTypeId = 4;
            contactBP.value = this.mobileNumber;
            contactBP.moduleCode = "PNTM";
            contactBP.createdBy = "sip-C-0011";
            contactBP.createdTimestamp = "09-15-2020";
            contactholder.push(contactBP);
        }
        if (this.emailAddress != null) {
            var contactBP = Object.assign({}, this.contactrequestBP);
            contactBP.contactTypeId = 2;
            contactBP.value = this.emailAddress;
            contactBP.moduleCode = "PNTM";
            contactBP.createdBy = "sip-C-0011";
            contactBP.createdTimestamp = "09-15-2020";
            contactholder.push(contactBP);
        }
        if ((this.mobileNumber == null || this.mobileNumber == '') && (this.homePhone == null || this.homePhone == '')) {
            contactholder = [];
        }
        this.saveRequest.data.patientDemographics.contacts = contactholder;
        console.log("saveRequest", this.saveRequest);
        const saveURL = environment.API_PATIENT_ENDPOINT + 'save';
        this.patientnService.savePatient(saveURL, this.saveRequest).subscribe(resp => {
            console.log("Save Response", resp);
            if (resp['result'].codeType == "S") {
                // this.notifier.getConfig().behaviour.autoHide = 8000;
                this.notifier.notify("success", "Patient saved Successfully");
                this.submitted = false;
                this.ngxLoader.stop();
                this.router.navigate(['all-patients']);
            }
            else {
                this.notifier.notify("error", "Error while saving patient");
                this.submitted = false;
                this.ngxLoader.stop();
                return;
            }
        }, error => {
            this.ngxLoader.stop();
            this.notifier.notify("error", "Error while checking client existance backend connect aborted");
        });
    }
    updatePatient() {
        var contactholder = [];
        this.saveRequest.data.patientDemographics.contacts = [];
        if (this.mobileNumber != null) {
            var contactBP = Object.assign({}, this.contactrequestBPUpdate);
            contactBP.contactTypeId = 1;
            if (typeof $('#mobile-number').attr('contactID-value') == "undefined") {
                contactBP.contactId = null;
            }
            else {
                contactBP.contactId = $('#mobile-number').attr('contactID-value');
            }
            contactBP.value = this.mobileNumber;
            contactBP.moduleCode = "PNTM";
            contactBP.createdBy = "sip-C-0011";
            contactBP.createdTimestamp = "09-15-2020";
            contactholder.push(contactBP);
        }
        if (this.homePhone != null) {
            var contactBP = Object.assign({}, this.contactrequestBPUpdate);
            contactBP.contactTypeId = 4;
            if (typeof $('#home-number').attr('contactID-value') == "undefined") {
                contactBP.contactId = null;
            }
            else {
                contactBP.contactId = $('#home-number').attr('contactID-value');
            }
            contactBP.value = this.homePhone;
            contactBP.moduleCode = "PNTM";
            contactBP.createdBy = "sip-C-0011";
            contactBP.createdTimestamp = "09-15-2020";
            contactholder.push(contactBP);
        }
        if (this.emailAddress != null) {
            var contactBP = Object.assign({}, this.contactrequestBPUpdate);
            contactBP.contactTypeId = 2;
            if (typeof $('#email-address').attr('contactID-value') == "undefined") {
                contactBP.contactId = null;
            }
            else {
                contactBP.contactId = $('#email-address').attr('contactID-value');
            }
            contactBP.value = this.emailAddress;
            contactBP.moduleCode = "PNTM";
            contactBP.createdBy = "sip-C-0011";
            contactBP.createdTimestamp = "09-15-2020";
            contactholder.push(contactBP);
        }
        if ((this.mobileNumber == null || this.mobileNumber == '') && (this.homePhone == null || this.homePhone == '')) {
            contactholder = [];
        }
        this.saveRequest.data.patientDemographics.contacts = contactholder;
        this.saveRequest.data.patientId = this.update_patientId;
        console.log("saveRequest", this.saveRequest);
        const saveURL = environment.API_PATIENT_ENDPOINT + 'update';
        this.patientnService.updatePatient(saveURL, this.saveRequest).subscribe(resp => {
            console.log("Save Response", resp);
            if (resp['result'].codeType == "S") {
                // this.notifier.getConfig().behaviour.autoHide = 8000;
                this.notifier.notify("success", "Patient updated Successfully");
                this.submitted = false;
                this.ngxLoader.stop();
                // this.router.navigate(['all-patients']);
            }
            else {
                this.notifier.notify("error", "Error while updating patient");
                this.submitted = false;
                this.ngxLoader.stop();
                return;
            }
        }, error => {
            this.ngxLoader.stop();
            this.notifier.notify("error", "Error while checking client existance backend connect aborted");
        });
    }
    getAge(e) {
        var today = new Date();
        var birthDate = new Date(e);
        // var birthDate = new Date(e.target.value);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age = age - 1;
        }
        // var temp  = birthDate.getMonth()+1;
        var ageDB;
        var monthDB;
        var dayDB;
        if ((birthDate.getMonth() + 1) < 10) {
            monthDB = "0" + (birthDate.getMonth() + 1);
            // ageDB = a + "-" + birthDate.getDate() + "-" + birthDate.getFullYear();
        }
        else {
            monthDB = (birthDate.getMonth() + 1);
            // ageDB = (birthDate.getMonth()+1) + "-" + birthDate.getDate() + "-" + birthDate.getFullYear();
        }
        if (birthDate.getDate() < 10) {
            dayDB = "0" + birthDate.getDate();
        }
        else {
            dayDB = birthDate.getDate();
        }
        ageDB = monthDB + "-" + dayDB + "-" + birthDate.getFullYear();
        // console.log("ageDB",ageDB);
        this.saveRequest.data.patientDemographics.dateOfBirth = ageDB;
        var ar = ageDB.split('-');
        console.log("ar", ar);
        this.dobToShow = ar[2] + "-" + ar[0] + "-" + ar[1];
        console.log("dobToShow", this.dobToShow);
        // console.log("age",age);
        this.dateOfBirth = age;
        this.patientExistCheck();
        // return;
    }
    changeInsurance(e) {
        var value = e.target.value;
        // console.log("value",value);
        if (value == 1) {
            this.isInsurance = 1;
            this.patientForm.get('insuranceName').setValidators([Validators.required, Validators.maxLength(50)]);
            this.patientForm.controls["insuranceName"].updateValueAndValidity();
            // this.patientForm.get('insuranceName').setValidators(Validators.maxLength(50))
            this.patientForm.get('insuranceID').setValidators([Validators.required, Validators.maxLength(50)]);
            this.patientForm.controls["insuranceID"].updateValueAndValidity();
            // this.patientForm.get('insuranceID').setValidators(Validators.maxLength(50))
        }
        else {
            this.patientForm.get('insuranceName').clearValidators();
            this.patientForm.controls["insuranceName"].updateValueAndValidity();
            this.patientForm.get('insuranceID').clearValidators();
            this.patientForm.controls["insuranceID"].updateValueAndValidity();
            this.isInsurance = 0;
        }
    }
    ////////// search client
    loadSearchClients(e) {
        if (e.target.value.length >= 3) {
            this.clientLoading = true;
            const searchURL = environment.API_CLIENT_ENDPOINT + 'searchclientbyname';
            this.searchClientName.data.searchString = e.target.value;
            this.patientnService.searchClientByName(searchURL, this.searchClientName).subscribe(resp => {
                this.clientsDropdown = resp['data'];
                this.clientLoading = false;
            });
        }
        else {
            console.log("length not match");
        }
    }
    clientChanged() {
        // console.log("this.saveRequest",this.saveRequest);
        this.saveRequest.data.clientId = this.clientSearchString;
        // this.saveRequest.data.clientId = this.clientSearchString.clientId;
        console.log("client Changed", this.clientSearchString);
        this.patientExistCheck();
    }
    patientExistCheck() {
        console.log("dob", this.saveRequest.data.patientDemographics.dateOfBirth);
        // console.log("firstName",this.saveRequest.data.patientDemographics.firstName);
        // console.log("lastName",this.saveRequest.data.patientDemographics.lastName);
        // console.log("ssn",this.saveRequest.data.patientDemographics.ssn);
        if (this.saveRequest.data.patientDemographics.dateOfBirth == '' || this.saveRequest.data.patientDemographics.firstName == '' || this.saveRequest.data.patientDemographics.lastName == ''
            || this.saveRequest.data.patientDemographics.dateOfBirth == null || this.saveRequest.data.patientDemographics.firstName == null || this.saveRequest.data.patientDemographics.lastName == null || this.saveRequest.data.clientId == null) {
            return;
        }
        else {
            $('.existCheck-loadergif').css('display', "inline-block");
            var birthDate = new Date(this.saveRequest.data.patientDemographics.dateOfBirth);
            // var ageDB = (birthDate.getMonth()+1) + "-" + birthDate.getDate() + "-" + birthDate.getFullYear();
            var sarchreqData = Object.assign({}, this.patientExistReq);
            sarchreqData.data.firstName = this.saveRequest.data.patientDemographics.firstName;
            sarchreqData.data.lastName = this.saveRequest.data.patientDemographics.lastName;
            // sarchreqData.data.dateOfBirth  = ageDB;
            sarchreqData.data.dateOfBirth = this.saveRequest.data.patientDemographics.dateOfBirth;
            sarchreqData.data.clientId = this.saveRequest.data.clientId;
            console.log("sarchreqData", sarchreqData);
            const existURL = environment.API_PATIENT_ENDPOINT + 'patientexists';
            this.patientnService.patientexists(existURL, sarchreqData).subscribe(resp => {
                console.log("resp Patient Exist", resp);
                if (resp['data'] == true) {
                    $('.existCheck-loadergif').css('display', "none");
                    this.clientExist = true;
                    // this.notifier.getConfig().behaviour.autoHide = 8000;
                    this.notifier.notify("warning", "Client already exists please change Firstname/LastName/Date of Birth");
                }
                else if (resp['data'] == null) {
                    this.clientExist = true;
                    this.notifier.notify("warning", resp['result']['description']);
                }
                else {
                    $('.existCheck-loadergif').css('display', "none");
                    this.clientExist = false;
                    return;
                }
            }, error => {
                $('.existCheck-loadergif').css('display', "none");
                this.notifier.notify("error", "Error while checking client existance backend connect aborted");
            });
        }
    }
    ssnExistCheck() {
        // console.log("ssn",this.saveRequest.data.patientDemographics.ssn);
        if (this.saveRequest.data.patientDemographics.ssn == '' || this.saveRequest.data.patientDemographics.ssn == null) {
            return;
        }
        else {
            $('#ssn-loadergif').css('display', "inline-block");
            var sarchreqData = Object.assign({}, this.ssnExistReq);
            sarchreqData.data.ssn = this.saveRequest.data.patientDemographics.ssn;
            console.log("sarchreqData", sarchreqData);
            const existURL = environment.API_PATIENT_ENDPOINT + 'patientexists';
            this.patientnService.patientexists(existURL, sarchreqData).subscribe(resp => {
                console.log("resp SSN", resp);
                if (resp['data'] == true) {
                    $('#ssn-loadergif').css('display', "none");
                    this.ssnExist = true;
                    this.notifier.notify("warning", "SSN already exists, Please provide different SSN");
                }
                else {
                    this.ssnExist = false;
                    $('#ssn-loadergif').css('display', "none");
                    return;
                }
            }, error => {
                $('#ssn-loadergif').css('display', "none");
                this.notifier.notify("error", "Error while checking client existance backend connect aborted");
            });
        }
    }
    scrollTo(el) {
        if (el) {
            el.scrollIntoView({ behavior: 'smooth', block: 'center' });
        }
    }
    scrollToError() {
        const firstElementWithError = document.querySelector('.ng-invalid[formControlName]');
        console.log("aa:", firstElementWithError);
        this.scrollTo(firstElementWithError);
    }
    toggleActiveClient(type, patientId) {
        var mes = '';
        if (type == true) {
            mes = "activate";
        }
        else {
            mes = "deactivate";
        }
        console.log("patientToDisable", patientId);
        console.log("type", type);
        var requestTemplate = { header: { uuid: "", partnerCode: "", userCode: "", referenceNumber: "", systemCode: "", moduleCode: "", functionalityCode: "", systemHostAddress: "", remoteUserAddress: "", dateTime: "" }, data: { username: "danish", patientId: patientId, status: type } };
        var url = environment.API_PATIENT_ENDPOINT + 'changestatus';
        Swal.fire({
            title: 'Are you sure?',
            text: "Are you sure to " + mes + " the patient!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, do it!'
        }).then((result) => {
            this.ngxLoader.start();
            this.patientnService.togglePatientStatus(url, requestTemplate).subscribe(resp => {
                $('#deactivateModal').modal('hide');
                this.ngxLoader.stop();
                if (resp['result']['codeType'] == 'S') {
                    this.notifier.notify("success", "Patient deactivated successfully");
                }
                else {
                    this.notifier.notify("error", resp['result']['description']);
                }
            }, error => {
                this.notifier.notify("error", "Error while changing status check DB connection");
            });
        });
    }
    getLookups() {
        this.ngxLoader.start();
        return new Promise((resolve, reject) => {
            var patCat = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=PATIENT_TYPE_CACHE&partnerCode=sip').then(response => {
                return response;
            });
            var ethinType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=ETHNICITY_CACHE&partnerCode=sip').then(response => {
                return response;
            });
            var raceType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=RACE_CACHE&partnerCode=sip').then(response => {
                return response;
            });
            var billingType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=PAYMENT_TYPE_CACHE&partnerCode=sip').then(response => {
                return response;
            });
            var allCountry = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=COUNTRY_CACHE&partnerCode=sip').then(response => {
                return response;
            });
            // var allState = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=STATE_CACHE&partnerCode=sip').then(response =>{
            //   return response;
            // })
            var allCity = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=PAYMENT_TYPE_CACHE&partnerCode=sip').then(response => {
                return response;
            });
            forkJoin([patCat, billingType, ethinType, raceType, allCountry, allCity]).subscribe(allLookups => {
                console.log("allLookups", allLookups);
                // this.allpattypes    = allLookups[0];
                this.allpattypes = this.sortPipe.transform(allLookups[0], "asc", "patientTypeName");
                this.allBillingypes = this.sortPipe.transform(allLookups[1], "asc", "paymentTypeName");
                // this.allEthnicities = allLookups[2];
                this.allEthnicities = this.sortPipe.transform(allLookups[2], "asc", "ethnicityName");
                this.allRaces = this.sortPipe.transform(allLookups[3], "asc", "raceName");
                this.allCountry = this.sortPipe.transform(allLookups[4], "asc", "countryName");
                // this.allState       = this.sortPipe.transform(allLookups[5], "asc", "stateName");
                for (let index = 0; index < allLookups[4]['length']; index++) {
                    if (allLookups[4][index]['countryName'] == 'USA' || allLookups[4][index]['countryName'] == 'usa') {
                        this.saveRequest.data.patientDemographics.address.countryId = allLookups[4][index]['countryId'];
                        this.changeCountry();
                    }
                }
                resolve();
            });
        });
    }
    changeCountry() {
        if (this.saveRequest.data.patientDemographics.address.countryId != null) {
            var cid = this.saveRequest.data.patientDemographics.address.countryId;
            this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=STATE_CACHE&partnerCode=sip&id=' + cid).then(response => {
                // console.log("states",response);
                this.allState = this.sortPipe.transform(response, "asc", "stateName");
            });
        }
    }
    changeState() {
        if (this.saveRequest.data.patientDemographics.address.stateId != null) {
            var cid = this.saveRequest.data.patientDemographics.address.stateId;
            this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=CITY_CACHE&partnerCode=sip&id=' + cid).then(response => {
                // console.log("states",response);
                this.allCities = this.sortPipe.transform(response, "asc", "cityName");
            });
        }
    }
    checkLookups() {
        console.log("saveRequest", this.saveRequest);
    }
};
AddPatientComponent = __decorate([
    Component({
        selector: 'app-add-patient',
        templateUrl: './add-patient.component.html',
        styleUrls: ['./add-patient.component.css'],
        host: {
            class: 'mainComponentStyle'
        }
    })
], AddPatientComponent);
export { AddPatientComponent };
//# sourceMappingURL=add-patient.component.js.map
