import { __decorate } from "tslib";
import { Injectable, Pipe } from '@angular/core';
let SortPipe = class SortPipe {
    transform(value, sortOrder = 'asc', sortKey) {
        sortOrder = sortOrder && sortOrder.toLowerCase();
        if (!value || (sortOrder !== 'asc' && sortOrder !== 'desc'))
            return value;
        let numberArray = [];
        let stringArray = [];
        if (!sortKey) {
            numberArray = value.filter(item => typeof item === 'number').sort();
            stringArray = value.filter(item => typeof item === 'string').sort();
        }
        else {
            numberArray = value.filter(item => typeof item[sortKey] === 'number').sort((a, b) => a[sortKey] - b[sortKey]);
            stringArray = value
                .filter(item => typeof item[sortKey] === 'string')
                .sort((a, b) => {
                if (a[sortKey] < b[sortKey])
                    return -1;
                else if (a[sortKey] > b[sortKey])
                    return 1;
                else
                    return 0;
            });
        }
        const sorted = numberArray.concat(stringArray);
        return sortOrder === 'asc' ? sorted : sorted.reverse();
    }
};
SortPipe = __decorate([
    Injectable(),
    Pipe({
        name: 'sort',
    })
], SortPipe);
export { SortPipe };
//# sourceMappingURL=sort.pipe.js.map