import { __decorate } from "tslib";
import { Pipe } from '@angular/core';
let SafeUrlPipe = class SafeUrlPipe {
    constructor(sanitizer) {
        this.sanitizer = sanitizer;
    }
    transform(value, ...args) {
        return this.sanitizer.bypassSecurityTrustHtml(value);
    }
};
SafeUrlPipe = __decorate([
    Pipe({
        name: 'safeUrl'
    })
], SafeUrlPipe);
export { SafeUrlPipe };
//# sourceMappingURL=safe-url.pipe.js.map