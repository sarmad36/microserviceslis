import { __decorate } from "tslib";
import { Directive, HostListener, Input } from '@angular/core';
let USNumberDirective = class USNumberDirective {
    constructor(el) {
        this.el = el;
    }
    onKeyDown() {
        this.changeFormat();
    }
    changeFormat() {
        var x = this.el.nativeElement.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
        this.el.nativeElement.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
    }
};
__decorate([
    Input('appUSNumber')
], USNumberDirective.prototype, "elementValue", void 0);
__decorate([
    HostListener('keyup')
], USNumberDirective.prototype, "onKeyDown", null);
USNumberDirective = __decorate([
    Directive({
        selector: '[appUSNumber]'
    })
], USNumberDirective);
export { USNumberDirective };
//# sourceMappingURL=usnumber.directive.js.map