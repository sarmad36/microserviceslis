import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from './includes/dashboard/dashboard.component';
import { ManagePatientsComponent } from './patients/manage-patients/manage-patients.component';
import { AddPatientComponent } from './patients/add-patient/add-patient.component';
const routes = [
    {
        path: 'dashboard',
        component: DashboardComponent,
    },
    {
        path: 'all-patients',
        component: ManagePatientsComponent,
    },
    {
        path: 'add-patient',
        component: AddPatientComponent, pathMatch: 'full'
    },
    {
        path: 'edit-patient/:id',
        component: AddPatientComponent, pathMatch: 'full'
    },
    {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full'
    }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = __decorate([
    NgModule({
        imports: [RouterModule.forRoot(routes)],
        exports: [RouterModule]
    })
], AppRoutingModule);
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map