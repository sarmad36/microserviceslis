import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { throwError } from 'rxjs';
// import { catchError, retry, finalize, tap, map, takeUntil, delay } from 'rxjs/operators';
import { catchError } from "rxjs/operators";
let GlobalApiCallsService = class GlobalApiCallsService {
    constructor(http, sanitizer) {
        this.http = http;
        this.sanitizer = sanitizer;
        this.corsHeaders = new HttpHeaders({
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        });
    }
    getLookUps(url) {
        return this.http.get(url)
            .pipe()
            .toPromise()
            .then(resp => {
            return resp;
        })
            .catch(error => {
            catchError(this.errorHandler);
        });
    }
    errorHandler(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // Get client-side error
            errorMessage = error.error.message;
        }
        else {
            // Get server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        console.log(errorMessage);
        return throwError(errorMessage);
    }
};
GlobalApiCallsService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], GlobalApiCallsService);
export { GlobalApiCallsService };
//# sourceMappingURL=global-api-call.service.js.map