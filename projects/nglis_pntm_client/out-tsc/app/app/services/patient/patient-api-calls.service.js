import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { throwError, Subject } from 'rxjs';
import { catchError, map, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
let PatientApiCallsService = class PatientApiCallsService {
    constructor(http) {
        this.http = http;
        ////////// for seacrch client
        this.baseUrl = 'https://api.cdnjs.com/libraries';
        this.queryUrl = '?search=';
        this.searchClientName = {
            header: {
                uuid: "",
                partnerCode: "",
                userCode: "",
                referenceNumber: "",
                systemCode: "",
                moduleCode: "CLIM",
                functionalityCode: "",
                systemHostAddress: "",
                remoteUserAddress: "",
                dateTime: ""
            },
            data: {
                sortColumn: "name",
                sortingOrder: "asc",
                searchString: ""
            }
        };
        this.subject = new Subject();
        this.corsHeaders = new HttpHeaders({
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        });
    }
    savePatient(url, saveData) {
        // return this.http.post<Patient>(url, saveData, this.corsHeaders)
        return this.http.post(url, saveData)
            .pipe(catchError(this.errorHandler));
    }
    getAllPatient(url, saveData) {
        return this.http.put(url, saveData)
            .pipe(catchError(this.errorHandler));
    }
    getPatientbyID(url, saveData) {
        return this.http.put(url, saveData)
            .pipe(catchError(this.errorHandler));
    }
    updatePatient(url, saveData) {
        return this.http.put(url, saveData)
            .pipe(catchError(this.errorHandler));
    }
    getEligiblePatient() {
    }
    getNonEligiblePatient() {
    }
    selfRegistrationSingle() {
    }
    selfRegistrationBulk() {
    }
    checkEligibility() {
    }
    checkEligibilityMultiple() {
    }
    exportPatients(url, saveData) {
        return this.http.put(url, saveData)
            .pipe(catchError(this.errorHandler));
    }
    importPatients(url, saveData) {
        return this.http.put(url, saveData)
            .pipe(catchError(this.errorHandler));
    }
    getPrimaryPatient() {
    }
    getSecondaryPatient() {
    }
    mergePatient() {
    }
    togglePatientStatus(url, saveData) {
        return this.http.put(url, saveData)
            .pipe(catchError(this.errorHandler));
    }
    searchClientByName(url, saveData) {
        return this.http.post(url, saveData)
            .pipe(catchError(this.errorHandler));
    }
    search(terms) {
        return terms.pipe(debounceTime(400))
            .pipe(distinctUntilChanged())
            .pipe(switchMap(term => this.searchEntries(term)));
    }
    searchEntries(term) {
        // if (term.length >=3 || term.length == 0) {
        this.searchClientName.data.searchString = term;
        const searchURL = environment.API_CLIENT_ENDPOINT + 'searchclientbyname';
        return this.http
            // .get(this.baseUrl + this.queryUrl + term)
            .post(searchURL, this.searchClientName)
            .pipe(map(res => res)).pipe(catchError(this.errorHandler));
        // }
    }
    patientexists(url, saveData) {
        return this.http.put(url, saveData)
            .pipe(catchError(this.errorHandler));
    }
    getClientByName(url, saveData) {
        return this.http.post(url, saveData)
            .pipe(catchError(this.errorHandler));
    }
    getClientByIds(url, saveData) {
        return this.http.post(url, saveData)
            .pipe()
            .toPromise()
            .then(resp => {
            // console.log('resp: ', resp);
            // Set loader false
            return resp;
        })
            .catch(error => {
            catchError(this.errorHandler);
        });
        // return this.http.post(url, saveData)
        // .pipe(
        //   catchError(this.errorHandler)
        // )
    }
    getPatientsByIds(url, saveData) {
        return this.http.post(url, saveData)
            .pipe()
            .toPromise()
            .then(resp => {
            // console.log('resp: ', resp);
            // Set loader false
            return resp;
        })
            .catch(error => {
            catchError(this.errorHandler);
        });
        // return this.http
        // .put(url, saveData)
        // .pipe(
        //   catchError(this.errorHandler)
        // )
    }
    saveMinAccession(url, saveData) {
        return this.http.post(url, saveData, this.corsHeaders)
            .pipe(catchError(this.errorHandler));
    }
    caseByPatientId(url, saveData) {
        return this.http.post(url, saveData, this.corsHeaders)
            .pipe(catchError(this.errorHandler));
    }
    errorHandler(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // Get client-side error
            errorMessage = error.error.message;
        }
        else {
            // Get server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        console.log("serviceError", errorMessage);
        return throwError(errorMessage);
        // return errorMessage;
    }
};
PatientApiCallsService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], PatientApiCallsService);
export { PatientApiCallsService };
//# sourceMappingURL=patient-api-calls.service.js.map