import { __decorate } from "tslib";
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { DataTablesModule } from 'angular-datatables';
import { DashboardComponent } from './includes/dashboard/dashboard.component';
import { FooterComponent } from './includes/footer/footer.component';
import { NavbarComponent } from './includes/navbar/navbar.component';
import { SidebarComponent } from './includes/sidebar/sidebar.component';
import { ManagePatientsComponent } from './patients/manage-patients/manage-patients.component';
import { AddPatientComponent } from './patients/add-patient/add-patient.component';
import { PatientApiCallsService } from './services/patient/patient-api-calls.service';
import { GlobalApiCallsService } from './services/global/global-api-call.service';
import { USNumberDirective } from './directives/usnumber.directive';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { NotifierModule } from 'angular-notifier';
import { SafeUrlPipe } from './pipes/safe-url.pipe';
import { SortPipe } from './pipes/sort.pipe';
/**
 * Custom angular notifier options
 */
const customNotifierOptions = {
    position: {
        horizontal: {
            position: 'left',
            distance: 110
        },
        vertical: {
            position: 'top',
            distance: 100,
            gap: 10
        }
    },
    theme: 'material',
    behaviour: {
        autoHide: 3000,
        onClick: 'hide',
        onMouseover: 'pauseAutoHide',
        showDismissButton: true,
        stacking: 4
    },
    animations: {
        enabled: true,
        show: {
            preset: 'slide',
            speed: 300,
            easing: 'ease'
        },
        hide: {
            preset: 'fade',
            speed: 300,
            easing: 'ease',
            offset: 50
        },
        shift: {
            speed: 300,
            easing: 'ease'
        },
        overlap: 150
    }
};
let AppModule = class AppModule {
};
AppModule = __decorate([
    NgModule({
        declarations: [
            AppComponent,
            DashboardComponent,
            FooterComponent,
            NavbarComponent,
            SidebarComponent,
            ManagePatientsComponent,
            AddPatientComponent,
            USNumberDirective,
            SafeUrlPipe,
        ],
        imports: [
            BrowserModule,
            AppRoutingModule,
            HttpClientModule,
            ReactiveFormsModule,
            FormsModule,
            NgSelectModule,
            DataTablesModule,
            NgxUiLoaderModule,
            NotifierModule.withConfig(customNotifierOptions),
        ],
        providers: [PatientApiCallsService, GlobalApiCallsService, SortPipe],
        bootstrap: [AppComponent]
    })
], AppModule);
export { AppModule };
//# sourceMappingURL=app.module.js.map