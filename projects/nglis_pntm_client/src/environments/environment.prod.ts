export const environment = {
  production: true,
  API_USER_ENDPOINT      : 'http://192.168.2.8:8044/user/',
  API_PARTNER_ENDPOINT   : 'http://192.168.2.8:8044/partner/',
  API_ROLE_ENDPOINT      : 'http://192.168.2.8:8044/role/',
  API_CLIENT_ENDPOINT    : 'http://192.168.2.8:9002/clientapi/',
  API_SPECIMEN_ENDPOINT  : 'http://192.168.2.8:9004/specimenapi/',
  UI_SPECIMEN_ENDPOINT   : 'http://192.168.2.8:8083/',
  API_LOOKUP_ENDPOINT    : 'http://192.168.2.8:8046/'
};
