import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { environment } from '../../../../../../src/environments/environment';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Router, NavigationEnd } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
// import { AppSettings } from '../../../services/_services/app.setting';
// import { HttpRequestsService } from '../../../services/_services/http-requests/http-requests.service';
import { PatientApiCallsService } from '../../services/patient/patient-api-calls.service';
import { GlobalApiCallsService } from '../../services/global/global-api-call.service';

import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import Swal from 'sweetalert2'
declare var $ :any;
import { NotifierService } from 'angular-notifier';
import { concat, Observable, of, Subject, forkJoin } from "rxjs";
import { SortPipe } from "../../pipes/sort.pipe";
import { DataService } from "../../../../../../src/services/data/data.service";
import { filter } from 'rxjs/operators';
import { GlobalySharedModule } from './../../../../../../src/app/sharedmodules/globaly-shared/globaly-shared.module';
import { UrlGuard } from './../../../../../../src/services/gaurd/url.guard'
import * as CryptoJS from 'crypto-js';
import { DatePipe } from '@angular/common';

@Component({
  selector   : 'app-add-patient',
  templateUrl: './add-patient.component.html',
  styleUrls  : ['./add-patient.component.css'],
  host       : {
    class    :'mainComponentStyle'
}
})
export class AddPatientComponent implements OnInit {

  searchTerm$ = new Subject<string>();
  results     : Object;

  immuteable = {
    name: 'Dzon',
    age: 25,
    address: 'Sunny street 34'
  }
  clientsDropdown              : any = []
  allpattypes                  : any = [];
  allBillingypes               : any = [];
  insuranceDropdown            : any = [
    {id: 1, name: 'Fidelis Care NewYork'},
    {id: 2, name: '----------'},
    {id: 3, name: '----------'}
  ]
  allEthnicities               : any = [];
  allRaces                     : any = [];
  allCountry                   : any = [];
  allState                     : any = [];
  allCities                    : any = [];
  public patientForm           : FormGroup;
  submitted                    = false;
  dateOfBirth                  ;
  isInsurance                  = 0;
  pageType                     = 'add';
  mobileNumber                 = null;
  homePhone                    = null;

  public saveRequest           = {
  header:{
      uuid              :"",
      partnerCode       :"",
      userCode          :"",
      referenceNumber   :"",
      systemCode        :"",
      moduleCode        :"",
      functionalityCode: "PNTA-AP",
      systemHostAddress :"",
      remoteUserAddress :"",
      dateTime          :""
    },
    data:{
      accountNumber: "ABC-21",
      patientId    : null,
      clientId: null,
      status: true,
      createdSource: 1,
      patientInsurance: 123423234,
      patientDemographics: {
        patientTypeId: null,
        patientTypeStr: null,
        paymentTypeId: null,
        firstName: null,
        middleName: null,
        lastName: null,
        gender: "",
        dateOfBirth: "",
        ssn: "",
        ethnicityId: null,
        raceId: null,
        address: {
          street: "",
          suiteFloorBuilding: null,
          cityId: null,
          zip: null,
          stateId: null,
          stateOther: "ISB",
          countryId: 1,
          moduleCode: "PNTM",
          createdBy: "sip-C-0011",
          createdTimestamp: "2020-10-15 13:31:01"
        },
        contacts: [{
          contactTypeId: 1,
          value: "+92 345 1234567",
          moduleCode: "PNTM",
          createdBy: "sip-C-0011",
          createdTimestamp: "2020-10-15 13:31:01"
        }

      ],
      createdBy: "sip-C-0011",
      createdTimestamp: "2020-10-15 13:31:01"
    },
    createdBy: "danish",
    createdTimestamp: "2020-10-15 13:31:01",
    userName: "danish"
}

  }

  public patientExistReq = {
  header:{
      uuid               :"",
      partnerCode        :"",
      userCode           :"",
      referenceNumber    :"",
      systemCode         :"",
      moduleCode         :"",
      functionalityCode: "PNTA-AP",
      systemHostAddress  :"",
      remoteUserAddress  :"",
      dateTime           :""
    },
    data:{
      username    :"",
      firstName   :"",
      lastName    :"",
      dateOfBirth :"",
      clientId    :2
    }

  }

  public ssnExistReq = {
  header:{
      uuid               :"",
      partnerCode        :"",
      userCode           :"",
      referenceNumber    :"",
      systemCode         :"",
      moduleCode         :"",
      functionalityCode: "PNTA-AP",
      systemHostAddress  :"",
      remoteUserAddress  :"",
      dateTime           :""
    },
    data:{
      username    :"danish",
      ssn         :""
    }

  }

  public searchClientName = {
    header:{
      uuid               :"",
      partnerCode        :"",
      userCode           :"",
      referenceNumber    :"",
      systemCode         :"",
      moduleCode         :"CLIM",
      functionalityCode: "PNTA-AP",
      systemHostAddress  :"",
      remoteUserAddress  :"",
      dateTime           :""
    },
    data:{
      sortColumn  :"name",
      sortingOrder:"asc",
      searchString:"",
      userCode    : ""
    }
  }
  clientSearchString          ;
  clientLoading               : any;
  clientExist                 = false;
  ssnExist                    = false;

  contactrequestBP             = {
    contactTypeId: 1,
    value: '',
    moduleCode: "",
    createdBy: "",
    createdTimestamp: "2020-10-15 13:31:01"
  }
  contactrequestBPUpdate             = {
    contactTypeId: 1,
    contactId    : 1,
    value: "",
    moduleCode: "",
    createdBy: "",
    createdTimestamp: "2020-10-15 13:31:01"
  }
  selectedPatient        : any = [];
  contactID1   = null;
  contactID2   = null;
  dobToShow    ;
  contactID3   = null;
  emailAddress ;
  update_patientId = null;
  update_patientCode = null;
  patienttActiveToggle;
  maxDate;
  hideBreadCrumb = false;

  swalStyle              : any;
  selectedClientForAdd = null;
  selectedClientForAddAccNumber = null;
  public logedInUserRoles :any = {};
  previousUrl: string = null;
  currentUrl: string = null;
  oldStatus;
  redirectToClient = null;
  patienFromEditCase = null;
  // fName = null;
  // lName = null;
  // ssn = null;
  // streetAdd = null;
  // suiteName = null;
  // home = null;
  // cell = null;
  // emailSH = null;

  // selectedClientForAddDisabled = true;
  constructor(
    private formBuilder  : FormBuilder,
    private http         : HttpClient,
    private router       : Router ,
    private route        : ActivatedRoute,
    private sanitizer    : DomSanitizer,
    // private httpRequest  : HttpRequestsService,
    private ngxLoader    : NgxUiLoaderService,
    private  notifier       : NotifierService,
    private patientnService : PatientApiCallsService,
    private globalService   : GlobalApiCallsService,
    private sortPipe        :SortPipe,
    private sharedData: DataService,
    private rbac         : UrlGuard,
    private encryptDecrypt : GlobalySharedModule,
    private datePipe: DatePipe
  ) {

    // this.rbac.checkROle('PNTA-AP');

    this.notifier        = notifier;
    this.swalStyle       =  Swal.mixin({
      customClass : {
        confirmButton : 'btn btn-success my-swal-success',
        cancelButton  : 'btn btn-danger'
      },
      buttonsStyling  : true
    })
  }

  ngOnInit(): void {
    console.log("sasas",this.encryptDecrypt.decryptString('VUNRRnAyd2hHaVNHNkJHNjk2ZGVMUT09'));

    this.router.events.pipe(filter(event => event instanceof NavigationEnd))
    .subscribe((event: NavigationEnd) => {

      this.previousUrl = this.currentUrl;
      this.currentUrl = event.url;

      if (this.previousUrl != null) {


      if (this.previousUrl.indexOf( "/edit-client/") < 0) {
        // console.log('innnn***',this.previousUrl.indexOf( "/edit-client/"));

        this.sharedData.changeMessage('null');
        this.selectedClientForAdd = null;

      }
      }
      else{
        this.sharedData.changeMessage('null');
        this.selectedClientForAdd = null;

      }

    });
    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
    var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
    var originalText = bts.toString(CryptoJS.enc.Utf8);
    this.logedInUserRoles = JSON.parse(originalText);

    var clientCodeCHeck = JSON.parse(localStorage.getItem('clientCodeForPatient'))
    console.log('-----------***-*-*-*-*-* clientCodeCHeck',clientCodeCHeck);
    if (clientCodeCHeck != null) {
      this.selectedClientForAddAccNumber = clientCodeCHeck;
      this.redirectToClient = clientCodeCHeck;
    }
    else{
      this.selectedClientForAddAccNumber = null;
      this.redirectToClient = null;
    }

    var dtToday = new Date();

    var m = dtToday.getMonth()+1;
    var d = dtToday.getDate();
    var year = dtToday.getFullYear();
    var month;
    var day;
    if(m < 10)
    month = '0' + m.toString();
    else
    month = m
    if(d < 10)
    day = '0' + d.toString();
    else
    day = d

    var maxDate = year + '-' + month + '-' + day;
    this.maxDate = maxDate;
    this.ngxLoader.start();
    this.getLookups().then(lookupsLoaded => {

    this.ngxLoader.stop();
    });

    this.patientnService.search(this.searchTerm$)
      .subscribe(results => {
        console.log("results",results);
        this.clientsDropdown = results['data'];
        this.clientLoading = false;
      },
        error => {
          this.ngxLoader.stop();
          this.notifier.notify( "error", "Error while fetching clients backend connect aborted" );
          this.clientLoading = false;
          return;
          // console.log("error",error);
        });

    this.patientForm         = this.formBuilder.group({
      client                 : ['', [Validators.required]],
      ssn                    : [''],
      patientID              : [{value: '', disabled: true}],
      lastName               : ['', [Validators.required, Validators.maxLength(50)]],
      firstName              : ['', [Validators.required, Validators.maxLength(50)]],
      middleName             : [''],
      // middleName             : ['', [Validators.pattern(/^((?![\s])[^\$%\^&*()_\-=\+@#!~`{}\[\]|\\\/:;\"\'\?\.,<>]+)$/)]],
      dob                    : ['', [Validators.required]],
      gender                 : ['', [Validators.required]],
      email                  : ['', [Validators.email]],
      mobile                 : ['', [Validators.maxLength(50)]],
      phone                  : ['', [Validators.maxLength(50)]],
      patientType            : ['', [Validators.required]],
      billingInfo            : ['', [Validators.required]],
      insuranceRadio         : [{value: '0', disabled: true}, [Validators.required]],
      insuranceName          : [{value: '', disabled: true}],
      ethnicity              : ['', [Validators.required]],
      race                   : ['', [Validators.required]],
      insuranceID            : [{value: '', disabled: true}],
      street                 : ['', [Validators.maxLength(100)]],
      suite                  : ['', [Validators.maxLength(100)]],
      country                : [''],
      state                  : [''],
      city                   : [''],
      zip                    : ['', [Validators.maxLength(10)]],
      activedeactive         : ['']
    })
    this.route.params.subscribe(params => {
      console.log('params',params);
      if (typeof params['id'] != "undefined") {
        // var bytes  = CryptoJS.AES.decrypt(params['id'], 'nglis');
        // var originalText = bytes.toString(CryptoJS.enc.Utf8);
        // console.log('originalText',originalText)
        var coverted = params['id'].replace('xsarm','+').replace('ydan','/').replace('zhar','=');
        var bytes  = CryptoJS.AES.decrypt(coverted, 'nglis');
        var originalText = bytes.toString(CryptoJS.enc.Utf8).toString();
        var test = originalText.split(';')
        console.log("test--0-",test);



      // var bytes  = CryptoJS.AES.decrypt(params['id'], 'nglis');
      // var originalText = bytes.toString(CryptoJS.enc.Utf8);
      // console.log('originalText',originalText)
      // var test = originalText.split(';')

      // console.log('this.router',this.router);
      // console.log('this.router',this.router);
      // console.log("this.route.snapshot.paramMap.get('param1')",this.route.snapshot.paramMap.get('from'));

      if (typeof test[1] == "undefined" || test[1] != 6) {
        this.pageType = 'add';
         if(this.logedInUserRoles['allowedRoles'].indexOf("SPCA-CD") == -1 && this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UC") == -1 && this.logedInUserRoles['allowedRoles'].indexOf("SPCA-VC") == -1){
          this.rbac.checkROle('PNTA-AP');
        }

        this.sharedData.currentClientId.subscribe(client => {
          // this.patientForm.reset();
          console.log("selectedClienttId---",client);
          if (client != 'null') {
            localStorage.setItem("ptatientLoadedFromClient","true");
            // console.log("client not null",client);
            this.selectedClientForAdd = client;
            // this.selectedClientForAddDisabled = true;
            this.patientForm.get('client').disable();
            this.selectClientForAddSpecificToClient(client)

          }
        })

        // this.sharedData.clientCodeForPatient.subscribe(clientCode => {
        //   console.log("clientCode---",clientCode);
        //   if (clientCode != 'null') {
        //     this.selectedClientForAddAccNumber = clientCode;
        //     this.redirectToClient = clientCode;
        //
        //   }
        // })
        this.sharedData.currentPatientId.subscribe(patient => {
          console.log("selectedPatientId---",patient);
          this.patientForm.reset();
          if (patient != 'null') {
            console.log('-----------',patient);
            // this.encryptDecrypt.encryptString(patient)
            this.patienFromEditCase = patient;

            this.hideBreadCrumb = true;
            this.pageType = 'edit';
             if(this.logedInUserRoles['allowedRoles'].indexOf("SPCA-CD") == -1 && this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UC") == -1 && this.logedInUserRoles['allowedRoles'].indexOf("SPCA-VC") == -1){
              this.rbac.checkROle('PNTA-UP');
             }
            let getPatientReq  = {}

            var selectURL = environment.API_PATIENT_ENDPOINT + 'patientbyid';
            getPatientReq={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"",functionalityCode: "PNTA-MP",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{username:"danish",patientId:patient}}
            this.getSelectedRecord(selectURL,getPatientReq).then(selectedPatient => {
              // console.log("Selected Case response",selectedPatient);
              // this.chec();
              console.log("selectedPatient",this.selectedPatient);


            });
          }
        })
      }
      else{
        // var bytes  = CryptoJS.AES.decrypt(params['id'], 'nglis');
        // var originalText = bytes.toString(CryptoJS.enc.Utf8);
        // console.log('originalText',originalText)
        // var test = originalText.split(';')
        //
        this.pageType = 'edit';
         if(this.logedInUserRoles['allowedRoles'].indexOf("SPCA-CD") == -1 && this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UC") == -1 && this.logedInUserRoles['allowedRoles'].indexOf("SPCA-VC") == -1 ){
          this.rbac.checkROle('PNTA-UP');
        }
        let getPatientReq  = {}

        var selectURL = environment.API_PATIENT_ENDPOINT + 'patientbyid';
        getPatientReq={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"",functionalityCode: "PNTA-MP",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{username:"danish",patientId:test[0]}}
        // getPatientReq={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"",functionalityCode: "PNTA-AP",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{username:"danish",patientId:params['id']}}
        this.getSelectedRecord(selectURL,getPatientReq).then(selectedPatient => {
          // console.log("Selected Case response",selectedPatient);
          // this.chec();
          console.log("selectedPatient",this.selectedPatient);


        });
      }
      console.log('pageType:  ',this.pageType);
    }
    else{
      this.pageType = 'add';
       if(this.logedInUserRoles['allowedRoles'].indexOf("SPCA-CD") == -1 && this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UC") == -1 && this.logedInUserRoles['allowedRoles'].indexOf("SPCA-VC") == -1){
        this.rbac.checkROle('PNTA-AP');
      }


      this.sharedData.currentClientId.subscribe(client => {
        // this.patientForm.reset();
        console.log("selectedClienttId---",client);
        if (client != 'null') {
          localStorage.setItem("ptatientLoadedFromClient","true");
          // console.log("client not null",client);
          this.selectedClientForAdd = client;
          // this.selectedClientForAddDisabled = true;
          this.patientForm.get('client').disable();
          this.selectClientForAddSpecificToClient(client)

        }
      })

      // this.sharedData.clientCodeForPatient.subscribe(clientCode => {
      //   console.log("clientCode---",clientCode);
      //   if (clientCode != 'null') {
      //     this.selectedClientForAddAccNumber = clientCode;
      //     this.redirectToClient = clientCode;
      //
      //   }
      // })
      if (this.logedInUserRoles['allowedRoles'].indexOf("PNTA-AP") == -1) {
        if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-CD") != -1 || this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UC") != -1 || this.logedInUserRoles['allowedRoles'].indexOf("SPCA-VC") != -1) {
          // this.notifier.notify('warning','Unauthorized');
          $('.hideScreen').css('visibility','hidden');
          $('#showAuthError').css('display','inline-block');
        }
        else{
          $('.hideScreen').css('visibility','hidden');
          $('#showAuthError').css('display','inline-block');
          // this.notifier.notify('warning','Unauthorized');
          // this.router.navigateByUrl('/');
        }


      }
      this.sharedData.currentPatientId.subscribe(patient => {
        console.log("selectedPatientId---",patient);
        this.patientForm.reset();
        if (patient != 'null') {
          console.log('-----------',patient);
          this.patienFromEditCase = patient;

          this.hideBreadCrumb = true;
          this.pageType = 'edit';
           if(this.logedInUserRoles['allowedRoles'].indexOf("SPCA-CD") == -1 && this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UC") == -1 && this.logedInUserRoles['allowedRoles'].indexOf("SPCA-VC") == -1){
            this.rbac.checkROle('PNTA-UP');
           }
          let getPatientReq  = {}

          var selectURL = environment.API_PATIENT_ENDPOINT + 'patientbyid';
          getPatientReq={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"",functionalityCode: "PNTA-MP",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{username:"danish",patientId:patient}}
          this.getSelectedRecord(selectURL,getPatientReq).then(selectedPatient => {
            // console.log("Selected Case response",selectedPatient);
            // this.chec();
            console.log("selectedPatient",this.selectedPatient);


          });
        }
      })
    }
    })
  }
  ngAfterViewInit(): void {
  }
  check(e){
    console.log("date",e.target.value);




  }

  selectClientForAddSpecificToClient(clientId){
    var getCLurl    = environment.API_CLIENT_ENDPOINT + "clientidsnames";
    var reqTemplate = {
      header:{
        uuid               :"",
        partnerCode        :this.logedInUserRoles.partnerCode,
        userCode           :this.logedInUserRoles.userCode,
        referenceNumber    :"",
        systemCode         :"",
        moduleCode         :"CLIM",
        functionalityCode: "CLTA-VC",
        systemHostAddress  :"",
        remoteUserAddress  :"",
        dateTime           :""
      },
      data:{
        clientIds:[clientId]
      }
    }
    // var reqTemplate = {clientIds:[resp['data'].clientId]}
    this.patientnService.getClientByName(getCLurl,reqTemplate).subscribe(selectedClient => {
      console.log("selectedClient",selectedClient);
      if (selectedClient['data']['length'] != 0) {

        this.clientsDropdown    = selectedClient['data'];
        this.clientSearchString = selectedClient['data'][0].clientId;
        this.ngxLoader.stop();
      }
      else{
        this.notifier.notify('error','no client found');
        this.ngxLoader.stop();
      }



    },
      error => {
        this.ngxLoader.stop();
        this.notifier.notify( "error", "Error while fetching clients backend connect aborted" );
        return;
        // console.log("error",error);
      });
  }

  zeroPadded(val) {
    if (val >= 10)
      return val;
    else
      return '0' + val;
  }

  getSelectedRecord(url,data) {
    this.ngxLoader.start();
      return new Promise((resolve, reject) => {
        this.patientnService.getPatientbyID(url,data).subscribe( resp => {
          if (typeof resp['result'] != 'undefined') {
            if (typeof resp['result']['description'] != 'undefined') {
              if (resp['result']['description'].indexOf('UnAuthorized') != -1) {
                this.notifier.notify('warning',resp['result']['description'])
                this.router.navigateByUrl('/page-restrict');
              }
            }
          }
          this.getAge(resp['data'].patientDemographics.dateOfBirth,'getrecord')
          var ar = resp['data'].patientDemographics.dateOfBirth.split('-');
          console.log("ar",ar);
          this.dobToShow = ar[2]+"-"+ar[0]+"-"+ar[1];
          console.log("dobToShow",this.dobToShow);

          if (typeof resp['data'].patientDemographics.contacts[0] != "undefined") {
            if (resp['data'].patientDemographics.contacts[0].contactTypeId == 1) {
              this.contactID1   =  resp['data'].patientDemographics.contacts[0].contactId;
              this.mobileNumber =  this.encryptDecrypt.decryptString(resp['data'].patientDemographics.contacts[0].value);
            }
            else if(resp['data'].patientDemographics.contacts[0].contactTypeId == 4){
              this.contactID2 = resp['data'].patientDemographics.contacts[0].contactId;
              this.homePhone  =  this.encryptDecrypt.decryptString(resp['data'].patientDemographics.contacts[0].value)
            }
            else if(resp['data'].patientDemographics.contacts[0].contactTypeId == 2){
              this.contactID3   = resp['data'].patientDemographics.contacts[0].contactId;
              this.emailAddress =  this.encryptDecrypt.decryptString(resp['data'].patientDemographics.contacts[0].value)
            }

          }
          if(typeof resp['data'].patientDemographics.contacts[1] != "undefined"){
            if (resp['data'].patientDemographics.contacts[1].contactTypeId == 1) {
              this.contactID1   = resp['data'].patientDemographics.contacts[1].contactId;
              this.mobileNumber =  this.encryptDecrypt.decryptString(resp['data'].patientDemographics.contacts[1].value)
            }
            else if(resp['data'].patientDemographics.contacts[1].contactTypeId == 4){
              this.contactID2 = resp['data'].patientDemographics.contacts[1].contactId;
              this.homePhone  =  this.encryptDecrypt.decryptString(resp['data'].patientDemographics.contacts[1].value)
            }
            else if(resp['data'].patientDemographics.contacts[1].contactTypeId == 2){
              this.contactID3   = resp['data'].patientDemographics.contacts[1].contactId;
              this.emailAddress =  this.encryptDecrypt.decryptString(resp['data'].patientDemographics.contacts[1].value)
            }

          }
          if(typeof resp['data'].patientDemographics.contacts[2] != "undefined"){
            if (resp['data'].patientDemographics.contacts[2].contactTypeId == 1) {
              this.contactID1   = resp['data'].patientDemographics.contacts[2].contactId;
              this.mobileNumber =  this.encryptDecrypt.decryptString(resp['data'].patientDemographics.contacts[2].value)
            }
            else if(resp['data'].patientDemographics.contacts[2].contactTypeId == 4){
              this.contactID2 = resp['data'].patientDemographics.contacts[2].contactId;
              this.homePhone  =  this.encryptDecrypt.decryptString(resp['data'].patientDemographics.contacts[2].value)
            }
            else if(resp['data'].patientDemographics.contacts[2].contactTypeId == 2){
              this.contactID3   = resp['data'].patientDemographics.contacts[2].contactId;
              this.emailAddress =  this.encryptDecrypt.decryptString(resp['data'].patientDemographics.contacts[2].value)
            }

          }
          this.update_patientId = resp['data'].patientId;
          this.update_patientCode = this.encryptDecrypt.decryptString(resp['data'].patientCode);



          if (typeof resp['data'].patientDemographics.address == "undefined") {
            resp['data'].patientDemographics.address = {
              street: "",
              suiteFloorBuilding: "",
              cityId: "",
              zip: "",
              stateId: null,
              stateOther: "",
              countryId: "",
              moduleCode: "PNTM",
              createdBy: "sip-C-0011",
              createdTimestamp: "2020-10-15 13:31:01"
            }
          }
          else{
            if (typeof resp['data'].patientDemographics.address.street  == 'undefined' || resp['data'].patientDemographics.address.street == '')
            { resp['data'].patientDemographics.address.street=''}
            else if (typeof resp['data'].patientDemographics.address.street  != 'undefined' && resp['data'].patientDemographics.address.street  != null) {
              resp['data'].patientDemographics.address.street = this.encryptDecrypt.decryptString(resp['data'].patientDemographics.address.street)
            }
            if (typeof resp['data'].patientDemographics.address.suiteFloorBuilding == 'undefined' || resp['data'].patientDemographics.address.suiteFloorBuilding == '')
            { resp['data'].patientDemographics.address.suiteFloorBuilding=''}
            else if (typeof resp['data'].patientDemographics.address.suiteFloorBuilding != 'undefined' && resp['data'].patientDemographics.address.suiteFloorBuilding != null){
              // console.log("resp['data'].patientDemographics.address.suiteFloorBuilding",resp['data'].patientDemographics.address.suiteFloorBuilding);

              resp['data'].patientDemographics.address.suiteFloorBuilding = this.encryptDecrypt.decryptString(resp['data'].patientDemographics.address.suiteFloorBuilding)
            }
            if (typeof resp['data'].patientDemographics.address.cityId == 'undefined' || resp['data'].patientDemographics.address.cityId == '')
            { resp['data'].patientDemographics.address.cityId=''}
            if (typeof resp['data'].patientDemographics.address.zip  == 'undefined' || resp['data'].patientDemographics.address.zip == '')
            { resp['data'].patientDemographics.address.zip=''}
            if (typeof resp['data'].patientDemographics.address.stateId == 'undefined' || resp['data'].patientDemographics.address.stateId == '')
            { resp['data'].patientDemographics.address.stateId=''}
            if (typeof resp['data'].patientDemographics.address.countryId == 'undefined' || resp['data'].patientDemographics.address.countryId == '')
            { resp['data'].patientDemographics.address.countryId=''}
          }

          if (typeof resp['data'].patientDemographics.middleName == 'undefined') {
            resp['data'].patientDemographics.middleName = '';
          }

          if (typeof resp['data'].patientDemographics.ssn == 'undefined') {
            resp['data'].patientDemographics.ssn = '';
          }
          else if (typeof resp['data'].patientDemographics.ssn != 'undefined' && resp['data'].patientDemographics.ssn != '' ) {
            resp['data'].patientDemographics.ssn = this.encryptDecrypt.decryptString(resp['data'].patientDemographics.ssn)
          }


          resp['data'].patientDemographics.firstName = this.encryptDecrypt.decryptString(resp['data'].patientDemographics.firstName)
          resp['data'].patientDemographics.lastName = this.encryptDecrypt.decryptString(resp['data'].patientDemographics.lastName)


          var getCLurl    = environment.API_CLIENT_ENDPOINT + "clientidsnames";
          var reqTemplate = {
            header:{
              uuid               :"",
              partnerCode        :this.logedInUserRoles.partnerCode,
              userCode           :this.logedInUserRoles.userCode,
              referenceNumber    :"",
              systemCode         :"",
              moduleCode         :"CLIM",
              functionalityCode: "CLTA-VC",
              systemHostAddress  :"",
              remoteUserAddress  :"",
              dateTime           :""
            },
            data:{
              clientIds:[resp['data'].clientId]
            }
          }
          // var reqTemplate = {clientIds:[resp['data'].clientId]}
          this.patientnService.getClientByName(getCLurl,reqTemplate).subscribe(selectedClient => {
            console.log("selectedClient",selectedClient);
            if (selectedClient['data']['length'] != 0) {

              this.clientsDropdown    = selectedClient['data'];
              this.clientSearchString = selectedClient['data'][0].clientId;
              this.ngxLoader.stop();
            }
            else{
              this.notifier.notify('error','no client found');
              this.ngxLoader.stop();
            }



          },
            error => {
              this.ngxLoader.stop();
              this.notifier.notify( "error", "Error while fetching clients backend connect aborted" );
              return;
              // console.log("error",error);
            });
            console.log("resp['data']",resp['data']);
            console.log("resp['data'].patientDemographics.address.cityId",resp['data'].patientDemographics.address.cityId);

            this.selectedPatient      = resp['data'];
            this.saveRequest.data     = resp['data'];
            this.oldStatus            = resp['data']['status']
            this.changeState(resp['data'].patientDemographics.address.cityId);
          //
          // this.changeState();
          // console.log("this.saveRequest",this.saveRequest);

          // return true;
          // this.ngxLoader.stop();
          resolve();
        },
        error => {
          // return false;
          // console.log("error: ",error);
          this.ngxLoader.stop();
          // this.notifier.notify( "error", "Error while loading case number search results")
          reject();
        });
          // resolve();
      });
  }

  get f() { return this.patientForm.controls; }
  addPatient(){

    this.ngxLoader.start();
    this.submitted = true;
    if (this.patientForm.invalid) {
      this.ngxLoader.stop();
      // alert('form invalid');
      this.scrollToError();
      return;
    }
    if (this.pageType == 'add') {
      this.savePatientRequest()

    }
    else if(this.pageType == 'edit'){
      this.updatePatient();
    }
    else{
      this.ngxLoader.start();
    }


  }
  savePatientRequest(){
     var contactholder = [];
     var myDate = new Date();
     var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy');
     var currentTimeStamp = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');

     this.saveRequest.data.patientDemographics.contacts = [];


     if(this.mobileNumber != null){
       var x = this.mobileNumber.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
       this.mobileNumber = !x[2] ? x[1] : '(' + x[1] + ')' + x[2] + (x[3] ? '-' + x[3] : '');
       var contactBP              = {...this.contactrequestBP};
       contactBP.contactTypeId    = 1;
       contactBP.value            = this.encryptDecrypt.encryptString(this.mobileNumber);
       contactBP.moduleCode       = "PNTM";
       contactBP.createdBy        = this.logedInUserRoles.userCode;
       contactBP.createdTimestamp = currentTimeStamp;
       contactholder.push(contactBP);
     }
     if(this.homePhone != null){
       var x = this.homePhone.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
       this.homePhone = !x[2] ? x[1] : '(' + x[1] + ')' + x[2] + (x[3] ? '-' + x[3] : '');
       var contactBP              = {...this.contactrequestBP};
       contactBP.contactTypeId    = 4;
       contactBP.value            = this.encryptDecrypt.encryptString(this.homePhone);
       contactBP.moduleCode       = "PNTM";
       contactBP.createdBy        = this.logedInUserRoles.userCode;
       contactBP.createdTimestamp = currentTimeStamp;
       contactholder.push(contactBP);

     }
     if(this.emailAddress != null){
       var contactBP              = {...this.contactrequestBP};
       contactBP.contactTypeId    = 2;
       contactBP.value            = this.encryptDecrypt.encryptString(this.emailAddress);
       contactBP.moduleCode       = "PNTM";
       contactBP.createdBy        = this.logedInUserRoles.userCode;
       contactBP.createdTimestamp = currentTimeStamp;
       contactholder.push(contactBP);

     }
     if ((this.mobileNumber == null || this.mobileNumber == '') && (this.homePhone == null || this.homePhone == '')) {
       contactholder = [];
     }
     this.saveRequest.data.patientDemographics.contacts = contactholder;
     this.saveRequest.header.partnerCode = this.logedInUserRoles.partnerCode;
     this.saveRequest.header.userCode = this.logedInUserRoles.userCode;
     this.saveRequest.header.functionalityCode = "PNTA-AP";
     // this.saveRequest.data.patientDemographics.address.street = this.encryptDecrypt.encryptString(this.saveRequest.data.patientDemographics.address.street);
     // this.saveRequest.data.patientDemographics.address.suiteFloorBuilding = this.encryptDecrypt.encryptString(this.saveRequest.data.patientDemographics.address.suiteFloorBuilding);
     this.saveRequest.data.patientDemographics.lastName = this.encryptDecrypt.encryptString(this.saveRequest.data.patientDemographics.lastName);
     this.saveRequest.data.patientDemographics.firstName = this.encryptDecrypt.encryptString(this.saveRequest.data.patientDemographics.firstName);
     if (this.saveRequest.data.patientDemographics.ssn != null && this.saveRequest.data.patientDemographics.ssn != '') {
       this.saveRequest.data.patientDemographics.ssn = this.encryptDecrypt.encryptString(this.saveRequest.data.patientDemographics.ssn);
     }
     if (this.saveRequest.data.patientDemographics.address.street != null && this.saveRequest.data.patientDemographics.address.street != '') {
       this.saveRequest.data.patientDemographics.address.street = this.encryptDecrypt.encryptString(this.saveRequest.data.patientDemographics.address.street);
     }
     if (this.saveRequest.data.patientDemographics.address.suiteFloorBuilding != null && this.saveRequest.data.patientDemographics.address.suiteFloorBuilding != '') {
       this.saveRequest.data.patientDemographics.address.suiteFloorBuilding = this.encryptDecrypt.encryptString(this.saveRequest.data.patientDemographics.address.suiteFloorBuilding);
     }
     this.saveRequest.data.patientDemographics.address.createdBy        = this.logedInUserRoles.userCode;
     this.saveRequest.data.patientDemographics.createdBy                = this.logedInUserRoles.userCode;
     this.saveRequest.data.createdBy                                    = this.logedInUserRoles.userCode;
     this.saveRequest.data.clientId = this.clientSearchString;

     // this.saveRequest.data.patientDemographics.ssn = this.encryptDecrypt.encryptString(this.saveRequest.data.patientDemographics.ssn);

     console.log("saveRequest",this.saveRequest);
    //  return0.2333333333333333333333333333323
     const saveURL    = environment.API_PATIENT_ENDPOINT + 'save';
     this.patientnService.savePatient(saveURL,this.saveRequest).subscribe(resp=>{
       console.log("Save Response",resp);
       this.saveRequest.data.patientDemographics.lastName = this.encryptDecrypt.decryptString(this.saveRequest.data.patientDemographics.lastName);
       this.saveRequest.data.patientDemographics.firstName = this.encryptDecrypt.decryptString(this.saveRequest.data.patientDemographics.firstName);
       if (this.saveRequest.data.patientDemographics.ssn != null && this.saveRequest.data.patientDemographics.ssn != '') {
         this.saveRequest.data.patientDemographics.ssn = this.encryptDecrypt.decryptString(this.saveRequest.data.patientDemographics.ssn);
       }
       if (this.saveRequest.data.patientDemographics.address.street != null && this.saveRequest.data.patientDemographics.address.street != '') {
         this.saveRequest.data.patientDemographics.address.street = this.encryptDecrypt.decryptString(this.saveRequest.data.patientDemographics.address.street);
       }
       if (this.saveRequest.data.patientDemographics.address.suiteFloorBuilding != null && this.saveRequest.data.patientDemographics.address.suiteFloorBuilding != '') {
         this.saveRequest.data.patientDemographics.address.suiteFloorBuilding = this.encryptDecrypt.decryptString(this.saveRequest.data.patientDemographics.address.suiteFloorBuilding);
       }
       if (resp['statusCode'] == "Internal Server Error") {
         // alert('No Physician Associated to this Client')
         this.ngxLoader.stop();
         this.notifier.notify("error","No response from server please check your connection")
         return;
       }
       if (resp['result'].codeType == "S") {
         // this.notifier.getConfig().behaviour.autoHide = 8000;
         this.notifier.notify( "success", "Patient saved Successfully");
         this.submitted = false;
         this.ngxLoader.stop();

         if (this.selectedClientForAddAccNumber != null) {
           // localStorage.removeItem('userRolesInfo');
           localStorage.setItem("backFromPatientToClient",JSON.stringify(this.clientSearchString))
           var a = this.selectedClientForAddAccNumber+';3'
           // var ciphertext = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
           var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
           var bx = ax.replace('+','xsarm');
           var cx = bx.replace('/','ydan');
           var ciphertext = cx.replace('=','zhar');
           this.selectedClientForAddAccNumber = null;
           // this.router.navigateByUrl('/edit-client/'+ciphertext)
           this.router.navigate(['edit-client', ciphertext]);
           // this.router.navigateByUrl('/edit-client/'+this.selectedClientForAddAccNumber+';from='+3)
         }
         else{
           this.router.navigate(['all-patients']);
         }


       }
       else{
         this.saveRequest.data.patientDemographics.lastName = this.encryptDecrypt.decryptString(this.saveRequest.data.patientDemographics.lastName);
         this.saveRequest.data.patientDemographics.firstName = this.encryptDecrypt.decryptString(this.saveRequest.data.patientDemographics.firstName);
         if (this.saveRequest.data.patientDemographics.ssn != null && this.saveRequest.data.patientDemographics.ssn != '') {
           this.saveRequest.data.patientDemographics.ssn = this.encryptDecrypt.decryptString(this.saveRequest.data.patientDemographics.ssn);
         }
         if (this.saveRequest.data.patientDemographics.address.street != null && this.saveRequest.data.patientDemographics.address.street != '') {
           this.saveRequest.data.patientDemographics.address.street = this.encryptDecrypt.decryptString(this.saveRequest.data.patientDemographics.address.street);
         }
         if (this.saveRequest.data.patientDemographics.address.suiteFloorBuilding != null && this.saveRequest.data.patientDemographics.address.suiteFloorBuilding != '') {
           this.saveRequest.data.patientDemographics.address.suiteFloorBuilding = this.encryptDecrypt.decryptString(this.saveRequest.data.patientDemographics.address.suiteFloorBuilding);
         }
         this.notifier.notify( "error", "Error while saving patient");
         this.submitted = false;
         this.ngxLoader.stop();

         return;
       }

     },
     error=>{
       this.saveRequest.data.patientDemographics.lastName = this.encryptDecrypt.decryptString(this.saveRequest.data.patientDemographics.lastName);
       this.saveRequest.data.patientDemographics.firstName = this.encryptDecrypt.decryptString(this.saveRequest.data.patientDemographics.firstName);
       if (this.saveRequest.data.patientDemographics.ssn != null && this.saveRequest.data.patientDemographics.ssn != '') {
         this.saveRequest.data.patientDemographics.ssn = this.encryptDecrypt.decryptString(this.saveRequest.data.patientDemographics.ssn);
       }
       if (this.saveRequest.data.patientDemographics.address.street != null && this.saveRequest.data.patientDemographics.address.street != '') {
         this.saveRequest.data.patientDemographics.address.street = this.encryptDecrypt.decryptString(this.saveRequest.data.patientDemographics.address.street);
       }
       if (this.saveRequest.data.patientDemographics.address.suiteFloorBuilding != null && this.saveRequest.data.patientDemographics.address.suiteFloorBuilding != '') {
         this.saveRequest.data.patientDemographics.address.suiteFloorBuilding = this.encryptDecrypt.decryptString(this.saveRequest.data.patientDemographics.address.suiteFloorBuilding);
       }
       this.ngxLoader.stop();
       this.notifier.notify( "error", "Error while checking client existance backend connect aborted" );
     })
   }

   updatePatient(){
      var contactholder = [];
      var myDate = new Date();
      var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy');
      var currentTimeStamp = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');

      this.saveRequest.data.patientDemographics.contacts = [];


      if(this.mobileNumber != null){
        var x = this.mobileNumber.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
        this.mobileNumber = !x[2] ? x[1] : '(' + x[1] + ')' + x[2] + (x[3] ? '-' + x[3] : '');

        var contactBP              = {...this.contactrequestBPUpdate};
        contactBP.contactTypeId    = 1;
        if (typeof $('#mobile-number').attr('contactID-value') == "undefined") {
          contactBP.contactId      = null
        }
        else{
          contactBP.contactId        = $('#mobile-number').attr('contactID-value');
        }
        contactBP.value            = this.encryptDecrypt.encryptString(this.mobileNumber);
        contactBP.moduleCode       = "PNTM";
        contactBP.createdBy        = this.logedInUserRoles.userCode;
        contactBP.createdTimestamp = currentTimeStamp;
        contactholder.push(contactBP);
      }
      if(this.homePhone != null){
        var x = this.homePhone.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
        this.homePhone = !x[2] ? x[1] : '(' + x[1] + ')' + x[2] + (x[3] ? '-' + x[3] : '');
        var contactBP              = {...this.contactrequestBPUpdate};
        contactBP.contactTypeId    = 4;
        if (typeof $('#home-number').attr('contactID-value') == "undefined") {
          contactBP.contactId      = null
        }
        else{
          contactBP.contactId        = $('#home-number').attr('contactID-value');
        }
        contactBP.value            = this.encryptDecrypt.encryptString(this.homePhone);
        contactBP.moduleCode       = "PNTM";
        contactBP.createdBy        = this.logedInUserRoles.userCode;
        contactBP.createdTimestamp = currentTimeStamp;
        contactholder.push(contactBP);

      }
      if(this.emailAddress != null){
        var contactBP              = {...this.contactrequestBPUpdate};
        contactBP.contactTypeId    = 2;
        if (typeof $('#email-address').attr('contactID-value') == "undefined") {
          contactBP.contactId      = null
        }
        else{
          contactBP.contactId        = $('#email-address').attr('contactID-value');
        }
        contactBP.value            = this.encryptDecrypt.encryptString(this.emailAddress);
        contactBP.moduleCode       = "PNTM";
        contactBP.createdBy        = this.logedInUserRoles.userCode;
        contactBP.createdTimestamp = currentTimeStamp;
        contactholder.push(contactBP);

      }
      if ((this.mobileNumber == null || this.mobileNumber == '') && (this.homePhone == null || this.homePhone == '')) {
        contactholder = [];
      }
      this.saveRequest.data.patientDemographics.address.street = this.encryptDecrypt.encryptString(this.saveRequest.data.patientDemographics.address.street);
      this.saveRequest.data.patientDemographics.address.suiteFloorBuilding = this.encryptDecrypt.encryptString(this.saveRequest.data.patientDemographics.address.suiteFloorBuilding);
      this.saveRequest.data.patientDemographics.lastName = this.encryptDecrypt.encryptString(this.saveRequest.data.patientDemographics.lastName);
      this.saveRequest.data.patientDemographics.firstName = this.encryptDecrypt.encryptString(this.saveRequest.data.patientDemographics.firstName);
      this.saveRequest.data.patientDemographics.ssn = this.encryptDecrypt.encryptString(this.saveRequest.data.patientDemographics.ssn);

      this.saveRequest.data.patientDemographics.contacts = contactholder;
      this.saveRequest.data.patientId = this.update_patientId;
      this.saveRequest.header.partnerCode = this.logedInUserRoles.partnerCode;
      this.saveRequest.header.userCode = this.logedInUserRoles.userCode;
      this.saveRequest.header.functionalityCode = "PNTA-UP";

      this.saveRequest.data.patientDemographics.address.createdBy        = this.logedInUserRoles.userCode;
      this.saveRequest.data.patientDemographics.createdBy                = this.logedInUserRoles.userCode;
      this.saveRequest.data.createdBy                                    = this.logedInUserRoles.userCode;
      this.saveRequest.data.patientDemographics.address['updatedBy']     = this.logedInUserRoles.userCode;


      console.log("saveRequest",this.saveRequest);
      const saveURL    = environment.API_PATIENT_ENDPOINT + 'update';
      this.patientnService.updatePatient(saveURL,this.saveRequest).subscribe(resp=>{
        console.log("Save Response",resp);
        if (typeof resp['result'] == 'undefined') {
          this.notifier.notify( "error", "Error while updating patient");
          this.submitted = false;
          this.ngxLoader.stop();

          return;
        }
        if (resp['result'].codeType == "S") {
          this.ngxLoader.stop();
          // this.notifier.getConfig().behaviour.autoHide = 8000;
          this.notifier.notify("success", "Patient updated Successfully");
          this.submitted = false;
          localStorage.removeItem('clientCodeForPatient');


          setTimeout(()=>{
            // localStorage.removeItem('clientCodeForPatient');
            if (this.selectedClientForAddAccNumber != null) {
              // this.selectedClientForAddAccNumber = null;
              // this.clientSearchString.clientId;
              localStorage.setItem("backFromPatientToClient",JSON.stringify(this.clientSearchString))
              var a = this.selectedClientForAddAccNumber+';3'
              // var ciphertext = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
              var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
              var bx = ax.replace('+','xsarm');
              var cx = bx.replace('/','ydan');
              var ciphertext = cx.replace('=','zhar');
              this.selectedClientForAddAccNumber = null;
              // console.log("localStorage.getItem('clientCodeForPatient')",localStorage.getItem('clientCodeForPatient'));
              // console.log("this.selectedClientForAddAccNumber",this.selectedClientForAddAccNumber);

              this.router.navigate(['edit-client', ciphertext]);
            }
            else{
              this.router.navigate(['all-patients']);
            }
          }, 1000)




          // var selectURL = environment.API_PATIENT_ENDPOINT + 'patientbyid';
          // var getPatientReq={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"",functionalityCode: "PNTA-AP",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{username:"danish",patientId:resp['data'].patientId}}
          // this.getSelectedRecord(selectURL,getPatientReq).then(selectedPatient => {
          //   // console.log("Selected Case response",selectedPatient);
          //   // this.chec();
          //   console.log("selectedPatient",this.selectedPatient);
          //
          //
          // });
          // this.router.navigate(['all-patients']);

        }
        else{
          this.notifier.notify( "error", "Error while updating patient");
          this.submitted = false;
          this.ngxLoader.stop();

          return;
        }

      },
      error=>{
        this.ngxLoader.stop();
        this.notifier.notify( "error", "Error while checking client existance backend connect aborted" );
      })
    }

    checkDate(e){
      console.log('this.maxDate',this.maxDate);
      console.log('e',e);

      var regexp = new RegExp(/((19|20)\d\d\-((0[1-9]|1[0-2])))\-((0|1)[0-9]|2[0-9]|3[0-1])$/);
      var datsOB = regexp.test(e);
        if (datsOB == true) {
          // console.log('e----',e);
          if (e > this.maxDate) {
            // alert("date greater then current"+e)
            this.dobToShow = null;
            return
          }
          else{

              this.getAge(e,'normal');


          }
        }
        else{
          if (e > this.maxDate) {
            // alert("date greater then current"+e)
            this.dobToShow = null;
            return
          }
        }

    }

  getAge(e,type) {

      var today = new Date();
      var birthDate = new Date(e);
      // var birthDate = new Date(e.target.value);
      var age = today.getFullYear() - birthDate.getFullYear();
      var m = today.getMonth() - birthDate.getMonth();
      if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age = age - 1;
      }
      // var temp  = birthDate.getMonth()+1;
      var ageDB ;
      var monthDB ;
      var dayDB ;
      if ((birthDate.getMonth()+1) < 10) {
        monthDB = "0"+(birthDate.getMonth()+1);
        // ageDB = a + "-" + birthDate.getDate() + "-" + birthDate.getFullYear();
      }
      else{
        monthDB = (birthDate.getMonth()+1);
        // ageDB = (birthDate.getMonth()+1) + "-" + birthDate.getDate() + "-" + birthDate.getFullYear();
      }
      if (birthDate.getDate() < 10) {
        dayDB = "0"+birthDate.getDate();
      }
      else{
        dayDB = birthDate.getDate();
      }
      ageDB = monthDB + "-" + dayDB + "-" + birthDate.getFullYear();
      // console.log("ageDB",ageDB);
      this.saveRequest.data.patientDemographics.dateOfBirth = ageDB;

      var ar = ageDB.split('-');
      console.log("ar",ar);
      // this.dobToShow = this.zeroPadded(ar[2])+"-"+this.zeroPadded(ar[0])+"-"+ar[1];
      // console.log("dobToShow",this.dobToShow);
       // console.log("age",age);
       this.dateOfBirth = age;
       // if (this.patienFromEditCase == null) {
       //   this.patientExistCheck()
       //
       // }
       console.log('get age');

       if (type == 'normal') {
         this.patientExistCheck()
       }



     // return;

  }
  changeInsurance(e) {
    var value = e.target.value;
    // console.log("value",value);

    if(value == 1) {
      this.isInsurance = 1;
      this.patientForm.get('insuranceName').setValidators([Validators.required, Validators.maxLength(50)])
      this.patientForm.controls["insuranceName"].updateValueAndValidity();
      // this.patientForm.get('insuranceName').setValidators(Validators.maxLength(50))
      this.patientForm.get('insuranceID').setValidators([Validators.required, Validators.maxLength(50)])
      this.patientForm.controls["insuranceID"].updateValueAndValidity();
      // this.patientForm.get('insuranceID').setValidators(Validators.maxLength(50))

    } else {

      this.patientForm.get('insuranceName').clearValidators();
      this.patientForm.controls["insuranceName"].updateValueAndValidity();
      this.patientForm.get('insuranceID').clearValidators();
      this.patientForm.controls["insuranceID"].updateValueAndValidity();
      this.isInsurance = 0;

    }
  }

////////// search client
  loadSearchClients(e){
    if (e.target.value.length >=3) {
      this.clientLoading = true
      const searchURL    = environment.API_CLIENT_ENDPOINT + 'searchclientidnamebyname';
      this.searchClientName.data.searchString = e.target.value;
      this.searchClientName.header.partnerCode = this.logedInUserRoles.partnerCode;
      this.searchClientName.header.userCode = this.logedInUserRoles.userCode;
      if (this.pageType=="add") {
        this.searchClientName.header.functionalityCode = "PNTA-AP";
      }
      if (this.pageType=="edit") {
        this.searchClientName.header.functionalityCode = "PNTA-UP";
      }
      this.patientnService.searchClientByName(searchURL, this.searchClientName).subscribe(resp =>{
        this.clientsDropdown  = resp['data'];
        this.clientLoading    = false;

      })
    }
    else{
      console.log("length not match");

    }

  }
  clientChanged(){
    // console.log("this.saveRequest",this.saveRequest);
    this.saveRequest.data.clientId = this.clientSearchString;
    // this.saveRequest.data.clientId = this.clientSearchString.clientId;
    console.log("client Changed-----",this.clientSearchString);
    // this.patientExistCheck();
    // if (this.patienFromEditCase == null) {
    //   this.patientExistCheck()
    //
    // }
    this.patientExistCheck()

  }

  patientExistCheck(){
    console.log("dob",this.saveRequest.data.patientDemographics.dateOfBirth);
    // console.log("firstName",this.saveRequest.data.patientDemographics.firstName);
    // console.log("lastName",this.saveRequest.data.patientDemographics.lastName);
    // console.log("ssn",this.saveRequest.data.patientDemographics.ssn);
    if (this.saveRequest.data.patientDemographics.dateOfBirth == '' || this.saveRequest.data.patientDemographics.firstName == '' || this.saveRequest.data.patientDemographics.lastName == ''
        || this.saveRequest.data.patientDemographics.dateOfBirth == null || this.saveRequest.data.patientDemographics.firstName == null || this.saveRequest.data.patientDemographics.lastName == null || this.clientSearchString == null) {
        return
    }
    else{
      $('.existCheck-loadergif').css('display',"inline-block");
      var birthDate = new Date(this.saveRequest.data.patientDemographics.dateOfBirth);
      console.log("firstName",this.saveRequest.data.patientDemographics.firstName);
      console.log("lastName",this.saveRequest.data.patientDemographics.lastName);
      // var ageDB = (birthDate.getMonth()+1) + "-" + birthDate.getDate() + "-" + birthDate.getFullYear();
      var sarchreqData = {...this.patientExistReq}
      sarchreqData.data.lastName  = this.encryptDecrypt.encryptString(this.saveRequest.data.patientDemographics.lastName);
      sarchreqData.data.firstName = this.encryptDecrypt.encryptString(this.saveRequest.data.patientDemographics.firstName);
      // this.saveRequest.data.patientDemographics.ssn = this.encryptDecrypt.encryptString(this.saveRequest.data.patientDemographics.ssn);

      // sarchreqData.data.firstName    = this.saveRequest.data.patientDemographics.firstName;
      // sarchreqData.data.lastName     = this.saveRequest.data.patientDemographics.lastName;
      // sarchreqData.data.dateOfBirth  = ageDB;
      sarchreqData.data.dateOfBirth  = this.saveRequest.data.patientDemographics.dateOfBirth;
      // sarchreqData.data.clientId     = this.saveRequest.data.clientId;
      sarchreqData.data.clientId     = this.clientSearchString;
      sarchreqData.header.partnerCode = this.logedInUserRoles.partnerCode;
      sarchreqData.header.userCode = this.logedInUserRoles.userCode;
      sarchreqData.header.functionalityCode = "PNTA-MP";
      
      // if (this.pageType=="add") {
      //   sarchreqData.header.functionalityCode = "PNTA-AP";
      // }
      // if (this.pageType=="edit") {
      //   sarchreqData.header.functionalityCode = "PNTA-UP";
      // }

      console.log("sarchreqData",sarchreqData);
      // console.log('this.saveRequest.data.patientDemographics.firstName',this.encryptDecrypt.decryptString(this.saveRequest.data.patientDemographics.firstName));
      //
      // return;
      const existURL    = environment.API_PATIENT_ENDPOINT + 'patientexists';
      this.patientnService.patientexists(existURL,sarchreqData).subscribe(resp=>{
        console.log("resp Patient Exist",resp);
        if (resp['data'] == true) {
          $('.existCheck-loadergif').css('display',"none");
          this.clientExist = true;
          // this.notifier.getConfig().behaviour.autoHide = 8000;
          this.notifier.notify( "warning", "Patient already exists please change Firstname/LastName/Date of Birth" );

        }
        else if (resp['data'] == null) {
          this.clientExist = true;
          this.notifier.notify( "warning", resp['result']['description']);

        }
        else{
          $('.existCheck-loadergif').css('display',"none");
          this.clientExist = false;
          return;
        }

      },
      error=>{
        $('.existCheck-loadergif').css('display',"none");
        this.notifier.notify( "error", "Error while checking client existance backend connect aborted" );
      })
    }

  }
  ssnExistCheck(){
    // console.log("ssn",this.saveRequest.data.patientDemographics.ssn);
    if (this.saveRequest.data.patientDemographics.ssn == '' || this.saveRequest.data.patientDemographics.ssn == null) {
        return
    }
    else{
      $('#ssn-loadergif').css('display',"inline-block");
      var sarchreqData = {...this.ssnExistReq}
      sarchreqData.data.ssn    = this.encryptDecrypt.encryptString(this.saveRequest.data.patientDemographics.ssn);
      console.log("sarchreqData",sarchreqData);
      sarchreqData.header.partnerCode = this.logedInUserRoles.partnerCode;
      sarchreqData.header.userCode = this.logedInUserRoles.userCode;
      sarchreqData.header.functionalityCode = "PNTA-MP";
      // if (this.pageType=="add") {
      //   sarchreqData.header.functionalityCode = "PNTA-AP";
      // }
      // if (this.pageType=="edit") {
      //   sarchreqData.header.functionalityCode = "PNTA-UP";
      // }
      const existURL    = environment.API_PATIENT_ENDPOINT + 'patientexists';

      this.patientnService.patientexists(existURL,sarchreqData).subscribe(resp=>{
        console.log("resp SSN",resp);
        if (resp['data'] == true) {
          $('#ssn-loadergif').css('display',"none");
          this.ssnExist = true;
          this.notifier.notify( "warning", "SSN already exists, Please provide different SSN" );
        }
        else{
          this.ssnExist = false;
          $('#ssn-loadergif').css('display',"none");
          return;

        }

      },
      error=>{
        $('#ssn-loadergif').css('display',"none");
        this.notifier.notify( "error", "Error while checking client existance backend connect aborted" );
      })

    }

  }
  scrollTo(el: Element): void {
    if (el) {
      el.scrollIntoView({ behavior: 'smooth', block: 'center' });
    }
  }
  scrollToError(): void {
    const firstElementWithError = document.querySelector('.ng-invalid[formControlName]');
    console.log("aa:", firstElementWithError);

    this.scrollTo(firstElementWithError);
  }
  toggleActiveClient(type,patientId){
    var mes = '';
    if (type == true) {
      mes = "activate"
    }
    else{
      mes = "deactivate"
    }
    console.log("patientToDisable",patientId);
    console.log("type",type);
    var requestTemplate={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"",functionalityCode: "PNTA-MP",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{username:"danish",patientId:patientId,status:type}}
    var url = environment.API_PATIENT_ENDPOINT + 'changestatus';
    Swal.fire({
        title: 'Are you sure?',
        text: "Are you sure to "+ mes +" the patient!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, do it!'
      }).then((result) => {
        if(result.isConfirmed){
          this.ngxLoader.start()
          this.patientnService.togglePatientStatus(url, requestTemplate).subscribe(resp =>{
            $('#deactivateModal').modal('hide');
            this.ngxLoader.stop()
            if (resp['result']['codeType'] == 'S') {
              this.oldStatus = this.saveRequest.data.status;
              this.notifier.notify( "success", "Patient deactivated successfully");
            }
            else{
              this.notifier.notify( "error", resp['result']['description']);
            }
          },
          error=>{
            this.notifier.notify( "error", "Error while changing status check DB connection");

          })
          }
          else {
            this.ngxLoader.stop();
            console.log('this.oldStatus',this.oldStatus);
            console.log('this.saveRequest.data.status',this.saveRequest.data.status);

            this.saveRequest.data.status = this.oldStatus;
          }
      }
      )

  }

  getLookups(){
    this.ngxLoader.start();
    return new Promise((resolve, reject) => {
      var patCat = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=PATIENT_TYPE_CACHE&partnerCode=sip').then(response =>{
        return response;
      })
      var ethinType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=ETHNICITY_CACHE&partnerCode=sip').then(response =>{
        return response;
      })
      var raceType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=RACE_CACHE&partnerCode=sip').then(response =>{
        return response;
      })
      var billingType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=PAYMENT_TYPE_CACHE&partnerCode=sip').then(response =>{
        return response;
      })
      var allCountry = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=COUNTRY_CACHE&partnerCode=sip').then(response =>{
        return response;
      })
      // var allState = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=STATE_CACHE&partnerCode=sip').then(response =>{
      //   return response;
      // })
      var allCity = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=PAYMENT_TYPE_CACHE&partnerCode=sip').then(response =>{
        return response;
      })


      forkJoin([patCat,billingType, ethinType, raceType, allCountry, allCity]).subscribe(allLookups => {
        console.log("allLookups",allLookups);
        // this.allpattypes    = allLookups[0];
        this.allpattypes    = this.sortPipe.transform(allLookups[0], "asc", "patientTypeName");
        this.allBillingypes = this.sortPipe.transform(allLookups[1], "asc", "paymentTypeName");
        // this.allEthnicities = allLookups[2];
        this.allEthnicities = this.sortPipe.transform(allLookups[2], "asc", "ethnicityName");
        this.allRaces       = this.sortPipe.transform(allLookups[3], "asc", "raceName");
        this.allCountry     = this.sortPipe.transform(allLookups[4], "asc", "countryName");
        // this.allState       = this.sortPipe.transform(allLookups[5], "asc", "stateName");

        for (let index = 0; index < allLookups[4]['length']; index++) {
        if (allLookups[4][index]['countryName'] == 'USA' || allLookups[4][index]['countryName'] == 'usa') {
          this.saveRequest.data.patientDemographics.address.countryId = allLookups[4][index]['countryId'];
          this.changeCountry()
        }

      }

        resolve();

      })
    })

  }
  changeCountry(){
  if (this.saveRequest.data.patientDemographics.address.countryId != null) {
    var cid = this.saveRequest.data.patientDemographics.address.countryId;
    this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=STATE_CACHE&partnerCode=sip&id='+cid).then(response =>{
      // console.log("states",response);
      this.allState = this.sortPipe.transform(response, "asc", "stateName");
    })
  }

}

changeState(city){
  console.log('this.saveRequest.data.patientDemographics.address.stateId',this.saveRequest.data.patientDemographics.address.stateId);

  if (this.saveRequest.data.patientDemographics.address.stateId != null) {
    var cid = this.saveRequest.data.patientDemographics.address.stateId;
    this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=CITY_CACHE&partnerCode=sip&id='+cid).then(response =>{
      // console.log("states",response);
      this.allCities = this.sortPipe.transform(response, "asc", "cityName");


      if (city != 0) {
      this.saveRequest.data.patientDemographics.address.cityId = city;
      }
    })
  }

}
  checkLookups(){
    console.log("saveRequest",this.saveRequest);

  }

  cancelEdit(){
    console.log('this.selectedClientForAddAccNumber',this.selectedClientForAddAccNumber);
    console.log('this.clientSearchString',this.clientSearchString);
    // return;
    localStorage.removeItem('clientCodeForPatient');
    if (this.selectedClientForAddAccNumber != null) {
      // this.selectedClientForAddAccNumber = null;
      // this.clientSearchString.clientId;
      localStorage.setItem("backFromPatientToClient",JSON.stringify(this.clientSearchString))
      var a = this.selectedClientForAddAccNumber+';3'
      // var ciphertext = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
      var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
      var bx = ax.replace('+','xsarm');
      var cx = bx.replace('/','ydan');
      var ciphertext = cx.replace('=','zhar');
      this.selectedClientForAddAccNumber = null;
      // this.router.navigateByUrl('/edit-client/'+ciphertext)
      this.router.navigate(['edit-client', ciphertext]);
      // this.router.navigateByUrl('/edit-client/'+this.selectedClientForAddAccNumber+';from='+3)
    }
    else{
      this.router.navigate(['all-patients']);
    }
  }
  setNumber(e, from){
    var regexp = new RegExp(/^[-0-9 \/_?:.,\)\(\s]+$/);
    var number = regexp.test(e.target.value);
    // console.log("number",number);
    // console.log("e.target.value",e.target.value);

      if (number == false) {
        e.target.value = null;
        if (from == 'cell') {
          this.mobileNumber = null;
        }
        else if(from == 'home'){
          this.homePhone = null;
        }
      }

  }

}
