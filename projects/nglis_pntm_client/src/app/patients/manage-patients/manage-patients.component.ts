import { Component, OnInit, ElementRef, ViewChild, ViewChildren, OnDestroy, ComponentFactoryResolver, Renderer2, ComponentRef, ViewContainerRef, QueryList, Input} from '@angular/core';
import { environment } from '../../../../../../src/environments/environment';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { DataTableDirective } from 'angular-datatables';
import { Subject, forkJoin } from 'rxjs';
declare var $ :any;
import { PatientApiCallsService } from '../../services/patient/patient-api-calls.service';
import { GlobalApiCallsService } from '../../services/global/global-api-call.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
declare var $ :any;
import Swal from 'sweetalert2'
import { NotifierService } from 'angular-notifier';
import * as dymo from '../../../assets/js/dymo-label.js';
declare var dymo: any;
import { DatePipe } from '@angular/common';
import { DataService } from "../../../../../../src/services/data/data.service";
// import { RbacDirective } from './../../../../../../src/app/directives/rbac.directive';
// import { NglisRbacDirective } from './../../../../../../src/app/app.module';
import { GlobalySharedModule } from './../../../../../../src/app/sharedmodules/globaly-shared/globaly-shared.module';
import { UrlGuard } from './../../../../../../src/services/gaurd/url.guard'
import * as CryptoJS from 'crypto-js';

@Component({
  selector   : 'app-manage-patients',
  templateUrl: './manage-patients.component.html',
  styleUrls  : ['./manage-patients.component.css'],
  host       : {
    class    :'mainComponentStyle'
}
})
export class ManagePatientsComponent implements OnInit {
  mergeView              = false;
  clientManagePatientsView   = true;

  @Input()
  selectedClientID;

  @ViewChildren(DataTableDirective)

  dtElement              : QueryList<any>;
  dtTrigger              : Subject<ManagePatientsComponent> = new Subject();
  // dtTrigger2             : Subject<ManagePatientsComponent> = new Subject();
  triggerHistoryTable    = false;
  public dtOptions       : DataTables.Settings[] = [];
  public paginatePage    : any = 0;
  public paginatePage2   : any = 0;
  public paginatePage3   : any = 0;
  public paginatePage4   : any = 0;
  public paginateLength  = 20;
  oldCharacterCount      = 0;
  oldCharacterCount2     = 0;
  oldCharacterCount3     = 0;
  oldCharacterCount4     = 0;
  updatePatientAction = false ;

  public requestData     ={
    header:{
        uuid               :"",
        partnerCode        :"",
        userCode           :"",
        referenceNumber    :"",
        systemCode         :"",
        moduleCode         :"",
        functionalityCode: "PNTA-MP",
        systemHostAddress  :"",
        remoteUserAddress  :"",
        dateTime           :""
      },
      data:{
      username          : "danish",
      patientType       : "",
      pageNumber        : 0,
      pageSize          : 20,
      sortColumn        :"patientId",
      sortType          :"desc"
      }
  }
  public newRequestData     ={
    header:{
        uuid               :"",
        partnerCode        :"",
        userCode           :"",
        referenceNumber    :"",
        systemCode         :"",
        moduleCode         :"",
        functionalityCode: "PNTA-MP",
        systemHostAddress  :"",
        remoteUserAddress  :"",
        dateTime           :""
      },
      data:{
      // username          : "danish",
      // patientType       : "",
      pageNumber        : 0,
      pageSize          : 20,
      sortColumn        :"patientId",
      sortType          :"desc",
      clientId          :'',
      mainSearch        : ''
      }
  }

  public clientPatientrequestData     ={
    header:{
        uuid               :"",
        partnerCode        :"",
        userCode           :"",
        referenceNumber    :"",
        systemCode         :"",
        moduleCode         :"PNTM",
        functionalityCode: "PNTA-MP",
        systemHostAddress  :"",
        remoteUserAddress  :"",
        dateTime           :""
      },
      data:{
      userName:"danish",
      clientIds:[],
      searchQuery:"",
      sortColumn:"patientId",
      sortType:"",
      pageSize:20,
      pageNumber:0,
      eligibility:null
      }
  }

  public searchData     ={
    header:{
        uuid               :"",
        partnerCode        :"",
        userCode           :"",
        referenceNumber    :"",
        systemCode         :"",
        moduleCode         :"",
        functionalityCode: "PNTA-MP",
        systemHostAddress  :"",
        remoteUserAddress  :"",
        dateTime           :""
      },
      data:{
      username          : "danish",
      searchCriteria    : "",
      eligibility       : null,
      pageNumber        : 0,
      pageSize          : 20,
      searchType        :"",
      sortColumn        :"patientId",
      sortType          :"desc"
      }
  }

  allPatients           : any = [];
  eligiblePatients      : any = [];
  nonEligiblePatients      : any = [];
  totalPatientsCount    ;
  totalPatientsCoun2    ;
  totalPatientsCoun3    ;
  totalSpecimenCount    ;
  patientToDisable      ;
  searchCreateriaHolderall = '';
  searchCreateriaHoldereligible = '' ;
  searchCreateriaHoldernoneligible = '' ;
  exportRequestData      = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"",
      functionalityCode: "PNTA-MP",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      username:"danish",
      searchCriteria: "",
      eligibility  : null,
      clientId : null

    }
  }
  fileBase64 = '';
  filePayLoad ;
  fileName   = '' ;
  successFileURL : any;
  successCount  = null;
  failureCount  = null;
  public saveRequest          = {
    header  : {
      uuid              :"",
      partnerCode       :"",
      userCode          :"",
      referenceNumber   :"",
      systemCode        :"",
      moduleCode        :"SPCM",
      functionalityCode: "PNTA-MP",
      systemHostAddress :"",
      remoteUserAddress :"",
      dateTime          :""
    },
    data:{

      caseCategoryPrefix:"MA",
      caseNumber:"",
      caseStatusId:1,
      caseCategoryId:null,
      clientId:null,
      attendingPhysicianId:"376",
      patientId:null,
      createdTimestamp:"2020-10-22T11:02:03.523+00:00",
      receivingDate:"2020-10-22T07:54:10.485+00:00",
      collectedBy:"",
      collectionDate:null,
      accessionTimestamp:"2020-10-22T07:54:10.485+00:00",
      createdBy:"abc",
      caseSpecimen:[
        {
          // caseSpecimenId:1,
          specimenTypeId:null,
          specimenSourceId:1,
          bodySiteId:1,
          procedureId:2,
          createdBy:"abc",
          createdTimestamp:"2020-10-22T07:54:10.485+00:00",
          updatedBy:"",
          updatedTimestamp:""
        }
      ]
    }
  }
  printedCaseNumber   = null;
  caseUrl             = '';
  printedCaseCategory = '';
  slectedPatientCases : any = [];
  selectedpatientId   = null;
  selectedpFname      = null;
  selectedpLname      = null;
  selectedpDob        = null;
  allSpecimenTypes   : any =[];
  logedInUserRoles :any;
  allCaseCategories         : any = [];
  // @Input() selectedClient: string;
// message = 'Hola Mundo!';
message:string;
fromtClient = false;
public triageRequest = {
  header  : {
    uuid              :"",
    partnerCode       :"",
    userCode          :"",
    referenceNumber   :"",
    systemCode        :"",
    moduleCode        :"TWMM",
    functionalityCode: "PNTA-MP",
    systemHostAddress :"",
    remoteUserAddress :"",
    dateTime          :""
  },
  data : {
    caseId:"6",
    testStatusId:1,
    testId:1,
    createdBy:"abc",
    createdTimestamp: "26-11-2020 20:07:25"

  }
}
  constructor(
    private formBuilder  : FormBuilder,
    private http         : HttpClient,
    private route        : ActivatedRoute,
    private router       : Router ,
    // private httpRequest  : HttpRequestsService,
    private ngxLoader    : NgxUiLoaderService,
    private sanitizer    : DomSanitizer,
    private  notifier    : NotifierService,
    private patientnService : PatientApiCallsService,
    private globalService   : GlobalApiCallsService,
    private datePipe: DatePipe,
    private sharedData: DataService,
    private rbac         : UrlGuard,
    private encryptDecrypt : GlobalySharedModule
  ) {

    // this.rbac.checkROle('PNTA-MP');
    // console.log("------------selectedClientIDofPatient",this.selectedClient);

   }

  ngOnInit(): void {
    localStorage.removeItem('clientCodeForPatient');
    // var a = 'H/er30L/jhrBBXpbG3o79Q==';
    // console.log('decrypt',this.encryptDecrypt.decryptString(a));
// return

    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
var originalText = bts.toString(CryptoJS.enc.Utf8);
this.logedInUserRoles = JSON.parse(originalText);


    this.sharedData.currentClientId.subscribe(clientId => {
      console.log("selectedClientId---",clientId);
      this.route.params.subscribe(params => {
        console.log("params",params);
        if (clientId != 'null' && typeof params['id'] != "undefined") {
          console.log("******----------*********");
          this.fromtClient = true;

          // this.sharedData.selectedClientCode('544545');

          // return;

          this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
            dtElement.dtInstance.then((dtInstance: any) => {
              dtInstance.ajax.reload();
              //               // if (this.fromtClient == true) {
              //   $('#all-patient-div-client').css('visibility','visible');
              // }
            })
          })
          //           ////if (this.fromtClient == true) {
          //   $('#all-patient-div-client').css('visibility','visible');
          // }
        }
        else{
          // if (localStorage.getItem("backFromPatientToClient") != null) {
          //
          //   this.selectedClientID = JSON.parse(localStorage.getItem("backFromPatientToClient"));
          //   this.fromtClient = true;
          //   console.log("4444444444443,",this.selectedClientID);
          //
          //   localStorage.getItem("backFromPatientToClient")
          //   this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
          //     dtElement.dtInstance.then((dtInstance: any) => {
          //       dtInstance.ajax.reload();
          //     })
          //   })
          // }
        }
        // else {
        //   this.rbac.checkROle('PNTA-MP');
        // }
        this.selectedClientID = clientId;


        this.message = clientId;
      });


    })


    // console.log("this.fromtClient",this.fromtClient);

    if(this.logedInUserRoles['allowedRoles'].indexOf("CLTA-VC") == -1 && this.logedInUserRoles['allowedRoles'].indexOf("CLTA-UC") == -1){
      this.rbac.checkROle('PNTA-MP');
    }

    // this.logedInUserRoles.userName;
    if(this.logedInUserRoles['allowedRoles'].indexOf("PNTA-UP") != -1){
      this.updatePatientAction = true;

    }
   if (this.router.url =='/edit-client/st122;from=2')
   {
    this.clientManagePatientsView=false;
   }
   else
   {
    this.clientManagePatientsView=true;
   }

    this.getLookups().then(lookupsLoaded => {
    });
    if (localStorage.getItem("backFromPatientToClient") != 'undefined') {
      if (localStorage.getItem("backFromPatientToClient") != null) {
        console.log('localStorage.getItem("backFromPatientToClient")',localStorage.getItem("backFromPatientToClient"));

        this.selectedClientID = JSON.parse(localStorage.getItem("backFromPatientToClient"));
        this.fromtClient = true;
        localStorage.removeItem("backFromPatientToClient")
      }
    }


    this.dtOptions[0] = {
      pagingType   : 'full_numbers',
      pageLength   : this.paginateLength,
      serverSide   : true,
      processing   : true,
      // ordering     : true,
      "order": [[ 1, "desc" ]],
      columnDefs: [
            { orderable: true, className: 'reorder', targets: [1,2,3,4,5,6] },
            { orderable: false, targets: '_all' }
        ],
      "lengthMenu": [20, 50, 75, 100 ],
      ajax: (dataTablesParameters: any, callback) => {
        this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
         dtElement.dtInstance.then((dtInstance: any) => {
           if (dtInstance.table().node().id == 'all-patient' ) {
             // console.log("dataTablesParameters",dataTablesParameters);
             var sortColumn = dataTablesParameters.order[0]['column'];
             var sortOrder  = dataTablesParameters.order[0]['dir'];
             var sortArray  = ["patientId",'patientId','patientFirstName','patientLastName','patientDateOfBirth','patientPhone','clientName','patientId','patientId','patientId','patientId','patientId']
             // console.log("sortArray",sortArray[sortColumn]);
             // return;


             this.paginatePage           = dtElement['dt'].page.info().page;
             this.paginateLength         = dtElement['dt'].page.len();
             let url = '';
             this.searchCreateriaHolderall = dataTablesParameters.search.value;
             var reqAllPatients = {};
             reqAllPatients = {...this.newRequestData}
             url    = environment.API_ACDM_ENDPOINT + 'allpatients';
             // console.log("this.fromtClient",this.fromtClient);


             if (dataTablesParameters.search.value != "" && (this.oldCharacterCount == 0 || this.oldCharacterCount == 3)) {
               if (this.oldCharacterCount == 3) {
               }
               reqAllPatients['data'].mainSearch    = dataTablesParameters.search.value;
             }
             else{
               if (dataTablesParameters.search.value.length >=3) {
               }
               else{
                 $('.dataTables_processing').css('display',"none");
                 if (this.fromtClient == true) {
                   $('#all-patient-div-client').css('visibility','visible');
                 }
                 if (this.oldCharacterCount == 3) {
                   this.oldCharacterCount     = 3
                 }
                 else{
                   this.oldCharacterCount     = 2;
                 }
               }
             }


               // console.log('reqAllPatients',reqAllPatients);
               // console.log('url',url);
               /////// reqAllPatients.data.patientType   = 1;
               this.oldCharacterCount     = 3;
               if (this.selectedClientID != 'null' && this.fromtClient == true){
                  // reqAllPatients=  {...this.clientPatientrequestData};
                  reqAllPatients['data'].userName = this.logedInUserRoles.userCode;
                  reqAllPatients['header'].userCode = this.logedInUserRoles.userCode;
                  reqAllPatients['header'].partnerCode = this.logedInUserRoles.partnerCode;
                  reqAllPatients['header'].functionalityCode = "PNTA-MP";
                  reqAllPatients['data'].clientId = this.selectedClientID;
                  // reqAllPatients['data'].clientIds = [this.selectedClientID];
                  reqAllPatients['data'].pageNumber  = dtElement['dt'].page.info().page;
                  reqAllPatients['data'].pageSize    = dtElement['dt'].page.len();
                  // reqAllPatients['data'].searchQuery = "";
                  // reqAllPatients['data'].eligibility = null;
                  reqAllPatients['data'].sortColumn  = sortArray[sortColumn];
                  reqAllPatients['data'].sortType    = sortOrder;
                  // url    = environment.API_PATIENT_ENDPOINT + 'searchpatientbyclientid';
               }
               else {

                  // reqAllPatients=  {...this.requestData}
                  // reqAllPatients['data'].pageNumber  = this.paginatePage;
                  reqAllPatients['data'].pageNumber  = dtElement['dt'].page.info().page;
                  reqAllPatients['data'].pageSize    = dtElement['dt'].page.len();
                  // reqAllPatients['data'].patientType = "";
                  reqAllPatients['data'].sortColumn  = sortArray[sortColumn];
                  reqAllPatients['data'].sortType    = sortOrder;
                  reqAllPatients['header'].userCode = this.logedInUserRoles.userCode;
                  reqAllPatients['header'].partnerCode = this.logedInUserRoles.partnerCode;
                  reqAllPatients['header'].functionalityCode = "PNTA-MP";
                  // url    = environment.API_ACDM_ENDPOINT + 'allpatients';

               }
               // let url    = environment.API_USER_ENDPOINT + 'partnerusers?partnerId=1&pageNumber='+ this.paginatePage +'&pageSize=' + this.paginateLength;

               this.http.put(url,reqAllPatients).toPromise()
               .then( resp => {
                 console.log('resp all Patient: ', resp);
                 if (typeof resp['result'] != 'undefined') {
                   if (typeof resp['result']['description'] != 'undefined') {
                     if (resp['result']['description'].indexOf('UnAuthorized') != -1) {
                       this.notifier.notify('warning',resp['result']['description'])
                       this.router.navigateByUrl('/page-restrict');
                     }
                   }
                 }

                 if (typeof resp['data']['patients'] ==null) {
                   this.totalPatientsCount = 0
                   callback({
                     recordsTotal    :  this.totalPatientsCount,
                     recordsFiltered :  this.totalPatientsCount,
                     data: []
                   });
                   this.newRequestData.data.mainSearch = '';
                   return;
                 }

                 this.allPatients = resp['data']['patients'];
                 this.totalPatientsCount = resp['data']['totalPatients'];

                 if (this.fromtClient == true) {
                   $('#all-patient-div-client').css('visibility','visible');
                 }

                 callback({
                   recordsTotal    :  this.totalPatientsCount,
                   recordsFiltered :  this.totalPatientsCount,
                   data: []
                 });
                 this.newRequestData.data.mainSearch = '';
                 return;

                 if (resp['data']['patients'] != null) {
                   const map = new Map();
                   var uniqueCLIds =[];
                   var uniquePIds =[];
                   for (const item of resp['data']['patients']) {
                     if(!map.has(item.clientId)){
                       map.set(item.clientId, true);    // set any value to Map
                       uniqueCLIds.push(item.clientId);
                     }
                     uniquePIds.push(item.patientId)
                   }


                   // console.log("Patient",uniquePIds);

                   var clURL           = environment.API_CLIENT_ENDPOINT + "clientidnamerequisition"
                   var clReqData = {
                     header:{
                       uuid               :"",
                       partnerCode        :"",
                       userCode           :"",
                       referenceNumber    :"",
                       systemCode         :"",
                       moduleCode         :"CLIM",
                       functionalityCode: "PNTA-MP",
                       systemHostAddress  :"",
                       remoteUserAddress  :"",
                       dateTime           :""
                     },
                     data:{
                       clientIds:uniqueCLIds,
                       userCodes: []
                     }
                   }
                   clReqData['header'].userCode = this.logedInUserRoles.userCode;
                   clReqData['header'].partnerCode = this.logedInUserRoles.partnerCode;
                   clReqData['header'].functionalityCode = "PNTA-MP";
                   // var clReqData       = {clientIds:uniqueCLIds}
                   this.popClientSpecData(clURL,clReqData,resp,uniquePIds).then(resCS => {
                     console.log("allPatients",this.allPatients);
                     if (this.fromtClient == true) {
                       $('#all-patient-div-client').css('visibility','visible');
                     }

                     callback({
                       recordsTotal    :  this.totalPatientsCount,
                       recordsFiltered :  this.totalPatientsCount,
                       data: []
                     });
                   });
                 }
                 else{
                   ///////////////// not patient Found
                   this.ngxLoader.stop();
                   if (this.fromtClient == true) {
                     $('#all-patient-div-client').css('visibility','visible');
                   }
                   // this.notifier.notify( "error", "Error while loading all patiens");
                   callback({
                     recordsTotal    :  0,
                     recordsFiltered :  0,
                     data: []
                   });
                 }


              })


           }
           // console.log(`The DataTable ${index} instance ID is: ${dtInstance.table().node().id}`);
         });
       });



      },
      // columns  : [
      //   { data : 'PatientId' },
      //   { data : 'firstName' },
      //   { data : 'lastName' },
      //   { data : 'username' },
      //   { data : 'phone' },
      //   { data : 'email' },
      //   { data : 'action' }
      // ]
      ///for search
    };


    //////////////////////////// Eligible Patients
    this.dtOptions[1] = {
      pagingType   : 'full_numbers',
      pageLength   : this.paginateLength,
      serverSide   : true,
      processing   : true,
      // ordering     : true,
      "order": [[ 1, "desc" ]],
      responsive   :true,
      columnDefs: [
            { orderable: true, className: 'reorder', targets: [1,2,3,4,5] },
            { orderable: false, targets: '_all' }
        ],
      "lengthMenu": [20, 50, 75, 100 ],
      ajax: (dataTablesParameters: any, callback) => {
        this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
         dtElement.dtInstance.then((dtInstance: any) => {
           if(dtInstance.table().node().id == 'patient-eligible'){
             console.log("dataTablesParameters",dataTablesParameters);

             var sortColumn = dataTablesParameters.order[0]['column'];
             var sortOrder  = dataTablesParameters.order[0]['dir'];
             var sortArray  = ["patientId",'patientId','firstName','lastName','dateOfBirth','ssn','patientId','patientId','patientId','patientId','patientId','patientId']

             // console.log("External",dtElement['dt']);
             // console.log("External page",dtElement['dt'].page.info().page);
             // console.log("External page len",dtElement['dt'].page.len());
             this.paginatePage2          = dtElement['dt'].page.info().page;
             this.paginateLength         = dtElement['dt'].page.len();
             if (dataTablesParameters.search.value == "" && (this.oldCharacterCount2 == 0 || this.oldCharacterCount2 == 3)) {

               this.searchCreateriaHoldereligible = dataTablesParameters.search.value;
               let url = '';
               this.searchCreateriaHolderall = dataTablesParameters.search.value;
               var reqEligiblePatients = {};
               // console.log("this.fromtClient",this.fromtClient);

               if (this.selectedClientID != 'null' && this.fromtClient == true){
                  reqEligiblePatients=  {...this.clientPatientrequestData};
                  reqEligiblePatients['header'].userCode = this.logedInUserRoles.userCode;
                  reqEligiblePatients['header'].partnerCode = this.logedInUserRoles.partnerCode;
                  // reqEligiblePatients['header'].partnerCode = this.logedInUserRoles.userCode;
                  reqEligiblePatients['header'].functionalityCode = "PNTA-MP";
                  reqEligiblePatients['data'].clientIds = [this.selectedClientID];
                  reqEligiblePatients['data'].pageNumber  = dtElement['dt'].page.info().page;
                  reqEligiblePatients['data'].pageSize    = dtElement['dt'].page.len();
                  reqEligiblePatients['data'].searchQuery = "";
                  reqEligiblePatients['data'].eligibility = 1;
                  reqEligiblePatients['data'].sortColumn  = sortArray[sortColumn];
                  reqEligiblePatients['data'].sortType    = sortOrder;
                  url    = environment.API_PATIENT_ENDPOINT + 'searchpatientbyclientid';
               }
               else {

                  reqEligiblePatients=  {...this.requestData}
                  // reqEligiblePatients['data'].pageNumber  = this.paginatePage;
                  reqEligiblePatients['data'].pageNumber  = dtElement['dt'].page.info().page;
                  reqEligiblePatients['data'].pageSize    = dtElement['dt'].page.len();
                  reqEligiblePatients['data'].patientType = "eligible";
                  reqEligiblePatients['data'].sortColumn  = sortArray[sortColumn];
                  reqEligiblePatients['data'].sortType    = sortOrder;
                  reqEligiblePatients['header'].userCode = this.logedInUserRoles.userCode;
                  reqEligiblePatients['header'].partnerCode = this.logedInUserRoles.partnerCode;
                  reqEligiblePatients['header'].functionalityCode = "PNTA-MP";
                  url    = environment.API_PATIENT_ENDPOINT + 'allpatients';

               }

               // var reqEligiblePatients = {...this.requestData}
               // reqEligiblePatients.data.pageNumber = dtElement['dt'].page.info().page;
               // reqEligiblePatients.data.pageSize   = this.paginateLength;
               // reqEligiblePatients.data.patientType= "eligible";
               // reqEligiblePatients.data.sortColumn  = sortArray[sortColumn];
               // reqEligiblePatients.data.sortType    = sortOrder;
               // reqEligiblePatients.data.userType   = 2;
               // let url    = environment.API_USER_ENDPOINT + 'partnerusers?partnerId=1&pageNumber='+ this.paginatePage +'&pageSize=' + this.paginateLength;
               // url    = environment.API_PATIENT_ENDPOINT + 'allpatients';


               // dataTablesParameters.pageNumber = this.paginatePage;
               this.http.put(url,reqEligiblePatients).toPromise()
               .then( resp => {
                 console.log('resp ELigible: ', resp);
                 if (resp['data']['patients'] != null) {
                   const map = new Map();
                   var uniqueCLIds =[];
                   for (const item of resp['data']['patients']) {
                     if(!map.has(item.clientId)){
                       map.set(item.clientId, true);    // set any value to Map
                       uniqueCLIds.push(item.clientId);
                     }
                   }
                   // console.log("uniqueCLIds",uniqueCLIds);

                   var clURL           = environment.API_CLIENT_ENDPOINT + "clientidsnames"
                   var clReqData = {
                     header:{
                       uuid               :"",
                       partnerCode        :"",
                       userCode           :"",
                       referenceNumber    :"",
                       systemCode         :"",
                       moduleCode         :"CLIM",
                       functionalityCode: "CLTA-VC",
                       systemHostAddress  :"",
                       remoteUserAddress  :"",
                       dateTime           :""
                     },
                     data:{
                       clientIds:uniqueCLIds
                     }
                   }
                   clReqData['header'].userCode = this.logedInUserRoles.userCode;
                   clReqData['header'].partnerCode = this.logedInUserRoles.partnerCode;
                   clReqData['header'].functionalityCode = "CLTA-VC";
                   // var clReqData       = {clientIds:uniqueCLIds}
                   this.patientnService.getClientByIds(clURL,clReqData).then(selectedClient => {
                     // console.log("this.selectedClient",selectedClient);
                     for (let i = 0; i < resp['data']['patients'].length; i++) {
                       for (let j = 0; j < selectedClient['data']['length']; j++) {
                         if (resp['data']['patients'][i].clientId == selectedClient['data'][j].clientId) {
                           resp['data']['patients'][i].clientName = selectedClient['data'][j].clientName;
                         }

                       }
                     }
                     this.eligiblePatients                 = resp['data']['patients'];
                     this.totalPatientsCoun2              = resp['data']['totalPatients'];
                     callback({
                       recordsTotal    :  this.totalPatientsCoun2,
                       recordsFiltered :  this.totalPatientsCoun2,
                       data: []
                     });
                   });
                 }
                 else{
                   this.eligiblePatients                 = resp['data']['patients'];
                   this.totalPatientsCoun2              = resp['data']['totalPatients'];
                   callback({
                     recordsTotal    :  this.totalPatientsCoun2,
                     recordsFiltered :  this.totalPatientsCoun2,
                     data: []
                   });
                 }


               })
               .catch(error => {
                 // Set loader false
                 // this.loading = false;

                 this.ngxLoader.stop();
                 this.notifier.notify( "error", "Error while loading eligible patiens");
               });
               this.oldCharacterCount2 = 3;
             }
             else{
               if (dataTablesParameters.search.value.length >= 3) {

                 this.searchCreateriaHoldereligible = dataTablesParameters.search.value;
                 var searchEligiblePatients = {}
                  let url    = '';
                 if (this.selectedClientID != 'null' && this.fromtClient == true){
                    searchEligiblePatients=  {...this.clientPatientrequestData};
                    searchEligiblePatients['data'].userName = this.logedInUserRoles.userCode;
                    searchEligiblePatients['header'].userCode = this.logedInUserRoles.userCode;
                    searchEligiblePatients['header'].partnerCode = this.logedInUserRoles.partnerCode;
                    searchEligiblePatients['header'].functionalityCode = "PNTA-MP";
                    searchEligiblePatients['data'].clientIds = [this.selectedClientID];
                    searchEligiblePatients['data'].pageNumber  = dtElement['dt'].page.info().page;
                    searchEligiblePatients['data'].pageSize    = dtElement['dt'].page.len();
                    searchEligiblePatients['data'].searchQuery = dataTablesParameters.search.value;
                    searchEligiblePatients['data'].eligibility = 1;
                    searchEligiblePatients['data'].sortColumn  = sortArray[sortColumn];
                    searchEligiblePatients['data'].sortType    = sortOrder;
                    url    = environment.API_PATIENT_ENDPOINT + 'searchpatientbyclientid';
                 }
                 else{
                   searchEligiblePatients = {...this.searchData}
                   searchEligiblePatients['data'].pageNumber  = this.paginatePage;
                   searchEligiblePatients['data'].pageSize    = this.paginateLength;
                   searchEligiblePatients['data'].eligibility = 1;
                   searchEligiblePatients['data'].sortColumn  = sortArray[sortColumn];
                   searchEligiblePatients['data'].sortType    = sortOrder;
                   searchEligiblePatients['header'].userCode = this.logedInUserRoles.userCode;
                   searchEligiblePatients['header'].partnerCode = this.logedInUserRoles.partnerCode;
                   searchEligiblePatients['header'].functionalityCode = "PNTA-MP";
                   url    = environment.API_PATIENT_ENDPOINT + 'searchpatients';
                   searchEligiblePatients['data'].searchCriteria = dataTablesParameters.search.value;
                 }

                 // var searchEligiblePatients = {...this.searchData}
                 // searchEligiblePatients.data.pageNumber     = this.paginatePage2;
                 // searchEligiblePatients.data.pageSize       = this.paginateLength;
                 // searchEligiblePatients.data.eligibility    = 1;
                 // searchEligiblePatients.data.searchCriteria = dataTablesParameters.search.value;
                 // searchEligiblePatients.data.sortColumn  = sortArray[sortColumn];
                 // searchEligiblePatients.data.sortType    = sortOrder;

                 // let url    = environment.API_USER_ENDPOINT + 'partnerusers?partnerId=1&pageNumber='+ this.paginatePage +'&pageSize=' + this.paginateLength;
                 // let url    = environment.API_PATIENT_ENDPOINT + 'searchpatients';
                 // console.log('this.paginatePage: ',this.paginatePage);

                 // dataTablesParameters.pageNumber = this.paginatePage;
                 this.http.put(url,searchEligiblePatients).toPromise()
                 .then( resp => {
                   console.log('resp Eligible Patient Search: ', resp);
                   if (resp['data']['patients'] != null) {
                     const map = new Map();
                     var uniqueCLIds =[];
                     for (const item of resp['data']['patients']) {
                       if(!map.has(item.clientId)){
                         map.set(item.clientId, true);    // set any value to Map
                         uniqueCLIds.push(item.clientId);
                       }
                     }
                     // console.log("uniqueCLIds",uniqueCLIds);

                     var clURL           = environment.API_CLIENT_ENDPOINT + "clientidsnames"
                     var clReqData = {
                       header:{
                         uuid               :"",
                         partnerCode        :"",
                         userCode           :"",
                         referenceNumber    :"",
                         systemCode         :"",
                         moduleCode         :"CLIM",
                         functionalityCode: "CLTA-VC",
                         systemHostAddress  :"",
                         remoteUserAddress  :"",
                         dateTime           :""
                       },
                       data:{
                         clientIds:uniqueCLIds
                       }
                     }
                     clReqData['header'].userCode = this.logedInUserRoles.userCode;
                     clReqData['header'].partnerCode = this.logedInUserRoles.partnerCode;
                     clReqData['header'].functionalityCode = "CLTA-VC";
                     // var clReqData       = {clientIds:uniqueCLIds}
                     this.patientnService.getClientByIds(clURL,clReqData).then(selectedClient => {
                       // console.log("this.selectedClient",selectedClient);
                       for (let i = 0; i < resp['data']['patients'].length; i++) {
                         for (let j = 0; j < selectedClient['data']['length']; j++) {
                           if (resp['data']['patients'][i].clientId == selectedClient['data'][j].clientId) {
                             resp['data']['patients'][i].clientName = selectedClient['data'][j].clientName;
                           }

                         }
                       }
                       this.eligiblePatients                 = resp['data']['patients'];
                       this.totalPatientsCoun2              = resp['data']['totalPatients'];
                       callback({
                         recordsTotal    :  this.totalPatientsCoun2,
                         recordsFiltered :  this.totalPatientsCoun2,
                         data: []
                       });
                     });
                   }
                   else{
                     this.eligiblePatients                 = resp['data']['patients'];
                     this.totalPatientsCoun2              = resp['data']['totalPatients'];
                     callback({
                       recordsTotal    :  this.totalPatientsCoun2,
                       recordsFiltered :  this.totalPatientsCoun2,
                       data: []
                     });
                   }
                 })
                 .catch(error => {
                   // Set loader false
                   // this.loading = false;

                   this.ngxLoader.stop();
                   this.notifier.notify( "error", "Error while loading eligible patiens");
                 });

               }
               else{
                 $('.dataTables_processing').css('display',"none");
                 if (this.oldCharacterCount2 == 3) {
                   this.oldCharacterCount2     = 3

                 }
                 else{
                   this.oldCharacterCount2     = 2;
                 }
               }
             }


           }

           // console.log(`The DataTable ${index} instance ID is: ${dtInstance.table().node().id}`);
         });
       });


      },
      columns  : [
        { data : 'SPD-002' },
        { data : 'firstName' },
        { data : 'lastName' },
        { data : 'username' },
        { data : 'phone' },
        { data : 'email' },
        { data : 'action' }
      ],

      ///for search
    };

    //////////////////////////// Non Eligible Patients
    this.dtOptions[2] = {
      pagingType   : 'full_numbers',
      pageLength   : this.paginateLength,
      serverSide   : true,
      processing   : true,
      // ordering     : false,
      responsive   :true,
      columnDefs: [
            { orderable: true, className: 'reorder', targets: [1,2,3,4,5] },
            { orderable: false, targets: '_all' }
        ],
      "lengthMenu": [20, 50, 75, 100 ],
      ajax: (dataTablesParameters: any, callback) => {
        this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
         dtElement.dtInstance.then((dtInstance: any) => {
           if(dtInstance.table().node().id == 'patient-noneligible'){
             var sortColumn = dataTablesParameters.order[0]['column'];
             var sortOrder  = dataTablesParameters.order[0]['dir'];
             var sortArray  = ["patientId",'patientId','firstName','lastName','dateOfBirth','ssn','patientId','patientId','patientId','patientId','patientId','patientId']

             this.paginatePage3          = dtElement['dt'].page.info().page;
             this.paginateLength         = dtElement['dt'].page.len();
             if (dataTablesParameters.search.value == "" && (this.oldCharacterCount3 == 0 || this.oldCharacterCount3 == 3)) {
               this.searchCreateriaHoldernoneligible = dataTablesParameters.search.value;
               let url = '';
               this.searchCreateriaHolderall = dataTablesParameters.search.value;
               var reqEligiblePatients = {};
               // console.log("this.fromtClient",this.fromtClient);

               if (this.selectedClientID != 'null' && this.fromtClient == true){
                  reqEligiblePatients=  {...this.clientPatientrequestData};
                  reqEligiblePatients['data'].userName = this.logedInUserRoles.userCode;
                  reqEligiblePatients['header'].userCode = this.logedInUserRoles.userCode;
                  reqEligiblePatients['header'].partnerCode = this.logedInUserRoles.partnerCode;
                  reqEligiblePatients['header'].functionalityCode = "PNTA-MP";
                  reqEligiblePatients['data'].clientIds = [this.selectedClientID];
                  reqEligiblePatients['data'].pageNumber  = dtElement['dt'].page.info().page;
                  reqEligiblePatients['data'].pageSize    = dtElement['dt'].page.len();
                  reqEligiblePatients['data'].searchQuery = "";
                  reqEligiblePatients['data'].eligibility = 0;
                  reqEligiblePatients['data'].sortColumn  = sortArray[sortColumn];
                  reqEligiblePatients['data'].sortType    = sortOrder;
                  url    = environment.API_PATIENT_ENDPOINT + 'searchpatientbyclientid';
               }
               else {

                  reqEligiblePatients=  {...this.requestData}
                  // reqEligiblePatients['data'].pageNumber  = this.paginatePage;
                  reqEligiblePatients['data'].pageNumber  = dtElement['dt'].page.info().page;
                  reqEligiblePatients['data'].pageSize    = dtElement['dt'].page.len();
                  reqEligiblePatients['data'].patientType = "ineligible";
                  reqEligiblePatients['data'].sortColumn  = sortArray[sortColumn];
                  reqEligiblePatients['data'].sortType    = sortOrder;
                  reqEligiblePatients['header'].userCode = this.logedInUserRoles.userCode;
                  reqEligiblePatients['header'].partnerCode = this.logedInUserRoles.partnerCode;
                  reqEligiblePatients['header'].functionalityCode = "PNTA-MP";
                  url    = environment.API_PATIENT_ENDPOINT + 'allpatients';

               }
               // var reqEligiblePatients = {...this.requestData}
               // reqEligiblePatients.data.pageNumber = dtElement['dt'].page.info().page;
               // reqEligiblePatients.data.pageSize   = this.paginateLength;
               // reqEligiblePatients.data.patientType= "ineligible";
               // reqEligiblePatients.data.sortColumn  = sortArray[sortColumn];
               // reqEligiblePatients.data.sortType    = sortOrder;
               // reqEligiblePatients.data.userType   = 2;
               // let url    = environment.API_USER_ENDPOINT + 'partnerusers?partnerId=1&pageNumber='+ this.paginatePage +'&pageSize=' + this.paginateLength;
               // let url    = environment.API_PATIENT_ENDPOINT + 'allpatients';


               // dataTablesParameters.pageNumber = this.paginatePage;
               this.http.put(url,reqEligiblePatients).toPromise()
               .then( resp => {
                 console.log('**** resp ELigible: ', resp);
                 if (resp['data']['patients'] != null) {
                   const map = new Map();
                   var uniqueCLIds =[];
                   for (const item of resp['data']['patients']) {
                     if(!map.has(item.clientId)){
                       map.set(item.clientId, true);    // set any value to Map
                       uniqueCLIds.push(item.clientId);
                     }
                   }
                   // console.log("uniqueCLIds",uniqueCLIds);

                   var clURL           = environment.API_CLIENT_ENDPOINT + "clientidsnames"
                   var clReqData = {
                     header:{
                       uuid               :"",
                       partnerCode        :"",
                       userCode           :"",
                       referenceNumber    :"",
                       systemCode         :"",
                       moduleCode         :"CLIM",
                       functionalityCode: "CLTA-VC",
                       systemHostAddress  :"",
                       remoteUserAddress  :"",
                       dateTime           :""
                     },
                     data:{
                       clientIds:uniqueCLIds
                     }
                   }
                   clReqData['header'].userCode = this.logedInUserRoles.userCode;
                   clReqData['header'].partnerCode = this.logedInUserRoles.partnerCode;
                   clReqData['header'].functionalityCode = "CLTA-VC";
                   // var clReqData       = {clientIds:uniqueCLIds}
                   this.patientnService.getClientByIds(clURL,clReqData).then(selectedClient => {
                     // console.log("this.selectedClient",selectedClient);
                     for (let i = 0; i < resp['data']['patients'].length; i++) {
                       for (let j = 0; j < selectedClient['data']['length']; j++) {
                         if (resp['data']['patients'][i].clientId == selectedClient['data'][j].clientId) {
                           resp['data']['patients'][i].clientName = selectedClient['data'][j].clientName;
                         }

                       }
                     }
                     this.nonEligiblePatients                 = resp['data']['patients'];
                     this.totalPatientsCoun3          = resp['data']['totalPatients'];
                     callback({
                       recordsTotal    :  this.totalPatientsCoun3,
                       recordsFiltered :  this.totalPatientsCoun3,
                       data: []
                     });
                   });
                 }
                 else{
                   this.nonEligiblePatients                 = resp['data']['patients'];
                   this.totalPatientsCoun3          = resp['data']['totalPatients'];
                   callback({
                     recordsTotal    :  this.totalPatientsCoun3,
                     recordsFiltered :  this.totalPatientsCoun3,
                     data: []
                   });
                 }

               })
               .catch(error => {
                 // Set loader false
                 // this.loading = false;

                 this.ngxLoader.stop();
                 this.notifier.notify( "error", "Error while loading non-eligible patiens");
               });
               this.oldCharacterCount3 = 3;
             }
             else{
               if (dataTablesParameters.search.value.length >= 3) {
                 this.searchCreateriaHoldernoneligible = dataTablesParameters.search.value;
                 var searchEligiblePatients = {}
                  let url    = '';
                 if (this.selectedClientID != 'null' && this.fromtClient == true){
                    searchEligiblePatients=  {...this.clientPatientrequestData};
                    searchEligiblePatients['data'].userName = this.logedInUserRoles.userCode;
                    searchEligiblePatients['header'].userCode = this.logedInUserRoles.userCode;
                    searchEligiblePatients['header'].partnerCode = this.logedInUserRoles.partnerCode;
                    searchEligiblePatients['header'].functionalityCode = "PNTA-MP";
                    searchEligiblePatients['data'].clientIds = [this.selectedClientID];
                    searchEligiblePatients['data'].pageNumber  = dtElement['dt'].page.info().page;
                    searchEligiblePatients['data'].pageSize    = dtElement['dt'].page.len();
                    searchEligiblePatients['data'].searchQuery = dataTablesParameters.search.value;
                    searchEligiblePatients['data'].eligibility = 0;
                    searchEligiblePatients['data'].sortColumn  = sortArray[sortColumn];
                    searchEligiblePatients['data'].sortType    = sortOrder;
                    url    = environment.API_PATIENT_ENDPOINT + 'searchpatientbyclientid';
                 }
                 else{
                   searchEligiblePatients = {...this.searchData}
                   searchEligiblePatients['data'].pageNumber  = this.paginatePage;
                   searchEligiblePatients['data'].pageSize    = this.paginateLength;
                   searchEligiblePatients['data'].eligibility = 0;
                   searchEligiblePatients['data'].sortColumn  = sortArray[sortColumn];
                   searchEligiblePatients['data'].sortType    = sortOrder;
                   searchEligiblePatients['header'].userCode = this.logedInUserRoles.userCode;
                   searchEligiblePatients['header'].partnerCode = this.logedInUserRoles.partnerCode;
                   searchEligiblePatients['header'].functionalityCode = "PNTA-MP";
                   url    = environment.API_PATIENT_ENDPOINT + 'searchpatients';
                   searchEligiblePatients['data'].searchCriteria = dataTablesParameters.search.value;
                 }
                 // var searchEligiblePatients = {...this.searchData}
                 // searchEligiblePatients.data.pageNumber     = this.paginatePage2;
                 // searchEligiblePatients.data.pageSize       = this.paginateLength;
                 // searchEligiblePatients.data.eligibility    = 0;
                 // searchEligiblePatients.data.searchCriteria = dataTablesParameters.search.value;
                 // searchEligiblePatients.data.sortColumn  = sortArray[sortColumn];
                 // searchEligiblePatients.data.sortType    = sortOrder;
                 // let url    = environment.API_USER_ENDPOINT + 'partnerusers?partnerId=1&pageNumber='+ this.paginatePage +'&pageSize=' + this.paginateLength;
                 // let url    = environment.API_PATIENT_ENDPOINT + 'searchpatients';
                 // console.log('this.paginatePage: ',this.paginatePage);

                 // dataTablesParameters.pageNumber = this.paginatePage;
                 this.http.put(url,searchEligiblePatients).toPromise()
                 .then( resp => {
                   console.log('resp Non Eligibel patients Search: ', resp);
                   if (resp['data']['patients'] != null) {
                     const map = new Map();
                     var uniqueCLIds =[];
                     for (const item of resp['data']['patients']) {
                       if(!map.has(item.clientId)){
                         map.set(item.clientId, true);    // set any value to Map
                         uniqueCLIds.push(item.clientId);
                       }
                     }
                     // console.log("uniqueCLIds",uniqueCLIds);

                     var clURL           = environment.API_CLIENT_ENDPOINT + "clientidsnames"
                     var clReqData = {
                       header:{
                         uuid               :"",
                         partnerCode        :"",
                         userCode           :"",
                         referenceNumber    :"",
                         systemCode         :"",
                         moduleCode         :"CLIM",
                         functionalityCode: "CLTA-VC",
                         systemHostAddress  :"",
                         remoteUserAddress  :"",
                         dateTime           :""
                       },
                       data:{
                         clientIds:uniqueCLIds
                       }
                     }
                     clReqData['header'].userCode = this.logedInUserRoles.userCode;
                     clReqData['header'].partnerCode = this.logedInUserRoles.partnerCode;
                     clReqData['header'].functionalityCode = "CLTA-VC";
                     // var clReqData       = {clientIds:uniqueCLIds}
                     this.patientnService.getClientByIds(clURL,clReqData).then(selectedClient => {
                       // console.log("this.selectedClient",selectedClient);
                       for (let i = 0; i < resp['data']['patients'].length; i++) {
                         for (let j = 0; j < selectedClient['data']['length']; j++) {
                           if (resp['data']['patients'][i].clientId == selectedClient['data'][j].clientId) {
                             resp['data']['patients'][i].clientName = selectedClient['data'][j].clientName;
                           }

                         }
                       }
                       this.nonEligiblePatients                 = resp['data']['patients'];
                       this.totalPatientsCoun3          = resp['data']['totalPatients'];
                       callback({
                         recordsTotal    :  this.totalPatientsCoun3,
                         recordsFiltered :  this.totalPatientsCoun3,
                         data: []
                       });
                     });
                   }
                   else{
                     this.nonEligiblePatients                 = resp['data']['patients'];
                     this.totalPatientsCoun3          = resp['data']['totalPatients'];
                     callback({
                       recordsTotal    :  this.totalPatientsCoun3,
                       recordsFiltered :  this.totalPatientsCoun3,
                       data: []
                     });
                   }

                 })
                 .catch(error => {
                   // Set loader false
                   // this.loading = false;

                   this.ngxLoader.stop();
                   this.notifier.notify( "error", "Error while loading non-eligible patiens");
                 });

               }
               else{
                 $('.dataTables_processing').css('display',"none");
                 if (this.oldCharacterCount3 == 3) {
                   this.oldCharacterCount3     = 3

                 }
                 else{
                   this.oldCharacterCount3     = 2;
                 }
               }
             }


           }

           // console.log(`The DataTable ${index} instance ID is: ${dtInstance.table().node().id}`);
         });
       });


      },
      columns  : [
        { data : 'SPD-002' },
        { data : 'firstName' },
        { data : 'lastName' },
        { data : 'username' },
        { data : 'phone' },
        { data : 'email' },
        { data : 'action' }
      ],

      ///for search
    };
    this.dtOptions[3] = {
      pagingType   : 'full_numbers',
      pageLength   : this.paginateLength,
      serverSide   : true,
      processing   : true,
      ordering     : false,
      searching       : false,
      "lengthMenu": [20, 50, 75, 100 ],
      ajax: (dataTablesParameters: any, callback) => {
        if (this.triggerHistoryTable == true) {


          this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {

            dtElement.dtInstance.then((dtInstance: any) => {
              if (dtInstance.table().node().id == 'spec-col-history' ) {
                this.paginatePage4          = dtElement['dt'].page.info().page;
                this.paginateLength         = dtElement['dt'].page.len();
                if (dataTablesParameters.search.value == "" && (this.oldCharacterCount4 == 0 || this.oldCharacterCount4 == 3)) {
                  if (this.oldCharacterCount4 == 3) {
                  }
                  var reqURL = environment.API_SPECIMEN_ENDPOINT + 'findcasesbypatientid';
                  var reqData={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "SPCA-MC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{page:this.paginatePage4,size:"20",sortColumn:"caseNumber",sortingOrder:"asc",patientIds:[this.selectedpatientId]}};
                  this.patientnService.caseByPatientId(reqURL, reqData).subscribe(respCases =>{
                    console.log("respCases",respCases);
                    var uniqueClientId = [];
                    const map = new Map();
                    for (const item of respCases['data']['caseDetails']) {
                      if(!map.has(item.clientId)){
                        map.set(item.clientId, true);    // set any value to Map
                        uniqueClientId.push(item.clientId);

                      }
                    }
                    var clURL           = environment.API_CLIENT_ENDPOINT + "clientidsnames"
                    var clReqData = {
                      header:{
                        uuid               :"",
                        partnerCode        :"",
                        userCode           :"",
                        referenceNumber    :"",
                        systemCode         :"",
                        moduleCode         :"CLIM",
                        functionalityCode: "CLTA-VC",
                        systemHostAddress  :"",
                        remoteUserAddress  :"",
                        dateTime           :""
                      },
                      data:{
                        clientIds:uniqueClientId
                      }
                    }
                    clReqData['header'].userCode = this.logedInUserRoles.userCode;
                    clReqData['header'].partnerCode = this.logedInUserRoles.partnerCode;
                    clReqData['header'].functionalityCode = "CLTA-VC";
                    // var clReqData       = {clientIds:uniqueClientId}
                    this.patientnService.getPatientsByIds(clURL,clReqData).then(selectedClient => {
                      console.log("selectedClient",selectedClient);
                      for (let i = 0; i < respCases['data']['caseDetails'].length; i++) {
                        for (let j = 0; j <  selectedClient['data']['length']; j++) {
                          if (respCases['data']['caseDetails'][i].clientId == selectedClient['data'][j].clientId) {
                            respCases['data']['caseDetails'][i].clientName = selectedClient['data'][j].clientName;
                          }

                        }
                        if (respCases['data']['caseDetails'][i].caseStatusId == 1 || respCases['data']['caseDetails'][i].caseNumber == null || respCases['data']['caseDetails'][i].caseNumber == '') {
                          var a = respCases['data']['caseDetails'][i].caseId+';2';
                          var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
                          var bx = ax.replace('+','xsarm');
                          var cx = bx.replace('/','ydan');
                          var ciphertext = cx.replace('=','zhar');
                          console.log('ciphertext',ciphertext);
                          var ur = location.origin+'/edit-case/'+ciphertext+'/1';
                          respCases['data']['caseDetails'][i].caseUrl = ur;
                          // respCases['data']['caseDetails'][i].caseUrl = "edit-case/"+respCases['data']['caseDetails'][i].caseId+"/1;from=2";
                        }
                        else{
                          var a = respCases['data']['caseDetails'][i].caseNumber+';0';
                          var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
                          var bx = ax.replace('+','xsarm');
                          var cx = bx.replace('/','ydan');
                          var ciphertext = cx.replace('=','zhar');
                          console.log('ciphertext',ciphertext);
                          var ur = location.origin+'/edit-case/'+ciphertext+'/1';
                          respCases['data']['caseDetails'][i].caseUrl = ur;
                          // respCases['data']['caseDetails'][i].caseUrl = "edit-case/"+respCases['data']['caseDetails'][i].caseNumber+'/1';
                        }
                        respCases['data']['caseDetails'][i].pFname = this.selectedpFname;
                        respCases['data']['caseDetails'][i].pLname = this.selectedpLname;
                        respCases['data']['caseDetails'][i].pDob   = this.selectedpDob;
                        for (let sp = 0; sp < this.allSpecimenTypes.length; sp++) {
                          if (typeof respCases['data']['caseDetails'][i]['caseSpecimen'] != 'undefined') {
                            if (this.allSpecimenTypes[sp].specimenTypeId == respCases['data']['caseDetails'][i]['caseSpecimen'][0].specimenTypeId) {
                              respCases['data']['caseDetails'][i]['caseSpecimen'][0].specimenPrefix = this.allSpecimenTypes[sp].specimenTypePrefix;
                            }
                          }

                        }

                      }



                      this.slectedPatientCases = respCases['data']['caseDetails'];
                      // $('#spec-collectionhistory-Modal').modal('show');
                      this.ngxLoader.stop();
                      this.totalSpecimenCount = respCases['data']['totalCount'];
                      callback({
                        recordsTotal    :  this.totalSpecimenCount,
                        recordsFiltered :  this.totalSpecimenCount,
                        data: []
                      });

                    });

                  })


                  // reqAllPatients.data.patientType   = 1;
                  this.oldCharacterCount4     = 3;

                }
                else{
                  if (dataTablesParameters.search.value.length >=3) {
                    $('.dataTables_processing').css('display',"none");
                    if (this.oldCharacterCount4 == 3) {
                      this.oldCharacterCount4     = 3
                    }
                    else{
                      this.oldCharacterCount4     = 2;
                    }
                    // var searchUrl     = environment.API_SPECIMEN_ENDPOINT + 'searchcases';
                    // var searchReq     = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "PNTA-MP",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{page:"0",size:"20",sortColumn:"caseNumber",sortingOrder:"desc",searchDate:"20",caseNumber: dataTablesParameters.search.value, caseStatus:0}}
                    //  this.patientnService.caseByPatientId(searchUrl, searchReq).subscribe(respCases =>{
                    //   console.log("search cases response",respCases);
                    //   var uniqueClientId = [];
                    //   const map = new Map();
                    //   for (const item of respCases['data']['caseDetails']) {
                    //     if(!map.has(item.clientId)){
                    //       map.set(item.clientId, true);    // set any value to Map
                    //       uniqueClientId.push(item.clientId);
                    //
                    //     }
                    //   }
                    //   var clURL           = environment.API_CLIENT_ENDPOINT + "clientsname"
                    //   var clReqData = {
                    //     header:{
                    //       uuid               :"",
                    //       partnerCode        :"",
                    //       userCode           :"",
                    //       referenceNumber    :"",
                    //       systemCode         :"",
                    //       moduleCode         :"CLIM",
                    //       functionalityCode: "PNTA-MP",
                    //       systemHostAddress  :"",
                    //       remoteUserAddress  :"",
                    //       dateTime           :""
                    //     },
                    //     data:{
                    //       clientIds:uniqueClientId
                    //     }
                    //   }
                    //   // var clReqData       = {clientIds:uniqueClientId}
                    //   this.patientnService.getPatientsByIds(clURL,clReqData).then(selectedClient => {
                    //     console.log("selectedClient",selectedClient);
                    //     for (let i = 0; i < respCases['data']['caseDetails'].length; i++) {
                    //       for (let j = 0; j <  selectedClient['data']['length']; j++) {
                    //         if (respCases['data']['caseDetails'][i].clientId == selectedClient['data'][j].clientId) {
                    //           respCases['data']['caseDetails'][i].clientName = selectedClient['data'][j].name;
                    //         }
                    //
                    //       }
                    //       if (respCases['data']['caseDetails'][i].caseStatusId == 1 || respCases['data']['caseDetails'][i].caseNumber == null || respCases['data']['caseDetails'][i].caseNumber == '') {
                    //         respCases['data']['caseDetails'][i].caseUrl = environment.UI_SPECIMEN_ENDPOINT + "/edit-case/"+respCases['data']['caseDetails'][i].caseId+";from=2";
                    //       }
                    //       else{
                    //         respCases['data']['caseDetails'][i].caseUrl = environment.UI_SPECIMEN_ENDPOINT + "/edit-case/"+respCases['data']['caseDetails'][i].caseNumber;
                    //       }
                    //       respCases['data']['caseDetails'][i].pFname = this.selectedpFname;
                    //       respCases['data']['caseDetails'][i].pLname = this.selectedpLname;
                    //       respCases['data']['caseDetails'][i].pDob   = this.selectedpDob;
                    //
                    //     }
                    //
                    //     this.slectedPatientCases = respCases['data']['caseDetails'];
                    //     // $('#spec-collectionhistory-Modal').modal('show');
                    //     this.ngxLoader.stop();
                    //     this.totalSpecimenCount = respCases['data']['totalCount'];
                    //     callback({
                    //       recordsTotal    :  this.totalSpecimenCount,
                    //       recordsFiltered :  this.totalSpecimenCount,
                    //       data: []
                    //     });
                    //
                    //   });
                    //
                    // })
                    //
                    // ///////////// goes here
                    // // this.oldCharacterCount4    = 3;
                  }
                  else{
                    $('.dataTables_processing').css('display',"none");
                    if (this.oldCharacterCount4 == 3) {
                      this.oldCharacterCount4     = 3
                    }
                    else{
                      this.oldCharacterCount4     = 2;
                    }
                  }
                }

              }
              // console.log(`The DataTable ${index} instance ID is: ${dtInstance.table().node().id}`);
            });
          });


        }
      },
      // columns  : [
      //   { data : 'SPD-002' },
      //   { data : 'firstName' },
      //   { data : 'lastName' },
      //   { data : 'username' },
      //   { data : 'phone' },
      //   { data : 'email' },
      //   { data : 'action' }
      // ]
      ///for search
    };


  }
  ngAfterViewInit(): void {
    if(this.logedInUserRoles['allowedRoles'].indexOf("PNTA-MP") != -1){
      this.dtTrigger.next();
      // this.rbac.checkROle('PNTA-MP');
    }
  // this.dtTrigger.next();
}
ngOnDestroy(): void {
  // Do not forget to unsubscribe the event
  this.dtTrigger.unsubscribe();
}

popClientSpecData(clURL,clReqData,resp,uniquePIds){
  return new Promise((resolve, reject) => {

    var byPidURL = environment.API_SPECIMEN_ENDPOINT + 'findbypatientid';
   var reqPid={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"PNTA-MP",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{patientIds:uniquePIds}}

    var patientout = this.patientnService.getPatientsByIds(byPidURL, reqPid).then(patient=>{
        return patient;
      })
      var clientsout = this.patientnService.getClientByIds(clURL,clReqData).then(respClient => {
        console.log('respClient REQ',respClient);

        return respClient['data'];
      });
    forkJoin([clientsout, patientout]).subscribe(bothresults => {
      var selectedClient  = bothresults[0];
      var selectedPatient = bothresults[1];
      for (let i = 0; i < resp['data']['patients'].length; i++) {
        for (let j = 0; j < selectedClient['length']; j++) {
          if (resp['data']['patients'][i].clientId == selectedClient[j].clientId) {
            resp['data']['patients'][i].clientName = selectedClient[j].clientName;
            for (let k = 0; k < selectedClient[j]['clientRequisitionTypeDto'].length; k++) {
              if (selectedClient[j]['clientRequisitionTypeDto'][k]['formId'] == 1) {
                resp['data']['patients'][i].reqsuisitionType = true;
              }
              else{
                if (typeof resp['data']['patients'][i].reqsuisitionType == 'undefined') {
                  resp['data']['patients'][i].reqsuisitionType = false;
                }
              }

            }
            // resp['data']['patients'][i].reqsuisitionType = selectedClient[j]['clientRequisitiontypes'];
          }

        }
      }
      for (let i = 0; i < resp['data']['patients'].length; i++) {
        for (let j = 0; j < selectedPatient['data']['length']; j++) {
          if (resp['data']['patients'][i].patientId == selectedPatient['data'][j].patientId) {
            // console.log("selectedPatient['data'][j]",selectedPatient['data'][j]);
            if (selectedPatient['data'][j]['caseSpecimen'].length > 0) {
              var cbyname =""
              var specPre ="";
              if (selectedPatient['data'][j].collectedBy == 1) {
                cbyname = "Sip"
              }
              else if(selectedPatient['data'][j].collectedBy == 2){
                cbyname = "Client"
              }
              for (let sp = 0; sp < this.allSpecimenTypes.length; sp++) {
                if (this.allSpecimenTypes[sp].specimenTypeId == selectedPatient['data'][j]['caseSpecimen'][0].specimenTypeId) {
                  specPre = this.allSpecimenTypes[sp].specimenTypePrefix;
                }
              }
              resp['data']['patients'][i].collectedBy      = selectedPatient['data'][j].collectedBy;
              resp['data']['patients'][i].collectionDate   = selectedPatient['data'][j].collectionDate;
              resp['data']['patients'][i].lastCollected    = specPre + "Collected By" + cbyname+' AT ' + this.datePipe.transform(selectedPatient['data'][j].collectionDate,'MM/dd/yyyy HH:mm');
            }
            else{
              resp['data']['patients'][i].lastCollected = "No Case Created"
            }


            resp['data']['patients'][i].caseId = selectedPatient['data'][j].caseId;
          }

        }
      }
      this.allPatients                 = resp['data']['patients'];
      this.totalPatientsCount          = resp['data']['totalPatients'];
      resolve();
    })
  //   this.patientnService.getClientByIds(clURL,clReqData).then(selectedClient => {
  //     // console.log("this.selectedClient",selectedClient);
  //
  //
  //   })
  // .catch(error => {
  //
  //   console.log("error: ",error);
  //   this.notifier.notify( "error", "Error while loading all patiens");
  //   resolve();
  // });
    })


}

deactivatePatient(){
  console.log("patientToDisable",this.patientToDisable);
  var requestTemplate={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"",functionalityCode: "PNTA-MP",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{username:"danish",patientId:this.patientToDisable,status:true}}
  var url = environment.API_PATIENT_ENDPOINT + 'changestatus'
  this.patientnService.togglePatientStatus(url, requestTemplate).subscribe(resp =>{
    $('#deactivateModal').modal('hide');

    if (resp['result']['codeType'] == 'S') {
      this.notifier.notify( "success", "Patient deactivated successfully");
    }
    else{
      this.notifier.notify( "error", resp['result']['description']);
    }
  },
  error=>{
    this.notifier.notify( "error", "Error while changing status check DB connection");

  })
}

exportPatient(from){
  // console.log("from", from);
  var url = environment.API_PATIENT_ENDPOINT + 'exportpatients';
  if (from == 'all') {
    this.ngxLoader.start();
    var reqDataBP = {...this.exportRequestData}
    reqDataBP.data.searchCriteria = this.searchCreateriaHolderall;
    reqDataBP.data.eligibility    = "";
    reqDataBP['header'].userCode = this.logedInUserRoles.userCode;
    reqDataBP['header'].partnerCode = this.logedInUserRoles.partnerCode;
    reqDataBP['header'].functionalityCode = "PNTA-MP";
    if (this.fromtClient==true) {
      reqDataBP.data.clientId    = this.selectedClientID;
    }
    // console.log("searchCreateria",this.searchCreateriaHolderall);
    this.patientnService.exportPatients(url, reqDataBP).subscribe(allresp => {
      console.log("export all resp", allresp);
      this.ngxLoader.stop();
      if (allresp['result'].codeType == "S") {
        window.open(allresp['data']['exportedPatients'], "_blank");
      }
      else{
        this.notifier.notify( "error", allresp['result'].description);
      }


    },
    error=>{
      this.notifier.notify( "error", "Error while exporting Patients check DB connection");
    })
  }
  else if(from == 'el'){
    this.ngxLoader.start();
    var reqDataBP = {...this.exportRequestData}
    reqDataBP.data.searchCriteria = this.searchCreateriaHoldereligible;
    reqDataBP.data.eligibility    = 1;
    reqDataBP['header'].userCode = this.logedInUserRoles.userCode;
    reqDataBP['header'].partnerCode = this.logedInUserRoles.partnerCode;
    reqDataBP['header'].functionalityCode = "PNTA-MP";
    if (this.fromtClient==true) {
      reqDataBP.data.clientId    = this.selectedClientID;
    }
    this.patientnService.exportPatients(url, reqDataBP).subscribe(allresp => {
      console.log("export eligible resp", allresp);
      this.ngxLoader.stop();
      if (allresp['result'].codeType == "S") {
        window.open(allresp['data']['exportedPatients'], "_blank");
      }
      else{
        this.notifier.notify( "error", allresp['result'].description);
      }


    },
    error=>{
      this.notifier.notify( "error", "Error while exporting Patients check DB connection");
    })
  }
  else if(from == 'non'){
    this.ngxLoader.start();
    var reqDataBP = {...this.exportRequestData}
    reqDataBP.data.searchCriteria = this.searchCreateriaHoldernoneligible;
    reqDataBP.data.eligibility    = 0;
    reqDataBP['header'].userCode = this.logedInUserRoles.userCode;
    reqDataBP['header'].partnerCode = this.logedInUserRoles.partnerCode;
    reqDataBP['header'].functionalityCode = "PNTA-MP";
    if (this.fromtClient==true) {
      reqDataBP.data.clientId    = this.selectedClientID;
    }
    this.patientnService.exportPatients(url, reqDataBP).subscribe(allresp => {
      console.log("export non eligible resp", allresp);
      this.ngxLoader.stop();
      if (allresp['result'].codeType == "S") {
        window.open(allresp['data']['exportedPatients'], "_blank");
      }
      else{
        this.notifier.notify( "error", allresp['result'].description);
      }


    },
    error=>{
      this.notifier.notify( "error", "Error while exporting Patients check DB connection");
    })
  }
}

onFileSelect(input){
  console.log('size',input.files[0]);
  this.fileName = input.files[0].name;
    var mb = input.files[0].size/1048576;
    console.log("mb",mb);
    if (mb <= 1) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = (e: any) => {
          console.log('Got here: ', e.target.result);
          // console.log('input.files[0]: ', input.files[0]);

          this.fileBase64 = e.target.result;
          // this.obj.photoUrl = e.target.result;
        }
        reader.readAsDataURL(input.files[0]);
      }
      // console.log('this.imageBase64: ', this.imageBase64);
      if(input.files && input.files.length > 0) {
        this.filePayLoad = input.files[0];
      }
    }
    else{
      alert('File Size must be less then 1MB');
    }
}

importPatient(){
  var url = environment.API_PATIENT_ENDPOINT + 'importpatients';
  if (this.fileBase64 == "" || typeof this.fileBase64 !="undefined") {
    this.ngxLoader.start();
    var reqDataBP={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"",functionalityCode: "PNTA-MP",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{userName:"danish",fileName:this.fileName,importedFile:this.fileBase64}}
    console.log("this.fileBase64",this.fileBase64);

    this.patientnService.importPatients(url, reqDataBP).subscribe(allresp => {
      console.log("import resp", allresp);
      this.ngxLoader.stop();
      if (allresp['result'].codeType == "S") {
        $('#importModal').modal('hide');
        $('#import-successModal').modal('show');
        this.successFileURL = this.sanitizer.bypassSecurityTrustUrl(allresp['data'].exportedPatients);
        this.successCount   = allresp['data'].successCount;
        this.failureCount   = allresp['data'].failedCount;
        $('#importPatientFIle').val('');
        // window.open(allresp['data']['exportedPatients'], "_blank");
      }
      else{
        $('#importPatientFIle').val('');
        $('#importModal').modal('hide');
        this.notifier.notify( "error", allresp['result'].description);
      }


    },
    error=>{
      this.notifier.notify( "error", "Error while exporting Patients check DB connection");
    })

  }
  else{
    this.ngxLoader.stop();
    alert("Please Select a file first");
  }



}
createMinCase(patientId, clientId, collectedBy, collectionDate, typeId, pLName, pFName, pDob, prifix, clientName){
  this.ngxLoader.start();
  var checkTodayCaseUrl = environment.API_SPECIMEN_ENDPOINT+'caseexists';
  var checkTodayCaseReq = {
    header:
    {
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"SPCM",
      functionalityCode: "PNTA-MP",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      patientId:patientId,
      specimenTypeId:typeId
    }
  }
  checkTodayCaseReq['header'].userCode = this.logedInUserRoles.userCode;
  checkTodayCaseReq['header'].partnerCode = this.logedInUserRoles.partnerCode;
  checkTodayCaseReq['header'].functionalityCode = "PNTA-MP";
  console.log('checkTodayCaseReq',checkTodayCaseReq);

  // return;
  this.patientnService.checkCases(checkTodayCaseUrl,checkTodayCaseReq).subscribe(checkResp=>{
    console.log('checkResp',checkResp);
    // return;
    if (checkResp['data'].length == 0) {
      var d = new Date();

      var defaultPhysician = 0;
      if (typeof collectionDate == "undefined") {
        collectionDate = this.convertAge(d);
      }
      if (typeof collectedBy == "undefined") {
        collectedBy = 1;
      }
      // this.printLabel("cd123",pLName,pFName,pDob)
      var atdPhyUrl   =  environment.API_CLIENT_ENDPOINT + "clientidnamephysician";
      var reqTemplate = {
        header:{
          uuid               :"",
          partnerCode        :"",
          userCode           :"",
          referenceNumber    :"",
          systemCode         :"",
          moduleCode         :"CLIM",
          functionalityCode: "CLTA-MC",
          systemHostAddress  :"",
          remoteUserAddress  :"",
          dateTime           :""
        },
        data:{
          clientIds:[clientId],
          userCodes: []
        }
      }
      reqTemplate['header'].userCode = this.logedInUserRoles.userCode;
      reqTemplate['header'].partnerCode = this.logedInUserRoles.partnerCode;
      reqTemplate['header'].functionalityCode = "CLTA-MC";
      // var reqTemplate = {clientIds:[clientId]}
      this.patientnService.getClientByIds(atdPhyUrl,reqTemplate).then(respClient => {
        console.log("respClientPhysician",respClient);
        if (respClient['data'].length == 0) {
          alert('No Physician Associated to this Client')
          this.ngxLoader.stop();
          return;
        }else{
          if (typeof respClient['data'][0]['clientPhysician'] == 'undefined') {
            alert('No Physician Associated to this Client')
            this.ngxLoader.stop();
            return;
          }else{
            defaultPhysician = respClient['data'][0]['clientPhysician'];

          }
        }
        // if (respClient['data'][0]['clientUsers'].length == 0) {
        //   alert('No Physician Associated to this Client')
        //   this.ngxLoader.stop();
        //   return;
        // }
        // else {
        //   var physCount = 0; /////// if there is no default physician then slect 1s as physician
        //   var arrCount  = 0;
        //   for (let i = 0; i < respClient['data'][0]['clientUsers'].length; i++) {
        //     if (respClient['data'][0]['clientUsers'][i]['defaultPhysician'] == 1) {
        //       defaultPhysician = respClient['data'][0]['clientUsers'][i]['userCode'];
        //       physCount ++;
        //     }
        //     arrCount = arrCount+ 1;
        //     if (arrCount == respClient['data'][0]['clientUsers'].length) {
        //       if (physCount == 0) {
        //         defaultPhysician = respClient['data'][0]['clientUsers'][0]['userCode'];
        //       }
        //     }
        //
        //   }
        // }


        this.saveMinCase(patientId,clientId,collectedBy,collectionDate,defaultPhysician,prifix,pLName,pFName,pDob, typeId, clientName)

      },
      error =>{
        this.notifier.notify('error','Error while fetching default physicians');
      });
    }
    else{
      var myDate = new Date();
      var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy');
      this.ngxLoader.stop();
      var a = checkResp['data'][0]['caseNumber']+';3';
      var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
      var bx = ax.replace('+','xsarm');
      var cx = bx.replace('/','ydan');
      var ciphertext = cx.replace('=','zhar');
      console.log('ciphertext',ciphertext);
      var ur = location.origin+'/edit-case/'+ciphertext+'/1';
      console.log("url",ur);


      Swal.fire({
        title: 'Hi!',
        html: "The case <a style='color:blue' target='_blank' href='"+ur+"'>"+checkResp['data'][0]['caseNumber']+"</a> has already been created today "+currentDate+" for this specimen type, Are you sure you want to create a case again?",
        // html: "The case <a style='color:blue' target='_blank' href='edit-case/"+checkResp['data'][0]['caseNumber']+"/1;from=3'>"+checkResp['data'][0]['caseNumber']+"</a> has already been created today "+currentDate+" for this specimen type, Are you sure you want to create a case again?",
        // html: "The case <a style='color:blue' target='_blank' routerLink='edit-case/"+checkResp['data'][0]['caseNumber']+"/1;from=3'>"+checkResp['data'][0]['caseNumber']+"</a> has already been created today "+currentDate+" for this specimen type, Are you sure you want to create a case again?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Create Case!'
      }).then((result) => {
        if (result.value) {
          this.ngxLoader.start();
          var d = new Date();

          var defaultPhysician = 0;
          if (typeof collectionDate == "undefined") {
            collectionDate = this.convertAge(d);
          }
          if (typeof collectedBy == "undefined") {
            collectedBy = 1;
          }
          // this.printLabel("cd123",pLName,pFName,pDob)
          var atdPhyUrl   =  environment.API_CLIENT_ENDPOINT + "clientidnamephysician";
          var reqTemplate = {
            header:{
              uuid               :"",
              partnerCode        :"",
              userCode           :"",
              referenceNumber    :"",
              systemCode         :"",
              moduleCode         :"CLIM",
              functionalityCode: "PNTA-MP",
              systemHostAddress  :"",
              remoteUserAddress  :"",
              dateTime           :""
            },
            data:{
              clientIds:[clientId],
              userCodes: []
            }
          }
          reqTemplate['header'].userCode = this.logedInUserRoles.userCode;
          reqTemplate['header'].partnerCode = this.logedInUserRoles.partnerCode;
          reqTemplate['header'].functionalityCode = "PNTA-MP";
          // var reqTemplate = {clientIds:[clientId]}
          this.patientnService.getClientByIds(atdPhyUrl,reqTemplate).then(respClient => {
            console.log("respClient",respClient);
            if (respClient['data'].length == 0) {
              alert('No Physician Associated to this Client')
              this.ngxLoader.stop();
              return;
            }else{
              if (typeof respClient['data'][0]['clientPhysician'] == 'undefined') {
                alert('No Physician Associated to this Client')
                this.ngxLoader.stop();
                return;
              }else{
                defaultPhysician = respClient['data'][0]['clientPhysician'];

              }
            }
            // if (respClient['data'][0]['clientUsers'].length == 0) {
            //   alert('No Physician Associated to this Client')
            //   this.ngxLoader.stop();
            //   return;
            // }
            // else {
            //   var physCount = 0; /////// if there is no default physician then slect 1s as physician
            //   var arrCount  = 0;
            //   for (let i = 0; i < respClient['data'][0]['clientUsers'].length; i++) {
            //     if (respClient['data'][0]['clientUsers'][i]['defaultPhysician'] == 1) {
            //       defaultPhysician = respClient['data'][0]['clientUsers'][i]['userCode'];
            //       physCount ++;
            //     }
            //     arrCount = arrCount+ 1;
            //     if (arrCount == respClient['data'][0]['clientUsers'].length) {
            //       if (physCount == 0) {
            //         defaultPhysician = respClient['data'][0]['clientUsers'][0]['userCode'];
            //       }
            //     }
            //
            //   }
            // }


            this.saveMinCase(patientId,clientId,collectedBy,collectionDate,defaultPhysician,prifix,pLName,pFName,pDob, typeId, clientName)

          },
          error =>{
            this.notifier.notify('error','Error while fetching default physicians');
          });

        }
          else if (result.dismiss === Swal.DismissReason.cancel) {
            this.ngxLoader.stop();
            return;
          }
      })
      // this.notifier.notify('info','A case is allready created with same type for same patien')
    }

  })


}

saveMinCase(patientId,clientId,collectedBy,collectionDate,defaultPhysician,prifix,pLName,pFName,pDob, typeId, clientName){
    this.ngxLoader.start();
    var myDate = new Date();
    var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');
    collectionDate = this.datePipe.transform(collectionDate, 'MM-dd-yyyy HH:mm:ss');
    this.saveRequest.data.caseStatusId    = 3;
    this.saveRequest.data.patientId       = patientId;
    this.saveRequest.data.clientId        = clientId;
    this.saveRequest.data.collectionDate  = collectionDate;
    this.saveRequest.data.receivingDate   = collectionDate;
    this.saveRequest.data.collectedBy     = collectedBy;
    this.saveRequest.data.attendingPhysicianId = defaultPhysician;
    this.saveRequest.data.createdTimestamp = currentDate;
    this.saveRequest.data.accessionTimestamp = currentDate;
    this.saveRequest.data.createdBy = this.logedInUserRoles.userCode;
    this.saveRequest['header'].userCode = this.logedInUserRoles.userCode;
    this.saveRequest['header'].partnerCode = this.logedInUserRoles.partnerCode;
    this.saveRequest['header'].functionalityCode = "PNTA-MP";
    // this.saveRequest.data.caseCategoryPrefix = prifix;
    for (let i = 0; i < this.allCaseCategories.length; i++) {
      // console.log('this.allCaseCategories[i]',this.allCaseCategories[i]);
      if (this.allCaseCategories[i].caseCategoryName.toLowerCase().indexOf('nursing')>-1) {
        // console.log('this.allCaseCategories[i]',this.allCaseCategories[i]);

        this.saveRequest.data.caseCategoryId      = this.allCaseCategories[i].caseCategoryId;
        this.saveRequest.data.caseCategoryPrefix  = this.allCaseCategories[i].caseCategoryPrefix;
        // this.selectedCaseCat = this.allCaseCategories[i]
        // this.saveRequest.data.caseCategoryId     = this.selectedCaseCat.caseCategoryId;
        // this.saveRequest.data.caseCategoryPrefix = this.selectedCaseCat.caseCategoryPrefix;
      }
    }
    // this.saveRequest.data.caseCategoryId      = 1;
    // this.saveRequest.data.caseCategoryPrefix  = 'NV';
    console.log("typeId",typeId);

    this.saveRequest.header.functionalityCode = "SPCA-CQ"

    this.saveRequest.data.caseSpecimen[0]['specimenTypeId'] = typeId;
    this.saveRequest.data.caseSpecimen[0]['createdTimestamp'] = currentDate;
    this.saveRequest.data.caseSpecimen[0]['createdBy'] = this.logedInUserRoles.userCode;
    this.saveRequest.data.caseSpecimen[0]['updatedBy'] = this.logedInUserRoles.userCode;
    console.log("this.saveRequest",this.saveRequest);
    var saveminUrl = environment.API_SPECIMEN_ENDPOINT + "minaccession";
    // this.ngxLoader.stop();return;
    this.patientnService.saveMinAccession(saveminUrl,this.saveRequest).subscribe(saveResp =>{
      console.log("saveResp",saveResp);
      if (typeof saveResp['error'] != 'undefined') {
        this.notifier.notify( "error", "Error while saving case" );
        this.ngxLoader.stop();
        return;
      }
      this.notifier.notify( "success", "Case saved successfully" );
      this.printedCaseNumber = saveResp['data'].caseNumber;
      this.printedCaseCategory = prifix;
      var a = this.printedCaseNumber+';3';
      var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
      var bx = ax.replace('+','xsarm');
      var cx = bx.replace('/','ydan');
      var ciphertext = cx.replace('=','zhar');
      console.log('ciphertext',ciphertext);
      var ur = location.origin+'/edit-case/'+ciphertext+'/1';

      this.caseUrl =ur;
      // this.caseUrl ="edit-case/"+this.printedCaseNumber+"/1;from=3";
      this.ngxLoader.stop();
      $('#successcreate-modal').modal('show');
      this.triageCase(saveResp['data'].caseId);
      this.printLabel(this.printedCaseNumber,pLName,pFName,pDob, prifix, clientName,clientId)


    },
    error=>{
      this.notifier.notify( "error", "Error while saving case backend exception" );
    })



  }
  triageCase(caseId){
    // console.log("casid",caseId);

    var myDate = new Date();
    var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyy HH:mm:ss');
    var triageUrl = environment.API_COVID_ENDPOINT + 'triage';
    var triageRequest = {...this.triageRequest}
    triageRequest.data.caseId = caseId;
    triageRequest.data.testStatusId = 1;
    triageRequest.data.testId = 2;
    triageRequest.data.createdTimestamp = currentDate;
    triageRequest.data.createdBy = this.logedInUserRoles.userCode;
    triageRequest['header'].userCode = this.logedInUserRoles.userCode;
    triageRequest['header'].partnerCode = this.logedInUserRoles.partnerCode;
    triageRequest['header'].functionalityCode = "PNTA-MP";
    this.patientnService.triageRequest(triageUrl,triageRequest).subscribe(triageResp=>{
      console.log('triageResp',triageResp);
      this.ngxLoader.stop();

    },error=>{
      this.notifier.notify('error','error while triaging')
      this.ngxLoader.stop();
    })


  }

  caseBypatientId(patientId,pFname,pLname,pDob){
    this.selectedpatientId = patientId;
    this.selectedpFname    = pFname;
    this.selectedpLname    = pLname;
    this.selectedpDob      = pDob;
    this.triggerHistoryTable = true;
    this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
        dtElement.dtInstance.then((dtInstance: any) => {
          if (dtInstance.table().node().id == 'spec-col-history' ) {
            // dtInstance.destroy();
            // dtInstance.draw();
            dtInstance.ajax.reload()
            // dtInstance.columns().every(function () {
            //   $(this.header())
            //     .search()
            //     .draw();
            // })
            // dtInstance.destroy();

          }
          // dtInstance.destroy();
        })
      })
      $('#spec-collectionhistory-Modal').modal('show');

      // this.dtTrigger.next();
    // this.ngxLoader.start();
    // var reqURL = environment.API_SPECIMEN_ENDPOINT + 'findcasesbypatientid';
    // var reqData={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"",functionalityCode: "PNTA-MP",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{page:"0",size:"2",sortColumn:"caseNumber",sortingOrder:"asc",patientIds:[patientId]}};
    // this.patientnService.caseByPatientId(reqURL, reqData).subscribe(respCases =>{
    //   console.log("respCases",respCases);
    //   var uniqueClientId = [];
    //   const map = new Map();
    //   for (const item of respCases['data']['caseDetails']) {
    //     if(!map.has(item.clientId)){
    //       map.set(item.clientId, true);    // set any value to Map
    //       uniqueClientId.push(item.clientId);
    //
    //     }
    //   }
    //   var clURL           = environment.API_CLIENT_ENDPOINT + "clientsname"
    //   var clReqData       = {clientIds:uniqueClientId}
    //   this.patientnService.getPatientsByIds(clURL,clReqData).then(selectedClient => {
    //     console.log("selectedClient",selectedClient);
    //     for (let i = 0; i < respCases['data']['caseDetails'].length; i++) {
    //       for (let j = 0; j <  selectedClient['length']; j++) {
    //         if (respCases['data']['caseDetails'][i].clientId == selectedClient[j].clientId) {
    //           respCases['data']['caseDetails'][i].clientName = selectedClient[j].name;
    //         }
    //
    //       }
    //       if (respCases['data']['caseDetails'][i].caseStatusId == 1 || respCases['data']['caseDetails'][i].caseNumber == null || respCases['data']['caseDetails'][i].caseNumber == '') {
    //         respCases['data']['caseDetails'][i].caseUrl = environment.UI_SPECIMEN_ENDPOINT + "/edit-case/"+respCases['data']['caseDetails'][i].caseId+";from=2";
    //       }
    //       else{
    //         respCases['data']['caseDetails'][i].caseUrl = environment.UI_SPECIMEN_ENDPOINT + "/edit-case/"+respCases['data']['caseDetails'][i].caseNumber;
    //       }
    //       respCases['data']['caseDetails'][i].pFname = pFname;
    //       respCases['data']['caseDetails'][i].pLname = pLname;
    //       respCases['data']['caseDetails'][i].pDob   = pDob;
    //
    //     }
    //
    //     this.slectedPatientCases = respCases['data']['caseDetails'];
    //     $('#spec-collectionhistory-Modal').modal('show');
    //     this.ngxLoader.stop();
    //
    //   });
    //
    // })
  }

  printLabel(caseNumber,lName,fName,dob, prifix, clientName,clientId){
    // console.log('dymo',dymo);

    if(typeof dymo == "undefined"){
      this.ngxLoader.stop();
      return;
    }
    // console.log("caseNumber",caseNumber);
    // console.log("fName",fName);
    // console.log("lName",lName);
    // console.log("dob",dob);
    // console.log("prifix",prifix);
    // return;
    // console.log("clientName",clientName);
    dob = this.datePipe.transform(dob, 'MM/dd/yyy');

    this.ngxLoader.start();
    console.log("caseNumber",caseNumber);
    var labelXml = '<DieCutLabel Version="8.0" Units="twips" MediaType="Default"> <PaperOrientation>Landscape</PaperOrientation> <Id>Small30332</Id> <IsOutlined>false</IsOutlined> <PaperName>30332 1 in x 1 in</PaperName> <DrawCommands> <RoundRectangle X="0" Y="0" Width="1440" Height="1440" Rx="180" Ry="180" /> </DrawCommands> <ObjectInfo> <TextObject> <Name>FirstLast</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <HorizontalAlignment>Left</HorizontalAlignment> <VerticalAlignment>Top</VerticalAlignment> <TextFitMode>ShrinkToFit</TextFitMode> <UseFullFontHeight>True</UseFullFontHeight> <Verticalized>False</Verticalized> <StyledText> <Element> <String xml:space="preserve">First Last</String> <Attributes> <Font Family="Arial" Size="5" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" HueScale="100" /> </Attributes> </Element> </StyledText> </TextObject> <Bounds X="144" Y="199.393133236425" Width="1186.49076398256" Height="120" /> </ObjectInfo> <ObjectInfo> <BarcodeObject> <Name>BARCODE</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <Text>12345</Text> <Type>QRCode</Type> <Size>Medium</Size> <TextPosition>None</TextPosition> <TextFont Family="Arial" Size="8" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <CheckSumFont Family="Arial" Size="8" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <TextEmbedding>None</TextEmbedding> <ECLevel>0</ECLevel> <HorizontalAlignment>Center</HorizontalAlignment> <QuietZonesPadding Left="0" Top="0" Right="0" Bottom="0" /> </BarcodeObject> <Bounds X="381.704497509707" Y="771.219004098847" Width="541.292842170495" Height="526.701837189909" /> </ObjectInfo> <ObjectInfo> <TextObject> <Name>Facility</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <HorizontalAlignment>Left</HorizontalAlignment> <VerticalAlignment>Top</VerticalAlignment> <TextFitMode>ShrinkToFit</TextFitMode> <UseFullFontHeight>True</UseFullFontHeight> <Verticalized>False</Verticalized> <StyledText> <Element> <String xml:space="preserve">Facility</String> <Attributes> <Font Family="Arial" Size="4" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" HueScale="100" /> </Attributes> </Element> </StyledText> </TextObject> <Bounds X="164.897098682172" Y="651.471024372647" Width="1183.87862664729" Height="120" /> </ObjectInfo> <ObjectInfo> <TextObject> <Name>DOB</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <HorizontalAlignment>Left</HorizontalAlignment> <VerticalAlignment>Top</VerticalAlignment> <TextFitMode>ShrinkToFit</TextFitMode> <UseFullFontHeight>True</UseFullFontHeight> <Verticalized>False</Verticalized> <StyledText> <Element> <String xml:space="preserve">DOB</String> <Attributes> <Font Family="Arial" Size="5" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" HueScale="100" /> </Attributes> </Element> </StyledText> </TextObject> <Bounds X="144" Y="310.963055930226" Width="1210" Height="120" /> </ObjectInfo> <ObjectInfo> <TextObject> <Name>Client</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <HorizontalAlignment>Left</HorizontalAlignment> <VerticalAlignment>Top</VerticalAlignment> <TextFitMode>ShrinkToFit</TextFitMode> <UseFullFontHeight>True</UseFullFontHeight> <Verticalized>False</Verticalized> <StyledText> <Element> <String xml:space="preserve">Client</String> <Attributes> <Font Family="Arial" Size="5" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" HueScale="100" /> </Attributes> </Element> </StyledText> </TextObject> <Bounds X="144" Y="418.060686676358" Width="1210" Height="120" /> </ObjectInfo> <ObjectInfo> <TextObject> <Name>CaseCode</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <HorizontalAlignment>Left</HorizontalAlignment> <VerticalAlignment>Top</VerticalAlignment> <TextFitMode>ShrinkToFit</TextFitMode> <UseFullFontHeight>True</UseFullFontHeight> <Verticalized>False</Verticalized> <StyledText> <Element> <String xml:space="preserve">Case Code</String> <Attributes> <Font Family="Arial" Size="5" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" HueScale="100" /> </Attributes> </Element> </StyledText> </TextObject> <Bounds X="144" Y="525.15831742249" Width="1210" Height="120" /> </ObjectInfo> </DieCutLabel>';
    var label = dymo.label.framework.openLabelXml(labelXml);

    // var barcodeData = 'Si Paradigm'
    label.setObjectText('BARCODE', caseNumber);
    // set label text
    label.setObjectText("FirstLast",lName +','+ fName);
    label.setObjectText("DOB",  dob);
    label.setObjectText("Facility", prifix);
    // label.setObjectText("Client", clientName.substring(0, 10));

    label.setObjectText("CaseCode", caseNumber);
    label.setObjectText("Client", clientName.substring(0, 10));


    var printers = dymo.label.framework.getPrinters();
    if (printers.length == 0){
      this.notifier.notify("error","No DYMO printers are installed. Install DYMO printers.")
      this.ngxLoader.stop()
      throw "No DYMO printers are installed. Install DYMO printers.";

    }
    else{
      var myDate = new Date();
      var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyy HH:mm:ss');
      var saveLabeRequest = {
        header: {
          uuid: "",
          partnerCode: "sip",
          userCode: "usyduysad",
          referenceNumber: "",
          systemCode: "",
          moduleCode: "SPCM",
          functionalityCode: "PNTA-MP",
          systemHostAddress: "",
          remoteUserAddress: "",
          dateTime: ""
        },
        data:
        {
          caseNumber: "",
          clientId: 1522,
          createdTimestamp: "",
          printedBy: ""
        }
      }

      saveLabeRequest.data.caseNumber = caseNumber;
      saveLabeRequest.data.clientId = clientId;
      saveLabeRequest.data.createdTimestamp = currentDate;
      saveLabeRequest.data.printedBy = this.logedInUserRoles.userCode;
      saveLabeRequest.header.userCode = this.logedInUserRoles.userCode;
      saveLabeRequest.header.partnerCode = this.logedInUserRoles.partnerCode;
      saveLabeRequest.header.functionalityCode = "PNTA-MP";

      var saveLabelUrl = environment.API_SPECIMEN_ENDPOINT+'savelabel';
      this.patientnService.printLabel(saveLabelUrl,saveLabeRequest).subscribe(saveLabelResponse =>{
        console.log('saveLabelResponse',saveLabelResponse);

      },error=>{
        this.notifier.notify("error","Error while saveing print history.")
      })

    }

    var printerName = "";
    for (var i = 0; i < printers.length; ++i)
    {
      var printer = printers[i];
      if (printer.printerType == "LabelWriterPrinter")
      {
        printerName = printer.name;
        break;
      }
    }

    label.print(printerName);
    this.printedCaseNumber = caseNumber;
    console.log("this.printedCaseNumber--",this.printedCaseNumber);
    // $('#pecimenlabel-Modal').modal('show');
    // $('#pecimenlabel-Modal').modal('show');
    this.ngxLoader.stop();

  }

  toggleActiveClient(type,patientId,index){
    var mes = '';
    if (type == true) {
      mes = "activate"
    }
    else{
      mes = "deactivate"
    }
    console.log("patientToDisable",patientId);
    console.log("type",type);
    var requestTemplate={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"",functionalityCode: "PNTA-MP",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{username:"danish",patientId:patientId,status:type}}
    var url = environment.API_PATIENT_ENDPOINT + 'changestatus';
    Swal.fire({
        title: 'Are you sure?',
        text: "Are you sure to "+ mes +" the patient!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, do it!'
      }).then((result) => {
        if (result.value) {
          this.ngxLoader.start()
          this.patientnService.togglePatientStatus(url, requestTemplate).subscribe(resp =>{
            $('#deactivateModal').modal('hide');
            this.ngxLoader.stop()
            if (resp['result']['codeType'] == 'S') {
              this.notifier.notify( "success", "Patient deactivated successfully");
              if (type == true) {
                this.allPatients[index].status = 1;
              }
              else{
                this.allPatients[index].status = 0;
              }

            }
            else{
              this.notifier.notify( "error", resp['result']['description']);
            }
          },
          error=>{
            this.notifier.notify( "error", "Error while changing status check DB connection");
            this.ngxLoader.stop();
          })
        }
        else if (result.dismiss === Swal.DismissReason.cancel) {
          this.ngxLoader.stop();
        }

      })

  }

  showMergeTable(firstPatientID){
    this.mergeView = true;
  }
  //////////////////////////////////////////////////////////
  ////////// MERGE PATIENT FUNCTIONS //////////////////////
  ////////////////////////////////////////////////////////


  //////////////////////////////////////////////////////////
  ////////// MERGE PATIENT FUNCTIONS END //////////////////////
  ////////////////////////////////////////////////////////

  convertAge(today){
      var monthDB ;
      var dayDB ;
      if ((today.getMonth()+1) < 10) {
        monthDB = "0"+(today.getMonth()+1);
      }
      else{
        monthDB = (today.getMonth()+1);
      }
      if (today.getDate() < 10) {
        dayDB = "0"+today.getDate();
      }
      else{
        dayDB = today.getDate();
      }
      return (today.getFullYear()  + "-" + monthDB + "-" + dayDB)
    }

    getLookups(){
      // this.ngxLoader.start();
      return new Promise((resolve, reject) => {
        var specType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=SPECIMEN_TYPE_CACHE&partnerCode=sip').then(response =>{
          return response;
        })
        var caseCat = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=CASE_CATEGORY_CACHE&partnerCode=sip').then(response =>{
          return response;
        })


        forkJoin([specType,caseCat]).subscribe(allLookups => {
          console.log("allLookups",allLookups);
          // this.allpattypes    = allLookups[0];
          this.allSpecimenTypes    = allLookups[0];
          this.allCaseCategories     = allLookups[1];

          resolve();

        })
      })

    }

    gotoEditPatient(patientId){
      var a = patientId+';6';
      console.log('patientId',patientId);


      // var ciphertext = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
      var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
      var bx = ax.replace('+','xsarm');
      var cx = bx.replace('/','ydan');
      var ciphertext = cx.replace('=','zhar');
      console.log('ciphertext',ciphertext);
      // return;
      // this.router.navigateByUrl('/edit-client/'+ciphertext)
      this.router.navigate(['/edit-patient/', ciphertext]);
    }

}
