import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appUSNumber]'
})
export class USNumberDirective {
  @Input('appUSNumber') elementValue: any;

  constructor(private el: ElementRef) { }

  @HostListener('keyup') onKeyDown() {

   this.changeFormat();
 }

 changeFormat(){
   var x = (this.el.nativeElement as HTMLInputElement).value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
   (this.el.nativeElement as HTMLInputElement).value = !x[2] ? x[1] : '(' + x[1] + ')' + x[2] + (x[3] ? '-' + x[3] : '')
 }

}
