import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavbarComponent } from './includes/navbar/navbar.component';
import { SidebarComponent } from './includes/sidebar/sidebar.component';
import { FooterComponent } from './includes/footer/footer.component';
// import { BreadcrumbComponent } from './../../../../src/app/includes/breadcrumb/breadcrumb.component';
// import { DashboardComponent } from './includes/dashboard/dashboard.component';
import { ManagePatientsComponent } from './patients/manage-patients/manage-patients.component';
import { AddPatientComponent } from './patients/add-patient/add-patient.component';

import { HomeLayoutComponent } from '../../../../src/app/includes/layouts/home-layout.component';
import { DashboardComponent } from '../../../../src/app/includes/dashboard/dashboard.component';
import { AppGuard } from "../../../../src/services/gaurd/app.guard";


const routes: Routes = [
  {
    path: '',
    component: HomeLayoutComponent,
    children: [
      {
        path      : 'all-patients',
        canActivate: [AppGuard],
        canDeactivate: [AppGuard],
        component : ManagePatientsComponent,
      },
      {
        path      : 'add-patient',
        canActivate: [AppGuard],
        canDeactivate: [AppGuard],
        component : AddPatientComponent, pathMatch: 'full'
      },
      {
        path      : 'edit-patient/:id',
        canActivate: [AppGuard],
        canDeactivate: [AppGuard],
        component : AddPatientComponent, pathMatch: 'full'
      },
    ]
  }


  // {
  //   path       : '',
  //   redirectTo : 'nglis_pntm_client/dashboard',
  //   pathMatch  : 'full'
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
