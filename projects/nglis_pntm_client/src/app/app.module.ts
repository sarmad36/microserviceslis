import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { DataTablesModule } from 'angular-datatables';
import { DashboardComponent } from './includes/dashboard/dashboard.component';
import { FooterComponent } from './includes/footer/footer.component';
import { NavbarComponent } from './includes/navbar/navbar.component';
import { SidebarComponent } from './includes/sidebar/sidebar.component';
import { BreadcrumbComponent } from './../../../../src/app/includes/breadcrumb/breadcrumb.component';
import { ManagePatientsComponent } from './patients/manage-patients/manage-patients.component';
import { AddPatientComponent } from './patients/add-patient/add-patient.component';
import { PatientApiCallsService } from './services/patient/patient-api-calls.service';
import { GlobalApiCallsService } from './services/global/global-api-call.service';
import { USNumberDirective } from './directives/usnumber.directive';
import { NgxUiLoaderModule } from  'ngx-ui-loader';
import { NotifierModule, NotifierOptions } from 'angular-notifier';
import { SafeUrlPipe } from './pipes/safe-url.pipe';
import { SortPipe } from './pipes/sort.pipe';
import { DatePipe } from '@angular/common';
// import { RbacDirective } from './../../../../src/app/directives/rbac.directive';
// import { NglisRbacDirective } from './../../../../src/app/app.module';
import { GlobalySharedModule } from './../../../../src/app/sharedmodules/globaly-shared/globaly-shared.module'


const providers = [DatePipe, PatientApiCallsService, GlobalApiCallsService, SortPipe];

/**
 * Custom angular notifier options
 */
const customNotifierOptions: NotifierOptions = {
  position: {
		horizontal: {
			position: 'left',
			distance: 110
		},
		vertical: {
			position: 'top',
			distance: 100,
			gap: 10
		}
	},
  theme: 'material',
  behaviour: {
    autoHide: 3000,
    onClick: 'hide',
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 300,
      easing: 'ease'
    },
    hide: {
      preset: 'fade',
      speed: 300,
      easing: 'ease',
      offset: 50
    },
    shift: {
      speed: 300,
      easing: 'ease'
    },
    overlap: 150
  }
};

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    BreadcrumbComponent,
    ManagePatientsComponent,
    AddPatientComponent,
    USNumberDirective,
    SafeUrlPipe,
    // RbacDirective

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule,
    DataTablesModule,
    NgxUiLoaderModule,
    NotifierModule.withConfig(customNotifierOptions),
    GlobalySharedModule
    // RbacDirective

  ],
  providers: providers,
  bootstrap: [AppComponent],
  exports: [ManagePatientsComponent, AddPatientComponent]
})
export class AppModule { }

export class NglisPntmClientSharedModule{

  static forRoot(): ModuleWithProviders<NglisPntmClientSharedModule> {
    return {
      ngModule: AppModule,
      providers: providers
    }
  }
}
