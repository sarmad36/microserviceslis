import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import { of } from 'rxjs';
import { BehaviorSubject, Observable, throwError, Subject } from 'rxjs';
import { catchError, retry, finalize, tap, map, takeUntil, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { Patient } from '../../interfaces/patient';
import { environment } from '../../../../../../src/environments/environment';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class PatientApiCallsService {

  ////////// for seacrch client
  baseUrl: string = 'https://api.cdnjs.com/libraries';
  queryUrl: string = '?search=';
  public searchClientName = {
    header:{
      uuid               :"",
      partnerCode        :"",
      userCode           :"",
      referenceNumber    :"",
      systemCode         :"",
      moduleCode         :"CLIM",
      functionalityCode  :"CLTA-VC",
      systemHostAddress  :"",
      remoteUserAddress  :"",
      dateTime           :""
    },
    data:{
    sortColumn  :"name",
    sortingOrder:"asc",
    searchString:"",
    userCode    : ""
  }
}
public logedInUserRoles :any = {};

  private subject = new Subject<string>()

  public corsHeaders: any = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
  });

  constructor(private http: HttpClient) { }

  savePatient(url, saveData){
    // return this.http.post<Patient>(url, saveData, this.corsHeaders)
    return this.http.post<any>(url, saveData)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  getAllPatient(url, saveData){
    return this.http.put<any>(url, saveData)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  getPatientbyID(url, saveData){
    return this.http.put<Patient>(url, saveData)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  updatePatient(url, saveData){
    return this.http.put<Patient>(url, saveData)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  triageRequest(url, saveData){
    return this.http.post<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  getEligiblePatient(){

  }

  getNonEligiblePatient(){

  }

  selfRegistrationSingle(){

  }

  selfRegistrationBulk(){

  }

  checkEligibility(){

  }

  checkEligibilityMultiple(){

  }

  exportPatients(url, saveData){
    return this.http.put<any>(url, saveData)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  importPatients(url, saveData){
    return this.http.put<any>(url, saveData)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  getPrimaryPatient(){

  }

  getSecondaryPatient(){

  }

  mergePatient(){

  }

  togglePatientStatus(url, saveData){
    return this.http.put<any>(url, saveData)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  searchClientByName(url, saveData){
    return this.http.post<any>(url, saveData)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  search(terms) {
    return terms.pipe(debounceTime(400))
      .pipe(distinctUntilChanged())
      .pipe(switchMap(term => this.searchEntries(term)));
  }

  searchEntries(term) {
    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
    var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
    var originalText = bts.toString(CryptoJS.enc.Utf8);
    this.logedInUserRoles = JSON.parse(originalText)
    // if (term.length >=3 || term.length == 0) {
      this.searchClientName.data.searchString = term;
      // if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-UC") != -1) {
      //   this.searchClientName['header'].functionalityCode    = "CLTA-UC";
      // }
      // else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-VC") != -1){
      //   this.searchClientName['header'].functionalityCode    = "CLTA-VC";
      // }
      // else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-CD") != -1){
      //   this.searchClientName['header'].functionalityCode    = "CLTA-CD";
      // }
      this.searchClientName['header'].userCode = this.logedInUserRoles.userCode;
      this.searchClientName['header'].partnerCode = this.logedInUserRoles.partnerCode;
      this.searchClientName['header'].functionalityCode    = "CLTA-MC";
      
      const searchURL    = environment.API_CLIENT_ENDPOINT + 'searchclientidnamebyname';
      return this.http
      // .get(this.baseUrl + this.queryUrl + term)
          .post(searchURL,this.searchClientName)
          .pipe(map(res => res)).pipe(
            catchError(this.errorHandler)
          )
    // }


  }


patientexists(url, saveData){
  return this.http.put<any>(url, saveData)
  .pipe(
    catchError(this.errorHandler)
  )
}

getClientByName(url, saveData){
  return this.http.post<any>(url, saveData)
  .pipe(
    catchError(this.errorHandler)
  )
}

getClientByIds(url, saveData){
    return this.http.post(url,saveData)
    .pipe()
    .toPromise()
    .then( resp => {
      // console.log('resp: ', resp);

      // Set loader false
      return resp;
    })
    .catch(error => {
      catchError(this.errorHandler)
    });
  // return this.http.post(url, saveData)
  // .pipe(
  //   catchError(this.errorHandler)
  // )
}
getPatientsByIds(url, saveData){
    return this.http.post(url,saveData)
    .pipe()
    .toPromise()
    .then( resp => {
      // console.log('resp: ', resp);

      // Set loader false
      return resp;
    })
    .catch(error => {
      catchError(this.errorHandler)
    });
    // return this.http
    // .put(url, saveData)
    // .pipe(
    //   catchError(this.errorHandler)
    // )
  }
  saveMinAccession(url, saveData){
    return this.http.post<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  checkCases(url, saveData){
    return this.http.post<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  caseByPatientId(url, saveData){
    return this.http.post<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  printLabel(url, saveData){
    return this.http.post<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  errorHandler(error) {
     let errorMessage = '';
     if(error.error instanceof ErrorEvent) {
       // Get client-side error
       errorMessage = error.error.message;
     } else {
       // Get server-side error
       errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
     }
     console.log("serviceError",errorMessage);
     return throwError(errorMessage);
     // return errorMessage;
  }

}
