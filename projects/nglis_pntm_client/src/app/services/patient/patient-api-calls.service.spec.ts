import { TestBed } from '@angular/core/testing';

import { PatientApiCallsService } from './patient-api-calls.service';

describe('PatientApiCallsService', () => {
  let service: PatientApiCallsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PatientApiCallsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
