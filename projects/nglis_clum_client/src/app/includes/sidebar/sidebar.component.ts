import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { AppSettings } from '../../services/_services/app.setting';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { HttpRequestsService } from '../../services/_services/http-requests/http-requests.service';
import { DataTableDirective } from 'angular-datatables';
import { NgxUiLoaderService } from 'ngx-ui-loader';
declare var $ :any;
import Swal from 'sweetalert2';
import { NotifierService } from 'angular-notifier';
import { PartnerApiEndpointsService } from '../../services/partner/partner-api-endpoints.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  public getAgreement   =
  {
    header              : {
      uuid              : "",
      partnerCode       : "",
      userCode          : "",
      referenceNumber   : "",
      systemCode        : "",
      moduleCode        : "",
      functionalityCode : "",
      systemHostAddress : "",
      remoteUserAddress : "",
      dateTime          : ""
    },
    data                :{
      partnerCode         : 1
    }
  }
  agreementText         ;
  termsConditions       = false;
  swalStyle              : any;
  constructor(
    private formBuilder  : FormBuilder,
    private http         : HttpClient,
    private router       : Router ,
    private httpRequest  : HttpRequestsService,
    private ngxLoader    : NgxUiLoaderService,
    private partnerService  : PartnerApiEndpointsService,
    private  notifier    : NotifierService
  ) {
    this.swalStyle       =  Swal.mixin({
      customClass : {
        confirmButton : 'btn btn-success my-swal-success',
        cancelButton  : 'btn btn-danger'
      },
      buttonsStyling  : true
    })
    this.notifier        = notifier;
   }

  ngOnInit(): void {
    this.bindJQueryFuncs();
  }

  showAgreement(){
    // let url  = environment.API_PARTNER_ENDPOINT + 'agreement?partnerCode=1';
    this.ngxLoader.start();
    let url  = environment.API_PARTNER_ENDPOINT + 'agreement';
    this.partnerService.getAgreement(url,this.getAgreement).subscribe(resp => {
      console.log("resp",resp);
      this.ngxLoader.stop();
      if (resp['result'].codeType =="E") {
        // alert("Agreement already accepted");
        this.router.navigate(['dashboard']);
        this.notifier.notify( "info", "Agreement Already Accepted");
      }
      else{
        this.agreementText = resp['data'].text;
        this.ngxLoader.stop();
        $('#wizardsModal').modal('show');
      }

      // this.router.navigate(['/editlisting', JSON.stringify(resp)]);
    })
  }
  acceptAgreement(){
    this.ngxLoader.start();
    if (this.termsConditions) {

      // let url  = environment.API_PARTNER_ENDPOINT + 'acceptagreement?partnerCode=1';
      let url  = environment.API_PARTNER_ENDPOINT + 'acceptagreement';
      this.partnerService.acceptAgreement(url, this.getAgreement).subscribe(resp => {
        console.log("resp",resp);
        this.ngxLoader.stop();
        if (resp['result'].codeType == 'S') {
          this.swalStyle.fire({
            icon: 'success',
            type: 'sucess',
            title: 'Thankyou for accepting terms',
            showConfirmButton: true,
            timer: 3000
          })
        }
        else{
          this.ngxLoader.stop();
          this.swalStyle.fire({
            icon: 'error',
            type: 'sucess',
            title: 'There was an error while accepting terms',
            showConfirmButton: true,
            timer: 3000
          })

        }
        // this.router.navigate(['/editlisting', JSON.stringify(resp)]);
      })
      this.ngxLoader.stop();
      $('#wizardsModal').modal('hide');
    }
    else{
      alert("Please Accept The Terms and COnditions");
    }

  }

  bindJQueryFuncs() {
    $("#sidebarToggle, #sidebarToggleTop").on('click', function(e) {
      // console.log("here");
      $("body").toggleClass("sidebar-toggled");
      $(".sidebar").toggleClass("toggled");
      if ($(".sidebar").hasClass("toggled")) {
        $('.sidebar .collapse').collapse('hide');
      };
    });

    // Close any open menu accordions when window is resized below 768px
    $(window).resize(function() {
      if ($(window).width() < 768) {
        $('.sidebar .collapse').collapse('hide');
      };
    });

    // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
    $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function(e) {
      if ($(window).width() > 768) {
        var e0           = e.originalEvent,
        delta            = e0.wheelDelta || -e0.detail;
        this.scrollTop   += (delta < 0 ? 1 : -1) * 30;
        e.preventDefault();
      }
    });
  }

}
