import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { AppSettings } from '../../services/_services/app.setting';
import { HttpRequestsService } from '../../services/_services/http-requests/http-requests.service';
import { UserApiEndpointsService } from '../../services/users/user-api-endpoints.service';
declare var $ :any;
import * as CryptoJS from 'crypto-js';

@Component({
  selector   : 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls  : ['./dashboard.component.css'],
  host       : {
    class    :'mainComponentStyle'
}
})
export class DashboardComponent implements OnInit {

  totalUsers            ;
  totalClients          ;
  public requestData    =
  {
    header              : {
      uuid              : "",
      partnerCode       : "",
      userCode          : "",
      referenceNumber   : "",
      systemCode        : "",
      moduleCode        : "",
      functionalityCode : "",
      systemHostAddress : "",
      remoteUserAddress : "",
      dateTime          : ""
    },
    data                : {
      // partnerCode         : 1,
      userType          : 1
    }
  };

  public clientRequestData    =
  {
    header              : {
      uuid              : "",
      partnerCode       : "",
      userCode          : "",
      referenceNumber   : "",
      systemCode        : "",
      moduleCode        : "CLIM",
      functionalityCode : "",
      systemHostAddress : "",
      remoteUserAddress : "",
      dateTime          : ""
    },
    data                : {}
  };
  public logedInUserRoles :any = {};

  constructor(
    private formBuilder  : FormBuilder,
    private http         : HttpClient,
    private router       : Router ,
    private httpRequest  : HttpRequestsService,
    private userService  : UserApiEndpointsService,
  ) { }

  ngOnInit(): void {
    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
    var originalText = bts.toString(CryptoJS.enc.Utf8);
    // console.log("originalText",originalText);
    this.logedInUserRoles = JSON.parse(originalText)
    // console.log('this.logedInUserRoles',this.logedInUserRoles);

    // this.logedInUserRoles.userName    = "snazirkhanali";
    // this.logedInUserRoles.partnerCode = 1;
    this.requestData.header.userCode    = this.logedInUserRoles.userCode
    this.requestData.header.partnerCode = this.logedInUserRoles.partnerCode
    // var url                = environment.API_USER_ENDPOINT + 'totalusers?partnerCode=1';
    var url                = environment.API_USER_ENDPOINT + 'totalusers';
    this.userService.totalUsers(url, this.requestData).subscribe(resp => {
      console.log('total Users',resp)
      this.totalUsers = resp['data'];
    })

    // var url                = environment.API_USER_ENDPOINT + 'totalusers?partnerCode=1';
    var url                = environment.API_CLIENT_ENDPOINT + 'totalclients';
    this.httpRequest.httpPostRequests(url,this.clientRequestData).then(resp => {
      console.log('total Clients',resp)
      this.totalClients = resp.data;
    })
  }

}
