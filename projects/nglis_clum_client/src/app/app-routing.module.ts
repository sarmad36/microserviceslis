import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavbarComponent } from './includes/navbar/navbar.component';
import { SidebarComponent } from './includes/sidebar/sidebar.component';
import { FooterComponent } from './includes/footer/footer.component';
// import { DashboardComponent } from './includes/dashboard/dashboard.component';
import { AddUserComponent } from './partner-admin/user/add-user/add-user.component';
import { ManageUsersComponent } from './partner-admin/user/manage-users/manage-users.component';
import { AddClientComponent } from './partner-admin/client/add-client/add-client.component';
import { EditUserComponent } from './partner-admin/user/edit-user/edit-user.component';
import { ManageClientComponent } from './partner-admin/client/manage-client/manage-client.component';
import { EditClientComponent } from './partner-admin/client/edit-client/edit-client.component';
// import { ChangePasswordComponent } from './partner-admin/user/change-password/change-password.component';
// import { ManagePatientComponent } from './patient/manage-patient/manage-patient.component';
import { HomeLayoutComponent } from '../../../../src/app/includes/layouts/home-layout.component';
import { DashboardComponent } from '../../../../src/app/includes/dashboard/dashboard.component';
import { ChangePasswordComponent } from '../../../../src/app/includes/change-password/change-password.component';
import { AppGuard } from "../../../../src/services/gaurd/app.guard";




const routes: Routes = [
  {
    path: '',
    component: HomeLayoutComponent,
    children: [
      {
        path: '',
        canActivate: [AppGuard],
        canDeactivate: [AppGuard],
        component: DashboardComponent,
      },
      {
        path      : 'add-user',
        canActivate: [AppGuard],
        canDeactivate: [AppGuard],
        component      : AddUserComponent

      },
      {
        path      : 'all-users',
        canActivate: [AppGuard],
        canDeactivate: [AppGuard],
        component : ManageUsersComponent
      },
      {
        path      : 'edit-user/:id',
        canActivate: [AppGuard],
        canDeactivate: [AppGuard],
        component : EditUserComponent, pathMatch: 'full'
      },
      {
        path      : 'change-password',
        canActivate: [AppGuard],
        canDeactivate: [AppGuard],
        component : ChangePasswordComponent,
      },
      {
        path      : 'add-client',
        canActivate: [AppGuard],
        canDeactivate: [AppGuard],
        component : AddClientComponent
      },
      {
        path      : 'all-clients',
        canActivate: [AppGuard],
        canDeactivate: [AppGuard],
        component : ManageClientComponent
      },
      {
        // path      : 'edit-client',
        path      : 'edit-client/:id',
        canActivate: [AppGuard],
        canDeactivate: [AppGuard],
        component : EditClientComponent, pathMatch: 'full'
      },
      // {
      //   path       : '',
      //   redirectTo : 'add-user',
      //   pathMatch  : 'full'
      // }

    ]
  },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
