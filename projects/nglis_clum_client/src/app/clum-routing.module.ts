import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavbarComponent } from './includes/navbar/navbar.component';
import { SidebarComponent } from './includes/sidebar/sidebar.component';
import { FooterComponent } from './includes/footer/footer.component';
import { DashboardComponent } from './includes/dashboard/dashboard.component';
import { AddUserComponent } from './partner-admin/user/add-user/add-user.component';
import { ManageUsersComponent } from './partner-admin/user/manage-users/manage-users.component';
import { AddClientComponent } from './partner-admin/client/add-client/add-client.component';
import { EditUserComponent } from './partner-admin/user/edit-user/edit-user.component';
import { ManageClientComponent } from './partner-admin/client/manage-client/manage-client.component';
import { EditClientComponent } from './partner-admin/client/edit-client/edit-client.component';
import { ChangePasswordComponent } from './partner-admin/user/change-password/change-password.component';
// import { ManagePatientComponent } from './patient/manage-patient/manage-patient.component';

export const CLUMroutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent,
      },
      {
        path      : 'add-user',
        component      : AddUserComponent

      },
      {
        path      : 'all-users',
        component : ManageUsersComponent
      },
      {
        path      : 'edit-user/:id',
        component : EditUserComponent, pathMatch: 'full'
      },
      // {
      //   path      : 'change-password',
      //   component : ChangePasswordComponent,
      // },
      {
        path      : 'add-client',
        component : AddClientComponent
      },
      {
        path      : 'all-clients',
        component : ManageClientComponent
      },
      {
        // path      : 'edit-client',
        path      : 'edit-client/:id',
        component : EditClientComponent, pathMatch: 'full'
      },
      // {
      //   path       : '',
      //   redirectTo : 'add-user',
      //   pathMatch  : 'full'
      // }
    ]
  },

  // {
  //   path      : 'add-user',
  //   component      : AddUserComponent
  //
  // },
  // {
  //   path      : 'all-users',
  //   component : ManageUsersComponent
  // },
  // {
  //   path      : 'edit-user/:id',
  //   component : EditUserComponent, pathMatch: 'full'
  // },
  // // {
  // //   path      : 'change-password',
  // //   component : ChangePasswordComponent,
  // // },
  // {
  //   path      : 'add-client',
  //   component : AddClientComponent
  // },
  // {
  //   path      : 'all-clients',
  //   component : ManageClientComponent
  // },
  // {
  //   // path      : 'edit-client',
  //   path      : 'edit-client/:id',
  //   component : EditClientComponent, pathMatch: 'full'
  // },
  {
    path       : '',
    redirectTo : 'add-user',
    pathMatch  : 'full'
  }
];

// @NgModule({
//   // imports: [RouterModule.forRoot(routes)],
//   // exports: [RouterModule]
// })
// // export class ClumRoutingModule { }
