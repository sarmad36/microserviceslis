import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import { AppSettings } from '../_services/app.setting';
import { of } from 'rxjs';
import { BehaviorSubject, Observable, throwError, Subject } from 'rxjs';
import { catchError, retry, finalize, tap, map, takeUntil } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class UserApiEndpointsService {
  public corsHeaders: any = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
  });
  protected ngUnsubscribe  : Subject<void> = new Subject<void>();

  constructor(private http: HttpClient) { }

  saveUser(url: any,saveData: any) : Promise<any>{
    return this.http.post(url,saveData)
    .pipe( takeUntil(this.ngUnsubscribe) )
    .toPromise()
    .then( resp => {
      // Set loader false
      return resp;
    })
    .catch(error => {
      // Set loader false

      if(typeof error.error != "undefined") {
        console.log("error message", error.error.message);
      } else {
        console.log("Failure Something Went wrong");
      }
      // this.showFailure    = true;
    });
  }

  updateUser(url: any,saveData: any){
    return this.http.put(url,saveData)
  }

  getPartnerUsers(url: any,saveData: any){
    return this.http.put(url,saveData)
  }

  getUsers(url: any,saveData: any){
    return this.http.put(url,saveData)
  }

  totalUsers(url: any,saveData: any){
    return this.http.put(url,saveData)
  }

  updatePassword(url: any,saveData: any){
    return this.http.put(url,saveData)
  }

  updateStatus(url: any,saveData: any){
    return this.http.put(url,saveData)
  }

  getReportingTo(url: any,saveData: any){
    return this.http.put(url,saveData)
  }

  userNameAvailability(url: any,saveData: any){
    return this.http.put(url,saveData)
  }

  getClientUsers(url: any,saveData: any){
    return this.http.put(url,saveData)
  }

  getUserByNPIEmail(url: any,saveData: any){
    return this.http.put(url,saveData)
  }
  checkCaseforUSer(url: any,saveData: any){
    return this.http.put(url,saveData);
  }

  getSalesRepresentatives(url: any,saveData: any) : Promise<any>{
    return this.http.put(url,saveData)
    .pipe( takeUntil(this.ngUnsubscribe) )
    .toPromise()
    .then( resp => {
      // console.log('resp: ', resp);

      // Set loader false
      return resp;
    })
    .catch(error => {
      // Set loader false
      console.log("error message", error);
      if (error.error.text == 'User Password updated successfully') {
          return true;
      }
      else{
        return false;
      }
      if(typeof error.error != "undefined") {
        console.log("error message", error.error.message);
      } else {
        console.log("Failure Something Went wrong");
      }
      // this.showFailure    = true;
    });
  }



}
