import { TestBed } from '@angular/core/testing';

import { UserApiEndpointsService } from './user-api-endpoints.service';

describe('UserApiEndpointsService', () => {
  let service: UserApiEndpointsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserApiEndpointsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
