import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import { AppSettings } from '../_services/app.setting';
import { of } from 'rxjs';
import { BehaviorSubject, Observable, throwError, Subject } from 'rxjs';
import { catchError, retry, finalize, tap, map, takeUntil } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LookupsApiEndpointsService {
  public corsHeaders: any = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
  });
  protected ngUnsubscribe  : Subject<void> = new Subject<void>();

  constructor(private http: HttpClient) { }

  attachmentTypes(url: any){
    return this.http.get(url);
  }

  allowedFiletypes(url: any){
    return this.http.get(url);
  }

  resquisitiontypes(url: any){
    return this.http.get(url);
  }

  priority(url: any){
    return this.http.get(url);
  }

  countries(url: any){
    return this.http.get(url);
  }

  provinces(url: any){
    return this.http.get(url);
  }

  cities(url: any){
    return this.http.get(url);
  }

  deliveryMethods(url: any){
    return this.http.get(url);
  }

  reportFormats(url: any){
    return this.http.get(url);
  }

}
