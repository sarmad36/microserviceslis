import { TestBed } from '@angular/core/testing';

import { LookupsApiEndpointsService } from './lookups-api-endpoints.service';

describe('LookupsApiEndpointsService', () => {
  let service: LookupsApiEndpointsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LookupsApiEndpointsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
