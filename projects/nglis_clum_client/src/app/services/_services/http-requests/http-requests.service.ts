import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import { AppSettings } from '../app.setting';
import { of } from 'rxjs';
import { BehaviorSubject, Observable, throwError, Subject } from 'rxjs';
import { catchError, retry, finalize, tap, map, takeUntil } from 'rxjs/operators';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class HttpRequestsService {
  public logedInUserRoles :any = {};
  public corsHeaders: any = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
  });
  protected ngUnsubscribe  : Subject<void> = new Subject<void>();

  constructor(private http: HttpClient) {
    // let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
// var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
    // var originalText = bts.toString(CryptoJS.enc.Utf8);
    // this.logedInUserRoles = JSON.parse(originalText)
   }

  httpPostRequests(url, saveData): Promise<any> {
    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
    var originalText = bts.toString(CryptoJS.enc.Utf8);
    this.logedInUserRoles = JSON.parse(originalText)
    const headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization':'Bearer '+this.logedInUserRoles.token,
        'Accept'       : "*/*"
    });
    // const myObjStr = JSON.stringify(saveData);
    // return this.http.post(url,{
    //   params: {
    //     saveData  : saveData
    //   }
    // }).toPromise()
    // this.corsHeaders = {
    //   headers: new HttpHeaders()
    //   .set('Content-Type', 'multipart/form-data')
    //   .set('Accept',  'application/json')
    // }
    return this.http.post(url,saveData)
    .pipe( takeUntil(this.ngUnsubscribe) )
    .toPromise()
    .then( resp => {
      // Set loader false
      return resp;
    })
    .catch(error => {
      // Set loader false

      if(typeof error.error != "undefined") {
        console.log("error message", error.error.message);
      } else {
        console.log("Failure Something Went wrong");
      }
      // this.showFailure    = true;
    });
  }

  httpGetRequests(url): Promise<any> {
    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
    var originalText = bts.toString(CryptoJS.enc.Utf8);
    this.logedInUserRoles = JSON.parse(originalText)
    const headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization':'Bearer '+this.logedInUserRoles.token,
        'Accept'       : "*/*"
    });

     return this.http.get(url,{headers})
     .pipe( takeUntil(this.ngUnsubscribe) )
     .toPromise()
     .then( resp => {
        // Set loader false
        return resp;
     })
     .catch(error => {
        // Set loader false
        console.log("error message", error);

        if(typeof error.error != "undefined") {
           console.log("error message", error.error.message);
        } else {
           console.log("Failure Something Went wrong");
        }
        // this.showFailure    = true;
     });
  }

  httpPutRequests(url, saveData): Promise<any> {
    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
    var originalText = bts.toString(CryptoJS.enc.Utf8);
    this.logedInUserRoles = JSON.parse(originalText)
    const headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization':'Bearer '+this.logedInUserRoles.token,
        'Accept'       : "*/*"
    });
    console.log('headers',headers);


    return this.http.put(url,saveData,{headers})
    .pipe( takeUntil(this.ngUnsubscribe) )
    .toPromise()
    .then( resp => {
      // console.log('resp: ', resp);

      // Set loader false
      return resp;
    })
    .catch(error => {
      // Set loader false
      console.log("error message", error);
      if (error.error.text == 'User Password updated successfully') {
          return true;
      }
      else{
        return false;
      }
      if(typeof error.error != "undefined") {
        console.log("error message", error.error.message);
      } else {
        console.log("Failure Something Went wrong");
      }
      // this.showFailure    = true;
    });
  }


}
