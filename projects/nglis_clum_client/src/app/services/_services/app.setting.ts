import Swal from 'sweetalert2'
import { isDevMode } from '@angular/core'
export class AppSettings {
  constructor() { }
  // if (isDevMode()) {
  //   console.log('👋 Development!');
  //   this.API_USER_ENDPOINT      = 'http://localhost:8044/user/';
  //   this.API_PARTNER_ENDPOINT   = 'http://localhost:8044/partner/';
  //   this.API_ROLE_ENDPOINT      = 'http://localhost:8044/role/';
  //   this.API_CLIENT_ENDPOINT    = 'http://localhost:9002/clientapi/';
  // } else {
  //   console.log('💪 Production!');
  //   this.API_USER_ENDPOINT      = 'http://192.168.2.8:8044/user/';
  //   this.API_PARTNER_ENDPOINT   = 'http://192.168.2.8:8044/partner/';
  //   this.API_ROLE_ENDPOINT      = 'http://192.168.2.8:8044/role/';
  //   this.API_CLIENT_ENDPOINT    = 'http://192.168.2.8:9002/clientapi/';
  // }
  // public static API_USER_ENDPOINT      = 'http://192.168.2.8:8044/user/';
  public static API_USER_ENDPOINT      = 'http://localhost:8044/user/';
  // public static API_PARTNER_ENDPOINT   = 'http://192.168.2.8:8044/partner/';
  public static API_PARTNER_ENDPOINT   = 'http://localhost:8044/partner/';
  // public static API_ROLE_ENDPOINT      = 'http://192.168.2.8:8044/role/';
  public static API_ROLE_ENDPOINT      = 'http://localhost:8044/role/';
  // public static API_USERDASH_ENDPOINT  = 'http://localhost:8044/client/';
  // public static API_CLIENT_ENDPOINT    = 'http://192.168.2.8:9002/clientapi/';
  public static API_CLIENT_ENDPOINT    = 'http://localhost:9002/clientapi/';
   // public static IMG_ENDPOINT    = 'http://localhost:3000/';

  // public static URLS = getURLS();
  // static sum(a: number, b: number): number {
  //       return a + b;
  //   }

  static getURLS(){
     var urls = {};
     if (isDevMode()) {
       console.log('👋 Development!');
       urls = {
         API_USER_ENDPOINT      : 'http://localhost:8044/user/',
         API_PARTNER_ENDPOINT   : 'http://localhost:8044/partner/',
         API_ROLE_ENDPOINT      : 'http://localhost:8044/role/',
         API_CLIENT_ENDPOINT    : 'http://localhost:9002/clientapi/'
       }

     } else {
       console.log('💪 Production!');
       urls = {
         API_USER_ENDPOINT      : 'http://192.168.2.8:8044/user/',
         API_PARTNER_ENDPOINT   : 'http://192.168.2.8:8044/partner/',
         API_ROLE_ENDPOINT      : 'http://192.168.2.8:8044/role/',
         API_CLIENT_ENDPOINT    : 'http://192.168.2.8:9002/clientapi/'
       }

     }
     return urls;
   }

   public static defaultAccountTypes    = [
     {id: 1,  name: 'Hematology'},
     {id: 2,  name: 'Oncology'},
     {id: 3,  name: 'Obstetrics & Gynecology'},
     {id: 4,  name: 'Urology'},
     {id: 5,  name: 'Dermatopathology'},
     {id: 6,  name: 'Gastroenterology'},
     {id: 7,  name: 'Clinical Laboratory'},
     {id: 8,  name: 'General Care (Practice)'},
     {id: 9,  name: 'Nursing Homes'},
     {id: 10, name: 'Surgical Centers'}
   ];
   public static defaultAccountPriority = [
     {id: 1,  name: 'Medical State [Highest]'},
     {id: 2,  name: 'STAT (RUSH)'},
     {id: 3,  name: 'Before 5 PM'},
     {id: 4,  name: 'Before Noon'}
   ];
   public static defaultPCOrganization  = [
     {id: 1,  name: 'SiParadigm'},
     {id: 1,  name: 'Cairo'}
   ];
   public static defaultReqType         = [
     {id: 1,  name: 'Hematology'},
     {id: 2,  name: 'Rumor'}
   ];
   public static defaultAttType         = [
     {id: 1,  name: 'Insurance Card'},
     {id: 2,  name: 'CBC'},
     {id: 3,  name: 'Clinical notes'}
   ];
   public static defaultFileType       = [
     {id: 1,  name: 'pdf'},
     {id: 2,  name: 'jpg'},
     {id: 3,  name: 'jpeg'},
     {id: 3,  name: 'png'},
     {id: 3,  name: 'docx'}
   ];
   public static deliverMethod         = [
     { id: 1,             name: 'Fax',            checked: false},
     { id: 2,             name: 'EMR',            checked: false},
     { id: 3,             name: 'email',          checked: false},
     { id: 4,             name: 'Courier',        checked: false},
     { id: 5,             name: 'Remote Printing',checked: false}
   ];
   // public static deliverMethod         = [
   //   { id: 'fax',             name: 'Fax',            checked: false},
   //   { id: 'emr',             name: 'EMR',            checked: false},
   //   { id: 'email',           name: 'email',          checked: false},
   //   { id: 'courier',         name: 'Courier',        checked: false},
   //   { id: 'remote printing', name: 'Remote Printing',checked: false}
   // ];
   public static defaultClientUserRole = [
     {id: 1,  name: 'Attending Physician'},
     {id: 2,  name: 'Referring Physician'},
     {id: 3,  name: 'Non-Physician'}
   ];
   public static additionalHeaders    = [
     {uniqueID          : null},
     {partnerCode       : null},
     {userCode          : null},
     {refNumber         : null},
     {clientCode        : null},
     {systemCode        : null},
     {moduleCode        : null},
     {functionalityCode : null},
     {systemHostAdd     : null},
     {remoteUserAdd     : null},
     {dateTime          : null},
   ];
}
