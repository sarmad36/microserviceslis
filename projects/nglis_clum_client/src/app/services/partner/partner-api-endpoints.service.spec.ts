import { TestBed } from '@angular/core/testing';

import { PartnerApiEndpointsService } from './partner-api-endpoints.service';

describe('PartnerApiEndpointsService', () => {
  let service: PartnerApiEndpointsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartnerApiEndpointsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
