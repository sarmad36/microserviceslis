import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import { AppSettings } from '../_services/app.setting';
import { of } from 'rxjs';
import { BehaviorSubject, Observable, throwError, Subject } from 'rxjs';
import { catchError, retry, finalize, tap, map, takeUntil } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PartnerApiEndpointsService {
  public corsHeaders: any = new HttpHeaders({
    'Content-Type': 'application/json',
    'Accept': 'application/json',
  });
  protected ngUnsubscribe  : Subject<void> = new Subject<void>();

  constructor(private http: HttpClient) { }

  getAgreement(url: any,saveData: any){
    return this.http.put(url,saveData)
  }

  acceptAgreement(url: any,saveData: any){
    return this.http.put(url,saveData)
  }
}
