import { TestBed } from '@angular/core/testing';

import { RolesApiEndpointsService } from './roles-api-endpoints.service';

describe('RolesApiEndpointsService', () => {
  let service: RolesApiEndpointsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RolesApiEndpointsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
