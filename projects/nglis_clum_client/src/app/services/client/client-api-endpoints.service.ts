import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import { AppSettings } from '../_services/app.setting';
import { of } from 'rxjs';
import { BehaviorSubject, Observable, throwError, Subject } from 'rxjs';
import { catchError, retry, finalize, tap, map, takeUntil, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { first } from 'rxjs/operators';
import { environment } from '../../../../../../src/environments/environment';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class ClientApiEndpointsService {
  public logedInUserRoles :any = {};

  public searchClientName = {
    header:{
      uuid               :"",
      partnerCode        :"",
      userCode           :"",
      referenceNumber    :"",
      systemCode         :"",
      moduleCode         :"CLIM",
      functionalityCode  :"",
      systemHostAddress  :"",
      remoteUserAddress  :"",
      dateTime           :""
    },
    data:{
    sortColumn  :"name",
    sortingOrder:"asc",
    searchString:"",
    userCode    : ""
  }
}
  public corsHeaders: any = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
  });
  protected ngUnsubscribe  : Subject<void> = new Subject<void>();

  constructor(private http: HttpClient) { }
  search(terms) {
    return terms.pipe(debounceTime(400))
      .pipe(distinctUntilChanged())
      .pipe(switchMap(term => this.searchEntries(term)));
  }

  searchEntries(term) {
    // if (term.length >=3 || term.length == 0) {
    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
    var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
    var originalText = bts.toString(CryptoJS.enc.Utf8);
    this.logedInUserRoles = JSON.parse(originalText)

    // if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-UC") != -1) {
    //   this.searchClientName.header.functionalityCode    = "CLTA-UC";
    // }
    // else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-VC") != -1){
    //   this.searchClientName.header.functionalityCode    = "CLTA-VC";
    // }
    // else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-AC") != -1){
    //   this.searchClientName.header.functionalityCode    = "CLTA-AC";
    // }
      this.searchClientName.data.searchString = term;
      this.searchClientName.header.functionalityCode    = "CLTA-MC";
      const searchURL    = environment.API_CLIENT_ENDPOINT + 'searchclientidnamebyname';
      return this.http
      // .get(this.baseUrl + this.queryUrl + term)
          .post(searchURL,this.searchClientName)
          .pipe(map(res => res)).pipe(
            catchError(this.errorHandler)
          )
    // }


  }
  getClientByName(url, saveData){
    return this.http.post<any>(url, saveData)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  saveClient(url: any,saveData: any){
    return this.http.post(url,saveData)
  }

  updateClient(url: any,saveData: any){
    return this.http.post(url,saveData);
  }

  getClient(url: any){
    return this.http.get(url)
    .pipe(map(client => {
              return client;
          }));
  }

  getAllClients(url: any,saveData: any){
    return this.http.post(url,saveData);
  }
  searchCLientByUsers(url: any,saveData: any){
    return this.http.post(url,saveData);
  }

  deleteClientUser(url: any,saveData: any){
    return this.http.post(url,saveData);
  }

  updateClientUser(url: any,saveData: any){
    return this.http.post(url,saveData);
  }

  changeClientStatus(url: any,saveData: any){
    return this.http.post(url,saveData);
  }

  clientTypes(url: any){
    return this.http.get(url);
  }

  updateClientUserContact(url: any,saveData: any){
    return this.http.post(url,saveData);
  }

  updateClientContact(url: any,saveData: any){
    return this.http.post(url,saveData);
  }

  addClientUser(url: any,saveData: any){
    return this.http.post(url,saveData);
  }

  getClientUser(url: any,saveData: any){
    return this.http.post(url,saveData);
  }

  getClientUserContact(url: any,saveData: any){
    return this.http.post(url,saveData);
  }

  checkAccountNumber(url: any){
    return this.http.get(url);
  }

  getTotalClientsCount(url: any){
    return this.http.get(url);
  }

  changeClientUserStatus(url: any,saveData: any){
    return this.http.post(url,saveData);
  }
  getParentClient(url: any,saveData: any){
    return this.http.post(url,saveData)
    .pipe( takeUntil(this.ngUnsubscribe) )
    .toPromise()
    .then( resp => {
      // Set loader false
      return resp;
    })
    .catch(error => {
      // Set loader false

      if(typeof error.error != "undefined") {
        console.log("error message", error.error.message);
      } else {
        console.log("Failure Something Went wrong");
      }
      // this.showFailure    = true;
    });
  }

  errorHandler(error) {
     let errorMessage = '';
     if(error.error instanceof ErrorEvent) {
       // Get client-side error
       errorMessage = error.error.message;
     } else {
       // Get server-side error
       errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
     }
     console.log("serviceError",errorMessage);
     return throwError(errorMessage);
     // return errorMessage;
  }

}
