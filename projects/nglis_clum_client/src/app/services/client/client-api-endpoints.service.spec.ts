import { TestBed } from '@angular/core/testing';

import { ClientApiEndpointsService } from './client-api-endpoints.service';

describe('ClientApiEndpointsService', () => {
  let service: ClientApiEndpointsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ClientApiEndpointsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
