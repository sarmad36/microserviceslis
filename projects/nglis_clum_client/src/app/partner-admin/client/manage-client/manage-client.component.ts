import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { environment } from '../../../../../../../src/environments/environment';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { AppSettings } from '../../../services/_services/app.setting';
import { HttpRequestsService } from '../../../services/_services/http-requests/http-requests.service';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { DataTableDirective } from 'angular-datatables';
import { Subject, forkJoin } from 'rxjs';
import { NgxUiLoaderService } from 'ngx-ui-loader';
declare var $ :any;
import Swal from 'sweetalert2';
import { NotifierService } from 'angular-notifier';
import { UserApiEndpointsService } from '../../../services/users/user-api-endpoints.service';
import { ClientApiEndpointsService } from '../../../services/client/client-api-endpoints.service';
// import { DataTableDirective } from 'angular-datatables';
import { GlobalySharedModule } from './../../../../../../../src/app/sharedmodules/globaly-shared/globaly-shared.module';
import { UrlGuard } from './../../../../../../../src/services/gaurd/url.guard'
import * as CryptoJS from 'crypto-js';

@Component({
  selector   : 'app-manage-client',
  templateUrl: './manage-client.component.html',
  styleUrls  : ['./manage-client.component.css'],
  host       : {
    class    :'mainComponentStyle'
  }
})
export class ManageClientComponent implements OnInit {
  @ViewChild(DataTableDirective, {static: false})

  dtElement              : DataTableDirective;
  dtTrigger              : Subject<ManageClientComponent> = new Subject();
  public dtOptions       : DataTables.Settings = {};
  allClients             : any;
  public paginatePage    : any = 0;
  public paginateLength  = 20;
  filePayLoad            ;
  fileBase64             = '';
  fileSendClientID       = '';
  fileSendClientEmail    = '';
  totalClientsCount        ;
  oldCharacterCount      = 0;
  // totalClientsCount        = 500;
  defaultAccountTypes    ;

  public getAllClients   : any ={
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"CLIM",
      functionalityCode:"",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data: {
      page : 0,
      size : 20,
      sortColumn: "name",
      sortingOrder: "desc",
      userCode    : ''
    }

  }

  public getAllClientsForPortal   : any ={
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"CLIM",
      functionalityCode:"",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data: {
      page : 0,
      size : 20,
      sortColumn: "name",
      sortingOrder: "desc",
    }

  }

  public searchAllClients : any ={
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"CLIM",
      functionalityCode:"",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data: {
      page : 0,
      size : 20,
      sortColumn: "name",
      sortingOrder: "desc",
      searchString: "",
      userCode:''
    }
  }

  public clientStatus   : any ={
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"CLIM",
      functionalityCode:"",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data: {
      clientStatus : 0,
      clientId     : 1
    }
  }
  swalStyle              : any;
  totalClients           ;

  public PUsersRequest: any = {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode  : "",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 : {
      userCodes          :[]
    }
  }

  public salesRepReq     : any = {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode  : "",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 : ""
  }
  allSalesRep            ;
  public searchSalesRep  : any = {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode  : "",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                  :{
      searchQuery          : ""
    }

  }
  allClientsforSearch     :any;
  public logedInUserRoles :any = {};

  public clientByUserSwearchRep  : any = {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "CLTM",
      functionalityCode  : "",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                  :{
      page : 0,
      size : 20,
      sortColumn: "name",
      sortingOrder: "desc",
      userCodes: []
    }

  }
  clienPortal =false;
  showUpdateAction = false;

  public newAllClientReq = {
    data:
    {
      pageNumber: 0,
      pageSize: 20,
      sortColumn: "clientCode",
      sortType: "desc",
      mainSearch:""
    },
    header:
    {
      uuid: "",
      partnerCode: "sip",
      userCode: "",
      referenceNumber: "",
      systemCode: "",
      moduleCode: "ACDM",
      functionalityCode: "CLTA-MC",
      systemHostAddress: "",
      remoteUserAddress: "",
      dateTime: ""
    }
  }

  constructor(
    private formBuilder  : FormBuilder,
    private http         : HttpClient,
    private router       : Router ,
    private sanitizer    : DomSanitizer,
    private httpRequest  : HttpRequestsService,
    private ngxLoader    : NgxUiLoaderService,
    private  notifier    : NotifierService,
    private userService  : UserApiEndpointsService,
    private clientService  : ClientApiEndpointsService,
    private rbac         : UrlGuard,
    private encryptDecrypt : GlobalySharedModule
  ) {

    this.rbac.checkROle('CLTA-MC');
    this.notifier        = notifier;
    this.swalStyle       =  Swal.mixin({
      customClass : {
        confirmButton : 'btn btn-success my-swal-success',
        cancelButton  : 'btn btn-danger'
      },
      buttonsStyling  : true
    })
   }

  ngOnInit(): void {
    this.ngxLoader.stop();
    this.notifier.hideAll();
    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
var originalText = bts.toString(CryptoJS.enc.Utf8);
this.logedInUserRoles = JSON.parse(originalText)
    if(this.logedInUserRoles['allowedRoles'].indexOf("USRA-VUE") != -1 && this.logedInUserRoles['userType'] == 2){
      this.clienPortal = true;

    }

    /////////////// defaultAccountTypes
    var url                = environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=CLIENT_TYPE_CACHE&partnerCode=sip';
    this.httpRequest.httpGetRequests(url).then(resp => {
      console.log('defaultAccountTypes',resp)
      this.defaultAccountTypes = resp;
    })

    if(this.logedInUserRoles['allowedRoles'].indexOf("USRA-VUE") != -1 && this.logedInUserRoles['userType'] == 2){
      this.clienPortal = true;

    }


    /////////////// SalesReps


    this.dtOptions = {
      pagingType   : 'full_numbers',
      pageLength   : this.paginateLength,
      serverSide   : true,
      processing   : true,
      // ordering     : false,
      "order": [[ 0, "desc" ]],
      columnDefs: [
            { orderable: true, className: 'reorder', targets: [0,1,5] },
            { orderable: false, targets: '_all' }
        ],
      "lengthMenu": [20, 50, 75, 100 ],
      // "language"   : { processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '},
      ajax: (dataTablesParameters: any, callback) => {
        if(typeof this.dtElement['dt'] != 'undefined'){
            this.paginatePage           = this.dtElement['dt'].page.info().page;
            this.paginateLength         = this.dtElement['dt'].page.len();
            // console.log("this.dtElement['dt']: ", this.dtElement['dt']);
        }
        console.log("this.oldCharacterCount",this.oldCharacterCount);
        var ids          = [];
        var sortColumn = dataTablesParameters.order[0]['column'];
        var sortOrder  = dataTablesParameters.order[0]['dir'];
        var sortArray  = ['clientCode','clientName','accountType','contactNo','status','status']


        // let url    = environment.API_USER_ENDPOINT + 'partnerusers?partnerCode=1&pageNumber='+ this.paginatePage +'&pageSize=' + this.paginateLength;
        // if (dataTablesParameters.search.value == "" && (this.oldCharacterCount == 0 || this.oldCharacterCount == 3)) {


          // var clientHolder = [];
          // console.log('this.clienPortal',this.clienPortal);
          //
          // var requestData = {...this.getAllClients};
          // requestData.data.page    = this.paginatePage;
          // requestData.data.size    = this.paginateLength;
          // requestData.data.size    = this.paginateLength;
          // requestData.data.sortColumn    = sortArray[sortColumn];
          // requestData.data.sortingOrder    = sortOrder
          // this.oldCharacterCount     = 3;
          // requestData.header.userCode    = this.logedInUserRoles.userCode;
          // requestData.header.partnerCode = this.logedInUserRoles.partnerCode;
          // requestData.header.functionalityCode = "CLTA-MC";
          // if (this.clienPortal == true) {
          //   requestData.data.userCode = this.logedInUserRoles.userCode
          // }
          // else{
          //   requestData.data.userCode = ''
          // }

          var a = {...this.newAllClientReq};
          a.header.userCode = this.logedInUserRoles.userCode;
          a.data['pageNumber']    = this.paginatePage;
          a.data['pageSize']    = this.paginateLength;
          a.data['sortColumn']    = sortArray[sortColumn];
          a.data['sortType']    = sortOrder
          let url    = environment.API_ACDM_ENDPOINT + 'allclients';
          if (dataTablesParameters.search.value.length >= 3) {
            a.data['mainSearch']    = dataTablesParameters.search.value;
          }
          // let url    = environment.API_CLIENT_ENDPOINT + 'allclients';
          ////console.log('this.paginatePage: ',this.paginatePage);

          // dataTablesParameters.pageNumber = this.paginatePage;
          // this.http.post(url,requestData).toPromise()
          this.http.put(url,a).toPromise()
          .then( resp => {
            console.log('**** resp ACDM: ', resp);
            if (typeof resp['result'] != 'undefined') {
              if (typeof resp['result']['description'] != 'undefined') {
                if (resp['result']['description'].indexOf('UnAuthorized') != -1) {
                  this.notifier.notify('warning',resp['result']['description'])
                  this.router.navigateByUrl('/page-restrict');
                }
              }
            }

            this.allClients = resp['data']['clients'];
            this.totalClientsCount = resp['data']['totalClients'];

            callback({
              recordsTotal    :  this.totalClientsCount,
              recordsFiltered :  this.totalClientsCount,
              data: []
            });
            this.newAllClientReq.data.mainSearch = '';
          })
          .catch(error => {
            // Set loader false
            // this.loading = false;

            console.log("error: ",error);
          });

      },
      // columns  : [
      //   { data : 'SPD-002' },
      //   { data : 'firstName' },
      //   { data : 'lastName' },
      //   { data : 'username' },
      //   { data : 'phone' },
      //   { data : 'email' },
      //   { data : 'action' }
      // ]
      ///for search
    };
    if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-UC") != -1) {
      this.showUpdateAction = true;
    }
    if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-VC") != -1) {
      this.showUpdateAction = true;
    }
  }

  ngAfterViewInit(): void {

    var url                = environment.API_USER_ENDPOINT + 'salesrepresentatives';
    this.salesRepReq.header.userCode    = this.logedInUserRoles.userCode;
    this.salesRepReq.header.partnerCode = this.logedInUserRoles.partnerCode;
    this.salesRepReq.header.functionalityCode = "CLTA-MC";
    this.http.put(url,this.salesRepReq).subscribe(resp => {
      console.log('defaultAccountTypes',resp)
      if (typeof resp['data'] != 'undefined') {
        this.allSalesRep = resp;
        this.dtTrigger.next();
      }
      else{
        var temp = {
          data: [],
          header:[]
        }
        this.allSalesRep = temp;
        console.log('this.allSalesRep',this.allSalesRep);

        this.dtTrigger.next();
      }

    },error=>{
      var temp = {
        data: [],
        header:[]
      }
      this.allSalesRep = temp;
      console.log('this.allSalesRep',this.allSalesRep);

      this.dtTrigger.next();
    })

  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  bulkSendModal(clientID, clientEmail){
    this.fileSendClientID    = clientID;
    this.fileSendClientEmail = clientEmail;
    $('#import-registrationModal').modal('show');
  }
  onFileSelect(input) {
    console.log(input.files);
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = (e: any) => {
        // console.log('Got here: ', e.target.result);
        this.fileBase64 = e.target.result;
        // this.obj.photoUrl = e.target.result;
      }
      reader.readAsDataURL(input.files[0]);
    }
    // console.log('this.fileBase64: ', this.fileBase64);
    if(input.files && input.files.length > 0) {
      this.filePayLoad  = input.files[0];
    }
  }

  getSelectedClient(clientID){
    var a = clientID+';2';

    // var ciphertext = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
    var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
    var bx = ax.replace('+','xsarm');
    var cx = bx.replace('/','ydan');
    var ciphertext = cx.replace('=','zhar');
    console.log('ciphertext',ciphertext);


    // this.router.navigateByUrl('/edit-client/'+ciphertext)
    this.router.navigate(['edit-client', ciphertext]);
  }

  toggleStatus(clientID, status, clientIndex){
    this.clientStatus.data.clientStatus = status;
    this.clientStatus.data.clientId     = clientID;
    this.clientStatus.header.userCode    = this.logedInUserRoles.userCode;
    this.clientStatus.header.partnerCode = this.logedInUserRoles.partnerCode;
    this.clientStatus.header.functionalityCode = "CLTA-MC";

    var url                = environment.API_CLIENT_ENDPOINT + 'changeclientstatus';
    if (status == 2) {
      Swal.fire({
        title              : 'Are you sure?',
        text               : "User of this client won't be able to access the client portal!",
        icon               : 'warning',
        showCancelButton   : true,
        confirmButtonColor : '#3085d6',
        cancelButtonColor  : '#d33',
        confirmButtonText  : 'Yes, deactivate it!'
      }).then((result) => {
        if (result.value) {
          this.ngxLoader.start();
          this.httpRequest.httpPostRequests(url, this.clientStatus).then(resp => {
            this.ngxLoader.stop();
            console.log('resp',resp)
            if (typeof resp == "undefined") {
              alert('Error While Chaging Status')
            }
            else if (resp['data'] == 1) {
              this.notifier.getConfig().behaviour.autoHide = 3000;
              this.notifier.notify( "success", "Client Deactivated");
              this.allClients[clientIndex].statusId = 2;
              this.allClients[clientIndex].status = "Inactive";
              // $('#disable-'+clientID).css('display','none');
              // $('#enable-'+clientID).css('display','inline-block');
            }
            else{
              console.log("error while changing status");

            }
            // else if(resp == 1){
            //   this.notifier.notify( "success", "Client Ativated");
            //   this.allClients[clientIndex].active = 1;
            //   // $('#enable-'+clientID).css('display','none');
            //   // $('#disable-'+clientID).css('display','inline-block');
            //   // console.log("error Ehile Deleting");
            // }
            // this.reportFormat = resp;

          })

        }
      })
    }
    else{
      // this.ngxLoader.start();
      Swal.fire({
        title              : 'Are you sure?',
        text               : "To activate the client. Please confirm",
        icon               : 'warning',
        showCancelButton   : true,
        confirmButtonColor : '#3085d6',
        cancelButtonColor  : '#d33',
        confirmButtonText  : 'Yes, activate it!'
      }).then((result) => {
        if (result.value) {
          this.ngxLoader.start();
          this.httpRequest.httpPostRequests(url, this.clientStatus).then(resp => {
            this.ngxLoader.stop();
            console.log('resp',resp)
            if (typeof resp == "undefined") {
              alert('Error While Chaging Status')
            }
            else if (resp['data'] == 1) {
              if (status == 0) {
                this.allClients[clientIndex].active = 0;
                this.notifier.getConfig().behaviour.autoHide = 3000;
                this.notifier.notify( "success", "Client Deactivated");
                // $('#disable-'+clientID).css('display','none');
                // $('#enable-'+clientID).css('display','inline-block');
              }
              else if (status == 1) {
                this.allClients[clientIndex].status = "Active";
                this.allClients[clientIndex].statusId = 1;
                this.notifier.notify( "success", "Client Ativated");
                // $('#enable-'+clientID).css('display','none');
                // $('#disable-'+clientID).css('display','inline-block');
                // console.log("error Ehile Deleting");
              }

            }
            else {
              console.log("error");

            }
            // this.reportFormat = resp;

          })

        }
      })

    }



  }

}
