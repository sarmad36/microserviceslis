import { Component, OnInit, ElementRef, ViewChild, Input} from '@angular/core';
import { environment } from '../../../../../../../src/environments/environment';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { AppSettings } from '../../../services/_services/app.setting';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { HttpRequestsService } from '../../../services/_services/http-requests/http-requests.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { first } from 'rxjs/operators';
declare var $ :any;
import Swal from 'sweetalert2'
import { NotifierService } from 'angular-notifier';
import { DataTableDirective } from 'angular-datatables';
import { Subject, Observable, forkJoin } from 'rxjs';
import { UserApiEndpointsService } from '../../../services/users/user-api-endpoints.service';
import { RolesApiEndpointsService } from '../../../services/roles/roles-api-endpoints.service';
import { ClientApiEndpointsService } from '../../../services/client/client-api-endpoints.service';
import { switchMap, map } from 'rxjs/operators';
import { GlobalApiCallsService } from '../../../services/gloabal/global-api-calls.service';
import { SortPipe } from "../../../pipes/sort.pipe";
import { Console } from 'console';
import { ManagePatientsComponent } from '../../../../../../../projects/nglis_pntm_client/src/app/patients/manage-patients/manage-patients.component';
import { DataService } from "../../../../../../../src/services/data/data.service";
import { GlobalySharedModule } from './../../../../../../../src/app/sharedmodules/globaly-shared/globaly-shared.module'
import * as CryptoJS from 'crypto-js';
import { DatePipe } from '@angular/common';

@Component({
  selector   : 'app-edit-client',
  templateUrl: './edit-client.component.html',
  styleUrls  : ['./edit-client.component.css'],
  host       : {
    class    :'mainComponentStyle'
  }
})
export class EditClientComponent implements OnInit {
  imagePayLoad           ;
  product$               : Observable<any>;
  imageBase64            = '';
  selectedClientID       = 11;
  submitted              = false;
  submitted2             = false;
  conactHolder           = [{
    ""             : "",
    "ContactNo"    : "a",
    "Fax"          : "a",
    "Email"        : "aa",
    "Default"      : "true",
  }]
  conactHolderUpdate     = [];
  // conactHolder           = [];
  contactData            = false;
  tableSize              ;
  setCross               ;
  userShowData           = [];
  appendIcons            ;
  deleteuserCode           ;
  ///////// addd and remove row from Contact table
  public fieldArray      : Array<any> = [];
  public newAttribute    : any = {};
  ///////// addd and remove row from Contact table
  clientUserTable;
  public clientForm      : FormGroup;
  public clientUserForm  : FormGroup;
  public userContactForm : FormGroup;
  defaultAccountTypes     ;
  defaultAccountPriority  ;
  defaultPCOrganization  = AppSettings.defaultPCOrganization;
  defaultReqType          ;
  defaultAttType          ;
  defaultFileType         ;
  deliverMethod           ;
  defaultClientUserRole  = AppSettings.defaultClientUserRole;
  ClientTypeHolder       ;
  requisitionDTOTEMP     : any = [];
  AttachmentTypeHolder   ;
  AttachmentFileHolder;

  ///////////////// Template Helper SaveData
  reqTypeTemplate        : any = {}
  ///////////////// Template Helper SaveData
  contactFormat          : any = [];
  tempContact            : any = {};
  tempDeliver            : any = [];
  tempDev                : any = {};
  saveDataReqType        : any = [];
  saveDataReqTypeTemp    : any = {
    // formId: 1,
    // addedBy:1,
    // addedTimestamp:null,
    // updatedBy:null,
    // updatedTimestamp:null
  };


  saveDataClientType     : any = [];
  saveDataClientTypeTemp : any = {
    // attachmentTypeId : null
  };

  saveDataReqAttType     : any = [];
  saveDataReqAttTypeTemp : any = {
    // attachmentTypeId : null
  };

  saveDataReqFIleType     : any = [];
  saveDataReqFIleTypeTemp : any = {
    // attachmentFileTypeId : null
  };
  saveDataRequistionTemp: any = {};
  saveDataRequistion    : any = [];

  // requisitionDTOTEMP     ;
  // AttachmentFileHolder   ;
  // ClientTypeHolder       ;
  // AttachmentTypeHolder   ;

  //////////////// Update Request Selects Models
  UpdateFormClientTypeHolder       ;
  UpdateFormAttachmentFileHolder   ;
  UpdateFormAttachmentTypeHolder   ;
  updateRequisitionDTOTEMP         ;
  deliveryMethodUpdate             ;
  updateSRFname                    ;
  updateSRLname                    ;
  updateSREmail                    ;
  updateSRContact                  ;
  contactLength                    = 1;

  selectedPostNominal             ;
  dPostNominal           : any = [];
  allTittles             : any = [];

  UName                  ;
  FName                  ;
  LName                  ;
  defaultRoles           ;
  validUsername          = true;
  inValidUsername        = false;
  inValidUsernpi         = false;
  inValidUseremail       = false;

  editClientAction = false;
  viewClientAction = false;
  viewClientUserAction = false;
  manageClientUserAction = false;
  updateClientUserAction =false;


  public userNameReq     =
  {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode: "CLTA-UC",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 : {
      username           : '',
      // partnerCode          : 1
    }
  }

  public clientUserReq   : any = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"CLIM",
      functionalityCode: "CLTA-UC",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      clientId          :"",
      userCode            :"",
      superUser         :"",
      visibleInCases    :"",
      isPhysician       :"",
      defaultPhysician  :"",
      practiseCode      :"",
      addedBy           :"",
      addedTimestamp    :""
    }
  }



  public newReqTemp      : any = {
    formId: 1,
    name: 'Hematology',
    addedBy:1,
    addedTimestamp:null,
    updatedBy:null,
    updatedTimestamp:null
  };
  public allRolesReq     =
  {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode: "CLTA-UC",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 :{
      roleType:          1
    }
  }
  public requestData     : any = {
    header               : {
      uuid               : "",
      partnerCode        : "sip",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode: "CLTA-UC",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data: {
      partnerCode          : 1,
      // partnerCode        : "sip",
      user               : {
        userCode         : "",
        // displayCode      : "",
        firstName        : "",
        lastName         : "",
        username         : "",
        password         : "",
        email            : "",
        phone            : "",
        photo            : null,
        title            : null,
        npi              : null,
        postNominal      : null,
        userType         : 2,
        active           : true,
        agreementActive  : true,
        accessCode       : "",
        isDoctor         : false,
        createdBy        :1,
        createdTimestamp :"2020-08-28T19:10:37.000+00:00",
        roles            :[]
      },
      createdBy          :1
    }
  }

  public updateclientusercontact : any = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"CLIM",
      functionalityCode: "CLTA-UC",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      contactId    :"",
      contactTypeId:"",
      value:"",
      createdBy:1,///// loggedin user
      createdTimestamp: "2020-09-17T04:13:52.551+00:00",
      updatedBy:850,///// loggedin user who has changed it
      clientId:387,
      userCode:567,
      updatedTimestamp:"",
      moduleCode:"CLTM",
      defaultContact:0,
    }
  }

  public updateClientUser : any = {
    clientId:1,
    userCode:1,
    superUser:"",
    visibleInCases:"",
    isPhysician:"",
    defaultPhysician:"",
    practiseCode:"",
    addedBy:"",
    addedTimestamp:"",
    active:""
  }



  public selectedData     : any = {
    // clientId:1,
    // accountNumber:"1111",
    // partnerCode:1,
    // salesRepresentativeId:1,
    // parentClientId:1,
    // name:"test client",
    // defaultPriority:null,
    // logo:"",
    // addressId:11,
    // defaultReportFormatId:1,
    // showWordFile:1,
    // encryptionCode:"abcdef1223",
    // allowedFilesize:12,
    // notes:"asdsadasdsadasdasdas",
    // active:1,
    // AddressDetailsDto:{
    //   addressId:11,
    //   street:"abc street",
    //   suiteFloorBuilding:"1",
    //   city:1,
    //   zip:"11111",
    //   stateId:1,
    //   stateOther:"",
    //   countryId:1,
    //   createdBy:1,
    //   createdTimestamp:null,
    //   updatedBy:null,
    //   updatedTimestamp:null,
    //   moduleCode:"CLTM"
    // },
    // ContactDto : [],
    // ClientRequisitionTypeDto:[],
    // DeliveryMethodDto:[],
    // AttachmentTypeDto:  [],
    // AttachmentFileTypeDto:[],
    // ClientTypeDto:[]
  }

  public saveClientData  : any = {
    otherData            : [],
    contactInfo          : [],
    imagePayload         : [],
    imageBase64          : '',
    clientID             : 0
  };
  public SaveCUserData  : any = {
    clientUserData      : []
  }
  swalStyle              ;
  allCountries           ;
  allProvinces           ;
  allCities              ;
  reportFormat           ;

  testContactData = [
    {
      contactTypeId:1,
      value:"cell1",
      createdBy:null,
      createdTimestamp:null,
      updatedBy:null,
      updatedTimestamp:null,
      moduleCode:"CLTM"
    },
    {
      contactTypeId:2,
      value:"fax1",
      createdBy:null,
      createdTimestamp:null,
      updatedBy:null,
      updatedTimestamp:null,
      moduleCode:"CLTM"
    },
    {
      contactTypeId:3,
      value:"email1",
      createdBy:null,
      createdTimestamp:null,
      updatedBy:null,
      updatedTimestamp:null,
      moduleCode:"CLTM"
    },
    {
      contactTypeId:1,
      value:"cell2",
      createdBy:null,
      createdTimestamp:null,
      updatedBy:null,
      updatedTimestamp:null,
      moduleCode:"CLTM"
    },
    {
      contactTypeId:2,
      value:"fax2",
      createdBy:null,
      createdTimestamp:null,
      updatedBy:null,
      updatedTimestamp:null,
      moduleCode:"CLTM"
    },
    {
      contactTypeId:3,
      value:"email2",
      createdBy:null,
      createdTimestamp:null,
      updatedBy:null,
      updatedTimestamp:null,
      moduleCode:"CLTM"
    },
    {
      contactTypeId:1,
      value:"cell3",
      createdBy:null,
      createdTimestamp:null,
      updatedBy:null,
      updatedTimestamp:null,
      moduleCode:"CLTM"
    },
    {
      contactTypeId:2,
      value:"fax3",
      createdBy:null,
      createdTimestamp:null,
      updatedBy:null,
      updatedTimestamp:null,
      moduleCode:"CLTM"
    },
    {
      contactTypeId:3,
      value:"email3",
      createdBy:null,
      createdTimestamp:null,
      updatedBy:null,
      updatedTimestamp:null,
      moduleCode:"CLTM"
    }
  ];

  public updateRequestData : any = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"CLIM",
      functionalityCode: "CLTA-UC",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data: {}
  }

  // public updateClientUser : any = {
  // clientId:"1",
  // userCode:"",
  // superUser:"",
  // visibleInCases:"",
  // isPhysician:"",
  // defaultPhysician:"",
  // practiseCode:"",
  // addedBy:"",
  // addedTimestamp:""
  // }

  public deleteClientUserData = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"CLIM",
      functionalityCode: "CLTA-UC",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      clientId:1,
      userCode:"",
    }
  }

  updateAccountNumber;
  updateAccountNumberOLD;
  updateAccountName;
  updateStreet;
  updateCountry;
  updateState;
  updateCity;
  updatesuiteFloorBuilding;
  updateZip;
  updateSalesRepresentativeId;
  updateShowWordFile;
  updateEncryptionCode;
  updateAllowedFilesize;
  updateAccountNotes  ;



  clientActiveToggle;
  public clientStatus   : any ={
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"CLIM",
      functionalityCode: "CLTA-UC",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      clientStatus : 0,
      clientId     : 1
    }
  }

  contactEmailValidity = true;
  contactRowValid        = false;
  contactTablePhone;
  contactTableFax;
  disabledSalesRepFields = true;

  encryptionCodeRequired = false;
  addedClientID           ;

  partnerUsersID         : any[];

  public PUsersRequest: any = {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode: "CLTA-UC",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 : {
      userCodes            :[]
    }
  }
  // public getClientUsers  : any ={
  //   page     : 1,
  //   size     : 3,
  //   clientId : 1
  // }
  public getClientUsers  : any ={
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"CLIM",
      functionalityCode: "CLTA-UC",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      page     : 1,
      size     : 3,
      clientId : 1

    }
  }

  public getClientUsersNew  : any ={
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"CLIM",
      functionalityCode: "CLTA-UC",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      // pageNumber: 0,
      // pageSize: 20,
      // sortColumn: "userCode",
      // clientId:"",
      // sortType: "desc"

    }
  }

  public searchClientUsers  : any ={
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"CLIM",
      functionalityCode: "CLTA-UC",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      page     : 1,
      size     : 3,
      clientId : 1,
      sortColumn: "userCode",
      sortingOrder: "asc",
      searchString: ""
    }
  }

  public userByNpi: any = {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode: "CLTA-UC",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 : {
      npi                : "",
      email              : "",
      userType           : 2,
    }
  }

  validProvidedEmail     = false;

  accNumberExist        = false
  addedClientAcc        ;
  clientIdForUsers      ;

  // clientActiveToggle;
  // public clientStatus   : any ={
  //   clientStatus : 0,
  //   clientId     : 1
  // }
  validPartnerUsers     : any = [];
  enableCompleteSuperCheck    = 0;
  enableCompleteDefaultCheck  = 0;

  public disableUserReq  =
  {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode: "CLTA-UC",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 : {
      userCode             : 1,
      status             : false
    }
  }

  // emptyClientUserHolder  : any = {
  //   active: 0,
  //   addedBy: 0,
  //   addedTimestamp: null,
  //   clientId: 0,
  //   contactDetails: [],
  //   defaultPhysician: 0,
  //   isPhysician: 0,
  //   practiseCode: "",
  //   superUser: 0,
  //   userData: {},
  //   userCode: 1,
  //   visibleInCases: 0
  // }
  public clientUSerStatus : any = {

    clientId:0,
    userCode:0,
    superUser:"",
    visibleInCases:"",
    isPhysician:"",
    defaultPhysician:"",
    practiseCode:"",
    addedBy:1,
    addedTimestamp:"",
    active:0


  }

  hideButtonView = false;
  addClientTabPan  = true;
  clientUserTabPan = false;

  public salesRepReq     =
  {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode: "CLTA-UC",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 : ""
  }
  defaultSalesReps       : any;

  public externalCLient = {
    page:"0",
    size:"10",
    sortColumn:"",
    sortingOrder:"asc",
    searchString:"131"
  }

  public parentCLientReq : any = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"CLIM",
      functionalityCode: "CLTA-UC",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:      {
      clientId:1515,
      // userCodes:[ 1165,1210,1243] //opt
    }
  }
  addUserManually = true;
  responseUserNPIEmail = {};


  @ViewChild(DataTableDirective, {static: false})
  dtElement              : DataTableDirective;
  dtTrigger              : Subject<EditClientComponent> = new Subject();
  public dtOptions       : DataTables.Settings = {};
  allClients             : any;
  public paginatePage    : any = 0;
  public paginateLength  = 20;
  totalUSersCount        ;
  totalUSersCount2       = 0;

  oldCharacterCount      = 0;

  public searchFromUser  : any = {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode: "CLTA-UC",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 : {
      clientId           : 1,
      searchQuery        : ""
    }
  }
  public logedInUserRoles :any = {};
  AssignedFileName = '';
  selectedClientIDofPatient = '0';
  selectedClientAccountNumber = null;
  // @ViewChild(ManagePatientsComponent) child;
  message:string;
  returnFromPatient = false;

  clientsDropdown  : any = [];
  clientSearchString          ;
  clientLoading               : any;
  searchTerm$ = new Subject<string>();



  constructor(
    private formBuilder  : FormBuilder,
    private http         : HttpClient,
    private route        : ActivatedRoute,
    private router       : Router ,
    private httpRequest  : HttpRequestsService,
    private ngxLoader    : NgxUiLoaderService,
    private sanitizer    : DomSanitizer,
    private  notifier    : NotifierService,
    private userService  : UserApiEndpointsService,
    private roleService  : RolesApiEndpointsService,
    private clientService: ClientApiEndpointsService,
    private globalService: GlobalApiCallsService,
    private sortPipe     :SortPipe,
    private sharedData: DataService,
    private datePipe: DatePipe,
    private encryptDecrypt    : GlobalySharedModule
  ) {
    this.notifier        = notifier;
    this.swalStyle       =  Swal.mixin({
      customClass : {
        confirmButton : 'btn btn-success my-swal-success',
        cancelButton  : 'btn btn-danger'
      },
      buttonsStyling  : true
    })
    // console.log("constructor",this.notifier);
    // console.log("this.child",this.child);
    // console.log("this.child.message",this.child.message);


  }

  setClientIdForPatient() {
    console.log('this.selectedClientIDofPatient',this.selectedClientIDofPatient);
    // console.log('this.selectedClientAccountNumber',this.selectedClientAccountNumber);
    localStorage.setItem("clientCodeForPatient",JSON.stringify(this.selectedClientAccountNumber));


    // this.sharedData.changeMessage("Hello from Sibling")
    this.sharedData.changeMessage(this.selectedClientIDofPatient);
    // $('#all-patient-div-client').css('visibility','visible');
    // this.sharedData.selectedClientCode(this.selectedClientAccountNumber);
  }

  ngOnInit(): void {
    this.updateclientusercontact.data.updatedby = this.logedInUserRoles.userCode;
    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
    var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
    var originalText = bts.toString(CryptoJS.enc.Utf8);
    this.logedInUserRoles = JSON.parse(originalText)
    this.sharedData.currentClientId.subscribe(message => this.message = message)
    // console.log("constructor",this.notifier);
    // console.log("this.child",this.child);
    // console.log("this.child.message",this.child.message);

    if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-UC") != -1) {
      this.getClientUsers.header.functionalityCode    = "CLTA-UC";
      this.getClientUsersNew.header.functionalityCode    = "CLTA-UC";
      this.PUsersRequest.header.functionalityCode     = "CLTA-UC";
      // this.salesRepReq.header.functionalityCode    = "CLTA-UC"
      // this.allRolesReq.header.functionalityCode    = "CLTA-UC";
    }
    else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-VC") != -1){
      this.getClientUsers.header.functionalityCode    = "CLTA-VC";
      this.getClientUsersNew.header.functionalityCode    = "CLTA-VC";
      this.PUsersRequest.header.functionalityCode     = "CLTA-VC";
      // this.salesRepReq.header.functionalityCode    = "CLTA-VC";
      // this.allRolesReq.header.functionalityCode    = "CLTA-VC";
    }

    this.notifier.hideAll();
    this.getLookups().then(lookupsLoaded => {

      this.ngxLoader.start();
      /////////  ROles form // DB
      var url        = environment.API_ROLE_ENDPOINT + 'allroles';
      this.allRolesReq.header.partnerCode = this.logedInUserRoles.partnerCode;
      this.allRolesReq.header.userCode    = this.logedInUserRoles.userCode;
      this.allRolesReq.header.functionalityCode    = "CLTA-MC";
      this.roleService.getAllRoles(url, this.allRolesReq).subscribe(resp => {
        console.log('resp Role',resp)
        this.defaultRoles = resp['data'];
      },error=>{
        this.ngxLoader.stop();
        this.notifier.getConfig().behaviour.autoHide = 3000;
        this.notifier.notify( "error", "Error while loading roles" );
      })

      var url                = environment.API_USER_ENDPOINT + 'salesrepresentatives';
      // this.userService.getSalesRepresentatives(url, this.salesRepReq).then(resp => {
      this.salesRepReq.header.userCode    = this.logedInUserRoles.userCode;
      this.salesRepReq.header.partnerCode = this.logedInUserRoles.partnerCode;
      this.salesRepReq.header.functionalityCode    = "CLTA-MC";

      this.httpRequest.httpPutRequests(url, this.salesRepReq).then(resp => {
        console.log('defaultSalesReps',resp)
        this.defaultSalesReps = resp['data'];
        // this.ngxLoader.stop();
      },error=>{
        this.ngxLoader.stop();
        this.notifier.getConfig().behaviour.autoHide = 3000;
        this.notifier.notify( "error", "Error while loading slaes representatives" );
      })
      // this.product$ = this.route.paramMap.pipe(
      //   switchMap((params: Iterable<any>) => {
      //     // console.log("params+++++++++++",params);
      //
      //     return params;
      //   }),
      // )
      // console.log("product$",this.product$);

      console.log('beforeRour');

      this.route.params.subscribe(params => {
        console.log('params11111',params);

        // var bytes  = CryptoJS.AES.decrypt(params['id'], 'nglis');
        //
        // var originalText = bytes.toString(CryptoJS.enc.Utf8);
        // console.log('originalText',originalText);
        var coverted = params['id'].replace('xsarm','+').replace('ydan','/').replace('zhar','=');
        var bytes  = CryptoJS.AES.decrypt(coverted, 'nglis');
        var originalText = bytes.toString(CryptoJS.enc.Utf8).toString();
        console.log("originalText",originalText);
        var test = originalText.split(';')
        console.log("test",test);



        if (typeof test[0] == "undefined") {
          this.notifier.notify( "warning", "No User Found" );
          this.ngxLoader.stop();
          return;
        }
        // let id = params['id'];
        let id = test[0];
        // console.log('id',id);
        let from = test[1];
        console.log("from-=-=-=-",from);

        // let from = params['from'];
        if (from == 1) {
          console.log('from 1');


          this.addClientTabPan  = false;
          this.clientUserTabPan = true;
          $('#manage_client_user').tab('show');

          // this.notifier.getConfig().behaviour.autoHide = 3000;
          // this.notifier.notify( "success", "Client saved Successfully" );
          // ////////////this.getAllUsers()
          // this.notifier.getConfig().behaviour.autoHide = 3000;
          // this.notifier.notify( "success", "Client saved Successfully" );
          // $('#update_client').tab('dispose');


        }
        else if(from == 2){

          ////// coming from Manage page
          this.addClientTabPan  = true;
          this.clientUserTabPan = false;
          $('#update_client').tab('show')

        }
        else if(from == 3){
          // localStorage.setItem("backFromPatientToClient","true")
          this.returnFromPatient = true;
          // $('#openPatienTab').trigger('click');


        }

        // console.log("id",id);
        var reqD = {
          header:{
            uuid:"",
            partnerCode:"",
            userCode:"",
            referenceNumber:"",
            systemCode:"",
            moduleCode:"CLIM",
            functionalityCode: "CLTA-UC",
            systemHostAddress:"",
            remoteUserAddress:"",
            dateTime:""
          },
          data:{
            userCode:id
          }
        }
        var url                = environment.API_CLIENT_ENDPOINT + 'clientbyid';

        reqD.header.partnerCode = this.logedInUserRoles.partnerCode;
        reqD.header.userCode    = this.logedInUserRoles.userCode;
        if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-VC") != -1) {
          reqD.header.functionalityCode    = "CLTA-VC";
        }
        else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-UC") != -1){
          reqD.header.functionalityCode    = "CLTA-UC";
        }
        // reqD.header.functionalityCode    = "CLTA-MC";

        this.httpRequest.httpPutRequests(url,reqD).then(resp => {
          console.log("**********selectedClient Response",resp);
          // this.ngxLoader.stop();
          // this.allUsers.push(resp);
          // this.ngxLoader.stop();
          if (typeof resp['result'] != 'undefined') {
            if (typeof resp['result']['description'] != 'undefined') {
              if (resp['result']['description'].indexOf('UnAuthorized') != -1) {
                this.notifier.notify('warning',resp['result']['description'])
                this.router.navigateByUrl('/page-restrict');
              }
            }
          }
          if (typeof resp['data'] != 'undefined') {
            this.updateRequestData.data = resp['data'];
            console.log("this.updateRequestData",this.updateRequestData);

            if (this.updateRequestData.data.active == 1 || this.updateRequestData.data.active == 2) {
              this.hideButtonView = true;
            }
            else{
              this.hideButtonView = false;
            }
            console.log('------------------------------',resp['data']['clientId'])
            this.selectedClientID   = resp['data']['clientId'];
            this.addedClientID      = resp['data']['clientId'];
            this.selectedClientIDofPatient = resp['data']['clientId'];
            this.selectedClientAccountNumber = resp['data']['accountNumber'];
            if (this.returnFromPatient == true) {
              // this.setClientIdForPatient()
              // $('#openPatienTab').trigger('click');
              localStorage.setItem("clientCodeForPatient",JSON.stringify(this.selectedClientAccountNumber));
              localStorage.setItem("backFromPatientToClient",JSON.stringify(this.selectedClientID))
              this.sharedData.changeMessage(this.selectedClientIDofPatient);
              console.log("");

              $('#openPatienTab').trigger('click');

            }
            // this.sharedData.changeMessage(this.addedClientID);
            this.populateUpdateForm(this.addedClientID, "onLoad")

            ////////// this.getAllUsers(0)

          }
          else{
            this.ngxLoader.start();
            this.notifier.notify( "error", "No client found");
          }

        })
        // console.log("********************************************");
        // let guid = params['guid'];
        //
        // console.log(`${id},${guid}`);
      });


    });
    // console.log("$('#inputGroupFile01')",$('#inputGroupFile01'));
    // console.log("$('#inputGroupFile01').files;",$('#inputGroupFile01')[0].files);
    this.clientService.search(this.searchTerm$)
    .subscribe(results => {
      console.log("results",results);
      this.clientsDropdown = results['data'];
      this.clientLoading = false;
    },
    error => {
      this.ngxLoader.stop();
      this.notifier.notify( "error", "Error while fetching clients backend connect aborted" );
      this.clientLoading = false;
      return;
      // console.log("error",error);
    });



    // var url               = environment.API_USER_ENDPOINT + 'update';
    this.clientForm       = this.formBuilder.group({
      accountType         : [1, [Validators.required]],
      accountId           : ['111', [Validators.required]],
      clientName          : ['sarmad', [Validators.required]],
      priority            : [1, [Validators.required]],
      pcOrganization      : [1],
      // clientLogo          : [''],
      // fileSource          : [''],
      notes               : [''],
      streetAddress       : ['', [Validators.required]],
      building            : [''],
      zip                 : ['', [Validators.required]],
      country             : ['', [Validators.required]],
      state               : [1, [Validators.required]],
      city                : [1, [Validators.required]],
      internalUser        : ['', [Validators.required]],
      srFirstName         : ['asdasd'],
      srLastName          : ['asdasd'],
      srEmail             : ['asdasd@mail.com'],
      srContact           : ['asdasd'],
      reportFormat        : [1, [Validators.required]],
      wordFile            : [true],
      delivery            : this.formBuilder.array([], [Validators.required]),
      encryption          : [''],
      reqType             : [1, [Validators.required]],
      fileSize            : ['', [Validators.required]],
      attType             : [1, [Validators.required]],
      fileType            : [1, [Validators.required]],
      activedeactive      : ['']
    });

    this.tableSize        = this.conactHolder.length;
    if (this.conactHolder.length > 1) {
      this.setCross       = true;
    }

    this.clientUserForm  = this.formBuilder.group({
      clientUserRole     : ['', [Validators.required]],
      clientUserNpi      : [''],
      clientUserEmail    : ['', [Validators.required, Validators.email]],
      clientUserPNominal : ['', [Validators.required]],
      clientUserTitle    : ['', [Validators.required]],
      clientUserFName    : ['', [Validators.required]],
      clientUserLName    : ['', [Validators.required]],
      clientUserUName    : ['', [Validators.required]],
      clientUserContact  : ['', [Validators.required,Validators.minLength(13)]]
    });


    if(this.logedInUserRoles['allowedRoles'].indexOf("CLTA-UC") != -1){
      this.editClientAction = true;

    }
    if(this.logedInUserRoles['allowedRoles'].indexOf("CLTA-VC") != -1){

      if(this.logedInUserRoles['allowedRoles'].indexOf("CLTA-UC") == -1){
        this.viewClientAction = true;
        this.clientForm.disable();
      }


    }
    if(this.logedInUserRoles['allowedRoles'].indexOf("USRA-MUE") != -1){
      this.manageClientUserAction = true;

    }
    if(this.logedInUserRoles['allowedRoles'].indexOf("USRA-VUE") != -1){
      this.viewClientUserAction = true;

    }
    if(this.logedInUserRoles['allowedRoles'].indexOf("USRA-UUE") != -1){
      this.updateClientUserAction = true;

    }
    // console.log('this.viewClientUserAction----',this.viewClientUserAction);



    // this.bindJQueryFuncs();


    this.dtOptions = {
      pagingType   : 'full_numbers',
      pageLength   : this.paginateLength,
      serverSide   : true,
      processing   : true,
      ordering     : false,
      "lengthMenu": [20, 50, 75, 100 ],
      ajax: (dataTablesParameters: any, callback) => {
        if(typeof this.dtElement['dt'] != 'undefined'){
          this.paginatePage           = this.dtElement['dt'].page.info().page;
          this.paginateLength         = this.dtElement['dt'].page.len();
          // console.log("this.dtElement['dt']: ", this.dtElement['dt']);
        }
        // console.log("this.selectedClientID------------//////////",this.selectedClientID);
        if (typeof this.selectedClientID == "undefined") {
          this.ngxLoader.stop();
          return
        }

          // if (this.oldCharacterCount == 3) {
          //   this.notifier.hideAll();
          // }
          if (dataTablesParameters.search.value != "" && (this.oldCharacterCount == 0 || this.oldCharacterCount == 3)) {
            if (this.oldCharacterCount == 3) {
              this.notifier.hideAll();
            }
            this.getClientUsersNew['data']['mainSearch'] = dataTablesParameters.search.value;

          }
          else{

              $('.dataTables_processing').css('display',"none");
              if (this.oldCharacterCount == 3) {
                this.oldCharacterCount     = 3

              }
              else{
                this.oldCharacterCount     = 2;
              }

          }
          this.partnerUsersID = [];
          var temporaryContact   = [];
          var userShowDataTemp   = [];

          var userClientDetails  = [];

          this.getClientUsersNew['data']['pageNumber']     = this.paginatePage;
          this.getClientUsersNew['data']['pageSize']       = this.paginateLength;
          this.getClientUsersNew['data']['clientId']       = this.selectedClientID;
          this.getClientUsersNew['data']['sortColumn']       = 'userCode';
          this.getClientUsersNew['data']['sortType']       = 'desc';

          this.oldCharacterCount = 3

          // let url    = environment.API_CLIENT_ENDPOINT + 'clientusers';
          let url    = environment.API_ACDM_ENDPOINT + 'allclientusers';
          // console.log('this.paginatePage: ',this.paginatePage);
          this.getClientUsersNew.header.partnerCode = this.logedInUserRoles.partnerCode;
          this.getClientUsersNew.header.userCode    = this.logedInUserRoles.userCode;
          // this.getClientUsers.header.functionalityCode    = "USRA-UUE";
          this.httpRequest.httpPutRequests(url, this.getClientUsersNew).then(resp => {
            this.ngxLoader.stop();
            console.log('Client Users Resp',resp);
            if (typeof resp['message'] != 'undefined') {
              this.notifier.notify("error",resp['message']);
              this.totalUSersCount = 0;
              callback({
                recordsTotal    :  0,
                recordsFiltered :  0,
                data: []
              });
              this.getClientUsersNew.data = {};

              return;
            }
            if (resp['data'] == null) {
              this.notifier.notify("error",resp['result']['description']);
              this.totalUSersCount = 0;
              callback({
                recordsTotal    :  0,
                recordsFiltered :  0,
                data: []
              });
              this.getClientUsersNew.data = {};


              return
            }
            if (resp['data']['users'] == null) {
              // this.notifier.notify("error",resp['result']['description']);
              this.totalUSersCount = 0;
              callback({
                recordsTotal    :  0,
                recordsFiltered :  0,
                data: []
              });
              this.getClientUsersNew.data = {};

              return
            }
            if (resp['data']['users'] != null) {
              for (let k = 0; k < resp['data']['users'].length; k++) {
                if (resp['data']['users'][k]['clientProvidedEmail'] != null) {
                  resp['data']['users'][k]['clientProvidedEmail'] = this.encryptDecrypt.decryptString(resp['data']['users'][k]['clientProvidedEmail']);
                }
                if (resp['data']['users'][k]['clientProvidedFax'] != null) {
                  resp['data']['users'][k]['clientProvidedFax'] = this.encryptDecrypt.decryptString(resp['data']['users'][k]['clientProvidedFax']);
                }
                  // allData['data'][l]['clientName']  =  patient['data'][k].firstName;



              }
              // this.notifier.notify("error",resp['result']['description']);
              this.totalUSersCount = resp['data'].totalUsers;
              this.userShowData = resp['data']['users'];
              resp['data']['users'].some(el =>{
                // console.log("el",el);

                if (el.isDefaultPhysician === 1) {
                  this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck +1;

                }
                if (el.isSuperUser === 1){
                    this.enableCompleteSuperCheck   = this.enableCompleteSuperCheck + 1;

                }

              });

              // if (userShowDataTemp[i].defaultPhysician == 1) {
              //   this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck +1;
              // }
              // if (userShowDataTemp[i].superUser == 1) {
              //   this.enableCompleteSuperCheck   = this.enableCompleteSuperCheck + 1;
              // }

              if (this.enableCompleteSuperCheck == 0 && this.enableCompleteDefaultCheck > 0) {
                this.notifier.getConfig().behaviour.autoHide = false;
                this.notifier.notify( "warning", "You need to define a super user for this client");
              }
              else if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck == 0) {
                this.notifier.getConfig().behaviour.autoHide = false;
                this.notifier.notify( "warning", "You need to define an attending physician for this client");
              }
              else if (this.enableCompleteSuperCheck == 0 && this.enableCompleteDefaultCheck == 0) {

                this.notifier.getConfig().behaviour.autoHide = false;
                this.notifier.notify( "warning", "One super user, and one attending physician must be defined in order to add cases for this client");
              }


              this.userShowData = resp['data']['users'];
              callback({
                recordsTotal    :  this.totalUSersCount,
                recordsFiltered :  this.totalUSersCount,
                data: []
              });
              this.getClientUsersNew.data = {};

              return;
            }
            return;


            // if (resp[0].userDetails['length'] > 0) {
            for (let i = 0; i < resp['data']['details']['length']; i++) {
              // this.partnerUsersID.push(resp[0].userDetails[i].userCode);
              //   userShowDataTemp[i] = resp[0].userDetails[i];
              if (resp['data']['details'][i].userCode > 0 || typeof resp['data']['details'][i].userCode == "string") {
                this.partnerUsersID.push(resp['data']['details'][i].userCode);
                userShowDataTemp[i] = resp['data']['details'][i];
                if (userShowDataTemp[i].defaultPhysician == 1) {
                  this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck +1;
                }
                if (userShowDataTemp[i].superUser == 1) {
                  this.enableCompleteSuperCheck   = this.enableCompleteSuperCheck + 1;
                }

              }
              else{
              }
            }
            if (this.enableCompleteSuperCheck == 0 && this.enableCompleteDefaultCheck > 0) {
              this.notifier.getConfig().behaviour.autoHide = false;
              this.notifier.notify( "warning", "You need to define a super user for this client");
            }
            else if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck == 0) {
              this.notifier.getConfig().behaviour.autoHide = false;
              this.notifier.notify( "warning", "You need to define an attending physician for this client");
            }
            else if (this.enableCompleteSuperCheck == 0 && this.enableCompleteDefaultCheck == 0) {

              this.notifier.getConfig().behaviour.autoHide = false;
              this.notifier.notify( "warning", "One super user, and one attending physician must be defined in order to add cases for this client");
            }
            // }

            console.log("idsss:", this.partnerUsersID);
            // console.log("userShowDataTemp:", userShowDataTemp);
            var url2 = environment.API_USER_ENDPOINT + 'clientusers';
            this.PUsersRequest.data.userCodes = this.partnerUsersID;
            this.PUsersRequest.header.userCode    = this.logedInUserRoles.userCode;
            this.PUsersRequest.header.partnerCode = this.logedInUserRoles.partnerCode;
            // this.PUsersRequest.header.functionalityCode = "USRA-UUE";
            this.userService.getClientUsers(url2, this.PUsersRequest).subscribe(response2 => {
              if (response2['result']['description'].indexOf('UnAuthorized Call') != -1) {
                this.ngxLoader.stop()
                return;
              }
              // this.ngxLoader.stop();
              console.log('resp********** User ',response2)
              var uniqUsers = []
              if (response2 != false) {
                for (let i = 0; i < response2["data"].length; i++) {
                  // if (response2["data"].length < userShowDataTemp.length) {
                  for (let index = 0; index < userShowDataTemp.length; index++) {
                    var a  = this.encryptDecrypt.decryptString(userShowDataTemp[index]['userCode']);
                    if (response2["data"][i]['userCode'] == userShowDataTemp[index]['userCode']) {
                      userShowDataTemp[index]['userData'] = response2["data"][i];
                    }
                    // else{
                    //   if (typeof userShowDataTemp[index]['userData'] == 'undefined') {
                    //     userShowDataTemp.splice(index,1)
                    //     index--;
                    //   }
                    // }

                  }
                }
                userShowDataTemp = userShowDataTemp.filter(function( obj ) {
                  return typeof obj.userData != 'undefined';
                });
              }

              if (userShowDataTemp.length == 0) {
                this.ngxLoader.stop();
                // $('.dataTables_processing').css('display',"none");
                callback({

                  recordsTotal    :  0,
                  recordsFiltered :  0,
                  data: []
                });
              }
              var count = 0;
              // var totalLengthOfValidRecord = 0;
              // console.log("uniqUsers",uniqUsers);
              console.log("userShowDataTemp",userShowDataTemp);
              for (let i = 0; i < userShowDataTemp.length; i++) {
                // for (let i = 0; i < uniqUsers.length; i++) {
                // if(typeof userShowDataTemp[i]['userData'] != 'undefined'){
                //
                //   totalLengthOfValidRecord ++;


                var uid = userShowDataTemp[i]['userData'].userCode;
                // var uid = uniqUsers[i];
                console.log('uid',uid);
                var reqData ={
                  header:{
                    uuid:"",
                    partnerCode:"",
                    userCode:"",
                    referenceNumber:"",
                    systemCode:"",
                    moduleCode:"CLIM",
                    functionalityCode: "CLTA-UC",
                    systemHostAddress:"",
                    remoteUserAddress:"",
                    dateTime:""
                  },
                  data:{
                    userCode: uid

                  }
                }
                reqData.header.partnerCode = this.logedInUserRoles.partnerCode;
                reqData.header.userCode    = this.logedInUserRoles.userCode;
                if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-UC") != -1) {
                  reqData.header.functionalityCode    = "CLTA-UC";
                }
                else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-VC") != -1){
                  reqData.header.functionalityCode    = "CLTA-VC";
                }

                // var url3 = environment.API_CLIENT_ENDPOINT + 'clientuserscontact/'+uid;
                var url3 = environment.API_CLIENT_ENDPOINT + 'clientuserscontact';
                this.httpRequest.httpPostRequests(url3, reqData).then(clientuserscontact => {
                  if (typeof clientuserscontact['error'] != 'undefined') {
                    this.ngxLoader.stop()
                    return;
                  }
                  if (clientuserscontact['result']['description'].indexOf('UnAuthorized Call') != -1) {
                    this.ngxLoader.stop()
                    return;
                  }
                  if (clientuserscontact['data'] == null) {
                    temporaryContact[0]     = "";
                    temporaryContact[1]     = "";
                    // this.ngxLoader.stop()
                    // return;
                  // }
                  }
                  if (clientuserscontact['data'] != null) {



                  // console.log('resp///////////////// CLient ',clientuserscontact);
                  if (clientuserscontact['data'].length > 0) {
                    // console.log(">>0",clientuserscontact['data']);
                    // temporaryContact   = []

                    for(let j = 0; j < clientuserscontact['data'].length; j++){


                      if (clientuserscontact['data'][j]['contactTypeId'] == 2) {
                        clientuserscontact['data'][j]['value'] = this.encryptDecrypt.decryptString(clientuserscontact['data'][j]['value']);
                        temporaryContact[0] = clientuserscontact['data'][j];
                      }
                      else{
                        if (typeof temporaryContact[0] == 'undefined') {
                          temporaryContact[0] = "";
                        }
                      }
                      if(clientuserscontact['data'][j]['contactTypeId'] == 3){
                        clientuserscontact['data'][j]['value'] = this.encryptDecrypt.decryptString(clientuserscontact['data'][j]['value']);
                        temporaryContact[1] = clientuserscontact['data'][j];
                      }
                      else{
                        if (typeof temporaryContact[1] == 'undefined') {
                          temporaryContact[1] = "";
                        }
                      }
                    }
                  }
                  }

                  else{
                    // console.log("<<0",clientuserscontact['data']);
                    temporaryContact[0]     = "";
                    // temporaryContact[0]     = "";
                    temporaryContact[1]     = "";
                    // temporaryContact[1]     = "";
                  }
                  console.log('temporaryContact',temporaryContact);
                  // if (condition) {
                  //
                  // }

                  userShowDataTemp[i]['contactDetails'] = temporaryContact;
                  temporaryContact   = []
                  if (typeof userShowDataTemp[i]['userData'] == 'undefined') {
                    userShowDataTemp.splice(i,1)
                    i--;
                    count--;
                  }
                  count++;
                  if (count == userShowDataTemp.length) {


                    var index = this.userShowData.length;
                    // console.log("this.userShowData.length",this.userShowData.length);
                    // this.userShowData.push(data);
                    if (index == 0) {

                      this.userShowData = userShowDataTemp;
                      // this.ngxLoader.stop();
                    }
                    else if (index > 0) { //////// if ther are users allready in sho data (I think i did it for nothing)
                      for (let m = 0; m < userShowDataTemp.length; m++) {
                        this.userShowData[index] = userShowDataTemp[m];
                        index = index + 1;
                        // this.ngxLoader.stop();
                      }
                      // this.userShowData[] = data;
                    }
                    console.log("this.userShowData.length ---- if",this.userShowData);
                    // this.totalUSersCount = totalLengthOfValidRecord;
                    callback({
                      // recordsTotal    :  10,
                      // recordsFiltered :  10,

                      recordsTotal    :  this.totalUSersCount,
                      recordsFiltered :  this.totalUSersCount,

                      // recordsFiltered: resp['recordsFiltered'],
                      // recordsFiltered: 1000,
                      data: []
                    });
                    this.ngxLoader.stop();
                  }



                },error=>{
                  this.ngxLoader.stop();
                  this.notifier.getConfig().behaviour.autoHide = 3000;
                  this.notifier.notify( "error", "Error while user contacts" );
                })

                // this.popTable(userClientDetails);
                // }
                // else{
                //   userShowDataTemp.splice(i,1)
                //   i--;
                //   count--;
                //   console.log('this.userShowData',this.userShowData);
                // }

              }

            },error=>{
              this.ngxLoader.stop();
              this.notifier.getConfig().behaviour.autoHide = 3000;
              this.notifier.notify( "error", "Error while users" );
            })


          },error=>{
            this.ngxLoader.stop();
            this.notifier.getConfig().behaviour.autoHide = 3000;
            this.notifier.notify( "error", "Error while loading Users" );
          })



      },
      // columns  : [
      //   { data : 'SPD-002' },
      //   { data : 'firstName' },
      //   { data : 'lastName' },
      //   { data : 'username' },
      //   { data : 'phone' },
      //   { data : 'email' },
      //   { data : 'action' }
      // ]
      ///for search
    }

  }

  onCheckboxChange(e) {
    const delivery: FormArray = this.clientForm.get('delivery') as FormArray;
    // console.log('delivery',delivery);
    // console.log('e',e);
    // console.log('e.target.checked',e.target.checked);
    // console.log('e.target',e);
    this.tempDev = {};

    if (e.target.checked) {
      delivery.push(new FormControl(e.target.value));
      this.tempDev.deliveryMethodId = e.target.value;
      // console.log('e.target.value',e.target.value);
      console.log('tempDev',this.tempDev);
      this.tempDeliver.push(this.tempDev);
      var tem = e.target.value;
      for (let j = 0; j < this.tempDeliver.length; j++) {
        if(this.tempDeliver[j].deliveryMethodId === tem){
          for (let i = 0; i < this.deliverMethod.length; i++) {
            if (this.deliverMethod[i].deliveryMethodId == this.tempDeliver[j].deliveryMethodId) {
              if (this.deliverMethod[i].name == "Email") {
                // this.encryptionCodeRequired = false;

                this.clientForm.get('encryption').setValidators([Validators.required])
                this.clientForm.controls["encryption"].updateValueAndValidity();
              }
            }

          }

        }
        else{
          // console.log("not eqular");

        }

      }
    } else {
      let i: number = 0;
      delivery.controls.forEach((item: FormControl) => {
        if (item.value == e.target.value) {
          delivery.removeAt(i);
          for (let j = 0; j < this.tempDeliver.length; j++) {
            if(this.tempDeliver[j].deliveryMethodId == e.target.value){
              for (let i = 0; i < this.deliverMethod.length; i++) {
                if (this.deliverMethod[i].deliveryMethodId == this.tempDeliver[j].deliveryMethodId) {
                  if (this.deliverMethod[i].name == "Email") {


                    // this.encryptionCodeRequired = false;

                    this.clientForm.get('encryption').clearValidators()
                    this.clientForm.controls["encryption"].updateValueAndValidity();
                  }
                }

              }
              this.tempDeliver.splice(j, 1);
            }

          }
          // var start = parseInt(item.value) -1;
          // var end   = parseInt(e.target.value) -1;
          // this.tempDeliver.splice(start, 1);
          return;
        }
        i++;
      });
    }
    // console.log('delivery',delivery);

  }

  setUserName(){
    if (typeof this.LName == "undefined" || this.LName == null || this.LName == '') {
      return;
    }
    else if (typeof this.FName == "undefined" || this.FName == null || this.FName == '') {

    }
    else{
      let temp                       = this.FName.charAt(0)+this.LName
      this.UName                     = temp.toLowerCase();
      this.userNameReq.data.username = this.UName;
      this.userNameReq.header.userCode    = this.logedInUserRoles.userCode;
      this.userNameReq.header.partnerCode = this.logedInUserRoles.partnerCode;
      this.userNameReq.header.functionalityCode = "CLTA-UC";

      var url                        = environment.API_USER_ENDPOINT + 'useravailable';
      this.userService.userNameAvailability(url, this.userNameReq).subscribe(resp => {
        console.log('resp UserName',resp)
        if (resp['data'] == true) {
          this.inValidUsername   = false;
          this.validUsername     = true;
          $('#user_name').attr("disabled","ture");

          // enable submit button
        }else{
          // console.log('false');

          this.validUsername     = false;
          this.inValidUsername   = true;
          // $('#user_name').attr("disabled","false");
          $('#user_name').removeAttr("disabled");
        }
        // this.defaultRoles = resp.data;
      })
    }
  }

  checkUserName(){
    this.UName                     = this.UName.toLowerCase();
    this.userNameReq.data.username = this.UName;
    this.userNameReq.header.userCode    = this.logedInUserRoles.userCode;
    this.userNameReq.header.partnerCode = this.logedInUserRoles.partnerCode;
    this.userNameReq.header.functionalityCode = "CLTA-UC";
    var url                        = environment.API_USER_ENDPOINT + 'useravailable';
    this.userService.userNameAvailability(url, this.userNameReq).subscribe(resp => {
      console.log('resp UserName',resp)
      if (resp['data'] == true) {
        this.inValidUsername   = false;
        this.validUsername     = true
        // $('#user_name').attr("disabled","ture");
        // enable submit button
      }else{
        this.validUsername     = false;
        this.inValidUsername   = true;
        // $('#user_name').removeAttr("disabled");
      }
      // this.defaultRoles = resp.data;
    })

  }


  get f() { return this.clientForm.controls; }
  updateClient(status) {
    console.log('status',status);

    this.submitted                  = true;
    if (this.clientForm.invalid) {
      // alert('form invalid');
      // this.submitted                  = false;
      this.scrollToError();
      return;
    }
    if (status == 1) {
      ///// complete
      if (this.enableCompleteSuperCheck >0 && this.enableCompleteDefaultCheck>0) {
        if(this.updateRequestData.data.active == 2){
          this.updateRequestData.data.active = 2;
        }
        else{
          this.updateRequestData.data.active = 1;
        }



        // console.log("this.updateRequestData.data.active",this.updateRequestData.data.active);
      }
      else{
        // this.updateRequestData.data.active = 0;
        this.notifier.getConfig().behaviour.autoHide = false;
        this.notifier.notify( "error", "You must add one default attending physician and one super user to add the client" );
        this.submitted                  = false;
        return;
      }

    }
    else if(status == 3){
      ///// Draft
      this.updateRequestData.data.active = 3;

    }
    console.log('this.updateRequestData',this.updateRequestData);

    // console.log('clientForm',this.clientForm.value);
    var table                       = document.querySelector("table");
    var contactData                 = this.parseTable(table);
    console.log('contactData',contactData);

    this.saveClientData.otherData    = this.clientForm.value;
    this.saveClientData.contactInfo  = contactData;
    this.saveClientData.imagePayload = this.imagePayLoad;
    this.saveClientData.imageBase64  = this.imageBase64;
    // ///////console.log('this.saveData',this.saveClientData);
    // return;
    this.formatUpdateRequestData(status)
    ////// this.changeTabsCheck();
    ///////////// this.fileUploadCheck();

  }

  get f3() { return this.clientUserForm.controls; }
  submitClientUser(){
    this.ngxLoader.start();
    this.submitted2                    = true;
    if (this.clientUserForm.invalid) {
      // alert('form invalid');
      this.ngxLoader.stop();
      return;
    }
    // this.saveData.userData                      = this.clientUserForm.value;
    // this.requestData.data.user.userCode           = 0;
    // this.requestData.data.user.displayCode      = "AB94";
    this.requestData.data.user.firstName        = this.clientUserForm.value.clientUserFName;
    this.requestData.data.user.lastName         = this.clientUserForm.value.clientUserLName;
    this.requestData.data.user.username         = this.clientUserForm.value.clientUserUName;
    this.requestData.data.user.accessCode       = "AB94";
    this.requestData.data.user.postNominal      = this.selectedPostNominal;
    this.requestData.data.user.createdBy        = this.logedInUserRoles.userCode;
    this.requestData.data.createdBy             = this.logedInUserRoles.userCode;
    var x = this.requestData.data.user.phone.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
    this.requestData.data.user.phone = !x[2] ? x[1] : '(' + x[1] + ')' + x[2] + (x[3] ? '-' + x[3] : '');
    var myDate = new Date();
    var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy');
    var currentTimeStamp = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');


    // for (let index = 0; index < this.requestData.data.user.roles.length; index++) {
    //   this.requestData.data.user.roles[index]['createdBy'] = this.logedInUserRoles.userCode;
    //   this.requestData.data.user.roles[index]['createdTimestamp'] = currentTimeStamp;
    //
    // }
    this.requestData.header.userCode    = this.logedInUserRoles.userCode;
    // this.requestData.data.user.roles    = this.logedInUserRoles.userCode;
    this.requestData.header.partnerCode = this.logedInUserRoles.partnerCode;
    this.requestData.data.partnerCode   = this.logedInUserRoles.partnerCode;
    this.requestData.header.functionalityCode   = "USRA-AUE";
    var url                = environment.API_USER_ENDPOINT + 'save';
    console.log("this.userShowData",this.userShowData)
    // return;
    this.userService.saveUser(url,this.requestData).then(resp => {
      console.log("resp",resp);

      // this.allUsers.push(resp);
      // this.ngxLoader.stop();
      if (resp['result'].codeType == 'S') {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.ajax.reload();
        });
        this.notifier.getConfig().behaviour.autoHide = 3000;
        // this.notifier.notify( "success", "User saved Successfully USER API" );
        /////////////// Request To client API
        this.clientUserReq.data.userCode   = resp['data'].userCode;
        this.clientUserReq.data.clientId = this.selectedClientID;
        this.clientUserReq.header.partnerCode = this.logedInUserRoles.partnerCode;
        this.clientUserReq.header.userCode    = this.logedInUserRoles.userCode;
        this.clientUserReq.header.functionalityCode    = "USRA-AUE";
        url                         = environment.API_CLIENT_ENDPOINT + 'addclientuser';
        this.httpRequest.httpPostRequests(url,this.clientUserReq).then(response => {
          console.log("--08000resp Client User",response);

          // this.allUsers.push(resp);
          this.ngxLoader.stop();
          if (typeof response['data'] != "undefined") {
            this.notifier.getConfig().behaviour.autoHide = 3000;
            this.notifier.notify( "success", "User saved Successfully" );
            var temp = '';
            console.log('before',resp['data']);
            for (let i = 0; i < resp['data'].roles.length; i++) {
              temp = resp['data'].roles[i]['name']+', '+ temp;
            }
            resp['data'].userRoles  = temp;
            console.log("resp['data']",resp['data']);
            // console.log("resp['data']",resp['data']);
            var emptyClientUserHolder  = {
              active: 0,
              addedBy: 0,
              addedTimestamp: null,
              clientId: 0,
              contactDetails: [],
              defaultPhysician: 0,
              isPhysician: 0,
              practiseCode: "",
              superUser: 0,
              userData: {},
              userCode: 1,
              visibleInCases: 0
            }
            var temporaryContact = [];
            temporaryContact[0]     = "";
            temporaryContact[0]     = "";
            temporaryContact[1]     = "";
            temporaryContact[1]     = "";
            emptyClientUserHolder.userData       = resp["data"];
            emptyClientUserHolder.userCode         = resp["data"].userCode;
            emptyClientUserHolder.clientId       = this.selectedClientID;
            emptyClientUserHolder.contactDetails = temporaryContact;
            console.log('emptyClientUserHolder',emptyClientUserHolder);



            //////////////// if Selected Client is Active then////
            ////////////////Set newly aded user as activ
            if (this.updateRequestData.data.active == 1) {
              //////////////// if New Added/Loaded User is Attending Physician the then////
              ////////////////Check if its first AP if yes the make it default
              const foundRols = resp["data"].roles.some(el => el.name === 'Attending Physician');
              if (foundRols == true) {
                if(this.userShowData.length > 0){
                  const found = this.userShowData.some(el => el.isDefaultPhysician === 1);
                  console.log("FOUNDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD",found);

                  if(found == false){
                    var setActivereq = {
                      header:{
                        uuid:"",
                        partnerCode:"",
                        userCode:"",
                        referenceNumber:"",
                        systemCode:"",
                        moduleCode:"CLIM",
                        functionalityCode: "CLTA-UC",
                        systemHostAddress:"",
                        remoteUserAddress:"",
                        dateTime:""
                      },
                      data:{
                        clientId:1,
                        userCode:1,
                        superUser:"",
                        visibleInCases:"",
                        isPhysician:"",
                        defaultPhysician:"",
                        practiseCode:"",
                        addedBy:"",
                        addedTimestamp:"",
                        active:""
                      }
                    }
                    setActivereq.data.clientId            = this.selectedClientID;
                    setActivereq.data.userCode              = resp["data"].userCode;
                    setActivereq.data.defaultPhysician    = '1';
                    setActivereq.data.active              = '1';
                    setActivereq.header.partnerCode = this.logedInUserRoles.partnerCode;
                    setActivereq.header.userCode    = this.logedInUserRoles.userCode;
                    setActivereq.header.functionalityCode    = "USRA-UUE";
                    url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
                    this.httpRequest.httpPostRequests(url, setActivereq).then(respPhy => {
                      console.log('respPhy resp:', respPhy);
                      this.ngxLoader.stop();
                      if (respPhy['data'].defaultPhysician == 1) {
                        emptyClientUserHolder.defaultPhysician = 1;
                        emptyClientUserHolder.active = 1;
                        this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck +1;
                        if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck > 0) {
                          this.notifier.hideAll();
                        }
                      }
                      else{
                        emptyClientUserHolder.defaultPhysician = 0;
                        emptyClientUserHolder.active = 1;
                      }
                      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                        dtInstance.ajax.reload();
                        // Destroy the table first
                        // dtInstance.destroy();
                        // // Call the dtTrigger to rerender again
                        // this.dtTrigger.next();
                      });

                    })
                  }
                  else{
                    var setActivereq = {
                      header:{
                        uuid:"",
                        partnerCode:"",
                        userCode:"",
                        referenceNumber:"",
                        systemCode:"",
                        moduleCode:"CLIM",
                        functionalityCode: "CLTA-UC",
                        systemHostAddress:"",
                        remoteUserAddress:"",
                        dateTime:""
                      },
                      data:{
                        clientId:1,
                        userCode:1,
                        superUser:"",
                        visibleInCases:"",
                        isPhysician:"",
                        defaultPhysician:"",
                        practiseCode:"",
                        addedBy:"",
                        addedTimestamp:"",
                        active:""
                      }
                    }
                    setActivereq.data.clientId            = this.selectedClientID;
                    setActivereq.data.userCode              = resp["data"].userCode;
                    setActivereq.data.defaultPhysician    = '0';
                    setActivereq.data.active              = '1';
                    setActivereq.header.partnerCode = this.logedInUserRoles.partnerCode;
                    setActivereq.header.userCode    = this.logedInUserRoles.userCode;
                    setActivereq.header.functionalityCode    = "USRA-UUE";
                    url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
                    this.httpRequest.httpPostRequests(url, setActivereq).then(respPhy => {
                      console.log('respPhy resp:', respPhy);
                      this.ngxLoader.stop();
                      if (respPhy['data'].defaultPhysician == 1) {
                        emptyClientUserHolder.defaultPhysician = 1;
                        emptyClientUserHolder.active = 1;
                        this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck +1;
                        if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck > 0) {
                          this.notifier.hideAll();
                        }
                      }
                      else{
                        emptyClientUserHolder.defaultPhysician = 0;
                        emptyClientUserHolder.active = 1;
                      }
                      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                        dtInstance.ajax.reload();
                        // Destroy the table first
                        // dtInstance.destroy();
                        // // Call the dtTrigger to rerender again
                        // this.dtTrigger.next();
                      });

                    })
                  }
                  // for (let k = 0; k < this.userShowData.length; k++) {
                  //   this.userShowData[k]
                  //
                  // }
                }
                else{
                  console.log("no Data******");

                  var setActivereq = {
                    header:{
                      uuid:"",
                      partnerCode:"",
                      userCode:"",
                      referenceNumber:"",
                      systemCode:"",
                      moduleCode:"CLIM",
                      functionalityCode: "CLTA-UC",
                      systemHostAddress:"",
                      remoteUserAddress:"",
                      dateTime:""
                    },
                    data:{
                      clientId:1,
                      userCode:1,
                      superUser:"",
                      visibleInCases:"",
                      isPhysician:"",
                      defaultPhysician:"",
                      practiseCode:"",
                      addedBy:"",
                      addedTimestamp:"",
                      active:""
                    }
                  }
                  setActivereq.data.clientId            = this.selectedClientID;
                  setActivereq.data.userCode              = resp["data"].userCode;
                  setActivereq.data.active              = '1';
                  setActivereq.data.defaultPhysician    = '1';
                  setActivereq.header.partnerCode = this.logedInUserRoles.partnerCode;
                  setActivereq.header.userCode    = this.logedInUserRoles.userCode;
                  setActivereq.header.functionalityCode    = "USRA-UUE";
                  url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
                  this.httpRequest.httpPostRequests(url, setActivereq).then(respPhy => {
                    console.log('att phys resp resp:', respPhy);
                    this.ngxLoader.stop();
                    if (respPhy['data'].defaultPhysician == 1) {
                      emptyClientUserHolder.defaultPhysician = 1;
                      emptyClientUserHolder.active = 1;
                      this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck +1;
                      if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck > 0) {
                        this.notifier.hideAll();
                      }
                    }
                    else{
                      emptyClientUserHolder.defaultPhysician = 0;
                      emptyClientUserHolder.active = 1;
                    }
                    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                      dtInstance.ajax.reload();
                      // Destroy the table first
                      // dtInstance.destroy();
                      // // Call the dtTrigger to rerender again
                      // this.dtTrigger.next();
                    });

                  })
                }
              }

            }
            else{
              //////////////// if New Added/Loaded User is Attending Physician the then////
              ////////////////Check if its first AP if yes the make it default
              const foundRols = resp["data"].roles.some(el => el.name === 'Attending Physician');
              console.log("FOUNDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD active 0 out",foundRols);
              if (foundRols == true) {
                if(this.userShowData.length > 0){
                  const found = this.userShowData.some(el => el.isDefaultPhysician === 1);
                  // this.defaultAccountTypes[m].name.toLowerCase().indexOf('nursing')>-1
                  console.log("FOUNDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD active 0",found);

                  if(found == false){
                    var setActivereq = {
                      header:{
                        uuid:"",
                        partnerCode:"",
                        userCode:"",
                        referenceNumber:"",
                        systemCode:"",
                        moduleCode:"CLIM",
                        functionalityCode: "CLTA-UC",
                        systemHostAddress:"",
                        remoteUserAddress:"",
                        dateTime:""
                      },
                      data:{
                        clientId:1,
                        userCode:1,
                        superUser:"",
                        visibleInCases:"",
                        isPhysician:"",
                        defaultPhysician:"",
                        practiseCode:"",
                        addedBy:"",
                        addedTimestamp:"",
                        active:""
                      }
                    }
                    setActivereq.data.clientId            = this.selectedClientID;
                    setActivereq.data.userCode              = resp["data"].userCode;
                    setActivereq.data.defaultPhysician    = '1';
                    setActivereq.data.active              = '0';
                    setActivereq.header.partnerCode = this.logedInUserRoles.partnerCode;
                    setActivereq.header.userCode    = this.logedInUserRoles.userCode;
                    setActivereq.header.functionalityCode    = "USRA-UUE";
                    url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
                    this.httpRequest.httpPostRequests(url, setActivereq).then(respPhy => {
                      console.log('respPhy resp:', respPhy);
                      this.ngxLoader.stop();
                      if (respPhy['data'].defaultPhysician == 1) {
                        emptyClientUserHolder.defaultPhysician = 1;
                        emptyClientUserHolder.active = 0;
                        this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck +1;
                        if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck > 0) {
                          this.notifier.hideAll();
                        }
                      }
                      else{
                        emptyClientUserHolder.defaultPhysician = 0;
                        emptyClientUserHolder.active = 0;
                      }
                      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                        dtInstance.ajax.reload();
                        // Destroy the table first
                        // dtInstance.destroy();
                        // // Call the dtTrigger to rerender again
                        // this.dtTrigger.next();
                      });

                    })
                  }
                  else{
                    var setActivereq = {
                      header:{
                        uuid:"",
                        partnerCode:"",
                        userCode:"",
                        referenceNumber:"",
                        systemCode:"",
                        moduleCode:"CLIM",
                        functionalityCode: "CLTA-UC",
                        systemHostAddress:"",
                        remoteUserAddress:"",
                        dateTime:""
                      },
                      data:{
                        clientId:1,
                        userCode:1,
                        superUser:"",
                        visibleInCases:"",
                        isPhysician:"",
                        defaultPhysician:"",
                        practiseCode:"",
                        addedBy:"",
                        addedTimestamp:"",
                        active:""
                      }
                    }
                    setActivereq.data.clientId            = this.selectedClientID;
                    setActivereq.data.userCode              = resp["data"].userCode;
                    setActivereq.data.defaultPhysician    = '0';
                    setActivereq.data.active              = '0';
                    setActivereq.header.partnerCode = this.logedInUserRoles.partnerCode;
                    setActivereq.header.userCode    = this.logedInUserRoles.userCode;
                    setActivereq.header.functionalityCode    = "USRA-UUE";
                    url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
                    this.httpRequest.httpPostRequests(url, setActivereq).then(respPhy => {
                      console.log('respPhy resp:', respPhy);
                      this.ngxLoader.stop();
                      if (respPhy['data'].defaultPhysician == 1) {
                        emptyClientUserHolder.defaultPhysician = 1;
                        emptyClientUserHolder.active = 0;
                        this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck +1;
                        if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck > 0) {
                          this.notifier.hideAll();
                        }
                      }
                      else{
                        emptyClientUserHolder.defaultPhysician = 0;
                        emptyClientUserHolder.active = 0;
                      }

                    })
                    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                      dtInstance.ajax.reload();
                      // Destroy the table first
                      // dtInstance.destroy();
                      // // Call the dtTrigger to rerender again
                      // this.dtTrigger.next();
                    });
                  }
                  // for (let k = 0; k < this.userShowData.length; k++) {
                  //   this.userShowData[k]
                  //
                  // }
                }
                else{
                  console.log("no Data******");

                  var setActivereq = {
                    header:{
                      uuid:"",
                      partnerCode:"",
                      userCode:"",
                      referenceNumber:"",
                      systemCode:"",
                      moduleCode:"CLIM",
                      functionalityCode: "CLTA-UC",
                      systemHostAddress:"",
                      remoteUserAddress:"",
                      dateTime:""
                    },
                    data:{
                      clientId:1,
                      userCode:1,
                      superUser:"",
                      visibleInCases:"",
                      isPhysician:"",
                      defaultPhysician:"",
                      practiseCode:"",
                      addedBy:"",
                      addedTimestamp:"",
                      active:""
                    }
                  }
                  setActivereq.data.clientId            = this.selectedClientID;
                  setActivereq.data.userCode              = resp["data"].userCode;
                  setActivereq.data.active                = '0';
                  setActivereq.data.defaultPhysician      = '1';
                  setActivereq.header.partnerCode = this.logedInUserRoles.partnerCode;
                  setActivereq.header.userCode    = this.logedInUserRoles.userCode;
                  setActivereq.header.functionalityCode    = "USRA-UUE";
                  url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
                  this.httpRequest.httpPostRequests(url, setActivereq).then(respPhy => {
                    console.log('att phys resp resp:', respPhy);
                    this.ngxLoader.stop();
                    if (respPhy['data'].defaultPhysician == 1) {
                      emptyClientUserHolder.defaultPhysician = 1;
                      this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck +1;
                      if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck > 0) {
                        this.notifier.hideAll();
                      }
                      emptyClientUserHolder.active = 0;
                    }
                    else{
                      emptyClientUserHolder.defaultPhysician = 0;
                      emptyClientUserHolder.active = 0;
                    }
                    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                      dtInstance.ajax.reload();
                      // Destroy the table first
                      // dtInstance.destroy();
                      // // Call the dtTrigger to rerender again
                      // this.dtTrigger.next();
                    });

                  })
                }
              }

            }



            this.userShowData.push(emptyClientUserHolder);

            // this.userShowData.push(resp['data']);
            this.submitted2        = false;
            this.inValidUsernpi   = false;
            this.inValidUseremail = false;
            this.clientUserForm.reset();
            this.notifier.hideAll();
            // this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            //   dtInstance.ajax.reload();
            //   // Destroy the table first
            //   // dtInstance.destroy();
            //   // // Call the dtTrigger to rerender again
            //   // this.dtTrigger.next();
            // });


          }
          else{
            this.ngxLoader.stop();
            this.notifier.getConfig().behaviour.autoHide = 3000;
            this.notifier.notify( "error", "Error while saving user CLIENT API");
          }
        })
        // var temp = '';
        //   console.log('before',resp.data);
        // for (let i = 0; i < resp.data.roles.length; i++) {
        //     temp = resp.data.roles[i]['name']+', '+ temp;
        // }
        //
        //
        // resp.data.userRoles  = temp;
        // this.userShowData.push(resp.data);
        // this.submitted = false;
        // this.clientUserForm.reset();


      }
      else{
        this.ngxLoader.stop();
        this.notifier.getConfig().behaviour.autoHide = 3000;
        if (resp['result'].description.indexOf("[u_email]") > -1) {
          this.notifier.notify( "error", "Error while saving user Email Already Exist");
        }
        else{
          this.notifier.notify( "error", "Error while saving user");
        }
        // this.notifier.notify( "error", "Error while saving user USER API");


      }
    })
  }

  getSelectedUserData(userCode){
    localStorage.setItem("userFromEditCLient",JSON.stringify(this.updateRequestData.data.accountNumber));
    // console.log('userCode',userCode);
    this.ngxLoader.start();
    // var ciphertext = CryptoJS.AES.encrypt(userCode, 'nglis', { outputLength: 224 }).toString();
    var ax = CryptoJS.AES.encrypt(userCode, 'nglis', { outputLength: 224 }).toString();
    var bx = ax.replace('+','xsarm');
    var cx = bx.replace('/','ydan');
    var ciphertext = cx.replace('=','zhar');
    this.router.navigate(['edit-user', ciphertext]);
    // this.router.navigate(['edit-user', userCode]);

  }


  ///////// add row to contact table
  addFieldValue_update(index) {
    var object = {
      ""             : "",
      "ContactNo"    : "",
      "contactTypeId": "1",
      "ContactID"    : "",
      "Fax"          : "",
      "FaxID"        : "3",
      "ContactID1"    : "",
      "Email"        : "",
      "EmailID"      : "2",
      "ContactID2"    : "",
      "Default"      : ""
    }
    this.setCross    = true;
    this.tableSize   = this.tableSize+1;
    this.conactHolderUpdate.push(object)
    this.contactLength = this.conactHolderUpdate.length;
    console.log('here',this.conactHolderUpdate);
    console.log('contactLength',this.contactLength);

  }

  attTypeChanged(){

  }
  ///////// remove row from contact table
  deleteFieldValue_update(index) {
    if (index !== -1) {
      // console.log('if',index);
      this.conactHolderUpdate.splice(index+1, 1);
    }
    else{
      console.log('else',index);
    }

  }

  // onFileSelect(input) {
  //   console.log(input.files);
  //   if (input.files && input.files[0]) {
  //     var reader = new FileReader();
  //     reader.onload = (e: any) => {
  //       // console.log('Got here: ', e.target.result);
  //       this.imageBase64 = e.target.result;
  //       // this.obj.photoUrl = e.target.result;
  //     }
  //     reader.readAsDataURL(input.files[0]);
  //   }
  //   // console.log('this.imageBase64: ', this.imageBase64);
  //   if(input.files && input.files.length > 0) {
  //     this.imagePayLoad = input.files[0];
  //   }
  // }
  onFileChangeUpdateClient(input) {
    console.log(input.files);
    var mb = input.files[0].size/1048576;
    var sl = input.files[0].name.length
    console.log("sl",sl);
    // console.log("mb",mb);

    if (mb <= 1 && sl <= 40) {
      if (input.files && input.files[0]) {
        this.imagePayLoad = input.files[0];
        var reader = new FileReader();
        reader.onload = (e: any) => {
          // console.log('Got here: ', e.target.result);
          this.imageBase64 = e.target.result;
          // this.obj.photoUrl = e.target.result;
        }
        reader.readAsDataURL(input.files[0]);
      }
      // console.log('this.imageBase64: ', this.imageBase64);
      if(input.files && input.files.length > 0) {
        this.imagePayLoad = input.files[0];
        // this.imagePayLoad = input.files[0];
      }
    }
    else{
      if (mb > 1) {alert('File Size must be less then 1MB');}
      if (sl > 40) {alert('File Name Too Large');}

      $('#inputGroupFile01').val('');
    }
  }
  onAddItem(event){
    if(typeof event.formId =='undefined'){
      console.log('event',event);
      // var ln        = this.defaultReqType.length;
      // var newFormID = ln+1;
      // // if (ln>2) {
      // //   newFormID = ln+1;
      // // }
      // console.log("this.requestData.clientDetails.ClientRequisitionTypeDto",this.requestData.clientDetails.ClientRequisitionTypeDto);

      // this.newReqTemp.formId           =  newFormID;
      // this.newReqTemp.name             =  event.name;
      // this.newReqTemp.addedTimestamp   =  null;
      // this.newReqTemp.updatedBy        =  null;
      // this.newReqTemp.updatedTimestamp =  null;
      // console.log('this.newReqTemp',this.newReqTemp);
      //
      // this.defaultReqType.push(this.newReqTemp);
      // this.requestData.clientDetails.ClientRequisitionTypeDto.push(this.newReqTemp);

    }
    else{
      console.log("existss");

    }
    // console.log("this.defaultReqType",this.defaultReqType);
    // console.log("this.requestData.clientDetails.ClientRequisitionTypeDto",this.requestData.clientDetails.ClientRequisitionTypeDto);


  }

  populateUpdateForm(selectedClientID, callingEvent){

    this.contactFormat            = [];
    this.tempContact              = {};
    this.conactHolderUpdate       = [];
    this.contactFormat            = [];
    this.tempContact              = {};
    this.saveDataRequistionTemp   = {};
    this.saveDataRequistion       = [];
    this.contactFormat            = [];
    this.tempDeliver              = [];
    this.saveDataReqType          = [];
    this.saveDataClientType       = [];
    this.saveDataReqAttType       = [];
    this.tempContact              = {};
    this.tempDev                  = {};
    this.saveDataReqTypeTemp      = {};
    this.saveDataClientTypeTemp   = {};
    this.saveDataReqAttTypeTemp   = {};
    this.saveDataReqFIleType      = [];
    this.saveDataReqFIleTypeTemp  = {};
    this.imagePayLoad             = '';
    this.contactRowValid          = false;
    this.contactEmailValidity     = true;
    this.contactTablePhone;
    this.contactTableFax;
    this.disabledSalesRepFields   = true;
    this.encryptionCodeRequired   = false;


    var getCLurl    = environment.API_CLIENT_ENDPOINT + "clientidsnames";
    var reqTemplate = {
      header:{
        uuid               :"",
        partnerCode        :this.logedInUserRoles.partnerCode,
        userCode           :this.logedInUserRoles.userCode,
        referenceNumber    :"",
        systemCode         :"",
        moduleCode         :"CLIM",
        functionalityCode  :"CLTA-UC",
        systemHostAddress  :"",
        remoteUserAddress  :"",
        dateTime           :""
      },
      data:{
        clientIds:[this.updateRequestData.data.parentClientId]
      }
    }
    if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-UC") != -1) {
      reqTemplate.header.functionalityCode    = "CLTA-UC";
    }
    else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-VC") != -1){
      reqTemplate.header.functionalityCode    = "CLTA-VC";
    }
    // var reqTemplate = {clientIds:[resp['data'].clientId]}
    this.clientService.getClientByName(getCLurl,reqTemplate).subscribe(selectedClient => {
      console.log("selectedClient",selectedClient);
      if (selectedClient['data']['length'] != 0) {

        this.clientsDropdown    = selectedClient['data'];
        this.clientSearchString = selectedClient['data'][0].clientId;
        // this.ngxLoader.stop();
      }
      else{
        this.clientsDropdown    = [];
        this.clientSearchString = null;
        // this.notifier.notify('error','no client found');
        // this.ngxLoader.stop();
      }



    },
    error => {
      this.clientsDropdown    = [];
      this.clientSearchString = null;
      // this.notifier.notify( "error", "Error while fetching clients backend connect aborted" );
      return;
      // console.log("error",error);
    });


    /////////////////////////////////////
    ///// For Update FormData///////////
    //////////////////////////////////
    // var af ;
    // var al = 0;
    // // var a = "/root/1606464156660drop-down-arrow.png"
    // af = this.updateRequestData.data.logo.split('\\')
    // // af = af.split('\\')
    // al = af.length - 1;
    // if (al == 0) {
    //   af = af.split('/')
    //   al = af.length - 1;
    // }
    // var aname =  af[al];
    // console.log("af",af);
    // console.log("al",al);
    // console.log("aname",aname);
    // this.AssignedFileName = aname;
    // $('#inputGroupFile01').val(aname)
    this.AssignedFileName = this.updateRequestData.data.logo;

    this.clientActiveToggle           = this.updateRequestData.data.active
    this.updateAccountNumber          = this.updateRequestData.data.accountNumber;
    this.updateAccountNumberOLD       = this.updateRequestData.data.accountNumber;
    this.updateAccountName            = this.updateRequestData.data.name;
    this.updateAccountNotes           = this.updateRequestData.data.notes;
    // this.updateStreet                 = this.updateRequestData.data.AddressDetailsDto.street;
    this.updateStreet                 = this.encryptDecrypt.decryptString(this.updateRequestData.data.AddressDetailsDto.street);
    // if (this.updateRequestData.data.AddressDetailsDto.countryId > 4) {
    //   this.updateCountry                = 1;
    // }
    // else{
    //   this.updateCountry                = this.updateRequestData.data.AddressDetailsDto.countryId;
    // }
    this.updateCountry                = this.updateRequestData.data.AddressDetailsDto.countryId;
    this.changeCountry()
    // if (this.updateRequestData.data.AddressDetailsDto.stateId > 4) {
    //   this.updateState                = 1;
    // }
    // else{
    //   this.updateState                  = this.updateRequestData.data.AddressDetailsDto.stateId;
    // }
    this.updateState                  = this.updateRequestData.data.AddressDetailsDto.stateId;
    this.changeState()
    console.log("this.updateRequestData.data.AddressDetailsDto.city",this.updateRequestData.data.AddressDetailsDto.city);

    // if (this.updateRequestData.data.AddressDetailsDto.city > 4 ) {
    //   // this.updateCity                   =1;
    // }
    if (typeof this.updateRequestData.data.AddressDetailsDto.city =='string') {
      // this.updateCity  = 1
      this.updateRequestData.data.AddressDetailsDto.city = parseInt(this.updateRequestData.data.AddressDetailsDto.city);

    }
    this.updateCity = this.updateRequestData.data.AddressDetailsDto.city;
    // console.log("t---------------------",this.updateRequestData.data.AddressDetailsDto.city);
    // console.log("country---------------------",this.updateRequestData.data.AddressDetailsDto.countryId);
    // console.log("State---------------------",this.updateRequestData.data.AddressDetailsDto.stateId);

    // this.updateStreet                 = "test abc";
    // this.updatesuiteFloorBuilding     = "suite 1";
    // this.updatesuiteFloorBuilding     = this.updateRequestData.data.AddressDetailsDto.suiteFloorBuilding;
    this.updatesuiteFloorBuilding     = this.encryptDecrypt.decryptString(this.updateRequestData.data.AddressDetailsDto.suiteFloorBuilding);
    this.updateZip                    = this.updateRequestData.data.AddressDetailsDto.zip;
    // this.updateZip                    = "12356";
    this.updateSalesRepresentativeId  = this.updateRequestData.data.salesRepresentativeId;
    this.updateShowWordFile           = this.updateRequestData.data.showWordFile;
    this.updateEncryptionCode         = this.updateRequestData.data.encryptionCode;
    this.updateAllowedFilesize        = this.updateRequestData.data.allowedFilesize;

    ////////// Sales Representative Temporary Data
    this.updateSRFname                = '';
    this.updateSRLname                = '';
    this.updateSREmail                = '';
    this.updateSRContact              = '';

    ///////////////////////////////////////
    /////// For Update FormData///////////
    ////////////////////////////////////

    // this.UpdateFormClientTypeHolder = ctdto[0].clientTypeId;

    /////// setting client Type
    console.log("this.updateRequestData.data.ClientTypeDto",this.updateRequestData.data.ClientTypeDto);

    if (this.updateRequestData.data.ClientTypeDto['length'] == 0) {
      this.UpdateFormClientTypeHolder = 0;
    }
    else{
      this.UpdateFormClientTypeHolder = this.updateRequestData.data.ClientTypeDto[0].clientTypeId;
      // this.UpdateFormClientTypeHolder['clientId'] = this.updateRequestData.data.ClientTypeDto[0].clientId;
    }

    ////// setting requistion type
    let tempReq  = [];
    for (let i = 0; i < this.updateRequestData.data.ClientRequisitionTypeDto.length; i++) {
      tempReq.push(this.updateRequestData.data.ClientRequisitionTypeDto[i].formId);
    }
    this.updateRequisitionDTOTEMP   = tempReq;
    this.requisitionDTOTEMP   = tempReq;
    console.log("this.updateRequisitionDTOTEMP",this.updateRequisitionDTOTEMP);


    ////// setting attachment type
    let tempatt  = [];
    for (let i = 0; i < this.updateRequestData.data.AttachmentTypeDto.length; i++) {
      tempatt.push(this.updateRequestData.data.AttachmentTypeDto[i].attachmentTypeId);
    }
    this.UpdateFormAttachmentTypeHolder = tempatt;

    ////// setting allowed attachment type
    let tempalow = [];
    for (let i = 0; i < this.updateRequestData.data.AttachmentFileTypeDto.length; i++) {
      tempalow.push(this.updateRequestData.data.AttachmentFileTypeDto[i].attachmentFileTypeId);
    }
    this.UpdateFormAttachmentFileHolder = tempalow;

    ////// setting Dekivery Method
    for (let i = 0; i < this.deliverMethod.length; i++) {
      this.deliverMethod[i].checked = false;
    }

    /////// set value for form controll and DTO
    const delivery: FormArray = this.clientForm.get('delivery') as FormArray;
    this.tempDev     = {};
    this.tempDeliver = [];
    /////// show check box visibility and setting temporary holder that will be assigned to request data
    for (let i = 0; i < this.updateRequestData.data.DeliveryMethodDto.length; i++) {
      var d = String(this.updateRequestData.data.DeliveryMethodDto[i].deliveryMethodId);
      delivery.push(new FormControl(d));
      this.tempDev.deliveryMethodId = d;
      this.tempDeliver.push(this.tempDev);  /////// to assign to request Data
      this.tempDev     = {}
      for (let j = 0; j < this.deliverMethod.length; j++) {
        if (this.updateRequestData.data.DeliveryMethodDto[i].deliveryMethodId == this.deliverMethod[j].deliveryMethodId) {
          this.deliverMethod[j].checked = true;
          if (this.deliverMethod[j].name == "Email") {
            // this.encryptionCodeRequired = false;
            // console.log("MAIIIIIIIIL");
            this.clientForm.get('encryption').setValidators([Validators.required])
            this.clientForm.controls["encryption"].updateValueAndValidity();
          }
        }

      }
    }

    ///////////////// set contact table



    // var object = {
    //   ""             : "",
    //   "ContactNo"    : "",
    //   "Fax"          : "",
    //   "Email"        : "",
    //   "Default"      : ""
    // }
    var object = {
      ""             : "",
      "ContactNo"    : "",
      "contactTypeId": "",
      "ContactID"    : "",
      "Fax"          : "",
      "FaxID"        : "",
      "ContactID1"    : "",
      "Email"        : "",
      "EmailID"      : "",
      "ContactID2"    : "",
      "Default"      : "",
      "disableButton"      : 0
    }
    // this.conactHolder = object;
    console.log("this.conactHolderUpdate",this.conactHolderUpdate);
    // console.log("this.updateRequestData.data.ContactDto",this.updateRequestData.data.ContactDto);
    // console.log("testContactData",this.testContactData);
    this.updateRequestData.data.ContactDto.sort(function(a, b) {
      return parseFloat(a.contactId) - parseFloat(b.contactId);
    });
    console.log("this.updateRequestData.data.ContactDto",this.updateRequestData.data.ContactDto);

    var chunks = [],tempO={},arrayA=[], i = 0, n = this.updateRequestData.data.ContactDto.length;
    while (i<n) {
      chunks.push(this.updateRequestData.data.ContactDto.slice(i, i += 3));
    }
    console.log("chuncks",chunks);

    if (chunks.length) {

    }

    for (let j = 0; j < chunks.length; j++) {
      // console.log("in for");
      // console.log("chunks[j]",chunks[j]);
      // console.log("chunks[j][0]",chunks[j]['0']);
      // console.log("chunks[j][1]",chunks[j]['1']);
      // console.log("chunks[j][2]",chunks[j]['2']);

      // var c =0;
      if (typeof chunks[j]['0'] !="undefined") {
        if (chunks[j]['0']["contactTypeId"] == 1 ) {
          object["ContactNo"]     = this.encryptDecrypt.decryptString(chunks[j]['0'].value);
          object["contactTypeId"] = chunks[j]['0'].contactTypeId;
          object["ContactID"]     = chunks[j]['0'].contactId;
          object["Default"]       = chunks[j]['0'].defaultContact;


        }
        if (chunks[j]['0']["contactTypeId"] == 3) {
          object["Fax"]           = this.encryptDecrypt.decryptString(chunks[j]['0'].value);
          object["FaxID"]         = chunks[j]['0'].contactTypeId;
          object["ContactID1"]    = chunks[j]['0'].contactId;
          object["Default"]       = chunks[j]['0'].defaultContact;
        }
        if (chunks[j]['0']["contactTypeId"] == 2) {
          object["Email"]     = this.encryptDecrypt.decryptString(chunks[j]['0'].value);
          object["EmailID"]   = chunks[j]['0'].contactTypeId;
          object["ContactID2"]     = chunks[j]['0'].contactId;
          object["Default"]     = chunks[j]['0'].defaultContact;
        }
        // this.conactHolderUpdate.push(object)
        // object = {
        //   ""             : "",
        //   "ContactNo"    : "",
        //   "contactTypeId": "",
        //   "ContactID"    : "",
        //   "Fax"          : "",
        //   "FaxID"        : "",
        //   "ContactID1"    : "",
        //   "Email"        : "",
        //   "EmailID"      : "",
        //   "ContactID2"    : "",
        //   "Default"      : ""
        // }
      }
      if (typeof chunks[j]['1'] !="undefined") {

        if (typeof chunks[j]['1'] !="undefined" && chunks[j]['1']["contactTypeId"] ==1 ) {
          object["ContactNo"] = this.encryptDecrypt.decryptString(chunks[j]['1'].value);
          object["contactTypeId"] = chunks[j]['1'].contactTypeId;
          object["ContactID"]     = chunks[j]['1'].contactId;
          object["Default"]     = chunks[j]['1'].defaultContact;
        }
        if (typeof chunks[j]['1'] !="undefined" && chunks[j]['1']["contactTypeId"] ==3) {
          object["Fax"]       = this.encryptDecrypt.decryptString(chunks[j]['1'].value);
          object["FaxID"]     = chunks[j]['1'].contactTypeId;
          object["ContactID1"]     = chunks[j]['1'].contactId;
          object["Default"]     = chunks[j]['1'].defaultContact;
        }
        if (typeof chunks[j]['1'] !="undefined"&& chunks[j]['1']["contactTypeId"] ==2) {
          object["Email"]     = this.encryptDecrypt.decryptString(chunks[j]['1'].value);
          object["EmailID"]   = chunks[j]['1'].contactTypeId;
          object["ContactID2"]     = chunks[j]['1'].contactId;
          object["Default"]     = chunks[j]['1'].defaultContact;
        }
        // this.conactHolderUpdate.push(object)
        // object = {
        //   ""             : "",
        //   "ContactNo"    : "",
        //   "contactTypeId": "",
        //   "ContactID"    : "",
        //   "Fax"          : "",
        //   "FaxID"        : "",
        //   "ContactID1"    : "",
        //   "Email"        : "",
        //   "EmailID"      : "",
        //   "ContactID2"    : "",
        //   "Default"      : ""
        // }
      }
      if (typeof chunks[j]['2'] !="undefined") {

        if (typeof chunks[j]['2'] !="undefined" && chunks[j]['2']["contactTypeId"] ==1 ) {
          object["ContactNo"] = this.encryptDecrypt.decryptString(chunks[j]['2'].value);
          object["contactTypeId"] = chunks[j]['2'].contactTypeId;
          object["ContactID"]     = chunks[j]['2'].contactId;
          object["Default"]     = chunks[j]['2'].defaultContact;
        }
        if (typeof chunks[j]['2'] !="undefined" && chunks[j]['2']["contactTypeId"] ==3) {
          object["Fax"]       = this.encryptDecrypt.decryptString(chunks[j]['2'].value);
          object["FaxID"]     = chunks[j]['2'].contactTypeId;
          object["ContactID1"]     = chunks[j]['2'].contactId;
          object["Default"]     = chunks[j]['2'].defaultContact;
        }
        if (typeof chunks[j]['2'] !="undefined"&& chunks[j]['2']["contactTypeId"] ==2) {
          object["Email"]     = this.encryptDecrypt.decryptString(chunks[j]['2'].value);
          object["EmailID"]   = chunks[j]['2'].contactTypeId;
          object["ContactID2"]     = chunks[j]['2'].contactId;
          object["Default"]     = chunks[j]['2'].defaultContact;
        }
        // this.conactHolderUpdate.push(object)
        // object = {
        //   ""             : "",
        //   "ContactNo"    : "",
        //   "contactTypeId": "",
        //   "ContactID"    : "",
        //   "Fax"          : "",
        //   "FaxID"        : "",
        //   "ContactID1"    : "",
        //   "Email"        : "",
        //   "EmailID"      : "",
        //   "ContactID2"    : "",
        //   "Default"      : ""
        // }
      }
      this.conactHolderUpdate.push(object)
      object = {
        ""             : "",
        "ContactNo"    : "",
        "contactTypeId": "",
        "ContactID"    : "",
        "Fax"          : "",
        "FaxID"        : "",
        "ContactID1"    : "",
        "Email"        : "",
        "EmailID"      : "",
        "ContactID2"    : "",
        "Default"      : "",
        "disableButton": 0
      }






      // object["ContactNo"] = chunks[j]['0'].value;
      // object["contactTypeId"] = chunks[j]['0'].contactTypeId;
      // object["Fax"]       = chunks[j]['1'].value;
      // object["FaxID"]     = chunks[j]['1'].contactTypeId;
      // object["Email"]     = chunks[j]['2'].value;
      // object["EmailID"]   = chunks[j]['2'].contactTypeId;

      // if (typeof chunks[j]['0'] !="undefined") {
      //   console.log("chunks[j]['0']",chunks[j]['0']);
      //
      //   object["ContactNo"] = chunks[j]['0'].value;
      //   object["contactTypeId"] = chunks[j]['0'].contactTypeId;
      // }
      // else{
      //   object["ContactNo"] = "";
      //   object["contactTypeId"] = "1";
      // }
      // if (typeof chunks[j]['1'] !="undefined") {
      //   object["Fax"]       = chunks[j]['1'].value;
      //   object["FaxID"]     = chunks[j]['1'].contactTypeId;
      // }
      // else if(chunks[j]['1'] !="undefined" && chunks[j].length> 1){
      //   object["ContactNo"] = "";
      //   object["contactTypeId"] = 2;
      // }
      // if (typeof chunks[j]['2'] !="undefined") {
      //   object["Email"]     = chunks[j]['2'].value;
      //   object["EmailID"]   = chunks[j]['2'].contactTypeId;
      // }
      // else if(chunks[j]['1'] !="undefined" && chunks[j].length> 2){
      //   object["ContactNo"] = "";
      //   object["contactTypeId"] = 3;
      // }


      // console.log('object IN',object);

      // this.conactHolder.push(object);
    }
    if (this.updateRequestData.data.ContactDto.length == 0) {
      object = {
        ""             : "",
        "ContactNo"    : "",
        "contactTypeId": "1",
        "ContactID"    : "",
        "Fax"          : "",
        "FaxID"        : "3",
        "ContactID1"    : "",
        "Email"        : "",
        "EmailID"      : "2",
        "ContactID2"    : "",
        "Default"      : "",
        "disableButton": 0

      }
      this.conactHolderUpdate.push(object)
      this.contactRowValid  = true;
    }
    console.log("this.conactHolderUpdate",this.conactHolderUpdate);
    this.contactLength = this.conactHolderUpdate.length;
    this.tableSize        = this.conactHolderUpdate.length;
    if (this.conactHolderUpdate.length > 1) {
      this.setCross       = true;
      this.contactRowValid  = true;
    }
    if (this.conactHolderUpdate.length == 1) {
      this.contactRowValid  = true;
    }


    //
    // }
    // else{
    //
    // }

    // })
    // console.log('populate Form',this.updateRequestData);
    if (typeof this.updateSalesRepresentativeId == "undefined" || this.updateSalesRepresentativeId == "") {
      this.disabledSalesRepFields = false;
      // console.log("in");
    }
    else{
      this.disabledSalesRepFields = true;
    }



    if (callingEvent == "onLoad") {

      this.getAllUsers(0)
    }
    else if(callingEvent == "onUpdate"){
      this.getAllUsers(1)
    }


    // else if((this.updateRequestData.data.active == 0 || this.updateRequestData.data.active == 2) && callingEvent == "onUpdate"){
    //   this.getAllUsers(1)
    // }
    // else{
    //   this.getAllUsers(0)
    // }

    // this.ngxLoader.stop();

  }

  ///////////////////////////////Formate Data for Update CLient
  ///////////////////// format data acc to request template
  formatUpdateRequestData(status) {
    this.ngxLoader.start();
    this.contactFormat            = [];
    this.tempContact              = {};
    this.contactFormat            = [];
    this.tempContact              = {};
    this.saveDataRequistionTemp   = {};
    this.saveDataRequistion       = [];
    this.contactFormat            = [];
    // this.tempDeliver              = [];
    this.saveDataReqType          = [];
    this.saveDataClientType       = [];
    this.saveDataReqAttType       = [];
    this.tempContact              = {};
    this.tempDev                  = {};
    this.saveDataReqTypeTemp      = {};
    this.saveDataClientTypeTemp   = {};
    this.saveDataReqAttTypeTemp   = {};
    this.saveDataReqFIleType      = [];
    this.saveDataReqFIleTypeTemp  = {};

    var table                       = document.querySelector("table");
    var contactData                 = this.parseTableUpdate(table);
    console.log('contactData',contactData);

    //////// attachment Type DTO
    // console.log("this.UpdateFormAttachmentTypeHolder",this.UpdateFormAttachmentTypeHolder);

    this.saveDataReqAttTypeTemp = {}
    this.saveDataReqAttType     = []
    for (let j = 0; j < this.UpdateFormAttachmentTypeHolder.length; j++) {
      this.saveDataReqAttTypeTemp.attachmentTypeId = this.UpdateFormAttachmentTypeHolder[j]
      this.saveDataReqAttType.push(this.saveDataReqAttTypeTemp);

      this.saveDataReqAttTypeTemp = {}
    }
    this.updateRequestData.data.AttachmentTypeDto       = this.saveDataReqAttType;
    // console.log("attachment type",this.updateRequestData.data.AttachmentTypeDto);


    // ///////// allowed file type
    // console.log("this.UpdateFormAttachmentFileHolder",this.UpdateFormAttachmentFileHolder);
    this.saveDataReqFIleTypeTemp = {}
    this.saveDataReqFIleType     = [];
    for (let j = 0; j < this.UpdateFormAttachmentFileHolder.length; j++) {
      this.saveDataReqFIleTypeTemp.attachmentFileTypeId = this.UpdateFormAttachmentFileHolder[j]
      this.saveDataReqFIleType.push(this.saveDataReqFIleTypeTemp);
      this.saveDataReqFIleTypeTemp = {}
    }
    this.updateRequestData.data.AttachmentFileTypeDto       = this.saveDataReqFIleType;
    // console.log("allowed file",this.updateRequestData.data.AttachmentFileTypeDto);

    // ///////// Requisition Tye
    // console.log("this.updateRequisitionDTOTEMP",this.updateRequisitionDTOTEMP);
    this.saveDataRequistionTemp = {}
    this.saveDataRequistion     = []
    for (let j = 0; j < this.updateRequisitionDTOTEMP.length; j++) {
      this.saveDataRequistionTemp.formId           = this.updateRequisitionDTOTEMP[j]
      this.saveDataRequistionTemp.addedBy          = this.logedInUserRoles.userCode;
      this.saveDataRequistionTemp.addedTimestamp   = null
      this.saveDataRequistionTemp.updatedBy        = this.logedInUserRoles.userCode;
      this.saveDataRequistionTemp.updatedTimestamp = null
      this.saveDataRequistion.push(this.saveDataRequistionTemp);
      this.saveDataRequistionTemp = {}
    }
    this.updateRequestData.data.ClientRequisitionTypeDto       = this.saveDataRequistion;
    // console.log("requsition",this.updateRequestData.data.ClientRequisitionTypeDto);

    this.contactFormat = [];
    var df             = [];
    for (let l = 0; l < contactData.length; l++) {
      this.tempContact = {};
      df           = contactData[l].Default.split(",")
      ///// for cell
      if (contactData[l].ContactNo != "") {
        var splitted                      = contactData[l].ContactNo.split(",");
        if (splitted[0] != "") {
          if (splitted[1] == "") {
            splitted[1] = "1"
          }

          this.tempContact.contactId        = splitted[2]
          this.tempContact.contactTypeId    = splitted[1];
          this.tempContact.value            = this.encryptDecrypt.encryptString(splitted[0]);
          // var df                            = contactData[l].Default.split(",")
          if (df[0] == "true") {
            contactData[l].Default = 1;
          }
          else{
            contactData[l].Default = 0
          }
          this.tempContact.createdBy        = this.logedInUserRoles.userCode;;
          this.tempContact.createdTimestamp = null;
          this.tempContact.updatedBy        = null;
          this.tempContact.updatedTimestamp = null;
          this.tempContact.moduleCode       = "CLTM";
          this.tempContact.defaultContact   = contactData[l].Default;
          this.contactFormat.push(this.tempContact);
        }

      }
      this.tempContact = {};

      ///// for Fax
      if (contactData[l].Fax != "") {
        var splitted                      = contactData[l].Fax.split(",");
        if (splitted[0] != "") {
          if (splitted[1] == "") {
            splitted[1] = "3"
          }

          this.tempContact.contactId        = splitted[2];
          this.tempContact.contactTypeId    = splitted[1];
          this.tempContact.value            = this.encryptDecrypt.encryptString(splitted[0]);
          // var df                            = contactData[l].Default.split(",")
          if (df[0] == "true") {
            contactData[l].Default = 1;
          }
          else{
            contactData[l].Default = 0
          }
          this.tempContact.createdBy        = this.logedInUserRoles.userCode;;
          this.tempContact.createdTimestamp = null;
          this.tempContact.updatedBy        = null;
          this.tempContact.updatedTimestamp = null;
          this.tempContact.moduleCode       = "CLTM";
          this.tempContact.defaultContact   = contactData[l].Default;
          this.contactFormat.push(this.tempContact);
        }

      }
      this.tempContact = {};

      // ///// for Email
      if (contactData[l].Email != "") {
        var splitted                      = contactData[l].Email.split(",");
        if (splitted[0] != "") {
          // console.log("email split",contactData[l].Email);
          if (splitted[1] == "") {
            splitted[1] = "2"
          }

          this.tempContact.contactId        = splitted[2];
          this.tempContact.contactTypeId    = splitted[1];
          this.tempContact.value            = this.encryptDecrypt.encryptString(splitted[0]);
          // var df                            = contactData[l].Default.split(",")
          if (df[0] == "true") {
            contactData[l].Default = 1;
          }
          else{
            contactData[l].Default = 0
          }
          this.tempContact.createdBy        = this.logedInUserRoles.userCode;;
          this.tempContact.createdTimestamp = null;
          this.tempContact.updatedBy        = null;
          this.tempContact.updatedTimestamp = null;
          this.tempContact.moduleCode       = "CLTM";
          this.tempContact.defaultContact   = contactData[l].Default;
          this.contactFormat.push(this.tempContact);
        }

      }


    }
    this.updateRequestData.data.ContactDto       = this.contactFormat;
    console.log("this.updateRequestData.data.ContactDto",this.updateRequestData.data.ContactDto);
    // this.ngxLoader.stop(); return;
    this.updateRequestData.data.DeliveryMethodDto       = this.tempDeliver;
    // console.log("this.tempDeliver",this.tempDeliver);
    // console.log("this.deliverMethod",this.deliverMethod);

    //// Check if delivery method is email the set encription code requires
    // for (let i = 0; i < this.tempDeliver.length; i++) {
    //   for (let n = 0; n < this.deliverMethod.length; n++) {
    //     // console.log("this.deliverMethod",this.deliverMethod[n]);
    //     if (this.deliverMethod[n].name == 'Email') {
    //       if (this.tempDeliver[i].deliveryMethodId == this.deliverMethod[n].deliveryMethodId) {
    //         this.encryptionCodeRequired = true;
    //       }
    //     }
    //   }
    // }
    // if (this.updateEncryptionCode == "" && this.encryptionCodeRequired == true) {
    //   alert('Encryption Code is required as you have selected Email as delivery method')
    //   this.ngxLoader.stop();
    //   return
    // }
    // console.log("this.updateRequestData.data.DeliveryMethodDto",this.updateRequestData.data.DeliveryMethodDto);

    this.saveDataClientTypeTemp = {};
    this.saveDataClientType     = [];
    this.saveDataClientTypeTemp.clientTypeId = this.UpdateFormClientTypeHolder
    this.saveDataClientTypeTemp.clientId = this.updateRequestData.data.clientId;
    this.saveDataClientType.push(this.saveDataClientTypeTemp);
    this.saveDataClientTypeTemp = {}
    this.updateRequestData.data.ClientTypeDto       = this.saveDataClientType;
    // console.log("this.updateRequestData.data.ClientTypeDto ",this.updateRequestData.data.ClientTypeDto );

    this.updateRequestData.data.accountNumber            = this.updateAccountNumber;
    this.updateRequestData.data.name                     = this.updateAccountName;
    this.updateRequestData.data.notes                    = this.updateAccountNotes;
    this.updateRequestData.data.AddressDetailsDto.street = this.encryptDecrypt.encryptString(this.updateStreet);
    this.updateRequestData.data.AddressDetailsDto.countryId = this.updateCountry;
    this.updateRequestData.data.AddressDetailsDto.stateId   = this.updateState;
    this.updateRequestData.data.AddressDetailsDto.city      = this.updateCity;

    this.updateRequestData.data.AddressDetailsDto.suiteFloorBuilding = this.encryptDecrypt.encryptString(this.updatesuiteFloorBuilding);
    this.updateRequestData.data.AddressDetailsDto.zip                = this.updateZip;

    this.updateRequestData.data.salesRepresentativeId                = this.updateSalesRepresentativeId;
    // this.updateRequestData.data.showWordFile                         = this.updateShowWordFile;
    if (this.updateShowWordFile == true) {
      this.updateRequestData.data.showWordFile                         = 1;
    }
    else if (this.updateShowWordFile == false) {
      this.updateRequestData.data.showWordFile                         = 0;
    }
    else{
      this.updateRequestData.data.showWordFile                         = 0;
    }
    this.updateRequestData.data.encryptionCode                       = this.updateEncryptionCode;
    this.updateRequestData.data.allowedFilesize                      = this.updateAllowedFilesize;

    // console.log("befor",this.updateRequestData.data.active);

    if (this.updateRequestData.data.active == 0 ) {
      this.updateRequestData.data.active                             = 0;
    }
    // else{
    //   console.log("this.clientActiveToggle",this.clientActiveToggle);
    //
    //   if (typeof this.clientActiveToggle != "undefined") {
    //     this.updateRequestData.data.active                             = this.clientActiveToggle;
    //   }
    // }
    // console.log("this.updateRequestData.data.active",this.updateRequestData.data.active);
    // return;
    this.updateRequestData.data.clientId    = this.selectedClientID;
    this.updateRequestData.data.createdBy   = this.logedInUserRoles.userCode;;
    this.updateRequestData.header.partnerCode = this.logedInUserRoles.partnerCode;
    this.updateRequestData.header.userCode    = this.logedInUserRoles.userCode;
    this.updateRequestData.header.functionalityCode    = "CLTA-UC";


    ///formdata for file
    let formRequestData = new FormData();
    console.log('has this.imagePayLoad',this.imagePayLoad);
    if (this.imagePayLoad == '' || this.imagePayLoad == null) {
      // this.imagePayLoad = $('#inputGroupFile01')[0].files;
      // this.imagePayLoad = [];
      var f = new File([""], "");
      console.log("FILE",f);
      this.imagePayLoad = f;
      // this.imagePayLoad = '';
    }
    else {
      if (this.updateRequestData.data.logo != "") {
        this.updateRequestData.data.logo = "";
      }

    }
    formRequestData.append('file', this.imagePayLoad);
    var reqString = JSON.stringify(this.updateRequestData);
    formRequestData.append('clientDetails', reqString);
    console.log('this.updateRequestData',this.updateRequestData);
    // this.ngxLoader.stop(); return;
    var url                = environment.API_CLIENT_ENDPOINT + 'updateclient';
    /////// this.httpRequest.httpPostRequests(url,this.requestData).then(resp => {
    this.httpRequest.httpPostRequests(url,formRequestData).then(resp => {
      console.log("++++ Update resp",resp);
      this.submitted  = false;
      this.submitted2 = false;
      this.ngxLoader.stop();
      // this.allUsers.push(resp);
      // this.ngxLoader.stop();
      if (typeof resp['data'] != 'undefined') {
        if (resp['result']['codeType'] == 'E') {
          this.notifier.getConfig().behaviour.autoHide = 3000;
          this.notifier.notify( "error", "Error while updating client" );
          this.ngxLoader.stop();
        }
        else{
          console.log('record added succesfully add response divs');
          this.notifier.getConfig().behaviour.autoHide = 3000;
          this.notifier.notify( "success", "Client updated Successfully" );
          this.ngxLoader.stop();
          this.router.navigate(['all-clients']);
        }


      }
      else{
        this.ngxLoader.stop();
        this.notifier.getConfig().behaviour.autoHide = 3000;
        this.notifier.notify( "error", "Error While Updating Client" );
        // this.swalStyle.fire({
        //   icon: 'error',
        //   type: 'error',
        //   title: 'Error While Saving Client',
        //   showConfirmButton: true
        // })

      }

    })
    this.contactFormat            = [];
    this.tempContact              = {};
    this.contactFormat            = [];
    this.tempContact              = {};
    this.saveDataRequistionTemp   = {};
    this.saveDataRequistion       = [];
    this.contactFormat            = [];
    this.tempDeliver              = [];
    this.saveDataReqType          = [];
    this.saveDataClientType       = [];
    this.saveDataReqAttType       = [];
    this.tempContact              = {};
    this.tempDev                  = {};
    this.saveDataReqTypeTemp      = {};
    this.saveDataClientTypeTemp   = {};
    this.saveDataReqAttTypeTemp   = {};
    this.saveDataReqFIleType      = [];
    this.saveDataReqFIleTypeTemp  = {};
  }

  addClientProvidedEmail(userCode, e){
    // console.log('clientProvidedEmail',this.clientProvidedEmail);
    // console.log('capmail',e.target.value);

    console.log('cu-getAttribute("data-value")',e.target.getAttribute("cu-contactId"));
    // console.log('userCode',userCode);
    $('#email-spiner-'+userCode).css("display","inline-block");
    var regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    var vemail = regexp.test(e.target.value);
    if (vemail) {
      this.validProvidedEmail     = true;
      $('#email-spiner-'+userCode).css("display","inline-block");
      $('#email-error-'+userCode).css("display","none");
      // console.log("true");
    }
    else{
      // console.log("false");
      // alert("invalid Email");
      this.validProvidedEmail     = false;
      $('#email-spiner-'+userCode).css("display","none");
      $('#email-error-'+userCode).css("display","inline-block");
      return
    }
    if (e.target.getAttribute("cu-contactId") == null || typeof e.target.getAttribute("cu-contactId") == "undefined") {
      // this.updateclientusercontact.data.contactId   = '';
      this.updateclientusercontact.data.contactId   = null;
    }
    else{
      this.updateclientusercontact.data.contactId   = parseInt(e.target.getAttribute("cu-contactId"));
    }
    this.updateclientusercontact.data.value         = this.encryptDecrypt.encryptString(e.target.value);
    this.updateclientusercontact.data.contactTypeId = 2;
    this.updateclientusercontact.data.clientId      = this.selectedClientID;
    this.updateclientusercontact.data.userCode        = userCode;
    this.updateclientusercontact.data.createdBy        = this.logedInUserRoles.userCode;
    this.updateclientusercontact.header.partnerCode = this.logedInUserRoles.partnerCode;
    this.updateclientusercontact.header.userCode    = this.logedInUserRoles.userCode;
    this.updateclientusercontact.header.functionalityCode    = "CLTA-UC";

    console.log("updateclientusercontact",this.updateclientusercontact);


    var url                = environment.API_CLIENT_ENDPOINT + 'updateclientusercontact';
    this.httpRequest.httpPostRequests(url, this.updateclientusercontact).then(resp => {
      console.log('Client Provided Email',resp);
      if (resp['data'].contactId != null || resp['data'].contactId != '') {
        e.target.setAttribute("cu-contactId",resp['data'].contactId)
      }
      $('#email-spiner-'+userCode).css("display","none");
    })

  }
  addClientProvidedFax(userCode, e){
    // console.log('clientProvidedFax',this.clientProvidedFax);
    // console.log('cpfax',e.target.value);
    // console.log('userCode',userCode);
    $('#fax-spiner-'+userCode).css("display","inline-block");
    if (e.target.getAttribute("cu-contactId") == null || typeof e.target.getAttribute("cu-contactId") == "undefined") {
      // this.updateclientusercontact.data.contactId   = '';
      this.updateclientusercontact.data.contactId   = null;
    }
    else{
      this.updateclientusercontact.data.contactId   = parseInt(e.target.getAttribute("cu-contactId"));
    }
    this.updateclientusercontact.data.value         = this.encryptDecrypt.encryptString(e.target.value);
    this.updateclientusercontact.data.contactTypeId = 3;
    this.updateclientusercontact.data.clientId      = this.selectedClientID;
    this.updateclientusercontact.data.userCode        = userCode;
    this.updateclientusercontact.data.createdBy        = this.logedInUserRoles.userCode;
    // console.log("$('#email-fax-'+userCode)",$('#email-fax-'+user ID));
    this.updateclientusercontact.header.partnerCode = this.logedInUserRoles.partnerCode;
    this.updateclientusercontact.header.userCode    = this.logedInUserRoles.userCode;

    console.log("updateclientusercontact",this.updateclientusercontact);


    var url                = environment.API_CLIENT_ENDPOINT + 'updateclientusercontact';
    this.httpRequest.httpPostRequests(url, this.updateclientusercontact).then(resp => {
      console.log('Client Provided Fax',resp);
      if (resp['data'].contactId != null || resp['data'].contactId != '') {
        e.target.setAttribute("cu-contactId", resp['data'].contactId)
      }
      $('#fax-spiner-'+userCode).css("display","none");
    })

  }
  addClientProvidedPracticeCode(userCode, e, userIndex){
    var req = {
      header:{
        uuid:"",
        partnerCode:"",
        userCode:"",
        referenceNumber:"",
        systemCode:"",
        moduleCode:"CLIM",
        functionalityCode: "CLTA-UC",
        systemHostAddress:"",
        remoteUserAddress:"",
        dateTime:""
      },
      data:{
        clientId:1,
        userCode:1,
        superUser:"",
        visibleInCases:"",
        isPhysician:"",
        defaultPhysician:"",
        practiseCode:"",
        addedBy:1,
        addedTimestamp:"",
        active:""
      }
    }
    // console.log('clientProvidedPracticeCode',this.clientProvidedPCode);
    console.log('cpcode',e.target.value);
    // console.log('userCode',userCode);

    $('#practice-spiner-'+userCode).css("display","inline-block");
    req.data.visibleInCases   = this.userShowData[userIndex].isVisibleInCases;
    req.data.superUser        = this.userShowData[userIndex].isSuperUser;
    req.data.active           = this.userShowData[userIndex].active;
    req.data.defaultPhysician = this.userShowData[userIndex].isDefaultPhysician;

    req.data.practiseCode          = e.target.value;
    // req.data.contactTypeId        = 2;
    req.data.clientId              = this.selectedClientID;
    req.data.userCode                = userCode;
    req.header.partnerCode = this.logedInUserRoles.partnerCode;
    req.header.userCode    = this.logedInUserRoles.userCode;
    req.header.functionalityCode    = "USRA-UUE";
    // console.log("$('#email-fax-'+userCode)",$('#email-fax-'+userCode));
    console.log("req",req);

    var url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
    this.httpRequest.httpPostRequests(url, req).then(resp => {
      console.log('Client Provided pRACTICE',resp);
      $('#practice-spiner-'+userCode).css("display","none");
      if (resp['data'].practiseCode == 1 || resp['data'].practiseCode == "1") {
        console.log('practice success');
      }

    })

  }
  addClientProvidedDefault(userCode, e, userIndex){
    var req = {
      header:{
        uuid:"",
        partnerCode:"",
        userCode:"",
        referenceNumber:"",
        systemCode:"",
        moduleCode:"CLIM",
        functionalityCode: "CLTA-UC",
        systemHostAddress:"",
        remoteUserAddress:"",
        dateTime:""
      },
      data:{
        clientId:1,
        userCode:1,
        superUser:"",
        visibleInCases:"",
        isPhysician:"",
        defaultPhysician:"",
        practiseCode:"",
        addedBy:1,
        addedTimestamp:"",
        active:"",
        isClientPortalAccess:""
      }
    }
    // console.log('clientProvidedDefault',this.clientProvidedDefault);
    $('#default-spiner-'+userCode).css("display","inline-block");
    console.log('cpdefault',e.target.value);
    console.log('userCode',userCode);
    if (e.target.checked) {
      req.data.defaultPhysician = "1"
    }
    else{
      req.data.defaultPhysician = "0"
    }
    req.data.userCode   = userCode;
    req.data.clientId = this.selectedClientID;

    req.data.visibleInCases   = this.userShowData[userIndex].isVisibleInCases;
    req.data.superUser        = this.userShowData[userIndex].isSuperUser;
    req.data.practiseCode     = this.userShowData[userIndex].practiseCode;
    req.data.active           = this.userShowData[userIndex].active;
    req.data.isClientPortalAccess = this.userShowData[userIndex].isClientPortalAccess;
    req.header.partnerCode = this.logedInUserRoles.partnerCode;
    req.header.userCode    = this.logedInUserRoles.userCode;
    req.header.functionalityCode    = "USRA-UUE";

    console.log('default',req)
    var url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
    this.httpRequest.httpPostRequests(url, req).then(resp => {
      console.log('Default Resp',resp)
      $('#default-spiner-'+userCode).css("display","none");
      if (typeof resp['data'].clientId !="undefined") {
        console.log("Successfully set to default");
        if (resp['data'].defaultPhysician == 1) {
          console.log('defautl success');

          this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck +1;
          if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck > 0) {
            this.notifier.hideAll();
          }
        }
        else{
          if (this.enableCompleteDefaultCheck > 0) {
            this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck -1;
          }
          else{
            this.enableCompleteDefaultCheck = 0
          }
          this.notifier.getConfig().behaviour.autoHide = 3000;
          this.notifier.notify( "warning", "Unable to set default physician");

        }
      }

      // this.reportFormat = resp;
    })

  }
  superUserCheck(userCode, e, userIndex){
    var req = {
      header:{
        uuid:"",
        partnerCode:"",
        userCode:"",
        referenceNumber:"",
        systemCode:"",
        moduleCode:"CLIM",
        functionalityCode: "CLTA-UC",
        systemHostAddress:"",
        remoteUserAddress:"",
        dateTime:""
      },
      data:{
        clientId:1,
        userCode:1,
        superUser:"",
        visibleInCases:"",
        isPhysician:"",
        defaultPhysician:"",
        practiseCode:"",
        addedBy:1,
        addedTimestamp:"",
        active:"",
        isClientPortalAccess:""
      }
    }
    console.log('userCode',userCode);
    // console.log('userIndex',userIndex);
    console.log('event',e.target.checked);
    if (e.target.checked) {
      req.data.superUser = "1"
    }
    else{
      req.data.superUser = "0"
    }
    // console.log('e.',e);
    // console.log('class',e.srcElement.className);
    // console.log('$("."+e.srcElement.className)',$("."+e.srcElement.className));
    // var a = e.srcElement.parentNode.parentNode.parentNode.nextSibling;
    // console.log('a',a);
    // console.log('child0',a.children[0]);
    // console.log('child1',a.children[1]);
    // console.log('child0style',a.children[0].style.display);
    // console.log('child1style',a.children[1].style.display);
    //
    // console.log('nextsbling',e.srcElement.parentNode.parentNode.parentNode.nextSibling);
    req.data.userCode   = userCode;
    req.data.clientId = this.selectedClientID;

    req.data.visibleInCases   = this.userShowData[userIndex].isVisibleInCases;
    req.data.defaultPhysician = this.userShowData[userIndex].isDefaultPhysician;
    req.data.practiseCode     = this.userShowData[userIndex].practiseCode;
    req.data.active           = this.userShowData[userIndex].active;
    req.data.isClientPortalAccess   = this.userShowData[userIndex].isClientPortalAccess

    req.header.partnerCode = this.logedInUserRoles.partnerCode;
    req.header.userCode    = this.logedInUserRoles.userCode;
    req.header.functionalityCode    = "USRA-UUE";

    console.log('updateClientUser',req)
    // req.data.userCode = userCode;
    var url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
    this.httpRequest.httpPostRequests(url, req).then(resp => {
      console.log('reportFormat',resp)
      if(resp['data'].superUser == "1"){
        // console.log('this.userShowData[userIndex]',this.userShowData[userIndex]);

        this.userShowData[userIndex].isSuperUser = 1;
        req.data.superUser = "";
        // var a = e.srcElement.parentNode.parentNode.parentNode.nextSibling;
        // a.children[0].style.display="inline";
        this.enableCompleteSuperCheck = this.enableCompleteSuperCheck + 1;
        if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck > 0) {
          this.notifier.hideAll();
        }
      }
      else{
        this.userShowData[userIndex].isSuperUser = 0;
        req.data.superUser = "";
        // var a = e.srcElement.parentNode.parentNode.parentNode.nextSibling;
        // a.children[0].style.display="none";
        if (this.enableCompleteSuperCheck > 0) {
          this.enableCompleteSuperCheck = this.enableCompleteSuperCheck -1;
        }
        else{
          this.enableCompleteSuperCheck = 0
        }
        // this.notifier.notify( "warning", "Unable to set as Super User");


        // this.enableCompleteSuperCheck = this.enableCompleteSuperCheck - 1;
      }

      // this.reportFormat = resp;
    })
  }
  clientPortalAccessCheck(userCode, e, userIndex){
    console.log('userCode',userCode);
    console.log('event',e.target.checked);
    var req = {
      header:{
        uuid:"",
        partnerCode:"",
        userCode:"",
        referenceNumber:"",
        systemCode:"",
        moduleCode:"CLIM",
        functionalityCode: "CLTA-UC",
        systemHostAddress:"",
        remoteUserAddress:"",
        dateTime:""
      },
      data:{
        clientId:1,
        userCode:1,
        superUser:"",
        visibleInCases:"",
        isPhysician:"",
        defaultPhysician:"",
        practiseCode:"",
        addedBy:1,
        addedTimestamp:"",
        active:"",
        isClientPortalAccess:""
      }
    }
    console.log('userCode',userCode);
    console.log('event',e.target.checked);
    if (e.target.checked) {
      req.data.isClientPortalAccess = "1"
    }
    else{
      req.data.isClientPortalAccess = "0"
    }
    // console.log('reportFormat',req)
    // req.data.userCode = userCode;
    req.data.userCode           = userCode;
    req.data.clientId         = this.selectedClientID;

    req.data.superUser        = this.userShowData[userIndex].isSuperUser;
    req.data.defaultPhysician = this.userShowData[userIndex].isDefaultPhysician;
    req.data.practiseCode     = this.userShowData[userIndex].practiseCode;
    req.data.active           = this.userShowData[userIndex].active;
    req.data.visibleInCases   = this.userShowData[userIndex].isVisibleInCases

    req.header.partnerCode = this.logedInUserRoles.partnerCode;
    req.header.userCode    = this.logedInUserRoles.userCode;
    req.header.functionalityCode    = "USRA-UUE";

    var url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
    this.httpRequest.httpPostRequests(url, req).then(resp => {
      console.log('resp',resp)
      // this.reportFormat = resp;
      if(resp['data'].isClientPortalAccess == "1"){
        this.userShowData[userIndex].isClientPortalAccess = 1;
        req.data.isClientPortalAccess = "";
        // var a = e.srcElement.parentNode.parentNode.parentNode.nextSibling;
        // a.children[2].style.display="inline"
      }
      else{
        this.userShowData[userIndex].isClientPortalAccess = 0;
        req.data.isClientPortalAccess = "";
        // var a = e.srcElement.parentNode.parentNode.parentNode.nextSibling;
        // a.children[2].style.display="none"
      }

    })

    //  if(event.target.checked){
    //    $('#appenIcons').append('<i title="Visible in Cases" class="fas fa-chalkboard-teacher text-info"></i>');
    // }
  }
  visibleInCasesCheck(userCode, e, userIndex){
    var req = {
      header:{
        uuid:"",
        partnerCode:"",
        userCode:"",
        referenceNumber:"",
        systemCode:"",
        moduleCode:"CLIM",
        functionalityCode: "CLTA-UC",
        systemHostAddress:"",
        remoteUserAddress:"",
        dateTime:""
      },
      data:{
        clientId:1,
        userCode:1,
        superUser:"",
        visibleInCases:"",
        isPhysician:"",
        defaultPhysician:"",
        practiseCode:"",
        addedBy:1,
        addedTimestamp:"",
        active:"",
        isClientPortalAccess:""
      }
    }
    console.log('userCode',userCode);
    console.log('event',e.target.checked);
    if (e.target.checked) {
      req.data.visibleInCases = "1"
    }
    else{
      req.data.visibleInCases = "0"
    }
    // console.log('reportFormat',req)
    // req.data.userCode = userCode;
    req.data.userCode           = userCode;
    req.data.clientId         = this.selectedClientID;

    req.data.superUser        = this.userShowData[userIndex].isSuperUser;
    req.data.defaultPhysician = this.userShowData[userIndex].isDefaultPhysician;
    req.data.practiseCode     = this.userShowData[userIndex].practiseCode;
    req.data.active           = this.userShowData[userIndex].active;
    req.data.isClientPortalAccess   = this.userShowData[userIndex].isClientPortalAccess
    req.header.partnerCode = this.logedInUserRoles.partnerCode;
    req.header.userCode    = this.logedInUserRoles.userCode;
    req.header.functionalityCode    = "USRA-UUE";

    var url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
    this.httpRequest.httpPostRequests(url, req).then(resp => {
      console.log('resp',resp)
      // this.reportFormat = resp;
      if(resp['data'].visibleInCases == "1"){
        this.userShowData[userIndex].isVisibleInCases = 1;
        req.data.visibleInCases = "";
        // var a = e.srcElement.parentNode.parentNode.parentNode.nextSibling;
        // a.children[2].style.display="inline"
      }
      else{
        this.userShowData[userIndex].isVisibleInCases = 0;
        req.data.visibleInCases = "";
        // var a = e.srcElement.parentNode.parentNode.parentNode.nextSibling;
        // a.children[2].style.display="none"
      }

    })

    //  if(event.target.checked){
    //    $('#appenIcons').append('<i title="Visible in Cases" class="fa fa-calendar-plus-o text-info"></i>');
    // }
  }
  activeInactiveCheck(userCode, e, userIndex){
    var req = {
      header:{
        uuid:"",
        partnerCode:"",
        userCode:"",
        referenceNumber:"",
        systemCode:"",
        moduleCode:"CLIM",
        functionalityCode: "CLTA-UC",
        systemHostAddress:"",
        remoteUserAddress:"",
        dateTime:""
      },
      data:{
        clientId:1,
        userCode:1,
        superUser:"",
        visibleInCases:"",
        isPhysician:"",
        defaultPhysician:"",
        practiseCode:"",
        addedBy:1,
        addedTimestamp:"",
        active:"",
        isClientPortalAccess:""
      }
    }
    // console.log("eee",e);
    console.log("e.target",e.target);
    console.log("e.target.checked",e.target.checked);
    var targetUemail = $("#TR-"+userCode).find("input[name*='clientProvidedEmail']")[0].value;
    console.log("targetUemail",targetUemail);
    var targetUfax = $("#TR-"+userCode).find("input[name*='clientProvidedFax']")[0].value;
    console.log("targetUfax",targetUfax);

    if ((targetUemail =="" || typeof targetUemail =="undefined") || (targetUfax =="" || typeof targetUfax =="undefined")) {
      this.notifier.getConfig().behaviour.autoHide = 3000;
      this.notifier.notify( "warning", "Provide email and fax to enable user" );
      e.target.checked = false;
      return;
    }
    else{
      // this.disableUserReq.data.userCode = userCode;
      req.data.userCode           = userCode;
      req.data.clientId         = this.selectedClientID;
      req.data.superUser        = this.userShowData[userIndex].isSuperUser;
      req.data.visibleInCases   = this.userShowData[userIndex].isVisibleInCases;
      req.data.defaultPhysician = this.userShowData[userIndex].isDefaultPhysician;
      req.data.practiseCode     = this.userShowData[userIndex].practiseCode;
      req.data.isClientPortalAccess   = this.userShowData[userIndex].isClientPortalAccess

      req.header.partnerCode = this.logedInUserRoles.partnerCode;
      req.header.userCode    = this.logedInUserRoles.userCode;
      req.header.functionalityCode    = "USRA-UUE";


      let url  = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
      if (e.target.checked) {
        if (this.updateRequestData.data.active == 1 ) {
          req.data.active = '1';
          console.log('req',req);
          this.httpRequest.httpPostRequests(url, req).then(resp => {
            console.log('disable resp:', resp);
            this.ngxLoader.stop();
            if (resp['data'].active == 1) {
              this.userShowData[userIndex].active = 1;
              req.data.active = "";
            }
            else{
              this.userShowData[userIndex].active = 0;
              req.data.active = "";
            }

          })
        }
        else if(this.updateRequestData.data.active == 0 ){
          e.target.checked = false;
          this.notifier.notify( "warning", "Sorry! Actiavtion is not allowed because the selected client is in Draft state" );
        }
        else{
          e.target.checked = false;
          this.notifier.notify( "warning", "Sorry! Actiavtion is not allowed because the selected client is in InActive state" );

        }


      }
      else{
        req.data.active = '0';
        Swal.fire({
          title              : 'Are you sure?',
          text               : "This user won't be able to access the client portal!",
          icon               : 'warning',
          showCancelButton   : true,
          confirmButtonColor : '#3085d6',
          cancelButtonColor  : '#d33',
          confirmButtonText  : 'Yes, deactivate it!'
        }).then((result) => {
          if (result.value) {
            this.ngxLoader.start();
            console.log('req',req);

            this.httpRequest.httpPostRequests(url, req).then(resp => {
              console.log('disable resp:', resp);
              this.ngxLoader.stop();
              if (resp['data'].active == 1) {
                this.userShowData[userIndex].active = 1;
                req.data.active = "";
              }
              else{
                this.userShowData[userIndex].active = 0;
                req.data.active = "";
              }

            })

          }
        })
      }




      // if (e.target.checked) {
      //    var a = e.srcElement.parentNode.parentNode.parentNode.nextSibling;
      //    a.children[3].style.display="inline"
      //  }
      //  else{
      //    var a = e.srcElement.parentNode.parentNode.parentNode.nextSibling;
      //    a.children[3].style.display="none"
      //  }

    }

  }
  deleteModal(userCode){
    console.log('del');
    this.deleteuserCode = userCode;
    $('#deleteModalClient').modal('show');
  }
  deleteClientUser(){
    console.log('this.deleteuserCode',this.deleteuserCode);
    console.log('this.client',this.selectedClientID);
    // return;

    this.deleteClientUserData.data.clientId = this.selectedClientID;
    this.deleteClientUserData.data.userCode   = this.deleteuserCode;
    // console.log('userShowData',this.userShowData);
    //
    // return;
    this.deleteClientUserData.header.partnerCode = this.logedInUserRoles.partnerCode;
    this.deleteClientUserData.header.userCode    = this.logedInUserRoles.userCode;
    this.deleteClientUserData.header.functionalityCode    = "CLTA-UC";
    var url                = environment.API_CLIENT_ENDPOINT + 'deleteclientuser ';
    this.httpRequest.httpPostRequests(url, this.deleteClientUserData).then(resp => {
      console.log('resp',resp)
      if (resp['data'] == 1) {
        for (let i = 0; i < this.userShowData.length; i++) {
          if (this.userShowData[i].userCode == this.deleteuserCode) {
            this.userShowData.splice(i, 1);
          }

        }
        $('#deleteModalClient').modal('hide');
        // $('#TR-'+this.deleteuserCode).css('display','none');
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.ajax.reload();
          // // Destroy the table first
          // dtInstance.destroy();
          // // Call the dtTrigger to rerender again
          // this.dtTrigger.next();
        });
      }
      else{
        console.log("error Ehile Deleting");
      }
      // this.reportFormat = resp;

    })
    $('#deleteModal').modal('hide');
  }

  populateForm(resp){
    this.selectedData = resp;
    this.ClientTypeHolder   = resp.ClientTypeDto[0].clientTypeId;
    console.log("this.reqTypeTemplate",this.reqTypeTemplate);
    // for (let i = 0; i < 3; i++) {
    //
    // }

    this.requisitionDTOTEMP =this.reqTypeTemplate[0];

    // console.log('this.reqTypeTemplate',this.reqTypeTemplate);
    // console.log('this.requisitionDTOTEMP',this.requisitionDTOTEMP);
    // this.accType = resp.ClientTypeDto[0].clientTypeId;
    // this.accID   = resp.accountNumber;
    // this.cname   = resp.name;
    // this.accproir= resp.defaultPriority;
    // this.pcOrg   = resp.parentClientId;
    // this.cNotes  = resp.notes;
    // console.log("this.testContactData",this.testContactData);
    var object = {
      ""             : "",
      "ContactNo"    : "",
      "Fax"          : "",
      "Email"        : "",
      "Default"      : "",
    }

    var chunks = [], i = 0, n = this.testContactData.length;
    while (i<n) {
      chunks.push(this.testContactData.slice(i, i += 3));
    }
    console.log("chunks",chunks);
    for (let j = 0; j < chunks.length; j++) {
      object["ContactNo"] = chunks[j]['0'].value;
      object["Fax"]       = chunks[j]['1'].value;
      object["Email"]     = chunks[j]['1'].value;

      // console.log('object IN',object);
      this.conactHolder.push(object);

    }


    console.log("this.conactHolder",this.conactHolder);


  }


  saveCompleteData(flag){
    console.log("flag",flag);

    ////// if flag = 1 then save as draft else save completely
    // this.saveClientData.draftFlag = flag;
    // console.log('this.saveData',this.saveClientData);
    // console.log('this.SaveCUserData',this.SaveCUserData);
    // this.saveData.ClientData = this.saveClientData;
    // this.saveData.ClientData = this.SaveCUserData;


  }



  // // // Used to convert Contact Details Table into object
  mapRow(headings) {
    return function mapRowToObject({ cells }) {
      return [...cells].reduce(function(result, cell, i) {
        const input = cell.querySelector("input,select");
        var value;

        if (input) {
          value = input.type === "radio" ? input.checked : input.value;
        } else if(input != '') {
          value = cell.innerText;
        }

        return Object.assign(result, { [headings[i]]: value });
      }, {});
    };
  }

  parseTable(table) {
    var headings = [...table.tHead.rows[0].cells].map(
      heading => heading.innerText
    );
    return [...table.tBodies[0].rows].map(this.mapRow(headings));
  }

  // // // Used to convert Contact Details Table into object
  mapRowUpdate(headings) {
    console.log("HEEDAIINAD",headings);

    headings[0] = "ContactNo"
    return function mapRowToObject({ cells }) {
      return [...cells].reduce(function(result, cell, i) {
        const input = cell.querySelector("input,select");
        // const input = cell.querySelectorAll("input,select");
        var value;
        // console.log("cell",cell);
        // console.log("input",input);
        var idvalue;
        var contact_id;
        if (input == null) {
          idvalue= null;
          contact_id = null;
        }
        else{
          idvalue    = input.getAttribute("data-value")
          contact_id = input.getAttribute("contact-id")
        }
        // console.log("idvalue",idvalue);
        // console.log("0",input[0]);
        // console.log("1",input[1]);
        if (input) {
          value = input.type === "radio" ? input.checked : input.value;
          // console.log("value",value);

        } else if(input != '') {
          value = cell.innerText;
          // console.log("cell",cell);

        }
        var completeValue = value +','+ idvalue +','+ contact_id;

        // return Object.assign(result, { [headings[i]]: value });
        return Object.assign(result, { [headings[i]]: completeValue });
      }, {});
    };
  }

  parseTableUpdate(table) {
    var headings = [...table.tHead.rows[0].cells].map(
      heading => heading.innerText
    );
    return [...table.tBodies[0].rows].map(this.mapRowUpdate(headings));
  }

  toggleActiveClient(){
    console.log("toggle",this.clientActiveToggle);
    console.log("this.selectedClientID",this.selectedClientID);

    this.clientStatus.data.clientStatus = this.clientActiveToggle;
    this.clientStatus.data.clientId     = this.selectedClientID;
    this.clientStatus.header.partnerCode = this.logedInUserRoles.partnerCode;
    this.clientStatus.header.userCode    = this.logedInUserRoles.userCode;
    this.clientStatus.header.functionalityCode    = "CLTA-MC";
    var url                = environment.API_CLIENT_ENDPOINT + 'changeclientstatus';

    if (this.clientActiveToggle == 2) {
      Swal.fire({
        title              : 'Are you sure?',
        text               : "This user won't be able to access the client portal!",
        icon               : 'warning',
        showCancelButton   : true,
        confirmButtonColor : '#3085d6',
        cancelButtonColor  : '#d33',
        confirmButtonText  : 'Yes, deactivate it!'
      }).then((result) => {
        if (result.value) {
          this.ngxLoader.start();
          this.httpRequest.httpPostRequests(url, this.clientStatus).then(resp => {
            this.ngxLoader.stop();
            console.log('resp',resp)
            if (typeof resp['data'] == "undefined") {
              // alert('Error While Chaging Status')
              this.notifier.getConfig().behaviour.autoHide = 3000;
              this.notifier.notify( "error", "Error While Chaging Status");
            }
            if (resp['data'] == 1) {
              // this.hideButtonView = false;
              // alert('success')
              this.updateRequestData.data.active = 2;
              this.notifier.getConfig().behaviour.autoHide = 3000;
              this.notifier.notify( "success", "Status Changed Successfully");
            }
            else if(resp['data'] == 0){
              // console.log("error Ehile Deleting");
            }
            // this.reportFormat = resp;

          })

        }
      })

    }
    else{
      Swal.fire({
        title              : 'Are you sure?',
        text               : "To activate the client. Please confirm",
        icon               : 'warning',
        showCancelButton   : true,
        confirmButtonColor : '#3085d6',
        cancelButtonColor  : '#d33',
        confirmButtonText  : 'Yes, activate it!'
      }).then((result) => {
        if (result.value) {
          this.ngxLoader.start();
          this.httpRequest.httpPostRequests(url, this.clientStatus).then(resp => {

            console.log('resp',resp)
            this.ngxLoader.stop();
            if (typeof resp['data'] == "undefined") {
              this.notifier.getConfig().behaviour.autoHide = 3000;
              this.notifier.notify( "error", "Error While Chaging Status");
            }
            if (resp['data'] == 1) {
              // this.hideButtonView = true;
              // alert('success')
              this.updateRequestData.data.active = 1;
              this.notifier.getConfig().behaviour.autoHide = 3000;
              this.notifier.notify( "success", "Status Changed Successfully");
            }
            else if(resp['data'] == 0){
              // console.log("error Ehile Deleting");
            }
            // this.reportFormat = resp;

          })

        }
      })


    }

  }

  checkContactEmail(e){
    var regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    var serchfind = regexp.test(e.target.value);
    // console.log("serchfind",serchfind);
    if (e.target.value != '' && typeof e.target.value != 'undefined') {
      if (serchfind) {
        this.contactEmailValidity   = true;
        this.contactRowValid        = true;
        // this.enableAddContactButton();
      }
      else{
        if (this.contactRowValid == true) {
          // this.contactEmailValidity   = true;
          this.contactEmailValidity   = false;
        }
        else{
          this.contactEmailValidity   = false;
          this.contactRowValid        = false;
        }

      }
    }
    else{
      this.contactEmailValidity   = true;
    }


  }
  enableAddContactButton(e){
    console.log(e.target.value);

    if (typeof e.target.value != "undefined" && e.target.value != "") {
      this.contactRowValid        = true;
    }
    else{
      if (this.contactEmailValidity == true) {
        this.contactRowValid        = true;
      }else{
        this.contactRowValid        = false;
      }

    }
    var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
    e.target.value = !x[2] ? x[1] : '(' + x[1] + ')' + x[2] + (x[3] ? '-' + x[3] : '')

  }

  changeSalesRep(e){
    // console.log("e",e);

    if (typeof this.defaultSalesReps !="undefined") {
      for (let i = 0; i < this.defaultSalesReps.length; i++) {


        if (e == this.defaultSalesReps[i].userCode) {
          // console.log("this.defaultSalesReps[i]",this.defaultSalesReps[i]);
          // console.log("this.defaultSalesReps",this.defaultSalesReps);
          // console.log("this.defaultSalesReps[i].userCode",this.defaultSalesReps[i].userCode);
          // console.log("this.updateSalesRepresentativeId",this.updateSalesRepresentativeId);
          this.clientForm.patchValue({
            srFirstName : this.defaultSalesReps[i].firstName,
            srLastName  : this.defaultSalesReps[i].lastName,
            srEmail     : this.defaultSalesReps[i].email,
            srContact   : this.defaultSalesReps[i].phone,
          });
        }
      }
    }
    if (e =='') {
      this.clientForm.patchValue({
        srFirstName : '',
        srLastName  : '',
        srEmail     : '',
        srContact   : '',
      });
    }


    // console.log('this.updateSalesRepresentativeId',this.updateSalesRepresentativeId);
    //
    // if (typeof this.updateSalesRepresentativeId == "undefined" || this.updateSalesRepresentativeId == "") {
    //     this.disabledSalesRepFields = false;
    //     console.log("in");
    // }
    // else{
    //   this.disabledSalesRepFields = true;
    // }
  }

  getAllUsers(fromState){
    // this.notifier.hideAll();
    console.log("fromState",fromState);


    /////////////////// from = 1 means when user have Submited the client
    /////////////////// from = 0 means it is commoming from manage page or data is already loaded or Tab is clicked
    if (fromState == 1) {
      this.ngxLoader.start();
      $('#clientUsers').DataTable().destroy();
      // console.log("//////////////////////////////////DETRO",$('#clientUsers').DataTable());

      // this.notifier.hideAll();
      this.partnerUsersID    = [];
      this.validPartnerUsers = [];
      this.userShowData      = [];
      var temporaryContact   = [];
      var userShowDataTemp   = [];
      var userClientDetails  = [];
      this.enableCompleteDefaultCheck = 0;
      this.enableCompleteSuperCheck   = 0;

      // this.ngxLoader.start();
      console.log("users");
      this.partnerUsersID = [];


      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
      // if (this.enableCompleteSuperCheck == 0 && this.enableCompleteDefaultCheck > 0) {
      //   this.notifier.getConfig().behaviour.autoHide = false;
      //   this.notifier.notify( "warning", "You need to define a super user for this client");
      // }
      // else if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck == 0) {
      //   this.notifier.getConfig().behaviour.autoHide = false;
      //   this.notifier.notify( "warning", "You need to define a attending physician for this client");
      // }
      // else if (this.enableCompleteSuperCheck == 0 && this.enableCompleteDefaultCheck == 0) {
      //
      //   this.notifier.getConfig().behaviour.autoHide = false;
      //   this.notifier.notify( "warning", "One super user, and one attending physician must be defined in order to add cases for this client");
      // }
    }
    else if(fromState == 0){

      if (typeof this.userShowData =='undefined' || this.userShowData.length == 0) {
        this.ngxLoader.start();
        // console.log("elseif");

        $('#clientUsers').DataTable().destroy();
        // console.log("//////////////////////////////////DETRO",$('#clientUsers').DataTable());

        this.notifier.hideAll();
        this.partnerUsersID    = [];
        this.validPartnerUsers = [];
        this.userShowData      = [];
        var temporaryContact   = [];
        var userShowDataTemp   = [];
        var userClientDetails  = [];
        this.enableCompleteDefaultCheck = 0;
        this.enableCompleteSuperCheck   = 0;

        // this.ngxLoader.start();
        // console.log("users");
        this.partnerUsersID = [];


        // Call the dtTrigger to rerender again
        this.dtTrigger.next();
      }
      else{
        // console.log("ppppppppppp");

        this.ngxLoader.stop();
      }
    }
    else{
      this.ngxLoader.stop();
    }

    // this.ngxLoader.stop();






  }
  popTable(data){
    console.log("77777777777777777777",data);
    var index = this.userShowData.length;
    // var araayIndex = index
    console.log("this.userShowData.length",this.userShowData.length);
    // this.userShowData.push(data);
    if (index == 0) {
      this.userShowData = data;
    }
    else if (index > 0) { //////// if ther are users allready in sho data (I think i did it for nothing)
      for (let i = 0; i < data.length; i++) {
        this.userShowData[index] = data[i];
        index = index + 1;
      }
      // this.userShowData[] = data;
    }
    // for (let j = 0; j < this.userShowData.length; j++) {
    //   var a = this.userShowData[j].userData.userRoles.search('Attending Physician')
    //   console.log("a",a);
    //
    // }
    console.log("this.userShowData.length",this.userShowData);

    // this.userShowData = data;
    // this.dtTrigger.next();
    // this.dtTrigger.unsubscribe();

  }
  // ngOnDestroy(): void {
  //   // Do not forget to unsubscribe the event
  //   this.dtTrigger.unsubscribe();
  // }
  getNPI(){
    $('#NPI-spiner').css("display","inline-block");
    this.userByNpi.data.npi = this.requestData.data.user.npi;
    console.log("userByNpi",this.userByNpi);
    this.userByNpi.header.userCode    = this.logedInUserRoles.userCode;
    this.userByNpi.header.partnerCode = this.logedInUserRoles.partnerCode;
    this.userByNpi.header.functionalityCode = "USRA-UU";

    let url    = environment.API_USER_ENDPOINT + 'userbynpiemail';
    this.http.put(url,this.userByNpi).toPromise()
    .then( resp => {
      console.log('**** resp NPI: ', resp);
      this.userByNpi.data.npi   = "";
      // this.userByNpi.data.email = "";
      ////// already pushed
      // if (found == false) arr.push({ id, username: name });
      $('#NPI-spiner').css("display","none");
      // this.ngxLoader.stop();
      if (resp["result"].codeType == "S") {
        Swal.fire({
          title              : 'User Exists?',
          text               : "This user already exists, do you want to load!",
          icon               : 'warning',
          showCancelButton   : true,
          confirmButtonColor : '#3085d6',
          cancelButtonColor  : '#d33',
          confirmButtonText  : 'Yes, load it!'
        }).then((result) => {
          if (result.value) {
            const found = this.userShowData.some(el => el.userCode === resp["data"].userCode);
            if (found) {
              this.inValidUsernpi = true;
              this.notifier.getConfig().behaviour.autoHide = 3000;
              this.notifier.notify( "info", "User already affiliated with client" );
            }
            else{
              this.ngxLoader.start();
              // var a  = resp['data']['userRoles'].split(',');
              // this.requestData.data.user.roles = a;
              // this.requestData.data.user.npi   = resp['data']['npi'];
              // this.requestData.data.user.email = resp['data']['email'];
              // // this.selectedPostNominal        = resp['data']['postNominal'];
              // this.requestData.data.user.title = resp['data']['title'];
              // this.FName = resp['data']['firstName'];
              // this.LName = resp['data']['lastName'];
              // this.UName = resp['data']['username'];
              // this.requestData.data.user.phone = resp['data']['phone'];
              // this.addUserManually = true;
              // this.responseUserNPIEmail = resp;
              //
              // this.ngxLoader.stop();
              //
              // return;
              /////////////// Request To client API
              this.clientUserReq.data.userCode   = resp["data"].userCode;
              this.clientUserReq.data.clientId = this.selectedClientID;
              this.clientUserReq.header.partnerCode    = this.logedInUserRoles.partnerCode;
              this.clientUserReq.header.userCode    = this.logedInUserRoles.userCode;
              this.clientUserReq.header.functionalityCode    = "USRA-AUE";
              let url                         = environment.API_CLIENT_ENDPOINT + 'addclientuser';
              this.httpRequest.httpPostRequests(url,this.clientUserReq).then(response => {
                console.log("--resp Client User",response);

                // this.allUsers.push(resp);
                this.ngxLoader.stop();
                if (typeof response['data'] != "undefined") {
                  this.notifier.getConfig().behaviour.autoHide = 3000;
                  this.notifier.notify( "success", "User saved Successfully" );
                  var emptyClientUserHolder  = {
                    active: 0,
                    addedBy: 0,
                    addedTimestamp: null,
                    clientId: 0,
                    contactDetails: [],
                    defaultPhysician: 0,
                    isPhysician: 0,
                    practiseCode: "",
                    superUser: 0,
                    userData: {},
                    userCode: 1,
                    visibleInCases: 0
                  }

                  var temporaryContact = [];
                  temporaryContact[0]     = "";
                  temporaryContact[0]     = "";
                  temporaryContact[1]     = "";
                  temporaryContact[1]     = "";
                  emptyClientUserHolder.userData       = resp["data"];
                  emptyClientUserHolder.userCode         = resp["data"].userCode;
                  emptyClientUserHolder.clientId       = this.selectedClientID;
                  emptyClientUserHolder.contactDetails = temporaryContact;
                  console.log('emptyClientUserHolder',emptyClientUserHolder);

                  //////////////// if Selected Client is Active then////
                  ////////////////Set newly aded user as activ

                  //////////////// if Selected Client is Active then////
                  ////////////////Set newly aded user as activ
                  if (this.updateRequestData.data.active == 1) {
                    //////////////// if New Added/Loaded User is Attending Physician the then////
                    ////////////////Check if its first AP if yes the make it default
                    // const foundRols = resp["data"].userRoles.some(el => el.name === 'Attending Physician');
                    if (resp["data"].userRoles.search('Attending Physician')!= -1) {
                      if(this.userShowData.length > 0){
                        const found = this.userShowData.some(el => el.isDefaultPhysician === 1);
                        console.log("FOUNDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD",found);

                        if(found == false){
                          var setActivereq = {
                            header:{
                              uuid:"",
                              partnerCode:"",
                              userCode:"",
                              referenceNumber:"",
                              systemCode:"",
                              moduleCode:"CLIM",
                              functionalityCode: "CLTA-UC",
                              systemHostAddress:"",
                              remoteUserAddress:"",
                              dateTime:""
                            },
                            data:{
                              clientId:1,
                              userCode:1,
                              superUser:"",
                              visibleInCases:"",
                              isPhysician:"",
                              defaultPhysician:"",
                              practiseCode:"",
                              addedBy:"",
                              addedTimestamp:"",
                              active:""
                            }
                          }
                          setActivereq.data.clientId            = this.selectedClientID;
                          setActivereq.data.userCode              = resp["data"].userCode;
                          setActivereq.data.defaultPhysician    = '1';
                          setActivereq.data.active              = '1';
                          setActivereq.header.partnerCode = this.logedInUserRoles.partnerCode;
                          setActivereq.header.userCode    = this.logedInUserRoles.userCode;
                          setActivereq.header.functionalityCode    = "USRA-UUE";
                          let url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
                          this.httpRequest.httpPostRequests(url, setActivereq).then(respPhy => {
                            console.log('respPhy resp:', respPhy);
                            this.ngxLoader.stop();
                            if (respPhy['data'].defaultPhysician == 1) {
                              emptyClientUserHolder.defaultPhysician = 1;
                              emptyClientUserHolder.active = 1;
                              this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck +1;
                              if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck > 0) {
                                this.notifier.hideAll();
                              }
                            }
                            else{
                              emptyClientUserHolder.defaultPhysician = 0;
                              emptyClientUserHolder.active = 1;
                            }
                            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                              dtInstance.ajax.reload();
                              // Destroy the table first
                              // dtInstance.destroy();
                              // // Call the dtTrigger to rerender again
                              // this.dtTrigger.next();
                            });
                            // this.responseUserNPIEmail = {}
                            // this.resetUserForm()

                          })
                        }
                        else{
                          var setActivereq = {
                            header:{
                              uuid:"",
                              partnerCode:"",
                              userCode:"",
                              referenceNumber:"",
                              systemCode:"",
                              moduleCode:"CLIM",
                              functionalityCode: "CLTA-UC",
                              systemHostAddress:"",
                              remoteUserAddress:"",
                              dateTime:""
                            },
                            data:{
                              clientId:1,
                              userCode:1,
                              superUser:"",
                              visibleInCases:"",
                              isPhysician:"",
                              defaultPhysician:"",
                              practiseCode:"",
                              addedBy:"",
                              addedTimestamp:"",
                              active:""
                            }
                          }
                          setActivereq.data.clientId            = this.selectedClientID;
                          setActivereq.data.userCode              = resp["data"].userCode;
                          setActivereq.data.defaultPhysician    = '0';
                          setActivereq.data.active              = '1';
                          setActivereq.header.partnerCode = this.logedInUserRoles.partnerCode;
                          setActivereq.header.userCode    = this.logedInUserRoles.userCode;
                          setActivereq.header.functionalityCode    = "USRA-UUE";
                          let url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
                          this.httpRequest.httpPostRequests(url, setActivereq).then(respPhy => {
                            console.log('respPhy resp:', respPhy);
                            this.ngxLoader.stop();
                            if (respPhy['data'].defaultPhysician == 1) {
                              emptyClientUserHolder.defaultPhysician = 1;
                              emptyClientUserHolder.active = 1;
                              this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck +1;
                              if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck > 0) {
                                this.notifier.hideAll();
                              }
                            }
                            else{
                              emptyClientUserHolder.defaultPhysician = 0;
                              emptyClientUserHolder.active = 1;
                            }
                            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                              dtInstance.ajax.reload();
                              // Destroy the table first
                              // dtInstance.destroy();
                              // // Call the dtTrigger to rerender again
                              // this.dtTrigger.next();
                            });
                            // this.responseUserNPIEmail = {}
                            // this.resetUserForm()

                          })
                        }
                        // for (let k = 0; k < this.userShowData.length; k++) {
                        //   this.userShowData[k]
                        //
                        // }
                      }
                      else{
                        console.log("no Data******");

                        var setActivereq = {
                          header:{
                            uuid:"",
                            partnerCode:"",
                            userCode:"",
                            referenceNumber:"",
                            systemCode:"",
                            moduleCode:"CLIM",
                            functionalityCode: "CLTA-UC",
                            systemHostAddress:"",
                            remoteUserAddress:"",
                            dateTime:""
                          },
                          data:{
                            clientId:1,
                            userCode:1,
                            superUser:"",
                            visibleInCases:"",
                            isPhysician:"",
                            defaultPhysician:"",
                            practiseCode:"",
                            addedBy:"",
                            addedTimestamp:"",
                            active:""
                          }
                        }
                        setActivereq.data.clientId            = this.selectedClientID;
                        setActivereq.data.userCode              = resp["data"].userCode;
                        setActivereq.data.active              = '1';
                        setActivereq.data.defaultPhysician    = '1';
                        setActivereq.header.partnerCode = this.logedInUserRoles.partnerCode;
                        setActivereq.header.userCode    = this.logedInUserRoles.userCode;
                        setActivereq.header.functionalityCode    = "USRA-UUE";
                        let url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
                        this.httpRequest.httpPostRequests(url, setActivereq).then(respPhy => {
                          console.log('att phys resp resp:', respPhy);
                          this.ngxLoader.stop();
                          if (respPhy['data'].defaultPhysician == 1) {
                            emptyClientUserHolder.defaultPhysician = 1;
                            emptyClientUserHolder.active = 1;
                            this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck +1;
                            if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck > 0) {
                              this.notifier.hideAll();
                            }
                          }
                          else{
                            emptyClientUserHolder.defaultPhysician = 0;
                            emptyClientUserHolder.active = 1;
                          }
                          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                            dtInstance.ajax.reload();
                            // Destroy the table first
                            // dtInstance.destroy();
                            // // Call the dtTrigger to rerender again
                            // this.dtTrigger.next();
                          });
                          // this.responseUserNPIEmail = {}
                          // this.resetUserForm()

                        })
                      }
                    }

                  }
                  else{
                    //////////////// if New Added/Loaded User is Attending Physician the then////
                    ////////////////Check if its first AP if yes the make it default
                    console.log('resp["data"]',resp["data"]);

                    // const foundRols = resp["data"].userRoles.some(el => el.name === 'Attending Physician');
                    // console.log("FOUNDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD active 0 out",foundRols);
                    if (resp["data"].userRoles.search('Attending Physician')!= -1) {
                      if(this.userShowData.length > 0){
                        const found = this.userShowData.some(el => el.isDefaultPhysician === 1);
                        // this.defaultAccountTypes[m].name.toLowerCase().indexOf('nursing')>-1
                        console.log("FOUNDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD active 0",found);

                        if(found == false){
                          var setActivereq = {
                            header:{
                              uuid:"",
                              partnerCode:"",
                              userCode:"",
                              referenceNumber:"",
                              systemCode:"",
                              moduleCode:"CLIM",
                              functionalityCode: "CLTA-UC",
                              systemHostAddress:"",
                              remoteUserAddress:"",
                              dateTime:""
                            },
                            data:{
                              clientId:1,
                              userCode:1,
                              superUser:"",
                              visibleInCases:"",
                              isPhysician:"",
                              defaultPhysician:"",
                              practiseCode:"",
                              addedBy:"",
                              addedTimestamp:"",
                              active:""
                            }
                          }
                          setActivereq.data.clientId            = this.selectedClientID;
                          setActivereq.data.userCode              = resp["data"].userCode;
                          setActivereq.data.defaultPhysician    = '1';
                          setActivereq.data.active              = '0';
                          setActivereq.header.partnerCode = this.logedInUserRoles.partnerCode;
                          setActivereq.header.userCode    = this.logedInUserRoles.userCode;
                          setActivereq.header.functionalityCode    = "USRA-UUE";
                          let url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
                          this.httpRequest.httpPostRequests(url, setActivereq).then(respPhy => {
                            console.log('respPhy resp:', respPhy);
                            this.ngxLoader.stop();
                            if (respPhy['data'].defaultPhysician == 1) {
                              emptyClientUserHolder.defaultPhysician = 1;
                              emptyClientUserHolder.active = 0;
                              this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck +1;
                              if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck > 0) {
                                this.notifier.hideAll();
                              }
                            }
                            else{
                              emptyClientUserHolder.defaultPhysician = 0;
                              emptyClientUserHolder.active = 0;
                            }
                            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                              dtInstance.ajax.reload();
                              // Destroy the table first
                              // dtInstance.destroy();
                              // // Call the dtTrigger to rerender again
                              // this.dtTrigger.next();
                            });
                            // this.responseUserNPIEmail = {}
                            // this.resetUserForm()

                          })
                        }
                        else{
                          var setActivereq = {
                            header:{
                              uuid:"",
                              partnerCode:"",
                              userCode:"",
                              referenceNumber:"",
                              systemCode:"",
                              moduleCode:"CLIM",
                              functionalityCode: "CLTA-UC",
                              systemHostAddress:"",
                              remoteUserAddress:"",
                              dateTime:""
                            },
                            data:{
                              clientId:1,
                              userCode:1,
                              superUser:"",
                              visibleInCases:"",
                              isPhysician:"",
                              defaultPhysician:"",
                              practiseCode:"",
                              addedBy:"",
                              addedTimestamp:"",
                              active:""
                            }
                          }
                          setActivereq.data.clientId            = this.selectedClientID;
                          setActivereq.data.userCode              = resp["data"].userCode;
                          setActivereq.data.defaultPhysician    = '0';
                          setActivereq.data.active              = '0';
                          setActivereq.header.partnerCode = this.logedInUserRoles.partnerCode;
                          setActivereq.header.userCode    = this.logedInUserRoles.userCode;
                          setActivereq.header.functionalityCode    = "USRA-UUE";
                          let url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
                          this.httpRequest.httpPostRequests(url, setActivereq).then(respPhy => {
                            console.log('respPhy resp:', respPhy);
                            this.ngxLoader.stop();
                            if (respPhy['data'].defaultPhysician == 1) {
                              emptyClientUserHolder.defaultPhysician = 1;
                              emptyClientUserHolder.active = 0;
                              this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck +1;
                              if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck > 0) {
                                this.notifier.hideAll();
                              }
                            }
                            else{
                              emptyClientUserHolder.defaultPhysician = 0;
                              emptyClientUserHolder.active = 0;
                            }

                          })
                          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                            dtInstance.ajax.reload();
                            // Destroy the table first
                            // dtInstance.destroy();
                            // // Call the dtTrigger to rerender again
                            // this.dtTrigger.next();
                          });
                          // this.responseUserNPIEmail = {}
                          // this.resetUserForm()
                        }
                        // for (let k = 0; k < this.userShowData.length; k++) {
                        //   this.userShowData[k]
                        //
                        // }
                      }
                      else{
                        console.log("no Data******");

                        var setActivereq = {
                          header:{
                            uuid:"",
                            partnerCode:"",
                            userCode:"",
                            referenceNumber:"",
                            systemCode:"",
                            moduleCode:"CLIM",
                            functionalityCode: "CLTA-UC",
                            systemHostAddress:"",
                            remoteUserAddress:"",
                            dateTime:""
                          },
                          data:{
                            clientId:1,
                            userCode:1,
                            superUser:"",
                            visibleInCases:"",
                            isPhysician:"",
                            defaultPhysician:"",
                            practiseCode:"",
                            addedBy:"",
                            addedTimestamp:"",
                            active:""
                          }
                        }
                        setActivereq.data.clientId            = this.selectedClientID;
                        setActivereq.data.userCode              = resp["data"].userCode;
                        setActivereq.data.active                = '0';
                        setActivereq.data.defaultPhysician      = '1';
                        setActivereq.header.partnerCode = this.logedInUserRoles.partnerCode;
                        setActivereq.header.userCode    = this.logedInUserRoles.userCode;
                        setActivereq.header.functionalityCode    = "USRA-UUE";
                        let url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
                        this.httpRequest.httpPostRequests(url, setActivereq).then(respPhy => {
                          console.log('att phys resp resp:', respPhy);
                          this.ngxLoader.stop();
                          if (respPhy['data'].defaultPhysician == 1) {
                            emptyClientUserHolder.defaultPhysician = 1;
                            this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck +1;
                            if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck > 0) {
                              this.notifier.hideAll();
                            }
                            emptyClientUserHolder.active = 0;
                          }
                          else{
                            emptyClientUserHolder.defaultPhysician = 0;
                            emptyClientUserHolder.active = 0;
                          }
                          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                            dtInstance.ajax.reload();

                          });
                          // this.responseUserNPIEmail = {}
                          // this.resetUserForm()

                        })
                      }
                    }

                  }

                  this.userShowData.push(emptyClientUserHolder);
                  // var temp = '';
                  //   console.log('before',resp.data);
                  // for (let i = 0; i < resp.data.roles.length; i++) {
                  //     temp = resp.data.roles[i]['name']+', '+ temp;
                  // }
                  // resp.data.userRoles  = temp;
                  //////this.userShowData.push(resp['data']);
                  this.submitted2 = false;
                  this.clientUserForm.reset();
                  this.notifier.hideAll();
                  this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                    // Destroy the table first
                    dtInstance.destroy();
                    // Call the dtTrigger to rerender again
                    this.dtTrigger.next();
                  });
                  // this.responseUserNPIEmail = {}
                  // this.resetUserForm()

                }
                else{
                  this.ngxLoader.stop();
                  this.notifier.getConfig().behaviour.autoHide = 3000;
                  this.notifier.notify( "error", "Error while saving user CLIENT API");
                }
              })

              // this.userShowData.push(resp["data"])
            }

            // this.ngxLoader.start();

          }
          else{
            this.inValidUsernpi = true;
            this.requestData.data.user.npi = null;
          }
        })
      }
      else{
        this.inValidUsernpi   = false;
      }
      //
    })
    .catch(error => {
      console.log("error NPI: ",error);
    });
  }


  getEMAIL(){
    $('#EMAIL-spiner').css("display","inline-block");
    this.userByNpi.data.email = this.requestData.data.user.email;
    console.log("userByNpi",this.userByNpi);
    this.userByNpi.header.userCode    = this.logedInUserRoles.userCode;
    this.userByNpi.header.partnerCode = this.logedInUserRoles.partnerCode;
    this.userByNpi.header.functionalityCode = "CLTA-UC";

    let url    = environment.API_USER_ENDPOINT + 'userbynpiemail';
    this.http.put(url,this.userByNpi).toPromise()
    .then( resp => {
      console.log('**** resp Email: ', resp);
      // this.userByNpi.data.npi   = "";
      this.userByNpi.data.email = "";
      ////// already pushed
      // if (found == false) arr.push({ id, username: name });
      $('#EMAIL-spiner').css("display","none");
      // this.ngxLoader.stop();
      if (resp["result"].codeType == "S") {
        Swal.fire({
          title              : 'User Exists?',
          text               : "This user already exists, do you want to load!",
          icon               : 'warning',
          showCancelButton   : true,
          confirmButtonColor : '#3085d6',
          cancelButtonColor  : '#d33',
          confirmButtonText  : 'Yes, load it!'
        }).then((result) => {
          if (result.value) {
            const found = this.userShowData.some(el => el.userCode === resp["data"].userCode);
            if (found) {
              this.inValidUseremail = true;
              this.notifier.getConfig().behaviour.autoHide = 3000;
              this.notifier.notify( "info", "User already affiliated with client" );
            }
            else{

              this.ngxLoader.start();
              // var a  = resp['data']['userRoles'].split(',');
              // this.requestData.data.user.roles = a;
              // this.requestData.data.user.npi   = resp['data']['npi'];
              // this.requestData.data.user.email = resp['data']['email'];
              // // this.selectedPostNominal        = resp['data']['postNominal'];
              // this.requestData.data.user.title = resp['data']['title'];
              // this.FName = resp['data']['firstName'];
              // this.LName = resp['data']['lastName'];
              // this.UName = resp['data']['username'];
              // this.requestData.data.user.phone = resp['data']['phone'];
              // this.addUserManually = true;
              // this.responseUserNPIEmail = resp;
              //
              // this.ngxLoader.stop();
              //
              // return;
              /////////////// Request To client API
              this.clientUserReq.data.userCode   = resp["data"].userCode;
              this.clientUserReq.data.clientId = this.selectedClientID;
              this.clientUserReq.header.partnerCode    = this.logedInUserRoles.partnerCode;
              this.clientUserReq.header.userCode    = this.logedInUserRoles.userCode;
              this.clientUserReq.header.functionalityCode    = "USRA-AUE";
              url                         = environment.API_CLIENT_ENDPOINT + 'addclientuser';
              this.httpRequest.httpPostRequests(url,this.clientUserReq).then(response => {
                console.log("--resp Client User",response);

                // this.allUsers.push(resp);
                this.ngxLoader.stop();
                if (typeof response['data'] != "undefined") {
                  this.notifier.getConfig().behaviour.autoHide = 3000;
                  this.notifier.notify( "success", "User saved Successfully" );
                  var emptyClientUserHolder  = {
                    active: 0,
                    addedBy: 0,
                    addedTimestamp: null,
                    clientId: 0,
                    contactDetails: [],
                    defaultPhysician: 0,
                    isPhysician: 0,
                    practiseCode: "",
                    superUser: 0,
                    userData: {},
                    userCode: 1,
                    visibleInCases: 0
                  }
                  var temporaryContact = [];
                  temporaryContact[0]     = "";
                  temporaryContact[0]     = "";
                  temporaryContact[1]     = "";
                  temporaryContact[1]     = "";
                  emptyClientUserHolder.userData       = resp["data"];
                  emptyClientUserHolder.userCode         = resp["data"].userCode;
                  emptyClientUserHolder.clientId       = this.selectedClientID;
                  emptyClientUserHolder.contactDetails = temporaryContact;
                  console.log('emptyClientUserHolder EMail',emptyClientUserHolder);

                  //////////////// if Selected Client is Active then////
                  ////////////////Set newly aded user as activ
                  //////////////// if Selected Client is Active then////
                  ////////////////Set newly aded user as activ
                  if (this.updateRequestData.data.active == 1) {
                    //////////////// if New Added/Loaded User is Attending Physician the then////
                    ////////////////Check if its first AP if yes the make it default
                    // const foundRols = resp["data"].userRoles.some(el => el.name === 'Attending Physician');
                    if (resp["data"].userRoles.search('Attending Physician')!= -1) {
                      if(this.userShowData.length > 0){
                        const found = this.userShowData.some(el => el.isDefaultPhysician === 1);
                        console.log("FOUNDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD",found);

                        if(found == false){
                          var setActivereq = {
                            header:{
                              uuid:"",
                              partnerCode:"",
                              userCode:"",
                              referenceNumber:"",
                              systemCode:"",
                              moduleCode:"CLIM",
                              functionalityCode: "CLTA-UC",
                              systemHostAddress:"",
                              remoteUserAddress:"",
                              dateTime:""
                            },
                            data:{
                              clientId:1,
                              userCode:1,
                              superUser:"",
                              visibleInCases:"",
                              isPhysician:"",
                              defaultPhysician:"",
                              practiseCode:"",
                              addedBy:"",
                              addedTimestamp:"",
                              active:""
                            }
                          }
                          setActivereq.data.clientId            = this.selectedClientID;
                          setActivereq.data.userCode              = resp["data"].userCode;
                          setActivereq.data.defaultPhysician    = '1';
                          setActivereq.data.active              = '1';
                          setActivereq.header.partnerCode = this.logedInUserRoles.partnerCode;
                          setActivereq.header.userCode    = this.logedInUserRoles.userCode;
                          setActivereq.header.functionalityCode    = "USRA-UUE";
                          url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
                          this.httpRequest.httpPostRequests(url, setActivereq).then(respPhy => {
                            console.log('respPhy resp:', respPhy);
                            this.ngxLoader.stop();
                            if (respPhy['data'].defaultPhysician == 1) {
                              emptyClientUserHolder.defaultPhysician = 1;
                              emptyClientUserHolder.active = 1;
                              this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck +1;
                              if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck > 0) {
                                this.notifier.hideAll();
                              }
                            }
                            else{
                              emptyClientUserHolder.defaultPhysician = 0;
                              emptyClientUserHolder.active = 1;
                            }
                            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                              dtInstance.ajax.reload();
                              // Destroy the table first
                              // dtInstance.destroy();
                              // // Call the dtTrigger to rerender again
                              // this.dtTrigger.next();
                            });

                          })
                        }
                        else{
                          var setActivereq = {
                            header:{
                              uuid:"",
                              partnerCode:"",
                              userCode:"",
                              referenceNumber:"",
                              systemCode:"",
                              moduleCode:"CLIM",
                              functionalityCode: "CLTA-UC",
                              systemHostAddress:"",
                              remoteUserAddress:"",
                              dateTime:""
                            },
                            data:{
                              clientId:1,
                              userCode:1,
                              superUser:"",
                              visibleInCases:"",
                              isPhysician:"",
                              defaultPhysician:"",
                              practiseCode:"",
                              addedBy:"",
                              addedTimestamp:"",
                              active:""
                            }
                          }
                          setActivereq.data.clientId            = this.selectedClientID;
                          setActivereq.data.userCode              = resp["data"].userCode;
                          setActivereq.data.defaultPhysician    = '0';
                          setActivereq.data.active              = '1';
                          setActivereq.header.partnerCode = this.logedInUserRoles.partnerCode;
                          setActivereq.header.userCode    = this.logedInUserRoles.userCode;
                          setActivereq.header.functionalityCode    = "USRA-UUE";
                          url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
                          this.httpRequest.httpPostRequests(url, setActivereq).then(respPhy => {
                            console.log('respPhy resp:', respPhy);
                            this.ngxLoader.stop();
                            if (respPhy['data'].defaultPhysician == 1) {
                              emptyClientUserHolder.defaultPhysician = 1;
                              emptyClientUserHolder.active = 1;
                              this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck +1;
                              if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck > 0) {
                                this.notifier.hideAll();
                              }
                            }
                            else{
                              emptyClientUserHolder.defaultPhysician = 0;
                              emptyClientUserHolder.active = 1;
                            }
                            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                              dtInstance.ajax.reload();
                              // Destroy the table first
                              // dtInstance.destroy();
                              // // Call the dtTrigger to rerender again
                              // this.dtTrigger.next();
                            });

                          })
                        }
                        // for (let k = 0; k < this.userShowData.length; k++) {
                        //   this.userShowData[k]
                        //
                        // }
                      }
                      else{
                        console.log("no Data******");

                        var setActivereq = {
                          header:{
                            uuid:"",
                            partnerCode:"",
                            userCode:"",
                            referenceNumber:"",
                            systemCode:"",
                            moduleCode:"CLIM",
                            functionalityCode: "CLTA-UC",
                            systemHostAddress:"",
                            remoteUserAddress:"",
                            dateTime:""
                          },
                          data:{
                            clientId:1,
                            userCode:1,
                            superUser:"",
                            visibleInCases:"",
                            isPhysician:"",
                            defaultPhysician:"",
                            practiseCode:"",
                            addedBy:"",
                            addedTimestamp:"",
                            active:""
                          }
                        }
                        setActivereq.data.clientId            = this.selectedClientID;
                        setActivereq.data.userCode              = resp["data"].userCode;
                        setActivereq.data.active              = '1';
                        setActivereq.data.defaultPhysician    = '1';
                        setActivereq.header.partnerCode = this.logedInUserRoles.partnerCode;
                        setActivereq.header.userCode    = this.logedInUserRoles.userCode;
                        setActivereq.header.functionalityCode    = "USRA-UUE";
                        url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
                        this.httpRequest.httpPostRequests(url, setActivereq).then(respPhy => {
                          console.log('att phys resp resp:', respPhy);
                          this.ngxLoader.stop();
                          if (respPhy['data'].defaultPhysician == 1) {
                            emptyClientUserHolder.defaultPhysician = 1;
                            emptyClientUserHolder.active = 1;
                            this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck +1;
                            if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck > 0) {
                              this.notifier.hideAll();
                            }
                          }
                          else{
                            emptyClientUserHolder.defaultPhysician = 0;
                            emptyClientUserHolder.active = 1;
                          }
                          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                            dtInstance.ajax.reload();
                            // Destroy the table first
                            // dtInstance.destroy();
                            // // Call the dtTrigger to rerender again
                            // this.dtTrigger.next();
                          });

                        })
                      }
                    }
                    // var setActivereq = {
                    //   header:{
                    //     uuid:"",
                    //     partnerCode:"",
                    //     userCode:"",
                    //     referenceNumber:"",
                    //     systemCode:"",
                    //     moduleCode:"CLIM",
                    //     functionalityCode: "CLTA-UC",
                    //     systemHostAddress:"",
                    //     remoteUserAddress:"",
                    //     dateTime:""
                    //   },
                    //   data:{
                    //     clientId:1,
                    //     userCode:1,
                    //     superUser:"",
                    //     visibleInCases:"",
                    //     isPhysician:"",
                    //     defaultPhysician:"",
                    //     practiseCode:"",
                    //     addedBy:"",
                    //     addedTimestamp:"",
                    //     active:""
                    //   }
                    // }
                    // setActivereq.data.clientId  = this.selectedClientID;
                    // setActivereq.data.userCode    = resp["data"].userCode;
                    // setActivereq.data.active    = '1';
                    // url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
                    // this.httpRequest.httpPostRequests(url, setActivereq).then(resp => {
                    //   console.log('disable resp:', resp);
                    //   this.ngxLoader.stop();
                    //   if (resp['data'].active == 1) {
                    //     emptyClientUserHolder.active = 1;
                    //   }
                    //   else{
                    //     emptyClientUserHolder.active = 0;
                    //   }
                    //
                    //
                    // })
                  }
                  else{
                    //////////////// if New Added/Loaded User is Attending Physician the then////
                    ////////////////Check if its first AP if yes the make it default
                    // const foundRols = resp["data"].userRoles.some(el => el.name === 'Attending Physician');
                    // console.log("FOUNDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD active 0 out",foundRols);
                    if (resp["data"].userRoles.search('Attending Physician')!= -1) {
                      if(this.userShowData.length > 0){
                        const found = this.userShowData.some(el => el.isDefaultPhysician === 1);
                        // this.defaultAccountTypes[m].name.toLowerCase().indexOf('nursing')>-1
                        console.log("FOUNDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD active 0",found);

                        if(found == false){
                          var setActivereq = {
                            header:{
                              uuid:"",
                              partnerCode:"",
                              userCode:"",
                              referenceNumber:"",
                              systemCode:"",
                              moduleCode:"CLIM",
                              functionalityCode: "CLTA-UC",
                              systemHostAddress:"",
                              remoteUserAddress:"",
                              dateTime:""
                            },
                            data:{
                              clientId:1,
                              userCode:1,
                              superUser:"",
                              visibleInCases:"",
                              isPhysician:"",
                              defaultPhysician:"",
                              practiseCode:"",
                              addedBy:"",
                              addedTimestamp:"",
                              active:""
                            }
                          }
                          setActivereq.data.clientId            = this.selectedClientID;
                          setActivereq.data.userCode              = resp["data"].userCode;
                          setActivereq.data.defaultPhysician    = '1';
                          setActivereq.data.active              = '0';
                          setActivereq.header.partnerCode = this.logedInUserRoles.partnerCode;
                          setActivereq.header.userCode    = this.logedInUserRoles.userCode;
                          setActivereq.header.functionalityCode    = "USRA-UUE";
                          url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
                          this.httpRequest.httpPostRequests(url, setActivereq).then(respPhy => {
                            console.log('respPhy resp:', respPhy);
                            this.ngxLoader.stop();
                            if (respPhy['data'].defaultPhysician == 1) {
                              emptyClientUserHolder.defaultPhysician = 1;
                              emptyClientUserHolder.active = 0;
                              this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck +1;
                              if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck > 0) {
                                this.notifier.hideAll();
                              }
                            }
                            else{
                              emptyClientUserHolder.defaultPhysician = 0;
                              emptyClientUserHolder.active = 0;
                            }
                            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                              dtInstance.ajax.reload();
                              // Destroy the table first
                              // dtInstance.destroy();
                              // // Call the dtTrigger to rerender again
                              // this.dtTrigger.next();
                            });

                          })
                        }
                        else{
                          var setActivereq = {
                            header:{
                              uuid:"",
                              partnerCode:"",
                              userCode:"",
                              referenceNumber:"",
                              systemCode:"",
                              moduleCode:"CLIM",
                              functionalityCode: "CLTA-UC",
                              systemHostAddress:"",
                              remoteUserAddress:"",
                              dateTime:""
                            },
                            data:{
                              clientId:1,
                              userCode:1,
                              superUser:"",
                              visibleInCases:"",
                              isPhysician:"",
                              defaultPhysician:"",
                              practiseCode:"",
                              addedBy:"",
                              addedTimestamp:"",
                              active:""
                            }
                          }
                          setActivereq.data.clientId            = this.selectedClientID;
                          setActivereq.data.userCode              = resp["data"].userCode;
                          setActivereq.data.defaultPhysician    = '0';
                          setActivereq.data.active              = '0';
                          setActivereq.header.partnerCode = this.logedInUserRoles.partnerCode;
                          setActivereq.header.userCode    = this.logedInUserRoles.userCode;
                          setActivereq.header.functionalityCode    = "USRA-UUE";
                          url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
                          this.httpRequest.httpPostRequests(url, setActivereq).then(respPhy => {
                            console.log('respPhy resp:', respPhy);
                            this.ngxLoader.stop();
                            if (respPhy['data'].defaultPhysician == 1) {
                              emptyClientUserHolder.defaultPhysician = 1;
                              emptyClientUserHolder.active = 0;
                              this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck +1;
                              if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck > 0) {
                                this.notifier.hideAll();
                              }
                            }
                            else{
                              emptyClientUserHolder.defaultPhysician = 0;
                              emptyClientUserHolder.active = 0;
                            }

                          })
                          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                            dtInstance.ajax.reload();
                            // Destroy the table first
                            // dtInstance.destroy();
                            // // Call the dtTrigger to rerender again
                            // this.dtTrigger.next();
                          });
                        }
                        // for (let k = 0; k < this.userShowData.length; k++) {
                        //   this.userShowData[k]
                        //
                        // }
                      }
                      else{
                        console.log("no Data******");

                        var setActivereq = {
                          header:{
                            uuid:"",
                            partnerCode:"",
                            userCode:"",
                            referenceNumber:"",
                            systemCode:"",
                            moduleCode:"CLIM",
                            functionalityCode: "CLTA-UC",
                            systemHostAddress:"",
                            remoteUserAddress:"",
                            dateTime:""
                          },
                          data:{
                            clientId:1,
                            userCode:1,
                            superUser:"",
                            visibleInCases:"",
                            isPhysician:"",
                            defaultPhysician:"",
                            practiseCode:"",
                            addedBy:"",
                            addedTimestamp:"",
                            active:""
                          }
                        }
                        setActivereq.data.clientId            = this.selectedClientID;
                        setActivereq.data.userCode              = resp["data"].userCode;
                        setActivereq.data.active                = '0';
                        setActivereq.data.defaultPhysician      = '1';
                        setActivereq.header.partnerCode = this.logedInUserRoles.partnerCode;
                        setActivereq.header.userCode    = this.logedInUserRoles.userCode;
                        setActivereq.header.functionalityCode    = "USRA-UUE";
                        url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
                        this.httpRequest.httpPostRequests(url, setActivereq).then(respPhy => {
                          console.log('att phys resp resp:', respPhy);
                          this.ngxLoader.stop();
                          if (respPhy['data'].defaultPhysician == 1) {
                            emptyClientUserHolder.defaultPhysician = 1;
                            this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck +1;
                            if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck > 0) {
                              this.notifier.hideAll();
                            }
                            emptyClientUserHolder.active = 0;
                          }
                          else{
                            emptyClientUserHolder.defaultPhysician = 0;
                            emptyClientUserHolder.active = 0;
                          }
                          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                            dtInstance.ajax.reload();
                            // Destroy the table first
                            // dtInstance.destroy();
                            // // Call the dtTrigger to rerender again
                            // this.dtTrigger.next();
                          });

                        })
                      }
                    }
                    // var setActivereq = {
                    //   header:{
                    //     uuid:"",
                    //     partnerCode:"",
                    //     userCode:"",
                    //     referenceNumber:"",
                    //     systemCode:"",
                    //     moduleCode:"CLIM",
                    //     functionalityCode: "CLTA-UC",
                    //     systemHostAddress:"",
                    //     remoteUserAddress:"",
                    //     dateTime:""
                    //   },
                    //   data:{
                    //     clientId:1,
                    //     userCode:1,
                    //     superUser:"",
                    //     visibleInCases:"",
                    //     isPhysician:"",
                    //     defaultPhysician:"",
                    //     practiseCode:"",
                    //     addedBy:"",
                    //     addedTimestamp:"",
                    //     active:""
                    //   }
                    // }
                    // setActivereq.data.clientId  = this.selectedClientID;
                    // setActivereq.data.userCode    = resp["data"].userCode;
                    // setActivereq.data.active    = '0';
                    // url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
                    // this.httpRequest.httpPostRequests(url, setActivereq).then(respAct => {
                    //   console.log('disable resp:', respAct);
                    //   this.ngxLoader.stop();
                    //   if (respAct['data'].active == 1) {
                    //     emptyClientUserHolder.active = 1;
                    //   }
                    //   else{
                    //     emptyClientUserHolder.active = 0;
                    //   }
                    //
                    //
                    // })
                  }


                  //////////////// if New Added/Loaded User is Attending Physician the then////
                  ////////////////Check if its first AP if yes the make it default
                  // const foundRols = resp["data"].roles.some(el => el.name === 'Attending Physician');
                  // console.log('foundRols',foundRols);


                  this.userShowData.push(emptyClientUserHolder)
                  // var temp = '';
                  //   console.log('before',resp.data);
                  // for (let i = 0; i < resp.data.roles.length; i++) {
                  //     temp = resp.data.roles[i]['name']+', '+ temp;
                  // }
                  // resp.data.userRoles  = temp;
                  ///////this.userShowData.push(resp['data']);
                  this.submitted2 = false;
                  this.clientUserForm.reset();
                  this.notifier.hideAll();
                  this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                    // Destroy the table first
                    dtInstance.destroy();
                    // Call the dtTrigger to rerender again
                    this.dtTrigger.next();
                  });

                }
                else{
                  this.ngxLoader.stop();
                  this.notifier.getConfig().behaviour.autoHide = 3000;
                  this.notifier.notify( "error", "Error while saving user CLIENT API");
                }
              })
              // this.userShowData.push(resp["data"])
            }

            // this.ngxLoader.start();

          }
          else{
            this.inValidUseremail = true;
            this.requestData.data.user.email = null;
          }
        })
      }
      else{
        this.inValidUseremail      = false;
      }
      //
    })
    .catch(error => {
      console.log("error EMAIL: ",error);
    });
  }

  loadClieUserinTable(){
    // console.log("this.responseUserNPIEmail",this.responseUserNPIEmail);
    // return;
    this.clientUserForm.disable();
    this.inValidUseremail = false;
  var resp = this.responseUserNPIEmail;
  /////////////// Request To client API
  this.clientUserReq.data.userCode   = resp["data"].userCode;
  this.clientUserReq.data.clientId = this.selectedClientID;
  this.clientUserReq.header.partnerCode    = this.logedInUserRoles.partnerCode;
  this.clientUserReq.header.userCode    = this.logedInUserRoles.userCode;
  this.clientUserReq.header.functionalityCode    = "USRA-AUE";
  let url                         = environment.API_CLIENT_ENDPOINT + 'addclientuser';
  this.httpRequest.httpPostRequests(url,this.clientUserReq).then(response => {
    console.log("--resp Client User",response);

    // this.allUsers.push(resp);
    this.ngxLoader.stop();
    if (typeof response['data'] != "undefined") {
      this.notifier.getConfig().behaviour.autoHide = 3000;
      this.notifier.notify( "success", "User saved Successfully" );
      var emptyClientUserHolder  = {
        active: 0,
        addedBy: 0,
        addedTimestamp: null,
        clientId: 0,
        contactDetails: [],
        defaultPhysician: 0,
        isPhysician: 0,
        practiseCode: "",
        superUser: 0,
        userData: {},
        userCode: 1,
        visibleInCases: 0
      }

      var temporaryContact = [];
      temporaryContact[0]     = "";
      temporaryContact[0]     = "";
      temporaryContact[1]     = "";
      temporaryContact[1]     = "";
      emptyClientUserHolder.userData       = resp["data"];
      emptyClientUserHolder.userCode         = resp["data"].userCode;
      emptyClientUserHolder.clientId       = this.selectedClientID;
      emptyClientUserHolder.contactDetails = temporaryContact;
      console.log('emptyClientUserHolder',emptyClientUserHolder);

      //////////////// if Selected Client is Active then////
      ////////////////Set newly aded user as activ

      //////////////// if Selected Client is Active then////
      ////////////////Set newly aded user as activ
      if (this.updateRequestData.data.active == 1) {
        //////////////// if New Added/Loaded User is Attending Physician the then////
        ////////////////Check if its first AP if yes the make it default
        // const foundRols = resp["data"].userRoles.some(el => el.name === 'Attending Physician');
        if (resp["data"].userRoles.search('Attending Physician')!= -1) {
          if(this.userShowData.length > 0){
            const found = this.userShowData.some(el => el.isDefaultPhysician === 1);
            console.log("FOUNDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD",found);

            if(found == false){
              var setActivereq = {
                header:{
                  uuid:"",
                  partnerCode:"",
                  userCode:"",
                  referenceNumber:"",
                  systemCode:"",
                  moduleCode:"CLIM",
                  functionalityCode: "CLTA-UC",
                  systemHostAddress:"",
                  remoteUserAddress:"",
                  dateTime:""
                },
                data:{
                  clientId:1,
                  userCode:1,
                  superUser:"",
                  visibleInCases:"",
                  isPhysician:"",
                  defaultPhysician:"",
                  practiseCode:"",
                  addedBy:"",
                  addedTimestamp:"",
                  active:""
                }
              }
              setActivereq.data.clientId            = this.selectedClientID;
              setActivereq.data.userCode              = resp["data"].userCode;
              setActivereq.data.defaultPhysician    = '1';
              setActivereq.data.active              = '1';
              setActivereq.header.partnerCode = this.logedInUserRoles.partnerCode;
              setActivereq.header.userCode    = this.logedInUserRoles.userCode;
              setActivereq.header.functionalityCode    = "USRA-UUE";
              let url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
              this.httpRequest.httpPostRequests(url, setActivereq).then(respPhy => {
                console.log('respPhy resp:', respPhy);
                this.ngxLoader.stop();
                if (respPhy['data'].defaultPhysician == 1) {
                  emptyClientUserHolder.defaultPhysician = 1;
                  emptyClientUserHolder.active = 1;
                  this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck +1;
                  if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck > 0) {
                    this.notifier.hideAll();
                  }
                }
                else{
                  emptyClientUserHolder.defaultPhysician = 0;
                  emptyClientUserHolder.active = 1;
                }
                this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                  dtInstance.ajax.reload();
                  // Destroy the table first
                  // dtInstance.destroy();
                  // // Call the dtTrigger to rerender again
                  // this.dtTrigger.next();
                });
                this.responseUserNPIEmail = {}
                this.resetUserForm()

              })
            }
            else{
              var setActivereq = {
                header:{
                  uuid:"",
                  partnerCode:"",
                  userCode:"",
                  referenceNumber:"",
                  systemCode:"",
                  moduleCode:"CLIM",
                  functionalityCode: "CLTA-UC",
                  systemHostAddress:"",
                  remoteUserAddress:"",
                  dateTime:""
                },
                data:{
                  clientId:1,
                  userCode:1,
                  superUser:"",
                  visibleInCases:"",
                  isPhysician:"",
                  defaultPhysician:"",
                  practiseCode:"",
                  addedBy:"",
                  addedTimestamp:"",
                  active:""
                }
              }
              setActivereq.data.clientId            = this.selectedClientID;
              setActivereq.data.userCode              = resp["data"].userCode;
              setActivereq.data.defaultPhysician    = '0';
              setActivereq.data.active              = '1';
              setActivereq.header.partnerCode = this.logedInUserRoles.partnerCode;
              setActivereq.header.userCode    = this.logedInUserRoles.userCode;
              setActivereq.header.functionalityCode    = "USRA-UUE";
              let url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
              this.httpRequest.httpPostRequests(url, setActivereq).then(respPhy => {
                console.log('respPhy resp:', respPhy);
                this.ngxLoader.stop();
                if (respPhy['data'].defaultPhysician == 1) {
                  emptyClientUserHolder.defaultPhysician = 1;
                  emptyClientUserHolder.active = 1;
                  this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck +1;
                  if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck > 0) {
                    this.notifier.hideAll();
                  }
                }
                else{
                  emptyClientUserHolder.defaultPhysician = 0;
                  emptyClientUserHolder.active = 1;
                }
                this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                  dtInstance.ajax.reload();
                  // Destroy the table first
                  // dtInstance.destroy();
                  // // Call the dtTrigger to rerender again
                  // this.dtTrigger.next();
                });
                this.responseUserNPIEmail = {}
                this.resetUserForm()

              })
            }
            // for (let k = 0; k < this.userShowData.length; k++) {
            //   this.userShowData[k]
            //
            // }
          }
          else{
            console.log("no Data******");

            var setActivereq = {
              header:{
                uuid:"",
                partnerCode:"",
                userCode:"",
                referenceNumber:"",
                systemCode:"",
                moduleCode:"CLIM",
                functionalityCode: "CLTA-UC",
                systemHostAddress:"",
                remoteUserAddress:"",
                dateTime:""
              },
              data:{
                clientId:1,
                userCode:1,
                superUser:"",
                visibleInCases:"",
                isPhysician:"",
                defaultPhysician:"",
                practiseCode:"",
                addedBy:"",
                addedTimestamp:"",
                active:""
              }
            }
            setActivereq.data.clientId            = this.selectedClientID;
            setActivereq.data.userCode              = resp["data"].userCode;
            setActivereq.data.active              = '1';
            setActivereq.data.defaultPhysician    = '1';
            setActivereq.header.partnerCode = this.logedInUserRoles.partnerCode;
            setActivereq.header.userCode    = this.logedInUserRoles.userCode;
            setActivereq.header.functionalityCode    = "USRA-UUE";
            let url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
            this.httpRequest.httpPostRequests(url, setActivereq).then(respPhy => {
              console.log('att phys resp resp:', respPhy);
              this.ngxLoader.stop();
              if (respPhy['data'].defaultPhysician == 1) {
                emptyClientUserHolder.defaultPhysician = 1;
                emptyClientUserHolder.active = 1;
                this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck +1;
                if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck > 0) {
                  this.notifier.hideAll();
                }
              }
              else{
                emptyClientUserHolder.defaultPhysician = 0;
                emptyClientUserHolder.active = 1;
              }
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.ajax.reload();
                // Destroy the table first
                // dtInstance.destroy();
                // // Call the dtTrigger to rerender again
                // this.dtTrigger.next();
              });
              this.responseUserNPIEmail = {}
              this.resetUserForm()

            })
          }
        }

      }
      else{
        //////////////// if New Added/Loaded User is Attending Physician the then////
        ////////////////Check if its first AP if yes the make it default
        console.log('resp["data"]',resp["data"]);

        // const foundRols = resp["data"].userRoles.some(el => el.name === 'Attending Physician');
        // console.log("FOUNDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD active 0 out",foundRols);
        if (resp["data"].userRoles.search('Attending Physician')!= -1) {
          if(this.userShowData.length > 0){
            const found = this.userShowData.some(el => el.isDefaultPhysician === 1);
            // this.defaultAccountTypes[m].name.toLowerCase().indexOf('nursing')>-1
            console.log("FOUNDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD active 0",found);

            if(found == false){
              var setActivereq = {
                header:{
                  uuid:"",
                  partnerCode:"",
                  userCode:"",
                  referenceNumber:"",
                  systemCode:"",
                  moduleCode:"CLIM",
                  functionalityCode: "CLTA-UC",
                  systemHostAddress:"",
                  remoteUserAddress:"",
                  dateTime:""
                },
                data:{
                  clientId:1,
                  userCode:1,
                  superUser:"",
                  visibleInCases:"",
                  isPhysician:"",
                  defaultPhysician:"",
                  practiseCode:"",
                  addedBy:"",
                  addedTimestamp:"",
                  active:""
                }
              }
              setActivereq.data.clientId            = this.selectedClientID;
              setActivereq.data.userCode              = resp["data"].userCode;
              setActivereq.data.defaultPhysician    = '1';
              setActivereq.data.active              = '0';
              setActivereq.header.partnerCode = this.logedInUserRoles.partnerCode;
              setActivereq.header.userCode    = this.logedInUserRoles.userCode;
              setActivereq.header.functionalityCode    = "USRA-UUE";
              let url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
              this.httpRequest.httpPostRequests(url, setActivereq).then(respPhy => {
                console.log('respPhy resp:', respPhy);
                this.ngxLoader.stop();
                if (respPhy['data'].defaultPhysician == 1) {
                  emptyClientUserHolder.defaultPhysician = 1;
                  emptyClientUserHolder.active = 0;
                  this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck +1;
                  if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck > 0) {
                    this.notifier.hideAll();
                  }
                }
                else{
                  emptyClientUserHolder.defaultPhysician = 0;
                  emptyClientUserHolder.active = 0;
                }
                this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                  dtInstance.ajax.reload();
                  // Destroy the table first
                  // dtInstance.destroy();
                  // // Call the dtTrigger to rerender again
                  // this.dtTrigger.next();
                });
                this.responseUserNPIEmail = {}
                this.resetUserForm()

              })
            }
            else{
              var setActivereq = {
                header:{
                  uuid:"",
                  partnerCode:"",
                  userCode:"",
                  referenceNumber:"",
                  systemCode:"",
                  moduleCode:"CLIM",
                  functionalityCode: "CLTA-UC",
                  systemHostAddress:"",
                  remoteUserAddress:"",
                  dateTime:""
                },
                data:{
                  clientId:1,
                  userCode:1,
                  superUser:"",
                  visibleInCases:"",
                  isPhysician:"",
                  defaultPhysician:"",
                  practiseCode:"",
                  addedBy:"",
                  addedTimestamp:"",
                  active:""
                }
              }
              setActivereq.data.clientId            = this.selectedClientID;
              setActivereq.data.userCode              = resp["data"].userCode;
              setActivereq.data.defaultPhysician    = '0';
              setActivereq.data.active              = '0';
              setActivereq.header.partnerCode = this.logedInUserRoles.partnerCode;
              setActivereq.header.userCode    = this.logedInUserRoles.userCode;
              setActivereq.header.functionalityCode    = "USRA-UUE";
              let url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
              this.httpRequest.httpPostRequests(url, setActivereq).then(respPhy => {
                console.log('respPhy resp:', respPhy);
                this.ngxLoader.stop();
                if (respPhy['data'].defaultPhysician == 1) {
                  emptyClientUserHolder.defaultPhysician = 1;
                  emptyClientUserHolder.active = 0;
                  this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck +1;
                  if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck > 0) {
                    this.notifier.hideAll();
                  }
                }
                else{
                  emptyClientUserHolder.defaultPhysician = 0;
                  emptyClientUserHolder.active = 0;
                }

              })
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.ajax.reload();
                // Destroy the table first
                // dtInstance.destroy();
                // // Call the dtTrigger to rerender again
                // this.dtTrigger.next();
              });
              this.responseUserNPIEmail = {}
              this.resetUserForm()
            }
            // for (let k = 0; k < this.userShowData.length; k++) {
            //   this.userShowData[k]
            //
            // }
          }
          else{
            console.log("no Data******");

            var setActivereq = {
              header:{
                uuid:"",
                partnerCode:"",
                userCode:"",
                referenceNumber:"",
                systemCode:"",
                moduleCode:"CLIM",
                functionalityCode: "CLTA-UC",
                systemHostAddress:"",
                remoteUserAddress:"",
                dateTime:""
              },
              data:{
                clientId:1,
                userCode:1,
                superUser:"",
                visibleInCases:"",
                isPhysician:"",
                defaultPhysician:"",
                practiseCode:"",
                addedBy:"",
                addedTimestamp:"",
                active:""
              }
            }
            setActivereq.data.clientId            = this.selectedClientID;
            setActivereq.data.userCode              = resp["data"].userCode;
            setActivereq.data.active                = '0';
            setActivereq.data.defaultPhysician      = '1';
            setActivereq.header.partnerCode = this.logedInUserRoles.partnerCode;
            setActivereq.header.userCode    = this.logedInUserRoles.userCode;
            setActivereq.header.functionalityCode    = "USRA-UUE";
            let url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
            this.httpRequest.httpPostRequests(url, setActivereq).then(respPhy => {
              console.log('att phys resp resp:', respPhy);
              this.ngxLoader.stop();
              if (respPhy['data'].defaultPhysician == 1) {
                emptyClientUserHolder.defaultPhysician = 1;
                this.enableCompleteDefaultCheck = this.enableCompleteDefaultCheck +1;
                if (this.enableCompleteSuperCheck > 0 && this.enableCompleteDefaultCheck > 0) {
                  this.notifier.hideAll();
                }
                emptyClientUserHolder.active = 0;
              }
              else{
                emptyClientUserHolder.defaultPhysician = 0;
                emptyClientUserHolder.active = 0;
              }
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.ajax.reload();

              });
              this.responseUserNPIEmail = {}
              this.resetUserForm()

            })
          }
        }

      }

      this.userShowData.push(emptyClientUserHolder);
      // var temp = '';
      //   console.log('before',resp.data);
      // for (let i = 0; i < resp.data.roles.length; i++) {
      //     temp = resp.data.roles[i]['name']+', '+ temp;
      // }
      // resp.data.userRoles  = temp;
      //////this.userShowData.push(resp['data']);
      this.submitted2 = false;
      this.clientUserForm.reset();
      this.notifier.hideAll();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        // Destroy the table first
        dtInstance.destroy();
        // Call the dtTrigger to rerender again
        this.dtTrigger.next();
      });
      this.responseUserNPIEmail = {}
      this.resetUserForm()

    }
    else{
      this.ngxLoader.stop();
      this.notifier.getConfig().behaviour.autoHide = 3000;
      this.notifier.notify( "error", "Error while saving user CLIENT API");
    }
  })


  }

  checkAccountNumberUpdate(){
    console.log("updateAccountNumber",this.updateAccountNumber);
    var requestD = {
      header:{
        uuid:"",
        partnerCode:"",
        userCode:"",
        referenceNumber:"",
        systemCode:"",
        moduleCode:"CLIM",
        functionalityCode: "CLTA-UC",
        systemHostAddress:"",
        remoteUserAddress:"",
        dateTime:""
      },
      data: {
        userCode:this.updateAccountNumber
      }
    }
    /////////////// defaultAccountTypes
    var url                = environment.API_CLIENT_ENDPOINT + 'checkaccountnumber';
    requestD.header.partnerCode = this.logedInUserRoles.partnerCode;
    requestD.header.userCode    = this.logedInUserRoles.userCode;
    requestD.header.functionalityCode    = "CLTA-UC";
    this.httpRequest.httpPutRequests(url,requestD).then(resp => {
      console.log('Account Number',resp)
      if (resp['data'] == true) {
        console.log("true");

        if (this.updateAccountNumberOLD == this.updateAccountNumber) {
          this.accNumberExist = false;
        }
        else{
          this.accNumberExist = true;
        }
      }
      else{
        this.accNumberExist = false;
      }
      // this.defaultAccountTypes = resp;
    })
  }
  formatePhoneUS(e){
    // console.log("eeee",e);

    var x = this.requestData.data.user.phone.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
    this.requestData.data.user.phone = !x[2] ? x[1] : '(' + x[1] + ')' + x[2] + (x[3] ? '-' + x[3] : '')

  }
  formateFaxUScusers(e){
    // console.log("eeee",e);

    var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
    e.target.value = !x[2] ? x[1] : '(' + x[1] + ')' + x[2] + (x[3] ? '-' + x[3] : '')

  }

  roleIsDoctor() {

    var value = this.requestData.data.user.roles;
    // console.log("value",value);
    if (value.length > 0) {
      for (let i = 0; i < value.length; i++) {

        if(value[i].isPhysician == true) {
          // console.log('yes2');
          this.clientUserForm.get('clientUserNpi').setValidators([Validators.required])
          this.clientUserForm.controls["clientUserNpi"].updateValueAndValidity();
          return;

        } else {
          // console.log('no2');
          this.clientUserForm.get('clientUserNpi').clearValidators();
          this.clientUserForm.controls["clientUserNpi"].updateValueAndValidity();


        }

      }
    }
    else{
      // console.log("zero");
      this.clientUserForm.get('clientUserNpi').clearValidators();
      this.clientUserForm.controls["clientUserNpi"].updateValueAndValidity();

    }
  }

  scrollTo(el: Element): void {
    if (el) {
      el.scrollIntoView({ behavior: 'smooth', block: 'center' });
    }
  }

  scrollToError(): void {
    const firstElementWithError = document.querySelector('.ng-invalid[formControlName]');
    console.log("aa:", firstElementWithError);

    this.scrollTo(firstElementWithError);
  }

  backToManage(){
    $('#cancelModal').modal('hide');
    this.router.navigate(['all-clients']);
  }
  getLookups(){
    // this.ngxLoader.start();
    return new Promise((resolve, reject) => {
      var accType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=CLIENT_TYPE_CACHE&partnerCode=sip').then(response =>{
        return response;
      })
      var priori = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=PRIORITY_CACHE&partnerCode=sip').then(response =>{
        return response;
      })
      var allcountry = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=COUNTRY_CACHE&partnerCode=sip').then(response =>{
        return response;
      })
      var repFormat = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=ATTACHMENT_FILE_TYPE_CACHE&partnerCode=sip').then(response =>{
        return response;
      })
      var deliveryM = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=DELIVERY_METHOD_CACHE&partnerCode=sip').then(response =>{
        return response;
      })
      var attachmenttype = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=ATTACHMENT_TYPE_CACHE&partnerCode=sip').then(response =>{
        return response;
      })
      var fileType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=ATTACHMENT_FILE_TYPE_CACHE&partnerCode=sip').then(response =>{
        return response;
      })
      // var uri = environment.API_CLIENT_ENDPOINT+ 'parentclient';
      // this.parentCLientReq.clientId = 1; /////// logedin users client id
      // var parentCli = this.clientService.getParentClient(uri, this.parentCLientReq).then(response =>{
      //   return response;
      // })
      var parentCli = [0]

      // var allRoles = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=ATTACHMENT_FILE_TYPE_CACHE&partnerCode=sip').then(response =>{
      //   return response;
      // })
      var allpostNom = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=POSTNOMINAL_CACHE&partnerCode=sip').then(response =>{
        return response;
      })
      var allTitle = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=TITLE_CACHE&partnerCode=sip').then(response =>{
        return response;
      })
      var reqtype = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=REQUISITION_TYPE_CACHE&partnerCode=sip').then(response =>{
        return response;
      })



      forkJoin([accType, priori, allcountry, repFormat, attachmenttype, fileType, parentCli, deliveryM, allpostNom,allTitle, reqtype]).subscribe(allLookups => {
        console.log("allLookups",allLookups);
        this.defaultAccountTypes     = this.sortPipe.transform(allLookups[0], "asc", "name");
        this.defaultAccountPriority  = this.sortPipe.transform(allLookups[1], "asc", "priorityName");
        this.allCountries            = this.sortPipe.transform(allLookups[2], "asc", "countryName");
        this.reportFormat            = this.sortPipe.transform(allLookups[3], "asc", "attachmentFileTypeName");
        this.defaultAttType          = this.sortPipe.transform(allLookups[4], "asc", "attachmentTypeName");
        this.defaultFileType         = this.sortPipe.transform(allLookups[5], "asc", "attachmentFileTypeName");
        this.defaultPCOrganization   = this.sortPipe.transform(allLookups[6]['data'], "asc", "name");
        this.deliverMethod           = this.sortPipe.transform(allLookups[7], "asc", "name");
        this.dPostNominal            = this.sortPipe.transform(allLookups[8], "asc", "value");
        this.allTittles             = this.sortPipe.transform(allLookups[9], "asc", "titleValue");
        this.defaultReqType         = this.sortPipe.transform(allLookups[10], "asc", "requisitionTypeName");

        for (let index = 0; index < allLookups[9]['length']; index++) {
          if (allLookups[9][index]['titleValue'] == 'Mr.' || allLookups[9][index]['titleValue'] == 'mr.' || allLookups[9][index]['titleValue'] == 'MR.') {
            this.requestData.data.user.title = allLookups[9][index]['titleId'];
            // this.changeCountry()
          }

        }
        resolve();
      })
    })

  }
  accountTypeChange(){
    // console.log("ClientTypeHolder",this.ClientTypeHolder);

  }
  changeCountry(){
    if (this.updateCountry != null) {
      var cid = this.updateCountry;
      this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=STATE_CACHE&partnerCode=sip&id='+cid).then(response =>{
        // console.log("states",response);
        // this.updateState = null;
        this.allProvinces = this.sortPipe.transform(response, "asc", "stateName");
      })
    }

  }

  changeState(){
    if (this.updateState != null) {
      var cid = this.updateState;
      this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=CITY_CACHE&partnerCode=sip&id='+cid).then(response =>{
        // console.log("states",response);
        // this.updateCity = null;
        this.allCities = this.sortPipe.transform(response, "asc", "cityName");
      })
    }

  }

  addRow(index){
    var temp  = {
      ContactNo  : "",
      contactTypeId: "1",
      ContactID  : "",
      Fax        : "",
      FaxID      : "3",
      ContactID1  : "",
      Email      : "",
      EmailID    : "2",
      ContactID2  : "",
      Default    : "",
      disableButton: 1
    }
    this.conactHolderUpdate.push(temp);
    // var len = this.conactHolderUpdate.length;

  }
  remRow(index){

    this.conactHolderUpdate.splice(index,1);
  }
  ToggleContactButton(e, index, type){
    if (type == 'fax' || type == 'phone') {
      var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
      e.target.value = !x[2] ? x[1] : '(' + x[1] + ')' + x[2] + (x[3] ? '-' + x[3] : '')
    }
    // console.log('e',e.target.value);
    // console.log('index',index);
    // console.log('type',type);
    // console.log('phone',$('#PHONE-'+index).val());
    // console.log('phone',$('#FAX-'+index).val());
    // console.log('EMAIL',$('#EMAIL-'+index).val());
    if ($('#PHONE-'+index).val() == '' && $('#FAX-'+index).val() == '' && $('#EMAIL-'+index).val() == '') {
      if (this.conactHolderUpdate.length > 1) {
        // console.log('--------',$('#CHECKBOX-'+index));

        $('#CHECKBOX-'+index).prop("checked", false);
        // $('#CHECKBOX-'+index).removeAttr('checked');
      }
      else{
        $('#BTN-'+index).attr('disabled','true');
      }

      $('#EMAILERROR-'+index).css("display","none");
      // this.conactHolderUpdate[index].checkEnalbe = 0;
      $('#CHECKBOX-'+index).attr('disabled','true')
    }
    else{
      if ($('#PHONE-'+index).val() == '' && $('#FAX-'+index).val() == '' && $('#EMAIL-'+index).val() != '') {
        var regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        var vemail = regexp.test($('#EMAIL-'+index).val());
        console.log("$('#EMAIL-'+index).val()",$('#EMAIL-'+index).val());

        console.log('vemail',vemail);

        if (vemail == false) {
          $('#EMAILERROR-'+index).css("display","inline-block");
          if (this.conactHolderUpdate.length > 1) {

          }
          else{
            $('#BTN-'+index).attr('disabled','true');
          }
          $('#CHECKBOX-'+index).attr('disabled','true')
        }
        else{
          $('#EMAILERROR-'+index).css("display","none");
          $('#BTN-'+index).removeAttr('disabled');
          // this.conactHolderUpdate[index].checkEnalbe = 1;
          $('#CHECKBOX-'+index).removeAttr('disabled')
        }
      }
      else if ($('#PHONE-'+index).val() != '' && $('#FAX-'+index).val() != '' && $('#EMAIL-'+index).val() != '') {
        var regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        var vemail = regexp.test($('#EMAIL-'+index).val());
        // console.log('vemail',vemail);
        if (vemail == false) {
          $('#EMAILERROR-'+index).css("display","inline-block");
          if (this.conactHolderUpdate.length > 1) {

          }
          else{
            $('#BTN-'+index).attr('disabled','true');
          }
          $('#CHECKBOX-'+index).attr('disabled','true')
        }
        else{
          $('#EMAILERROR-'+index).css("display","none");
          $('#BTN-'+index).removeAttr('disabled');
          // this.conactHolderUpdate[index].checkEnalbe = 1;
          $('#CHECKBOX-'+index).removeAttr('disabled')
        }
      }
      else if ($('#PHONE-'+index).val() == '' || $('#FAX-'+index).val() == '' && $('#EMAIL-'+index).val() != '') {
        var regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        var vemail = regexp.test($('#EMAIL-'+index).val());
        // console.log('vemail',vemail);
        if (vemail == false) {
          $('#EMAILERROR-'+index).css("display","inline-block");
          if (this.conactHolderUpdate.length > 1) {

          }
          else{
            $('#BTN-'+index).attr('disabled','true');
          }
          $('#CHECKBOX-'+index).attr('disabled','true')
        }
        else{
          $('#EMAILERROR-'+index).css("display","none");
          $('#BTN-'+index).removeAttr('disabled');
          $('#CHECKBOX-'+index).removeAttr('disabled')
        }
      }
      else if ($('#PHONE-'+index).val() != '' || $('#FAX-'+index).val() != '' && $('#EMAIL-'+index).val() == '') {
        console.log('here');

        $('#BTN-'+index).removeAttr('disabled');
        $('#EMAILERROR-'+index).css("display","none");
        $('#CHECKBOX-'+index).removeAttr('disabled')
      }
    }

  }
  resetUserForm(){
    this.clientUserForm.reset();
    this.submitted2                    = false;
    console.log("$('#collapse-div')",$('#collapse-div'));

    $('#collapse-div').collapse('hide')
    $('#cancelModalUser').modal('hide')
  }

  clientChanged(){
    console.log("client Changed-----",this.clientSearchString);
    this.updateRequestData.data.parentClientId = this.clientSearchString;
    // this.saveRequest.data.clientId = this.clientSearchString.clientId;


  }


}
