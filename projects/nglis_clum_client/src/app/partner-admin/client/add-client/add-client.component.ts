import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { environment } from '../../../../../../../src/environments/environment';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { AppSettings } from '../../../services/_services/app.setting';
import { HttpRequestsService } from '../../../services/_services/http-requests/http-requests.service';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import Swal from 'sweetalert2'
declare var $ :any;
import { NotifierService } from 'angular-notifier';
import { UserApiEndpointsService } from '../../../services/users/user-api-endpoints.service';
import { ClientApiEndpointsService } from '../../../services/client/client-api-endpoints.service';
import { GlobalApiCallsService } from '../../../services/gloabal/global-api-calls.service';
import { concat, Observable, of, Subject, forkJoin } from "rxjs";
import { SortPipe } from "../../../pipes/sort.pipe";
import { GlobalySharedModule } from './../../../../../../../src/app/sharedmodules/globaly-shared/globaly-shared.module';
import { UrlGuard } from './../../../../../../../src/services/gaurd/url.guard'
import * as CryptoJS from 'crypto-js';

@Component({
  selector   : 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls  : ['./add-client.component.css'],
  host       : {
    class    :'mainComponentStyle'
  }
})
export class AddClientComponent implements OnInit {
  imagePayLoad           ;
  imageBase64            = '';
  submitted              = false;
  addClientTabPan        = true;
  clientUserTabPan       = false;
  rowCount               = 0;
  myHtml                 ;
  conactHolder           = [];
  conactHolderUpdate     = [];
  contactData            = false;
  tableSize              ;
  setCross               ;
  userShowData           = [];
  appendIcons            ;
  deleteuserCode           ;
  deliverTypeModel       ;
  ///////// addd and remove row from Contact table
  public fieldArray     : Array<any> = [];
  public newAttribute   : any = {};
  ///////// addd and remove row from Contact table
  clientUserTable;
  public clientForm      : FormGroup;
  public clientUserForm  : FormGroup;
  public contactForm     : FormGroup;
  allCountries           ;
  allProvinces           ;
  allCities              ;
  // defaultAccountTypes    = AppSettings.defaultAccountTypes;
  defaultAccountPriority ;
  // defaultAccountPriority = AppSettings.defaultAccountPriority;
  defaultPCOrganization  ;
  // defaultReqType         = AppSettings.defaultReqType;
  // defaultAttType         = AppSettings.defaultAttType;
  // defaultFileType        = AppSettings.defaultFileType;
  deliverMethod          ;
  reportFormat           ;
  // deliverMethod          = environment.deliverMethod;
  defaultClientUserRole  = AppSettings.defaultClientUserRole;
  defaultAttType         ;
  // defaultAttType         = [
  //   {attachmentTypeId: 1,  name: 'Insurance Card'},
  //   {attachmentTypeId: 2,  name: 'CBC'},
  //   {attachmentTypeId: 3,  name: 'Clinical notes'}
  // ];
  defaultFileType        ;
  // defaultFileType        = [
  //   {attachmentFileTypeId: 1,  name: 'pdf'},
  //   {attachmentFileTypeId: 2,  name: 'jpg'},
  //   {attachmentFileTypeId: 3,  name: 'jpeg'},
  //   {attachmentFileTypeId: 4,  name: 'png'},
  //   {attachmentFileTypeId: 5,  name: 'docx'}
  // ];
  defaultAccountTypes    ;
  // defaultAccountTypes    = [
  //   {clientTypeId: 1,  name: 'Hematology'},
  //   {clientTypeId: 2,  name: 'Oncology'},
  //   {clientTypeId: 3,  name: 'Obstetrics & Gynecology'},
  //   {clientTypeId: 4,  name: 'Urology'},
  //   {clientTypeId: 5,  name: 'Dermatopathology'},
  //   {clientTypeId: 6,  name: 'Gastroenterology'},
  //   {clientTypeId: 7,  name: 'Clinical Laboratory'},
  //   {clientTypeId: 8,  name: 'General Care (Practice)'},
  //   {clientTypeId: 9,  name: 'Nursing Homes'},
  //   {clientTypeId: 10, name: 'Surgical Centers'}
  // ];
  defaultReqType          ;
  // defaultReqType         = [
  //   {formId: 1,  name: 'Hematology', addedBy:1, addedTimestamp:null, updatedBy:null, updatedTimestamp:null},
  //   {formId: 2,  name: 'Tumor', addedBy:1, addedTimestamp:null, updatedBy:null, updatedTimestamp:null}
  // ];
  contactFormat          : any = [];
  tempContact            : any = {};
  tempDeliver            : any = [];
  tempDev                : any = {};
  saveDataReqType        : any = [];
  saveDataReqTypeTemp    : any = {
    // formId: 1,
    // addedBy:1,
    // addedTimestamp:null,
    // updatedBy:null,
    // updatedTimestamp:null
  };


  saveDataClientType     : any = [];
  saveDataClientTypeTemp : any = {
    // attachmentTypeId : null
  };

  saveDataReqAttType     : any = [];
  saveDataReqAttTypeTemp : any = {
    // attachmentTypeId : null
  };

  saveDataReqFIleType     : any = [];
  saveDataReqFIleTypeTemp : any = {
    // attachmentFileTypeId : null
  };
  saveDataRequistionTemp: any = {};
  saveDataRequistion    : any = [];

  requisitionDTOTEMP     ;
  AttachmentFileHolder   ;
  ClientTypeHolder       ;
  AttachmentTypeHolder   ;

  //////////////// Update Request Selects Models
  UpdateFormClientTypeHolder       ;
  UpdateFormAttachmentFileHolder   ;
  UpdateFormAttachmentTypeHolder   ;
  updateRequisitionDTOTEMP         ;
  deliveryMethodUpdate             ;
  updateSRFname                    ;
  updateSRLname                    ;
  updateSREmail                    ;
  updateSRContact                  ;
  contactLength                    = 1;


  public newReqTemp      : any = {
    formId: 1,
    name: 'Hematology',
    addedBy:1,
    addedTimestamp:null,
    updatedBy:null,
    updatedTimestamp:null
  };

  public saveClientData  : any = {
    otherData            : [],
    contactInfo          : [],
    imagePayload         : [],
    imageBase64          : '',
    draftFlag            : 0
  };
  public SaveCUserData  : any = {
    clientUserData      : []
  }
  public saveData       : any = {
    ClientData          : [],
    cleintUserData      : []
  }

  public requestData    : any = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"CLIM",
      functionalityCode:"",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data: {
      file                : [],
      // clientId: 111,
      createdBy: 1,
      accountNumber: "",
      partnerCode: 1,
      salesRepresentativeId: '',
      parentClientId: null,
      name: "",
      defaultPriority: null,
      logo: "",
      addressId: null,
      defaultReportFormatId: 1,
      showWordFile: 0,
      encryptionCode: "",
      allowedFilesize: "",
      notes: "",
      active: 3,
      AddressDetailsDto: {
        addressId: null,
        street: "",
        suiteFloorBuilding: "",
        city: null,
        zip: "",
        stateId: null,
        stateOther: "",
        countryId: 5,
        createdBy: 1,
        createdTimestamp: null,
        updatedBy: null,
        updatedTimestamp: null,
        moduleCode: "CLTM"
      },
      ContactDto: [],
      ClientRequisitionTypeDto: [],
      DeliveryMethodDto: [],
      AttachmentTypeDto: [],
      AttachmentFileTypeDto: [],
      ClientTypeDto: []

    }

  }


public updateRequestData : any = {
  // clientId: 1,
  // accountNumber: "11132312432423",
  // partnerCode: 0,
  // salesRepresentativeId: 1,
  // parentClientId: 1,
  // name: "test cliesdfnt",
  // defaultPriority: 1,
  // logo: "F:\\files\\NGLIS\\ClientFiles\\AddUserError.png",
  // addressId: 11,
  // defaultReportFormatId: 1,
  // showWordFile: 1,
  // encryptionCode: "abcdef1223",
  // allowedFilesize: 12,
  // notes: "asdsadasdsadasdasdas",
  // active: 1,
  // AddressDetailsDto: {
  //   addressId: 11,
  //   stateId: 11,
  //   countryId: 11,
  //   createdBy: 1,
  //   createdTimestamp: "2020-09-07T15:05:14.459+00:00"
  // },
  // ContactDto: [{
  //   contactTypeId: 10391,
  //   value: "2",
  //   createdBy: 0,
  //   updatedBy: 0,
  //   moduleCode: "CL",
  //   defaultContact: 0
  // }, {
  //   contactTypeId: 10392,
  //   value: "2",
  //   createdBy: 0,
  //   updatedBy: 0,
  //   moduleCode: "CL",
  //   defaultContact: 0
  // }, {
  //   contactTypeId: 10393,
  //   value: "2",
  //   createdBy: 0,
  //   updatedBy: 0,
  //   moduleCode: "CL",
  //   defaultContact: 0
  // }, {
  //   contactTypeId: 10394,
  //   value: "1",
  //   createdBy: 0,
  //   updatedBy: 0,
  //   moduleCode: "CL",
  //   defaultContact: 0
  // }, {
  //   contactTypeId: 10395,
  //   value: "1",
  //   createdBy: 0,
  //   updatedBy: 0,
  //   moduleCode: "CL",
  //   defaultContact: 0
  // }, {
  //   contactTypeId: 10396,
  //   value: "1",
  //   createdBy: 0,
  //   updatedBy: 0,
  //   moduleCode: "CL",
  //   defaultContact: 0
  // }],
  // ClientRequisitionTypeDto: [{
  //   formId: 1,
  //   clientId: 1,
  //   addedBy: 0
  // },
  // {
  //   formId: 2,
  //   clientId: 1,
  //   addedBy: 0
  // }],
  // DeliveryMethodDto: [{
  //   deliveryMethodId: 1,
  //   clientId: 1,
  //   id: 1
  // }, {
  //   deliveryMethodId: 2,
  //   clientId: 1,
  //   id: 2
  // }],
  // AttachmentTypeDto: [{
  //   attachmentTypeId: 2,
  //   clientId: 1
  // }],
  // AttachmentFileTypeDto: [{
  //   attachmentFileTypeId: 2,
  //   clientId: 1
  // }],
  // ClientTypeDto: [{
  //   clientTypeId: 2,
  //   clientId: 1
  // }]
}



public clientUserReq   : any = {
  clientId          :"",
  userCode            :"",
  superUser         :"",
  visibleInCases    :"",
  isPhysician       :"",
  defaultPhysician  :"",
  practiseCode      :"",
  addedBy           :"",
  addedTimestamp    :""
}

  swalStyle              : any;
  updateAccountNumber;
  updateAccountNumberOLD;
  updateAccountName;
  updateStreet;
  updateCountry;
  updateState;
  updateCity;
  updatesuiteFloorBuilding;
  updateZip;
  updateSalesRepresentativeId;
  updateShowWordFile;
  updateEncryptionCode;
  updateAllowedFilesize;
  updateAccountNotes  ;
  contactEmailValidity = true;
  contactRowValid        = false;
  contactTablePhone;
  contactTableFax;
  disabledSalesRepFields = true;

  encryptionCodeRequired = false;

  addedClientID     ;
  // encryptionCodeRequired = false;
  // addedClientID           ;

  partnerUsersID         : any[];

  public PUsersRequest: any = {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode  : "",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 : {
      userCodes            :[]
    }
  }
  public getClientUsers  : any ={
    page : 1,
    size : 4
  }

  // defaultRoles           : any = {};
  public allRolesReq     =
  {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode  : "",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 :{
      roleType:          1
    }
  }
  public UserRequestData     : any = {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode  : "",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data: {
      partnerCode          : 1,
      user               : {
        userCode           : 0,
        displayCode      : "",
        firstName        : "",
        lastName         : "",
        username         : "",
        password         : "",
        email            : "",
        phone            : "",
        photo            : null,
        title            : null,
        npi              : null,
        postNominal      : null,
        userType         : 2,
        active           : true,
        agreementActive  : true,
        accessCode       : "",
        isDoctor         : false,
        createdBy        :1,
        createdTimestamp :"2020-08-28T19:10:37.000+00:00",
        roles            :[]
      },
      createdBy          :1
    }
  }

  public userNameReq     =
  {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode  : "",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 : {
      username           : '',
      partnerCode          : 1
    }
  }

  selectedPostNominal             ;
  dPostNominal           = [
    {id: 1,          name: 'MD'},
    {id: 2,          name: 'PHD'},
    {id: 3,          name: 'MBBCH'},
    {id: 4,          name: 'SH(ASCPi)'},
    {id: 5,          name: 'CBAP'}
  ];
  UName                  ;
  FName                  ;
  LName                  ;
  defaultRoles           ;
  validUsername          = true;
  inValidUsername        = false;
  inValidUsernpi         = false;
  inValidUseremail       = false;

  public deleteClientUserData = {
  clientId:"",
  userCode:"",
  }
  selectedClientID       ;

  public updateClientUser : any = {
  clientId:"1",
  userCode:"",
  superUser:"",
  visibleInCases:"",
  isPhysician:"",
  defaultPhysician:"",
  practiseCode:"",
  addedBy:"",
  addedTimestamp:""
  }

  public updateclientusercontact : any = {
    contactTypeId:"",
    value:"",
    updatedBy:"",
    updatedTimestamp:"",
    moduleCode:"",
    defaultContact:"",
    clientId:"",
    userCode:""
  }
  public userByNpi: any = {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode  : "",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 : {
      npi                : "",
      email              : ""
    }
  }
  validProvidedEmail    = false;
  accNumberExist        = false
  addedClientAcc        ;
  clientIdForUsers      ;


  clientActiveToggle;
  public clientStatus   : any ={
    clientStatus : 0,
    clientId     : 1
  }

  validPartnerUsers     : any = [];
  enableCompleteSuperCheck    = 0;
  enableCompleteDefaultCheck  = 0;

  public disableUserReq  =
  {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode  : "",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 : {
      userCode             : 1,
      status             : false
    }
  }

  public salesRepReq     =
  {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode  : "",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 : ""
  }
  defaultSalesReps       : any;

  public parentCLientReq : any = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"CLIM",
      functionalityCode:"",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:      {
      clientId:1515,
      // userCodes:[ 1165,1210,1243] //opt
    }
  }
  public logedInUserRoles :any = {};
  public ContactObject = {
    ContactNo : "",
    Fax       : "",
    Email     : "",
    Default   : true,
    Button    : 1,
    checkEnalbe: 1
  }
  contactTableData : any = [];
  clientsDropdown  : any = [];
  clientSearchString          ;
  clientLoading               : any;
  searchTerm$ = new Subject<string>();


constructor(
  private formBuilder  : FormBuilder,
  private http         : HttpClient,
  private router       : Router ,
  private sanitizer    : DomSanitizer,
  private httpRequest  : HttpRequestsService,
  private ngxLoader    : NgxUiLoaderService,
  private  notifier    : NotifierService,
  private userService  : UserApiEndpointsService,
  private clientService: ClientApiEndpointsService,
  private globalService: GlobalApiCallsService,
  private sortPipe     :SortPipe,
  private rbac         : UrlGuard,
  private encryptDecrypt : GlobalySharedModule
) {

  this.rbac.checkROle('CLTA-AC');

  this.notifier        = notifier;
  this.swalStyle       =  Swal.mixin({
    customClass : {
      confirmButton : 'btn btn-success my-swal-success',
      cancelButton  : 'btn btn-danger'
    },
    buttonsStyling  : true
  })
}

ngOnInit(): void {
  this.contactTableData.push(this.ContactObject)
  let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
var originalText = bts.toString(CryptoJS.enc.Utf8);
this.logedInUserRoles = JSON.parse(originalText)
  // this.logedInUserRoles.userName    = "snazirkhanali";
  // this.logedInUserRoles.partnerCode = 1;
  this.ngxLoader.start();
  this.getLookups().then(lookupsLoaded => {
    });
  // this.notifier.notify( "success", "Client saved Successfully" );
  console.log('  this.notifier.notify',  this.notifier);

  // this.notifier.notify( "success", "Client saved Successfully" );
  /////////  ROles form // DB
  var url        = environment.API_ROLE_ENDPOINT + 'allroles';
  this.allRolesReq.header.partnerCode = this.logedInUserRoles.partnerCode;
  this.allRolesReq.header.userCode    = this.logedInUserRoles.userCode;
  this.allRolesReq.header.functionalityCode    = "CLTA-MC";
  this.httpRequest.httpPutRequests(url, this.allRolesReq).then(resp => {
    console.log('resp Role',resp)
    this.defaultRoles = resp.data;
  })



  /////////////////// get sales representatives
  this.ngxLoader.start();
  var url                = environment.API_USER_ENDPOINT + 'salesrepresentatives';
  this.salesRepReq.header.userCode    = this.logedInUserRoles.userCode;
  this.salesRepReq.header.partnerCode = this.logedInUserRoles.partnerCode;
  this.salesRepReq.header.functionalityCode    = 'CLTA-MC';

  // this.userService.getSalesRepresentatives(url, this.salesRepReq).then(resp => {
  this.httpRequest.httpPutRequests(url, this.salesRepReq).then(resp => {
    console.log('defaultSalesReps',resp)
    this.defaultSalesReps = resp['data'];
    this.ngxLoader.stop();
  })
  // console.log("after Sales Rep||||||||||||||||||||||||||");

  this.clientService.search(this.searchTerm$)
    .subscribe(results => {
      console.log("results",results);
      this.clientsDropdown = results['data'];
      this.clientLoading = false;
    },
      error => {
        this.ngxLoader.stop();
        this.notifier.notify( "error", "Error while fetching clients backend connect aborted" );
        this.clientLoading = false;
        return;
        // console.log("error",error);
      });



  // var url               = environment.API_USER_ENDPOINT + 'save';
  this.clientForm       = this.formBuilder.group({
    accountType         : ['', [Validators.required]],
    accountId           : ['', [Validators.required]],
    clientName          : ['', [Validators.required]],
    priority            : ['', [Validators.required]],
    pcOrganization      : [''],
    // clientLogo          : [''],
    // fileSource          : [''],
    notes               : [''],
    streetAddress       : ['', [Validators.required]],
    building            : [''],
    zip                 : ['', [Validators.required]],
    country             : ['', [Validators.required]],
    state               : ['', [Validators.required]],
    city                : ['', [Validators.required]],
    internalUser        : ['', [Validators.required]],
    srFirstName         : [''],
    srLastName          : [''],
    srEmail             : [''],
    srContact           : [''],
    reportFormat        : ['', [Validators.required]],
    wordFile            : [''],
    // delivery            : this.formBuilder.array([]),
    delivery            : this.formBuilder.array([], [Validators.required]),
    encryption          : [''],
    reqType             : ['', [Validators.required]],
    fileSize            : ['', [Validators.required]],
    attType             : ['', [Validators.required]],
    fileType            : ['', [Validators.required]],
    clientFax           : ['', [Validators.pattern(/^\(\d{3}\)\s\d{3}-\d{4}$/)]],
    clientPhone         : ['', [Validators.pattern(/^\(\d{3}\)\s\d{3}-\d{4}$/)]],
    // check            : ['', [Validators.required]]
    activedeactive      : ['']
  });

  this.clientUserForm  = this.formBuilder.group({
    clientUserRole     : ['', [Validators.required]],
    clientUserNpi      : ['', [Validators.required]],
    clientUserEmail    : ['', [Validators.required, Validators.email]],
    clientUserPNominal : ['', [Validators.required]],
    clientUserTitle    : ['', [Validators.required]],
    clientUserFName    : ['', [Validators.required]],
    clientUserLName    : ['', [Validators.required]],
    clientUserUName    : ['', [Validators.required]],
    clientUserContact  : ['', [Validators.required]]
    // clientUserContact  : ['', [Validators.pattern(/^\(\d{3}\)\s\d{3}-\d{4}$/),Validators.required]]
  });
  // this.contactForm     = this.formBuilder.group({
  //   clientFax          : ['', [Validators.required]],
  //   clientPhone        : ['', [Validators.pattern(/^\(\d{3}\)\s\d{3}-\d{4}$/),Validators.required]]
  // });

  // console.log('clientForm',this.clientForm.value);
  ///////////// disable sales representative fileds
  if (typeof this.requestData.data.salesRepresentativeId == "undefined" || this.requestData.data.salesRepresentativeId == "") {
      this.disabledSalesRepFields = false;
  }
  this.ngxLoader.stop();
  this.bindJQueryFuncs();
}

checkAccountNumber(){
  console.log("requestData.accountNumber",this.requestData.data.accountNumber);
  var requestD = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"CLIM",
      functionalityCode:"",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data: {
      userCode:this.requestData.data.accountNumber
    }
  }
  /////////////// defaultAccountTypes
  var url                = environment.API_CLIENT_ENDPOINT + 'checkaccountnumber';
  requestD.header.partnerCode = this.logedInUserRoles.partnerCode;
  requestD.header.userCode    = this.logedInUserRoles.userCode;
  requestD.header.functionalityCode    = "CLTA-AC";

  this.httpRequest.httpPutRequests(url,requestD).then(resp => {
    console.log('Account Number',resp)
    if (resp['data'] == true) {
      console.log("true");
      this.accNumberExist = true;
    }
    else{
      this.accNumberExist = false;
    }
    // this.defaultAccountTypes = resp;
  })
}
checkAccountNumberUpdate(){
  // console.log("updateAccountNumber",this.updateAccountNumber);
  var requestD = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"CLIM",
      functionalityCode:"",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data: {
      userCode:this.updateAccountNumber
    }
  }
  /////////////// defaultAccountTypes
  var url                = environment.API_CLIENT_ENDPOINT + 'checkaccountnumber';
  requestD.header.partnerCode = this.logedInUserRoles.partnerCode;
  requestD.header.userCode    = this.logedInUserRoles.userCode;
  requestD.header.userCode    = "CLTA-AC";
  this.httpRequest.httpPutRequests(url,requestD).then(resp => {
    console.log('Account Number',resp)
    if (resp['data'] == true) {
      console.log("true");
      // this.accNumberExist = true;
      if (this.updateAccountNumberOLD == this.updateAccountNumber) {
        this.accNumberExist = false;
      }
      else{
        this.accNumberExist = true;
      }
    }
    else{
      this.accNumberExist = false;
    }
    // this.defaultAccountTypes = resp;
  })
}

con(){
}

// deliveryChange(){
//   console.log('deliverTypeModel',this.deliverTypeModel);
//
// }

onCheckboxChange(e) {
  const delivery: FormArray = this.clientForm.get('delivery') as FormArray;
  // console.log('delivery',delivery);
  // console.log('e',e);
  // console.log('e.target.checked',e.target.checked);
  // console.log('e.target',e);
  this.tempDev = {};

  if (e.target.checked) {
    delivery.push(new FormControl(e.target.value));
    this.tempDev.deliveryMethodId = e.target.value;

    this.tempDeliver.push(this.tempDev);

    // var tem = parseInt(e.target.value);
    var tem = e.target.value;
    for (let j = 0; j < this.tempDeliver.length; j++) {
      if(this.tempDeliver[j].deliveryMethodId === tem){
        for (let i = 0; i < this.deliverMethod.length; i++) {
          if (this.deliverMethod[i].deliveryMethodId == this.tempDeliver[j].deliveryMethodId) {
            if (this.deliverMethod[i].name == "Email") {
              // this.encryptionCodeRequired = false;

              this.clientForm.get('encryption').setValidators([Validators.required])
              this.clientForm.controls["encryption"].updateValueAndValidity();
            }
          }

        }

      }
      else{
        // console.log("not eqular");

      }

    }

  } else {
    let i: number = 0;
    delivery.controls.forEach((item: FormControl) => {
      if (item.value == e.target.value) {
        delivery.removeAt(i);
        for (let j = 0; j < this.tempDeliver.length; j++) {
          if(this.tempDeliver[j].deliveryMethodId == e.target.value){
            for (let i = 0; i < this.deliverMethod.length; i++) {
              if (this.deliverMethod[i].deliveryMethodId == this.tempDeliver[j].deliveryMethodId) {
                if (this.deliverMethod[i].name == "Email") {


                  // this.encryptionCodeRequired = false;

                  this.clientForm.get('encryption').clearValidators()
                  this.clientForm.controls["encryption"].updateValueAndValidity();
                }
              }

            }
            this.tempDeliver.splice(j, 1);
          }

        }
        // var start = parseInt(item.value) -1;
        // var end   = parseInt(e.target.value) -1;
        // this.tempDeliver.splice(start, 1);
        return;
      }
      i++;
    });
  }
  // console.log('delivery',delivery);

}

get f() { return this.clientForm.controls; }
submitClient() {
  this.ngxLoader.start();
  this.submitted                  = true;
  if (this.clientForm.invalid) {
    this.ngxLoader.stop();
    this.scrollToError();
    // alert('form invalid');
    return;
  }
  console.log('clientForm',this.clientForm.value);
  // console.log('clientForm',this.clientForm.value.priority);
  var table                       = document.querySelector("table");
  var contactData                 = this.parseTable(table);
  console.log('contactData',contactData);

  this.saveClientData.otherData    = this.clientForm.value;
  this.saveClientData.contactInfo  = contactData;
  this.saveClientData.imagePayload = this.imagePayLoad;
  this.saveClientData.imageBase64  = this.imageBase64;
  // console.log('this.saveData',this.saveClientData);
  console.log('this.saveData',this.requestData);
  // this.changeTabsCheck();
  // this.fileUploadCheck();
  this.formatRequestData();

}

get f2() { return this.clientForm.controls; }
updateClient(status) {
  this.submitted                  = true;
  if (this.clientForm.invalid) {
    // alert('form invalid');
    this.submitted                  = false;
    return;
  }
  if (status == 1) {
    // this.populateUpdateForm()
    ///// complete
    if (this.enableCompleteSuperCheck >0 && this.enableCompleteDefaultCheck>0) {
      this.updateRequestData.active = 1;
    }
    else{
      this.notifier.notify( "error", "You must add one default attending physician and one super user to add the client" );
      this.submitted                  = false;
      return;
    }
    // this.updateRequestData.active = 1;
  }
  else if(status == 0){
    ///// Draft
    this.updateRequestData.active = 0;

  }
  this.formatUpdateRequestData();
  // console.log('clientForm',this.clientForm.value);
  // var table                       = document.querySelector("table");
  // var contactData                 = this.parseTableUpdate(table);
  // console.log('contactData',contactData);

  // this.saveClientData.otherData   = this.clientForm.value;
  // this.saveClientData.contactInfo = contactData;
  // this.saveClientData.imagePayload = this.imagePayLoad;
  // this.saveClientData.imageBase64  = this.imageBase64;
  // console.log('this.saveData',this.saveClientData);


}

// saveClientData(status){
//
// }

get f3() { return this.clientUserForm.controls; }
submitClientUser(){
  this.ngxLoader.start();
  this.submitted                    = true;
  if (this.clientUserForm.invalid) {
    // alert('form invalid');
    this.ngxLoader.stop();
    return;
  }
  // this.saveData.userData                      = this.clientUserForm.value;
  this.UserRequestData.data.user.userCode           = 0;
  this.UserRequestData.data.user.displayCode      = "AB94";
  this.UserRequestData.data.user.firstName        = this.clientUserForm.value.clientUserFName;
  this.UserRequestData.data.user.lastName         = this.clientUserForm.value.clientUserLName;
  this.UserRequestData.data.user.username         = this.clientUserForm.value.clientUserUName;
  this.UserRequestData.data.user.accessCode       = "AB94";
  this.UserRequestData.data.user.postNominal      = this.selectedPostNominal;
  this.UserRequestData.header.partnerCode = this.logedInUserRoles.partnerCode;
  this.UserRequestData.header.userCode    = this.logedInUserRoles.userCode;
  this.UserRequestData.header.functionalityCode    = "USRA-AUE";



  console.log('this.UserRequestData USer',this.UserRequestData);
  // console.log('clientForm',this.UserRequestData.data.user.phone);
  // console.log('clientForm',this.clientUserForm.value);
  // this.SaveCUserData.clientUserData = this.clientUserForm.value;
  // this.userShowData.push(this.clientUserForm.value);
  var url                = environment.API_USER_ENDPOINT + 'save';
  this.httpRequest.httpPostRequests(url,this.UserRequestData).then(resp => {
    console.log("resp",resp);

    // this.allUsers.push(resp);
    // this.ngxLoader.stop();
    if (resp.result.codeType == 'S') {
      this.notifier.notify( "success", "User saved Successfully USER API" );
      /////////////// Request To client API
      this.clientUserReq.userCode   = resp.data.userCode;
      this.clientUserReq.clientId = this.selectedClientID;
      url                         = environment.API_CLIENT_ENDPOINT + 'addclientuser';
      this.httpRequest.httpPostRequests(url,this.clientUserReq).then(response => {
        console.log("--resp Client User",response);

        // this.allUsers.push(resp);
        this.ngxLoader.stop();
        if (typeof response != "undefined") {
          this.notifier.notify( "success", "User saved Successfully Client API" );
          var temp = '';
            console.log('before',resp.data);
          for (let i = 0; i < resp.data.roles.length; i++) {
              temp = resp.data.roles[i]['name']+', '+ temp;
          }
          resp.data.userRoles  = temp;
          // this.userShowData.push(resp.data);
          this.userShowData.push(resp['data']);
          this.submitted = false;
          this.inValidUsernpi   = false;
          this.inValidUseremail = false;
          this.clientUserForm.reset();

        }
        else{
          this.ngxLoader.stop();
          this.notifier.notify( "error", "Error while saving user CLIENT API");
        }
      })
      // var temp = '';
      //   console.log('before',resp.data);
      // for (let i = 0; i < resp.data.roles.length; i++) {
      //     temp = resp.data.roles[i]['name']+', '+ temp;
      // }
      //
      //
      // resp.data.userRoles  = temp;
      // this.userShowData.push(resp.data);
      // this.submitted = false;
      // this.clientUserForm.reset();


    }
    else{
      this.ngxLoader.stop();
      this.notifier.notify( "error", "Error while saving user USER API");
      // this.swalStyle.fire({
      //   icon: 'error',
      //   type: 'sucess',
      //   title: 'There was an error while Saving user',
      //   showConfirmButton: true,
      //   timer: 3000
      // })
      // $('#errorMSG').css("display","block");

    }
  })
}

bindJQueryFuncs() {
  // console.log('$(".requisitionMaxLength .ng-input input[type=checkbox]")',$(".requisitionMaxLength"));
  //
  // $(".requisitionMaxLength .ng-input input[type=checkbox]").attr('maxlength','6');
  $( document ).ready(function() {
    $(".reqTypeLength .ng-input input").attr('maxlength', 50);
  });
}

///////// add row to contact table
addFieldValue(index) {
  this.fieldArray.push(this.newAttribute)
  // this.contactRowValid        = false;
  this.newAttribute = {};
}
///////// remove row from contact table
deleteFieldValue(index) {
  this.fieldArray.splice(index, 1);
}

//////// adding fields while updating client
addFieldValue_update(index) {
  var object = {
    ""             : "",
    "ContactNo"    : "",
    "contactTypeId": "1",
    "ContactID"    : "",
    "Fax"          : "",
    "FaxID"        : "3",
    "ContactID1"    : "",
    "Email"        : "",
    "EmailID"      : "2",
    "ContactID2"    : "",
    "Default"      : ""
  }
  this.setCross    = true;
  this.tableSize   = this.tableSize+1;
  this.conactHolderUpdate.push(object)
  // this.contactRowValid        = false;
  this.contactLength = this.conactHolderUpdate.length;
  // console.log('here',this.conactHolderUpdate);
  // console.log('contactLength',this.contactLength);

}
///////// remove row from contact table
deleteFieldValue_update(index) {
  if (index !== -1) {
    console.log('if',index);
    this.conactHolderUpdate.splice(index+1, 1);
  }
  else{
    console.log('else',index);
  }
  this.contactLength = this.conactHolderUpdate.length;
  // this.conactHolder.splice(index, 1);

}
//////// adding fields while updating client


changeTabsCheck() {
  this.addClientTabPan  = false;
  this.clientUserTabPan = true;
  var table             = document.querySelector("table");
  var contactData       = this.parseTable(table);
  this.tableSize        = contactData.length;
  if (contactData.length > 1) {
    this.setCross       = true;
  }
  this.conactHolder     = contactData;
  console.log('this.conactHolder',this.conactHolder);
  console.log('this.clientForm.value',this.clientForm.value.delivery);
  console.log('this.deliverMethod',this.deliverMethod);
  for (let i = 0; i < this.clientForm.value.delivery.length; i++) {
    for (let j = 0; j < this.deliverMethod.length; j++) {
      if (this.deliverMethod[j].id == this.clientForm.value.delivery[i]) {
        console.log('match',this.deliverMethod[j].id);
        this.deliverMethod[j].checked = true;
        j++;
      }

    }

  }
  // console.log('this.deliverMethod',this.deliverMethod);
  // console.log('at Change RequestData',this.requestData);
  // console.log('newUpdateRequest',this.newUpdateRequest);

  // console.log('at Change Update RequestData',this.updateRequestData);




  // $('.nav-continue-back > .nav-item > .active').parent().next('li').find('a').trigger('click');

}

saveCompleteData(flag){
  ////// if flag = 1 then save as draft else save completely
  this.saveClientData.draftFlag = flag;
  console.log('this.saveData',this.saveClientData);
  console.log('this.SaveCUserData',this.SaveCUserData);
  this.saveData.ClientData = this.saveClientData;
  this.saveData.ClientData = this.SaveCUserData;


}
onFileSelect(input) {
  console.log(input.files);
  // return;
  var mb = input.files[0].size/1048576;
  // console.log("mb",mb);
  var sl = input.files[0].name.length
  console.log("sl",sl);

  if (mb <= 1 && sl <= 40) {

    const formData: FormData = new FormData();
    // console.log('has',formData.has('file'));

    // console.log('this.imagePayLoad',this.imagePayLoad);
    if (input.files && input.files[0]) {
      this.imagePayLoad = input.files[0];
      var reader = new FileReader();
      reader.onload = (e: any) => {
        // console.log('Got here: ', e.target.result);
        this.imageBase64 = e.target.result;
        // this.obj.photoUrl = e.target.result;
      }
      reader.readAsDataURL(input.files[0]);
    }
    // console.log('this.imageBase64: ', this.imageBase64);
    if(input.files && input.files.length > 0) {
      // this.imagePayLoad = input.files;
      // // this.imagePayLoad = input.files[0];
      this.imagePayLoad = input.files[0];

        // formData.append('file', input.files[0]);
      // console.log('this.files',input.files);


      // this.imagePayLoad = input.files[0];
  }

  }
  else{
    if (mb > 1) {alert('File Size must be less then 1MB');}
    if (sl > 40) {alert('File Name Too Large');}
    $('#clientLogo').val('');



  }
  // console.log('imagePayLoad on change',this.imagePayLoad);\


  // console.log('formData',formData);
  // console.log('formData',formData.entries());
  // console.log('formData',formData.values());
  // console.log('has',formData.has('file'));
  // console.log('formData',formData.getAll('file'));


}


onFileChangeUpdateClient(input) {
  console.log(input.files);
  var mb = input.files[0].size/1048576;
  var sl = input.files[0].name.length
  console.log("sl",sl);

  // console.log("mb",mb);

  if (mb <= 1 && sl <= 40) {
  if (input.files && input.files[0]) {
    this.imagePayLoad = input.files[0];
    var reader = new FileReader();
    reader.onload = (e: any) => {
      // console.log('Got here: ', e.target.result);
      this.imageBase64 = e.target.result;
      // this.obj.photoUrl = e.target.result;
    }
    reader.readAsDataURL(input.files[0]);
  }
  // console.log('this.imageBase64: ', this.imageBase64);
  if(input.files && input.files.length > 0) {
    this.imagePayLoad = input.files[0];
    // this.imagePayLoad = input.files[0];
  }
}
else{
  if (mb > 1) {alert('File Size must be less then 1MB');}
  if (sl > 40) {alert('File Name Too Large');}
  $('#inputGroupFile02').val('');
}
}
checkChange(){
  var table                       = document.querySelector("table");
  var contactData                 = this.parseTable(table);
  console.log('contactData',contactData);


}
attTypeChanged(){

}

///////////////////// format data acc to request template
formatRequestData() {
  this.contactFormat          = [];
  this.tempContact            = {};
  // console.log("this.attribute: ", this.requestData);
  // console.log('this.deliverMethod',this.deliverMethod);
  // console.log('this.deliverMethod',this.clientForm.value.delivery);
  var table                       = document.querySelector("table");
  var contactData                 = this.parseTable(table);
  // console.log('contactData',contactData);
  //////// to populate Update Form Attachment Type

  // this.requestData.data.AttachmentTypeDto = this.AttachmentTypeHolder;
  // for (let i = 0; i < this.requestData.data.AttachmentTypeDto.length; i++) {
  //   delete this.requestData.data.AttachmentTypeDto[i].name;
  //
  // }
  // this.requestData.data.parentClientId = 1;
  // defaultPriority

  this.saveDataReqAttTypeTemp = {}
  for (let j = 0; j < this.AttachmentTypeHolder.length; j++) {
    // delete this.AttachmentFileHolder[j].name;
    this.saveDataReqAttTypeTemp.attachmentTypeId = this.AttachmentTypeHolder[j].attachmentTypeId
    this.saveDataReqAttType.push(this.saveDataReqAttTypeTemp);
    this.saveDataReqAttTypeTemp = {}
  }
  this.requestData.data.AttachmentTypeDto       = this.saveDataReqAttType;

  // console.log("before", this.requestData.data.AttachmentFileTypeDto);
  this.saveDataReqFIleTypeTemp = {}
  for (let j = 0; j < this.AttachmentFileHolder.length; j++) {
    // delete this.AttachmentFileHolder[j].name;
    this.saveDataReqFIleTypeTemp.attachmentFileTypeId = this.AttachmentFileHolder[j].attachmentFileTypeId
    this.saveDataReqFIleType.push(this.saveDataReqFIleTypeTemp);
    this.saveDataReqFIleTypeTemp = {}
  }

  this.requestData.data.AttachmentFileTypeDto       = this.saveDataReqFIleType;
  // taa = this.saveDataReqFIleType;
  // this.updateRequestData.AttachmentFileTypeDto = taa;
  // console.log("allowedFilesize", this.requestData.data.AttachmentFileTypeDto);

  // delete this.requestData.data.ClientTypeHolder.name;
  // taa = this.requestData.data.ClientRequisitionTypeDto
  // this.updateRequestData.ClientRequisitionTypeDto = taa;
  for (let k = 0; k < this.requestData.data.ClientRequisitionTypeDto.length; k++) {
    delete this.requestData.data.ClientRequisitionTypeDto[k].name;

  }
  // console.log("After Dele: ", this.requestData.data.AttachmentTypeDto);
  // console.log("AttachmentFileTypeDto", this.requestData.data.AttachmentFileTypeDto);
  // console.log("ClientTypeDto", this.requestData.data.ClientTypeDto);
  // console.log("ClientRequisitionTypeDto", this.requestData.data.ClientRequisitionTypeDto);

  let helperContatArray      =[1,2,3,4];
  let helperIndexArray       ={0: 'ContactNo', 1: 'Fax', 2: 'Email', 3:'Default'};
  // console.log('helperIndexArray',helperIndexArray);

  for (let l = 0; l < contactData.length; l++) {
    // for (let m = 0; m < 4; m++) {
      this.tempContact = {};
      ///// for cell
      this.tempContact.contactTypeId    = 1;
      if (contactData[l].Default == true) {
        contactData[l].Default = 1;
      }
      else{
          contactData[l].Default = 0
      }
      if (contactData[l].ContactNo != "") {
        this.tempContact.value            = this.encryptDecrypt.encryptString(contactData[l].ContactNo);
        //this.tempContact.defaultContact   = contactData[l].Default;
        this.tempContact.createdBy        = this.logedInUserRoles.userCode;
        this.tempContact.createdTimestamp = null;
        this.tempContact.updatedBy        = null;
        this.tempContact.updatedTimestamp = null;
        this.tempContact.moduleCode       = "CLTM";
        this.tempContact.defaultContact   = contactData[l].Default;
        // this.contactFormat[0] = this.tempContact;
        // console.log('tempContact11',this.tempContact);
        this.contactFormat.push(this.tempContact);
      }

      this.tempContact = {};

      ///// for Fax
      this.tempContact.contactTypeId    = 3;
      if (contactData[l].Default == true) {
        contactData[l].Default = 1;
      }
      else{
          contactData[l].Default = 0
      }

      if (contactData[l].Fax != "") {
        this.tempContact.value            = this.encryptDecrypt.encryptString(contactData[l].Fax);
        //this.tempContact.defaultContact   = contactData[l].Default;
        this.tempContact.createdBy        = this.logedInUserRoles.userCode;;
        this.tempContact.createdTimestamp = null;
        this.tempContact.updatedBy        = null;
        this.tempContact.updatedTimestamp = null;
        this.tempContact.moduleCode       = "CLTM";
        this.tempContact.defaultContact   = contactData[l].Default;
        // this.contactFormat[1] = this.tempContact;
        // console.log('tempContact222',this.tempContact);
        this.contactFormat.push(this.tempContact);
      }

      this.tempContact = {};
      // this.contactFormat.push(this.tempContact);

      // ///// for Email
      this.tempContact.contactTypeId    = 2;
      if (contactData[l].Default == true) {
        contactData[l].Default = 1;
      }
      else{
          contactData[l].Default = 0
      }

      if (contactData[l].Email != "") {
        this.tempContact.value            = this.encryptDecrypt.encryptString(contactData[l].Email);
        //this.tempContact.defaultContact   = contactData[l].Default;
        this.tempContact.createdBy        = this.logedInUserRoles.userCode;;
        this.tempContact.createdTimestamp = null;
        this.tempContact.updatedBy        = null;
        this.tempContact.updatedTimestamp = null;
        this.tempContact.moduleCode       = "CLTM";
        this.tempContact.defaultContact   = contactData[l].Default;
        // this.contactFormat[2] = this.tempContact;
        this.contactFormat.push(this.tempContact);
      }

      // console.log('tempContact222',this.tempContact);
      // this.contactFormat.push(this.tempContact);
    // }
    // this.contactFormat.push(this.tempContact);

  }
  // console.log("this.contactFormat ", this.contactFormat);
  // var t2 = this.contactFormat;
  this.requestData.data.ContactDto       = this.contactFormat;
  // this.updateRequestData.ContactDto = t2;
  // console.log("this.attribute: ", this.requestData);
  this.requestData.data.DeliveryMethodDto       = this.tempDeliver;


  // var t3      = this.tempDeliver;
  // this.updateRequestData.DeliveryMethodDto = t3;
  // console.log("this.requestData.data.DeliveryMethodDto: ", this.requestData.data.DeliveryMethodDto);
  // console.log("this.requestData.data.ClientRequisitionTypeDto",this.requisitionDTOTEMP);
  // console.log("this.requestData.data.defaultPriority",this.requestData.data.defaultPriority);
  // console.log("this.requestData.data.defaultReportFormatId",this.requestData.data.defaultReportFormatId);
  this.saveDataReqTypeTemp = {}
  for (let p = 0; p < this.requisitionDTOTEMP.length; p++) {

    this.saveDataReqTypeTemp.formId           = this.requisitionDTOTEMP[p].requisitionTypeId;
    this.saveDataReqTypeTemp.addedBy          = this.logedInUserRoles.userCode;
    this.saveDataReqTypeTemp.addedTimestamp   = null;
    this.saveDataReqTypeTemp.updatedBy        = this.logedInUserRoles.userCode;
    this.saveDataReqTypeTemp.updatedTimestamp = null;
    this.saveDataReqType.push(this.saveDataReqTypeTemp);
    this.saveDataReqTypeTemp = {}
  }
  this.requestData.data.ClientRequisitionTypeDto       = this.saveDataReqType;

  //// Check if delivery method is email the set encription code requires
  for (let i = 0; i < this.tempDeliver.length; i++) {
    for (let n = 0; n < this.deliverMethod.length; n++) {
      // console.log("this.deliverMethod",this.deliverMethod[n]);
      if (this.deliverMethod[n].name == 'Email') {
        if (this.tempDeliver[i].deliveryMethodId == this.deliverMethod[n].deliveryMethodId) {
          this.encryptionCodeRequired = true;
        }
      }
    }
  }
  if (this.requestData.data.encryptionCode == "" && this.encryptionCodeRequired == true) {
    alert('Encryption Code is required as you have selected Email as delivery method')
    this.ngxLoader.stop();
    return;

  }


  // var t4       = this.saveDataReqType;
  // this.updateRequestData.ClientRequisitionTypeDto = t4;
  // console.log('this.AFTER',this.requestData.data.ClientRequisitionTypeDto);
  // var taa2 = this.ClientTypeHolder;
  // this.UpdateFormClientTypeHolder = taa2;
  // delete this.ClientTypeHolder.name;

  // this.requestData.data.ClientTypeDto.push(this.ClientTypeHolder);
  // for (let lm = 0; lm < this.requestData.data.ClientTypeDto.length; lm++) {
  //   delete this.requestData.data.ClientTypeDto[lm].name;
  //
  // }
  this.saveDataClientTypeTemp = {}
  this.saveDataClientTypeTemp.clientTypeId = this.ClientTypeHolder.clientTypeId
  this.saveDataClientType.push(this.saveDataClientTypeTemp);
  this.saveDataClientTypeTemp = {}
  this.requestData.data.ClientTypeDto       = this.saveDataClientType

  if (this.requestData.data.showWordFile == true) {
    this.requestData.data.showWordFile                         = 1;
  }
  else if (this.requestData.data.showWordFile == false) {
    this.requestData.data.showWordFile                         = 0;
  }
  else{
    this.requestData.data.showWordFile                         = 0;
  }
  let testID = Math.floor(Math.random() * 1000);
  this.requestData.header.partnerCode = this.logedInUserRoles.partnerCode;
  this.requestData.header.userCode    = this.logedInUserRoles.userCode;
  this.requestData.header.functionalityCode    = "CLTA-AC";
  this.requestData.data.createdBy = this.logedInUserRoles.userCode;
  this.requestData.data.AddressDetailsDto.createdBy = this.logedInUserRoles.userCode;
  this.requestData.data.AddressDetailsDto.suiteFloorBuilding = this.encryptDecrypt.encryptString(this.requestData.data.AddressDetailsDto.suiteFloorBuilding);
  this.requestData.data.AddressDetailsDto.street = this.encryptDecrypt.encryptString(this.requestData.data.AddressDetailsDto.street);
  // this.requestData.data.clientId = testID;
  /////formdata for file
  let formRequestData = new FormData();
  // console.log('has file',formRequestData.has('file'));
  // console.log('has clientData',formRequestData.has('clientDetails'));
  if (this.imagePayLoad == '' || this.imagePayLoad == null) {
      // this.imagePayLoad == null;
      // this.imagePayLoad = [];
      var f = new File([""], "");
      console.log("FILE",f);
      this.imagePayLoad = f;
      // this.imagePayLoad = '';
      // this.imagePayLoad = $('#clientLogo')[0].files;
  }
  console.log('has this.imagePayLoad',this.imagePayLoad);
  formRequestData.append('file', this.imagePayLoad);
  // this.requestData.data.file = formRequestData;
  // this.requestData.data.file = this.imagePayLoad;
  var reqString = JSON.stringify(this.requestData);
  // formRequestData.append('clientDetails', this.requestData.data.clientDetails);
  formRequestData.append('clientDetails', reqString);
//   console.log("formRequestData.values()",formRequestData.values());
//
//   for (var value of formRequestData.values()) {
//    console.log('value',value);
// }
console.log('this.requestData',this.requestData);
  ////// this.updateRequestData.ClientTypeDto.push(this.ClientTypeHolder);
  // this.ngxLoader.stop(); return;

  var url                = environment.API_CLIENT_ENDPOINT + 'saveclient';
  // this.httpRequest.httpPostRequests(url,formRequestData).then(resp => {
  this.clientService.saveClient(url,formRequestData).subscribe(resp => {
    console.log("resp",resp);

    // this.allUsers.push(resp);
    // this.ngxLoader.stop();
    if (typeof resp['data'] != 'undefined') {
      this.submitted = false;
      if (resp['result']['codeType'] == 'E') {
        this.notifier.getConfig().behaviour.autoHide = 3000;
        this.notifier.notify( "error", "Error while saving client" );
        this.ngxLoader.stop();
      }
      else{
        console.log('record added succesfully----');
        this.notifier.notify( "success", "Client saved Successfully" );
        setTimeout(()=>{                           //<<<---using ()=> syntax
        console.log('inTimeOut');
        // var data = {from:1,accNumber:resp['data']['accountNumber']}
        // this.router.navigateByUrl('/edit-client', { state: data });
        var a = resp['data']['accountNumber']+';1'
        // var ciphertext = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
        var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
        var bx = ax.replace('+','xsarm');
        var cx = bx.replace('/','ydan');
        var ciphertext = cx.replace('=','zhar');
        this.router.navigate(['edit-client', ciphertext]);

          // this.router.navigateByUrl('/edit-client/'+resp['data']['accountNumber']+';from='+1);
          this.ngxLoader.stop();
   }, 3000);

        // this.swalStyle.fire({
        //   icon: 'success',
        //   type: 'sucess',
        //   title: 'Client Added Successfully',
        //   showConfirmButton: true
        // })
        // this.updateRequestData = resp;
        //////////////////
        //// GetCLient USers
        // this.clientIdForUsers = resp.clientId;

        //////////////////
        this.addClientTabPan  = false;
        this.clientUserTabPan = true;
      }

      // var table             = document.querySelector("table");
      // var contactData       = this.parseTable(table);
      // this.tableSize        = contactData.length;
      // if (contactData.length > 1) {
      //   this.setCross       = true;
      // }
      // this.conactHolder     = contactData;
      // console.log('this.conactHolder',this.conactHolder);
      // console.log('this.clientForm.value',this.clientForm.value.delivery);
      // console.log('this.deliverMethod',this.deliverMethod);
      // for (let i = 0; i < this.clientForm.value.delivery.length; i++) {
      //   for (let j = 0; j < this.deliverMethod.length; j++) {
      //     if (this.deliverMethod[j].id == this.clientForm.value.delivery[i]) {
      //       console.log('match',this.deliverMethod[j].id);
      //       this.deliverMethod[j].checked = true;
      //       j++;
      //     }
      //
      //   }
      //
      // }
      // console.log('this.deliverMethod',this.deliverMethod);
      // console.log('at Change RequestData',this.requestData);
      // console.log('newUpdateRequest',this.newUpdateRequest);
      this.contactFormat            = [];
      this.tempDeliver              = [];
      this.saveDataReqType          = [];
      this.saveDataClientType       = [];
      this.saveDataReqAttType       = [];
      this.tempContact              = {};
      this.tempDev                  = {};
      this.saveDataReqTypeTemp      = {};
      this.saveDataClientTypeTemp   = {};
      this.saveDataReqAttTypeTemp   = {};
      this.saveDataReqFIleType      = [];
      this.saveDataReqFIleTypeTemp  = {};

      // this.updateRequestData = resp;
      // this.changeCheck();
      // this.allUsers.push(resp);
      // this.clientForm.reset();
      this.addedClientID    = resp['data']['clientId'];
      this.addedClientAcc   = resp['data']['accountNumber'];
      this.selectedClientID = resp['data']['clientId'];
      // this.router.navigate(['edit-client', resp.accountNumber]);
      // this.router.navigate(['edit-client',resp['accountNumber'], { from: 1 }]);
      // this.populateUpdateForm()
      // this.populateUpdateForm(this.addedClientID)

    }
    else{

      this.ngxLoader.stop();
      this.contactFormat            = [];
      this.tempDeliver              = [];
      this.saveDataReqType          = [];
      this.saveDataClientType       = [];
      this.saveDataReqAttType       = [];
      this.tempContact              = {};
      this.tempDev                  = {};
      this.saveDataReqTypeTemp      = {};
      this.saveDataClientTypeTemp   = {};
      this.saveDataReqAttTypeTemp   = {};
      this.saveDataReqFIleType      = [];
      this.saveDataReqFIleTypeTemp  = {};
      this.notifier.notify( "error", "Error while saving client" );
      // this.swalStyle.fire({
      //   icon: 'error',
      //   type: 'error',
      //   title: 'Error While Saving Client',
      //   showConfirmButton: true
      // })

    }
    // console.log('/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/');
    // this.router.navigate(['edit-client',resp['accountNumber'], { from: 1 }]);
    // this.router.navigateByUrl('/edit-client/'+resp['accountNumber']+';from'+ 1)
    // this.router.navigateByUrl('/edit-client/'+resp['accountNumber']+';from='+ 1)


  })
  this.contactFormat            = [];
  this.tempDeliver              = [];
  this.saveDataReqType          = [];
  this.saveDataClientType       = [];
  this.saveDataReqAttType       = [];
  this.tempContact              = {};
  this.tempDev                  = {};
  this.saveDataReqTypeTemp      = {};
  this.saveDataClientTypeTemp   = {};
  this.saveDataReqAttTypeTemp   = {};
  this.saveDataReqFIleType      = [];
  this.saveDataReqFIleTypeTemp  = {};
}

populateUpdateForm(){
}


///////////////////// format data acc to request template
formatUpdateRequestData() {
}



allowedAttachmentTypeChange(event){
  console.log('event',event);
  ////// To Copy Samevalues to Update Form
  this.UpdateFormAttachmentFileHolder = event;
  console.log('UpdateFormAttachmentFileHolder',this.UpdateFormAttachmentFileHolder);
}
accTypeChange(event){
  console.log('event',event);
  ////// To Copy Samevalues to Update Form
  // this.UpdateFormClientTypeHolder = event;
  // console.log('UpdateFormClientTypeHolder',this.UpdateFormClientTypeHolder);
}
attachmentTypeChange(event){
  // console.log('event',event);
  ////// To Copy Samevalues to Update Form
  // this.UpdateFormAttachmentTypeHolder = event;
  // console.log('UpdateFormAttachmentTypeHolder',this.UpdateFormAttachmentTypeHolder);
}

fileUploadCheck(){
  // this.saveClientData.imagePayload = this.imagePayLoad;
  // console.log('fileUploadCheck',this.saveClientData);
}

addClientProvidedEmail(userCode, e){
}
addClientProvidedFax(userCode, e){

}
addClientProvidedPracticeCode(userCode, e){

}
addClientProvidedDefault(userCode, e){

}
superUserCheck(userCode, e){
}
clientPortalAccessCheck(userCode, e){
}
visibleInCasesCheck(userCode, e){
}
activeInactiveCheck(userCode, e){
}
deleteModal(userCode){
}
deleteClientUser(){
}
onAddItem(event){
  if(typeof event.formId =='undefined'){
    console.log('event',event);


  }
  else{
    console.log("existss");

  }


}


mapRow(headings) {

  headings[0] = "ContactNo"


  return function mapRowToObject({ cells }) {
    return [...cells].reduce(function(result, cell, i) {
      const input = cell.querySelector("input,select");
      // const input = cell.querySelectorAll("input,select");
      var value;
      if (input) {
        value = input.type === "radio" ? input.checked : input.value;
        // console.log("value",value);

      } else if(input != '') {
        value = cell.innerText;
        // console.log("cell",cell);

      }
      // var completeValue = value +','+ idvalue;

      return Object.assign(result, { [headings[i]]: value });
      // return Object.assign(result, { [headings[i]]: completeValue });
    }, {});
  };
}

parseTable(table) {
  var headings = [...table.tHead.rows[0].cells].map(
    heading => heading.innerText
  );


  return [...table.tBodies[0].rows].map(this.mapRow(headings));
}

// // // Used to convert Contact Details Table into object
mapRowUpdate(headings) {
  headings[0] = "ContactNo";
  return function mapRowToObject({ cells }) {
    return [...cells].reduce(function(result, cell, i) {
      const input = cell.querySelector("input,select");
      // const input = cell.querySelectorAll("input,select");
      var value;
      var contact_id;
      // console.log("cell",cell);
      console.log("input",input);
      var idvalue;
      if (input == null) {
        idvalue= null;
        contact_id = null;
      }
      else{
        idvalue = input.getAttribute("data-value")
        contact_id = input.getAttribute("contact-id")
      }
      // console.log("idvalue",idvalue);
      // console.log("0",input[0]);
      // console.log("1",input[1]);
      if (input) {
        value = input.type === "radio" ? input.checked : input.value;
        console.log("value",value);

      } else if(input != '') {
        value = cell.innerText;
        // console.log("cell",cell);

      }
      // var completeValue = value +','+ idvalue;
      var completeValue = value +','+ idvalue +','+ contact_id;

      // return Object.assign(result, { [headings[i]]: value });
      return Object.assign(result, { [headings[i]]: completeValue });
    }, {});
  };
}

parseTableUpdate(table) {
  var headings = [...table.tHead.rows[0].cells].map(
    heading => heading.innerText
  );
  return [...table.tBodies[0].rows].map(this.mapRowUpdate(headings));
}

changeCheck(){

  // console.log("saveDataReqType",this.saveDataReqType);
  // console.log("saveDataClientType",this.saveDataClientType);
  // console.log("saveDataReqAttType",this.saveDataClientTypeTemp);
  // console.log("saveDataReqAttType",this.saveDataReqAttType);
}
checkContactEmail(e){
  var regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
  var serchfind = regexp.test(e.target.value);
  console.log("serchfind",serchfind);
  if (e.target.value != '' && typeof e.target.value != 'undefined') {
    if (serchfind) {
      this.contactEmailValidity   = true;
      this.contactRowValid        = true;
      // this.enableAddContactButton();
    }
    else{
      if(this.contactRowValid == true){
        this.contactEmailValidity   = false;
        // this.contactRowValid        = false;
      }
      else{
        this.contactEmailValidity   = false;
        this.contactRowValid        = false;
      }

    }
  }
  else{
    this.contactEmailValidity   = true;
  }

}
enableAddContactButton(e){
  // console.log(e.target.value);

  if (typeof e.target.value != "undefined" && e.target.value != "") {
    this.contactRowValid        = true;
  }
  else{
    if (this.contactEmailValidity == true) {
      this.contactRowValid        = true;
    }else{
      this.contactRowValid        = false;
    }
  }
  var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
  e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '')
  // console.log('x',x);


}

changeSalesRep(e){
  // console.log('e.target.value',e.target.value);
  for (let i = 0; i < this.defaultSalesReps.length; i++) {
    if (e.target.value == this.defaultSalesReps[i].userCode) {
      this.clientForm.patchValue({
        srFirstName : this.defaultSalesReps[i].firstName,
        srLastName  : this.defaultSalesReps[i].lastName,
        srEmail     : this.defaultSalesReps[i].email,
        srContact   : this.defaultSalesReps[i].phone,
      });
    }
  }
  if (e.target.value =='') {
    this.clientForm.patchValue({
      srFirstName : '',
      srLastName  : '',
      srEmail     : '',
      srContact   : '',
    });
  }

  // console.log('this.requestData.data.salesRepresentativeId',this.requestData.data.salesRepresentativeId);

  // if (typeof this.requestData.data.salesRepresentativeId == "undefined" || this.requestData.data.salesRepresentativeId == "") {
  //     this.disabledSalesRepFields = false;
  //     console.log("in");
  // }
  // else{
  //   this.disabledSalesRepFields = true;
  // }
}

changeSalesRepUpdate(){
  console.log('this.updateSalesRepresentativeId',this.updateSalesRepresentativeId);

  if (typeof this.updateSalesRepresentativeId == "undefined" || this.updateSalesRepresentativeId == "") {
      this.disabledSalesRepFields = false;
      console.log("in");
  }
  else{
    this.disabledSalesRepFields = true;
  }
}


setUserName(){
}

checkUserName(){

}

getSelectedUserData(userCode){

}

getAllUsers(){

}

getNPI(){

}
getEMAIL(){

}

toggleActiveClient(){

}

setEncrypptionCheckFalse(){
  if (this.requestData.data.encryptionCode != "" && typeof this.requestData.data.encryptionCode != "undefined") {
    this.encryptionCodeRequired = false;

  }
}
formatePhoneUS(e){
  // console.log("eeee",e);

  var x = this.UserRequestData.data.user.phone.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
  this.UserRequestData.data.user.phone = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '')

}

formateFaxUScusers(e){
  // console.log("eeee",e);

  var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
  e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '')

}

scrollTo(el: Element): void {
  if (el) {
    el.scrollIntoView({ behavior: 'smooth', block: 'center' });
  }
}

scrollToError(): void {
  const firstElementWithError = document.querySelector('.ng-invalid[formControlName]');
  console.log("aa:", firstElementWithError);

  this.scrollTo(firstElementWithError);
}

getLookups(){
  // this.ngxLoader.start();
  return new Promise((resolve, reject) => {
    var accType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=CLIENT_TYPE_CACHE&partnerCode=sip').then(response =>{
      return response;
    })
    var priori = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=PRIORITY_CACHE&partnerCode=sip').then(response =>{
      return response;
    })
    var allcountry = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=COUNTRY_CACHE&partnerCode=sip').then(response =>{
      return response;
    })
    var repFormat = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=ATTACHMENT_FILE_TYPE_CACHE&partnerCode=sip').then(response =>{
      return response;
    })
    var deliveryM = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=DELIVERY_METHOD_CACHE&partnerCode=sip').then(response =>{
      return response;
    })
    var attachmenttype = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=ATTACHMENT_TYPE_CACHE&partnerCode=sip').then(response =>{
      return response;
    })
    var fileType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=ATTACHMENT_FILE_TYPE_CACHE&partnerCode=sip').then(response =>{
      return response;
    })
    // var uri = environment.API_CLIENT_ENDPOINT+ 'parentclient';
    // this.parentCLientReq.clientId = 1; /////// logedin users client id
    // var parentCli = this.clientService.getParentClient(uri, this.parentCLientReq).then(response =>{
    //   return response;
    // })
    var parentCli = [0];
    var reqtype = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=REQUISITION_TYPE_CACHE&partnerCode=sip').then(response =>{
      return response;
    })



    forkJoin([accType, priori, allcountry, repFormat, attachmenttype, fileType, parentCli, deliveryM, reqtype]).subscribe(allLookups => {
      console.log("allLookups",allLookups);
      this.defaultAccountTypes     = this.sortPipe.transform(allLookups[0], "asc", "name");
      this.defaultAccountPriority  = this.sortPipe.transform(allLookups[1], "asc", "priorityName");
      this.allCountries            = this.sortPipe.transform(allLookups[2], "asc", "countryName");
      this.reportFormat            = this.sortPipe.transform(allLookups[3], "asc", "attachmentFileTypeName");
      this.defaultAttType          = this.sortPipe.transform(allLookups[4], "asc", "attachmentTypeName");
      // this.defaultFileType         = this.sortPipe.transform(allLookups[5], "asc", "attachmentFileTypeName");
      var fileTypeHolder         = this.sortPipe.transform(allLookups[5], "asc", "attachmentFileTypeName");
      this.defaultPCOrganization   = this.sortPipe.transform(allLookups[6]['data'], "asc", "name");
      this.deliverMethod           = this.sortPipe.transform(allLookups[7], "asc", "name");
      this.defaultReqType          = this.sortPipe.transform(allLookups[8], "asc", "requisitionTypeName");
      // this.allStatusType         = this.sortPipe.transform(allLookups[8], "asc", "caseStatusName");
      for (let index = 0; index < allLookups[2]['length']; index++) {
        if (allLookups[2][index]['countryName'] == 'USA' || allLookups[2][index]['countryName'] == 'usa') {
          this.requestData.data.AddressDetailsDto.countryId = allLookups[2][index]['countryId'];
          this.changeCountry()
        }

      }
       var holder = [];
      for (let i = 0; i < fileTypeHolder.length; i++) {
        if (fileTypeHolder[i]['attachmentFileTypeName'].toUpperCase() != 'ZIP') {
          holder.push(fileTypeHolder[i]);
        }
      }
      this.defaultFileType = holder;
      resolve();
    })
  })

}
accountTypeChange(){
  // console.log("ClientTypeHolder",this.ClientTypeHolder);

}
changeCountry(){
  if (this.requestData.data.AddressDetailsDto.countryId != null) {
    var cid = this.requestData.data.AddressDetailsDto.countryId;
    this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=STATE_CACHE&partnerCode=sip&id='+cid).then(response =>{
      // console.log("states",response);
      this.requestData.data.AddressDetailsDto.stateId = null;
      this.allProvinces = this.sortPipe.transform(response, "asc", "stateName");
    })
  }

}

changeState(){
  if (this.requestData.data.AddressDetailsDto.stateId != null) {
    var cid = this.requestData.data.AddressDetailsDto.stateId;
    this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=CITY_CACHE&partnerCode=sip&id='+cid).then(response =>{
      // console.log("states",response);
      this.requestData.data.AddressDetailsDto.city = null;
      this.allCities = this.sortPipe.transform(response, "asc", "cityName");
    })
  }

}

addRow(index){
  var temp  = {
    ContactNo : "",
    Fax       : "",
    Email     : "",
    Default   : false,
    Button    : 0,
    checkEnalbe: 0,
  }
  this.contactTableData.push(temp);
  // var len = this.contactTableData.length;

}
remRow(index){

  this.contactTableData.pop(index,1);
}
ToggleContactButton(e, index, type){
  if (type == 'fax' || type == 'phone') {
    var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
    e.target.value = !x[2] ? x[1] : '(' + x[1] + ')' + x[2] + (x[3] ? '-' + x[3] : '')
  }
  console.log('e',e.target.value);
  console.log('index',index);
  console.log('type',type);
  console.log('phone',$('#PHONE-'+index).val());
  console.log('phone',$('#FAX-'+index).val());
  console.log('EMAIL',$('#EMAIL-'+index).val());
  if ($('#PHONE-'+index).val() == '' && $('#FAX-'+index).val() == '' && $('#EMAIL-'+index).val() == '') {
    if (this.contactTableData.length > 1) {
      // $('#CHECKBOX-'+index).removeAttr('checked');
      $('#CHECKBOX-'+index).prop("checked", false);
    }
    else{
      $('#BTN-'+index).attr('disabled','true');
    }

    $('#EMAILERROR-'+index).css("display","none");
    // this.contactTableData[index].checkEnalbe = 0;
    $('#CHECKBOX-'+index).attr('disabled','true')
  }
  else{
    if ($('#PHONE-'+index).val() == '' && $('#FAX-'+index).val() == '' && $('#EMAIL-'+index).val() != '') {
      var regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
      var vemail = regexp.test(e.target.value);
      // console.log('vemail',vemail);

      if (vemail == false) {
        $('#EMAILERROR-'+index).css("display","inline-block");
        if (this.contactTableData.length > 1) {

}
else{
  $('#BTN-'+index).attr('disabled','true');
}
        $('#CHECKBOX-'+index).attr('disabled','true')
      }
      else{
        $('#EMAILERROR-'+index).css("display","none");
        $('#BTN-'+index).removeAttr('disabled');
        // this.contactTableData[index].checkEnalbe = 1;
        $('#CHECKBOX-'+index).removeAttr('disabled')
      }
    }
    else if ($('#PHONE-'+index).val() != '' && $('#FAX-'+index).val() == '' && $('#EMAIL-'+index).val() != '') {
      var regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
      var vemail = regexp.test(e.target.value);
      // console.log('vemail',vemail);
      if (vemail == false) {
        $('#EMAILERROR-'+index).css("display","inline-block");
        if (this.contactTableData.length > 1) {

}
else{
  $('#BTN-'+index).attr('disabled','true');
}
        $('#CHECKBOX-'+index).attr('disabled','true')
      }
      else{
        $('#EMAILERROR-'+index).css("display","none");
        $('#BTN-'+index).removeAttr('disabled');
        // this.contactTableData[index].checkEnalbe = 1;
        $('#CHECKBOX-'+index).removeAttr('disabled')
      }
    }
    else if ($('#PHONE-'+index).val() != '' && $('#FAX-'+index).val() != '' && $('#EMAIL-'+index).val() != '') {
      var regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
      var vemail = regexp.test(e.target.value);
      // console.log('vemail',vemail);
      if (vemail == false) {
        $('#EMAILERROR-'+index).css("display","inline-block");
        if (this.contactTableData.length > 1) {

}
else{
  $('#BTN-'+index).attr('disabled','true');
}
        $('#CHECKBOX-'+index).attr('disabled','true')
      }
      else{
        $('#EMAILERROR-'+index).css("display","none");
        $('#BTN-'+index).removeAttr('disabled');
        $('#CHECKBOX-'+index).removeAttr('disabled')
      }
    }
    else if ($('#PHONE-'+index).val() != '' || $('#FAX-'+index).val() != '' && $('#EMAIL-'+index).val() == '') {
      console.log('here');

      $('#BTN-'+index).removeAttr('disabled');
      $('#EMAILERROR-'+index).css("display","none");
      $('#CHECKBOX-'+index).removeAttr('disabled')
    }
  }

}

clientChanged(){
  console.log("client Changed-----",this.clientSearchString);
  this.requestData.data.parentClientId = this.clientSearchString;
  // this.saveRequest.data.clientId = this.clientSearchString.clientId;


}

}
