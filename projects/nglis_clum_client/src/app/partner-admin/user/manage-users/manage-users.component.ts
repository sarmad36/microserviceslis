import { Component, OnInit, ElementRef, ViewChild, ViewChildren, OnDestroy, ComponentFactoryResolver, Renderer2, ComponentRef, ViewContainerRef, QueryList} from '@angular/core';
import { environment } from '../../../../../../../src/environments/environment';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { AppSettings } from '../../../services/_services/app.setting';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { HttpRequestsService } from '../../../services/_services/http-requests/http-requests.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { NgxUiLoaderService } from 'ngx-ui-loader';
declare var $ :any;
import Swal from 'sweetalert2';
import { NotifierService } from 'angular-notifier';
import { UserApiEndpointsService } from '../../../services/users/user-api-endpoints.service';
import { ChildComponent } from './child/child.component';
import { GlobalySharedModule } from './../../../../../../../src/app/sharedmodules/globaly-shared/globaly-shared.module';
// import { EncryptPipe, DecryptPipe } from './../../../../../../../src/app/pipes/encrypt-decrypt.pipe';
import { UrlGuard } from './../../../../../../../src/services/gaurd/url.guard'
import * as CryptoJS from 'crypto-js';

@Component({
  selector   : 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls  : ['./manage-users.component.css'],
  host       : {
    class    :'mainComponentStyle'
  }
})
export class ManageUsersComponent implements OnDestroy, OnInit {
  @ViewChildren(DataTableDirective)
  // @ViewChild(DataTableDirective, {static: false})
  // dtElement              : DataTableDirective;
  dtElement              : QueryList<any>;
  // dtElement2             : DataTableDirective;
  data: any[];
  private childRow: ComponentRef<ChildComponent>;
  // dtOptions              : any = {};
  showNested             = false;
  userShowData           = [];
  allUsers               ;
  totalUsersCount        = 500;
  totalUsersCount2       = 500;
  internalUsers          : any [];
  externalUsers          : any [];
  appendIcons            ;
  deleteUserID           ;
  clientToDeleteName     :any [];
  public dtOptions       : DataTables.Settings[] = [];
  public dtOptions2       : DataTables.Settings = {};
  // public paginatePage    : any;
  dtTrigger              : Subject<ManageUsersComponent> = new Subject();
  dtTrigger2             : Subject<ManageUsersComponent> = new Subject();
  public paginatePage    : any = 0;
  public paginatePage2   : any = 0;
  public paginateLength  = 20;
  totaUsersRecieved      ;
  viewClientUserAction = false;
  manageUserInternalAction = false;
  manageUserExternalAction = false;
  public totalUsersReq   =
  {
    header              : {
      uuid              : "",
      partnerCode       : "",
      userCode          : "",
      referenceNumber   : "",
      systemCode        : "",
      moduleCode        : "",
      functionalityCode: "USRA-MUI",
      systemHostAddress : "",
      remoteUserAddress : "",
      dateTime          : ""
    },
    data                : {
      partnerCode         : 'sip'
    }
  }

  public requestData     =
  {
    header              : {
      uuid              : "",
      partnerCode       : "",
      userCode          : "",
      referenceNumber   : "",
      systemCode        : "",
      moduleCode        : "",
      functionalityCode: "USRA-MUI",
      systemHostAddress : "",
      remoteUserAddress : "",
      dateTime          : ""
    },
    data                : {
      partnerCode         : 'sip',
      userType          : 1,
      pageNumber        : 0,
      pageSize          : 20,
      sortColumn        : 'userCode',
      sortType          : 'desc'
    }
  }

  public disableUserReq  =
  {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode  : "",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 : {
      userCode           : 1,
      status             : false
    }
  }
  swalStyle              : any;
  oldCharacterCount      = 0;
  oldCharacterCount2      = 0;

  public searchData      =
  {
    header              : {
      uuid              : "",
      partnerCode       : "",
      userCode          : "",
      referenceNumber   : "",
      systemCode        : "",
      moduleCode        : "",
      functionalityCode: "USRA-MUI",
      systemHostAddress : "",
      remoteUserAddress : "",
      dateTime          : ""
    },
    data                : {
      partnerCode         : 'sip',
      userType          : 1,
      searchQuery       : "",
      pageNumber        : 0,
      pageSize          : 20,
      sortColumn        : 'userCode',
      sortType          : 'desc'
    }
  }

  public externalCLient = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"CLIM",
      functionalityCode:"",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      page:"0",
      size:"20",
      sortColumn:"userCode",
      sortingOrder:"asc",
      searchString:"131"
    }
  }
  showClientData = [];
  validProvidedEmail  = false;

  public updateclientusercontact : any = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"CLIM",
      functionalityCode:"",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      contactId    :"",
      contactTypeId:"",
      value:"",
      createdBy:1,///// loggedin user
      createdTimestamp: "2020-09-17T04:13:52.551+00:00",
      updatedBy:850,///// loggedin user who has changed it
      clientId:387,
      userCode:567,
      updatedTimestamp:"",
      moduleCode:"CLTM",
      defaultContact:0,
    }
  }

  checkrowclick = "";
  public deleteClientUserData = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"CLIM",
      functionalityCode:"",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      clientId:"",
      userCode:"",
    }
  }
  deleteuserCode           ;
  deleteclientID           ;
  public logedInUserRoles :any = {};
  selectedClientID;
  selectedClientindex

  constructor(
    private formBuilder  : FormBuilder,
    private http         : HttpClient,
    private router       : Router ,
    private httpRequest  : HttpRequestsService,
    private ngxLoader    : NgxUiLoaderService,
    private  notifier    : NotifierService,
    private userService  : UserApiEndpointsService,
    private compFactory: ComponentFactoryResolver,
    private viewRef: ViewContainerRef,
    private _renderer: Renderer2,
    private rbac         : UrlGuard,
    private ecryptDecrypt : GlobalySharedModule,
    // private decryptPipe : DecryptPipe,

  ) {

    // this.rbac.checkROle('USRA-MUI');


    this.notifier        = notifier;
    this.swalStyle       =  Swal.mixin({
      customClass : {
        confirmButton : 'btn btn-success my-swal-success',
        cancelButton  : 'btn btn-danger'
      },
      buttonsStyling  : true
    })
  }
  // someClickHandler(info: any): void {
  //   this.checkrowclick = info.id + ' - ' + info.firstName;
  //   console.log("this.checkrowclick", this.checkrowclick);
  //
  // }
  ngOnInit(): void {

    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
    var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
    var originalText = bts.toString(CryptoJS.enc.Utf8);
    this.logedInUserRoles = JSON.parse(originalText)
    console.log('this.logedInUserRoles',this.logedInUserRoles);
    if(this.logedInUserRoles['allowedRoles'].indexOf("USRA-UUE") != -1){
      this.viewClientUserAction = true;

    }
    if(this.logedInUserRoles['allowedRoles'].indexOf("USRA-MUI") != -1){
      this.manageUserInternalAction = true;

    }
    if(this.logedInUserRoles['allowedRoles'].indexOf("USRA-MUE") != -1){
      this.manageUserExternalAction = true;

    }


    if(this.logedInUserRoles['allowedRoles'].indexOf('USRA-MUI') == -1 && this.logedInUserRoles['allowedRoles'].indexOf('USRA-MUE') == -1){
      this.router.navigate(['page-restrict']);

    }
    this.notifier.hideAll();

    var forexternal = JSON.parse(localStorage.getItem('toExternaTab'))
    // console.log("forexternal",forexternal);

    if (forexternal == true) {
      // $('#external-users').tab('show');
      $('#external-tab').trigger('click');
      localStorage.removeItem("toExternaTab");

    }



    this.dtOptions[0] = {
      pagingType   : 'full_numbers',
      pageLength   : this.paginateLength,
      serverSide   : true,
      processing   : true,
      // ordering     : false,
      "order": [[ 0, "desc" ]],
      columnDefs: [
        { orderable: true, className: 'reorder', targets: [0,2,3,4,5,6,7] },
        { orderable: false, targets: '_all' }
      ],
      "lengthMenu": [20, 50, 75, 100 ],
      ajax: (dataTablesParameters: any, callback) => {
        // console.log("this.dtElement",this.dtElement);
        console.log("dataTablesParameters",dataTablesParameters);

        // if(typeof this.dtElement['dt'] != 'undefined'){
        //     this.paginatePage           = this.dtElement['dt'].page.info().page;
        //     this.paginateLength         = this.dtElement['dt'].page.len();
        //     console.log("this.dtElement['dt']: ", this.dtElement['dt']);
        // }
        this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
          dtElement.dtInstance.then((dtInstance: any) => {
            // console.log("index",index +"--"+ dtInstance.table().node().id);
            if (dtInstance.table().node().id == 'users-internal' ) {

              // console.log("internal",dtElement['dt']);
              // console.log("internal page",dtElement['dt'].page.info().page);
              // console.log("internal page len",dtElement['dt'].page.len());
              this.paginatePage           = dtElement['dt'].page.info().page;
              this.paginateLength         = dtElement['dt'].page.len();
              var sortColumn = dataTablesParameters.order[0]['column'];
              var sortOrder  = dataTablesParameters.order[0]['dir'];
              var sortArray  = ["userCode",'userRoles','fullName','username','phone','npi','email','active']


              // console.log("dataTablesParameters",dataTablesParameters);
              // console.log("this.oldCharacterCount",this.oldCharacterCount);

              // this.requestData.data.pageNumber = this.paginatePage;
              // this.requestData.data.pageSize   = this.paginateLength;
              if (dataTablesParameters.search.value == "" && (this.oldCharacterCount == 0 || this.oldCharacterCount == 3)) {
                if (this.oldCharacterCount == 3) {
                  // $("#LoaderModal").modal({
                  //   backdrop: 'static',
                  //   keyboard: false
                  // });
                  // $("#LoaderModal").modal('show');
                }
                this.requestData.data.pageNumber = this.paginatePage;
                this.requestData.data.pageSize   = this.paginateLength;
                this.requestData.data.userType   = 1;
                this.oldCharacterCount     = 3;
                this.requestData.header.userCode    = this.logedInUserRoles.userCode
                this.requestData.header.partnerCode = this.logedInUserRoles.partnerCode;
                this.requestData.header.functionalityCode = "USRA-MUI";
                this.requestData.data.partnerCode = this.logedInUserRoles.partnerCode;
                this.requestData.data.sortColumn    = sortArray[sortColumn];
                this.requestData.data.sortType      = sortOrder;
                // let url    = environment.API_USER_ENDPOINT + 'partnerusers?partnerCode=1&pageNumber='+ this.paginatePage +'&pageSize=' + this.paginateLength;
                let url    = environment.API_USER_ENDPOINT + 'partnerusers';


                // dataTablesParameters.pageNumber = this.paginatePage;
                this.http.put(url,this.requestData).toPromise()
                .then( resp => {
                  console.log('**** resp: ', resp);
                  // var a = this.ecryptDecrypt.decryptString(resp['data']['users'][0]['userCode']);
                  // console.log("a",a);
                  // console.log("this.decryptPipe.transform('ssss');",this.ecryptDecrypt.encryptString(a));
                  this.allUsers                 = resp['data']['users'];
                  this.internalUsers            = this.allUsers;
                  // this.externalUsers            = this.allUsers;
                  // this.totaUsersRecieved        = 5;
                  this.totalUsersCount          = resp['data']['totalUsers'];
                  // $("#LoaderModal").modal("hide");
                  // this.totaUsersRecieved        = resp.length
                  // // console.log('this.dtOptions.pageLength',this.dtOptions.pageLength);
                  callback({
                    // recordsTotal    :  this.totaUsersRecieved,
                    // recordsFiltered :  this.totaUsersRecieved,
                    recordsTotal    :  this.totalUsersCount,
                    recordsFiltered :  this.totalUsersCount,

                    // recordsFiltered: resp['recordsFiltered'],
                    // recordsFiltered: 1000,
                    data: []
                  });
                  $('div.dataTables_length select').addClass('dt-form-control');
                  $('div.dataTables_filter input').addClass('dt-form-control');
                })
                .catch(error => {
                  // Set loader false
                  // this.loading = false;

                  console.log("error: ",error);
                });
              }
              else{
                if (dataTablesParameters.search.value.length >=3) {
                  this.searchData.data.pageNumber = this.paginatePage;
                  this.searchData.data.pageSize   = this.paginateLength;
                  this.searchData.data.userType   = 1;
                  this.searchData.header.userCode    = this.logedInUserRoles.userCode
                  this.searchData.header.partnerCode = this.logedInUserRoles.partnerCode
                  this.searchData.header.functionalityCode = "USRA-MUI"
                  this.searchData.data.partnerCode   = this.logedInUserRoles.partnerCode
                  this.searchData.data.sortColumn    = sortArray[sortColumn];
                  this.searchData.data.sortType      = sortOrder;
                  // $("#LoaderModal").modal({
                  //   backdrop: 'static',
                  //   keyboard: false
                  // });
                  let url    = environment.API_USER_ENDPOINT + 'search';
                  // console.log('this.paginatePage: ',this.paginatePage);
                  this.searchData.data.searchQuery = dataTablesParameters.search.value;
                  // console.log("this.searchData.data.searchQuery ",this.searchData.data.searchQuery);

                  // dataTablesParameters.pageNumber = this.paginatePage;
                  this.http.put(url,this.searchData).toPromise()
                  .then( resp => {
                    console.log('**** resp: ', resp);
                    this.allUsers                 = resp['data']['users'];
                    this.internalUsers            = this.allUsers;
                    // this.externalUsers            = this.allUsers;
                    // this.totaUsersRecieved        = 5;
                    this.totalUsersCount          = resp['data']['totalUsers'];
                    // $("#LoaderModal").modal("hide");
                    // this.totaUsersRecieved        = resp.length
                    // // console.log('this.dtOptions.pageLength',this.dtOptions.pageLength);
                    callback({
                      // recordsTotal    :  this.totaUsersRecieved,
                      // recordsFiltered :  this.totaUsersRecieved,
                      recordsTotal    :  this.totalUsersCount,
                      recordsFiltered :  this.totalUsersCount,

                      // recordsFiltered: resp['recordsFiltered'],
                      // recordsFiltered: 1000,
                      data: []
                    });
                    $('div.dataTables_length select').addClass('dt-form-control');
                    $('div.dataTables_filter input').addClass('dt-form-control');
                  })
                  .catch(error => {
                    // Set loader false
                    // this.loading = false;

                    console.log("error: ",error);
                  });
                  this.oldCharacterCount     = 3;
                }
                else{
                  $('.dataTables_processing').css('display',"none");
                  if (this.oldCharacterCount == 3) {
                    this.oldCharacterCount     = 3
                  }
                  else{
                    this.oldCharacterCount     = 2;
                  }
                }
              }

            }
            // console.log(`The DataTable ${index} instance ID is: ${dtInstance.table().node().id}`);
          });
        });



      },
      // columns  : [
      //   { data : 'SPD-002' },
      //   { data : 'firstName' },
      //   { data : 'lastName' },
      //   { data : 'username' },
      //   { data : 'phone' },
      //   { data : 'email' },
      //   { data : 'action' }
      // ]
      ///for search
    };
    // this.dtOptions2 = {
    //   pagingType: 'full_numbers',
    //   pageLength: 5,
    //   processing: true
    // };
    // this.loadDummyData()
    this.dtOptions[1] = {
      pagingType   : 'full_numbers',
      pageLength   : this.paginateLength,
      serverSide   : true,
      processing   : true,
      // ordering     : false,
      "order": [[ 0, "desc" ]],
      columnDefs: [
        { orderable: true, className: 'reorder', targets: [1,3,4,5,6,7,8] },
        { orderable: false, targets: '_all' }
      ],
      responsive   :true,
      "lengthMenu": [20, 50, 75, 100 ],
      ajax: (dataTablesParameters: any, callback) => {
        // console.log('this.dtElement2',this.dtElement);
        // console.log('this.dtElement2.Last',this.dtElement.last);

        // if(typeof this.dtElement['dt'] != 'undefined'){
        //     this.paginatePage2          = this.dtElement['dt'].page.info().page;
        //     this.paginateLength         = this.dtElement['dt'].page.len();
        //     // console.log("this.dtElement['dt']: ", this.dtElement['dt']);
        // }
        this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
          dtElement.dtInstance.then((dtInstance: any) => {
            // console.log("index",index +"--"+ dtInstance.table().node().id);
            // if(index == 1){
            if(dtInstance.table().node().id == 'users-external'){
              // console.log("External",dtElement['dt']);
              // console.log("External page",dtElement['dt'].page.info().page);
              // console.log("External page len",dtElement['dt'].page.len());
              this.paginatePage2           = dtElement['dt'].page.info().page;
              this.paginateLength         = dtElement['dt'].page.len();
              // console.log('this.paginatePage2: ',this.paginatePage2);
              var sortColumn = dataTablesParameters.order[0]['column'];
              var sortOrder  = dataTablesParameters.order[0]['dir'];
              var sortArray  = ["userCode","userCode",'userRoles','fullName','username','phone','npi','email','userCode','active']


              if (dataTablesParameters.search.value == "" && (this.oldCharacterCount2 == 0 || this.oldCharacterCount2 == 3)) {
                this.requestData.data.pageNumber = dtElement['dt'].page.info().page;
                this.requestData.data.pageSize   = this.paginateLength;
                this.requestData.data.userType   = 2;
                this.requestData.header.userCode    = this.logedInUserRoles.userCode
                this.requestData.header.partnerCode = this.logedInUserRoles.partnerCode
                this.requestData.header.functionalityCode = "USRA-MUE"
                // if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-UC") != -1) {
                //     this.searchClientName.header.functionalityCode    = "CLTA-UC";
                //   }
                //   else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-VC") != -1){
                //     this.searchClientName.header.functionalityCode    = "CLTA-VC";
                //   }
                this.requestData.data.partnerCode   = this.logedInUserRoles.partnerCode
                this.requestData.data.sortColumn    = sortArray[sortColumn];
                this.requestData.data.sortType      = sortOrder;

                // let url    = environment.API_USER_ENDPOINT + 'partnerusers?partnerCode=1&pageNumber='+ this.paginatePage +'&pageSize=' + this.paginateLength;
                let url    = environment.API_USER_ENDPOINT + 'partnerusers';


                // dataTablesParameters.pageNumber = this.paginatePage;
                this.http.put(url,this.requestData).toPromise()
                .then( resp => {
                  console.log('**** resp External: ', resp);
                  this.allUsers                 = resp['data']['users'];
                  // this.internalUsers            = this.allUsers;
                  this.externalUsers            = this.allUsers;
                  // this.totaUsersRecieved        = 5;
                  this.totalUsersCount2          = resp['data']['totalUsers'];
                  // this.totaUsersRecieved        = resp.length
                  // // console.log('this.dtOptions.pageLength',this.dtOptions.pageLength);
                  callback({
                    // recordsTotal    :  this.totaUsersRecieved,
                    // recordsFiltered :  this.totaUsersRecieved,
                    recordsTotal    :  this.totalUsersCount2,
                    recordsFiltered :  this.totalUsersCount2,

                    // recordsFiltered: resp['recordsFiltered'],
                    // recordsFiltered: 1000,
                    data: []
                  });
                  $('div.dataTables_length select').addClass('dt-form-control');
                  $('div.dataTables_filter input').addClass('dt-form-control');
                })
                .catch(error => {
                  // Set loader false
                  // this.loading = false;

                  console.log("error: ",error);
                });
                this.oldCharacterCount2 = 3;
              }
              else{
                if (dataTablesParameters.search.value.length >= 3) {
                  this.searchData.data.pageNumber  = this.paginatePage2;
                  this.searchData.data.pageSize    = this.paginateLength;
                  this.searchData.data.userType    = 2;
                  this.searchData.data.searchQuery = dataTablesParameters.search.value;
                  this.searchData.header.userCode    = this.logedInUserRoles.userCode
                  this.searchData.header.partnerCode = this.logedInUserRoles.partnerCode
                  this.searchData.header.functionalityCode = "USRA-MUI"
                  this.searchData.data.partnerCode   = this.logedInUserRoles.partnerCode
                  this.searchData.data.sortColumn    = sortArray[sortColumn];
                  this.searchData.data.sortType      = sortOrder;
                  // let url    = environment.API_USER_ENDPOINT + 'partnerusers?partnerCode=1&pageNumber='+ this.paginatePage +'&pageSize=' + this.paginateLength;
                  let url    = environment.API_USER_ENDPOINT + 'search';
                  // console.log('this.paginatePage: ',this.paginatePage);

                  // dataTablesParameters.pageNumber = this.paginatePage;
                  this.http.put(url,this.searchData).toPromise()
                  .then( resp => {
                    console.log('**** resp External: ', resp);
                    this.allUsers                 = resp['data']['users'];
                    // this.internalUsers            = this.allUsers;
                    this.externalUsers            = this.allUsers;
                    // this.totaUsersRecieved        = 5;
                    this.totalUsersCount2          = resp['data']['totalUsers'];
                    // this.totaUsersRecieved        = resp.length
                    // // console.log('this.dtOptions.pageLength',this.dtOptions.pageLength);
                    callback({
                      // recordsTotal    :  this.totaUsersRecieved,
                      // recordsFiltered :  this.totaUsersRecieved,
                      recordsTotal    :  this.totalUsersCount2,
                      recordsFiltered :  this.totalUsersCount2,

                      // recordsFiltered: resp['recordsFiltered'],
                      // recordsFiltered: 1000,
                      data: []
                    });
                    $('div.dataTables_length select').addClass('dt-form-control');
                    $('div.dataTables_filter input').addClass('dt-form-control');
                  })
                  .catch(error => {
                    // Set loader false
                    // this.loading = false;

                    console.log("error: ",error);
                  });

                }
                else{
                  $('.dataTables_processing').css('display',"none");
                  if (this.oldCharacterCount2 == 3) {
                    this.oldCharacterCount2     = 3

                  }
                  else{
                    this.oldCharacterCount2     = 2;
                  }
                }
              }


            }

            // console.log(`The DataTable ${index} instance ID is: ${dtInstance.table().node().id}`);
          });
        });


      },
      // columns  : [
      //   { data : 'SPD-002' },
      //   { data : 'firstName' },
      //   { data : 'lastName' },
      //   { data : 'username' },
      //   { data : 'phone' },
      //   { data : 'email' },
      //   { data : 'action' }
      // ],

      ///for search
    };


    // this.userShowData.push();

  }
  checkInst(){
    console.log("this.dtElement",this.dtElement);

    this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
      // console.log("dtElement********",dtElement);
      // console.log("dtElement********",dtElement['dt']);

      dtElement.dtInstance.then((dtInstance: any) => {
        console.log("index",index +"--"+ dtInstance.table().node().id);
        if (index == 0 ) {
          console.log("internal",dtElement['dt']);
          console.log("internal page",dtElement['dt'].page.info().page);
          console.log("internal page len",dtElement['dt'].page.len());

        }
        else if(index == 1){
          console.log("External",dtElement['dt']);
          console.log("External page",dtElement['dt'].page.info().page);
          console.log("External page len",dtElement['dt'].page.len());
        }

        // console.log(`The DataTable ${index} instance ID is: ${dtInstance.table().node().id}`);
      });
    });
  }
  loadDummyData(){
    this.data = [
      {
        "id": "3",
        "firstName": "Cartman",
        "lastName": "Whateveryournameis"
      },
      {
        "id": "10",
        "firstName": "Cartman",
        "lastName": "Titi"
      },
      {
        "id": "11",
        "firstName": "Toto",
        "lastName": "Lara"
      },
      {
        "id": "22",
        "firstName": "Luke",
        "lastName": "Yoda"
      },
      {
        "id": "26",
        "firstName": "Foo",
        "lastName": "Moliku"
      }
    ];
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
    this.dtTrigger2.next();
    // var url                = environment.API_USER_ENDPOINT + 'totalusers';
    // this.httpRequest.httpPutRequests(url, this.totalUsersReq).then(resp => {
    //   console.log('totalcount',resp)
    //   this.totalUsersCount = resp.data;
    //   this.dtTrigger.next();
    // })
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
    this.dtTrigger2.unsubscribe();
  }
  getSelectedUserData(userID){
    // console.log('userID',userID);
    this.ngxLoader.start();
    var ax = CryptoJS.AES.encrypt(userID, 'nglis', { outputLength: 224 }).toString();
    var bx = ax.replace('+','xsarm');
    var cx = bx.replace('/','ydan');
    var ciphertext = cx.replace('=','zhar');
    // var ciphertext = CryptoJS.AES.encrypt(userID, 'nglis', { outputLength: 224 }).toString().replace('+','xsarm').replace('/','ydan').replace('=','zhar');
    console.log("ciphertext",ciphertext);

    //
    //
    // var coverted = ciphertext.replace('xsarm','+').replace('ydan','/').replace('zhar','=');
    // console.log("coverted",coverted);
    //
    // var bytes  = CryptoJS.AES.decrypt(coverted, 'nglis');
    //
    //
    // var originalText = bytes.toString(CryptoJS.enc.Utf8).toString();
    // console.log("originalText",originalText);


    this.router.navigate(['edit-user', ciphertext]);




    // this.router.navigate(['edit-user', userID]);
    // let url  = environment.API_USER_ENDPOINT + 'getuser?userId=1';
    // this.httpRequest.httpGetRequests(url).then(resp => {
    //   console.log("resp",resp);
    //   this.router.navigate(['edit-user', userID]);
    //   // this.router.navigateByUrl('/edit-user', { state: resp });
    //   // this.router.navigate(['/editlisting', JSON.stringify(resp)]);
    // })

  }
  disableUser(userID, status, userIndex){
    // this.ngxLoader.start();
    console.log('userID',userID);
    console.log('this.disableUserReq',this.disableUserReq);
    // let url  = environment.API_USER_ENDPOINT + 'deactive?userId='+userID+'';
    this.disableUserReq.data.userCode = userID
    this.disableUserReq.header.userCode    = this.logedInUserRoles.userCode
    this.disableUserReq.header.partnerCode = this.logedInUserRoles.partnerCode
    if (this.logedInUserRoles['allowedRoles'].indexOf("USRA-MUI") != -1) {
      this.disableUserReq.header.functionalityCode    = "USRA-MUI";
    }
    else if (this.logedInUserRoles['allowedRoles'].indexOf("USRA-MUE") != -1){
      this.disableUserReq.header.functionalityCode    = "USRA-MUE";
    }
    // this.disableUserReq.header.functionalityCode = "USRA-MUI"
    // this.searchData.data.partnerCode   = this.logedInUserRoles.partnerCode
    let url  = environment.API_USER_ENDPOINT + 'updatestatus';
    if (status == 1) {
      this.disableUserReq.data.status = true;
      this.userService.updateStatus(url, this.disableUserReq).subscribe(resp => {
        console.log('disable resp:', resp);
        this.ngxLoader.stop();
        if (resp['result'].codeType == "S") {
          this.notifier.notify( "success", "User enabled Successfully" );
          // $('.TR-'+userID).css('display','none');
          this.internalUsers[userIndex].active = true;

        }
        else{
          this.notifier.notify( "error", "Error while disabling user");
          this.internalUsers[userIndex].active = false;
        }
        // this.router.navigateByUrl('/edit-user', { state: resp });
        // this.router.navigate(['/editlisting', JSON.stringify(resp)]);
      })
    }
    else{
      this.disableUserReq.data.status = false;
      Swal.fire({
        title: 'Are you sure?',
        text: "User wont be able to access the portal!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, deactivate it!'
      }).then((result) => {
        if (result.value) {
          this.ngxLoader.start();
          this.userService.updateStatus(url, this.disableUserReq).subscribe(resp => {
            console.log('disable resp:', resp);
            this.ngxLoader.stop();
            if (resp['result'].codeType == "S") {
              this.notifier.notify( "success", "User disabled Successfully" );
              // $('.TR-'+userID).css('display','none');
              this.internalUsers[userIndex].active = false;

            }
            else{
              this.notifier.notify( "error", "Error while disabling user");

            }
            // this.router.navigateByUrl('/edit-user', { state: resp });
            // this.router.navigate(['/editlisting', JSON.stringify(resp)]);
          })
        }
      })

    }


  }

  disableUserExternal(userID, status, userIndex){
    // this.ngxLoader.start();
    console.log('userID',userID);
    console.log('status',status);
    console.log('userIndex',userIndex);

    // let url  = environment.API_USER_ENDPOINT + 'deactive?userId='+userID+'';
    this.disableUserReq.header.userCode    = this.logedInUserRoles.userCode
    this.disableUserReq.header.partnerCode = this.logedInUserRoles.partnerCode
    if (this.logedInUserRoles['allowedRoles'].indexOf("USRA-MUI") != -1) {
      this.disableUserReq.header.functionalityCode    = "USRA-MUI";
    }
    else if (this.logedInUserRoles['allowedRoles'].indexOf("USRA-MUE") != -1){
      this.disableUserReq.header.functionalityCode    = "USRA-MUE";
    }
    // this.disableUserReq.header.functionalityCode = "USRA-MUI"
    this.disableUserReq.data.userCode = userID
    console.log('this.disableUserReq',this.disableUserReq);
    let url  = environment.API_USER_ENDPOINT + 'updatestatus';
    if (status == 1) {
      this.disableUserReq.data.status = true;
      this.userService.updateStatus(url, this.disableUserReq).subscribe(resp => {
        console.log('disable resp:', resp);
        this.ngxLoader.stop();
        if (resp['result'].codeType == "S") {
          this.notifier.notify( "success", "User enabled Successfully" );
          // $('.TR-'+userID).css('display','none');
          this.externalUsers[userIndex].active = true;
          this.showClientData[userIndex].clickedUser.active = true;

        }
        else{
          this.notifier.notify( "error", "Error while disabling user");
          this.externalUsers[userIndex].active = false;
          this.showClientData[userIndex].clickedUser.active = false;
        }
        // this.router.navigateByUrl('/edit-user', { state: resp });
        // this.router.navigate(['/editlisting', JSON.stringify(resp)]);
      })
    }
    else{
      this.disableUserReq.data.status = false;
      Swal.fire({
        title: 'Are you sure?',
        text: "You want to disabe this user!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, deactivate it!'
      }).then((result) => {
        if (result.value) {
          this.ngxLoader.start();
          this.userService.updateStatus(url, this.disableUserReq).subscribe(resp => {
            console.log('disable resp:', resp);
            this.ngxLoader.stop();
            if (resp['result'].codeType == "S") {
              this.notifier.notify( "success", "User disabled Successfully" );
              // $('.TR-'+userID).css('display','none');
              this.externalUsers[userIndex].active = false;
              this.showClientData[userIndex].clickedUser.active = false;

            }
            else{
              this.notifier.notify( "error", "Error while disabling user");

            }
            // this.router.navigateByUrl('/edit-user', { state: resp });
            // this.router.navigate(['/editlisting', JSON.stringify(resp)]);
          })
        }
      })

    }


  }
  nested(id, trRef){
    this.ngxLoader.start();
    console.log('here',id);
    console.log("tref",trRef);
    ////// console.log("clientUsers-external",$('#clientUsers-external')[0]);
    ////// tref.append($('#clientUsers-external')[0]);

    //  this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
    //   dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
    //     // console.log("index",index +"--"+ dtInstance.table().node().id);
    //     // if (dtInstance.table().node().id == 'users-external' ) {
    //       // var el = (document.getElementById("TR-"+id));
    //       // console.log("--------------",dtInstance.table());
    //       // console.log("--------------",dtInstance.table().row());
    //
    //
    //     console.log("tref",trRef);
    //      console.log("IDDD",$("#TR-"+id));
    //       // var row = dtInstance.row("#TR-"+id);
    //       // if (row.child.isShown()) {
    //       //     row.child.hide();
    //       //    this._renderer.removeClass(trRef, 'shown');
    //       //   }
    //       //   else {
    //       //     // let factory = this.compFactory.resolveComponentFactory(ChildComponent);
    //       //     // this.childRow = this.viewRef.createComponent(factory);
    //       //     // this.childRow.instance.data = [rowData];
    //       //     // this.childRow
    //       //     // row.child(this.childRow.location.nativeElement).show();
    //       //     // row.child(this.childRow.location.nativeElement).show();
    //       //
    //       //     var a =$('#clientUsers-external').html();
    //       //     row.child(a).show();
    //       //     this._renderer.addClass(trRef, 'shown');
    //       //   }
    //       // // console.log("row",row);
    //       // // console.log("child",row.child());
    //       // // console.log("shown",row.child.isShown());
    //       // console.log("shown",$('#clientUsers-external').html());
    //       // var a =$('#clientUsers-external').html();
    //       // row.child($('#clientUsers-external')).show();
    //       // row.child($('#clientUsers-external')[0]).show();
    //       // console.log("row.child($('#clientUsers-external')[0])",row.child($('#clientUsers-external')[0]));
    //
    //
    //       // this._renderer.addClass(trRef, "shown");
    //      // this._renderer.addClass(trRef, "shown");
    //
    //
    //       // console.log("internal",dtElement['dt']);
    //       // console.log("internal page",dtElement['dt'].page.info().page);
    //       // console.log("internal page len",dtElement['dt'].page.len());
    //       this.paginatePage           = dtElement['dt'].page.info().page;
    //       this.paginateLength         = dtElement['dt'].page.len();
    //
    //
    //     // }
    //     // console.log(`The DataTable ${index} instance ID is: ${dtInstance.table().node().id}`);
    //   });
    // });

    let url    = environment.API_CLIENT_ENDPOINT + 'clientsbyusercode';
    this.externalCLient.data.searchString = id;
    this.externalCLient.header.userCode    = this.logedInUserRoles.userCode;
    this.externalCLient.header.partnerCode = this.logedInUserRoles.partnerCode;
    this.externalCLient.header.functionalityCode = "USRA-MUE";
    this.httpRequest.httpPostRequests(url, this.externalCLient).then(resp => {
      console.log("resp",resp);
      for (let i = 0; i < this.externalUsers.length; i++) {
        if (id == this.externalUsers[i].userCode) {
          var clickeduser = this.externalUsers[i];
          console.log('clickeduser',clickeduser);
          for (let j = 0; j < resp['data']['details'].length; j++) {
            resp['data']['details'][j].clickedUser = clickeduser;

          }
        }
      }
      console.log("resp['details']",resp['data']['details']);
      var temporaryContact   = []

      for (let i = 0; i < resp['data']['details'].length; i++) {
        if (resp['data']['details'][i].contactDto.length > 0) {
          temporaryContact   = []

          for(let j = 0; j < resp['data']['details'][i].contactDto.length; j++){
            if (resp['data']['details'][i].contactDto[j] != null) {


              // const found = temporaryContact.some(el => el.contactTypeId === resp['data']['details'][i].contactDto[j]['contactTypeId']);
              // if(found == false){
              if (resp['data']['details'][i].contactDto[j]['contactTypeId'] == 2) {
                // console.log('email',resp['details'][i].contactDto[j]['value']);
                resp['data']['details'][i].contactDto[j]['value'] = this.ecryptDecrypt.decryptString(resp['data']['details'][i].contactDto[j]['value']);
                temporaryContact[0] = resp['data']['details'][i].contactDto[j]
              }
              else{
                if (typeof temporaryContact[0] == 'undefined') {
                  temporaryContact[0] = '';
                }
              }

              if(resp['data']['details'][i].contactDto[j]['contactTypeId'] == 3){
                // console.log('fax',resp['data']['details'][i].contactDto[j]['value']);
                resp['data']['details'][i].contactDto[j]['value'] = this.ecryptDecrypt.decryptString(resp['data']['details'][i].contactDto[j]['value']);

                temporaryContact[1] = resp['data']['details'][i].contactDto[j]
              }
              else{
                if (typeof temporaryContact[1] == 'undefined') {
                  temporaryContact[1] = '';
                }
              }
              // }
            }
          }
        }
        else{
          console.log("<<0",resp['data']['details'][i].contactDto);
          temporaryContact[0]     = "";
          temporaryContact[0]     = "";
          temporaryContact[1]     = "";
          temporaryContact[1]     = "";
        }
        resp['data']['details'][i].contactDetails = temporaryContact;

      }
      // console.log("temporaryContact",temporaryContact);



      this.showClientData = resp['data']['details'];

    })
    // $('#collapseContact').collapse('show');



    this.ngxLoader.stop();
    $('#contactDetailModal').modal('show');
  }

  addClientProvidedEmail(userCode, e, selectedClientID){
    // console.log('clientProvidedEmail',this.clientProvidedEmail);
    // console.log('capmail',e.target.value);

    console.log('cu-getAttribute("data-value")',e.target.getAttribute("cu-contactId"));
    // console.log('userCode',userCode);
    $('#email-spiner-'+userCode).css("display","inline-block");
    var regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    var vemail = regexp.test(e.target.value);
    if (vemail) {
      this.validProvidedEmail     = true;
      $('#email-spiner-'+userCode).css("display","inline-block");
      $('#email-error-'+userCode).css("display","none");
      // console.log("true");
    }
    else{
      // console.log("false");
      // alert("invalid Email");
      this.validProvidedEmail     = false;
      $('#email-spiner-'+userCode).css("display","none");
      $('#email-error-'+userCode).css("display","inline-block");
      return
    }
    if (e.target.getAttribute("cu-contactId") == null || typeof e.target.getAttribute("cu-contactId") == "undefined") {
      // this.updateclientusercontact.data.contactId   = '';
      this.updateclientusercontact.data.contactId   = null;
    }
    else{
      this.updateclientusercontact.data.contactId   = parseInt(e.target.getAttribute("cu-contactId"));
    }
    this.updateclientusercontact.data.value         = this.ecryptDecrypt.encryptString(e.target.value);
    this.updateclientusercontact.data.contactTypeId = 2;
    this.updateclientusercontact.data.clientId      = selectedClientID;
    this.updateclientusercontact.data.userCode        = userCode;
    this.updateclientusercontact.data.createdBy    = this.logedInUserRoles.userCode;
    this.updateclientusercontact.header.userCode    = this.logedInUserRoles.userCode;
    this.updateclientusercontact.header.partnerCode = this.logedInUserRoles.partnerCode;
    if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-UC") != -1) {
            this.updateclientusercontact.header.functionalityCode = "CLTA-UC";

      }
      else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-VC") != -1){
        this.updateclientusercontact.header.functionalityCode    = "CLTA-VC";
      }
    // this.updateclientusercontact.header.functionalityCode = "USRA-MUI";

    console.log("updateclientusercontact",this.updateclientusercontact);


    var url                = environment.API_CLIENT_ENDPOINT + 'updateclientusercontact';
    this.httpRequest.httpPostRequests(url, this.updateclientusercontact).then(resp => {
      console.log('Client Provided Email',resp);
      if (resp['data'].contactId != null || resp['data'].contactId != '') {
        e.target.setAttribute("cu-contactId",resp['data'].contactId)
      }
      $('#email-spiner-'+userCode).css("display","none");
    })

  }
  addClientProvidedFax(userCode, e, selectedClientID){
    // console.log('clientProvidedFax',this.clientProvidedFax);
    // console.log('cpfax',e.target.value);
    // console.log('userCode',userCode);
    $('#fax-spiner-'+userCode).css("display","inline-block");
    if (e.target.getAttribute("cu-contactId") == null || typeof e.target.getAttribute("cu-contactId") == "undefined") {
      // this.updateclientusercontact.data.contactId   = '';
      this.updateclientusercontact.data.contactId   = null;
    }
    else{
      this.updateclientusercontact.data.contactId   = parseInt(e.target.getAttribute("cu-contactId"));
    }
    this.updateclientusercontact.data.value         = this.ecryptDecrypt.encryptString(e.target.value);
    this.updateclientusercontact.data.contactTypeId = 3;
    this.updateclientusercontact.data.clientId      = selectedClientID;
    this.updateclientusercontact.data.userCode        = userCode;
    this.updateclientusercontact.header.userCode    = this.logedInUserRoles.userCode;
    this.updateclientusercontact.data.createdBy    = this.logedInUserRoles.userCode;
    this.updateclientusercontact.header.partnerCode = this.logedInUserRoles.partnerCode;
    if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-UC") != -1) {
      this.updateclientusercontact.header.functionalityCode = "CLTA-UC";

    }
    else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-VC") != -1){
      this.updateclientusercontact.header.functionalityCode    = "CLTA-VC";
    }
    // console.log("$('#email-fax-'+userCode)",$('#email-fax-'+user ID));

    console.log("updateclientusercontact",this.updateclientusercontact);


    var url                = environment.API_CLIENT_ENDPOINT + 'updateclientusercontact';
    this.httpRequest.httpPostRequests(url, this.updateclientusercontact).then(resp => {
      console.log('Client Provided Fax',resp);
      if (resp['data'].contactId != null || resp['data'].contactId != '') {
        e.target.setAttribute("cu-contactId", resp['data'].contactId)
      }
      $('#fax-spiner-'+userCode).css("display","none");
    })

  }
  addClientProvidedPracticeCode(userCode, e, userIndex, selectedClientID){
    var req = {
      header:{
        uuid:"",
        partnerCode:"",
        userCode:"",
        referenceNumber:"",
        systemCode:"",
        moduleCode:"CLIM",
        functionalityCode:"",
        systemHostAddress:"",
        remoteUserAddress:"",
        dateTime:""
      },
      data:
      {
        clientId:1,
        userCode:1,
        superUser:"",
        visibleInCases:"",
        isPhysician:"",
        defaultPhysician:"",
        practiseCode:"",
        addedBy:1,
        addedTimestamp:"",
        active:"",
        isClientPortalAccess:""
      }
    }
    // console.log('clientProvidedPracticeCode',this.clientProvidedPCode);
    console.log('cpcode',e.target.value);
    // console.log('userCode',userCode);

    $('#practice-spiner-'+userCode).css("display","inline-block");
    req.data.visibleInCases   = this.showClientData[userIndex].visibleInCases;
    req.data.superUser        = this.showClientData[userIndex].superUser;
    req.data.active           = this.showClientData[userIndex].active;
    req.data.defaultPhysician = this.showClientData[userIndex].defaultPhysician;
    req.data.isClientPortalAccess   = this.showClientData[userIndex].isClientPortalAccess
    req.data.practiseCode          = e.target.value;
    // req.data.contactTypeId        = 2;
    req.data.clientId              = selectedClientID;
    req.data.userCode                = userCode;
    req.header.userCode    = this.logedInUserRoles.userCode;
    req.header.partnerCode = this.logedInUserRoles.partnerCode;
    req.header.functionalityCode = "USRA-UUE";
    // console.log("$('#email-fax-'+userCode)",$('#email-fax-'+userCode));
    console.log("req",req);

    var url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
    this.httpRequest.httpPostRequests(url, req).then(resp => {
      console.log('Client Provided pRACTICE',resp);
      $('#practice-spiner-'+userCode).css("display","none");
      if (resp['data'].practiseCode == 1 || resp['data'].practiseCode == "1") {
        console.log('practice success');
      }

    })

  }
  addClientProvidedDefault(userCode, e, userIndex, selectedClientID){
    var req = {
      header:{
        uuid:"",
        partnerCode:"",
        userCode:"",
        referenceNumber:"",
        systemCode:"",
        moduleCode:"CLIM",
        functionalityCode:"",
        systemHostAddress:"",
        remoteUserAddress:"",
        dateTime:""
      },
      data:
      {
        clientId:1,
        userCode:1,
        superUser:"",
        visibleInCases:"",
        isPhysician:"",
        defaultPhysician:"",
        practiseCode:"",
        addedBy:1,
        addedTimestamp:"",
        active:"",
        isClientPortalAccess:""
      }
    }
    // console.log('clientProvidedDefault',this.clientProvidedDefault);
    $('#default-spiner-'+userCode).css("display","inline-block");
    console.log('cpdefault',e.target.value);
    console.log('userCode',userCode);
    if (e.target.checked) {
      req.data.defaultPhysician = "1"
    }
    else{
      req.data.defaultPhysician = "0"
    }
    req.data.userCode   = userCode;
    req.data.clientId = selectedClientID;

    req.data.visibleInCases   = this.showClientData[userIndex].visibleInCases;
    req.data.superUser        = this.showClientData[userIndex].superUser;
    req.data.practiseCode     = this.showClientData[userIndex].practiseCode;
    req.data.active           = this.showClientData[userIndex].active;
    req.data.isClientPortalAccess   = this.showClientData[userIndex].isClientPortalAccess
    req.header.userCode    = this.logedInUserRoles.userCode;
    req.header.partnerCode = this.logedInUserRoles.partnerCode;
    req.header.functionalityCode = "USRA-UUE";

    console.log('default',req)
    var url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
    this.httpRequest.httpPostRequests(url, req).then(resp => {
      console.log('Default Resp',resp)
      $('#default-spiner-'+userCode).css("display","none");
      if (typeof resp['data'].clientId !="undefined") {
        console.log("Successfully set to default");
        if (resp['data'].defaultPhysician == 1) {
          console.log('defautl success');

        }
        else{
          this.notifier.getConfig().behaviour.autoHide = 3000;
          this.notifier.notify( "warning", "Unable to set default physician");

        }
      }

      // this.reportFormat = resp;
    })

  }
  superUserCheck(userCode, e, userIndex, selectedClientID){
    console.log("userIndex", userIndex);

    var req = {
      header:{
        uuid:"",
        partnerCode:"",
        userCode:"",
        referenceNumber:"",
        systemCode:"",
        moduleCode:"CLIM",
        functionalityCode:"",
        systemHostAddress:"",
        remoteUserAddress:"",
        dateTime:""
      },
      data:
      {
        clientId:1,
        userCode:1,
        superUser:"",
        visibleInCases:"",
        isPhysician:"",
        defaultPhysician:"",
        practiseCode:"",
        addedBy:1,
        addedTimestamp:"",
        active:"",
        isClientPortalAccess:""
      }
    }
    console.log('userCode',userCode);
    // console.log('userIndex',userIndex);
    console.log('event',e.target.checked);
    if (e.target.checked) {
      req.data.superUser = "1"
    }
    else{
      req.data.superUser = "0"
    }

    req.data.userCode   = userCode;
    req.data.clientId = selectedClientID;

    req.data.visibleInCases   = this.showClientData[userIndex].visibleInCases;
    req.data.defaultPhysician = this.showClientData[userIndex].defaultPhysician;
    req.data.practiseCode     = this.showClientData[userIndex].practiseCode;
    req.data.active           = this.showClientData[userIndex].active;
    req.data.isClientPortalAccess   = this.showClientData[userIndex].isClientPortalAccess
    req.header.userCode    = this.logedInUserRoles.userCode;
    req.header.partnerCode = this.logedInUserRoles.partnerCode;
    req.header.functionalityCode = "USRA-UUE";

    console.log('updateClientUser',req)
    // req.data.userCode = userCode;
    var url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
    this.httpRequest.httpPostRequests(url, req).then(resp => {
      console.log('reportFormat',resp)
      if(resp['data'].superUser == "1"){
        // console.log('this.userShowData[userIndex]',this.userShowData[userIndex]);

        this.showClientData[userIndex].superUser = 1;
        req.data.superUser = "";
        // var a = e.srcElement.parentNode.parentNode.parentNode.nextSibling;
        // a.children[0].style.display="inline";

      }
      else{
        this.showClientData[userIndex].superUser = 0;
        req.data.superUser = "";
        // var a = e.srcElement.parentNode.parentNode.parentNode.nextSibling;
        // a.children[0].style.display="none";

        // this.notifier.notify( "warning", "Unable to set as Super User");


        // this.enableCompleteSuperCheck = this.enableCompleteSuperCheck - 1;
      }

      // this.reportFormat = resp;
    })
  }
  clientPortalAccessCheck(userCode, e, userIndex, selectedClientID){
    console.log('userID',userCode);
    console.log('event',e.target.checked);

    var req = {
      header:{
        uuid:"",
        partnerCode:"",
        userCode:"",
        referenceNumber:"",
        systemCode:"",
        moduleCode:"CLIM",
        functionalityCode:"",
        systemHostAddress:"",
        remoteUserAddress:"",
        dateTime:""
      },
      data:{
        clientId:1,
        userCode:1,
        superUser:"",
        visibleInCases:"",
        isPhysician:"",
        defaultPhysician:"",
        practiseCode:"",
        addedBy:1,
        addedTimestamp:"",
        active:"",
        isClientPortalAccess:""
      }
    }
    console.log('userCode',userCode);
    console.log('event',e.target.checked);
    if (e.target.checked) {
      req.data.isClientPortalAccess = "1"
    }
    else{
      req.data.isClientPortalAccess = "0"
    }
    // console.log('reportFormat',req)
    // req.userCode = userCode;
    req.data.userCode           = userCode;
    req.data.clientId         = selectedClientID;

    req.data.superUser        = this.showClientData[userIndex].superUser;
    req.data.defaultPhysician = this.showClientData[userIndex].defaultPhysician;
    req.data.practiseCode     = this.showClientData[userIndex].practiseCode;
    req.data.active           = this.showClientData[userIndex].active;
    req.data.visibleInCases   = this.showClientData[userIndex].visibleInCases
    req.header.userCode    = this.logedInUserRoles.userCode;
    req.header.partnerCode = this.logedInUserRoles.partnerCode;
    req.header.functionalityCode = "USRA-UUE";

    var url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
    this.httpRequest.httpPostRequests(url, req).then(resp => {
      console.log('resp',resp)
      // this.reportFormat = resp;
      if(resp['data'].visibleInCases == "1"){
        this.showClientData[userIndex].isClientPortalAccess = 1;
        req.data.isClientPortalAccess = "";
        // var a = e.srcElement.parentNode.parentNode.parentNode.nextSibling;
        // a.children[2].style.display="inline"
      }
      else{
        this.showClientData[userIndex].isClientPortalAccess = 0;
        req.data.isClientPortalAccess = "";
        // var a = e.srcElement.parentNode.parentNode.parentNode.nextSibling;
        // a.children[2].style.display="none"
      }

    })

    //  if(event.target.checked){
    //    $('#appenIcons').append('<i title="Visible in Cases" class="fas fa-chalkboard-teacher text-info"></i>');
    // }
  }
  visibleInCasesCheck(userCode, e, userIndex, selectedClientID){
    console.log("userIndex",userIndex);

    var req = {
      header:{
        uuid:"",
        partnerCode:"",
        userCode:"",
        referenceNumber:"",
        systemCode:"",
        moduleCode:"CLIM",
        functionalityCode:"",
        systemHostAddress:"",
        remoteUserAddress:"",
        dateTime:""
      },
      data:{
        clientId:1,
        userCode:1,
        superUser:"",
        visibleInCases:"",
        isPhysician:"",
        defaultPhysician:"",
        practiseCode:"",
        addedBy:1,
        addedTimestamp:"",
        active:"",
        isClientPortalAccess:""
      }
    }
    console.log('userCode',userCode);
    console.log('event',e.target.checked);
    if (e.target.checked) {
      req.data.visibleInCases = "1"
    }
    else{
      req.data.visibleInCases = "0"
    }
    // console.log('reportFormat',req)
    // req.userCode = userCode;
    req.data.userCode           = userCode;
    req.data.clientId         = selectedClientID;

    req.data.superUser        = this.showClientData[userIndex].superUser;
    req.data.defaultPhysician = this.showClientData[userIndex].defaultPhysician;
    req.data.practiseCode     = this.showClientData[userIndex].practiseCode;
    req.data.active           = this.showClientData[userIndex].active;
    req.data.isClientPortalAccess   = this.showClientData[userIndex].isClientPortalAccess
    req.header.userCode    = this.logedInUserRoles.userCode;
    req.header.partnerCode = this.logedInUserRoles.partnerCode;
    req.header.functionalityCode = "USRA-UUE";

    var url                = environment.API_CLIENT_ENDPOINT + 'updateclientuser';
    this.httpRequest.httpPostRequests(url, req).then(resp => {
      console.log('resp',resp)
      // this.reportFormat = resp;
      if(resp['data'].visibleInCases == "1"){
        this.showClientData[userIndex].visibleInCases = 1;
        req.data.visibleInCases = "";
        // var a = e.srcElement.parentNode.parentNode.parentNode.nextSibling;
        // a.children[2].style.display="inline"
      }
      else{
        this.showClientData[userIndex].visibleInCases = 0;
        req.data.visibleInCases = "";
        // var a = e.srcElement.parentNode.parentNode.parentNode.nextSibling;
        // a.children[2].style.display="none"
      }

    })

    //  if(event.target.checked){
    //    $('#appenIcons').append('<i title="Visible in Cases" class="fa fa-calendar-plus-o text-info"></i>');
    // }
  }


  deleteModal(userCode,clientId,index,clientName){
    console.log('del');
    this.clientToDeleteName=clientName;
    this.deleteuserCode = userCode;
    this.selectedClientID = clientId;
    this.selectedClientindex = index;
    $('#deleteModal').modal('show');
  }
  deleteClientUser(){
    /////////////// check if case agains user exist rexist or not
    var reqBody = {
      header: {
        uuid: "",
        partnerCode: "sip",
        userCode: "1202",
        referenceNumber: "",
        systemCode: "",
        moduleCode: "SPCM",
        functionalityCode: "",
        systemHostAddress: "",
        remoteUserAddress: "",
        dateTime: ""
      },
      data: {
        clientId: null,
        attendingPhysicianId: null,
      }
    }

    reqBody.header.partnerCode = this.logedInUserRoles.partnerCode;
    reqBody.header.userCode    = this.logedInUserRoles.userCode;
    reqBody.header.functionalityCode    = "SPCA-MC";
    reqBody.data.clientId      = this.selectedClientID;
    reqBody.data.attendingPhysicianId      = this.deleteuserCode;
    var urlforCase = environment.API_SPECIMEN_ENDPOINT+'casebyclientId';
    this.userService.checkCaseforUSer(urlforCase,reqBody).subscribe(userCaseResp =>{
      console.log('userCaseResp',userCaseResp);
      if (userCaseResp['result']['codeType'] == 'S') {
        if (userCaseResp['data'] == false) {
          // console.log('this.deleteuserCode',this.deleteuserCode);
          // console.log('this.client',this.selectedClientID);

          this.deleteClientUserData.data.clientId = this.selectedClientID;
          this.deleteClientUserData.data.userCode   = this.deleteuserCode;
          this.deleteClientUserData.header.userCode    = this.logedInUserRoles.userCode;
          this.deleteClientUserData.header.partnerCode = this.logedInUserRoles.partnerCode;
          if (this.logedInUserRoles['allowedRoles'].indexOf("USRA-MUE") != -1) {
              this.deleteClientUserData.header.functionalityCode    = "USRA-MUE";
            }
            else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-MC") != -1){
              this.deleteClientUserData.header.functionalityCode    = "CLTA-MC";
            }
          // this.deleteClientUserData.header.functionalityCode    = "USRA-MUI";
          // console.log('userShowData',this.userShowData);
          //
          // return;
          var url                = environment.API_CLIENT_ENDPOINT + 'deleteclientuser ';
          this.httpRequest.httpPostRequests(url, this.deleteClientUserData).then(resp => {
            console.log('resp',resp)
            if (resp['data'] == 1) {
              this.showClientData.splice(this.selectedClientindex, 1);
              $('#deleteModal').modal('hide');
            }
            else{
              this.ngxLoader.stop();
              this.notifier.notify('error','Error while deleting user')
            }

          }, error=>{
            this.ngxLoader.stop();
            this.notifier.notify('error','Error while deleting user')
          })

        }
        else{
          this.ngxLoader.stop();
          this.notifier.notify('warning','This user cannot be deleted, As this user is affiliated with a case.')
        }
      }
      else{
        this.ngxLoader.stop();
        this.notifier.notify('error','Error while fetching case info')
      }
      $('#deleteModal').modal('hide');

    }, error=>{
      this.ngxLoader.stop();
      this.notifier.notify('error','Error while fetching case info')
    })
    // return;


  }

  // expandRow(trRef, rowData) {
  //   console.log('trRef',trRef);
  //   console.log('rowData',rowData);
  //
  //   this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
  //     var row = dtInstance.row(trRef);
  //     console.log("row,",row);
  //
  //     if (row.child.isShown()) {
  //       console.log("if");
  //
  //       row.child.hide();
  //       this._renderer.removeClass(trRef, 'shown');
  //     }
  //     else {
  //         console.log("else");
  //       let factory = this.compFactory.resolveComponentFactory(ChildComponent);
  //       this.childRow = this.viewRef.createComponent(factory);
  //       this.childRow.instance.data = [rowData];
  //       // this.childRow
  //       row.child(this.childRow.location.nativeElement).show();
  //       this._renderer.addClass(trRef, 'shown');
  //     }
  //   })
  // }

  getExternalUsers (){
    if (typeof this.externalUsers == 'undefined' || this.externalUsers.length == 0) {
      $('#users-external').DataTable().destroy();
      this.ngxLoader.start();
      this.dtTrigger2.next();
      // this.dtOptions2 = {
      //   pagingType: 'full_numbers',
      //   pageLength: 5,
      //   processing: true
      // };
      // this.loadDummyData()


      // this.userShowData.push();
      this.ngxLoader.stop();
    }

  }


  formateFaxUScusers(e){
    // console.log("eeee",e);

    var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
    e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '')

  }

}
