import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../../../../src/environments/environment';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { AppSettings } from '../../../services/_services/app.setting';
import { HttpRequestsService } from '../../../services/_services/http-requests/http-requests.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';

declare var $ :any;
import Swal from 'sweetalert2';
import { NotifierService } from 'angular-notifier';
import { UserApiEndpointsService } from '../../../services/users/user-api-endpoints.service';
import { GlobalySharedModule } from './../../../../../../../src/app/sharedmodules/globaly-shared/globaly-shared.module'

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css'],
  host       : {
    class    :'mainComponentStyle'
  }
})
export class ChangePasswordComponent implements OnInit {
  submitted              = false;
  selctedUserID          = 1;
  isPasswordChange       = false;
  CNewPassword           = '';
  newPassword            = '';
  minLength              = false;
  maxLength              = false;
  passMisMatch           = false;
  passEmpty              = false
  CPassEmpty             = false;
  confirmNewPassword     ;
  notSame                = true;
  wrongCurrentPass       = false;

  public updatePasswordData: any ={
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode  : "",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 : {
      user: {
        userCode           : 1,
        password         : ''
      },
      currentPassword    : ""
    }
  }
  swalStyle              : any;
  public passwordForm      : FormGroup;
  currentPass;
  newPass;

  constructor(
    private formBuilder  : FormBuilder,
    private http         : HttpClient,
    private route        : ActivatedRoute,
    private router       : Router ,
    private httpRequest  : HttpRequestsService,
    private ngxLoader    : NgxUiLoaderService,
    private  notifier    : NotifierService,
    private userService  : UserApiEndpointsService,
    private encryptDecrypt  : GlobalySharedModule,
  ) {
    this.notifier        = notifier;
    this.swalStyle       =  Swal.mixin({
      customClass : {
        confirmButton : 'btn btn-success my-swal-success',
        cancelButton  : 'btn btn-danger'
      },
      buttonsStyling  : true
    })
   }

  ngOnInit(): void {
    // this.notifier.notify( "error", "Password is updated successfully");
    this.passwordForm  = this.formBuilder.group({
      currentPassword        : ['', [Validators.required]],
      NewPassword            : ['', [Validators.required]],
      confirmPassword        : ['', [Validators.required]]

     }, {validator: this.checkPasswords });
  }

  get f() { return this.passwordForm.controls; }

  updatePassword(){
    this.wrongCurrentPass = false;
    // // stop here if form is invalid
    this.submitted = true;
    this.ngxLoader.start();
    if (this.passwordForm.invalid) {
      // alert('form invalid');
      this.ngxLoader.stop();
      return;
    }

    console.log("updatePasswordData",this.updatePasswordData);
    this.updatePasswordData.data.user.password = this.encryptDecrypt.encryptString(this.newPass);
    this.updatePasswordData.data.user.currentPassword = this.encryptDecrypt.encryptString(this.currentPass);


    // this.updatePasswordData.data.userId    = this.selctedUserID;
    // this.updatePasswordData.data.password  = this.newPassword;
    // var url                = environment.API_USER_ENDPOINT + 'updatepassword';
    var url                = environment.API_USER_ENDPOINT + 'updatepassword';
    this.userService.updatePassword(url,this.updatePasswordData).subscribe(resp => {
      console.log("resp",resp);
      this.submitted = false;
      this.ngxLoader.stop();
      if (resp['result'].codeType == "S") {
        console.log('Password Updated succesfully add response divs');
        this.notifier.notify( "sucess", "Password is updated successfully");
        this.wrongCurrentPass = false;

      }
      else if(resp['result'].codeType == "E" && resp['result'].description == "Provided Current Password of the user is wrong"){
        this.notifier.notify( "error", "Provided Current Password of the user is wrong");
        this.wrongCurrentPass = true;

      }
      else{
        this.notifier.notify( "error", "Error while updating password");
        this.wrongCurrentPass = false;
      }

      setTimeout(function(){
        this.wrongCurrentPass = false;
      }, 1000);

    })


  }


checkPasswords(group: FormGroup) { // here we have the 'passwords' group
  let pass = group.get('NewPassword').value;
  let confirmPass = group.get('confirmPassword').value;

  return pass === confirmPass ? null : { notSame: true }
}


  passwordCheckRealTime(){
    if (this.newPassword.length < 8) {
      this.minLength    = true;
    }
    else if (this.newPassword.length > 50) {
      this.maxLength    = true;
    }
    else{
      this.passMisMatch  = false;
      this.passEmpty     = false;
      this.CPassEmpty    = false;
      this.minLength     = false;
      this.maxLength     = false;
    }

    if (this.CNewPassword != '') {
      if (this.newPassword != this.CNewPassword) {
        this.passMisMatch  = true;
      }
      else{
        this.passMisMatch  = false;
        this.passEmpty     = false;
        this.CPassEmpty    = false;
        this.minLength     = false;
        this.maxLength     = false;
      }

    }


  }
  // updatePassword(){
  //
  //   console.log('password');
  //
  //   if (this.newPassword != this.CNewPassword) {
  //     this.passMisMatch  = true;
  //   }
  //   else if (this.newPassword == '') {
  //     this.passEmpty     = true;
  //     console.log('empty');
  //
  //   }
  //   else if (this.newPassword == '') {
  //     this.CPassEmpty    = true;
  //   }
  //   else if (this.newPassword.length < 8) {
  //     this.minLength    = true;
  //   }
  //   else if (this.newPassword.length > 50) {
  //     this.maxLength    = true;
  //   }
  //   else{
  //     this.passMisMatch  = false;
  //     this.passEmpty     = false;
  //     this.CPassEmpty    = false;
  //     this.minLength     = false;
  //     this.maxLength     = false;
  //     this.ngxLoader.start();
  //     // this.updatePassData.userID         = this.selctedUserID;
  //     // // this.updatePassData.userID      = this.selctedUserID;
  //     // this.updatePassData.newPasswrod    = this.newPassword;
  //
  //     // this.updatePasswordData.userId    = this.selctedUserID;
  //     // this.updatePasswordData.password  = this.newPassword;
  //     this.updatePasswordData.data.userId    = this.selctedUserID;
  //     this.updatePasswordData.data.password  = this.newPassword;
  //     // var url                = environment.API_USER_ENDPOINT + 'updatepassword';
  //     var url                = environment.API_USER_ENDPOINT + 'updatepassword';
  //     console.log("this.updatePasswordData",this.updatePasswordData);
  //     console.log("this.selctedUserID",this.selctedUserID);
  //
  //     this.httpRequest.httpPutRequests(url,this.updatePasswordData).then(resp => {
  //       console.log("resp",resp);
  //       this.ngxLoader.stop();
  //       if (resp.result.codeType == "S") {
  //         console.log('Password Updated succesfully add response divs');
  //         this.newPassword   = '';
  //         this.CNewPassword = '';
  //         $('#successMSGPassword').css("display","block");
  //         // this.notifier.notify( "sucess", "Password is updated successfully");
  //
  //         // this.swalStyle.fire({
  //         //   icon: 'success',
  //         //   type: 'sucess',
  //         //   title: 'Password is updated',
  //         //   showConfirmButton: true,
  //         //   timer: 3000
  //         // })
  //         // $('#resetpassModal').modal('hide');
  //       }
  //       else{
  //         $('#errorMSGPassword').css("display","block");
  //         // this.swalStyle.fire({
  //         //   icon: 'error',
  //         //   type: 'sucess',
  //         //   title: 'Error While Updating Password',
  //         //   showConfirmButton: true,
  //         //   timer: 3000
  //         // })
  //         // this.notifier.notify( "error", "Error while updating password");
  //         // $('#resetpassModal').modal('hide');
  //
  //       }
  //
  //     })
  //     this.newPassword   = '';
  //     this.CNewPassword  = '';
  //     this.passMisMatch  = false;
  //     this.passEmpty     = false;
  //     this.CPassEmpty    = false;
  //     // $('#resetpassModal').modal('hide');
  //     setTimeout(function(){
  //       $('#successMSGPassword').css("display","none");
  //       $('#errorMSGPassword').css("display","none");
  //       // $('#resetpassModal').modal('hide');
  //       // this.router.navigateByUrl('/editlisting', { state: this.listing });
  //     },3000);
  //   }
  // }

}
