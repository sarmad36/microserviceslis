import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { environment } from '../../../../../../../src/environments/environment';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { AppSettings } from '../../../services/_services/app.setting';
import { HttpRequestsService } from '../../../services/_services/http-requests/http-requests.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject, forkJoin} from 'rxjs';
import { NgxUiLoaderService } from 'ngx-ui-loader';
declare var $ :any;
import Swal from 'sweetalert2';
import { NotifierService } from 'angular-notifier';
import { UserApiEndpointsService } from '../../../services/users/user-api-endpoints.service';
import { RolesApiEndpointsService } from '../../../services/roles/roles-api-endpoints.service';
// import { BootstrapAlertService, BootstrapAlert } from 'ng-bootstrap-alert';
// your-app.scss
// @import '~@sweetalert2/themes/dark/dark.scss';
import { GlobalApiCallsService } from '../../../services/gloabal/global-api-calls.service';
import { SortPipe } from "../../../pipes/sort.pipe";
import { GlobalySharedModule } from './../../../../../../../src/app/sharedmodules/globaly-shared/globaly-shared.module';
import { UrlGuard } from './../../../../../../../src/services/gaurd/url.guard'
// import { RbacDirective } from './../../../../../../../src/app/directives/rbac.directive'
import * as CryptoJS from 'crypto-js';

@Component({
  selector   : 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls  : ['./add-user.component.css'],
  host       : {
    class    :'mainComponentStyle'
  }
})
export class AddUserComponent implements OnDestroy, OnInit {
  @ViewChild(DataTableDirective, {static: false})
  // @ViewChild(RbacDirective, {static: false})

  // ViewChild(SDirective) vc:SomeDirective;

  // dtOptions: DataTables.Settings = {};
  dtElement              : DataTableDirective;
  submitted              = false;
  validUsername          = true;
  inValidUsername        = false;
  dtTrigger              : Subject<AddUserComponent> = new Subject();
  userTable;
  totalUsersCount        = 500;
  public dtOptions       : DataTables.Settings = {};
  // public paginatePage    : any;
  public paginatePage    : any = 0;
  public paginateLength   = 20;
  showTable              = false;
  totaUsersRecieved      ;
  showNested             = false;
  userShowData           = [];
  allUsers               : any;
  internalUsers          : any [];
  externalUsers          : any [];
  appendIcons            ;
  deleteUserID           ;
  defaultRoles           ;
  UName                  ;
  FName                  ;
  LName                  ;

  public userForm        : FormGroup;
  public saveData        : any = {
  additionalHeaders      : [],
  userData               : []
  };
  // defaultRoles           = [
  //   {roleId: 1, name         : 'Medical Director',   description : "", system_role : false, createdBy : 1, createdTimestamp : "2020-08-26T02:37:34.000+00:00" },
  //   {roleId: 2, name         : 'Organization Admin', description : "", system_role : false, createdBy : 1, createdTimestamp : "2020-08-26T02:37:34.000+00:00" },
  //   {roleId: 3, name         : 'Pathologist',        description : "", system_role : false, createdBy : 1, createdTimestamp : "2020-08-26T02:37:34.000+00:00" },
  //   {roleId: 4, name         : 'Technician',         description : "", system_role : false, createdBy : 1, createdTimestamp : "2020-08-26T02:37:34.000+00:00" }
  // ];
  public requestData     : any = {
    header               : {
      uuid               : "",
      partnerCode        : "sip",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode: "USRA-AUI",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data: {
      // partnerCode        : "sip",
      partnerCode          : 1,
      user               : {
        // userId           : 0,
        // displayCode      : "",
        userCode         :"",
        firstName        : "",
        lastName         : "",
        username         : "",
        password         : "",
        email            : "",
        phone            : "",
        photo            : null,
        title            : null,
        npi              : null,
        postNominal      : null,
        userType         : 1,
        active           : true,
        agreementActive  : true,
        accessCode       : "",
        isDoctor         : false,
        createdBy        :1,
        createdTimestamp :"2020-08-28T19:10:37.000+00:00",
        roles            :[]
      },
      createdBy          :1
    }
  }

  public allUsersData   =
  {
    header              : {
      uuid              : "",
      partnerCode       : "",
      userCode          : "",
      referenceNumber   : "",
      systemCode        : "",
      moduleCode        : "",
      functionalityCode: "USRA-AUI",
      systemHostAddress : "",
      remoteUserAddress : "",
      dateTime          : ""
    },
    data                : {
      partnerCode         : 1,
      userType          : 1,
      pageNumber        : 0,
      pageSize          : 10,
      sortColumn        : 'userCode',
      sortType          : 'desc'
    }
  }

  public totalUsersReq   =
  {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode: "USRA-AUI",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 : {
      partnerCode          : 1
    }
  }

  public allRolesReq     =
  {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode: "USRA-AUI",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 :{
      roleType:          0
    }
  }

  public disableUserReq  =
  {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode: "USRA-AUI",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 : {
      userCode           : 1,
      status             : false
    }
  }

  public userNameReq     =
  {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode: "USRA-AUI",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 : {
      username           : '',
      // partnerCode          : 1
    }
  }
  swalStyle              : any;
  oldCharacterCount      = 0;

  public searchData      =
  {
    header              : {
      uuid              : "",
      partnerCode       : "",
      userCode          : "",
      referenceNumber   : "",
      systemCode        : "",
      moduleCode        : "",
      functionalityCode: "USRA-AUI",
      systemHostAddress : "",
      remoteUserAddress : "",
      dateTime          : ""
    },
    data                : {
      partnerCode         : 1,
      userType          : 1,
      searchQuery       : "",
      pageNumber        : 0,
      pageSize          : 20,
      sortColumn        : 'userCode',
      sortType          : 'desc'
    }
  }
  inValidUseremail     = false;
  public userByNpi: any = {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode: "USRA-AUI",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 : {
      npi                : "",
      email              : "",
      userType           : 1,
    }
  }
  allTittles;
  public logedInUserRoles :any = {};


  constructor(
    private formBuilder  : FormBuilder,
    private http         : HttpClient,
    private router       : Router ,
    private httpRequest  : HttpRequestsService,
    private ngxLoader    : NgxUiLoaderService,
    private  notifier    : NotifierService,
    private userService  : UserApiEndpointsService,
    private roleService  : RolesApiEndpointsService,
    private globalService: GlobalApiCallsService,
    private sortPipe     : SortPipe,
    private rbac         : UrlGuard,
    private encryptDecrypt : GlobalySharedModule
  ) {

    // console.log("this.rbac",this.rbac);
    this.rbac.checkROle('USRA-AUI');


    this.notifier        = notifier;
    this.swalStyle       =  Swal.mixin({
      customClass : {
        confirmButton : 'btn btn-success my-swal-success',
        cancelButton  : 'btn btn-danger'
      },
      buttonsStyling  : true
    })


  }

  ngOnInit(): void {
    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
var originalText = bts.toString(CryptoJS.enc.Utf8);
this.logedInUserRoles = JSON.parse(originalText)
    this.getLookups().then(lookupsLoaded => {
      });
    // this.notifier.notify( "error", "User saved Successfully" );
    // console.log('here');
    /////////  ROles form // DB
    // console.log("URLS",environment.API_ROLE_ENDPOINT);

    var url        = environment.API_ROLE_ENDPOINT + 'allroles';
    this.allRolesReq.header.partnerCode = this.logedInUserRoles.partnerCode;
    this.allRolesReq.header.userCode    = this.logedInUserRoles.userCode;
    this.allRolesReq.header.functionalityCode    = "USRA-AUI";
    this.roleService.getAllRoles(url, this.allRolesReq).subscribe(resp => {
      console.log('resp Role',resp)
      this.defaultRoles = resp['data'];
    })

    this.dtOptions = {
      pagingType   : 'full_numbers',
      pageLength   : this.paginateLength,
      serverSide   : true,
      processing   : true,
      // ordering     : false,
      "order": [[ 0, "desc" ]],
      columnDefs: [
            { orderable: true, className: 'reorder', targets: [0,2,3,4,5]  },
            { orderable: false, targets: '_all' }
        ],
      "lengthMenu": [20, 50, 75, 100 ],
      ajax: (dataTablesParameters: any, callback) => {
        if(typeof this.dtElement['dt'] != 'undefined'){
            this.paginatePage           = this.dtElement['dt'].page.info().page;
            this.paginateLength         = this.dtElement['dt'].page.len();
            // console.log("this.dtElement['dt']: ", this.dtElement['dt']);
        }
        // console.log("dataTablesParameters",dataTablesParameters);


        // this.allUsersData.data.pageNumber = this.paginatePage;
        // this.allUsersData.data.pageSize   = this.paginateLength;
        var sortColumn = dataTablesParameters.order[0]['column'];
        var sortOrder  = dataTablesParameters.order[0]['dir'];
        var sortArray  = ["userCode",'userRoles','fullName','username','email','active']


        if (dataTablesParameters.search.value == "" && (this.oldCharacterCount == 0 || this.oldCharacterCount == 3)) {
          this.allUsersData.data.pageNumber = this.paginatePage;
          this.allUsersData.data.pageSize   = this.paginateLength;
          this.oldCharacterCount     = 3;
          // let url    = environment.API_USER_ENDPOINT + 'partnerusers?partnerCode=1&pageNumber='+ this.paginatePage +'&pageSize=' + this.paginateLength;
          let url    = environment.API_USER_ENDPOINT + 'partnerusers';
          console.log('this.paginatePage: ',this.paginatePage);
          this.allUsersData.header.userCode    = this.logedInUserRoles.userCode
          this.allUsersData.header.partnerCode = this.logedInUserRoles.partnerCode
          this.allUsersData.header.functionalityCode = "USRA-MUI"
          this.allUsersData.data.partnerCode   = this.logedInUserRoles.partnerCode
          this.allUsersData.data.sortColumn    = sortArray[sortColumn];
          this.allUsersData.data.sortType      = sortOrder;

          // dataTablesParameters.pageNumber = this.paginatePage;
          this.http.put(url,this.allUsersData).toPromise()
          .then( resp => {
            console.log('**** resp: ', resp);
            this.allUsers                 = resp['data']['users'];
            this.internalUsers            = this.allUsers;
            // this.externalUsers            = this.allUsers;
            // this.totaUsersRecieved        = 5;
            this.totalUsersCount          = resp['data']['totalUsers'];
            // this.totaUsersRecieved        = resp.length
            // // console.log('this.dtOptions.pageLength',this.dtOptions.pageLength);
            callback({
              // recordsTotal    :  this.totaUsersRecieved,
              // recordsFiltered :  this.totaUsersRecieved,
              recordsTotal    :  this.totalUsersCount,
              recordsFiltered :  this.totalUsersCount,

              // recordsFiltered: resp['recordsFiltered'],
              // recordsFiltered: 1000,
              data: []
            });
            $('div.dataTables_length select').addClass('dt-form-control');
            $('div.dataTables_filter input').addClass('dt-form-control');
          })
          .catch(error => {
            // Set loader false
            // this.loading = false;

            console.log("error: ",error);
          });
        }
        else{
          if (dataTablesParameters.search.value.length >=3) {

            this.searchData.data.pageNumber  = this.paginatePage;
            this.searchData.data.pageSize    = this.paginateLength;
            this.searchData.data.searchQuery = dataTablesParameters.search.value;


            // let url    = environment.API_USER_ENDPOINT + 'partnerusers?partnerCode=1&pageNumber='+ this.paginatePage +'&pageSize=' + this.paginateLength;
            let url    = environment.API_USER_ENDPOINT + 'search';
            console.log('oldCharacterCount',this.oldCharacterCount);
            this.searchData.header.userCode    = this.logedInUserRoles.userCode
            this.searchData.header.partnerCode = this.logedInUserRoles.partnerCode
            this.searchData.header.functionalityCode = "USRA-MUI"
            this.searchData.data.partnerCode  = this.logedInUserRoles.partnerCode
            this.searchData.data.sortColumn    = sortArray[sortColumn];
            this.searchData.data.sortType      = sortOrder;

            // dataTablesParameters.pageNumber = this.paginatePage;
            this.http.put(url,this.searchData).toPromise()
            .then( resp => {
              console.log('**** resp: ', resp);
              this.allUsers                 = resp['data']['users'];
              this.internalUsers            = this.allUsers;
              this.externalUsers            = this.allUsers;
              // this.totaUsersRecieved        = 5;
              this.totalUsersCount          = resp['data']['totalUsers'];
              // this.totaUsersRecieved        = resp.length
              // // console.log('this.dtOptions.pageLength',this.dtOptions.pageLength);
              callback({
                // recordsTotal    :  this.totaUsersRecieved,
                // recordsFiltered :  this.totaUsersRecieved,
                recordsTotal    :  this.totalUsersCount,
                recordsFiltered :  this.totalUsersCount,

                // recordsFiltered: resp['recordsFiltered'],
                // recordsFiltered: 1000,
                data: []
              });
              this.oldCharacterCount     = 3;
              $('div.dataTables_length select').addClass('dt-form-control');
              $('div.dataTables_filter input').addClass('dt-form-control');
            })
            .catch(error => {
              // Set loader false
              // this.loading = false;

              console.log("error: ",error);
            });

          }
          else{
            $('.dataTables_processing').css('display',"none");
            if (this.oldCharacterCount == 3) {
              this.oldCharacterCount     = 3
            }
            else{
              this.oldCharacterCount     = 2;
            }
          }
        }


      },
      // columns  : [
      //   { data : 'SPD-002' },
      //   { data : 'firstName' },
      //   { data : 'lastName' },
      //   { data : 'username' },
      //   { data : 'phone' },
      //   { data : 'email' },
      //   { data : 'action' }
      // ]
      ///for search
    };

    this.userForm  = this.formBuilder.group({
      userType     : ['', [Validators.required]],
      title        : [''],
      firstName    : ['', [Validators.required]],
      lastName     : ['', [Validators.required]],
      // mobileNo     : ['', [Validators.pattern(/^\(\d{3}\)\s\d{3}-\d{4}$/),Validators.required]],
      mobileNo     : ['', [Validators.required,Validators.minLength(13)]],
      userName     : ['', [Validators.required]],
      useremail    : ['', [Validators.required, Validators.email]],
      role         : ['', [Validators.required]],
    });
    this.bindJQueryFuncs();

  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rolesChanged() {
    console.log("this.requestData: ", this.requestData);
  }

  // // // Submit Add user form data
  get f() { return this.userForm.controls; }
  submitUser(){
    // // stop here if form is invalid
    this.submitted         = true;
    this.ngxLoader.start();
    if (this.userForm.invalid) {
      // alert('form invalid');
      this.ngxLoader.stop();
      return;
    }
    // var formatedMysqlString                = (new Date ((new Date((new Date(new Date())).toISOString() )).getTime() - ((new Date()).getTimezoneOffset()*60000))).toISOString().slice(0, 19).replace('T', ' ');

    // let testID = Math.floor(Math.random() * 1000);
    this.saveData.userData                      = this.userForm.value;
    // this.requestData.data.user.userId           = 0;
    // this.requestData.data.user.displayCode      = "AB94";
    this.requestData.data.user.firstName        = this.userForm.value.firstName;
    this.requestData.data.user.lastName         = this.userForm.value.lastName;
    this.requestData.data.user.username         = this.userForm.value.userName;
    this.requestData.data.user.accessCode       = "AB94";
    // this.requestData.data.user.isDoctor         = false;
    this.requestData.data.user.createdBy        = this.logedInUserRoles.userCode;
    this.requestData.data.createdBy             = this.logedInUserRoles.userCode;

    console.log('this.requestData',this.requestData);
    var x = this.requestData.data.user.phone.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
    this.requestData.data.user.phone = !x[2] ? x[1] : '(' + x[1] + ')' + x[2] + (x[3] ? '-' + x[3] : '')
    // console.log("this.requestData.data.user.phone ",this.requestData.data.user.phone );

    // console.log('this.saveData',this.saveData);
    this.requestData.header.userCode    = this.logedInUserRoles.userCode
    this.requestData.header.partnerCode = this.logedInUserRoles.partnerCode
    this.requestData.header.functionalityCode = "USRA-AUI"
    this.requestData.data.partnerCode = this.logedInUserRoles.partnerCode

    // var url                = environment.API_USER_ENDPOINT + 'save';
    var url                = environment.API_USER_ENDPOINT + 'save';
    this.userService.saveUser(url,this.requestData).then(resp => {
      console.log("resp",resp);

      // this.allUsers.push(resp);
      this.ngxLoader.stop();
      if (resp['result'].codeType == 'S') {
        this.notifier.notify( "success", "User saved Successfully" );
        var temp = '';
          console.log('before',resp['data']);
        for (let i = 0; i < resp['data'].roles.length; i++) {
            temp = resp['data'].roles[i]['name']+', '+ temp;
        }
        resp['data'].userRoles  = temp;
        this.allUsers.push(resp['data']);
        this.submitted = false;
        this.userForm.reset();
        // this.swalStyle.fire({
        //   icon: 'success',
        //   type: 'sucess',
        //   title: 'Your work has been saved',
        //   showConfirmButton: true,
        //   timer: 3000
        // })
        // console.log('record added succesfully add response divs');
        // $('#successMSG').css("display","block");


      }
      else{
        this.ngxLoader.stop();
        // this.notifier.notify( "error", "Error while saving user");
        if (resp['result'].description.indexOf("[u_email]") > -1) {
          this.notifier.notify( "error", "Error while saving user Email Already Exist");
        }
        else{
          this.notifier.notify( "error", "Error while saving user");
        }

        // this.swalStyle.fire({
        //   icon: 'error',
        //   type: 'sucess',
        //   title: 'There was an error while Saving user',
        //   showConfirmButton: true,
        //   timer: 3000
        // })
        // $('#errorMSG').css("display","block");

      }
    })

  }
  setUserName(){
    if (typeof this.LName == "undefined" || this.LName == null || this.LName == '') {
        return;
    }
    else if (typeof this.FName == "undefined" || this.FName == null || this.FName == '') {

    }
    else{
      let temp                       = this.FName.charAt(0)+this.LName
      this.UName                     = temp.toLowerCase();
      this.userNameReq.data.username = this.UName;
      this.userNameReq.header.userCode    = this.logedInUserRoles.userCode
      this.userNameReq.header.partnerCode = this.logedInUserRoles.partnerCode
      this.userNameReq.header.functionalityCode = "USRA-AUI"
      var url                        = environment.API_USER_ENDPOINT + 'useravailable';
      this.userService.userNameAvailability(url, this.userNameReq).subscribe(resp => {
        console.log('resp UserName',resp)
        if (resp['data'] == true) {
          this.inValidUsername   = false;
          this.validUsername     = true;
          $('#user_name').attr("disabled","ture");

            // enable submit button
        }else{
          // console.log('false');
          this.validUsername     = false;
          this.inValidUsername   = true;
          // $('#user_name').attr("disabled","false");
          $('#user_name').removeAttr("disabled");
        }
        // this.defaultRoles = resp.data;
      })
    }
  }

  checkUserName(){
    this.UName                     = this.UName.toLowerCase();
    this.userNameReq.data.username = this.UName;
    this.userNameReq.header.userCode    = this.logedInUserRoles.userCode
    this.userNameReq.header.partnerCode = this.logedInUserRoles.partnerCode
    this.userNameReq.header.functionalityCode = "USRA-AUI"
    var url                        = environment.API_USER_ENDPOINT + 'useravailable';
    this.userService.userNameAvailability(url, this.userNameReq).subscribe(resp => {
      console.log('resp UserName',resp)
      if (resp['data'] == true) {
        this.inValidUsername   = false;
        this.validUsername     = true
        // $('#user_name').attr("disabled","ture");
        // enable submit button
      }else{
        this.validUsername     = false;
        this.inValidUsername   = true;
        // $('#user_name').removeAttr("disabled");
      }
      // this.defaultRoles = resp.data;
    })

  }

  getSelectedUserData(userID){
    console.log('userID',userID);
    this.ngxLoader.start();
    // var ciphertext = CryptoJS.AES.encrypt(userID, 'nglis', { outputLength: 224 }).toString();
    var ax = CryptoJS.AES.encrypt(userID, 'nglis', { outputLength: 224 }).toString();
    var bx = ax.replace('+','xsarm');
    var cx = bx.replace('/','ydan');
    var ciphertext = cx.replace('=','zhar');
    this.router.navigate(['edit-user', ciphertext]);

  }
  disableUser(userID, status, userIndex){
    // this.ngxLoader.start();
    console.log('userID',userID);
    console.log('this.disableUserReq',this.disableUserReq);
    // let url  = environment.API_USER_ENDPOINT + 'deactive?userId='+userID+'';
    this.disableUserReq.data.userCode = userID
    this.disableUserReq.header.userCode    = this.logedInUserRoles.userCode
    this.disableUserReq.header.partnerCode = this.logedInUserRoles.partnerCode
    this.disableUserReq.header.functionalityCode = "USRA-MUI"
    let url  = environment.API_USER_ENDPOINT + 'updatestatus';
    if (status == 1) {
      this.disableUserReq.data.status = true;
      this.userService.updateStatus(url, this.disableUserReq).subscribe(resp => {
        console.log('disable resp:', resp);
        this.ngxLoader.stop();
        if (resp['result'].codeType == "S") {
          this.notifier.notify( "success", "User enabled Successfully" );
          // $('.TR-'+userID).css('display','none');
          this.internalUsers[userIndex].active = true;

        }
        else{
          this.notifier.notify( "error", "Error while disabling user");
          this.internalUsers[userIndex].active = false;
        }
        // this.router.navigateByUrl('/edit-user', { state: resp });
        // this.router.navigate(['/editlisting', JSON.stringify(resp)]);
      })
    }
    else{
      this.disableUserReq.data.status = false;
      Swal.fire({
        title: 'Are you sure?',
        text: "User wont be able to access the portal!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, deactivate it!'
      }).then((result) => {
        if (result.value) {
          this.ngxLoader.start();
          this.userService.updateStatus(url, this.disableUserReq).subscribe(resp => {
            console.log('disable resp:', resp);
            this.ngxLoader.stop();
            if (resp['result'].codeType == "S") {
              this.notifier.notify( "success", "User disabled Successfully" );
              // $('.TR-'+userID).css('display','none');
              this.internalUsers[userIndex].active = false;

            }
            else{
              this.notifier.notify( "error", "Error while disabling user");

            }
            // this.router.navigateByUrl('/edit-user', { state: resp });
            // this.router.navigate(['/editlisting', JSON.stringify(resp)]);
          })
        }
      })

    }


  }
  formatePhoneUS(e){
    // console.log("eeee",e);

    var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
    e.target.value = !x[2] ? x[1] : '(' + x[1] + ')' + x[2] + (x[3] ? '-' + x[3] : '')

  }

  // // // Jquery Events like initializing Datable, Select2 etc.
  bindJQueryFuncs() {
  }

  getEMAIL(){
    this.inValidUseremail      = true;
    $('#EMAIL-spiner').css("display","inline-block");
    this.userByNpi.data.email = this.requestData.data.user.email;
    console.log("userByNpi",this.userByNpi);
    this.userByNpi.header.userCode    = this.logedInUserRoles.userCode
    this.userByNpi.header.partnerCode = this.logedInUserRoles.partnerCode

    let url    = environment.API_USER_ENDPOINT + 'userbynpiemail';
    this.http.put(url,this.userByNpi).toPromise()
    .then( resp => {
      console.log('**** resp Email: ', resp);
      // this.userByNpi.data.npi   = "";
      this.userByNpi.data.email = "";
      ////// already pushed
      // if (found == false) arr.push({ id, username: name });
      $('#EMAIL-spiner').css("display","none");
      // this.ngxLoader.stop();
      if (resp["result"].codeType == "S") {
        this.inValidUseremail      = true;

      }
      else{
        this.inValidUseremail      = false;
      }
      //
    })
    .catch(error => {
      console.log("error EMAIL: ",error);
    });
  }

  getLookups(){
    // this.ngxLoader.start();
    return new Promise((resolve, reject) => {


      // var allRoles = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=ATTACHMENT_FILE_TYPE_CACHE&partnerCode=sip').then(response =>{
      //   return response;
      // })
      // var allpostNom = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=POSTNOMINAL_CACHE&partnerCode=sip').then(response =>{
      //   return response;
      // })
      var allTitle = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=TITLE_CACHE&partnerCode=sip').then(response =>{
        return response;
      })



      forkJoin([allTitle]).subscribe(allLookups => {
        console.log("allLookups",allLookups);

        // this.dPostNominal            = this.sortPipe.transform(allLookups[0], "asc", "value");
        this.allTittles             = this.sortPipe.transform(allLookups[0], "asc", "titleValue");
        for (let index = 0; index < allLookups[0]['length']; index++) {
          if (allLookups[0][index]['titleValue'] == 'Mr.' || allLookups[0][index]['titleValue'] == 'mr.' || allLookups[0][index]['titleValue'] == 'MR.') {
            this.requestData.data.user.title = allLookups[0][index]['titleId'];
            // this.changeCountry()
          }

        }
        resolve();
      })
    })

  }

}
