import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../../../../src/environments/environment';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { AppSettings } from '../../../services/_services/app.setting';
import { HttpRequestsService } from '../../../services/_services/http-requests/http-requests.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { UserApiEndpointsService } from '../../../services/users/user-api-endpoints.service';
import { RolesApiEndpointsService } from '../../../services/roles/roles-api-endpoints.service';
import { GlobalApiCallsService } from '../../../services/gloabal/global-api-calls.service';
import { SortPipe } from "../../../pipes/sort.pipe";
import { Subject, forkJoin} from 'rxjs';

declare var $ :any;
import Swal from 'sweetalert2';
import { NotifierService } from 'angular-notifier';
import { GlobalySharedModule } from './../../../../../../../src/app/sharedmodules/globaly-shared/globaly-shared.module'
import * as CryptoJS from 'crypto-js';

@Component({
  selector   : 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls  : ['./edit-user.component.css'],
  host       : {
    class    :'mainComponentStyle'
  }
})
export class EditUserComponent implements OnInit {
  submitted              = false;
  isPasswordChange       = false;
  showSuccess            = false;
  showError              = false;
  responseMSG            = '';
  newPassword            = '';
  CNewPassword           = '';
  minLength              = false;
  maxLength              = false;
  passMisMatch           = false;
  passEmpty              = false
  CPassEmpty             = false;
  selctedUserID          ;
  selctedUser            : any = [];
  imagePayLoad           ;
  imageBase64            = '';
  imageName              = '';
  imgSrc                 = null;
  public userForm        : FormGroup;
  defaultRoles           ;
  // defaultRoles           = [
  //   {roleId: 1, name         : 'Medical Director',   description : "", system_role : false, createdBy : 1, createdTimestamp : "2020-08-26T02:37:34.000+00:00" },
  //   {roleId: 2, name         : 'Organization Admin', description : "", system_role : false, createdBy : 1, createdTimestamp : "2020-08-26T02:37:34.000+00:00" },
  //   {roleId: 3, name         : 'Pathologist',        description : "", system_role : false, createdBy : 1, createdTimestamp : "2020-08-26T02:37:34.000+00:00" },
  //   {roleId: 4, name         : 'Technician',         description : "", system_role : false, createdBy : 1, createdTimestamp : "2020-08-26T02:37:34.000+00:00" }
  // ];
  dPostNominal           : any = [];
  deliverMethod          = [
    { id: 'fax',             name: 'Fax' },
    { id: 'emr',             name: 'EMR' },
    { id: 'email',           name: 'email' },
    { id: 'courier',         name: 'Courier' },
    { id: 'remote printing', name: 'Remote Printing' }
  ];
  selectedPostNominal    : any = [];
  selectedDefaultRoles   : any = [];
  // formToLink = false;
  public saveUserData    : any = {
    additionalHeaders    : [],
    otherData            : [],
    imagePayload         : [],
    imageBase64          : '',
    userID               : 0
   };
  public updatePassData  : any ={
    additionalHeaders    : [],
    userID               : '',
    newPasswrod          : ''
  }

  public requestData     : any = {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode: "USRA-MUI",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 : {
      // partnerCode          : 1,
      user               : {
        // userId           : 0,
        // displayCode      : "",
        userCode         : "",
        firstName        : "",
        lastName         : "",
        username         : "",
        password         : "",
        email            : "",
        phone            : "",
        photo            : null,
        title            : null,
        npi              : null,
        base64Image      : null,
        postNominal      : null,
        userType         : 1,
        active           : true,
        agreementActive  : true,
        accessCode       : "",
        isDoctor         : false,
        departmentId     : 1,
        createdBy        :1,
        createdTimestamp :"2020-08-28T19:10:37.000+00:00",
        updatedBy        :null,
        updatedTimestamp :null,
        roles            :[],
        reportingTo      :{
          userCode         : ''
        }
      },
      createdBy          :1
    }
  };
  // reportinToTemp         : any = {};
  // public updatePasswordData: any ={
  //   userId               : '',
  //   password             : ''
  // }
  public updatePasswordData: any ={
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode: "USRA-MUI",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 : {
      user: {
        userCode         : '',
        password         : ''
      },
      currentPassword    : ""
    }
  }
  public allRolesReq     =
  {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode: "USRA-MUI",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 :{
      roleType:          0
    }
  }

  public getSelectedUser =
  {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode: "USRA-MUI",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 : {
      userCode           : 1
    }
  }

  public getReportingTo  =
  {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode: "USRA-MUI",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 : {
      // partnerCode          : 1,
      departmentId       : 1   //for time being send it zero when need. We will use it.
    }
  }

  swalStyle              : any;
  reportsTo              ;
  reportingUser          ;
  SameimgSrc             = '';

  public userByNpi: any = {
    header               : {
      uuid               : "",
      partnerCode        : "",
      userCode           : "",
      referenceNumber    : "",
      systemCode         : "",
      moduleCode         : "",
      functionalityCode: "USRA-MUI",
      systemHostAddress  : "",
      remoteUserAddress  : "",
      dateTime           : ""
    },
    data                 : {
      npi                : "",
      email              : "",
      userType           : 1,
    }
  }
  inValidUsernpi         = false;
  allTittles;
  allDepartment;
  public logedInUserRoles :any = {};
  userTypeCheckbox = '1';
  passwordError = false;
  public externalCLient = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"CLIM",
      functionalityCode:"",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      page:"0",
      size:"20",
      sortColumn:"userCode",
      sortingOrder:"asc",
      searchString:"131"
    }
  }
  userClients;

  constructor(
    private formBuilder  : FormBuilder,
    private http         : HttpClient,
    private route        : ActivatedRoute,
    private router       : Router ,
    private httpRequest  : HttpRequestsService,
    private ngxLoader    : NgxUiLoaderService,
    private  notifier    : NotifierService,
    private userService  : UserApiEndpointsService,
    private roleService  : RolesApiEndpointsService,
    private globalService: GlobalApiCallsService,
    private sortPipe     :SortPipe,
    private encryptDecrypt : GlobalySharedModule
  ) {
    this.notifier        = notifier;
    this.swalStyle       =  Swal.mixin({
      customClass : {
        confirmButton : 'btn btn-success my-swal-success',
        cancelButton  : 'btn btn-danger'
      },
      buttonsStyling  : true
    })
    // let url  = environment.API_USER_ENDPOINT + 'allroles';
    // this.httpRequest.httpGetRequests(url).then(resp => {
    //   console.log("rolles",resp);
    //   // this.router.navigate(['/editlisting', JSON.stringify(resp)]);
    // })
    // this.saveUserData.additionalHeaders   = environment.additionalHeaders;
    // this.updatePassData.additionalHeaders = environment.additionalHeaders;
  }

  // ngOnInit(): void {
  ngOnInit(): void {
    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
    var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
    var originalText = bts.toString(CryptoJS.enc.Utf8);
    this.logedInUserRoles = JSON.parse(originalText);
    if (this.logedInUserRoles['allowedRoles'].indexOf("USRA-UU") != -1) {
      this.getReportingTo['header'].functionalityCode    = "USRA-UU";
      this.getSelectedUser['header'].functionalityCode    = "USRA-UU";
      // this.allRolesReq['header'].functionalityCode    = "USRA-AUI";
      this.userByNpi['header'].functionalityCode    = "USRA-UU";
      this.requestData['header'].functionalityCode    = "USRA-UU";
    }
    else if (this.logedInUserRoles['allowedRoles'].indexOf("USRA-UUE") != -1){
      this.getReportingTo['header'].functionalityCode    = "USRA-UUE";
      this.getSelectedUser['header'].functionalityCode    = "USRA-UUE";
      // this.allRolesReq['header'].functionalityCode    = "USRA-UUE";
      this.userByNpi['header'].functionalityCode    = "USRA-UUE";
      this.requestData['header'].functionalityCode    = "USRA-UUE";
    }




    this.ngxLoader.start();
    this.getLookups().then(lookupsLoaded => {
      });
    // var url                = environment.API_ROLE_ENDPOINT + 'allroles';


    // this.getReportingTo.data.departmentId = depID;
    var url        = environment.API_USER_ENDPOINT + 'reportingto';
    this.getReportingTo.header.userCode    = this.logedInUserRoles.userCode
    this.getReportingTo.header.partnerCode = this.logedInUserRoles.partnerCode
    // this.getReportingTo.header.functionalityCode = "USRA-UU"
    // this.getReportingTo.data.partnerCode = this.logedInUserRoles.partnerCode
    this.userService.getReportingTo(url, this.getReportingTo).subscribe(resp => {
      console.log('resp Reporting To',resp)
      this.reportsTo = resp['data'];
    })


    this.route.params.subscribe(params => {
      console.log('params',params);
      // var bytes  = CryptoJS.AES.decrypt(params['id'], 'nglis');
      // console.log('bytes',bytes);
      // var originalText = bytes.toString(CryptoJS.enc.Utf8);
      // console.log("decryptedData",originalText);
      var coverted = params['id'].replace('xsarm','+').replace('ydan','/').replace('zhar','=');
      console.log("coverted",coverted);

      var bytes  = CryptoJS.AES.decrypt(coverted, 'nglis');


      var originalText = bytes.toString(CryptoJS.enc.Utf8).toString();
      console.log("originalText",originalText);

      // let id = params['id'];
      let id = originalText;

      // let url  = environment.API_USER_ENDPOINT + 'getuser?userId='+id+'';
      this.getSelectedUser.data.userCode = id;
      this.getSelectedUser.header.userCode    = this.logedInUserRoles.userCode
      this.getSelectedUser.header.partnerCode = this.logedInUserRoles.partnerCode
      // this.getSelectedUser.header.functionalityCode = "USRA-MUI"
      let url  = environment.API_USER_ENDPOINT + 'getuser';
      this.userService.getUsers(url,this.getSelectedUser).subscribe(resp => {
        console.log("respSelectedUser",resp);
        // this.selctedUser   = resp;
        // this.selctedUserID = id;
        if (typeof resp['result'] != 'undefined') {
          if (typeof resp['result']['description'] != 'undefined') {
            if (resp['result']['description'].indexOf('UnAuthorized') != -1) {
              this.notifier.notify('warning',resp['result']['description'])
              this.router.navigateByUrl('/page-restrict');
            }
          }
        }
        if (typeof resp['data'] == "undefined") {

          // $('#errorMSGnoUser').css("display","block");
          console.log('here');
          // this.responseMSG = "no user exist with this id "+id+".";
          // this.showError   = true;
          this.notifier.notify('error','No User Fount with ID: '+id+'')
          // this.swalStyle.fire({
          //   icon: 'error',
          //   type: 'error',
          //   title: 'No User Fount with ID: '+id+'',
          //   showConfirmButton: true,
          // })
          this.ngxLoader.stop();
        }
        else{
          if (resp['data']['userType'] == 2) {
            if (this.logedInUserRoles['allowedRoles'].indexOf("USRA-UUE") == -1) {
              // this.notifier.notify('warning','Unauthorized');
              this.router.navigateByUrl('/');
            }

            this.userForm.get('workingWithClient').disable();
            let url    = environment.API_CLIENT_ENDPOINT + 'clientsbyusercode';
            this.externalCLient.data.searchString = id;
            this.externalCLient.header.userCode    = this.logedInUserRoles.userCode;
            this.externalCLient.header.partnerCode = this.logedInUserRoles.partnerCode;
            this.externalCLient.header.functionalityCode = "USRA-MUE";
            this.httpRequest.httpPostRequests(url, this.externalCLient).then(respc => {
              console.log("resp",respc);
              if (respc['data']['details']['length'] != 0) {
                this.userClients = respc['data']['details'];
                var hold = [];
                for (let i = 0; i < respc['data']['details'].length; i++) {
                  hold.push(respc['data']['details'][i]['clientId'])

                }
                resp['data']['clientsAssociated'] = hold;
              }
              else{
                this.userClients = [];
              }


            })
            this.selctedUser   = resp['data'];
          }
          else{
            if (this.logedInUserRoles['allowedRoles'].indexOf("USRA-UU") == -1) {
              this.notifier.notify('warning','Unauthorized');
              this.router.navigateByUrl('/');
            }
            this.selctedUser   = resp['data'];
          }



          /////////  ROles form // DB
          var url1        = environment.API_ROLE_ENDPOINT + 'allroles';
          if (this.selctedUser.userType == 1) {
            this.allRolesReq.data.roleType = 0 ////// interna
          }
          else{
            this.allRolesReq.data.roleType = 1
          }
          this.allRolesReq.header.userCode = this.logedInUserRoles.userCode;
          this.allRolesReq.header.partnerCode = this.logedInUserRoles.partnerCode;
          this.allRolesReq.header.functionalityCode = "USRA-AUI";
          
          this.roleService.getAllRoles(url1, this.allRolesReq).subscribe(resp => {
            console.log('resp Role',resp)
            this.defaultRoles = resp['data'];
          })
          this.populateForm(id)
        }
      })
      // let guid = params['guid'];
      //
      // console.log(`${id},${guid}`);
    });
    // this.ngxLoader.stop();
    // this.selctedUser = history.state;
    // console.log('this.selctedUser',this.selctedUser);
    // console.log('this.selctedUser.userId',this.selctedUser.userId);
    // this.selctedUserID = this.selctedUser.userId;
    // if (typeof this.selctedUser.userId == "undefined") {
    //   // this.router.navigateByUrl('/all-users');
    // }
  //   if (this.selctedUser.npi == null) {
  //     this.selctedUser.npi = '';
  //   }
  //
  //   // // ajax call to get selected user Data from DB
  //   if (typeof this.selctedUser.npi == "undefined") {
  //     this.selctedUser.npi = "abc";
  //   }
  // if (this.selctedUser != '' || this.selctedUser !=null) {
    this.userForm  = this.formBuilder.group({
      title        : [''],
      firstName    : ['', [Validators.required]],
      lastName     : ['', [Validators.required]],
      npiNumber    : [''],
      postNominal  : ['', [Validators.required]],
      role         : ['', [Validators.required]],
      department   : ['1', [Validators.required]],
      reportsTo    : ['1'],
      isDoctor     : ['', [Validators.required]],
      mobileNo     : ['', [Validators.required,Validators.minLength(13)]],
      userEmail    : ['', [Validators.required, Validators.email]],
      userName     : ['', [Validators.required]],
      userStatus   : ['active', [Validators.required]],
      userType     : [''],
      workingWithClient: ['']
     });
  // }
  if (this.logedInUserRoles['allowedRoles'].indexOf("USRA-UU") == -1 && this.logedInUserRoles['allowedRoles'].indexOf("USRA-UUE") == -1) {
    this.notifier.notify('warning','Unauthorized');
    this.router.navigateByUrl('/');
  }

     // this.selectedPostNominal.id = '1';
     // this.selectedPostNominal  = parseInt(this.selctedUser.postNominal);
     // this.selectedDefaultRoles = 2;
     // this.userForm.setValue({postNominal: '1'});
     // console.log('this.userForm.value.postNominal',this.userForm.value.postNominal);
     /////////  Get Reporting To USers

     if (this.logedInUserRoles['allowedRoles'].indexOf("USRA-UU") == -1 && this.logedInUserRoles['allowedRoles'].indexOf("USRA-UUE") == -1) {
      this.router.navigateByUrl('/')
      // this.userForm.disable();
        // this.formToLink = true;
    }
  // this.ngxLoader.stop();
  }

  rolesChanged() {

    var data = this.requestData.data.user.roles;
    console.log("requestData.data.user.roles",data);
    if (data.length == 0) {
      console.log("empty");
      this.selctedUser.isDoctor = false;
      this.userForm.get('npiNumber').clearValidators();
      this.userForm.controls["npiNumber"].updateValueAndValidity();
      // if (this.selctedUser.isDoctor == true) {
      //
      // }
      // else{
      //   this.selctedUser.isDoctor = false;
      // }
    }
    else{
      for (let i = 0; i < data.length; i++) {
        if (data[i]['isPhysician'] == true) {
          // alert("doctor")
          this.selctedUser.isDoctor = true;
          this.userForm.get('npiNumber').setValidators([Validators.required])
          this.userForm.controls["npiNumber"].updateValueAndValidity();
          return;
          // console.log("selctedUser.isDoctor",this.selctedUser.isDoctor);
          // alert("doctor")

        }
        else{
          // this.selctedUser.isDoctor = false;
        }

      }
    }



    // console.log("this.requestData: ", this.requestData);
  }
  populateForm(id){
    this.ngxLoader.start()
    this.selctedUserID = id;

    if (typeof this.selctedUser.npi == "undefined" || this.selctedUser.npi == null) {
      this.selctedUser.npi = null;
    }
    else{
      this.requestData.data.user.npi   = this.selctedUser.npi;
    }
    this.requestData.data.user.roles  = this.selctedUser.roles;
    this.requestData.data.user.title  =  this.selctedUser.title

    if (this.selctedUser.active) {
      this.requestData.data.user.active = 'active';
    }
    else{
      this.requestData.data.user.active = 'inactive';
    }

    if (typeof this.selctedUser.reportingTo != "undefined") {
      this.reportingUser = this.selctedUser.reportingTo.userCode;
      console.log("this.reportingUser",this.reportingUser);

    }

    if (typeof this.selctedUser.postNominal != "undefined") {
      this.selectedPostNominal = this.selctedUser.postNominal;
    }
    if (typeof this.selctedUser.photo != 'undefined') {
      this.imgSrc = this.selctedUser.base64Image;
    }
    if(this.SameimgSrc != ""){
      this.imgSrc = this.SameimgSrc;
    }

    if (this.selctedUser.userType == 1) {
      this.userTypeCheckbox = "1";
    }
    else{
      this.userTypeCheckbox = "2";
    }
    this.requestData.data.user.departmentId = this.selctedUser.departmentId;
    this.requestData.data.user.createdBy = this.selctedUser.createdBy;
    this.requestData.data.createdBy = this.selctedUser.createdBy;





    this.ngxLoader.stop();
    this.formatePhoneUSAuto();
    this.rolesChanged();



    // this.getReportingTo.data.departmentId = depID;
    // var url        = environment.API_USER_ENDPOINT + 'reportingto';
    // this.httpRequest.httpPutRequests(url, this.getReportingTo).then(resp => {
    //   console.log('resp Reporting To',resp)
    //   this.reportsTo = resp.data;
    // })



    // // ajax call to get selected user Data from DB
    // this.selectedPostNominal  = this.selctedUser.postNominal;
    // console.log('this.selctedUser.postNominal',this.selctedUser.postNominal);
    // console.log('this.selectedPostNominal',this.selectedPostNominal);



  }

  changeCheck(){
    // console.log('reportingUser',this.reportingUser);
    // this.requestData.data.user.reportingTo.userId = this.reportingUser;
    // console.log('request',this.requestData.data.user);
  }

  get f() { return this.userForm.controls; }

  updateUser(){
    // // stop here if form is invalid
    this.submitted = true;
    this.ngxLoader.start();
    if (this.userForm.invalid) {
      // alert('form invalid');
      this.ngxLoader.stop();
      return;
    }
    // console.log("this.requestData.data.user.roles",this.requestData.data.user.roles);
    // console.log("selctedUser.isDoctor",this.selctedUser.isDoctor);
    // this.ngxLoader.stop(); return;
    var isdocCheck = 0;
    for (let index = 0; index < this.requestData.data.user.roles.length; index++) {
      if (this.requestData.data.user.roles[index].isPhysician == true) {
        isdocCheck++;
      }

    }
    if (isdocCheck >0 && this.selctedUser.isDoctor == false) {
      this.ngxLoader.stop();
      this.notifier.notify('warning','A Physician should be is doctor so please check "is doctor"')
      return;
    }


    // else{
    //
    // }


    this.saveUserData.otherData    = this.userForm.value;
    this.saveUserData.imagePayload = this.imagePayLoad;
    this.saveUserData.imageBase64  = this.imageBase64;
    // console.log('saveUserData',this.saveUserData);
    console.log('this.userForm.value',this.userForm.value);


    // this.saveData.userData                 = this.userForm.value;

    this.requestData.data.user.userCode           = this.selctedUserID;
    // this.requestData.data.user.displayCode      = "AB94";
    this.requestData.data.user.firstName        = this.userForm.value.firstName;
    this.requestData.data.user.lastName         = this.userForm.value.lastName;
    this.requestData.data.user.username         = this.userForm.value.userName;
    this.requestData.data.user.password         = null;
    this.requestData.data.user.email            = this.userForm.value.userEmail;
    this.requestData.data.user.phone            = this.userForm.value.mobileNo;
    this.requestData.data.user.photo            =null;
    this.requestData.data.user.title            =this.userForm.value.title;
    this.requestData.data.user.npi              =this.userForm.value.npiNumber;
    this.requestData.data.user.postNominal      =this.userForm.value.postNominal;
    // this.requestData.data.user.userType         = 1;
    // this.requestData.data.user.active           =true;
    this.requestData.data.user.agreementActive  =true;
    this.requestData.data.user.accessCode       ="AB94";
    this.requestData.data.user.isDoctor         =this.userForm.value.isDoctor;
    // this.requestData.data.user.createdBy        =1;
    this.requestData.data.user.createdTimestamp ="2020-08-28T19:10:37.000+00:00";

    if (this.requestData.data.user.active == 'active') {
      this.requestData.data.user.active = true;
    }
    else if(this.requestData.data.user.active == 'inactive'){
      this.requestData.data.user.active = false;
    }
    if (this.selctedUser.userType == 1) {
      this.requestData.data.user.userType = 1;
    }
    else{
      this.requestData.data.user.userType = 2
    }

    // this.requestData.data.user.base64Image     = this.imageBase64.replace(/^data:image\/[a-z]+;base64,/, "");

    if (typeof this.imagePayLoad != 'undefined') {
      console.log("undefined");

      this.imageName                             = this.imagePayLoad['name'];
      this.requestData.data.user.photo           = this.imageName;
    }
    else{
      if (typeof this.selctedUser.photo != 'undefined') {
        this.requestData.data.user.photo = this.selctedUser.photo;
        this.SameimgSrc                  = this.selctedUser.base64Image;
      }
      // this.requestData.data.user.photo = null;

    }
    this.requestData.data.user.base64Image     = this.imageBase64;


    if (this.reportingUser == 'select') {
      // console.log('in');
      this.requestData.data.user.reportingTo.userCode = null;
    }
    else{
      this.requestData.data.user.reportingTo.userCode = this.reportingUser;
    }

    var x = this.requestData.data.user.phone.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
    this.requestData.data.user.phone = !x[2] ? x[1] : '(' + x[1] + ')' + x[2] + (x[3] ? '-' + x[3] : '')
    // this.reportinToTemp.userId = this.requestData.data.user.reportingTo

    // console.log('this.imagePayLoad',this.imagePayLoad);

    console.log('this.requestData',this.requestData);
    // this.ngxLoader.stop(); return;
    this.requestData.header.userCode    = this.logedInUserRoles.userCode
    this.requestData.header.partnerCode = this.logedInUserRoles.partnerCode
    // this.requestData.header.functionalityCode = "USRA-UU"
    var url                = environment.API_USER_ENDPOINT + 'update';
    this.userService.updateUser(url,this.requestData).subscribe(resp => {
      console.log("resp",resp);
      // if (typeof resp['result'] == 'undefined') {
      //   this.notifier.notify( "error", "Error while updating user" );
      //   this.ngxLoader.stop();
      //   return;
      // }
      if (resp['result'].codeType == "S") {
        console.log('record added succesfully add response divs');
        this.notifier.notify( "success", "User updated Successfully" );
        this.selctedUser   = resp['data'];
        this.populateForm(this.getSelectedUser.data.userCode);

      }
      else{
        this.ngxLoader.stop();
        if (resp['result'].description.indexOf('[u_email]') !== -1) {
          this.notifier.notify( "error", "Provided email already exists in the system.");
        }
        else{
          this.responseMSG  = "There was an error while updating user.";
          this.notifier.notify( "error", "Error while updating user");
        }


      }
      setTimeout(function(){
        this.showSuccess   = false;
        this.showSuccess   = false;
        // $('#successMSG').css("display","none");
        // $('#errorMSG').css("display","none");
        // this.router.navigateByUrl('/editlisting', { state: this.listing });
      }, 3000);
    })

  }
  departmentChange(){
    /////////  Get Reporting To USers
    this.getReportingTo.data.departmentId = this.requestData.data.user.departmentId;
    this.getReportingTo.header.userCode    = this.logedInUserRoles.userCode;
    this.getReportingTo.header.partnerCode = this.logedInUserRoles.partnerCode;
    // this.getReportingTo.header.functionalityCode = "USRA-UU";
    var url        = environment.API_USER_ENDPOINT + 'reportingto';
    this.userService.getReportingTo(url, this.getReportingTo).subscribe(resp => {
      console.log('resp Reporting To',resp)
      this.reportsTo = this.sortPipe.transform(resp['data'], "asc", "firstName")
      // this.reportsTo = resp['data'];
    })
  }
  onFileSelect(input) {
    console.log('size',input.files[0]);
    var mb = input.files[0].size/1048576;
    console.log("mb",mb);
    if (mb <= 1) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = (e: any) => {
          // console.log('Got here: ', e.target.result);
          // console.log('input.files[0]: ', input.files[0]);
          this.imgSrc      = e.target.result;
          this.imageBase64 = e.target.result;
          // this.obj.photoUrl = e.target.result;
        }
        reader.readAsDataURL(input.files[0]);
      }
      // console.log('this.imageBase64: ', this.imageBase64);
      if(input.files && input.files.length > 0) {
        this.imagePayLoad = input.files[0];
      }
    }
    else{
      alert('File Size must be less then 1MB');
    }


  }

  showPasswordModal(){
    $('#resetpassModal').modal('show');

  }
  passwordRegex(e){
    var regexp = new RegExp(/^(?=.*[A-Z])(?=.*[!@#$&*_])(?=.*[0-9])(?=.*[a-z]).{8,100}$/);
    var reg = regexp.test(e.target.value);
    if (reg == true) {
      this.passwordError = false;
    }
    else{
      this.passwordError = true;
    }
  }
  passwordCheckRealTime(e){
    // var regexp = new RegExp(/^(?=.*[A-Z])(?=.*[!@#$&*_])(?=.*[0-9])(?=.*[a-z]).{8,100}$/);
    // var reg = regexp.test(e.target.value);
    // if (reg == true) {
    //   this.passwordError = false;
    // }
    // else{
    //   this.passwordError = true;
    // }
    // if (this.newPassword.length < 8) {
    //   this.minLength    = true;
    //   return;
    // }
    // else
    if (this.newPassword.length > 50) {
      this.maxLength    = true;
      return;
    }
    else{
      this.passMisMatch  = false;
      this.passEmpty     = false;
      this.CPassEmpty    = false;
      this.minLength     = false;
      this.maxLength     = false;
    }

    if (this.CNewPassword != '') {
      if (this.newPassword != this.CNewPassword) {
        this.passMisMatch  = true;
        if (this.newPassword.length < 8) {
          this.minLength    = true;
        }
        return;
      }
      else{
        this.passMisMatch  = false;
        this.passEmpty     = false;
        this.CPassEmpty    = false;
        this.minLength     = false;
        this.maxLength     = false;
      }

    }


  }
  updatePassword(){

    console.log('password');

    if (this.newPassword != this.CNewPassword) {
      this.passMisMatch  = true;
    }
    else if (this.newPassword == '') {
      this.passEmpty     = true;
      console.log('empty');

    }
    else if (this.newPassword == '') {
      this.CPassEmpty    = true;
    }
    else if (this.newPassword.length < 8) {
      this.minLength    = true;
    }
    else if (this.newPassword.length > 50) {
      this.maxLength    = true;
    }
    else{
      this.passMisMatch  = false;
      this.passEmpty     = false;
      this.CPassEmpty    = false;
      this.minLength     = false;
      this.maxLength     = false;
      this.ngxLoader.start();
      this.updatePassData.userID         = this.selctedUserID;
      // this.updatePassData.userID      = this.selctedUserID;
      this.updatePassData.newPasswrod    = this.newPassword;

      // this.updatePasswordData.userId    = this.selctedUserID;
      // this.updatePasswordData.password  = this.newPassword;
      this.updatePasswordData.data.user.userCode    = this.selctedUserID;
      this.updatePasswordData.data.user.password  = this.encryptDecrypt.encryptString(this.newPassword);
      this.updatePasswordData.header.userCode    = this.logedInUserRoles.userCode;
      this.updatePasswordData.header.partnerCode = this.logedInUserRoles.partnerCode;
      if (this.selctedUser.userType == 1) {
        this.updatePasswordData.header.functionalityCode = "USRA-RPI";

      }
      else{
        this.updatePasswordData.header.functionalityCode = "USRA-RPE";

      }
      // var url                = environment.API_USER_ENDPOINT + 'updatepassword';
      var url                = environment.API_USER_ENDPOINT + 'updatepassword';
      console.log("this.updatePasswordData",this.updatePasswordData);
      console.log("this.selctedUserID",this.selctedUserID);

      this.userService.updatePassword(url,this.updatePasswordData).subscribe(resp => {
        console.log("resp",resp);
        this.ngxLoader.stop();
        if (resp['result'].codeType == "S") {
          console.log('Password Updated succesfully add response divs');
          this.newPassword   = '';
          this.CNewPassword = '';
          this.notifier.notify('success','Password updated successfully')
          // $('#successMSGPassword').css("display","block");
          $('#resetpassModal').modal('hide');
          // this.notifier.notify( "sucess", "Password is updated successfully");

          // this.swalStyle.fire({
          //   icon: 'success',
          //   type: 'sucess',
          //   title: 'Password is updated',
          //   showConfirmButton: true,
          //   timer: 3000
          // })
          // $('#resetpassModal').modal('hide');
        }
        else{
          // $('#errorMSGPassword').css("display","block");
          this.notifier.notify('error','Error while saving password')
          $('#resetpassModal').modal('hide');
          // this.swalStyle.fire({
          //   icon: 'error',
          //   type: 'sucess',
          //   title: 'Error While Updating Password',
          //   showConfirmButton: true,
          //   timer: 3000
          // })
          // this.notifier.notify( "error", "Error while updating password");
          // $('#resetpassModal').modal('hide');

        }

      })
      this.newPassword   = '';
      this.CNewPassword  = '';
      this.passMisMatch  = false;
      this.passEmpty     = false;
      this.CPassEmpty    = false;
      // $('#resetpassModal').modal('hide');
      setTimeout(function(){
        $('#successMSGPassword').css("display","none");
        $('#errorMSGPassword').css("display","none");
        // $('#resetpassModal').modal('hide');
        // this.router.navigateByUrl('/editlisting', { state: this.listing });
      },3000);
    }
  }

  selectPicClick(){
    console.log('here');
    $('.selectPicture').click();

    // document.getElementsByClassName('selectPicture')[0].querySelector('button').click();
  }
  formatePhoneUS(e){
    // console.log("eeee",e);

    var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
    e.target.value = !x[2] ? x[1] : '(' + x[1] + ')' + x[2] + (x[3] ? '-' + x[3] : '')

  }
  formatePhoneUSAuto(){
    var x = this.selctedUser.phone.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
    this.selctedUser.phone = !x[2] ? x[1] : '(' + x[1] + ')' + x[2] + (x[3] ? '-' + x[3] : '')

    if(this.selctedUser.isDoctor) {
      // console.log('yes1');

      this.userForm.get('npiNumber').setValidators([Validators.required])
      this.userForm.controls["npiNumber"].updateValueAndValidity();

    } else {

      this.userForm.get('npiNumber').clearValidators();
      this.userForm.controls["npiNumber"].updateValueAndValidity();
      // console.log('no1');
    }

  }
  changeIsDoctor(e) {

    var value = e.target.checked;
    console.log("value",value);


    if(value) {
      // console.log('yes2');
      this.userForm.get('npiNumber').setValidators([Validators.required])
      this.userForm.controls["npiNumber"].updateValueAndValidity();

    } else {
      // console.log('no2');
      this.userForm.get('npiNumber').clearValidators();
      this.userForm.controls["npiNumber"].updateValueAndValidity();


    }
  }

  getNPI(){
    $('#NPI-spiner').css("display","inline-block");
    if (this.selctedUser.npi == '' || typeof this.selctedUser.npi == 'undefined') {
      $('#NPI-spiner').css("display","none");
      this.inValidUsernpi = false;
      return;
    }
    this.userByNpi.data.npi = this.selctedUser.npi;
    console.log("userByNpi",this.userByNpi);
    this.userByNpi.header.userCode    = this.logedInUserRoles.userCode;
    this.userByNpi.header.partnerCode = this.logedInUserRoles.partnerCode;
    this.userByNpi.header.functionalityCode = "USRA-UU";

    let url    = environment.API_USER_ENDPOINT + 'userbynpiemail';
    this.http.put(url,this.userByNpi).toPromise()
    .then( resp => {
      console.log('**** resp NPI: ', resp);
      this.userByNpi.data.npi   = "";
      // this.userByNpi.data.email = "";
      ////// already pushed
      // if (!found) arr.push({ id, username: name });
      $('#NPI-spiner').css("display","none");
      // this.ngxLoader.stop();
      if (resp["result"].codeType == "S") {
        this.inValidUsernpi = true;
      }
      else{
        this.inValidUsernpi = false;
      }
      //
    })
    .catch(error => {
      console.log("error NPI: ",error);
    });
  }

  getLookups(){
    // this.ngxLoader.start();
    return new Promise((resolve, reject) => {


      // var allRoles = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=ATTACHMENT_FILE_TYPE_CACHE&partnerCode=sip').then(response =>{
      //   return response;
      // })
      var allpostNom = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=POSTNOMINAL_CACHE&partnerCode=sip').then(response =>{
        return response;
      })
      var allTitle   = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=TITLE_CACHE&partnerCode=sip').then(response =>{
        return response;
      })
      var allDep    = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=DEPARTMENT_CACHE&partnerCode=sip').then(response =>{
        return response;
      })



      forkJoin([allTitle, allpostNom, allDep]).subscribe(allLookups => {
        console.log("allLookups",allLookups);
        this.allTittles             = this.sortPipe.transform(allLookups[0], "asc", "titleValue");
        this.dPostNominal           = this.sortPipe.transform(allLookups[1], "asc", "value");
        this.allDepartment           = this.sortPipe.transform(allLookups[2], "asc", "departmentName");

        resolve();
      })
    })

  }
  navigateOnCancel(){
    var fromClient = JSON.parse(localStorage.getItem('userFromEditCLient'));

    if (fromClient == null) {
      // console.log("this.selctedUser.userType",this.selctedUser.userType);
      if (this.selctedUser.userType == 2) {
        localStorage.setItem("toExternaTab","true");
        this.router.navigateByUrl('/all-users');
      }
      else{
        this.router.navigateByUrl('/all-users');
      }

    }
    else{
      localStorage.removeItem('userFromEditCLient');
      var a = fromClient+';1'
      // var ciphertext = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
      var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
      var bx = ax.replace('+','xsarm');
      var cx = bx.replace('/','ydan');
      var ciphertext = cx.replace('=','zhar');
      // this.router.navigateByUrl('/edit-client/'+ciphertext)
      this.router.navigate(['edit-client', ciphertext]);

      // this.router.navigateByUrl('/edit-client/'+fromClient+';from='+1)
    }
  }

}
