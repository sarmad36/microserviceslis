import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
// import { CLUMroutes } from './clum-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './includes/navbar/navbar.component';
import { SidebarComponent } from './includes/sidebar/sidebar.component';
import { FooterComponent } from './includes/footer/footer.component';
import { DashboardComponent } from './includes/dashboard/dashboard.component';
import { AddUserComponent } from './partner-admin/user/add-user/add-user.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { DataTablesModule } from 'angular-datatables';
import { ManageUsersComponent } from './partner-admin/user/manage-users/manage-users.component';
import { AddClientComponent } from './partner-admin/client/add-client/add-client.component';
import { EditUserComponent } from './partner-admin/user/edit-user/edit-user.component';
import { ManageClientComponent } from './partner-admin/client/manage-client/manage-client.component';
import { EditClientComponent } from './partner-admin/client/edit-client/edit-client.component';
import { NgxPopper } from 'angular-popper';
// import { ManagePatientComponent } from './patient/manage-patient/manage-patient.component';
import { HttpRequestsService } from './services/_services/http-requests/http-requests.service';
// import { RoutingStateService } from './services/_services/routing-history/routing-state.service';
import { NgxUiLoaderModule } from  'ngx-ui-loader';
import { NotifierModule, NotifierOptions } from 'angular-notifier';
import { ChangePasswordComponent } from './partner-admin/user/change-password/change-password.component';
// import { PhoneMaskDirective } from './directives/phone-mask.directive';
// import { ManageClientUsersComponent } from './partner-admin/client/manage-client-users/manage-client-users.component';
// import { ValidateEqualModule } from 'ng-validate-equal';
import { UserApiEndpointsService } from './services/users/user-api-endpoints.service';
import { ClientApiEndpointsService } from './services/client/client-api-endpoints.service';
import { RolesApiEndpointsService } from './services/roles/roles-api-endpoints.service';
import { PartnerApiEndpointsService } from './services/partner/partner-api-endpoints.service';
import { ChildComponent } from './partner-admin/user/manage-users/child/child.component';
// import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { GlobalApiCallsService } from './services/gloabal/global-api-calls.service';
import { SortPipe } from './pipes/sort.pipe';
import { RouterModule } from '@angular/router';
import { NglisPntmClientSharedModule } from '../../../nglis_pntm_client/src/app/app.module';
import { GlobalySharedModule } from './../../../../src/app/sharedmodules/globaly-shared/globaly-shared.module'
// import { RbacDirective } from './../../../../src/app/directives/rbac.directive'
// import { EncryptPipe, DecryptPipe } from './../../../../src/app/pipes/encrypt-decrypt.pipe';

const providers = [GlobalApiCallsService,HttpRequestsService, UserApiEndpointsService, ClientApiEndpointsService, RolesApiEndpointsService, PartnerApiEndpointsService, SortPipe];


/**
 * Custom angular notifier options
 */
const customNotifierOptions: NotifierOptions = {
  position: {
		horizontal: {
			position: 'left',
			distance: 110
		},
		vertical: {
			position: 'top',
			distance: 100,
			gap: 10
		}
	},
  theme: 'material',
  behaviour: {
    autoHide: 3000,
    onClick: 'hide',
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 300,
      easing: 'ease'
    },
    hide: {
      preset: 'fade',
      speed: 300,
      easing: 'ease',
      offset: 50
    },
    shift: {
      speed: 300,
      easing: 'ease'
    },
    overlap: 150
  }
};

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    FooterComponent,
    DashboardComponent,
    AddUserComponent,
    ManageUsersComponent,
    AddClientComponent,
    EditUserComponent,
    ManageClientComponent,
    EditClientComponent,
    ChangePasswordComponent,
    ChildComponent,
    // RbacDirective
    // PhoneMaskDirective,
    // ManageClientUsersComponent,
    // ManagePatientComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NglisPntmClientSharedModule.forRoot(),
    // RouterModule.forChild(CLUMroutes),
    NgSelectModule,
    DataTablesModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    NgxPopper,
    NgxUiLoaderModule,
    NotifierModule.withConfig(customNotifierOptions),
    GlobalySharedModule
    // ValidateEqualModule
  ],
  // {provide: LocationStrategy, useClass: HashLocationStrategy},
  providers: providers,
  bootstrap: [AppComponent]

})
export class AppModule { }

export class NglisClumClientSharedModule{

  static forRoot(): ModuleWithProviders<NglisClumClientSharedModule> {
    return {
      ngModule: AppModule,
      providers: providers
    }
  }
}
