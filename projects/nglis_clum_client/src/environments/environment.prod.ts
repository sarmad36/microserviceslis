export const environment = {
  production             : true,
  API_USER_ENDPOINT      : 'http://192.168.2.8:8088/apigateway/user/',
  API_PARTNER_ENDPOINT   : 'http://192.168.2.8:8088/apigateway/partner/',
  API_ROLE_ENDPOINT      : 'http://192.168.2.8:8088/apigateway/role/',
  API_CLIENT_ENDPOINT    : 'http://192.168.2.8:8088/apigateway/clientapi/',
  API_LOOKUP_ENDPOINT    : 'http://192.168.2.8:8088/apigateway/'
};
