import { Component, OnInit, OnDestroy,ElementRef, ViewChild, AfterViewInit, ViewChildren, QueryList } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { environment } from '../../../../../../src/environments/environment';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import Swal from 'sweetalert2';
declare var $ :any;
import { NotifierService } from 'angular-notifier';
import { GlobalApiCallsService } from '../../services/global/global-api-calls.service';
import { concat, Observable, of, Subject, forkJoin } from "rxjs";
import * as moment from 'moment';
import { SortPipe } from "../../pipes/sort.pipe";
import { DatePipe } from '@angular/common';
declare var $ :any;
import { GlobalySharedModule } from '../../../../../../src/app/sharedmodules/globaly-shared/globaly-shared.module';
import { UrlGuard } from './../../../../../../src/services/gaurd/url.guard'
// import 'rxjs/add/observable/interval';
import { interval } from 'rxjs';
import { Subscription } from "rxjs";
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-manage-reports',
  templateUrl: './manage-reports.component.html',
  styleUrls: ['./manage-reports.component.css'],
  host        : {
    class     :'mainComponentStyle'
  }
})
export class ManageReportsComponent implements OnInit {
  searchTerm$      = new Subject<string>();
  searchTermSearch$= new Subject<string>();
  searchTermSearch2$= new Subject<string>();
  searchTermSearch3$= new Subject<string>();
  searchTermColum$ = new Subject<string>();
  searchTermColumLnameAct$ = new Subject<string>();
  searchTermColumFnameAct$ = new Subject<string>();
  searchTermColumFnameAll$ = new Subject<string>();
  searchTermColumLnameAll$ = new Subject<string>();
  @ViewChildren(DataTableDirective)

  dtElement              : QueryList<any>;
  dtTrigger              : Subject<ManageReportsComponent> = new Subject();
  public dtOptions       : DataTables.Settings[] = [];
  public paginatePage    : any = 0;
  public paginatePage2   : any = 0;
  public paginatePaheHistory : any = 0;
  public paginateLengthHistory : any = 20;
  public paginateLength  = 20;
  oldCharacterCount      = 0;
  oldCharacterCount2     = 0;
  oldCharacterCount3     = 0;
  submitted              = false;
  results     : Object;
  specimenAllData : any = [];

  allCaseCategories         : any = [];
  allInterpretation         : any = [];
  selectedCaseCat           = null;
  allClientName             : any;
  allClientNameSearch       : any;
  allClientNameSearch2      : any;
  allClientNameSearch3      : any;
  allAtdPhysicians          : any;
  allStatusType             : any;
  public caseForm           : FormGroup;
  insuranceCompany          ;
  saveTypeGlobal            = null;
  saveModalTitle            = '';
  saveModalText             = '';

  clientSearchString          ;
  clientLoading               : any;
  clientLoadingSearch         : any;
  clientLoadingSearch2        : any;
  clientLoadingSearch3        : any;
  clientInput$                = new Subject<string>();

  deliveredReports      : any = []
  deliveredReportsCount = 0;
  pendingReport         : any =[];
  pendingReportCount    = 0;
  notDeliveredReports         : any =[];
  notDeliveredReportsCount    = 0;

  allSpecimenTypes        : any = [];
  specimenType            = null;
  collectDate = null;
  maxDate;
  defaultAccountTypes : any ;
  public logedInUserRoles :any = {};

  patientFName = null;
  patientLName = null;
  patientDOB   = null;
  selectedPatientID = null;
  cNameforSearch;
  cNameforSearch2;
  cNameforSearch3;
  searchtextHolder;
  searchtextHolder2;
  searchtextHolder3;
  selectedAttPhysician = null;
  printedCaseNumber= null;
  patientDOBPrint;
  lNamePrint
  fNamePrint;
  greenColor = false;
  clientNameToPrint;
  //dtElement              : QueryList<any>;
  //public dtOptions       : DataTables.Settings[] = [];
  //dtTrigger              : Subject<ManageReportsComponent> = new Subject();
  //public paginatePage    : any = 0;
  //public paginateLength  = 20;

  public showAllCasesReq = {
    header  : {
      uuid              :"",
      partnerCode       :"",
      userCode          :"",
      referenceNumber   :"",
      systemCode        :"",
      moduleCode        :"MRMM",
      functionalityCode: "MRMA-MR",
      systemHostAddress :"",
      remoteUserAddress :"",
      dateTime          :"",

    },
    data : {
      page              :"0",
      size              :5,
      sortColumn        :"caseNumber",
      sortingOrder      :"asc",
      statuses          :[8,10,11], // (all cases without underreporting)
      caseStatus        :"3",
      intakeId          :"",
      clientId            :""

    }
  }
  public showInterpretationReq = {
    header  : {
      uuid              :"",
      partnerCode       :"",
      userCode          :"",
      referenceNumber   :"",
      systemCode        :"",
      moduleCode        :"TWMM",
      functionalityCode: "MRMA-MR",
      systemHostAddress :"",
      remoteUserAddress :"",
      dateTime          :""
    },
    data : {
      caseIds           :[4,3]

    }
  }
  public searchByCriteria : any ={
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"SPCM",
      functionalityCode: "MRMA-MR",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      page:"0",
      size:"20",
      sortColumn:"caseNumber",
      sortingOrder:"desc",
      collectionDate:"",
      caseNumber:"",
      clientId:"",
      collectedBy:"",
      caseStatusId:"",
      statuses:[],
      intakeId : '',
      userCode:""

    }

  }
  public patientByIdsReq   ={
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"",
      functionalityCode: "MRMA-MR",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      username:"danish",
      patientIds:[]
    }

  }
  public allCasesReq = {
    header  : {
      uuid              :"",
      partnerCode       :"",
      userCode          :"",
      referenceNumber   :"",
      systemCode        :"",
      moduleCode        :"SPCM",
      functionalityCode: "MRMA-MR",
      systemHostAddress :"",
      remoteUserAddress :"",
      dateTime          :""
    },
    data : {
      page         :"0",
      size         :5,
      sortColumn   :"caseNumber",
      sortingOrder :"desc",
      statuses     :[],
      caseStatus        :"3",
      intakeId          :"",
      client            :""

    }
  }
  public saveRequest          = {
    header  : {
      dateTime: "",
      functionalityCode: "",
      moduleCode: "SPCM",
      partnerCode: "",
      referenceNumber: "",
      remoteUserAddress: "",
      systemCode: "",
      systemHostAddress: "",
      userCode: "",
      uuid: ""
    },
    data:{

      caseCategoryPrefix:"NH",
      caseNumber:"",
      caseStatusId:1,
      caseCategoryId:null,
      clientId:null,
      attendingPhysicianId:"376",
      patientId:null,
      createdTimestamp:"2020-10-22T11:02:03.523+00:00",
      receivingDate:"2020-10-22T07:54:10.485+00:00",
      collectedBy:"1",
      collectionDate:null,
      accessionTimestamp:"2020-10-22T07:54:10.485+00:00",
      createdBy:"abc",
      caseSpecimen:[
        {
          // caseSpecimenId:1,
          specimenTypeId:2,
          specimenSourceId:1,
          bodySiteId:1,
          procedureId:2,
          createdBy:"abc",
          createdTimestamp:"2020-10-22T07:54:10.485+00:00",
          updatedBy:"",
          updatedTimestamp:""
        }
      ]
    }


  }
  public pendingReportReq = {
    header  : {
      uuid              :"",
      partnerCode       :"",
      userCode          :"",
      referenceNumber   :"",
      systemCode        :"",
      moduleCode        :"SPCM",
      functionalityCode: "MRMA-MR",
      systemHostAddress :"",
      remoteUserAddress :"",
      dateTime          :""
    },
    data : {
      pageNumber         :"0",
      pageSize         :5,
      sortColumn   :"caseId",
      sortingOrder :"desc",
      // statuses     :[8,9,11],
      caseStatus        :"3",
      intakeId          :"",
      clientId            :"",
      userCode          :""
    }
  }
  public pendingReportReqNew = {
    header  : {
      uuid              :"",
      partnerCode       :"",
      userCode          :"",
      referenceNumber   :"",
      systemCode        :"",
      moduleCode        :"SPCM",
      functionalityCode: "MRMA-MR",
      systemHostAddress :"",
      remoteUserAddress :"",
      dateTime          :""
    },
    data : {}
  }
  public consolidatedReportRequest = {
    header:{
      uuid:"67ffaebe-6188-476e-86ae-c5deb2effa80",
      partnerCode:"sip",
      userCode:"sip-12588",
      referenceNumber:"TWMM12032020115628",
      systemCode:"NGLIS",
      moduleCode:"TWMM",
      functionalityCode:"TWMA-UR",
      systemHostAddress:"0:0:0:0:0:0:0:1",
      remoteUserAddress:"0:0:0:0:0:0:0:1",
      dateTime:"12/03/2020 11:56:28"
    },
    data:{
      // clientName:"Express Healthcare, LLC",
      // reportDate:"08/10/2020",
      // creationDate:"12/11/2020",
      // clientEmail:"",
      // reportTemplateId:1,
      // reportPackageId:3,
      // fileTypeId:1,
      // reportDeliveryId:1,
      // caseResults:[
      //   {
      //     sNo:"1",
      //     lastName:"Abduljabbar",
      //     firstName:"Ayad",
      //     caseNumber:"TV20-10320",
      //     specimenType:"Nasal,Swab",
      //     result:"Negative",
      //     jobOrderId:3,
      //     patientRevisionId:23,
      //     collectionDate:"12/11/2020"
      //   }
      // ]
    }
  }
  listForconsolidatedReport = [];
  listForconsolidatedReportDelivered = [];
  listForconsolidatedReportnotDelivered = [];
  defaultFileType : any = [];
  deliverMethod : any = [];
  public showHistoryRequest          = {
    header  : {
      dateTime: "",
      functionalityCode: "",
      moduleCode: "MRMM",
      partnerCode: "",
      referenceNumber: "",
      remoteUserAddress: "",
      systemCode: "",
      systemHostAddress: "",
      userCode: "",
      uuid: ""
    },
    data:{
      jobOrderId:8,
      deliveryStatusId:3

    }


  }
  interIDforSearch  = '';
  interIDforSearch2 = '';
  interIDforSearch3 = '';
  public usersRequest: any = {
    header              : {
      uuid              : "",
      partnerCode       : "",
      userCode          : "",
      referenceNumber   : "",
      systemCode        : "",
      moduleCode        : "",
      functionalityCode : "MRMA-MR",
      systemHostAddress : "",
      remoteUserAddress : "",
      dateTime          : ""
    },
    data                : {
      userCodes           :[]
    }
  }
  reportPackages : any = [];
  reportTemplate : any = [];
  deliverStatus  : any = [];
  allHistoryData : any = [];
  allHistoryDataCount : any = [];

  public updateCaseStatusReq = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"TWMM",
      functionalityCode: "MRMA-MR",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      jobOrderId:[],
      testStatusId:5,
      updatedBy:"abc",
      updatedTimestamp: "30-11-2020 20:07:25"

    }
  }
  successFileURL;
  public allIntakesReq = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"SPCM",
      functionalityCode: "MRMA-MR",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      page:"0",
      size:200,
      sortColumn:"createdTimestamp",
      sortingOrder:"desc",
      clientId:[],
      searchString:""
    }

  }
  allIntakesForClient : any = [];
  disableIntake = true;
  selectedIntakeforClient = null;
  disableSearchButton   = true;
  disableDeliveryHistoryButton = true;
  mrc = false;
  dcr = false;
  cr = false;

  mrc1 = false;
  dcr1 = false;
  cr1 = false;

  mrc2 = false;
  dcr2 = false;
  cr2 = false;

  public multipleReportRequest = {
    header:{
      uuid:"67ffaebe-6188-476e-86ae-c5deb2effa80",
      partnerCode:"sip",
      userCode:"sip-12588",
      referenceNumber:"TWMM12032020115628",
      systemCode:"NGLIS",
      moduleCode:"TWMM",
      functionalityCode:"TWMA-UR",
      systemHostAddress:"0:0:0:0:0:0:0:1",
      remoteUserAddress:"0:0:0:0:0:0:0:1",
      dateTime:""
    },
    data:{
      summaryReport:{
        // clientName:"",
        // reportDate:"",
        // creationDate:"",
        // clientEmail:"",
        // reportTemplateId:1,
        // reportPackageId:3,
        // fileTypeId:1,
        // reportDeliveryMethodId:1,
        // caseResults:[
        //    {
        //    sNo:"1",
        //    lastName:"",
        //    firstName:"",
        //    patientDob:"",
        //    caseNumber:"TV20-10320",
        //    specimenType:"Nasal,Swab",
        //    result:"Negative",
        //    jobOrderId:3,
        //    patientRevisionId:23,
        //    collectionDate:"12/11/2020"
        //    },
        //    {
        //    sNo:"2",
        //    lastName:"Davis",
        //    firstName:"Sherman",
        //    patientDob:"02/24/1982",
        //    caseNumber:"TV20-103190",
        //    specimenType:"Nasal,Swab",
        //    result:"Positive",
        //    jobOrderId:6,
        //    patientRevisionId:25,
        //    collectionDate:"12/10/2020"
        //    },
        //    {
        //    sNo:"3",
        //    lastName:"Hart",
        //    firstName:"Tommie",
        //    patientDob:"02/24/1982",
        //    caseNumber:"TV20-10324",
        //    specimenType:"Nasal,Swab",
        //    result:"Positive",
        //    jobOrderId:8,
        //    patientRevisionId:29,
        //    collectionDate:"12/09/2020"
        //    }
        //   ]

      },
      detailedConsolidatedReport:{
        // clientName:"Express Healthcare, LLC",
        // reportDate:"08/10/2020",
        // creationDate:"12/11/2020",
        // clientEmail:"",
        // reportTemplateId:1,
        // reportPackageId:null,
        // reportPackageIdCompressed:5,
        // fileTypeId:1,
        // fileTypeIdCompressed:1,
        // reportDeliveryMethodId:1,
        // reportData:[
        //     {
        //    caseNumber:"TV20-0001",
        //    collectedDate:"12/11/2020",
        //    sentDate:"12/10/2020",
        //    submittedDate:"12/12/2020",
        //    patientName:"Abduljabbar, Ayad",
        //    patientDob:"02/24/1982",
        //    patientSex:"male",
        //    patientTell:"0251245365",
        //    client:"Express Healthcare, LLC",
        //    accountNumber:"12345-1",
        //    attendingPhysicianName:"Alex Jones",
        //    physicianTel:"051236632",
        //    address:"New York",
        //    diagnosis:"Nasal,Swab: Positive for SARS-CoV-2 (COVID-19) RNA",
        //             comment:"The TaqPathTM COVID-19 Combo Kit contains the assays and controls for a real-time reverse transcription polymerase chain reaction (RT-PCR). The test is intended for the qualitative detection of nucleic acid from SARS-CoV-2 in saliva, nasal, oropharyngeal, and nasopharyngeal swabs, nasopharyngeal and tracheal aspirates, and bronchoalveolar lavage (BAL) specimens from individuals suspected of COVID-19 by their healthcare provider. The FDA has not fully evaluated the TaqPathTM COVID-19 Combo Kit, but has approved it for use only under Emergency Use Authorization (EUA).\n\nA positive test result for COVID-19 indicates that RNA from SARS-CoV-2 was detected, and the patient is presumptively infected with the virus and presumed to be contagious. Laboratory test results should always be considered in the context of clinical observations and epidemiologic data in making a final diagnosis and patient management decisions. Patient management should follow current CDC guidelines. The TaqPathTM COVID-19 Combo Kit has been designed to minimize the likelihood of false positive test results. However, in the event of a false positive result, risks to patients could include any of the following: a recommendation for isolation of the individual, monitoring of household or other close contacts for symptoms, individual isolation that might limit contact with family or friends and may increase contact with other potentially COVID-19 patients, limits in the ability to work, delayed diagnosis and treatment for the true infection causing the symptoms, unnecessary prescription of a treatment or therapy, and/or other unintended adverse effects. Reasons for false positive results include - but are not limited to – cross-reactivity with other non-SARS-CoV-2 \"COVID-19\" family of Corona viruses, specimen mix-up, RNA contamination during product handling and/or cross contamination during specimen handling, preparation or testing. A negative test result means that SARSCoV-2 \"COVID-19\" RNA was not present in the specimen above the limit of detection. However, a negative result does not rule out COVID-19 and should not be used as the sole basis for treatment or patient management decisions. No evidence-based guidelines are established to determine the clinical sensitivity of the test in detecting carrier status or infection with SARS-CoV-2 COVID-19, particularly in the ambulatory setting (individuals with no symptoms). Nasopharyngeal specimens are most sensitive in an ambulatory setting. The limited available data suggests that the sensitivity of nasopharyngeal swabs may be in the range of 70-75%. Therefore, when diagnostic testing is negative, the possibility of a false negative result should be considered in the context of a patient's recent exposures and/or the presence of clinical signs and symptoms consistent with COVID-19. The possibility of a false negative result should especially be considered if the patient's recent exposures or clinical presentation indicate that COVID19 is likely, and diagnostic tests for other causes of illness (e.g., other respiratory illness) are negative. If COVID-19 is still suspected based on exposure history and/or other clinical findings, re-testing should be considered in consultation with public health authorities. In the event of a false negative result, risks to patients could include any of the following: delayed or lack of supportive treatment, lack of monitoring of infected individuals and their household or other close contacts for symptoms resulting in increased risk of spread of COVID-19 within the community, and/or other unintended adverse events. Causes for false negative results include - but are not limited to -: improper sample collection, degradation of the SARS-CoV-2 RNA during shipping/storage, the presence of RT-PCR inhibitors in the sample, reagents or supplies, specimen mix-up, significant mutation in the SARS-CoV-2 virus and specimen collection after SARS-CoV-2 RNA can no longer be found in the site sampled.\n\nPossible causes of indeterminate results include - but are not limited to -: suboptimal sample collection, improper specimen handling or shipping, low quantity of viral target in the clinical specimen approaching the Limit of Detection (LoD) of the assay, the presence of RT-PCR inhibitors in the sample, reagents or supplies, specimen mix-up, and/or low level non-specific nucleic acid amplifications. Please resubmit an additional specimen at no charge, if pertinent. The most up-to-date information on COVID-19 is available at the CDC General webpage: https://www.cdc.gov/COVID19. For additional information, please contact your healthcare provider.",
        //    jobOrderId:3,
        //    patientRevisionId:23
        //     },
        //     {
        //    caseNumber:"TV20-103190",
        //    collectedDate:"12/11/2020",
        //    sentDate:"12/10/2020",
        //    submittedDate:"12/12/2020",
        //    patientName:"Davis, Sherman",
        //    patientDob:"02/24/1982",
        //    patientSex:"male",
        //    patientTell:"054232588",
        //    client:"Express Healthcare, LLC",
        //    accountNumber:"12345-1",
        //    attendingPhysicianName:"Alex Jones",
        //    physicianTel:"051236632",
        //    address:"New York",
        //    daignosis:"Nasal,Swab: Positive for SARS-CoV-2 (COVID-19) RNA",
        //             comment:"The TaqPathTM COVID-19 Combo Kit contains the assays and controls for a real-time reverse transcription polymerase chain reaction (RT-PCR). The test is intended for the qualitative detection of nucleic acid from SARS-CoV-2 in saliva, nasal, oropharyngeal, and nasopharyngeal swabs, nasopharyngeal and tracheal aspirates, and bronchoalveolar lavage (BAL) specimens from individuals suspected of COVID-19 by their healthcare provider. The FDA has not fully evaluated the TaqPathTM COVID-19 Combo Kit, but has approved it for use only under Emergency Use Authorization (EUA).\n\nA positive test result for COVID-19 indicates that RNA from SARS-CoV-2 was detected, and the patient is presumptively infected with the virus and presumed to be contagious. Laboratory test results should always be considered in the context of clinical observations and epidemiologic data in making a final diagnosis and patient management decisions. Patient management should follow current CDC guidelines. The TaqPathTM COVID-19 Combo Kit has been designed to minimize the likelihood of false positive test results. However, in the event of a false positive result, risks to patients could include any of the following: a recommendation for isolation of the individual, monitoring of household or other close contacts for symptoms, individual isolation that might limit contact with family or friends and may increase contact with other potentially COVID-19 patients, limits in the ability to work, delayed diagnosis and treatment for the true infection causing the symptoms, unnecessary prescription of a treatment or therapy, and/or other unintended adverse effects. Reasons for false positive results include - but are not limited to – cross-reactivity with other non-SARS-CoV-2 \"COVID-19\" family of Corona viruses, specimen mix-up, RNA contamination during product handling and/or cross contamination during specimen handling, preparation or testing. A negative test result means that SARSCoV-2 \"COVID-19\" RNA was not present in the specimen above the limit of detection. However, a negative result does not rule out COVID-19 and should not be used as the sole basis for treatment or patient management decisions. No evidence-based guidelines are established to determine the clinical sensitivity of the test in detecting carrier status or infection with SARS-CoV-2 COVID-19, particularly in the ambulatory setting (individuals with no symptoms). Nasopharyngeal specimens are most sensitive in an ambulatory setting. The limited available data suggests that the sensitivity of nasopharyngeal swabs may be in the range of 70-75%. Therefore, when diagnostic testing is negative, the possibility of a false negative result should be considered in the context of a patient's recent exposures and/or the presence of clinical signs and symptoms consistent with COVID-19. The possibility of a false negative result should especially be considered if the patient's recent exposures or clinical presentation indicate that COVID19 is likely, and diagnostic tests for other causes of illness (e.g., other respiratory illness) are negative. If COVID-19 is still suspected based on exposure history and/or other clinical findings, re-testing should be considered in consultation with public health authorities. In the event of a false negative result, risks to patients could include any of the following: delayed or lack of supportive treatment, lack of monitoring of infected individuals and their household or other close contacts for symptoms resulting in increased risk of spread of COVID-19 within the community, and/or other unintended adverse events. Causes for false negative results include - but are not limited to -: improper sample collection, degradation of the SARS-CoV-2 RNA during shipping/storage, the presence of RT-PCR inhibitors in the sample, reagents or supplies, specimen mix-up, significant mutation in the SARS-CoV-2 virus and specimen collection after SARS-CoV-2 RNA can no longer be found in the site sampled.\n\nPossible causes of indeterminate results include - but are not limited to -: suboptimal sample collection, improper specimen handling or shipping, low quantity of viral target in the clinical specimen approaching the Limit of Detection (LoD) of the assay, the presence of RT-PCR inhibitors in the sample, reagents or supplies, specimen mix-up, and/or low level non-specific nucleic acid amplifications. Please resubmit an additional specimen at no charge, if pertinent. The most up-to-date information on COVID-19 is available at the CDC General webpage: https://www.cdc.gov/COVID19. For additional information, please contact your healthcare provider.",
        //    jobOrderId:6,
        //    patientRevisionId:25
        //     },
        //     {
        //    caseNumber:"TV20-10324",
        //    collectedDate:"12/11/2020",
        //    sentDate:"12/10/2020",
        //    submittedDate:"12/12/2020",
        //    patientName:"Hart, Tommie",
        //    patientDob:"02/24/1982",
        //    patientSex:"male",
        //    patientTell:"054232588",
        //    client:"Express Healthcare, LLC",
        //    accountNumber:"12345-1",
        //    attendingPhysicianName:"Alex Jones",
        //    physicianTel:"051236632",
        //    address:"New York",
        //    daignosis:"Nasal,Swab: Positive for SARS-CoV-2 (COVID-19) RNA",
        //             comment:"The TaqPathTM COVID-19 Combo Kit contains the assays and controls for a real-time reverse transcription polymerase chain reaction (RT-PCR). The test is intended for the qualitative detection of nucleic acid from SARS-CoV-2 in saliva, nasal, oropharyngeal, and nasopharyngeal swabs, nasopharyngeal and tracheal aspirates, and bronchoalveolar lavage (BAL) specimens from individuals suspected of COVID-19 by their healthcare provider. The FDA has not fully evaluated the TaqPathTM COVID-19 Combo Kit, but has approved it for use only under Emergency Use Authorization (EUA).\n\nA positive test result for COVID-19 indicates that RNA from SARS-CoV-2 was detected, and the patient is presumptively infected with the virus and presumed to be contagious. Laboratory test results should always be considered in the context of clinical observations and epidemiologic data in making a final diagnosis and patient management decisions. Patient management should follow current CDC guidelines. The TaqPathTM COVID-19 Combo Kit has been designed to minimize the likelihood of false positive test results. However, in the event of a false positive result, risks to patients could include any of the following: a recommendation for isolation of the individual, monitoring of household or other close contacts for symptoms, individual isolation that might limit contact with family or friends and may increase contact with other potentially COVID-19 patients, limits in the ability to work, delayed diagnosis and treatment for the true infection causing the symptoms, unnecessary prescription of a treatment or therapy, and/or other unintended adverse effects. Reasons for false positive results include - but are not limited to – cross-reactivity with other non-SARS-CoV-2 \"COVID-19\" family of Corona viruses, specimen mix-up, RNA contamination during product handling and/or cross contamination during specimen handling, preparation or testing. A negative test result means that SARSCoV-2 \"COVID-19\" RNA was not present in the specimen above the limit of detection. However, a negative result does not rule out COVID-19 and should not be used as the sole basis for treatment or patient management decisions. No evidence-based guidelines are established to determine the clinical sensitivity of the test in detecting carrier status or infection with SARS-CoV-2 COVID-19, particularly in the ambulatory setting (individuals with no symptoms). Nasopharyngeal specimens are most sensitive in an ambulatory setting. The limited available data suggests that the sensitivity of nasopharyngeal swabs may be in the range of 70-75%. Therefore, when diagnostic testing is negative, the possibility of a false negative result should be considered in the context of a patient's recent exposures and/or the presence of clinical signs and symptoms consistent with COVID-19. The possibility of a false negative result should especially be considered if the patient's recent exposures or clinical presentation indicate that COVID19 is likely, and diagnostic tests for other causes of illness (e.g., other respiratory illness) are negative. If COVID-19 is still suspected based on exposure history and/or other clinical findings, re-testing should be considered in consultation with public health authorities. In the event of a false negative result, risks to patients could include any of the following: delayed or lack of supportive treatment, lack of monitoring of infected individuals and their household or other close contacts for symptoms resulting in increased risk of spread of COVID-19 within the community, and/or other unintended adverse events. Causes for false negative results include - but are not limited to -: improper sample collection, degradation of the SARS-CoV-2 RNA during shipping/storage, the presence of RT-PCR inhibitors in the sample, reagents or supplies, specimen mix-up, significant mutation in the SARS-CoV-2 virus and specimen collection after SARS-CoV-2 RNA can no longer be found in the site sampled.\n\nPossible causes of indeterminate results include - but are not limited to -: suboptimal sample collection, improper specimen handling or shipping, low quantity of viral target in the clinical specimen approaching the Limit of Detection (LoD) of the assay, the presence of RT-PCR inhibitors in the sample, reagents or supplies, specimen mix-up, and/or low level non-specific nucleic acid amplifications. Please resubmit an additional specimen at no charge, if pertinent. The most up-to-date information on COVID-19 is available at the CDC General webpage: https://www.cdc.gov/COVID19. For additional information, please contact your healthcare provider.",
        //    jobOrderId:8,
        //    patientRevisionId:29
        //     }
        //   ]

      }
    }
  }

  public LoadCaseforReportingReq =
  {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"SPCM",
      functionalityCode: "MRMA-MR",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      // caseStatus:"3",
      intakeId:"",
      client:0,
    }
  }

  public showReportingJobsReq=
  {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"TWMM",
      functionalityCode: "MRMA-MR",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      caseIds: [],
      testStatusId: "6" //under reporting status
    }
  }

  public finalReportReq=
  {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"TWMM",
      functionalityCode: "MRMA-MR",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      jobOrderIds:[],
      // deliveryStatusId:3
      pageSize:20,
      pageNumber:0,
      intakeId:""
    }
  }
  allReportsData : any = [];

  triggerHistoryTable = false;
  GLobalUniqueJoBOrderIdsForHistory = [];
  compeleteAddressForReport = '';
  timeCount ;
  timeCount0 ;
  timeCount1 ;
  timeCount2 ;
  multipleReportsStatusCheck : any = [];
  reportMessageCheck : any = {};
  calledonce = false;
  subscription: Subscription;
  enablePendingReportTypeCheckBoxes = false;
  enableDeliveredReportTypeCheckBoxes = false;
  enableNotDeliveredReportTypeCheckBoxes = false;

  enableSendButtonPending = [];
  enableSendButtonDelivered = [];
  enableSendButtonNotDelivered = [];

  enableButtonPending = false;
  enableButtonDelivered = false;
  enableButtonNotDelivered = false;
  selectedIntakeRecord = null;


  constructor(
    private http            : HttpClient,
    private router          : Router ,
    private route           : ActivatedRoute,
    private sanitizer       : DomSanitizer,
    private ngxLoader       : NgxUiLoaderService,
    private notifier        : NotifierService,
    private globalService   : GlobalApiCallsService,
    private sortPipe        :SortPipe,
    private datePipe: DatePipe,
    private rbac         : UrlGuard,
    private encryptDecrypt  :GlobalySharedModule
  ) {

    this.rbac.checkROle('MRMA-MR');
    this.notifier        = notifier;
  }

  ngOnInit(): void {
    // localStorage.removeItem('reportStatusArray');

    this.subscription = interval(15000).subscribe(x => {
      // localStorage.removeItem('reportStatusArray');
      // this.reportMessageCheck = JSON.parse(localStorage.getItem('reportStatusArray'))
      // console.log("reportMessageCheck",this.reportMessageCheck);
      // if (this.reportMessageCheck != null) {

      if (this.calledonce == false) {
        this.checkReportEmailStatus();

      }
      // }
      // this.checkReportEmailStatus();
    });



    var dtToday = new Date();

    var m = dtToday.getMonth()+1;
    var d = dtToday.getDate();
    var year = dtToday.getFullYear();
    var month;
    var day;
    if(m < 10)
    month = '0' + m.toString();
    else
    month = m
    if(d < 10)
    day = '0' + d.toString();
    else
    day = d

    var maxDate = year + '-' + month + '-' + day;
    this.maxDate = maxDate;
    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
    var originalText = bts.toString(CryptoJS.enc.Utf8);
    this.logedInUserRoles = JSON.parse(originalText)
    // console.log("------",this.logedInUserRoles);

    // this.logedInUserRoles.userName    = "snazirkhanali";
    // this.logedInUserRoles.partnerCode = 1;
    // this.getLookups().then(lookupsLoaded => {
    //
    //   this.ngxLoader.stop();
    // });
    // console.log("SARMADNAZIRABBASI",history.state.fromCreate);

    if(typeof history.state.fromCreate != "undefined"){
      this.greenColor = true;
      // this.setOnce = true;
    }
    else{
      if (localStorage.getItem('FromCreate')=='true') {
        // console.log("in set once --------------");

        this.greenColor = true;
      }
    }

    this.globalService.search(this.searchTerm$)
    .subscribe(results => {
      console.log("results",results);
      this.allClientName = results['data'];
      this.clientLoading = false;
    },
    error => {
      this.ngxLoader.stop();
      this.notifier.notify( "error", "Error while fetching clients backend connect aborted" );
      this.clientLoading = false;
      return;
      // console.log("error",error);
    });

    /////////////// load client Name for search active
    this.globalService.search(this.searchTermSearch$)
    .subscribe(resultsSearch => {
      console.log("results",resultsSearch);
      this.allClientNameSearch = resultsSearch['data'];
      this.clientLoadingSearch = false;
    },
    error => {
      this.ngxLoader.stop();
      this.notifier.notify( "error", "Error while fetching clients backend connect aborted" );
      this.clientLoadingSearch = false;
      return;
      // console.log("error",error);
    });

    /////////////// load client Name for search
    this.globalService.search(this.searchTermSearch2$)
    .subscribe(resultsSearch2 => {
      console.log("results",resultsSearch2);
      this.allClientNameSearch2 = resultsSearch2['data'];
      this.clientLoadingSearch2 = false;
    },
    error => {
      this.ngxLoader.stop();
      this.notifier.notify( "error", "Error while fetching clients backend connect aborted" );
      this.clientLoadingSearch2 = false;
      return;
      // console.log("error",error);
    });

    /////////////// load client Name for search
    this.globalService.search(this.searchTermSearch3$)
    .subscribe(resultsSearch3 => {
      console.log("results",resultsSearch3);
      this.allClientNameSearch3 = resultsSearch3['data'];
      this.clientLoadingSearch3 = false;
    },
    error => {
      this.ngxLoader.stop();
      this.notifier.notify( "error", "Error while fetching clients backend connect aborted" );
      this.clientLoadingSearch3 = false;
      return;
      // console.log("error",error);
    });

    this.dtOptions[0] = {
      pagingType   : 'full_numbers',
      pageLength   : this.paginateLength,
      serverSide   : true,
      processing   : true,
      // ordering     : false,
      "order": [[ 10, "desc" ]],
      columnDefs: [
        { orderable: true, className: 'reorder', targets: [1,2,3,4,5,6,7,8, 'desc'] },
        { orderable: false, targets: [0,9] },
        {
          targets: [10],
          visible: false,
          searchable: false,
          orderable: true,
        },
      ],
      "dom": "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-3'i><'col-sm-12 col-md-3'f><'col-sm-12 col-md-4'p>>"+
      "<'row'<'col-sm-12'tr>>" +
      "<'row'<'col-sm-12 col-md-5'l><'col-sm-12 col-md-3'i><'col-sm-12 col-md-4'p>>",
      "lengthMenu": [20, 50, 75, 100 ],
      ajax: (dataTablesParameters: any, callback) => {

        if(this.timeCount0) {
          clearTimeout(this.timeCount0);
          // console.log("here",this.timeCount);

          this.timeCount0 = null;
        }
        this.timeCount0 = setTimeout(()=>{
          if ((this.selectedIntakeforClient == null || this.selectedIntakeforClient == "") && (this.clientSearchString == null || this.clientSearchString =="")) {
            this.pendingReportCount    = 0;
            this.deliveredReportsCount = 0;
            this.notDeliveredReportsCount   = 0;
            callback({
              recordsTotal    :  0,
              recordsFiltered :  0,
              data: []
            });
            return;
          }

          this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {



            dtElement.dtInstance.then((dtInstance: any) => {
              if(dtInstance.table().node().id == 'pending_reports'){
                console.log("dataTablesParameters",dataTablesParameters);
                // return;
                this.paginatePage2          = dtElement['dt'].page.info().page;
                this.paginateLength         = dtElement['dt'].page.len();
                var sortColumn = dataTablesParameters.order[0]['column'];
                var sortOrder  = dataTablesParameters.order[0]['dir'];
                var sortArray  = ["caseNumber","caseNumber",'patientLastName','patientFirstName','patientDateOfBirth','clientName','caseCollectionDate','caseCollectedBy','interpretationDiagnosis','createdTimestamp','createdTimestamp','createdTimestamp']
                var reqAllCases = {...this.pendingReportReqNew}
                var columnToSearch          = "";
                var searchValue             = "";
                var caseNumValue            = "";
                var clientValue             = "";
                var collectDateValue        = "";
                var collectedByValue        = "";
                var caseStatusValue         = "";
                var intakeValue         = "";


                if (dataTablesParameters.columns[1].search.value != "") {
                  // if (dataTablesParameters.columns[1].search.value.length>=3 || dataTablesParameters.columns[1].search.value.length == 0) {
                    // columnToSearch = "searchbycasenumber"; searchValue = dataTablesParameters.columns[1].search.value;
                    // caseNumValue = dataTablesParameters.columns[1].search.value;
                    reqAllCases['data']['caseNumber'] = dataTablesParameters.columns[1].search.value;


                  // }
                  // else{
                  //   return;
                  // }
                }
                if (dataTablesParameters.columns[6].search.value != "") {columnToSearch = "searchbydate"; reqAllCases['data']['caseCollectionDate'] = dataTablesParameters.columns[6].search.value;}
                if (dataTablesParameters.columns[7].search.value != "") {columnToSearch = "searchcollectedby"; reqAllCases['data']['caseCollectedBy'] = dataTablesParameters.columns[7].search.value;}
                // if (dataTablesParameters.columns[8].search.value != "") {columnToSearch = "searchbycasesstatus"; caseStatusValue = dataTablesParameters.columns[8].search.value; searchValue = dataTablesParameters.columns[8].search.value}
                // console.log("---------",this.cNameforSearch);
                // if (this.cNameforSearch != null) {
                  if (dataTablesParameters.columns[5].search.value != "") {
                    // columnToSearch = "searchbyclientname"; clientValue = this.cNameforSearch;searchValue  = this.cNameforSearch
                    reqAllCases['data']['clientName'] = dataTablesParameters.columns[5].search.value;
                  }
                // }

                if (dataTablesParameters.columns[2].search.value != "") {
                  // if (dataTablesParameters.columns[2].search.value.length>=3 || dataTablesParameters.columns[2].search.value.length == 0) {
                    // columnToSearch = "searchbylastname"; searchValue = dataTablesParameters.columns[2].search.value
                    reqAllCases['data']['patientLastName'] = dataTablesParameters.columns[2].search.value;
                  // }
                  // else{
                  //   return;
                  // }
                }

                if (dataTablesParameters.columns[3].search.value != "") {
                  reqAllCases['data']['patientFirstName'] = dataTablesParameters.columns[3].search.value;
                }
                // if (dataTablesParameters.columns[2].search.value != "") {columnToSearch = "searchbylastname";  searchValue = dataTablesParameters.columns[2].search.value}
                if (dataTablesParameters.columns[4].search.value != "") {
                  reqAllCases['data']['patientDateOfBirth'] = dataTablesParameters.columns[4].search.value;

                }
                if (dataTablesParameters.columns[8].search.value != "") {
                  // columnToSearch = "searchbyinterpretation";searchValue = dataTablesParameters.columns[8].search.value
                  reqAllCases['data']['interpretationDiagnosis'] = dataTablesParameters.columns[8].search.value;
                }


                // if (dataTablesParameters.columns[4].search.value != "") {columnToSearch = "searchbyclientname";searchValue = dataTablesParameters.columns[7].search.value}
                // this.paginatePage2          = dtElement['dt'].page.info().page;
                // this.paginateLength         = dtElement['dt'].page.len();


              if (dataTablesParameters.search.value != "" && (this.oldCharacterCount == 0 || this.oldCharacterCount == 3)) {
                if (this.oldCharacterCount == 3) {
                }
                reqAllCases['data']['mainSearch'] = dataTablesParameters.search.value;

              }
              else{

                  $('.dataTables_processing').css('display',"none");
                  if (this.oldCharacterCount == 3) {
                    this.oldCharacterCount     = 3

                  }
                  else{
                    this.oldCharacterCount     = 2;
                  }

              }
                reqAllCases.data['pageNumber']           = dtElement['dt'].page.info().page;
                reqAllCases.data['pageSize']           = dtElement['dt'].page.len();
                // reqAllCases.data.statuses       = [1,2,3,4,5,7,8,9,10,11,12];
                reqAllCases.data['caseStatus']     = '6';
                reqAllCases.header.partnerCode  = this.logedInUserRoles.partnerCode;
                reqAllCases.header.userCode     = this.logedInUserRoles.userCode;
                reqAllCases.header.functionalityCode     = "MRMA-MR";
                reqAllCases.data['sortColumn']     = sortArray[sortColumn];
                reqAllCases.data['sortType']   = sortOrder;
                // reqAllCases.data.patientType   = 1;
                this.oldCharacterCount     = 3;
                // if (this.logedInUserRoles.userType == 2) {
                //   reqAllCases.data.userCode = this.logedInUserRoles.userCode;
                // }

                let url    = environment.API_ACDM_ENDPOINT + 'allreports';
                console.log('reqAllCases',reqAllCases);


                this.http.put(url,reqAllCases).toPromise()
                // this.http.post(url,reqAllCases).toPromise()
                .then( resp => {
                  console.log('resp delivered Reports:  ACDM', resp);
                  if (typeof resp['message'] != 'undefined') {
                    this.notifier.notify("error",resp['message']);
                    this.pendingReportCount = 0;
                    callback({
                      recordsTotal    :  0,
                      recordsFiltered :  0,
                      data: []
                    });
                    this.pendingReportReqNew.data = {};
                    this.pendingReportReqNew.data['clientId'] = this.clientSearchString.clientId;
                    this.pendingReportReqNew.data['intakeId'] = this.selectedIntakeforClient;

                    return;
                  }
                  if (resp['data'] == null) {
                    this.notifier.notify("error",resp['result']['description']);
                    this.pendingReportCount = 0;
                    callback({
                      recordsTotal    :  0,
                      recordsFiltered :  0,
                      data: []
                    });
                    this.pendingReportReqNew.data = {};
                    this.pendingReportReqNew.data['clientId'] = this.clientSearchString.clientId;
                    this.pendingReportReqNew.data['intakeId'] = this.selectedIntakeforClient;

                    return
                  }
                  if (resp['data']['reports'] == null) {
                    // this.notifier.notify("error",resp['result']['description']);
                    this.pendingReportCount = 0;
                    callback({
                      recordsTotal    :  0,
                      recordsFiltered :  0,
                      data: []
                    });
                    this.pendingReportReqNew.data = {};
                    this.pendingReportReqNew.data['clientId'] = this.clientSearchString.clientId;
                    this.pendingReportReqNew.data['intakeId'] = this.selectedIntakeforClient;

                    return
                  }
                  else{
                    this.pendingReportCount    = resp['data']['totalReports'];
                    this.pendingReport         = resp['data']['reports'];
                    // this.deliveredReportsCount = 0;
                    // this.notDeliveredReportsCount   = 0;
                    callback({
                      recordsTotal    :  resp['data']['totalReports'],
                      recordsFiltered :  resp['data']['totalReports'],
                      data: []
                    });
                    this.pendingReportReqNew.data = {};
                    this.pendingReportReqNew.data['clientId'] = this.clientSearchString.clientId;
                    this.pendingReportReqNew.data['intakeId'] = this.selectedIntakeforClient;
                  }
                  return;

                })
                .catch(error => {

                  console.log("error: ",error);
                  this.ngxLoader.stop();
                  this.notifier.notify( "error", "Error while loading cases");
                });



            }

            // console.log(`The DataTable ${index} instance ID is: ${dtInstance.table().node().id}`);
          });
        });

      }, 500)



    },
    // columns  : [
    //   { data : 'SPD-002' },
    //   { data : 'firstName' },
    //   { data : 'lastName' },
    //   { data : 'username' },
    //   { data : 'phone' },
    //   { data : 'email' },
    //   { data : 'action' }
    // ]
    ///for search
  };
  //////////// delivered reports
  this.dtOptions[1] = {
    pagingType   : 'full_numbers',
    pageLength   : this.paginateLength,
    serverSide   : true,
    processing   : true,
    // ordering     : false,
    "order": [[ 10, "desc" ]],
    columnDefs: [
      { orderable: true, className: 'reorder', targets: [1,2,3,4,5,6,7,8, 'desc'] },
      { orderable: false, targets: [0,9] },
      {
        targets: [10],
        visible: false,
        searchable: false,
        orderable: true,
      },
    ],
    "dom": "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-3'i><'col-sm-12 col-md-3'f><'col-sm-12 col-md-4'p>>"+
    "<'row'<'col-sm-12'tr>>" +
    "<'row'<'col-sm-12 col-md-5'l><'col-sm-12 col-md-3'i><'col-sm-12 col-md-4'p>>",
    "lengthMenu": [20, 50, 75, 100 ],
    ajax: (dataTablesParameters: any, callback) => {
      if(this.timeCount1) {
        clearTimeout(this.timeCount1);
        // console.log("here",this.timeCount);

        this.timeCount1 = null;
      }

      // this.timeCount = setTimeout(this.addCaseInternal(e), 5000)
      this.timeCount1 = setTimeout(()=>{
        if ((this.selectedIntakeforClient == null || this.selectedIntakeforClient == "") && (this.clientSearchString == null || this.clientSearchString =="")) {
          this.pendingReportCount    = 0;
          this.deliveredReportsCount = 0;
          this.notDeliveredReportsCount   = 0;
          callback({
            recordsTotal    :  0,
            recordsFiltered :  0,
            data: []
          });
          return;
        }
        this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {

          dtElement.dtInstance.then((dtInstance: any) => {
            if(dtInstance.table().node().id == 'delivered_reports'){
              var sortColumn = dataTablesParameters.order[0]['column'];
              var sortOrder  = dataTablesParameters.order[0]['dir'];
              var sortArray  = ["caseNumber","caseNumber",'patientLastName','patientFirstName','patientDateOfBirth','clientName','caseCollectionDate','caseCollectedBy','interpretationDiagnosis','createdTimestamp','createdTimestamp','createdTimestamp']
              var reqAllCases = {...this.pendingReportReqNew}
              var columnToSearch          = "";
              var searchValue             = "";
              var caseNumValue            = "";
              var clientValue             = "";
              var collectDateValue        = "";
              var collectedByValue        = "";
              var caseStatusValue         = "";
              var intakeValue         = "";


              if (dataTablesParameters.columns[1].search.value != "") {
                // if (dataTablesParameters.columns[1].search.value.length>=3 || dataTablesParameters.columns[1].search.value.length == 0) {
                  // columnToSearch = "searchbycasenumber"; searchValue = dataTablesParameters.columns[1].search.value;
                  // caseNumValue = dataTablesParameters.columns[1].search.value;
                  reqAllCases['data']['caseNumber'] = dataTablesParameters.columns[1].search.value;


                // }
                // else{
                //   return;
                // }
              }
              if (dataTablesParameters.columns[6].search.value != "") {columnToSearch = "searchbydate"; reqAllCases['data']['caseCollectionDate'] = dataTablesParameters.columns[6].search.value;}
              if (dataTablesParameters.columns[7].search.value != "") {columnToSearch = "searchcollectedby"; reqAllCases['data']['caseCollectedBy'] = dataTablesParameters.columns[7].search.value;}
              // if (dataTablesParameters.columns[8].search.value != "") {columnToSearch = "searchbycasesstatus"; caseStatusValue = dataTablesParameters.columns[8].search.value; searchValue = dataTablesParameters.columns[8].search.value}
              // console.log("---------",this.cNameforSearch);
              // if (this.cNameforSearch != null) {
                if (dataTablesParameters.columns[5].search.value != "") {
                  // columnToSearch = "searchbyclientname"; clientValue = this.cNameforSearch;searchValue  = this.cNameforSearch
                  reqAllCases['data']['clientName'] = dataTablesParameters.columns[5].search.value;
                }
              // }

              if (dataTablesParameters.columns[2].search.value != "") {
                // if (dataTablesParameters.columns[2].search.value.length>=3 || dataTablesParameters.columns[2].search.value.length == 0) {
                  // columnToSearch = "searchbylastname"; searchValue = dataTablesParameters.columns[2].search.value
                  reqAllCases['data']['patientLastName'] = dataTablesParameters.columns[2].search.value;
                // }
                // else{
                //   return;
                // }
              }

              if (dataTablesParameters.columns[3].search.value != "") {
                reqAllCases['data']['patientFirstName'] = dataTablesParameters.columns[3].search.value;
              }
              // if (dataTablesParameters.columns[2].search.value != "") {columnToSearch = "searchbylastname";  searchValue = dataTablesParameters.columns[2].search.value}
              if (dataTablesParameters.columns[4].search.value != "") {
                reqAllCases['data']['patientDateOfBirth'] = dataTablesParameters.columns[4].search.value;

              }
              if (dataTablesParameters.columns[8].search.value != "") {
                // columnToSearch = "searchbyinterpretation";searchValue = dataTablesParameters.columns[8].search.value
                reqAllCases['data']['interpretationDiagnosis'] = dataTablesParameters.columns[8].search.value;
              }


              // if (dataTablesParameters.columns[4].search.value != "") {columnToSearch = "searchbyclientname";searchValue = dataTablesParameters.columns[7].search.value}
              // this.paginatePage2          = dtElement['dt'].page.info().page;
              // this.paginateLength         = dtElement['dt'].page.len();


            if (dataTablesParameters.search.value != "" && (this.oldCharacterCount2 == 0 || this.oldCharacterCount2 == 3)) {
              if (this.oldCharacterCount2 == 3) {
              }
              reqAllCases['data']['mainSearch'] = dataTablesParameters.search.value;

            }
            else{

                $('.dataTables_processing').css('display',"none");
                if (this.oldCharacterCount2 == 3) {
                  this.oldCharacterCount2     = 3

                }
                else{
                  this.oldCharacterCount2     = 2;
                }

            }
              reqAllCases.data['pageNumber']           = dtElement['dt'].page.info().page;
              reqAllCases.data['pageSize']           = dtElement['dt'].page.len();
              // reqAllCases.data.statuses       = [1,2,3,4,5,7,8,9,10,11,12];
              reqAllCases.data['caseStatus']     = '10';
              reqAllCases.header.partnerCode  = this.logedInUserRoles.partnerCode;
              reqAllCases.header.userCode     = this.logedInUserRoles.userCode;
              reqAllCases.header.functionalityCode     = "MRMA-MR";
              reqAllCases.data['sortColumn']     = sortArray[sortColumn];
              reqAllCases.data['sortType']   = sortOrder;
              // reqAllCases.data.patientType   = 1;
              this.oldCharacterCount2     = 3;
              // if (this.logedInUserRoles.userType == 2) {
              //   reqAllCases.data.userCode = this.logedInUserRoles.userCode;
              // }

              let url    = environment.API_ACDM_ENDPOINT + 'allreports';
              console.log('reqAllCases',reqAllCases);


              this.http.put(url,reqAllCases).toPromise()
              // this.http.post(url,reqAllCases).toPromise()
              .then( resp => {
                console.log('resp delivered Reports:  ACDM', resp);
                if (typeof resp['message'] != 'undefined') {
                  this.notifier.notify("error",resp['message']);
                  this.deliveredReportsCount = 0;
                  callback({
                    recordsTotal    :  0,
                    recordsFiltered :  0,
                    data: []
                  });
                  this.pendingReportReqNew.data = {};
                  this.pendingReportReqNew.data['clientId'] = this.clientSearchString.clientId;
                  this.pendingReportReqNew.data['intakeId'] = this.selectedIntakeforClient;

                  return;
                }
                if (resp['data'] == null) {
                  this.notifier.notify("error",resp['result']['description']);
                  this.deliveredReportsCount = 0;
                  callback({
                    recordsTotal    :  0,
                    recordsFiltered :  0,
                    data: []
                  });
                  this.pendingReportReqNew.data = {};
                  this.pendingReportReqNew.data['clientId'] = this.clientSearchString.clientId;
                  this.pendingReportReqNew.data['intakeId'] = this.selectedIntakeforClient;

                  return
                }
                if (resp['data']['reports'] == null) {
                  // this.notifier.notify("error",resp['result']['description']);
                  this.deliveredReportsCount = 0;
                  callback({
                    recordsTotal    :  0,
                    recordsFiltered :  0,
                    data: []
                  });
                  this.pendingReportReqNew.data = {};
                  this.pendingReportReqNew.data['clientId'] = this.clientSearchString.clientId;
                  this.pendingReportReqNew.data['intakeId'] = this.selectedIntakeforClient;

                  return
                }
                else{
                  this.deliveredReportsCount    = resp['data']['totalReports'];
                  this.deliveredReports         = resp['data']['reports'];
                  // this.deliveredReportsCount = 0;
                  // this.notDeliveredReportsCount   = 0;
                  callback({
                    recordsTotal    :  resp['data']['totalReports'],
                    recordsFiltered :  resp['data']['totalReports'],
                    data: []
                  });
                  this.pendingReportReqNew.data = {};
                  this.pendingReportReqNew.data['clientId'] = this.clientSearchString.clientId;
                  this.pendingReportReqNew.data['intakeId'] = this.selectedIntakeforClient;
                }
                return;

              })
              .catch(error => {

                console.log("error: ",error);
                this.ngxLoader.stop();
                this.notifier.notify( "error", "Error while loading cases");
              });




          }

          // console.log(`The DataTable ${index} instance ID is: ${dtInstance.table().node().id}`);
        });
      });
    }, 500)




  },
  // columns  : [
  //   { data : 'SPD-002' },
  //   { data : 'firstName' },
  //   { data : 'lastName' },
  //   { data : 'username' },
  //   { data : 'phone' },
  //   { data : 'email' },
  //   { data : 'action' }
  // ]
  ///for search
};

//////////// Not Delivered reports
this.dtOptions[2] = {
  pagingType   : 'full_numbers',
  pageLength   : this.paginateLength,
  serverSide   : true,
  processing   : true,
  // ordering     : false,
  "order": [[ 10, "desc" ]],
  columnDefs: [
    { orderable: true, className: 'reorder', targets: [1,2,3,4,5,6,7,8, 'desc'] },
    { orderable: false, targets: [0,9] },
    {
      targets: [10],
      visible: false,
      searchable: false,
      orderable: true,
    },
  ],
  "dom": "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-3'i><'col-sm-12 col-md-3'f><'col-sm-12 col-md-4'p>>"+
  "<'row'<'col-sm-12'tr>>" +
  "<'row'<'col-sm-12 col-md-5'l><'col-sm-12 col-md-3'i><'col-sm-12 col-md-4'p>>",
  "lengthMenu": [20, 50, 75, 100 ],
  ajax: (dataTablesParameters: any, callback) => {

    if(this.timeCount2) {
      clearTimeout(this.timeCount2);
      // console.log("here",this.timeCount);

      this.timeCount2 = null;
    }

    // this.timeCount = setTimeout(this.addCaseInternal(e), 5000)
    this.timeCount2 = setTimeout(()=>{
      if ((this.selectedIntakeforClient == null || this.selectedIntakeforClient == "") && (this.clientSearchString == null || this.clientSearchString =="")) {
        this.pendingReportCount    = 0;
        this.deliveredReportsCount = 0;
        this.notDeliveredReportsCount   = 0;
        callback({
          recordsTotal    :  0,
          recordsFiltered :  0,
          data: []
        });
        return;
      }
      this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {

        dtElement.dtInstance.then((dtInstance: any) => {
          if(dtInstance.table().node().id == 'notdelivered_reports'){

            var sortColumn = dataTablesParameters.order[0]['column'];
            var sortOrder  = dataTablesParameters.order[0]['dir'];
            var sortArray  = ["caseNumber","caseNumber",'patientLastName','patientFirstName','patientDateOfBirth','clientName','caseCollectionDate','caseCollectedBy','interpretationDiagnosis','createdTimestamp','createdTimestamp','createdTimestamp']
            var reqAllCases = {...this.pendingReportReqNew}
            var columnToSearch          = "";
            var searchValue             = "";
            var caseNumValue            = "";
            var clientValue             = "";
            var collectDateValue        = "";
            var collectedByValue        = "";
            var caseStatusValue         = "";
            var intakeValue         = "";


            if (dataTablesParameters.columns[1].search.value != "") {
              // if (dataTablesParameters.columns[1].search.value.length>=3 || dataTablesParameters.columns[1].search.value.length == 0) {
                // columnToSearch = "searchbycasenumber"; searchValue = dataTablesParameters.columns[1].search.value;
                // caseNumValue = dataTablesParameters.columns[1].search.value;
                reqAllCases['data']['caseNumber'] = dataTablesParameters.columns[1].search.value;


              // }
              // else{
              //   return;
              // }
            }
            if (dataTablesParameters.columns[6].search.value != "") {columnToSearch = "searchbydate"; reqAllCases['data']['caseCollectionDate'] = dataTablesParameters.columns[6].search.value;}
            if (dataTablesParameters.columns[7].search.value != "") {columnToSearch = "searchcollectedby"; reqAllCases['data']['caseCollectedBy'] = dataTablesParameters.columns[7].search.value;}
            // if (dataTablesParameters.columns[8].search.value != "") {columnToSearch = "searchbycasesstatus"; caseStatusValue = dataTablesParameters.columns[8].search.value; searchValue = dataTablesParameters.columns[8].search.value}
            // console.log("---------",this.cNameforSearch);
            // if (this.cNameforSearch != null) {
              if (dataTablesParameters.columns[5].search.value != "") {
                // columnToSearch = "searchbyclientname"; clientValue = this.cNameforSearch;searchValue  = this.cNameforSearch
                reqAllCases['data']['clientName'] = dataTablesParameters.columns[5].search.value;
              }
            // }

            if (dataTablesParameters.columns[2].search.value != "") {
              // if (dataTablesParameters.columns[2].search.value.length>=3 || dataTablesParameters.columns[2].search.value.length == 0) {
                // columnToSearch = "searchbylastname"; searchValue = dataTablesParameters.columns[2].search.value
                reqAllCases['data']['patientLastName'] = dataTablesParameters.columns[2].search.value;
              // }
              // else{
              //   return;
              // }
            }

            if (dataTablesParameters.columns[3].search.value != "") {
              reqAllCases['data']['patientFirstName'] = dataTablesParameters.columns[3].search.value;
            }
            // if (dataTablesParameters.columns[2].search.value != "") {columnToSearch = "searchbylastname";  searchValue = dataTablesParameters.columns[2].search.value}
            if (dataTablesParameters.columns[4].search.value != "") {
              reqAllCases['data']['patientDateOfBirth'] = dataTablesParameters.columns[4].search.value;

            }
            if (dataTablesParameters.columns[8].search.value != "") {
              // columnToSearch = "searchbyinterpretation";searchValue = dataTablesParameters.columns[8].search.value
              reqAllCases['data']['interpretationDiagnosis'] = dataTablesParameters.columns[8].search.value;
            }


            // if (dataTablesParameters.columns[4].search.value != "") {columnToSearch = "searchbyclientname";searchValue = dataTablesParameters.columns[7].search.value}
            // this.paginatePage2          = dtElement['dt'].page.info().page;
            // this.paginateLength         = dtElement['dt'].page.len();


          if (dataTablesParameters.search.value != "" && (this.oldCharacterCount2 == 0 || this.oldCharacterCount3 == 3)) {
            if (this.oldCharacterCount3 == 3) {
            }
            reqAllCases['data']['mainSearch'] = dataTablesParameters.search.value;

          }
          else{

              $('.dataTables_processing').css('display',"none");
              if (this.oldCharacterCount3 == 3) {
                this.oldCharacterCount3     = 3

              }
              else{
                this.oldCharacterCount3     = 2;
              }

          }
            reqAllCases.data['pageNumber']           = dtElement['dt'].page.info().page;
            reqAllCases.data['pageSize']           = dtElement['dt'].page.len();
            // reqAllCases.data.statuses       = [1,2,3,4,5,7,8,9,10,11,12];
            reqAllCases.data['caseStatus']     = '12';
            reqAllCases.header.partnerCode  = this.logedInUserRoles.partnerCode;
            reqAllCases.header.userCode     = this.logedInUserRoles.userCode;
            reqAllCases.header.functionalityCode     = "MRMA-MR";
            reqAllCases.data['sortColumn']     = sortArray[sortColumn];
            reqAllCases.data['sortType']   = sortOrder;
            // reqAllCases.data.patientType   = 1;
            this.oldCharacterCount3     = 3;
            // if (this.logedInUserRoles.userType == 2) {
            //   reqAllCases.data.userCode = this.logedInUserRoles.userCode;
            // }

            let url    = environment.API_ACDM_ENDPOINT + 'allreports';
            console.log('reqAllCases not delivered',reqAllCases);


            this.http.put(url,reqAllCases).toPromise()
            // this.http.post(url,reqAllCases).toPromise()
            .then( resp => {
              console.log('resp delivered Reports:  ACDM delivered', resp);
              if (typeof resp['message'] != 'undefined') {
                this.notifier.notify("error",resp['message']);
                this.notDeliveredReportsCount   = 0;
                callback({
                  recordsTotal    :  0,
                  recordsFiltered :  0,
                  data: []
                });
                this.pendingReportReqNew.data = {};
                this.pendingReportReqNew.data['clientId'] = this.clientSearchString.clientId;
                this.pendingReportReqNew.data['intakeId'] = this.selectedIntakeforClient;

                return;
              }
              if (resp['data'] == null) {
                this.notifier.notify("error",resp['result']['description']);
                this.notDeliveredReportsCount   = 0;
                callback({
                  recordsTotal    :  0,
                  recordsFiltered :  0,
                  data: []
                });
                this.pendingReportReqNew.data = {};
                this.pendingReportReqNew.data['clientId'] = this.clientSearchString.clientId;
                this.pendingReportReqNew.data['intakeId'] = this.selectedIntakeforClient;

                return
              }
              if (resp['data']['reports'] == null) {
                // this.notifier.notify("error",resp['result']['description']);
                this.notDeliveredReportsCount = 0;
                callback({
                  recordsTotal    :  0,
                  recordsFiltered :  0,
                  data: []
                });
                this.pendingReportReqNew.data = {};
                this.pendingReportReqNew.data['clientId'] = this.clientSearchString.clientId;
                this.pendingReportReqNew.data['intakeId'] = this.selectedIntakeforClient;

                return
              }
              else{
                this.notDeliveredReportsCount    = resp['data']['totalReports'];
                this.notDeliveredReports         = resp['data']['reports'];
                // console.log('this.notDeliveredReports',this.notDeliveredReports);

                // this.deliveredReportsCount = 0;
                // this.notDeliveredReportsCount   = 0;
                callback({
                  recordsTotal    :  resp['data']['totalReports'],
                  recordsFiltered :  resp['data']['totalReports'],
                  data: []
                });
                this.pendingReportReqNew.data = {};
                this.pendingReportReqNew.data['clientId'] = this.clientSearchString.clientId;
                this.pendingReportReqNew.data['intakeId'] = this.selectedIntakeforClient;
              }
              return;

            })
            .catch(error => {

              console.log("error: ",error);
              this.ngxLoader.stop();
              this.notifier.notify( "error", "Error while loading cases");
            });




        }

        // console.log(`The DataTable ${index} instance ID is: ${dtInstance.table().node().id}`);
      });
    });

  }, 500)




},
// columns  : [
//   { data : 'SPD-002' },
//   { data : 'firstName' },
//   { data : 'lastName' },
//   { data : 'username' },
//   { data : 'phone' },
//   { data : 'email' },
//   { data : 'action' }
// ]
///for search
};

//////////// history reports
this.dtOptions[3] = {
  pagingType   : 'full_numbers',
  pageLength   : this.paginateLengthHistory,
  serverSide   : true,
  processing   : true,
  ordering     : false,
  searching   : false,
  "lengthMenu": [20, 50, 75, 100 ],
  ajax: (dataTablesParameters: any, callback) => {
    if (this.triggerHistoryTable == true) {


      this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {

        dtElement.dtInstance.then((dtInstance: any) => {
          if (dtInstance.table().node().id == 'rep-history' ) {
            this.paginatePaheHistory           = dtElement['dt'].page.info().page;
            this.paginateLengthHistory         = dtElement['dt'].page.len();
            // console.log('this.GLobalUniqueJoBOrderIdsForHistory',this.GLobalUniqueJoBOrderIdsForHistory);
            // return;
            var toSend;
            if (this.selectedIntakeforClient != "none") {
               toSend = this.selectedIntakeforClient;
            }
            else{
               toSend = this.GLobalUniqueJoBOrderIdsForHistory;
            }

            this.fetchFinalReport(toSend,'temp').then( jobResponse =>{
            // this.fetchFinalReport(this.GLobalUniqueJoBOrderIdsForHistory,'temp').then( jobResponse =>{
              callback({
                recordsTotal    :  this.allHistoryDataCount,
                recordsFiltered :  this.allHistoryDataCount,
                data: []
              });
            })
          }
        })
      })
    }
  }
};

}
ngAfterViewInit(): void {
  this.getLookups().then(lookupsLoaded => {
    this.dtTrigger.next();
    // console.log('this.dtElement',this.dtElement);


    this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
      if (typeof dtElement.dtInstance != "undefined") {
        dtElement.dtInstance.then((dtInstance: any) => {
          if (dtInstance.table().node().id == 'pending_reports') {
            dtInstance.columns().every(function () {
              const that = this;
              ///////// case number search
              $('#caseNoSearch', this.footer()).on('keyup', function () {
                if (this['value'].length >=3 || this['value'].length == 0) {
                  that
                  .search(this['value'])
                  .draw();
                }

                // }
              });
              ////////////// Collected By
              $('#collectedBySearch', this.footer()).on('change', function () {
                that
                .search(this['value'])
                .draw();
              });
              /////// collection date filter
              $('#collectionDateFilter', this.footer()).on('keyup change', function () {
                // console.log("this['value']",this['value']);

                var searchString = ''
                if (this['value'] != '') {
                  var darr = this['value'].split("-");

                  searchString = darr[1]+"-"+darr[2]+"-"+darr[0];
                }
                that
                .search(searchString)
                .draw();

              });

              /////// Select filter client name
              $('#searchClient', this.footer()).on('keyup', function () {
                console.log("SearchClient",this['value']);
                if (this['value'].length >=3 || this['value'].length == 0) {
                that
                .search(this['value'])
                .draw();
                }


              });

              /////// Select Status
              $('#searchStatus', this.footer()).on('change', function () {
                that
                .search(this['value'])
                .draw();

              });

              /////// Patient First Name
              $('#pFnameSearch', this.footer()).on('keyup', function () {
                if (this['value'].length>=3 || this['value'].length==0) {
                  that
                  .search(this['value'])
                  .draw();
                }

              });

              /////// Patient Last Name
              $('#pLnameSearch', this.footer()).on('keyup', function () {
                // console.log("------");

                if (this['value'].length>=3 || this['value'].length==0) {
                  that
                  .search(this['value'])
                  .draw();
                }


              });

              /////// Patient DOB
              $('#dobFilter', this.footer()).on('change', function () {
                console.log("this['value']",this['value']);
                var ar = this['value'].split('-');
                console.log("ar",ar);
                var newd = "";
                if (ar[0] == "") {

                }
                else{
                  newd = ar[1]+"-"+ar[2]+"-"+ar[0];
                  // newd = ar[0]+"-"+ar[2]+"-"+ar[1];
                }
                that
                .search(newd)
                .draw();

              });

              /////// Select Intrpretations
              $('#interpretationSearch', this.footer()).on('change', function () {
                this.interIDforSearch = this['value'];
                that
                .search(this['value'])
                .draw();

              });


            });

          }
          else if (dtInstance.table().node().id == 'delivered_reports') {
            dtInstance.columns().every(function () {
              const that = this;
              ///////// case number search
              $('#caseNoSearch2', this.footer()).on('keyup', function () {
                if (this['value'].length >=3 || this['value'].length == 0) {
                  that
                  .search(this['value'])
                  .draw();
                }

                // }
              });
              ////////////// Collected By
              $('#collectedBySearch2', this.footer()).on('change', function () {
                that
                .search(this['value'])
                .draw();
              });
              /////// collection date filter
              $('#collectionDateFilter2', this.footer()).on('change', function () {
                var searchString = ''
                if (this['value'] != '') {
                  var darr = this['value'].split("-");

                  searchString = darr[1]+"-"+darr[2]+"-"+darr[0];
                }
                that
                .search(searchString)
                // .search(this['value'])
                .draw();

              });

              /////// Select filter client name
              $('#searchClient2', this.footer()).on('keyup', function () {
                // that
                // .search(this['value'])
                // .draw();
                if (this['value'].length >=3 || this['value'].length == 0) {
                  that
                  .search(this['value'])
                  .draw();
                }


              });

              /////// Select Status
              $('#searchStatus2', this.footer()).on('keyup', function () {
                that
                .search(this['value'])
                .draw();

              });

              /////// Patient First Name
              $('#pFnameSearch2', this.footer()).on('keyup', function () {
                if (this['value'].length>=3 || this['value'].length==0) {
                  that
                  .search(this['value'])
                  .draw();
                }

              });

              /////// Patient Last Name
              $('#pLnameSearch2', this.footer()).on('keyup', function () {
                if (this['value'].length>=3 || this['value'].length==0) {
                  that
                  .search(this['value'])
                  .draw();
                }

              });

              /////// Patient DOB
              $('#dobFilter2', this.footer()).on('keyup change', function () {
                // console.log("this['value']",this['value']);
                var ar = this['value'].split('-');
                var newd = "";
                if (ar[0] == "") {

                }
                else{
                  newd = ar[1]+"-"+ar[2]+"-"+ar[0];
                }

                that
                .search(newd)
                .draw();

              });
              /////// Select Intrpretations
              $('#interpretationSearch2', this.footer()).on('change', function () {
                this.interIDforSearch2 = this['value'];
                that
                .search(this['value'])
                .draw();

              });


            });

          }
          else if (dtInstance.table().node().id == 'notdelivered_reports') {
            dtInstance.columns().every(function () {
              const that = this;
              ///////// case number search
              $('#caseNoSearch3', this.footer()).on('keyup', function () {
                if (this['value'].length >=3 || this['value'].length == 0) {
                  that
                  .search(this['value'])
                  .draw();
                }

                // }
              });
              ////////////// Collected By
              $('#collectedBySearch3', this.footer()).on('change', function () {
                that
                .search(this['value'])
                .draw();
              });
              /////// collection date filter
              $('#collectionDateFilter3', this.footer()).on('change', function () {
                var searchString = ''
                if (this['value'] != '') {
                  var darr = this['value'].split("-");

                  searchString = darr[1]+"-"+darr[2]+"-"+darr[0];
                }
                that
                .search(this['value'])
                .draw();

              });

              /////// Select filter client name
              $('#searchClient3', this.footer()).on('keyup', function () {
                // that
                // .search(this['value'])
                // .draw();
                if (this['value'].length >=3 || this['value'].length == 0) {
                  that
                  .search(this['value'])
                  .draw();
                }


              });

              /////// Select Status
              $('#searchStatus3', this.footer()).on('keyup', function () {
                that
                .search(this['value'])
                .draw();

              });

              /////// Patient First Name
              $('#pFnameSearch3', this.footer()).on('keyup', function () {
                if (this['value'].length>=3 || this['value'].length==0) {
                  that
                  .search(this['value'])
                  .draw();
                }

              });

              /////// Patient Last Name
              $('#pLnameSearch3', this.footer()).on('keyup', function () {
                if (this['value'].length>=3 || this['value'].length==0) {
                  that
                  .search(this['value'])
                  .draw();
                }

              });

              /////// Patient DOB
              $('#dobFilter3', this.footer()).on('keyup change', function () {
                // console.log("this['value']",this['value']);
                var ar = this['value'].split('-');
                var newd = "";
                if (ar[0] == "") {

                }
                else{
                  newd = ar[1]+"-"+ar[2]+"-"+ar[0];
                }

                that
                .search(newd)
                .draw();

              });
              /////// Select Intrpretations
              $('#interpretationSearch3', this.footer()).on('change', function () {
                this.interIDforSearch3 = this['value'];
                that
                .search(this['value'])
                .draw();

              });


            });

          }
        });
      }

    });

    this.ngxLoader.stop();
  });



  // this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
  //   dtInstance.columns().every(function () {
  //     const that = this;
  //     $('input', this.footer()).on('keyup', function () {
  //       if (that.search() !== this['value']) {
  //         that
  //         .search(this['value'])
  //         .draw();
  //       }
  //     });
  //     /////// collection date filter
  //     $('#collectionDateFilter', this.footer()).on('keyup', function () {
  //       // console.log("that.search()",that.search());
  //       var darr = that.search().split("-");
  //       var searchString = darr[1]+"/"+darr[2]+"/"+darr[0];
  //       that
  //       .search(searchString)
  //       .draw();
  //
  //     });
  //
  //     /////// Select filter
  //     $('select', this.footer()).on('keyup', function () {
  //       // console.log("this['value']",this['value']);
  //
  //       // var darr = that.search().split("-");
  //       // var searchString = darr[1]+"/"+darr[2]+"/"+darr[0];
  //       that
  //       .search(this['value'])
  //       .draw();
  //
  //     });
  //
  //
  //   });
  // });
}
getindividualrColumnSearch (url, data,type) {
  // console.log("data",data);
  // console.log("url",url);
  return new Promise((resolve, reject) => {
    // console.log('f1');
    this.http.post(url,data).toPromise()
    .then( resp => {
      console.log('resp Active Cases Search: ', resp);
      if (resp['data']['caseDetails'].length == 0) {
        if (type == 'delivered') {
          this.deliveredReports       = resp['data']['caseDetails'];
          this.deliveredReportsCount  = resp['data']['totalCount'];
        }
        else if (type == 'pending') {
          this.pendingReport       = resp['data']['caseDetails'];
          this.pendingReportCount  = resp['data']['totalCount'];
        }

        resolve();
      }
      else{
        var a = resp['data']['caseDetails'];
        const uniqueIds = [];
        const uniqueCLIds = [];
        const map = new Map();
        for (const item of a) {
          if(!map.has(item.clientId)){
            map.set(item.clientId, true);    // set any value to Map
            uniqueCLIds.push(item.clientId);
          }
        }
        const map1 = new Map();
        for (const item of a) {
          if(!map1.has(item.patientId)){
            map1.set(item.patientId, true);    // set any value to Map
            uniqueIds.push(item.patientId);
          }
        }
        console.log("uniqueIds",uniqueIds);
        console.log("uniqueCLIds",uniqueCLIds);

        // getPatReq.data.patientIds = uniqueIds;
        this.getPatClData(uniqueIds,resp,uniqueCLIds,type).then(respAlldata => {

          // callback({
          //   recordsTotal    :  this.deliveredReportsCount,
          //   recordsFiltered :  this.deliveredReportsCount,
          //   data: []
          // });
          resolve();

        });
      }
      // return resp;
      // this.deliveredReports                 = resp['data']['caseDetails'];
      // this.deliveredReportsCount            = resp['data']['totalCount'];
      // return true;

    })
    .catch(error => {
      // return false;
      // console.log("error: ",error);
      this.ngxLoader.stop();
      this.notifier.notify( "error", "Error while loading search results DB connection")
      reject();
    });
    // resolve();
  });



}

getPatClData(userids,resp,clientids,type) {
  // this.ngxLoader.start();
  var uniqueCaseIds = []
  // console.log("resp--------",resp);
  if (typeof resp['data']['caseDetails'] !='undefined') {
    var check = resp['data']['caseDetails'];


    const map3 = new Map();
    for (const item of check) {
      if(!map3.has(item.caseId)){
        map3.set(item.caseId, true);    // set any value to Map
        uniqueCaseIds.push(item.caseId);
      }
    }
  }
  else{
    var check = resp['data'];

    const map3 = new Map();
    for (const item of check) {
      if(!map3.has(item.caseId)){
        map3.set(item.caseId, true);    // set any value to Map
        uniqueCaseIds.push(item.caseId);
      }
    }
  }


  const patienByidURL = environment.API_PATIENT_ENDPOINT + 'patientbyids';
  var getPatReq       = {...this.patientByIdsReq}
  var clURL           = environment.API_CLIENT_ENDPOINT + "clientidnameemail"
  var clReqData = {
    header:{
      uuid               :"",
      partnerCode        :"",
      userCode           :"",
      referenceNumber    :"",
      systemCode         :"",
      moduleCode         :"CLIM",
      functionalityCode: "MRMA-MR",
      systemHostAddress  :"",
      remoteUserAddress  :"",
      dateTime           :""
    },
    data:{
      clientIds:clientids,
      userCodes: []
    }
  }

  // var clReqData       = {clientIds:clientids}
  getPatReq.data.patientIds = userids;

  const interpretaionURL = environment.API_COVID_ENDPOINT + 'showresult';
  var getInterpretaionreq       = {...this.showInterpretationReq}
  getInterpretaionreq.data.caseIds = uniqueCaseIds;
  // getInterpretaionreq.header.userCode = this.logedInUserRoles.userCode;
  return new Promise((resolve, reject) => {
    if (resp['data'].length == 0) {
      if (type == 'delivered') {
        // this.deliveredReports = [];
        this.deliveredReportsCount = 0;
        // this.deliveredReports = finalData;
        this.ngxLoader.stop();
        resolve()
      }
      else if(type == 'pending'){
        // this.pendingReport = [];
        this.pendingReportCount = 0;
        // this.pendingReport = finalData;
        this.ngxLoader.stop();
        resolve()
      }
      else if(type == 'notdelivered'){
        this.notDeliveredReportsCount = 0;
        // this.pendingReport = finalData;
        this.ngxLoader.stop();
        resolve()
      }
    }
    var patientout = this.globalService.getPatientsByIds(patienByidURL, getPatReq).then(patient=>{
      return patient;
    })
    var clientsout = this.globalService.getClientByName(clURL,clReqData).then(selectedClient => {
      // console.log('selectedClient broken',selectedClient);

      return selectedClient['data'];
    })
    var interpretationout = this.globalService.getInterpretaion(interpretaionURL, getInterpretaionreq).then(interpretation=>{
      return interpretation;
    });

    forkJoin([patientout, clientsout, interpretationout]).subscribe(bothresults => {
      console.log('bothresults FOrk Join------',bothresults);
      var patientresp = bothresults[0];
      var clients      = bothresults[1];
      var showresult = bothresults[2];
      if (typeof resp['data']['caseDetails'] != "undefined") {
        for (let c = 0; c < resp['data']['caseDetails'].length; c++) {
          resp['data']['caseDetails'][c].checkBox = false;

        }
      }
      else{
        for (let c = 0; c < resp['data'].length; c++) {
          resp['data'][c].checkBox = false;

        }

      }

      // console.log("resp['data'/]---------",showresult);

      if (patientresp['result'].codeType == "S") {
        // console.log("---resp['data']",resp['data']);
        // console.log("---patientresp['data']",patientresp['data']);

        if (typeof resp['data']['caseDetails'] != "undefined") {
          for (let i = 0; i < resp['data']['caseDetails'].length; i++) {
            for (let j = 0; j < patientresp['data'].length; j++) {
              // console.log("loop main data",resp['data']['caseDetails'][i]);
              //   console.log("loop patient data",patientresp['data'][j]);
              if (resp['data']['caseDetails'][i].patientId == patientresp['data'][j].patientId) {
                resp['data']['caseDetails'][i].pFName = patientresp['data'][j].firstName;
                resp['data']['caseDetails'][i].pLName = patientresp['data'][j].lastName;
                resp['data']['caseDetails'][i].pDob   = patientresp['data'][j].dateOfBirth;
                resp['data']['caseDetails'][i].patientRevisionId   = patientresp['data'][j].patientRevisionId;
                resp['data']['caseDetails'][i].pPhone   = patientresp['data'][j].phone;
                resp['data']['caseDetails'][i].pGender   = patientresp['data'][j].gender;
                resp['data']['caseDetails'][i].pssn   = patientresp['data'][j].ssn;
              }
              else{
                // console.log("Elseeeeee",resp['data']['caseDetails'][i]);
              }

            }

          }
          // this.pendingReport       = resp['data']['caseDetails'];
          // this.pendingReportCount  = resp['data']['totalCount'];
        }
        else{

          for (let i = 0; i < resp['data'].length; i++) {
            for (let j = 0; j < patientresp['data'].length; j++) {
              if (resp['data'][i].patientId == patientresp['data'][j].patientId) {
                resp['data'][i].pFName = patientresp['data'][j].firstName;
                resp['data'][i].pLName = patientresp['data'][j].lastName;
                resp['data'][i].pDob   = patientresp['data'][j].dateOfBirth;
                resp['data'][i].patientRevisionId   = patientresp['data'][j].patientRevisionId;
                resp['data'][i].pPhone   = patientresp['data'][j].phone;
                resp['data'][i].pGender   = patientresp['data'][j].gender;
                resp['data'][i].pssn   = patientresp['data'][j].ssn;
              }

            }

          }
        }
      }
      else{
        this.ngxLoader.stop();
        this.notifier.notify( "error", "Error while fetching patients" );
        // return;
      }

      if (clients['length'] > 0) {
        if (typeof resp['data']['caseDetails'] != "undefined") {
          // for (let i = 0; i < resp['data']['caseDetails'].length; i++) {
          for (let i = 0; i < resp['data']['caseDetails'].length; i++) {
            for (let j = 0; j < clients['length']; j++) {
              // console.log("loop main Data Client Id",resp['data']['caseDetails'][i].clientId);
              //   console.log("loop patientData Client ID",patientresp['data'][j].clientId);
              if (resp['data']['caseDetails'][i].clientId   == clients[j].clientId) {
                resp['data']['caseDetails'][i].clientName   = clients[j].name;
                resp['data']['caseDetails'][i].clientEmail   = clients[j].email;
                // if (clients[j]['clientContactsByClientId'].length > 0) {
                //   for (let cl = 0; cl < clients[j]['clientContactsByClientId'].length; cl++) {
                //     if (clients[j]['clientContactsByClientId'][cl]['contactByContactId'].length >0) {
                //       if (clients[j]['clientContactsByClientId'][cl]['contactByContactId'][0]['contactTypeId'] == 2) {
                //         resp['data']['caseDetails'][i].clientEmail = clients[j]['clientContactsByClientId'][cl]['contactByContactId'][0]['value']
                //       }
                //     }
                //   }
                // }

              }

            }

          }
        }
        else{
          // for (let i = 0; i < resp['data']['caseDetails'].length; i++) {
          for (let i = 0; i < resp['data'].length; i++) {
            for (let j = 0; j < clients['length']; j++) {
              if (resp['data'][i].clientId   == clients[j].clientId) {
                resp['data'][i].clientName   = clients[j].name;
                if (clients[j]['clientContactsByClientId'].length > 0) {
                  for (let cl = 0; cl < clients[j]['clientContactsByClientId'].length; cl++) {
                    if (clients[j]['clientContactsByClientId'][cl]['contactByContactId'].length >0) {
                      if (clients[j]['clientContactsByClientId'][cl]['contactByContactId'][0]['contactTypeId'] == 2) {
                        resp['data'][i].clientEmail = clients[j]['clientContactsByClientId'][cl]['contactByContactId'][0]['value']
                      }
                    }
                  }
                }
              }

            }

          }
        }

      }
      else{
        // this.notifier.notify( "error", "No Client Found" );
        // return;
      }
      if (showresult['data']!= null) {
        if (typeof resp['data']['caseDetails'] != "undefined") {
          // for (let i = 0; i < resp['data']['caseDetails'].length; i++) {
          for (let i = 0; i < resp['data']['caseDetails'].length; i++) {
            // resp['data']['caseDetails'][i].checkBox = false;
            for (let j = 0; j < showresult['data']['length']; j++) {
              // console.log("showresult['data'][j].caseId",showresult['data'][j].caseId);
              //   console.log("resp['data']['caseDetails'][i].caseId",resp['data']['caseDetails'][i].caseId);
              if (resp['data']['caseDetails'][i].caseId   == showresult['data'][j].caseId) {

                // console.log("--------------------------",showresult['data'][j].caseId);

                resp['data']['caseDetails'][i].resultId     = showresult['data'][j].interpretationId;

                resp['data']['caseDetails'][i].jobOrderId   = showresult['data'][j].jobOrderId;
                //

                for (let l = 0; l < this.allInterpretation['length']; l++) {
                  console.log("showresult['data'][j].interpretationId",showresult['data'][j].interpretationId);
                  if (this.allInterpretation[l].testInterpretationId == showresult['data'][j].interpretationId) {
                    console.log("--------------------------",showresult['data'][j].interpretationId);
                    resp['data']['caseDetails'][i].resultName   = this.allInterpretation[l].value;
                    resp['data']['caseDetails'][i].resultComment   = this.allInterpretation[l].comment;
                  }

                }
              }

            }
            // console.log("this.allInterpretation",this.allInterpretation);

            // for (let l = 0; l < this.allInterpretation['length']; l++) {
            //   console.log("resp['data']['caseDetails'][i].caseId",resp['data']['caseDetails'][i].resultId);
            //   if (this.allInterpretation[l].testInterpretationId == resp['data']['caseDetails'][i].resultId) {
            //     resp['data']['caseDetails'][i].resultName   = this.allInterpretation[l].value;
            //   }
            //
            // }

          }
        }
        else{
          // for (let i = 0; i < resp['data']['caseDetails'].length; i++) {
          for (let i = 0; i < resp['data'].length; i++) {
            for (let j = 0; j < showresult['data']['length']; j++) {
              if (resp['data'][i].caseId   == showresult['data'][j].caseId) {
                // console.log("resp['data'][i]",resp['data'][i]);
                // console.log("showresult['data'][j]",showresult['data'][j]);

                resp['data'][i].resultId     = showresult['data'][j].interpretationId;
                resp['data'][i].jobOrderId   = showresult['data'][j].jobOrderId;
                for (let l = 0; l < this.allInterpretation['length']; l++) {
                  if (this.allInterpretation[l].testInterpretationId == showresult['data'][j].interpretationId) {
                    resp['data'][i].resultName   = this.allInterpretation[l].value;
                    resp['data'][i].resultComment   = this.allInterpretation[l].comment;
                  }

                }
              }

            }


          }
        }

      }
      else{
        // this.notifier.notify( "error", "No Client Found" );
        // return;
      }
      console.log("type",type);


      if (type == 'delivered') {
        console.log("resp['data']",resp['data']);

        if (typeof resp['data']['caseDetails'] != "undefined") {
          this.deliveredReports       = resp['data']['caseDetails'];
          this.deliveredReportsCount  = resp['data']['totalCount'];
        }
        else{
          this.deliveredReports       = resp['data'];
          this.deliveredReportsCount  = resp['data']['length'];
        }
      }
      else if(type == 'pending'){

        if (typeof resp['data']['caseDetails'] != "undefined") {
          this.pendingReport       = resp['data']['caseDetails'];
          this.pendingReportCount  = resp['data']['totalCount'];
        }
        else{
          this.pendingReport       = resp['data'];
          this.pendingReportCount  = resp['data']['length'];
        }
        console.log("pendingReport",this.pendingReport);
      }
      else if(type == 'notdelivered'){
        // console.log("resp['data']---------------",resp['data']);
        if (typeof resp['data']['caseDetails'] != "undefined") {
          this.notDeliveredReports       = resp['data']['caseDetails'];
          this.notDeliveredReportsCount  = resp['data']['totalCount'];
        }
        else{
          this.notDeliveredReports       = resp['data'];
          this.notDeliveredReportsCount  = resp['data']['length'];
        }
      }



      resolve();
    });
    // resolve();
  });
}
getindividualrColumnSearchpatient (url, data,type) {
  // console.log("data",data);
  // console.log("url",url);
  return new Promise((resolve, reject) => {
    // console.log('f1');
    this.http.put(url,data).toPromise()
    .then( resultsColumn => {
      console.log('resp Active Cases Patiend Search:', resultsColumn);
      if (resultsColumn['data']['patients'] != null) {
        const uniqueIds = [];
        var b = resultsColumn['data']['patients'];
        const map1 = new Map();
        for (const item of b) {
          if(!map1.has(item.patientId)){
            map1.set(item.patientId, true);    // set any value to Map
            uniqueIds.push(item.patientId);
          }
        }
        var status = []
        if (type == 'pending') {
          status = [6]
        }
        else if (type == 'delivered') {
          status = [10]
        }
        else if (type == 'notdelivered') {
          status = [12]
        }
        // console.log("inoquePateintId",uniqueIds);
        // var byPidURL = environment.API_SPECIMEN_ENDPOINT + 'findbypatientid';
        // var reqPid={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "MRMA-MR",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{patientIds:uniqueIds}}
        var byPidURL = environment.API_SPECIMEN_ENDPOINT + 'findcasesbypatientid';
        var reqPid={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "MRMA-MR",systemHostAddress:"",remoteUserAddress:"",dateTime:""},
        data:{
          page:"0",
          size:"20",
          sortColumn:"createdTimestamp",
          sortingOrder:"desc",
          patientIds:uniqueIds,
          intakeId:this.selectedIntakeforClient,
          client:this.clientSearchString.clientId,
          statuses: status,
          userCode:""
        }}
        if (this.logedInUserRoles.userType == 2) {
          reqPid.data.userCode = this.logedInUserRoles.userCode;
        }
        this.globalService.findByPatientIds(byPidURL, reqPid).subscribe(respPID=>{
          console.log("by Patien ID Active Cases",respPID);
          const uniqueCLIds = []
          const map = new Map();
          for (let i = 0; i < respPID['data']['caseDetails'].length; i++) {
            for (let j = 0; j < resultsColumn['data']['patients'].length; j++) {
              if(respPID['data']['caseDetails'][i].patientId == resultsColumn['data']['patients'].patientId){

              }
              for (const item of respPID['data']['caseDetails']) {
                if(!map.has(item.clientId)){
                  map.set(item.clientId, true);    // set any value to Map
                  uniqueCLIds.push(item.clientId);
                }
              }

            }

          }
          console.log("uniqueCLIds",uniqueCLIds);
          // getPatReq.data.patientIds = uniqueIds;
          this.getPatClData(uniqueIds,respPID,uniqueCLIds,type).then(respAlldata => {
            console.log("**pendingReport**",this.pendingReport);
            resolve();

          });


        },
        error=>{
          this.ngxLoader.stop();
          this.notifier.notify( "error", "Error while loading Patient Connection Check");

        })

      }
      else{
        if (type == 'delivered') {
          this.deliveredReports      = resultsColumn['data']['patients'];
          this.deliveredReportsCount = 0
        }
        else if(type == 'pending'){
          this.pendingReport      = resultsColumn['data']['patients'];
          this.pendingReportCount = 0
        }
        else if(type == 'notdelivered'){
          this.notDeliveredReports       = 0;
          this.notDeliveredReportsCount  = 0;

        }

        resolve();
        // callback({
        //     recordsTotal    :  this.deliveredReportsCount,
        //     recordsFiltered :  this.deliveredReportsCount,
        //     data: []
        //   });
      }


    })
    .catch(error => {
      // return false;
      // console.log("error: ",error);
      this.ngxLoader.stop();
      this.notifier.notify( "error", "Error while loading search results DB connection")
      reject();
    });
    // resolve();
  });
}

getindividualrColumnSearchCovid (url, data,type) {
  // console.log("data",data);
  // console.log("url",url);
  return new Promise((resolve, reject) => {
    // console.log('f1');
    this.globalService.getInterpretaionForSearch(url,data).subscribe( respCovid => {
      console.log('resp Covid Search:', respCovid);
      var uniqueSpecIds= [];
      const map1 = new Map();
      for (const item of respCovid['data']['viewBatchDto']) {
        if(!map1.has(item.caseId)){
          map1.set(item.caseId, true);    // set any value to Map
          uniqueSpecIds.push(item.caseId);
        }
      }
      // return;
      var getFromspecURL = environment.API_SPECIMEN_ENDPOINT + 'findbycaseidin';
      var getFromspecReq = {
        header:{
          uuid:"",
          partnerCode:"",
          userCode:"",
          referenceNumber:"",
          systemCode:"",
          moduleCode:"SPCM",
          functionalityCode: "MRMA-MR",
          systemHostAddress:"",
          remoteUserAddress:"",
          dateTime:""
        },
        data:{
          caseIds:uniqueSpecIds,
          statuses:[6]//empty or fill
          // statuses:[1,2,3,4,5,6,7,8,9,10,11,12]//empty or fill


        }
      }
      getFromspecReq.header.partnerCode  = this.logedInUserRoles.partnerCode;
      getFromspecReq.header.userCode     = this.logedInUserRoles.userCode;
      getFromspecReq.header.functionalityCode     = "MRMA-MR";
      if (type == 'delivered') {
        getFromspecReq.data.statuses = [10]
      }
      else if(type == 'pending'){
        getFromspecReq.data.statuses = [6]
      }
      else if(type == 'notdelivered'){
        getFromspecReq.data.statuses = [12]
      }

      this.globalService.getCasesByids(getFromspecURL,getFromspecReq).subscribe( resp => {
        console.log('getFromspecResp',resp);
        // return;
        if (resp['data'].length == 0) {
          if (type == 'delivered') {
            this.deliveredReports       = resp['data'];
            this.deliveredReportsCount  = resp['data']['length'];
          }
          else if (type == 'pending') {
            this.pendingReport       = resp['data'];
            this.pendingReportCount  = resp['data']['length'];
          }
          else if(type == 'notdelivered'){
            this.notDeliveredReports       = resp['data'];
            this.notDeliveredReportsCount  = resp['data']['length'];

          }

          resolve();
        }
        else{
          var a = resp['data'];
          const uniqueIds = [];
          const uniqueCLIds = [];
          const map = new Map();
          for (const item of a) {
            if(!map.has(item.clientId)){
              map.set(item.clientId, true);    // set any value to Map
              uniqueCLIds.push(item.clientId);
            }
          }
          const map1 = new Map();
          for (const item of a) {
            if(!map1.has(item.patientId)){
              map1.set(item.patientId, true);    // set any value to Map
              uniqueIds.push(item.patientId);
            }
          }
          console.log("uniqueIds",uniqueIds);
          console.log("uniqueCLIds",uniqueCLIds);

          // getPatReq.data.patientIds = uniqueIds;
          this.getPatClData(uniqueIds,resp,uniqueCLIds,type).then(respAlldata => {

            // callback({
            //   recordsTotal    :  this.deliveredReportsCount,
            //   recordsFiltered :  this.deliveredReportsCount,
            //   data: []
            // });
            resolve();

          });
        }
        // return resp;
        // this.deliveredReports                 = resp['data']['caseDetails'];
        // this.deliveredReportsCount            = resp['data']['totalCount'];
        // return true;



      },erorr=>{
        this.ngxLoader.stop();
        this.notifier.notify( "error", "Error while loading search results DB connection")
        reject();
      })

      // return;



    },error => {
      // return false;
      // console.log("error: ",error);
      this.ngxLoader.stop();
      this.notifier.notify( "error", "Error while loading search results DB connection")
      reject();
    });
    // resolve();
  });



}


popSearch(){
  this.searchtextHolder = this.cNameforSearch;
  $("#searchClient").trigger('change');
}
popSearch2(){
  this.searchtextHolder2 = this.cNameforSearch2;
  $("#searchClient2").trigger('change');
}
popSearch3(){
  console.log('here----');

  this.searchtextHolder3 = this.cNameforSearch3;
  $("#searchClient3").trigger('change');
}
fromDashboard(searchType, clientId) {
  return new Promise((resolve, reject) => {
    // console.log('f1');
    // console.log("searchType999999999999999",searchType);

    if (searchType =="caseDraft") {
      $('#searchStatus').val(1);
      $("#searchStatus").trigger('change');
    }
    else if(searchType =="caseAccess"){
      $('#searchStatus').val(2);
      $("#searchStatus").trigger('change');
    }
    else if(searchType =="totalDraft"){
      $('#searchStatus').val(1);
      $("#searchStatus").trigger('change');
    }
    else if(searchType =="totalAccess"){
      $('#searchStatus').val(2);
      $("#searchStatus").trigger('change');
    }
    resolve();
  });
}
megaSearch(clientSearchUrl,patienttSearchUrl,specimenSearchUrl,clientReq,patitentReq,casesReq,type,searchString) {
  this.ngxLoader.start();
  return new Promise((resolve, reject) => {
    var clientResult = this.http.post(clientSearchUrl,clientReq).toPromise().then(clientresp=>{
      return clientresp['data'];
    })
    var patientResult = this.http.put(patienttSearchUrl,patitentReq).toPromise().then(patientresp=>{
      return patientresp;
    })
    var casesResult = this.http.post(specimenSearchUrl,casesReq).toPromise().then(casesresp=>{
      return casesresp;
    })
    forkJoin([clientResult, patientResult, casesResult]).subscribe(allresults => {
      var client  = allresults[0];
      var patient = allresults[1];
      var casses  = allresults[2];

      console.log("client",client);
      console.log("patient",patient);
      console.log("casses",casses);

      //////////////////////////////////////////
      /////////// Scenario 1 all 0/////////////
      ///////////////////////////////////////
      if (client['length'] == 0 && patient['data'] == null && casses['data']['caseDetails']['length'] == 0) {
        console.log('ALL Zero');
        if (type == 'delivered') {
          this.deliveredReportsCount = casses['data']['caseDetails']['length'];
          this.deliveredReports = casses['data']['caseDetails'];
          // resolve()
        }
        else if(type == 'pending'){
          this.pendingReportCount = casses['data']['caseDetails']['length'];
          this.pendingReport = casses['data']['caseDetails'];
          // this.pendingReport.push(byClientIdsResp['data']['caseDetails'][index]);
        }
        else if(type == 'notdelivered'){
          this.notDeliveredReports       = casses['data']['caseDetails'];;
          this.notDeliveredReportsCount  = casses['data']['caseDetails'];['length'];

        }

        this.ngxLoader.stop();
        resolve();
      }
      //////////////////////////////////////////
      /////////// Scenario 2 Casess 0/////////////
      ///////////////////////////////////////
      else if(client['length'] > 0 || patient['data'] != null || casses['data']['caseDetails']['length'] != 0){
        var patientToSend = {data:{patients:{}}}
        patientToSend.data.patients = patient['data'];
        this.getMegaSearchPatientClientRecord(casses,client,patientToSend,type,searchString).then(megapatclientresp=>{
          // this.getMegaSearchPatientClientRecord(casses,client,patient,type,searchString).then(megapatclientresp=>{
          resolve();
        })
      }

      //////////////////////////////////////////
      /////////// Scenario 3 Casess > 0 and all >0 /////////////
      ///////////////////////////////////////
      // else if(client['length'] > 0 && patient['data']['patients']['length'] > 0 && casses['data']['caseDetails']['length'] > 0){
      //
      //   this.getMegaSearchPatientClientRecord(casses,client,patient,type).then(megapatclientresp=>{
      //     resolve();
      //   })
      // }

    })
    // console.log('f1');
    // resolve();
  });
}
getMegaSearchPatientClientRecord(caseData,clientData, patientDat, type,searchString){
  var casesDataBlueprint = { caseNumber: 'null', caseId: 'null', pFName: 'null', pLName: 'null', pDob: 'null', clientName: 'null', collectionDate: 'null', collectedBy: 'null', caseStatusId: 'null', }
  return new Promise((resolve, reject) => {
    this.ngxLoader.start();

    // if (searchString.toLowerCase() == 'client' || searchString.toLowerCase() == 'sip') {
    //   var a = caseData['data']['caseDetails'];
    //   const uniqueIds = [];
    //   const uniqueCLIds = [];
    //   const map = new Map();
    //   for (const item of a) {
    //     if(!map.has(item.clientId)){
    //       map.set(item.clientId, true);    // set any value to Map
    //       uniqueCLIds.push(item.clientId);
    //     }
    //   }
    //   const map1 = new Map();
    //   for (const item of a) {
    //     if(!map1.has(item.patientId)){
    //       map1.set(item.patientId, true);    // set any value to Map
    //       uniqueIds.push(item.patientId);
    //     }
    //   }
    //
    //   // getPatReq.data.patientIds = uniqueIds;
    //   this.getPatClData(uniqueIds,caseData,uniqueCLIds,type).then(respAlldata => {
    //     resolve()
    //
    //
    //   });
    // }
    // else{
    ////////// check if patien is greatr or client
    ////////// if patient is great then search for client ids in pat is exist then fine
    ///////// else need to find cases bases of patient id

    if (clientData['length'] > 0 && patientDat['data']['patients'] != null && caseData['data']['caseDetails']['length'] == 0) {
      /////////////// if client has mo data Yaha pe new endpoint ko call karni he

      if (clientData['length'] > patientDat['data']['patients'].length) {
        var uniqIdPatient = []
        var uniqueClientId = [];
        const map1 = new Map();
        for (const item of clientData) {
          if(!map1.has(item.clientId)){
            map1.set(item.clientId, true);    // set any value to Map
            uniqueClientId.push(item.clientId);
          }
        }

        const map = new Map();
        for (const item of patientDat['data']['patients']) {
          if(!map.has(item.patientId)){
            map.set(item.patientId, true);    // set any value to Map
            uniqIdPatient.push(item.patientId);

          }
        }
        var clientStatuses = []
        if (type == 'pending') {
          clientStatuses = [1,2,3,4,5,7,8,9,10,11,12]
        }
        else if (type == 'delivered') {
          clientStatuses = [1,2,3,4,5,7,8,9,6,11,12]
        }
        else if (type == 'notdelivered') {
          clientStatuses = [1,2,3,4,5,7,8,9,10,11,6]
        }

        var columnUrl      = environment.API_SPECIMEN_ENDPOINT + 'searchbyclient';
        var columnCaseReq      = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "MRMA-MR",systemHostAddress:"",remoteUserAddress:"",dateTime:""},
        data:{page:"0",size:20,sortColumn:"caseNumber",sortingOrder:"desc",
        clientId: uniqueClientId,
        intakeId: this.selectedIntakeforClient,
        statuses:[],
        userCode:""}}

        if (this.logedInUserRoles.userType == 2) {
          columnCaseReq.data.userCode = this.logedInUserRoles.userCode;
        }
        this.http.post(columnUrl, columnCaseReq).subscribe(byClientIdsResp=>{
          console.log("---byClientIdsResp",byClientIdsResp);
          //////////////////////////////// If no case found for clients
          if (byClientIdsResp['data']['caseDetails'].length > 0) {
            for (let i = 0; i < byClientIdsResp['data']['caseDetails'].length; i++) {
              for (let j = 0; j < patientDat['data']['patients'].length; j++) {
                if (byClientIdsResp['data']['caseDetails'][i].patientId == patientDat['data']['patients'][j].patientId) {
                  if(uniqIdPatient.indexOf(byClientIdsResp['data']['caseDetails'][i].patientId) !== -1){
                    // alert("Value exists!")
                  } else{
                    uniqIdPatient.push(byClientIdsResp['data']['caseDetails'][i].patientId);
                  }
                }
              }
            }

            var tempH    = {}
            var tempHold = []
            var status = []
            if (type == 'pending') {
              status = [6]
            }
            else if (type == 'delivered') {
              status = [10]
            }
            else if (type == 'notdelivered') {
              status = [12]
            }
            var specUrl = environment.API_SPECIMEN_ENDPOINT + "findbypatientid"
            var reqPid  ={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "MRMA-MR",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{
              patientIds:uniqIdPatient,
              intakeId:this.selectedIntakeforClient,client:this.clientSearchString.clientId, statuses:status, userCode:""}}
              if (this.logedInUserRoles.userType == 2) {
                reqPid.data.userCode = this.logedInUserRoles.userCode;
              }
              this.globalService.findByPatientIds(specUrl, reqPid).subscribe(casesBypatientIds =>{
                console.log("*******casesBypatientIds",casesBypatientIds);
                uniqueClientId = []

                for (var c = 0; c < byClientIdsResp['data']['caseDetails'].length; c++) {
                  for (let d = 0; d < casesBypatientIds['data'].length; d++) {
                    if (byClientIdsResp['data']['caseDetails'][c].patientId == casesBypatientIds['data'][d].patientId) {

                    }
                    else{
                      if(uniqIdPatient.indexOf(casesBypatientIds['data'][d].patientId) !== -1){
                        // alert("Value exists!")
                      } else{
                        uniqIdPatient.push(casesBypatientIds['data'][d].patientId);
                      }
                    }
                    // if (byClientIdsResp['data']['caseDetails'][c].caseId == casesBypatientIds['data'].caseId) {
                    //
                    // }
                    // else{
                    //   tempH[c] = casesBypatientIds['data'][d];
                    //   if (tempH[c].patientId == byClientIdsResp['data']['caseDetails'][c].patientId) {
                    //     tempH[c].pFName = casesBypatientIds['data'][d].firstName;
                    //     tempH[c].pLName = casesBypatientIds['data'][d].lastName;
                    //     tempH[c].pDob   = casesBypatientIds['data'][d].dateOfBirth;
                    //   }
                    //   tempHold.push(tempH);
                    //   tempH = {}
                    // }




                  }
                }
                var patientIdforName = [];
                const map2 = new Map();
                for (const item of byClientIdsResp['data']['caseDetails']) {
                  if(!map2.has(item.patientId)){
                    map2.set(item.patientId, true);    // set any value to Map
                    patientIdforName.push(item.patientId);

                  }
                }

                var uniqClientNames = [];
                const map3 = new Map();
                for (const item of byClientIdsResp['data']['caseDetails']) {
                  if(!map3.has(item.clientId)){
                    map3.set(item.clientId, true);    // set any value to Map
                    uniqClientNames.push(item.clientId);

                  }
                }
                const patienByidURL = environment.API_PATIENT_ENDPOINT + 'patientbyids';
                var getPatReq       = {...this.patientByIdsReq}
                getPatReq.data.patientIds = patientIdforName;

                this.http.put(patienByidURL, getPatReq).subscribe(forPatientName=>{
                  console.log("forPatientName",forPatientName);

                  for (let index = 0; index < byClientIdsResp['data']['caseDetails'].length; index++) {
                    for (let i = 0; i < forPatientName['data'].length; i++) {
                      if (byClientIdsResp['data']['caseDetails'][index].patientId == forPatientName['data'][i].patientId) {

                        // this.deliveredReports = [];
                        byClientIdsResp['data']['caseDetails'][index].pFName = forPatientName['data'][i].firstName;
                        byClientIdsResp['data']['caseDetails'][index].pLName = forPatientName['data'][i].lastName;
                        byClientIdsResp['data']['caseDetails'][index].pDob   = forPatientName['data'][i].dateOfBirth;
                        byClientIdsResp['data']['caseDetails'][index].patientRevisionId   = forPatientName['data'][i].patientRevisionId;
                        byClientIdsResp['data']['caseDetails'][index].pPhone   = forPatientName['data'][i].phone;
                        byClientIdsResp['data']['caseDetails'][index].pGender   = forPatientName['data'][i].gender;
                        byClientIdsResp['data']['caseDetails'][index].pssn   = forPatientName['data'][i].ssn;
                        // TempTableHolder.push(casesBypatientIds['data'][c]);
                        // uniqueClientId.push(casesBypatientIds['data'][c].clientId)


                      }
                      else{
                        if (typeof byClientIdsResp['data']['caseDetails'][index].pFName == 'undefined') {
                          byClientIdsResp['data']['caseDetails'][index].pFName = "no Name";
                          byClientIdsResp['data']['caseDetails'][index].pLName = "no Name";
                          byClientIdsResp['data']['caseDetails'][index].pDob   = "02-02-2021 19:10:00";
                          byClientIdsResp['data']['caseDetails'][index].patientRevisionId   = null;
                          byClientIdsResp['data']['caseDetails'][index].pPhone   = '';
                          byClientIdsResp['data']['caseDetails'][index].pGender   = '';
                          byClientIdsResp['data']['caseDetails'][index].pssn   = '';
                        }

                        // console.log("Id Doesnot match",casesBypatientIds['data'][c].patientId);
                        // casesBypatientIds['data'].splice(c, 1);
                        // caselen=casesBypatientIds['data'];

                      }

                    }

                  }
                  console.log("byClientIdsResp['data']['caseDetails']",byClientIdsResp['data']['caseDetails']);

                  // const combined2 = [...tempHold, ...byClientIdsResp['data']['caseDetails']];
                  // console.log("combined2",combined2);
                  var clURL           = environment.API_CLIENT_ENDPOINT + "clientidnameemail"
                  // var clReqData       = {clientIds:uniqClientNames}
                  var clReqData = {
                    header:{
                      uuid               :"",
                      partnerCode        :"",
                      userCode           :"",
                      referenceNumber    :"",
                      systemCode         :"",
                      moduleCode         :"CLIM",
                      functionalityCode: "MRMA-MR",
                      systemHostAddress  :"",
                      remoteUserAddress  :"",
                      dateTime           :""
                    },
                    data:{
                      clientIds:uniqClientNames,
                      userCodes:[]
                    }
                  }
                  clReqData.header.partnerCode  = this.logedInUserRoles.partnerCode;
                  clReqData.header.userCode     = this.logedInUserRoles.userCode;
                  clReqData.header.functionalityCode     = "MRMA-MR";
                  this.globalService.getClientByName(clURL,clReqData).then(selectedClient => {
                    // console.log("selectedClient",selectedClient);
                    for (let f = 0; f < selectedClient['data']['length']; f++) {
                      for (let e = 0; e <  byClientIdsResp['data']['caseDetails'].length; e++) {

                        if ( byClientIdsResp['data']['caseDetails'][e].clientId == selectedClient['data'][f].clientId) {
                          // console.log("in if");
                          byClientIdsResp['data']['caseDetails'][e].clientName = selectedClient['data'][f].name;
                          byClientIdsResp['data']['caseDetails'][e].clientEmail   = selectedClient['data'][f].email;
                          // if (selectedClient['data'][f]['clientContactsByClientId'].length > 0) {
                          //   for (let cl = 0; cl < selectedClient['data'][f]['clientContactsByClientId'].length; cl++) {
                          //     if (selectedClient['data'][f]['clientContactsByClientId'][cl]['contactByContactId'].length >0) {
                          //       if (selectedClient['data'][f]['clientContactsByClientId'][cl]['contactByContactId'][0]['contactTypeId'] == 2) {
                          //         byClientIdsResp['data']['caseDetails'][e].clientEmail = selectedClient['data'][f]['clientContactsByClientId'][cl]['contactByContactId'][0]['value']
                          //       }
                          //     }
                          //   }
                          // }

                          // casesBypatientIds['data'].map((i) => { i.clientName = selectedClient[f].name; return i; });
                        }
                        else{
                          if (typeof byClientIdsResp['data']['caseDetails'][e].clientName == 'undefined') {
                            casesBypatientIds['data'][e].clientName="No Name";
                            casesBypatientIds['data'][e].clientEmail=null;
                          }
                          // console.log("in else");

                          // casesBypatientIds['data'][e].clientName="No Name"
                        }

                      }

                    }
                    // var finalData = [];
                    var finalDataCount = 0;
                    for (let index = 0; index < byClientIdsResp['data']['caseDetails']['length']; index++) {
                      finalDataCount = finalDataCount +1
                      // console.log("byClientIdsResp['data']['caseDetails'][index]",byClientIdsResp['data']['caseDetails'][index]);

                      // if (typeof byClientIdsResp['data']['caseDetails'] != 'undefined') {
                      if (byClientIdsResp['data']['caseDetails'][index]['pFName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || byClientIdsResp['data']['caseDetails'][index]['pLName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || this.datePipe.transform(byClientIdsResp['data']['caseDetails'][index]['pDob'].toLowerCase(), 'MM/dd/yyyy').indexOf(searchString.toLowerCase()) !== -1 || byClientIdsResp['data']['caseDetails'][index]['clientName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1|| this.datePipe.transform( byClientIdsResp['data']['caseDetails'][index]['collectionDate'].toLowerCase(), 'MM/dd/yyyy').indexOf(searchString.toLowerCase()) !== -1) {
                        // finalData[index] = byClientIdsResp['data']['caseDetails'][index];
                        if (searchString.toLowerCase() == 'client' || searchString.toLowerCase() == 'sip') {
                          if (byClientIdsResp['data']['caseDetails'][index]['pFName'].toLowerCase().indexOf('client') === -1
                          && byClientIdsResp['data']['caseDetails'][index]['pLName'].toLowerCase().indexOf('client') === -1
                          && byClientIdsResp['data']['caseDetails'][index]['clientName'].toLowerCase().indexOf('client') === -1) {
                            byClientIdsResp['data']['caseDetails'] = byClientIdsResp['data']['caseDetails'].filter(function(value, index, arr){
                              // console.log("value.collectedBy",value.collectedBy);
                              return value.collectedBy == 2;
                            });

                          }
                          else if (byClientIdsResp['data']['caseDetails'][index]['pFName'].toLowerCase().indexOf('sip') === -1
                          && byClientIdsResp['data']['caseDetails'][index]['pLName'].toLowerCase().indexOf('sip') === -1
                          && byClientIdsResp['data']['caseDetails'][index]['clientName'].toLowerCase().indexOf('sip') === -1) {
                            byClientIdsResp['data']['caseDetails'] = byClientIdsResp['data']['caseDetails'].filter(function(value, index, arr){
                              // console.log("value.collectedBy",value.collectedBy);
                              return value.collectedBy == 1;
                            });
                          }
                          if (type == 'delivered') {
                            this.deliveredReports.push(byClientIdsResp['data']['caseDetails'][index]);
                            // resolve()
                          }
                          else if(type == 'pending'){
                            this.pendingReport.push(byClientIdsResp['data']['caseDetails'][index]);
                          }
                          else if(type == 'notdelivered'){
                            this.notDeliveredReports .push(byClientIdsResp['data']['caseDetails'][index]);


                          }
                        }
                        else{
                          if (type == 'delivered') {
                            this.deliveredReports.push(byClientIdsResp['data']['caseDetails'][index]);
                            // resolve()
                          }
                          else if(type == 'pending'){
                            this.pendingReport.push(byClientIdsResp['data']['caseDetails'][index]);
                          }
                          else if(type == 'notdelivered'){
                            this.notDeliveredReports .push(byClientIdsResp['data']['caseDetails'][index]);


                          }
                        }
                        // if (type == 'delivered') {
                        //   this.deliveredReports.push(byClientIdsResp['data']['caseDetails'][index]);
                        //   // resolve()
                        // }
                        // else if(type == 'pending'){
                        //   this.pendingReport.push(byClientIdsResp['data']['caseDetails'][index]);
                        // }
                        // else if(type == 'notdelivered'){
                        //     this.notDeliveredReports .push(byClientIdsResp['data']['caseDetails'][index]);
                        //
                        //
                        // }
                      }
                      // }


                      if (finalDataCount == byClientIdsResp['data']['caseDetails']['length']) {
                        if (type == 'delivered') {
                          // this.deliveredReports = [];
                          this.deliveredReportsCount = this.deliveredReports['length'];
                          // this.deliveredReports = finalData;
                          this.ngxLoader.stop();
                          resolve()
                        }
                        else if(type == 'pending'){
                          // this.pendingReport = [];
                          this.pendingReportCount = this.pendingReport['length'];
                          // this.pendingReport = finalData;
                          this.ngxLoader.stop();
                          resolve()
                        }
                        else if(type == 'notdelivered'){
                          this.notDeliveredReports = this.notDeliveredReports['length'];
                          // this.pendingReport = finalData;
                          this.ngxLoader.stop();
                          resolve()


                        }


                      }
                    }
                    // console.log("finalData",finalData);
                    //
                    //
                    // if (type == 'delivered') {
                    //   // this.deliveredReports = [];
                    //   this.deliveredReportsCount =finalData['length'];
                    //   this.deliveredReports = finalData;
                    //   this.ngxLoader.stop();
                    //   resolve()
                    // }
                    // else if(type == 'pending'){
                    //   // this.pendingReport = [];
                    //   this.pendingReportCount = finalData['length'];
                    //   this.pendingReport = finalData;
                    //   this.ngxLoader.stop();
                    //   resolve()
                    // }

                  });

                })

              })
            }
            else{
              /////////////////////////// if no cases found in client sthen we look in patient
              for (let i = 0; i < byClientIdsResp['data']['caseDetails'].length; i++) {
                for (let j = 0; j < patientDat['data']['patients'].length; j++) {
                  if (byClientIdsResp['data']['caseDetails'][i].patientId == patientDat['data']['patients'][j].patientId) {
                    if(uniqIdPatient.indexOf(byClientIdsResp['data']['caseDetails'][i].patientId) !== -1){
                      // alert("Value exists!")
                    } else{
                      uniqIdPatient.push(byClientIdsResp['data']['caseDetails'][i].patientId);
                    }
                  }
                }
              }

              var tempH    = {}
              var tempHold = []
              var status = []
              if (type == 'pending') {
                status = [6]
              }
              else if (type == 'delivered') {
                status = [10]
              }
              else if (type == 'notdelivered') {
                status = [12]
              }
              var specUrl = environment.API_SPECIMEN_ENDPOINT + "findbypatientid"
              var reqPid1  ={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "MRMA-MR",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{
                patientIds:uniqIdPatient,
                intakeId:this.selectedIntakeforClient,client:this.clientSearchString.clientId, statuses:status
                , userCode:""}}
                if (this.logedInUserRoles.userType == 2) {
                  reqPid1.data.userCode = this.logedInUserRoles.userCode;
                }
              this.globalService.findByPatientIds(specUrl, reqPid1).subscribe(casesBypatientIds =>{
                // console.log("*******casesBypatientIds",casesBypatientIds);
                // console.log("*******caseData",caseData);
                uniqueClientId = []

                if (casesBypatientIds['data'].length == 0) {
                  if (caseData['data']['caseDetails']['length'] >0) {
                    console.log("hereeeeeeeee---");

                    var a = caseData['data']['caseDetails'];
                    const uniqueIds = [];
                    const uniqueCLIds = [];
                    const map = new Map();
                    for (const item of a) {
                      if(!map.has(item.clientId)){
                        map.set(item.clientId, true);    // set any value to Map
                        uniqueCLIds.push(item.clientId);
                      }
                    }
                    const map1 = new Map();
                    for (const item of a) {
                      if(!map1.has(item.patientId)){
                        map1.set(item.patientId, true);    // set any value to Map
                        uniqueIds.push(item.patientId);
                      }
                    }

                    // getPatReq.data.patientIds = uniqueIds;
                    this.getPatClData(uniqueIds,caseData,uniqueCLIds,type).then(respAlldata => {
                      resolve()


                    });
                  }
                  else{
                    if (type == 'delivered') {
                      this.deliveredReports = [];
                      this.deliveredReportsCount = 0;
                      // this.deliveredReports = finalData;
                      this.ngxLoader.stop();
                      resolve()
                    }
                    else if(type == 'pending'){
                      this.pendingReport = [];
                      this.pendingReportCount = 0;
                      // this.pendingReport = finalData;
                      this.ngxLoader.stop();
                      resolve()
                    }
                    else if(type == 'notdelivered'){
                      this.notDeliveredReports = []
                      this.notDeliveredReportsCount = 0;
                      // this.pendingReport = finalData;
                      this.ngxLoader.stop();
                      resolve()
                    }
                  }

                }
                var patientIdforName = [];
                const map2 = new Map();
                for (const item of casesBypatientIds['data']) {
                  if(!map2.has(item.patientId)){
                    map2.set(item.patientId, true);    // set any value to Map
                    patientIdforName.push(item.patientId);

                  }
                }

                var uniqClientNames = [];
                const map3 = new Map();
                for (const item of casesBypatientIds['data']) {
                  if(!map3.has(item.clientId)){
                    map3.set(item.clientId, true);    // set any value to Map
                    uniqClientNames.push(item.clientId);

                  }
                }
                const patienByidURL = environment.API_PATIENT_ENDPOINT + 'patientbyids';
                var getPatReq       = {...this.patientByIdsReq}
                getPatReq.data.patientIds = patientIdforName;

                this.http.put(patienByidURL, getPatReq).subscribe(forPatientName=>{
                  console.log("forPatientName",forPatientName);

                  for (let index = 0; index < casesBypatientIds['data'].length; index++) {
                    for (let i = 0; i < forPatientName['data'].length; i++) {
                      if (casesBypatientIds['data'][index].patientId == forPatientName['data'][i].patientId) {

                        // this.deliveredReports = [];
                        casesBypatientIds['data'][index].pFName = forPatientName['data'][i].firstName;
                        casesBypatientIds['data'][index].pLName = forPatientName['data'][i].lastName;
                        casesBypatientIds['data'][index].pDob   = forPatientName['data'][i].dateOfBirth;
                        casesBypatientIds['data'][index].patientRevisionId   = forPatientName['data'][i].patientRevisionId;
                        casesBypatientIds['data'][index].pPhone   = forPatientName['data'][i].phone;
                        casesBypatientIds['data'][index].pGender   = forPatientName['data'][i].gender;
                        casesBypatientIds['data'][index].pssn   = forPatientName['data'][i].ssn;
                        // TempTableHolder.push(casesBypatientIds['data'][c]);
                        // uniqueClientId.push(casesBypatientIds['data'][c].clientId)


                      }
                      else{
                        if (typeof casesBypatientIds['data'][index].pFName == 'undefined') {
                          casesBypatientIds['data'][index].pFName = "no Name";
                          casesBypatientIds['data'][index].pLName = "no Name";
                          casesBypatientIds['data'][index].pDob   = "02-02-2021 19:10:00";
                          casesBypatientIds['data'][index].patientRevisionId   = null;
                          casesBypatientIds['data'][index].pPhone   = '';
                          casesBypatientIds['data'][index].pGender   = '';
                          casesBypatientIds['data'][index].pssn   = '';
                        }
                        // casesBypatientIds['data'][index].pFName = "no Name";
                        // casesBypatientIds['data'][index].pLName = "no Name";
                        // casesBypatientIds['data'][index].pDob   = "02-02-2021 19:10:00";
                        // console.log("Id Doesnot match",casesBypatientIds['data'][c].patientId);
                        // casesBypatientIds['data'].splice(c, 1);
                        // caselen=casesBypatientIds['data'];

                      }

                    }

                  }
                  // console.log("byClientIdsResp['data']['caseDetails']",byClientIdsResp['data']['caseDetails']);

                  // const combined2 = [...tempHold, ...byClientIdsResp['data']['caseDetails']];
                  // console.log("combined2",combined2);
                  var clURL           = environment.API_CLIENT_ENDPOINT + "clientidnameemail"
                  var clReqData = {
                    header:{
                      uuid               :"",
                      partnerCode        :"",
                      userCode           :"",
                      referenceNumber    :"",
                      systemCode         :"",
                      moduleCode         :"CLIM",
                      functionalityCode: "MRMA-MR",
                      systemHostAddress  :"",
                      remoteUserAddress  :"",
                      dateTime           :""
                    },
                    data:{
                      clientIds:uniqClientNames,
                      userCodes:[]
                    }
                  }
                  clReqData.header.partnerCode  = this.logedInUserRoles.partnerCode;
                  clReqData.header.userCode     = this.logedInUserRoles.userCode;
                  clReqData.header.functionalityCode     = "MRMA-MR";
                  // var clReqData       = {clientIds:uniqClientNames}
                  this.globalService.getClientByName(clURL,clReqData).then(selectedClient => {
                    // console.log("selectedClient",selectedClient);
                    for (let f = 0; f < selectedClient['data']['length']; f++) {
                      for (let e = 0; e < casesBypatientIds['data'].length; e++) {

                        if (casesBypatientIds['data'][e].clientId == selectedClient['data'][f].clientId) {
                          // console.log("in if");
                          casesBypatientIds['data'][e].clientName = selectedClient['data'][f].name;
                          casesBypatientIds['data'][e].clientEmail = selectedClient['data'][f].email;

                          // if (selectedClient['data'][f]['clientContactsByClientId'].length > 0) {
                          //   for (let cl = 0; cl < selectedClient['data'][f]['clientContactsByClientId'].length; cl++) {
                          //     if (selectedClient['data'][f]['clientContactsByClientId'][cl]['contactByContactId'].length >0) {
                          //       if (selectedClient['data'][f]['clientContactsByClientId'][cl]['contactByContactId'][0]['contactTypeId'] == 2) {
                          //         casesBypatientIds['data'][e].clientEmail = selectedClient['data'][f]['clientContactsByClientId'][cl]['contactByContactId'][0]['value']
                          //       }
                          //     }
                          //   }
                          // }

                          // casesBypatientIds['data'].map((i) => { i.clientName = selectedClient[f].name; return i; });
                        }
                        else{
                          // console.log("in else");
                          if (typeof casesBypatientIds['data'][e].clientName == 'undefined') {
                            casesBypatientIds['data'][e].clientName="No Name"
                            casesBypatientIds['data'][e].clientEmail=null
                          }

                          // casesBypatientIds['data'][e].clientName="No Name"
                        }

                      }

                    }
                    // var finalData = [];
                    var finalDataCount = 0;
                    for (let index = 0; index < casesBypatientIds['data']['length']; index++) {
                      finalDataCount = finalDataCount +1
                      // console.log("casesBypatientIds['data'][index]",casesBypatientIds['data'][index]);

                      // if (typeof casesBypatientIds['data'] != 'undefined') {
                      if (casesBypatientIds['data'][index]['pFName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || casesBypatientIds['data'][index]['pLName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || this.datePipe.transform(casesBypatientIds['data'][index]['pDob'].toLowerCase(), 'MM/dd/yyyy').indexOf(searchString.toLowerCase()) !== -1
                      || casesBypatientIds['data'][index]['clientName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || this.datePipe.transform( casesBypatientIds['data'][index]['collectionDate'].toLowerCase(), 'MM/dd/yyyy').indexOf(searchString.toLowerCase()) !== -1) {
                        // finalData[index] = casesBypatientIds['data'][index];
                        if (searchString.toLowerCase() == 'client' || searchString.toLowerCase() == 'sip') {
                          if (casesBypatientIds['data'][index]['pFName'].toLowerCase().indexOf('client') === -1
                          && casesBypatientIds['data'][index]['pLName'].toLowerCase().indexOf('client') === -1
                          && casesBypatientIds['data'][index]['clientName'].toLowerCase().indexOf('client') === -1) {
                            casesBypatientIds['data'] = casesBypatientIds['data'].filter(function(value, index, arr){
                              // console.log("value.collectedBy",value.collectedBy);
                              return value.collectedBy == 2;
                            });

                          }
                          else if (casesBypatientIds['data'][index]['pFName'].toLowerCase().indexOf('sip') === -1
                          && casesBypatientIds['data'][index]['pLName'].toLowerCase().indexOf('sip') === -1
                          && casesBypatientIds['data'][index]['clientName'].toLowerCase().indexOf('sip') === -1) {
                            casesBypatientIds['data'] = casesBypatientIds['data'].filter(function(value, index, arr){
                              // console.log("value.collectedBy",value.collectedBy);
                              return value.collectedBy == 1;
                            });
                          }
                          if (type == 'delivered') {
                            this.deliveredReports.push(casesBypatientIds['data'][index]);
                            // resolve()
                          }
                          else if(type == 'pending'){
                            this.pendingReport.push(casesBypatientIds['data'][index]);
                          }
                          else if(type == 'notdelivered'){
                            this.notDeliveredReports .push(casesBypatientIds['data'][index]);


                          }
                        }
                        else{
                          if (type == 'delivered') {
                            this.deliveredReports.push(casesBypatientIds['data'][index]);
                            // resolve()
                          }
                          else if(type == 'pending'){
                            this.pendingReport.push(casesBypatientIds['data'][index]);
                          }
                          else if(type == 'notdelivered'){
                            this.notDeliveredReports .push(casesBypatientIds['data'][index]);


                          }
                        }
                        // if (type == 'delivered') {
                        //   this.deliveredReports.push(casesBypatientIds['data'][index]);
                        //   // resolve()
                        // }
                        // else if(type == 'pending'){
                        //   this.pendingReport.push(casesBypatientIds['data'][index]);
                        // }
                        // else if(type == 'notdelivered'){
                        //     this.notDeliveredReports.push(casesBypatientIds['data'][index]);
                        // }
                      }
                      // }


                      if (finalDataCount == casesBypatientIds['data']['length']) {
                        if (type == 'delivered') {
                          // this.deliveredReports = [];
                          this.deliveredReportsCount = this.deliveredReports['length'];
                          // this.deliveredReports = finalData;
                          this.ngxLoader.stop();
                          resolve()
                        }
                        else if(type == 'pending'){
                          // this.pendingReport = [];
                          this.pendingReportCount = this.pendingReport['length'];
                          // this.pendingReport = finalData;
                          this.ngxLoader.stop();
                          resolve()
                        }
                        else if(type == 'notdelivered'){
                          this.notDeliveredReportsCount = this.notDeliveredReports['length'];
                          // this.pendingReport = finalData;
                          this.ngxLoader.stop();
                          resolve()
                        }


                      }
                    }
                    // console.log("finalData",finalData);
                    //
                    //
                    // if (type == 'delivered') {
                    //   // this.deliveredReports = [];
                    //   this.deliveredReportsCount =finalData['length'];
                    //   this.deliveredReports = finalData;
                    //   this.ngxLoader.stop();
                    //   resolve()
                    // }
                    // else if(type == 'pending'){
                    //   // this.pendingReport = [];
                    //   this.pendingReportCount = finalData['length'];
                    //   this.pendingReport = finalData;
                    //   this.ngxLoader.stop();
                    //   resolve()
                    // }

                  });

                })

              })
            }

          })




        }
        else{
          var uniqueClientId = [];
          var uniquePatientId = [];
          const map = new Map();
          for (const item of clientData) {
            if(!map.has(item.clientId)){
              map.set(item.clientId, true);    // set any value to Map
              uniqueClientId.push(item.clientId);

            }
            console.log("uniqueClientId----",uniqueClientId);


            // tempForTable.push(casesDataBlueprint);
            // casesDataBlueprint = { caseNumber: null, caseId: null, pFName: null, pLName: null, pDob: null, clientName: null, collectionDate: null, collectedBy: null, caseStatusId: null, }
          }
          const map2 = new Map();
          for (const item of patientDat['data']['patients']) {
            if(!map2.has(item.patientId)){
              map2.set(item.patientId, true);    // set any value to Map
              uniquePatientId.push(item.patientId);

            }

            // tempForTable.push(casesDataBlueprint);
            // casesDataBlueprint = { caseNumber: null, caseId: null, pFName: null, pLName: null, pDob: null, clientName: null, collectionDate: null, collectedBy: null, caseStatusId: null, }
          }
          // console.log("uniquePatientId",uniquePatientId);

          for (var j = 0, len2 = clientData['length']; j < len2; j++) {
            for (var i = 0, len = patientDat['data']['patients']['length']; i < len; i++) {
              if (patientDat['data']['patients'][i].clientId == clientData[j].clientId) {

              }
              else{
                console.log("clientData[j].clientId",clientData[j].clientId);
                for (let k = 0; k < uniqueClientId.length; k++) {
                  if (uniqueClientId[k] == patientDat['data']['patients'][i].clientId) {

                  }
                  else{
                    if(uniqueClientId.indexOf(patientDat['data']['patients'][i].clientId) !== -1){
                      // alert("Value exists!")
                    } else{
                      uniqueClientId.push(patientDat['data']['patients'][i].clientId);
                    }

                  }

                }

              }
            }
          }
          console.log("uniqueClientId222222",uniqueClientId);
          var clURL           = environment.API_PATIENT_ENDPOINT + "patientbyclientids"
          var reqPatBP  = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"",functionalityCode: "MRMA-MR",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{userName:"danish",clientIds:uniqueClientId}}
          this.globalService.getPatientsByClietnIds(clURL,reqPatBP).subscribe(patientByClientIds => {
            console.log("------patientByClientIds",patientByClientIds);
            this.deliveredReports = [];
            for (let dum = 0; dum < patientByClientIds['data'].length; dum++) {

              // casesDataBlueprint['pFName'] = patientByClientIds['data'][dum].firstName
              // casesDataBlueprint['pLName'] = patientByClientIds['data'][dum].lastName
              // casesDataBlueprint['pDob']   = patientByClientIds['data'][dum].dateOfBirth
              // casesDataBlueprint['caseNumber'] = "null"; casesDataBlueprint['caseId'] = "null"; casesDataBlueprint['clientName']= "null", casesDataBlueprint['collectionDate']="null"; casesDataBlueprint['collectedBy']="null"; casesDataBlueprint['caseStatusId']= "1";
              // if (type == 'delivered') {
              //   this.deliveredReports.push(casesDataBlueprint);
              //   casesDataBlueprint = { caseNumber: '', caseId: '', pFName: '', pLName: '', pDob: '', clientName: '', collectionDate: '', collectedBy: '', caseStatusId: '', }
              //
              // }
              // else if(type == 'pending'){
              //   // this.deliveredReports = [];
              //   this.pendingReport.push(casesDataBlueprint);
              //   casesDataBlueprint = { caseNumber: '', caseId: '', pFName: '', pLName: '', pDob: '', clientName: '', collectionDate: '', collectedBy: '', caseStatusId: '', }
              // }

            }



            var tempForTable    = [];
            const map = new Map();
            for (const item of patientByClientIds['data']) {
              if(!map.has(item.patientId)){
                map.set(item.patientId, true);    // set any value to Map
                uniquePatientId.push(item.patientId);

              }

              // tempForTable.push(casesDataBlueprint);
              // casesDataBlueprint = { caseNumber: null, caseId: null, pFName: null, pLName: null, pDob: null, clientName: null, collectionDate: null, collectedBy: null, caseStatusId: null, }
            }




            var TempTableHolder = [];
            var status = []
            if (type == 'pending') {
              status = [6]
            }
            else if (type == 'delivered') {
              status = [10]
            }
            else if (type == 'notdelivered') {
              status = [12]
            }
            var specUrl = environment.API_SPECIMEN_ENDPOINT + "findbypatientid"
            var reqPid  ={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "MRMA-MR",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:
            {patientIds:uniquePatientId,
              intakeId:this.selectedIntakeforClient,client:this.clientSearchString.clientId, statuses:status, userCode:""}}
              if (this.logedInUserRoles.userType == 2) {
                reqPid.data.userCode = this.logedInUserRoles.userCode;
              }
              this.globalService.findByPatientIds(specUrl, reqPid).subscribe(casesBypatientIds =>{
                console.log("*******casesBypatientIds",casesBypatientIds);
                // uniqueClientId = []

                for (var c = 0;c < casesBypatientIds['data'].length; c++) {
                  for (let j = 0; j < patientByClientIds['data'].length; j++) {
                    if (casesBypatientIds['data'][c].patientId == patientByClientIds['data'][j].patientId) {
                      console.log("patientByClientIds['data'][j].firstName",patientByClientIds['data'][j]);

                      // this.deliveredReports = [];
                      casesBypatientIds['data'][c].pFName = patientByClientIds['data'][j].firstName;
                      casesBypatientIds['data'][c].pLName = patientByClientIds['data'][j].lastName;
                      casesBypatientIds['data'][c].pDob   = patientByClientIds['data'][j].dateOfBirth;
                      casesBypatientIds['data'][c].patientRevisionId   = patientByClientIds['data'][j].patientRevisionId;
                      casesBypatientIds['data'][c].pPhone   = patientByClientIds['data'][j].phone;
                      casesBypatientIds['data'][c].pGender   = patientByClientIds['data'][j].gender;
                      casesBypatientIds['data'][c].pssn   = patientByClientIds['data'][j].ssn;
                      console.log("33232323232",casesBypatientIds['data'][c]);
                    }
                    else{
                      // console.log("else-----",patientByClientIds['data'][j].firstName);

                      // casesBypatientIds['data'][c].pFName = "No Name";
                      // casesBypatientIds['data'][c].pLName = "No Name";
                      // casesBypatientIds['data'][c].pDob   = "02-02-2021 19:10:00";
                    }
                    if(uniqueClientId.indexOf(casesBypatientIds['data'][c].clientId) !== -1){
                      // alert("Value exists!")
                    } else{
                      uniqueClientId.push(casesBypatientIds['data'][c].clientId);
                    }

                  }


                }
                console.log("uniqueClientId3333",uniqueClientId);
                var clURL           = environment.API_CLIENT_ENDPOINT + "clientidnameemail"
                var clReqData = {
                  header:{
                    uuid               :"",
                    partnerCode        :"",
                    userCode           :"",
                    referenceNumber    :"",
                    systemCode         :"",
                    moduleCode         :"CLIM",
                    functionalityCode: "MRMA-MR",
                    systemHostAddress  :"",
                    remoteUserAddress  :"",
                    dateTime           :""
                  },
                  data:{
                    clientIds:uniqueClientId,
                    userCodes:[]
                  }
                }
                clReqData.header.partnerCode  = this.logedInUserRoles.partnerCode;
                clReqData.header.userCode     = this.logedInUserRoles.userCode;
                clReqData.header.functionalityCode     = "MRMA-MR";
                // var clReqData       = {clientIds:uniqueClientId}
                this.globalService.getClientByName(clURL,clReqData).then(selectedClient => {
                  console.log("selectedClient",selectedClient);
                  for (let f = 0; f < selectedClient['data']['length']; f++) {
                    for (let e = 0; e < casesBypatientIds['data'].length; e++) {

                      if (casesBypatientIds['data'][e].clientId == selectedClient['data'][f].clientId) {
                        // console.log("in if");
                        casesBypatientIds['data'][e].clientName = selectedClient['data'][f].name;
                        casesBypatientIds['data'][e].clientName = selectedClient['data'][f].email;
                        // if (selectedClient['data'][f]['clientContactsByClientId'].length > 0) {
                        //   for (let cl = 0; cl < selectedClient['data'][f]['clientContactsByClientId'].length; cl++) {
                        //     if (selectedClient['data'][f]['clientContactsByClientId'][cl]['contactByContactId'].length >0) {
                        //       if (selectedClient['data'][f]['clientContactsByClientId'][cl]['contactByContactId'][0]['contactTypeId'] == 2) {
                        //         casesBypatientIds['data'][e].clientEmail = selectedClient['data'][f]['clientContactsByClientId'][cl]['contactByContactId'][0]['value']
                        //       }
                        //     }
                        //   }
                        // }

                        // casesBypatientIds['data'].map((i) => { i.clientName = selectedClient[f].name; return i; });
                      }
                      else{
                        // console.log("in else");

                        // casesBypatientIds['data'][e].clientName="No Name"
                      }

                    }
                    // console.log('=========');


                  }
                  // for (let i = 0; i < casesBypatientIds['data'].length; i++) {
                  //   for (let j = 0; j < patientDat['data']['patients'].length; j++) {
                  //     if (casesBypatientIds['data'][i].patientId == patientDat['data']['patients'][j].patientId) {
                  //       // this.deliveredReports = [];
                  //       casesBypatientIds['data'][i].pFName = patientDat['data']['patients'][j].firstName;
                  //       casesBypatientIds['data'][i].pLName = patientDat['data']['patients'][j].lastName;
                  //       casesBypatientIds['data'][i].pDob   = patientDat['data']['patients'][j].dateOfBirth;
                  //     }
                  //
                  //   }
                  //
                  // }
                  // var finalData = [];
                  // var finalDataHolder = {};
                  var finalDataCount = 0;
                  console.log("casesBypatientIds['data']",casesBypatientIds['data']);


                  for (let index = 0; index < casesBypatientIds['data']['length']; index++) {
                    finalDataCount = finalDataCount +1;
                    // console.log("casesBypatientIds['data'][index]",casesBypatientIds['data'][index]);

                    if (casesBypatientIds['data'][index]['pFName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || casesBypatientIds['data'][index]['pLName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || this.datePipe.transform(casesBypatientIds['data'][index]['pDob'].toLowerCase(), 'MM/dd/yyyy').indexOf(searchString.toLowerCase()) !== -1
                    || casesBypatientIds['data'][index]['clientName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || this.datePipe.transform(casesBypatientIds['data'][index]['collectionDate'].toLowerCase(), 'MM/dd/yyyy').indexOf(searchString.toLowerCase()) !== -1) {
                      // finalData[index] = casesBypatientIds['data'][index];
                      if (searchString.toLowerCase() == 'client' || searchString.toLowerCase() == 'sip') {
                        if (casesBypatientIds['data'][index]['pFName'].toLowerCase().indexOf('client') === -1
                        && casesBypatientIds['data'][index]['pLName'].toLowerCase().indexOf('client') === -1
                        && casesBypatientIds['data'][index]['clientName'].toLowerCase().indexOf('client') === -1) {
                          casesBypatientIds['data'] = casesBypatientIds['data'].filter(function(value, index, arr){
                            // console.log("value.collectedBy",value.collectedBy);
                            return value.collectedBy == 2;
                          });

                        }
                        else if (casesBypatientIds['data'][index]['pFName'].toLowerCase().indexOf('sip') === -1
                        && casesBypatientIds['data'][index]['pLName'].toLowerCase().indexOf('sip') === -1
                        && casesBypatientIds['data'][index]['clientName'].toLowerCase().indexOf('sip') === -1) {
                          casesBypatientIds['data'] = casesBypatientIds['data'].filter(function(value, index, arr){
                            // console.log("value.collectedBy",value.collectedBy);
                            return value.collectedBy == 1;
                          });
                        }
                        if (type == 'delivered') {
                          this.deliveredReports.push(casesBypatientIds['data'][index]);
                          // resolve()
                        }
                        else if(type == 'pending'){
                          this.pendingReport.push(casesBypatientIds['data'][index]);
                        }
                        else if(type == 'notdelivered'){
                          this.notDeliveredReports .push(casesBypatientIds['data'][index]);


                        }
                      }
                      else{
                        if (type == 'delivered') {
                          this.deliveredReports.push(casesBypatientIds['data'][index]);
                          // resolve()
                        }
                        else if(type == 'pending'){
                          this.pendingReport.push(casesBypatientIds['data'][index]);
                        }
                        else if(type == 'notdelivered'){
                          this.notDeliveredReports .push(casesBypatientIds['data'][index]);


                        }
                      }
                      // if (type == 'delivered') {
                      //   this.deliveredReports.push(casesBypatientIds['data'][index]);
                      //   // resolve()
                      // }
                      // else if(type == 'pending'){
                      //   this.pendingReport.push(casesBypatientIds['data'][index]);
                      // }
                      // else if(type == 'notdelivered'){
                      //     this.notDeliveredReports.push(casesBypatientIds['data'][index]);
                      // }
                    }

                    if (finalDataCount == casesBypatientIds['data']['length']) {
                      if (type == 'delivered') {
                        // this.deliveredReports = [];
                        this.deliveredReportsCount = this.deliveredReports['length'];
                        // this.deliveredReports = finalData;
                        this.ngxLoader.stop();
                        resolve()
                      }
                      else if(type == 'pending'){
                        // this.pendingReport = [];
                        this.pendingReportCount = this.pendingReport['length'];
                        // this.pendingReport = finalData;
                        this.ngxLoader.stop();
                        resolve()
                      }
                      else if(type == 'notdelivered'){
                        this.notDeliveredReportsCount = this.notDeliveredReports['length'];
                        // this.pendingReport = finalData;
                        this.ngxLoader.stop();
                        resolve()
                      }


                    }

                  }


                });

                // this.getPatClData(uniquePatientId,casesBypatientIds,uniqueCLIds,type).then(respAlldata => {
                //
                // })
              })

            });
          }
          // console.log('--------------------');
          // tempForTable.push(casesDataBlueprint);
          // casesDataBlueprint = { caseNumber: null, caseId: null, pFName: null, pLName: null, pDob: null, clientName: null, collectionDate: null, collectedBy: null, caseStatusId: null, }



        }
        else if(clientData['length'] == 0 && patientDat['data']['patients'] != null && caseData['data']['caseDetails']['length'] == 0){
          //////////////////////get uniqe patient Ids
          var uniquePatientId = [];
          var tempForTable    = [];
          const map = new Map();
          for (const item of patientDat['data']['patients']) {
            if(!map.has(item.patientId)){
              map.set(item.patientId, true);    // set any value to Map
              uniquePatientId.push(item.patientId);

            }
          }
          var status = []
          if (type == 'pending') {
            status = [6]
          }
          else if (type == 'delivered') {
            status = [10]
          }
          else if (type == 'notdelivered') {
            status = [12]
          }
          var byPidURL = environment.API_SPECIMEN_ENDPOINT + 'findbypatientid';
          var reqPid={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "MRMA-MR",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{
            patientIds:uniquePatientId,
            intakeId:this.selectedIntakeforClient,client:this.clientSearchString.clientId, statuses:status, userCode:""
          }}
          if (this.logedInUserRoles.userType == 2) {
            reqPid.data.userCode = this.logedInUserRoles.userCode;
          }
          this.globalService.findByPatientIds(byPidURL, reqPid).subscribe(respPID=>{
            console.log("by Patien ID Active Cases",respPID);
            if (respPID['data']['length'] == 0) {
              if (caseData['data']['caseDetails']['length'] >0) {
                console.log("hereeeeeeeee---");

                var a = caseData['data']['caseDetails'];
                const uniqueIds = [];
                const uniqueCLIds = [];
                const map = new Map();
                for (const item of a) {
                  if(!map.has(item.clientId)){
                    map.set(item.clientId, true);    // set any value to Map
                    uniqueCLIds.push(item.clientId);
                  }
                }
                const map1 = new Map();
                for (const item of a) {
                  if(!map1.has(item.patientId)){
                    map1.set(item.patientId, true);    // set any value to Map
                    uniqueIds.push(item.patientId);
                  }
                }

                // getPatReq.data.patientIds = uniqueIds;
                this.getPatClData(uniqueIds,caseData,uniqueCLIds,type).then(respAlldata => {
                  resolve()


                });
              }
              else{
                if (type == 'delivered') {
                  this.deliveredReports = [];
                  this.deliveredReportsCount = 0;
                  // this.deliveredReports = finalData;
                  this.ngxLoader.stop();
                  resolve()
                }
                else if(type == 'pending'){
                  this.pendingReport = [];
                  this.pendingReportCount = 0;
                  // this.pendingReport = finalData;
                  this.ngxLoader.stop();
                  resolve()
                }
                else if(type == 'notdelivered'){
                  this.notDeliveredReports = []
                  this.notDeliveredReportsCount = 0;
                  // this.pendingReport = finalData;
                  this.ngxLoader.stop();
                  resolve()
                }
              }
            }
            else{
              const uniqueCLIds = []
              const map = new Map();
              for (let i = 0; i < respPID['data'].length; i++) {
                for (let j = 0; j < patientDat['data']['patients'].length; j++) {
                  if(respPID['data'][i].patientId == patientDat['data']['patients'].patientId){

                  }
                  for (const item of respPID['data']) {
                    if(!map.has(item.clientId)){
                      map.set(item.clientId, true);    // set any value to Map
                      uniqueCLIds.push(item.clientId);
                    }
                  }

                }

              }


              this.getPatClDataMegaSearch(uniquePatientId,respPID,uniqueCLIds,type,searchString).then(respAlldata => {
                // console.log("activeCases",this.deliveredReports);
                resolve();

              });
            }



          })

          // console.log("----uniqueClientId",uniqueClientId);


          // }
        }
        else if(clientData['length'] > 0 && patientDat['data']['patients'] == null && caseData['data']['caseDetails']['length'] == 0){
          var uniqIdPatient = []
          var uniqueClientId = [];
          const map1 = new Map();
          for (const item of clientData) {
            if(!map1.has(item.clientId)){
              map1.set(item.clientId, true);    // set any value to Map
              uniqueClientId.push(item.clientId);
            }
          }
          var clientStatuses = []
          if (type == 'pending') {
            clientStatuses = [1,2,3,4,5,7,8,9,10,11,12]
          }
          else if (type == 'delivered') {
            clientStatuses = [1,2,3,4,5,7,8,9,6,11,12]
          }
          else if (type == 'notdelivered') {
            clientStatuses = [1,2,3,4,5,7,8,9,10,11,6]
          }


          let columnCaseReq  = {}
          var columnUrl      = environment.API_SPECIMEN_ENDPOINT + 'searchbyclient';
          columnCaseReq      = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "MRMA-MR",systemHostAddress:"",remoteUserAddress:"",dateTime:""},
          data:{page:"0",size:20,sortColumn:"caseNumber",sortingOrder:"desc",
          clientId: uniqueClientId,
          intakeId: this.selectedIntakeforClient,
          statuses:[],
          userCode:""}}
          if (this.logedInUserRoles.userType == 2) {
            columnCaseReq['data'].userCode = this.logedInUserRoles.userCode;
          }
          this.http.post(columnUrl, columnCaseReq).subscribe(byClientIdsResp=>{
            console.log("---byClientIdsResp",byClientIdsResp);
            if (byClientIdsResp['data']['caseDetails'].length == 0) {
              if (caseData['data']['caseDetails']['length'] >0) {
                console.log("hereeeeeeeee---");

                var a = caseData['data']['caseDetails'];
                const uniqueIds = [];
                const uniqueCLIds = [];
                const map = new Map();
                for (const item of a) {
                  if(!map.has(item.clientId)){
                    map.set(item.clientId, true);    // set any value to Map
                    uniqueCLIds.push(item.clientId);
                  }
                }
                const map1 = new Map();
                for (const item of a) {
                  if(!map1.has(item.patientId)){
                    map1.set(item.patientId, true);    // set any value to Map
                    uniqueIds.push(item.patientId);
                  }
                }

                // getPatReq.data.patientIds = uniqueIds;
                this.getPatClData(uniqueIds,caseData,uniqueCLIds,type).then(respAlldata => {
                  resolve()


                });
              }
              else{
                if (type == 'delivered') {
                  this.deliveredReports = [];
                  this.deliveredReportsCount = this.deliveredReports['length'];
                  // this.deliveredReports = finalData;
                  this.ngxLoader.stop();
                  resolve()
                }
                else if(type == 'pending'){
                  this.pendingReport = [];
                  this.pendingReportCount = this.pendingReport['length'];
                  // this.pendingReport = finalData;
                  this.ngxLoader.stop();
                  resolve()
                }
                else if(type == 'notdelivered'){
                  this.notDeliveredReportsCount = this.notDeliveredReports['length'];
                  this.notDeliveredReports = [];
                  this.ngxLoader.stop();
                  resolve()
                }
              }

            }
            for (let i = 0; i < byClientIdsResp['data']['caseDetails'].length; i++) {
              for (let j = 0; j < clientData['length']; j++) {
                if (byClientIdsResp['data']['caseDetails'][i].clientId == clientData[j].clientId) {
                  if(uniqueClientId.indexOf(byClientIdsResp['data']['caseDetails'][i].clientId) !== -1){
                    // alert("Value exists!")
                  } else{
                    uniqueClientId.push(byClientIdsResp['data']['caseDetails'][i].clientId);
                  }
                }
              }
            }

            ///////////////
            //////if case is not null/////////
            ///////////////////////////////////
            // const combined2
            if (caseData['data']['caseDetails']['length'] > 0) {
              var userTemporaryhold   = [];
              for (let cd = 0; cd < caseData['data']['caseDetails'].length; cd++) {
                for (let cc = 0; cc < byClientIdsResp['data']['caseDetails'].length; cc++) {

                  if (caseData['data']['caseDetails'][cd].caseId == byClientIdsResp['data']['caseDetails'][cc].caseId) {
                    // console.log("equal");

                    ///////////////////////no need to change
                  }
                  else{
                    userTemporaryhold[cd]   = caseData['data']['caseDetails'][cd];
                    // byClientIdsResp['data']['caseDetails'].push(caseData['data']['caseDetails'][cd]);
                    if(uniqueClientId.indexOf(caseData['data']['caseDetails'][cd].clientId) !== -1){
                      // alert("Value exists!")
                    } else{
                      uniqueClientId.push(caseData['data']['caseDetails'][cd].clientId);
                    }
                  }

                }

              }
              byClientIdsResp['data']['caseDetails'] = [...userTemporaryhold, ...byClientIdsResp['data']['caseDetails']];
              // console.log("after case involvement",byClientIdsResp);
              // console.log("after case involvement uniqueClientId",uniqueClientId);
              // console.log("combined2",combined2);
            }


            // uniqueClientId = []
            var clURL           = environment.API_PATIENT_ENDPOINT + "patientbyclientids"
            var reqPatBP  = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"",functionalityCode: "MRMA-MR",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{userName:"danish",clientIds:uniqueClientId}}
            this.globalService.getPatientsByClietnIds(clURL,reqPatBP).subscribe(patientByClientIds => {
              console.log("*******patientByClientIds",patientByClientIds);

              for (var c = 0; c< byClientIdsResp['data']['caseDetails']['length']; c++) {
                for (let d = 0; d < patientByClientIds['data']['length']; d++) {

                  if (byClientIdsResp['data']['caseDetails'][c].clientId == patientByClientIds['data'][d].clientId) {
                    // this.deliveredReports = [];
                    // console.log('------',patientByClientIds['data'][d].firstName);
                    // console.log('------',patientByClientIds['data'][d].lastName);
                    // console.log('------',byClientIdsResp['data']['caseDetails'][c].caseNumber);

                    byClientIdsResp['data']['caseDetails'][c].pFName = patientByClientIds['data'][d].firstName;
                    byClientIdsResp['data']['caseDetails'][c].pLName = patientByClientIds['data'][d].lastName;
                    byClientIdsResp['data']['caseDetails'][c].pDob   = patientByClientIds['data'][d].dateOfBirth;
                    byClientIdsResp['data']['caseDetails'][c].patientRevisionId   = patientByClientIds['data'][d].patientRevisionId;
                    byClientIdsResp['data']['caseDetails'][c].pPhone   = patientByClientIds['data'][d].phone;
                    byClientIdsResp['data']['caseDetails'][c].pGender   = patientByClientIds['data'][d].gender;
                    byClientIdsResp['data']['caseDetails'][c].pssn   = patientByClientIds['data'][d].ssn;
                    // TempTableHolder.push(casesBypatientIds['data'][c]);
                    // uniqueClientId.push(patientByClientIds['data'][c].clientId)
                    if(uniqueClientId.indexOf(patientByClientIds['data'][d].clientId) !== -1){
                      // alert("Value exists!")
                    } else{
                      uniqueClientId.push(patientByClientIds['data'][d].clientId);
                    }
                  }
                  else{
                    // console.log("else",byClientIdsResp['data']['caseDetails'][c].pFName);
                    if (typeof byClientIdsResp['data']['caseDetails'][c].pFName == 'undefined') {
                      // byClientIdsResp['data']['caseDetails'][c].pFName = patientByClientIds['data'][d].firstName;
                      // byClientIdsResp['data']['caseDetails'][c].pLName = patientByClientIds['data'][d].lastName;
                      // byClientIdsResp['data']['caseDetails'][c].pDob   = patientByClientIds['data'][d].dateOfBirth;
                      byClientIdsResp['data']['caseDetails'][c].pFName = "no Name";
                      byClientIdsResp['data']['caseDetails'][c].pLName = "no Name";
                      byClientIdsResp['data']['caseDetails'][c].pDob   = "02-02-2021 19:10:00";
                      byClientIdsResp['data']['caseDetails'][c].patientRevisionId   = null;
                      byClientIdsResp['data']['caseDetails'][c].pPhone   = '';
                      byClientIdsResp['data']['caseDetails'][c].pGender   = '';
                      byClientIdsResp['data']['caseDetails'][c].pssn   = '';
                      console.log(" patientByClientIds['data'][d].dateOfBirth", patientByClientIds['data'][d].dateOfBirth);

                      if(uniqueClientId.indexOf(byClientIdsResp['data']['caseDetails'][c].clientId) !== -1){
                        // alert("Value exists!")
                      } else{
                        uniqueClientId.push(byClientIdsResp['data']['caseDetails'][c].clientId);
                      }
                    }


                    // console.log("Id Doesnot match",casesBypatientIds['data'][c].patientId);
                    // casesBypatientIds['data'].splice(c, 1);
                    // caselen=casesBypatientIds['data'];

                    // byClientIdsResp['data']['caseDetails'][c].pFName = "no Name";
                    // byClientIdsResp['data']['caseDetails'][c].pLName = "no Name";
                    // byClientIdsResp['data']['caseDetails'][c].pDob   = "02-02-2021 19:10:00";


                  }

                }

              }
              var clURL           = environment.API_CLIENT_ENDPOINT + "clientidnameemail"
              var clReqData = {
                header:{
                  uuid               :"",
                  partnerCode        :"",
                  userCode           :"",
                  referenceNumber    :"",
                  systemCode         :"",
                  moduleCode         :"CLIM",
                  functionalityCode: "MRMA-MR",
                  systemHostAddress  :"",
                  remoteUserAddress  :"",
                  dateTime           :""
                },
                data:{
                  clientIds:uniqueClientId,
                  userCodes:[]
                }
              }
              clReqData.header.partnerCode  = this.logedInUserRoles.partnerCode;
              clReqData.header.userCode     = this.logedInUserRoles.userCode;
              clReqData.header.functionalityCode     = "MRMA-MR";
              // var clReqData       = {clientIds:uniqueClientId}
              this.globalService.getClientByName(clURL,clReqData).then(selectedClient => {
                console.log("selectedClient",selectedClient);
                for (let f = 0; f < selectedClient['data']['length']; f++) {
                  for (let e = 0; e < byClientIdsResp['data']['caseDetails'].length; e++) {
                    // console.log("byClientIdsResp['data']['caseDetails'][e]",byClientIdsResp['data']['caseDetails'][e]);
                    //     console.log("selectedClient['data'][f].clientId",selectedClient['data'][f].clientId);

                    if (byClientIdsResp['data']['caseDetails'][e].clientId == selectedClient['data'][f].clientId) {
                      // console.log("in if");
                      byClientIdsResp['data']['caseDetails'][e].clientName = selectedClient['data'][f].name;
                      byClientIdsResp['data']['caseDetails'][e].clientEmail = selectedClient['data'][f].email;
                      // if (selectedClient['data'][f]['clientContactsByClientId'].length > 0) {
                      //   for (let cl = 0; cl < selectedClient['data'][f]['clientContactsByClientId'].length; cl++) {
                      //     if (selectedClient['data'][f]['clientContactsByClientId'][cl]['contactByContactId'].length >0) {
                      //       if (selectedClient['data'][f]['clientContactsByClientId'][cl]['contactByContactId'][0]['contactTypeId'] == 2) {
                      //         byClientIdsResp['data']['caseDetails'][e].clientEmail = selectedClient['data'][f]['clientContactsByClientId'][cl]['contactByContactId'][0]['value']
                      //       }
                      //     }
                      //   }
                      // }

                      // casesBypatientIds['data'].map((i) => { i.clientName = selectedClient[f].name; return i; });
                    }
                    else{
                      // console.log("in else");
                      if (typeof byClientIdsResp['data']['caseDetails'][e].clientName == 'undefined') {
                        byClientIdsResp['data']['caseDetails'][e].clientName="No Name";
                        byClientIdsResp['data']['caseDetails'][e].clientEmail=null;
                      }


                    }

                  }

                }
                // var finalData = [];
                var finalDataCount = 0;
                for (let index = 0; index < byClientIdsResp['data']['caseDetails']['length']; index++) {
                  finalDataCount = finalDataCount +1
                  // if (1) {
                  // console.log("byClientIdsResp['data']['caseDetails'][index]",byClientIdsResp['data']['caseDetails'][index]);

                  // if (byClientIdsResp['data']['caseDetails'][index]['pFName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || byClientIdsResp['data']['caseDetails'][index]['pLName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || byClientIdsResp['data']['caseDetails'][index]['pDob'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || byClientIdsResp['data']['caseDetails'][index]['clientName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1) {
                  // finalData[index] = byClientIdsResp['data'][index];
                  if (searchString.toLowerCase() == 'client' || searchString.toLowerCase() == 'sip') {
                    if (byClientIdsResp['data']['caseDetails'][index]['pFName'].toLowerCase().indexOf('client') === -1
                    && byClientIdsResp['data']['caseDetails'][index]['pLName'].toLowerCase().indexOf('client') === -1
                    && byClientIdsResp['data']['caseDetails'][index]['clientName'].toLowerCase().indexOf('client') === -1) {
                      byClientIdsResp['data']['caseDetails'] = byClientIdsResp['data']['caseDetails'].filter(function(value, index, arr){
                        // console.log("value.collectedBy",value.collectedBy);
                        return value.collectedBy == 2;
                      });

                    }
                    else if (byClientIdsResp['data']['caseDetails'][index]['pFName'].toLowerCase().indexOf('sip') === -1
                    && byClientIdsResp['data']['caseDetails'][index]['pLName'].toLowerCase().indexOf('sip') === -1
                    && byClientIdsResp['data']['caseDetails'][index]['clientName'].toLowerCase().indexOf('sip') === -1) {
                      byClientIdsResp['data']['caseDetails'] = byClientIdsResp['data']['caseDetails'].filter(function(value, index, arr){
                        // console.log("value.collectedBy",value.collectedBy);
                        return value.collectedBy == 1;
                      });
                    }
                    if (type == 'delivered') {
                      this.deliveredReports.push(byClientIdsResp['data']['caseDetails'][index]);
                      // resolve()
                    }
                    else if(type == 'pending'){
                      this.pendingReport.push(byClientIdsResp['data']['caseDetails'][index]);
                    }
                    else if(type == 'notdelivered'){
                      this.notDeliveredReports .push(byClientIdsResp['data']['caseDetails'][index]);


                    }
                  }
                  else{
                    if (type == 'delivered') {
                      this.deliveredReports.push(byClientIdsResp['data']['caseDetails'][index]);
                      // resolve()
                    }
                    else if(type == 'pending'){
                      this.pendingReport.push(byClientIdsResp['data']['caseDetails'][index]);
                    }
                    else if(type == 'notdelivered'){
                      this.notDeliveredReports .push(byClientIdsResp['data']['caseDetails'][index]);


                    }
                  }
                  // if (type == 'delivered') {
                  //   this.deliveredReports.push(byClientIdsResp['data']['caseDetails'][index]);
                  //   // resolve()
                  // }
                  // else if(type == 'pending'){
                  //   this.pendingReport.push(byClientIdsResp['data']['caseDetails'][index]);
                  // }
                  // else if(type == 'notdelivered'){
                  //     this.notDeliveredReports.push(byClientIdsResp['data']['caseDetails'][index]);
                  // }
                  // }

                  if (finalDataCount == byClientIdsResp['data']['caseDetails']['length']) {
                    if (type == 'delivered') {
                      // this.deliveredReports = [];
                      this.deliveredReportsCount = this.deliveredReports['length'];
                      // this.deliveredReports = finalData;
                      this.ngxLoader.stop();
                      resolve()
                    }
                    else if(type == 'pending'){
                      // this.pendingReport = [];
                      this.pendingReportCount = this.pendingReport['length'];
                      // this.pendingReport = finalData;
                      this.ngxLoader.stop();
                      resolve()
                    }
                    else if(type == 'notdelivered'){
                      this.notDeliveredReportsCount = this.notDeliveredReports['length'];
                      // this.pendingReport = finalData;
                      this.ngxLoader.stop();
                      resolve()
                    }


                  }

                }
                // console.log("finalData",finalData);
                //
                // if (type == 'delivered') {
                //   // this.deliveredReports = [];
                //   this.deliveredReportsCount = finalData['length'];
                //   this.deliveredReports = finalData;
                //   this.ngxLoader.stop();
                //   resolve()
                // }
                // else if(type == 'pending'){
                //   // this.pendingReport = [];
                //   this.pendingReportCount = finalData['length'];
                //   this.pendingReport = finalData;
                //   this.ngxLoader.stop();
                //   resolve()
                // }

              });
            })

          })



        }
        else if(clientData['length'] == 0 && patientDat['data']['patients'] == null && caseData['data']['caseDetails']['length'] >0 ){
          var a = caseData['data']['caseDetails'];
          const uniqueIds = [];
          const uniqueCLIds = [];
          const map = new Map();
          for (const item of a) {
            if(!map.has(item.clientId)){
              map.set(item.clientId, true);    // set any value to Map
              uniqueCLIds.push(item.clientId);
            }
          }
          const map1 = new Map();
          for (const item of a) {
            if(!map1.has(item.patientId)){
              map1.set(item.patientId, true);    // set any value to Map
              uniqueIds.push(item.patientId);
            }
          }

          // getPatReq.data.patientIds = uniqueIds;
          this.getPatClData(uniqueIds,caseData,uniqueCLIds,type).then(respAlldata => {
            resolve()


          });


        }
        else if (clientData['length'] == 0 && patientDat['data']['patients'] != null && caseData['data']['caseDetails']['length'] >0){
          this.handlePatientandCaseFromSearch(caseData,clientData, patientDat, type,searchString).then(handlePatientandCaseFromSearch=>{
            resolve()
            // console.log("handlePatientandCaseFromSearch",handlePatientandCaseFromSearch);
          })
        }
        else if (clientData['length'] > 0 && patientDat['data']['patients'] == null && caseData['data']['caseDetails']['length'] >0){
          this.handleClientandCaseFromSearch(caseData,clientData, patientDat, type,searchString).then(handlePatientandCaseFromSearch=>{
            resolve()
            // console.log("handlePatientandCaseFromSearch",handlePatientandCaseFromSearch);
          })
        }
        else if (clientData['length'] > 0 && patientDat['data']['patients'] != null && caseData['data']['caseDetails']['length'] >0){
          this.handleClientPatientandCaseFromSearch(caseData,clientData, patientDat, type,searchString).then(handlePatientandCaseFromSearch=>{
            resolve()
            // console.log("handlePatientandCaseFromSearch",handlePatientandCaseFromSearch);
          })
        }
        // }




      })
    }
    getPatClDataMegaSearch(userids,resp,clientids,type,searchString) {
      // this.ngxLoader.start();

      const patienByidURL = environment.API_PATIENT_ENDPOINT + 'patientbyids';
      var getPatReq       = {...this.patientByIdsReq}
      getPatReq.header.partnerCode  = this.logedInUserRoles.partnerCode;
      getPatReq.header.userCode     = this.logedInUserRoles.userCode;
      getPatReq.header.functionalityCode     = "MRMA-MR";
      var clURL           = environment.API_CLIENT_ENDPOINT + "clientidnameemail"
      var clReqData = {
        header:{
          uuid               :"",
          partnerCode        :"",
          userCode           :"",
          referenceNumber    :"",
          systemCode         :"",
          moduleCode         :"CLIM",
          functionalityCode: "MRMA-MR",
          systemHostAddress  :"",
          remoteUserAddress  :"",
          dateTime           :""
        },
        data:{
          clientIds:clientids,
          userCodes:[]
        }
      }
      clReqData.header.partnerCode  = this.logedInUserRoles.partnerCode;
      clReqData.header.userCode     = this.logedInUserRoles.userCode;
      clReqData.header.functionalityCode     = "MRMA-MR";
      // var clReqData       = {clientIds:clientids}
      getPatReq.data.patientIds = userids;
      return new Promise((resolve, reject) => {
        if (resp['data'].length == 0) {
          if (type == 'delivered') {
            // this.deliveredReports = [];
            this.deliveredReportsCount = 0;
            // this.deliveredReports = finalData;
            this.ngxLoader.stop();
            resolve()
          }
          else if(type == 'pending'){
            // this.pendingReport = [];
            this.pendingReportCount = 0;
            // this.pendingReport = finalData;
            this.ngxLoader.stop();
            resolve()
          }
          else if(type == 'notdelivered'){
            this.notDeliveredReportsCount = 0;
            // this.pendingReport = finalData;
            this.ngxLoader.stop();
            resolve()
          }
          // return
        }
        var patientout = this.globalService.getPatientsByIds(patienByidURL, getPatReq).then(patient=>{
          return patient;
        })
        var clientsout = this.globalService.getClientByName(clURL,clReqData).then(selectedClient => {
          return selectedClient['data'];
        });
        forkJoin([patientout, clientsout]).subscribe(bothresults => {
          console.log('bothresults FOrk Join------',bothresults);
          var patientresp = bothresults[0];
          var clients      = bothresults[1];
          console.log("resp['data']---------",resp['data']);

          if (patientresp['result'].codeType == "S") {
            if (typeof resp['data'] != "undefined") {
              for (let i = 0; i < resp['data'].length; i++) {
                for (let j = 0; j < patientresp['data'].length; j++) {
                  if (resp['data'][i].patientId == patientresp['data'][j].patientId) {
                    resp['data'][i].pFName = patientresp['data'][j].firstName;
                    resp['data'][i].pLName = patientresp['data'][j].lastName;
                    resp['data'][i].pDob   = patientresp['data'][j].dateOfBirth;
                    resp['data'][i].patientRevisionId   = patientresp['data'][j].patientRevisionId;
                    resp['data'][i].pPhone   = patientresp['data'][j].phone;
                    resp['data'][i].pGender   = patientresp['data'][j].gender;
                    resp['data'][i].pssn   = patientresp['data'][j].ssn;
                  }
                  else{
                    if (typeof resp['data'][i].pFName == 'undefined') {
                      resp['data'][i].pFName = "no Name";
                      resp['data'][i].pLName = "no Name";
                      resp['data'][i].pDob   = "02-02-2021 19:10:00";
                      resp['data'][i].patientRevisionId   = null;
                      resp['data'][i].pPhone   = '';
                      resp['data'][i].pGender   = '';
                      resp['data'][i].pssn   = '';
                    }
                    // resp['data'][i].pFName = "no Name";
                    // resp['data'][i].pLName = "no Name";
                    // resp['data'][i].pDob   = "02-02-2021 19:10:00";
                  }

                }

              }
              // this.pendingReport       = resp['data'];
              // this.pendingReportCount  = resp['data']['totalCount'];
            }
            else{

              // for (let i = 0; i < resp['data'].length; i++) {
              //   for (let j = 0; j < patientresp['data'].length; j++) {
              //     if (resp['data'][i].patientId == patientresp['data'][j].patientId) {
              //       resp['data'][i].pFName = patientresp['data'][j].firstName;
              //       resp['data'][i].pLName = patientresp['data'][j].lastName;
              //       resp['data'][i].pDob   = patientresp['data'][j].dateOfBirth;
              //     }
              //
              //   }
              //
              // }
            }
          }
          else{
            this.ngxLoader.stop();
            this.notifier.notify( "error", "Error while fetching patients" );
            // return;
          }

          if (clients['length'] > 0) {
            if (typeof resp['data'] != "undefined") {
              // for (let i = 0; i < resp['data'].length; i++) {
              for (let i = 0; i < resp['data'].length; i++) {
                for (let j = 0; j < clients['length']; j++) {
                  if (resp['data'][i].clientId   == clients[j].clientId) {
                    resp['data'][i].clientName   = clients[j].name;
                    resp['data'][i].clientName   = clients[j].email;
                    // if (clients[j]['clientContactsByClientId'].length > 0) {
                    //   for (let cl = 0; cl < clients[j]['clientContactsByClientId'].length; cl++) {
                    //     if (clients[j]['clientContactsByClientId'][cl]['contactByContactId'].length >0) {
                    //       if (clients[j]['clientContactsByClientId'][cl]['contactByContactId'][0]['contactTypeId'] == 2) {
                    //         resp['data'][i].clientEmail = clients[j]['clientContactsByClientId'][cl]['contactByContactId'][0]['value']
                    //       }
                    //     }
                    //   }
                    // }
                  }
                  else{
                    if (typeof resp['data'][i].clientName =='undefined') {
                      resp['data'][i].clientName   = "no Name";
                      resp['data'][i].clientEmail   = null;
                    }

                  }

                }

              }
            }
            else{

              // for (let i = 0; i < resp['data'].length; i++) {
              // for (let i = 0; i < resp['data'].length; i++) {
              //   for (let j = 0; j < clients['length']; j++) {
              //     if (resp['data'][i].clientId   == clients[j].clientId) {
              //       resp['data'][i].clientName   = clients[j].name;
              //     }
              //
              //   }
              //
              // }
            }

          }
          else{
            // this.notifier.notify( "error", "No Client Found" );
            // return;
          }
          // var finalData = [];
          var finalDataCount = 0;
          for (let index = 0; index < resp['data']['length']; index++) {
            finalDataCount = finalDataCount +1
            if (resp['data'][index]['pFName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || resp['data'][index]['pLName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || this.datePipe.transform(resp['data'][index]['pDob'].toLowerCase(), 'MM/dd/yyyy').indexOf(searchString.toLowerCase()) !== -1
            || resp['data'][index]['clientName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || this.datePipe.transform(resp['data'][index]['collectionDate'].toLowerCase(), 'MM/dd/yyyy').indexOf(searchString.toLowerCase()) !== -1) {
              // finalData[index] = resp['data'][index];
              if (searchString.toLowerCase() == 'client' || searchString.toLowerCase() == 'sip') {
                if (resp['data'][index]['pFName'].toLowerCase().indexOf('client') === -1
                && resp['data'][index]['pLName'].toLowerCase().indexOf('client') === -1
                && resp['data'][index]['clientName'].toLowerCase().indexOf('client') === -1) {
                  resp['data'] = resp['data'].filter(function(value, index, arr){
                    // console.log("value.collectedBy",value.collectedBy);
                    return value.collectedBy == 2;
                  });

                }
                else if (resp['data'][index]['pFName'].toLowerCase().indexOf('sip') === -1
                && resp['data'][index]['pLName'].toLowerCase().indexOf('sip') === -1
                && resp['data'][index]['clientName'].toLowerCase().indexOf('sip') === -1) {
                  resp['data'] = resp['data'].filter(function(value, index, arr){
                    // console.log("value.collectedBy",value.collectedBy);
                    return value.collectedBy == 1;
                  });
                }
                if (type == 'delivered') {
                  this.deliveredReports.push(resp['data'][index]);
                  // resolve()
                }
                else if(type == 'pending'){
                  this.pendingReport.push(resp['data'][index]);
                }
                else if(type == 'notdelivered'){
                  this.notDeliveredReports .push(resp['data'][index]);


                }
              }
              else{
                if (type == 'delivered') {
                  this.deliveredReports.push(resp['data'][index]);
                  // resolve()
                }
                else if(type == 'pending'){
                  this.pendingReport.push(resp['data'][index]);
                }
                else if(type == 'notdelivered'){
                  this.notDeliveredReports .push(resp['data'][index]);


                }
              }
              // if (type == 'delivered') {
              //   this.deliveredReports.push(resp['data'][index]);
              //   // resolve()
              // }
              // else if(type == 'pending'){
              //   this.pendingReport.push(resp['data'][index]);
              // }
              // else if(type == 'notdelivered'){
              //     this.notDeliveredReports.push(resp['data'][index]);
              // }
            }

            if (finalDataCount == resp['data']['length']) {
              if (type == 'delivered') {
                // this.deliveredReports = [];
                this.deliveredReportsCount = this.deliveredReports['length'];
                // this.deliveredReports = finalData;
                this.ngxLoader.stop();
                resolve()
              }
              else if(type == 'pending'){
                // this.pendingReport = [];
                this.pendingReportCount = this.pendingReport['length'];
                // this.pendingReport = finalData;
                this.ngxLoader.stop();
                resolve()
              }
              else if(type == 'notdelivered'){
                this.notDeliveredReportsCount = this.notDeliveredReports['length'];
                // this.pendingReport = finalData;
                this.ngxLoader.stop();
                resolve()
              }


            }

          }
          // console.log("finalData",finalData);
          // if (type == 'delivered') {
          //   if (typeof resp['data'] != "undefined") {
          //     this.deliveredReports       = finalData;
          //     this.deliveredReportsCount  = finalData['length'];
          //   }
          //   else{
          //     // this.deliveredReports       = resp['data'];
          //     // this.deliveredReportsCount  = resp['data']['totalCount'];
          //   }
          // }
          // else{
          //   if (typeof resp['data'] != "undefined") {
          //     this.pendingReport       = finalData;
          //     this.pendingReportCount  = finalData['length'];
          //   }
          //   else{
          //     // this.pendingReport       = resp['data'];
          //     // this.pendingReportCount  = resp['data']['totalCount'];
          //   }
          // }



          resolve();
        });
        // resolve();
      });
    }
    getLookups(){
      // this.ngxLoader.start();
      return new Promise((resolve, reject) => {
        var caseCat = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=CASE_CATEGORY_CACHE&partnerCode=sip').then(response =>{
          return response;
        })
        var specType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=SPECIMEN_TYPE_CACHE&partnerCode=sip').then(response =>{
          return response;
        })
        var statusType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=CASE_STATUS_CACHE&partnerCode=sip').then(response =>{
          return response;
        })
        var accType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=CLIENT_TYPE_CACHE&partnerCode=sip').then(response =>{
          return response;
        })
        var interpretationType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=TEST_INTERPRETATION_CACHE&partnerCode=sip').then(response =>{
          return response;
        })
        var fileType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=ATTACHMENT_FILE_TYPE_CACHE&partnerCode=sip').then(response =>{
          return response;
        })
        var deliveryM = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=DELIVERY_METHOD_CACHE&partnerCode=sip').then(response =>{
          return response;
        })
        var reportPackage = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=REPORT_PACKAGE_CACHE&partnerCode=sip').then(response =>{
          return response;
        })
        var reportTemplate = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=REPORT_TEMPLATE_CACHE&partnerCode=sip').then(response =>{
          return response;
        })
        // var deliveryStatus = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=DELIVER_STATUS_CACHE&partnerCode=sip').then(response =>{
        //   return response;
        // })
        var deliveryStatus = [0];


        forkJoin([caseCat, specType, statusType, accType ,interpretationType, fileType, deliveryM,reportPackage,reportTemplate,deliveryStatus]).subscribe(allLookups => {
          console.log("allLookups",allLookups);
          this.allCaseCategories     = allLookups[0]
          this.allSpecimenTypes      = allLookups[1]
          // this.allSpecimenTypes      = allLookups[1]
          // console.log("this.allLookups[1]",allLookups[1]);
          // console.log("this.allSpecimenTypes",this.allSpecimenTypes);
          this.specimenType          = allLookups[1][1];
          this.allStatusType         = allLookups[2]
          // console.log("this.allSpecimenTypes",this.allSpecimenTypes);
          this.defaultAccountTypes     = allLookups[3], "asc", "name";
          this.allInterpretation     = allLookups[4];
          // console.log('this.allInterpretation',this.allInterpretation[0]['comment']);
          // console.log('this.allInterpretation escape',escape(this.allInterpretation[0]['comment']));

          this.defaultFileType         = allLookups[5];
          this.deliverMethod           = allLookups[6];
          this.reportPackages           = allLookups[7];
          this.reportTemplate           = allLookups[8];
          this.deliverStatus           = allLookups[9];
          // console.log("this.allInterpretation",this.allInterpretation);
          for (let i = 0; i < allLookups[0]['length']; i++) {
            if (allLookups[0][i].caseCategoryDefault == 1) {
              this.selectedCaseCat = allLookups[0][i];
              this.saveRequest.data.caseCategoryId     = allLookups[0][i].caseCategoryId;
              this.saveRequest.data.caseCategoryPrefix = allLookups[0][i].caseCategoryPrefix;
            }
          }
          resolve();

        })
      })

    }

    ngOnDestroy(): void {
      // Do not forget to unsubscribe the event
      this.dtTrigger.unsubscribe();
    }

    changeCheckBoxes(e,index,item,type) {
      // $('#check-all3').attr('checked', false);
      // console.log('index',index);
      // console.log('item',item);
      // console.log('type',type);


      if (e.target.checked) {

        var holder = {}
        holder['index'] = index;
        holder['item'] = item;
        holder['value'] = e.target.checked;
        // console.log("holder",holder);

        if (type == 'pending') {
          this.listForconsolidatedReport.push(holder);
          this.enablePendingReportTypeCheckBoxes = true;
        }
        else if (type == 'delivered') {
          this.listForconsolidatedReportDelivered.push(holder)
          this.enableDeliveredReportTypeCheckBoxes = true;
        }
        else if (type == 'notdelivered') {

          this.listForconsolidatedReportnotDelivered.push(holder);
          this.enableNotDeliveredReportTypeCheckBoxes = true;
        }
        // console.log('this.listForconsolidatedReportnotDelivered',this.listForconsolidatedReportnotDelivered);


      }
      else{
        if (type == 'pending') {
          for (let i = 0; i < this.listForconsolidatedReport.length; i++) {
            if (this.listForconsolidatedReport[i].index == index) {
              this.listForconsolidatedReport.splice(i,1)
            }
          }
          this.pendingReport[index].checkBox = false;
          if (this.listForconsolidatedReport.length == 0) {
            this.enablePendingReportTypeCheckBoxes = false;
            $('#check-all1')[0].checked = false;
          }
        }
        else if (type == 'delivered') {
          for (let i = 0; i < this.listForconsolidatedReportDelivered.length; i++) {
            if (this.listForconsolidatedReportDelivered[i].index == index) {
              this.listForconsolidatedReportDelivered.splice(i,1)

            }
          }
          this.deliveredReports[index].checkBox = false;
          if (this.listForconsolidatedReportDelivered.length == 0) {
            this.enableDeliveredReportTypeCheckBoxes = false;
            $('#check-all2')[0].checked = false;
          }
        }
        else if (type == 'notdelivered') {
          // console.log('this.listForconsolidatedReportnotDelivered',this.listForconsolidatedReportnotDelivered);
          // console.log('index',index);


          for (let i = 0; i < this.listForconsolidatedReportnotDelivered.length; i++) {
            if (this.listForconsolidatedReportnotDelivered[i].index == index) {
              console.log('this.listForconsolidatedReportnotDelivered[i]',this.listForconsolidatedReportnotDelivered[i]);

              this.listForconsolidatedReportnotDelivered.splice(i,1)
            }

          }
          this.notDeliveredReports[index].checkBox = false;
          if (this.listForconsolidatedReportnotDelivered.length == 0) {
            this.enableNotDeliveredReportTypeCheckBoxes = false;
            $('#check-all3')[0].checked = false;
          }
        }

      }
      // console.log('this.listForconsolidatedReport',this.listForconsolidatedReport);
    }
    checkallBoxes(e,type){
      // console.log("e.target.value",e.target.checked);

      if (e.target.checked) {
        var holderTemp = []
        var holder = {}
        if (type == 'pending') {
          for (let i = 0; i < this.pendingReport.length; i++) {
            holder['index'] = i;
            holder['item'] = this.pendingReport[i];
            holder['value'] = e.target.checked;
            holderTemp.push(holder)
            holder = {}
            this.pendingReport[i].checkBox = true;
          }
          this.listForconsolidatedReport = holderTemp;
          this.enablePendingReportTypeCheckBoxes = true;
          // this.enablePendingReportTypeCheckBoxes = true;
        }
        else if (type == 'delivered') {
          for (let i = 0; i < this.deliveredReports.length; i++) {
            holder['index'] = i;
            holder['item'] = this.deliveredReports[i];
            holder['value'] = e.target.checked;
            holderTemp.push(holder)
            holder = {}
            this.deliveredReports[i].checkBox = true;
          }
          this.listForconsolidatedReportDelivered = holderTemp;
          this.enableDeliveredReportTypeCheckBoxes = true;
        }
        else if (type == 'notdelivered') {
          for (let i = 0; i < this.notDeliveredReports.length; i++) {
            holder['index'] = i;
            holder['item'] = this.notDeliveredReports[i];
            holder['value'] = e.target.checked;
            holderTemp.push(holder)
            holder = {}
            this.notDeliveredReports[i].checkBox = true;
          }
          this.listForconsolidatedReportnotDelivered = holderTemp;
          this.enableNotDeliveredReportTypeCheckBoxes = true;
        }
      }
      else{
        // console.log('here');
        if (type == 'pending') {
          for (let i = 0; i < this.pendingReport.length; i++) {
            this.pendingReport[i].checkBox = false;
          }
          this.listForconsolidatedReport = [];
          this.enablePendingReportTypeCheckBoxes = false;
          $('#check-all1')[0].checked = false;
        }
        else if (type == 'delivered') {
          for (let i = 0; i < this.deliveredReports.length; i++) {
            this.deliveredReports[i].checkBox = false;
          }
          this.listForconsolidatedReportDelivered = [];
          this.enableDeliveredReportTypeCheckBoxes = false;
          $('#check-all2')[0].checked = false;
        }
        else if (type == 'notdelivered') {
          for (let i = 0; i < this.notDeliveredReports.length; i++) {
            this.notDeliveredReports[i].checkBox = false;
          }
          this.listForconsolidatedReportnotDelivered = [];
          this.enableNotDeliveredReportTypeCheckBoxes = false;
          $('#check-all3')[0].checked = false;
        }
      }
      // console.log('this.listForconsolidatedReport',this.listForconsolidatedReport);

    }


    consolidatedReport(reportPackageId,reportTemplate,fileTypeId,deliverMethod,tableType){
      console.log("CONSOLIDATED",this.listForconsolidatedReportnotDelivered);
      var tableFrom;
      if (tableType == 'pending') {
        tableFrom = this.listForconsolidatedReport;
      }
      else if (tableType == 'delivered') {
        tableFrom = this.listForconsolidatedReportDelivered;
      }
      else if (tableType == 'notdelivered') {
        tableFrom = this.listForconsolidatedReportnotDelivered;
      }

      // return;
      if (tableFrom.length == 0) {
        this.ngxLoader.stop()
        alert('please select atleast one case');
        return;
      }
      for (var check = 0; check < tableFrom.length; check++) {
        if (tableFrom[0].item.clientId != tableFrom[check].item.clientId) {
          this.ngxLoader.stop()
          alert('please select casses associated to same client');

          return;
        }
      }
      this.ngxLoader.start()
      var myDate = new Date();
      var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy');
      var currentTimeStamp = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');

      var requestTempHolder = {
        caseResults:[]
      };
      var defaultPhysicianID     = '';
      var physicianFax     = '';
      var physicianPhone     = '';
      var salesRepEmail     = '';
      var accountNumber     = '';
      var atdPhyUrl   =  environment.API_CLIENT_ENDPOINT + "clientadressinfo";
      var reqTemplate = {
        header:{
          uuid               :"",
          partnerCode        :"",
          userCode           :"",
          referenceNumber    :"",
          systemCode         :"",
          moduleCode         :"CLIM",
          functionalityCode: "CLTA-VC",
          systemHostAddress  :"",
          remoteUserAddress  :"",
          dateTime           :""
        },
        data:{
          clientIds:[tableFrom[0].item.clientId],
          userCodes: []
        }
      }
      reqTemplate.header.partnerCode  = this.logedInUserRoles.partnerCode;
      reqTemplate.header.userCode     = this.logedInUserRoles.userCode;
      reqTemplate.header.functionalityCode     = "CLTA-VC";
      // var reqTemplate = {clientIds:[clientId]}
      this.globalService.getClientByIds(atdPhyUrl,reqTemplate).subscribe(respClient => {
        console.log("respClient summary Broken",respClient);
        if (respClient['statusCode'] == "Internal Server Error") {
          // alert('No Physician Associated to this Client')
          this.ngxLoader.stop();
          this.notifier.notify("error","No response from server please check your connection")
          return;
        }
        if (respClient['data'].length == 0) {
          alert('No record found for this Client')
          this.ngxLoader.stop();
          return;
        }
        else {
          var physCount = 0; /////// if there is no default physician then slect 1s as physician
          var arrCount  = 0;
          accountNumber      = respClient['data'][0]['accountNumber'];
          defaultPhysicianID = respClient['data'][0]['defaultAttendingPhysician'];
          if (respClient['data'][0]['physicianFax'] != null && respClient['data'][0]['physicianFax'] != '') {
            physicianFax       = this.encryptDecrypt.decryptString(respClient['data'][0]['physicianFax']);
          }

          // for (let i = 0; i < respClient['data'][0]['clientUsers'].length; i++) {
          //
          //   if (respClient['data'][0]['clientUsers'][i]['defaultPhysician'] == 1) {
          //     // attendingPhysicianName = respClient['data'][0]['clientUsers'][i]['userCode'];
          //     defaultPhysicianID = respClient['data'][0]['clientUsers'][i]['userCode'];
          //     for (let n = 0; n < respClient['data'][0]['clientUserContactsByClientId'].length; n++) {
          //       if (respClient['data'][0]['clientUserContactsByClientId'][n]['userCode'] == respClient['data'][0]['clientUsers'][i]['userCode']) {
          //         if (respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['contactTypeId'] == 3) {
          //           physicianFax = this.encryptDecrypt.decryptString(respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['value']);
          //         }
          //
          //       }
          //
          //     }
          //
          //     physCount ++;
          //   }
          //   arrCount = arrCount+ 1;
          //   if (arrCount == respClient['data'][0]['clientUsers'].length) {
          //     if (physCount == 0) {
          //       // attendingPhysicianName = respClient['data'][0]['clientUsers'][0]['userCode'];
          //       defaultPhysicianID = respClient['data'][0]['clientUsers'][0]['userCode'];
          //     }
          //   }
          // }
          console.log("defaultPhysicianID",defaultPhysicianID);
          var userRequest = {...this.usersRequest};
          userRequest.data.userCodes = [defaultPhysicianID,respClient['data'][0]['salesRepresentativeId']];
          var userUrl = environment.API_USER_ENDPOINT + "getusersbyusercodes";
          this.globalService.getUserRecord(userUrl,userRequest).subscribe(getUserResp =>{
            // return getUserResp;
            console.log("getUserResp",getUserResp);
            if (getUserResp['data'] != null) {
              for (let index = 0; index < getUserResp['data'].length; index++) {
                if (defaultPhysicianID == getUserResp['data'][index].userCode) {
                  // attendingPhysicianName = getUserResp['data'][index].firstName+' '+getUserResp['data'][index].lastName;
                  if (typeof getUserResp['data'][index].phone != 'undefined') {
                    physicianPhone = getUserResp['data'][index].phone;
                  }
                }
                if (respClient['data'][0]['salesRepresentativeId'] == getUserResp['data'][index].userCode) {
                  if (typeof getUserResp['data'][index].email != 'undefined') {
                    salesRepEmail = getUserResp['data'][index].email;
                  }

                }

              }
            }

            var requestTemp = [];
            var clientIdForStatus = tableFrom[0].item.clientId;
            requestTempHolder['reportPackageId']  = reportPackageId;
            requestTempHolder['reportTemplateId']  =reportTemplate;
            requestTempHolder['fileTypeId']       =fileTypeId;
            requestTempHolder['reportDeliveryMethodId']    =deliverMethod;
            requestTempHolder['clientName']       =tableFrom[0].item.clientName;
            if (salesRepEmail == '' || typeof salesRepEmail == 'undefined') {
              requestTempHolder['clientEmail']      =this.encryptDecrypt.decryptString(tableFrom[0].item.clientEmail);
            }
            else{
              requestTempHolder['clientEmail']      =this.encryptDecrypt.decryptString(tableFrom[0].item.clientEmail)+','+salesRepEmail;
            }
            // requestTempHolder['clientEmail']      =tableFrom[0].item.clientEmail;
            var myDate = new Date();
            var currentDate = this.datePipe.transform(myDate, 'MM/dd/yyyy');
            var currentTimeStamp = this.datePipe.transform(myDate, 'MM/dd/yyyy HH:mm:ss');
            requestTempHolder['reportDate']       = currentDate;
            requestTempHolder['creationDate']     = currentDate;
            requestTempHolder['intakeId'] = tableFrom[0].item.intakeId;
            var currentZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
            requestTempHolder['timeZoneId'] = currentZone;
            var innerTemp = {};
            for (let i = 0; i < tableFrom.length; i++) {
              innerTemp['sNo'] = i+1;
              // innerTemp['sNo'] = tableFrom[i].index;
              innerTemp['lastName'] = this.encryptDecrypt.decryptString(tableFrom[i].item.patientLastName);
              innerTemp['firstName'] = this.encryptDecrypt.decryptString(tableFrom[i].item.patientFirstName);
              innerTemp['caseNumber'] = tableFrom[i].item.caseNumber;
              innerTemp['caseId'] = tableFrom[i].item.caseId;
              innerTemp['patientDob'] = this.datePipe.transform(tableFrom[i].item.patientDateOfBirth, 'MM/dd/yyyy');

              var specimen = '';
              var spType = tableFrom[i].item.specimenTypeId.split(',')

              for (let j = 0; j < this.allSpecimenTypes.length; j++) {
                // console.log('tableFrom[i].specimenTypeId',tableFrom[i].item.specimenTypeId);

                for (let k = 0; k < spType.length; k++) {
                  if (this.allSpecimenTypes[j].specimenTypeId == spType[k]) {
                    specimen = specimen +' ' + this.allSpecimenTypes[j].specimenTypeName;
                  }

                }
              }
              innerTemp['specimenType'] = specimen;
              if (tableFrom[i].item.interpretationDiagnosis == undefined) {
                innerTemp['result'] = null;
              }
              else{
                innerTemp['result'] = tableFrom[i].item.interpretationDiagnosis;
              }

              if (tableFrom[i].item.jobOrderId == undefined) {
                innerTemp['jobOrderId'] = null;
              }
              else{
                innerTemp['jobOrderId'] = tableFrom[i].item.jobOrderId;
              }

              innerTemp['patientRevisionId'] = tableFrom[i].item.patientRevisionId;
              // innerTemp['intakeId'] = tableFrom[i].item.intakeId;
              if (this.selectedIntakeRecord == null) {
                innerTemp['collectionDate'] = this.datePipe.transform(tableFrom[i].item.caseCollectionDate, 'MM/dd/yyy');

              }
              else{
                innerTemp['collectionDate'] = this.datePipe.transform(this.selectedIntakeRecord['collectionTimestamp'], 'MM/dd/yyy');

              }
              // innerTemp['collectionDate'] = this.datePipe.transform(tableFrom[i].item.caseCollectionDate, 'MM/dd/yyy');
              requestTempHolder.caseResults.push(innerTemp);
              innerTemp={}
              // console.log("requestTempHolder",requestTempHolder);
            }
            // console.log("requestTempHolder",requestTempHolder);

            var consolidatedReportUrl = environment.API_REPORTING_ENDPOINT+'summaryreport'
            var consolidatedRequest   = {...this.consolidatedReportRequest}
            consolidatedRequest.data = requestTempHolder;
            consolidatedRequest.header.userCode = this.logedInUserRoles.userCode;
            consolidatedRequest.header.partnerCode = this.logedInUserRoles.partnerCode;
            consolidatedRequest.header.functionalityCode     = "MRMA-MR";

            console.log("consolidatedRequest",consolidatedRequest);
            // console.log("consolidatedRequest",consolidatedRequest);
            // this.ngxLoader.stop() ; return;
            this.globalService.sendReport(consolidatedReportUrl,consolidatedRequest).subscribe(reportResp=>{
              console.log('reportResp',reportResp);

              // this.notifier.notify( "success", reportResp['result'].description);
              this.ngxLoader.stop()
              if (reportResp['result'].codeType == 'S') {
                const downloadLink = document.createElement("a");

                var fileName="";
                var fi = reportResp['data']['fileName'].split('/');
                var le = fi.length-1;
                fileName = fi[le];
                // if(reportPackageId == 1){
                //   fileName = "report.zip";
                // }
                // else{
                //   fileName = "report.pdf";
                // }

                downloadLink.href = reportResp['data']['base64EncodedFile'];
                downloadLink.download = fileName;
                //downloadLink.click();
                setTimeout(()=>{
                  this.notifier.getConfig().behaviour.autoHide = 5000;
                  // this.notifier.notify( "success", "The system is generating the report, the report shall be sent to the client, once it is ready.");
                  this.notifier.notify( "success", "The Detailed report has been delivered to "+this.clientSearchString.clientName+" successfully");

                  this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
                    dtElement.dtInstance.then((dtInstance: any) => {
                      // dtInstance.destroy();
                      dtInstance.ajax.reload();
                    })
                  })
                  this.clearCheckBoxes();

                  if (tableType == 'pending') {
                    for (let i = 0; i < this.pendingReport.length; i++) {
                      this.pendingReport[i].checkBox = false;
                    }
                    this.listForconsolidatedReport = [];
                    $('#check-all1')[0].checked = false;
                  }
                  else if (tableType == 'delivered') {
                    for (let i = 0; i < this.deliveredReports.length; i++) {
                      this.deliveredReports[i].checkBox = false;
                    }
                    this.listForconsolidatedReportDelivered = [];
                    $('#check-all2')[0].checked = false;
                  }
                  else if (tableType == 'notdelivered') {
                    for (let i = 0; i < this.notDeliveredReports.length; i++) {
                      this.notDeliveredReports[i].checkBox = false;
                    }
                    this.listForconsolidatedReportnotDelivered = [];
                    $('#check-all3')[0].checked = false;
                  }
                }, 500);

              }
              else{
                this.ngxLoader.stop()
                this.notifier.notify( "error", "Error while sending report" );
              }
            },error=>{
              this.ngxLoader.stop()
              this.notifier.notify( "error", "Error while sending report check Database connection" );
            })
          },error=>{
            this.ngxLoader.stop()
            this.notifier.notify( "error", "Error while getting attending physicain contacts" );
          })


        }
      },error=>{
        this.ngxLoader.stop()
        this.notifier.notify( "error", "EError while getting attending physicain" );
      })



    }

    showHistory(deliveryStatusId,jobOrderId){
      if (jobOrderId == undefined) {
        alert('jober order id not defined')
        return;
      }
      var historyUrl = environment.API_REPORTING_ENDPOINT+'showreporthistory';
      var reportingRequest = {...this.showHistoryRequest}
      reportingRequest.header.userCode    = this.logedInUserRoles.userCode;
      reportingRequest.header.partnerCode = this.logedInUserRoles.partnerCode;
      reportingRequest.header.functionalityCode = "MRMA-MR";

      reportingRequest.data.deliveryStatusId = deliveryStatusId;
      reportingRequest.data.jobOrderId = jobOrderId;
      // console.log('reportingRequest',reportingRequest);

      // return;
      this.globalService.showHistory(historyUrl,reportingRequest).subscribe(historyResp =>{
        console.log('historyResp',historyResp);
        // console.log('historyResp[sult.codeType',historyResp['result'].codeType);
        if (historyResp['result'].codeType == "S") {


          const uniqueUserIds = [];
          const map = new Map();
          for (const item of historyResp['data']) {
            if(!map.has(item.sentBy)){
              map.set(item.sentBy, true);    // set any value to Map
              uniqueUserIds.push(item.sentBy);
            }
          }
          console.log('uniqueUserIds',uniqueUserIds);

          var userRequest = {...this.usersRequest};
          userRequest.data.userCodes = uniqueUserIds;
          userRequest.header.partnerCode  = this.logedInUserRoles.partnerCode;
          userRequest.header.userCode     = this.logedInUserRoles.userCode;
          userRequest.header.functionalityCode     = "CLTA-VC";
          var userUrl = environment.API_USER_ENDPOINT + "clientusers";
          this.globalService.getUserRecord(userUrl,userRequest).subscribe(getUserResp =>{
            // return getUserResp;
            console.log("getUserResp",getUserResp);

            if (getUserResp['data'] != null) {
              for (let i = 0; i < historyResp['data'].length; i++) {
                for (let j = 0; j < getUserResp['data'].length; j++) {
                  if(historyResp['data'][i].sentBy == getUserResp['data'][j].userCode){
                    historyResp['data'][i].sentByName = getUserResp['data'][j].fullName;
                  }
                  else if(historyResp['data'][i].sentBy != getUserResp['data'][j].userCode){
                    if (typeof historyResp['data'][i].sentByName == 'undefined') {
                      historyResp['data'][i].sentByName = "No Name";
                    }
                  }
                }

                for (let k = 0; k < this.reportPackages.length; k++) {
                  if (this.reportPackages[k].reportPackageId == historyResp['data'][i].reportPackageId) {
                    historyResp['data'][i].reportPackageName = this.reportPackages[k].reportPackageName
                  }

                }
                if (historyResp['data'][i].deliveryStatusId == 3) {
                  historyResp['data'][i].deliveryStatusName = "Not Sent"
                }
                else if (historyResp['data'][i].deliveryStatusId == 2) {
                  historyResp['data'][i].deliveryStatusName = "sent"
                }
                else{
                  historyResp['data'][i].deliveryStatusName = "pending"
                }
                // for (let l = 0; l < this.deliverStatus.length; l++) {
                //   if (this.deliverStatus[l].deliveryStatusId == historyResp['data'][i].deliveryStatusId) {
                //     historyResp['data'][i].deliveryStatusName = this.deliverStatus[l].deliveryStatusName
                //   }
                //
                // }
                this.allHistoryData =   historyResp['data'];
                console.log('this.allHistoryData',historyResp['data']);

                $('#delivery-historyModal').modal('show');

              }
            }
          },error=>{
            this.ngxLoader.stop();
            this.notifier.notify("error","error While loading sent by")
          })
        }
        else{
          this.ngxLoader.stop()
          this.notifier.notify( "error", "Error while loading history" );
        }

      },error=>{
        this.ngxLoader.stop()
        this.notifier.notify( "error", "Error while loading history" );
      })
    }

    detailedReport(reportPackageId,reportTemplate,fileTypeId,deliverMethod,tableType){
      this.compeleteAddressForReport ='';
      var tableFrom;
      if (tableType == 'pending') {
        tableFrom = this.listForconsolidatedReport;
      }
      else if (tableType == 'delivered') {
        tableFrom = this.listForconsolidatedReportDelivered;
      }
      else if (tableType == 'notdelivered') {
        tableFrom = this.listForconsolidatedReportnotDelivered;
      }
      // console.log("this.listForconsolidatedReportnotDelivered",this.listForconsolidatedReportnotDelivered);
      console.log("tableFrom",tableFrom);
      if (tableFrom.length == 0) {
        this.ngxLoader.stop()
        alert('please select atleast one case');
        return;
      }
      for (var check = 0; check < tableFrom.length; check++) {
        if (tableFrom[0].item.clientId != tableFrom[check].item.clientId) {
          this.ngxLoader.stop()
          alert('please select casses associated to same client');

          return;
        }
      }

      this.ngxLoader.start()
      var attendingPhysicianName = '';
      var attendingPhysicianPhone ='';
      var defaultPhysicianID     = '';
      var physicianFax     = '';
      var physicianPhone     = '';
      var accountNumber     = '';
      var salesRepEmail     = '';
      var atdPhyUrl   =  environment.API_CLIENT_ENDPOINT + "clientadressinfo";
      var reqTemplate = {
        header:{
          uuid               :"",
          partnerCode        :"",
          userCode           :"",
          referenceNumber    :"",
          systemCode         :"",
          moduleCode         :"CLIM",
          functionalityCode: "CLTA-VC",
          systemHostAddress  :"",
          remoteUserAddress  :"",
          dateTime           :""
        },
        data:{
          clientIds:[tableFrom[0].item.clientId],
          userCodes: []
        }
      }
      reqTemplate.header.partnerCode  = this.logedInUserRoles.partnerCode;
      reqTemplate.header.userCode     = this.logedInUserRoles.userCode;
      reqTemplate.header.functionalityCode     = "CLTA-VC";
      // var reqTemplate = {clientIds:[clientId]}
      this.globalService.getClientByIds(atdPhyUrl,reqTemplate).subscribe(respClient => {
        console.log("respClient",respClient);
        if (respClient['statusCode'] == "Internal Server Error") {
          // alert('No Physician Associated to this Client')
          this.ngxLoader.stop();
          this.notifier.notify("error","No response from server please check your connection")
          return;
        }
        if (respClient['data'].length == 0) {
          alert('No record found for this Client')
          this.ngxLoader.stop();
          return;
        }
        else {
          var completeAddress = this.composeAddress(respClient['data'][0]['addressDto']['countryId'],respClient['data'][0]['addressDto']['stateId'],respClient['data'][0]['addressDto']['city'],respClient['data'][0]['addressDto']['street'],respClient['data'][0]['addressDto']['suiteFloorBuilding'],respClient['data'][0]['addressDto']['zip'])
          // console.log('completeAddress',completeAddress);

          var physCount = 0; /////// if there is no default physician then slect 1s as physician
          var arrCount  = 0;
          accountNumber = respClient['data'][0]['accountNumber'];
          defaultPhysicianID = respClient['data'][0]['defaultAttendingPhysician'];
          // physicianFax       = this.encryptDecrypt.decryptString(respClient['data'][0]['physicianFax']);
          if (respClient['data'][0]['physicianFax'] != null && respClient['data'][0]['physicianFax'] != '') {
            physicianFax       = this.encryptDecrypt.decryptString(respClient['data'][0]['physicianFax']);
          }

          // for (let i = 0; i < respClient['data'][0]['clientUsers'].length; i++) {
          //
          //   if (respClient['data'][0]['clientUsers'][i]['defaultPhysician'] == 1) {
          //     // attendingPhysicianName = respClient['data'][0]['clientUsers'][i]['userCode'];
          //     defaultPhysicianID = respClient['data'][0]['clientUsers'][i]['userCode'];
          //     for (let n = 0; n < respClient['data'][0]['clientUserContactsByClientId'].length; n++) {
          //       if (respClient['data'][0]['clientUserContactsByClientId'][n]['userCode'] == respClient['data'][0]['clientUsers'][i]['userCode']) {
          //         if (respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['contactTypeId'] == 3) {
          //           physicianFax = this.encryptDecrypt.decryptString(respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['value'])
          //         }
          //         // else if (respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['contactTypeId'] == 1) {
          //         //   // physicianPhone = respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['value']
          //         // }
          //       }
          //
          //     }
          //
          //     physCount ++;
          //   }
          //   arrCount = arrCount+ 1;
          //   if (arrCount == respClient['data'][0]['clientUsers'].length) {
          //     if (physCount == 0) {
          //       // attendingPhysicianName = respClient['data'][0]['clientUsers'][0]['userCode'];
          //       defaultPhysicianID = respClient['data'][0]['clientUsers'][0]['userCode'];
          //     }
          //   }
          //
          // }
        }
        console.log("defaultPhysicianID",defaultPhysicianID);
        var userRequest = {...this.usersRequest};
        userRequest.data.userCodes = [defaultPhysicianID,respClient['data'][0]['salesRepresentativeId']];
        var userUrl = environment.API_USER_ENDPOINT + "getusersbyusercodes";
        this.globalService.getUserRecord(userUrl,userRequest).subscribe(getUserResp =>{
          // return getUserResp;
          // console.log("getUserResp 222222222",getUserResp);
          if (getUserResp['data'] != null) {
            // console.log("innnnnnn---2222",respClient['data'][0]['salesRepresentativeId']);

            for (let index = 0; index < getUserResp['data'].length; index++) {


              if (defaultPhysicianID == getUserResp['data'][index].userCode) {
                attendingPhysicianName = getUserResp['data'][index].firstName+' '+getUserResp['data'][index].lastName;
                if (typeof getUserResp['data'][index].phone != 'undefined') {
                  physicianPhone = getUserResp['data'][index].phone;
                }
              }
              if (respClient['data'][0]['salesRepresentativeId'] == getUserResp['data'][index].userCode) {
                // console.log("getUserResp['data'][index].email---22222",getUserResp['data'][index].email);
                if (typeof getUserResp['data'][index].email != 'undefined') {
                  salesRepEmail = getUserResp['data'][index].email;
                }

              }

            }
            var myDate = new Date();
            var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy');
            var currentTimeStamp = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');
            // return;
            var requestTempHolder = {
              reportData:[]
            };
            var requestTemp = [];
            requestTempHolder['reportPackageId']  = reportPackageId;
            requestTempHolder['reportTemplateId']  =reportTemplate;
            requestTempHolder['fileTypeId']       =fileTypeId;
            requestTempHolder['reportDeliveryMethodId']    =deliverMethod;
            requestTempHolder['clientName']       =tableFrom[0].item.clientName;
            // requestTempHolder['clientEmail']      =tableFrom[0].item.clientEmail;
            if (salesRepEmail == '' || typeof salesRepEmail == 'undefined') {
              requestTempHolder['clientEmail']      =this.encryptDecrypt.decryptString(tableFrom[0].item.clientEmail);
            }
            else{
              requestTempHolder['clientEmail']      =this.encryptDecrypt.decryptString(tableFrom[0].item.clientEmail)+','+salesRepEmail;
            }
            var currentDate = this.datePipe.transform(myDate, 'MM/dd/yyyy');
            var currentTimeStamp = this.datePipe.transform(myDate, 'MM/dd/yyyy HH:mm:ss');
            requestTempHolder['reportDate']       = currentDate;
            requestTempHolder['creationDate']     = currentDate;
            requestTempHolder['intakeId'] = tableFrom[0].item.intakeId;
            var currentZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
            requestTempHolder['timeZoneId'] = currentZone;
            var innerTemp = {};
            for (let i = 0; i < tableFrom.length; i++) {
              // innerTemp['sNo'] = tableFrom[i].index;
              // innerTemp['lastName'] = tableFrom[i].item.patientLastName;
              // innerTemp['firstName'] = tableFrom[i].item.patientFirstName;
              innerTemp['caseNumber'] = tableFrom[i].item.caseNumber;
              innerTemp['caseId'] = tableFrom[i].item.caseId;
              var specimen = '';
              var spType = tableFrom[i].item.specimenTypeId.split(',')

              for (let j = 0; j < this.allSpecimenTypes.length; j++) {
                // console.log('tableFrom[i].specimenTypeId',tableFrom[i].item.specimenTypeId);

                for (let k = 0; k < spType.length; k++) {
                  if (this.allSpecimenTypes[j].specimenTypeId == spType[k]) {
                    specimen = specimen +' ' + this.allSpecimenTypes[j].specimenTypeName;
                  }

                }
              }
              innerTemp['diagnosis']   = specimen;
              if (tableFrom[i].item.interpretationDiagnosis == undefined) {
                innerTemp['diagnosis'] = innerTemp['diagnosis'] +': '+ null;
              }
              else{
                innerTemp['diagnosis'] = innerTemp['diagnosis'] + tableFrom[i].item.interpretationDiagnosis;
              }

              if (tableFrom[i].item.jobOrderId == undefined) {
                innerTemp['jobOrderId'] = null;
              }
              else{
                innerTemp['jobOrderId'] = tableFrom[i].item.jobOrderId;
              }

              innerTemp['patientRevisionId'] = tableFrom[i].item.patientRevisionId;
              if (this.selectedIntakeRecord == null) {
                innerTemp['collectedDate'] = this.datePipe.transform(tableFrom[i].item.caseCollectionDate, 'MM/dd/yyy');

              }
              else{
                innerTemp['collectedDate'] = this.datePipe.transform(this.selectedIntakeRecord['collectionTimestamp'], 'MM/dd/yyy');

              }
              // innerTemp['collectedDate'] = this.datePipe.transform(tableFrom[i].item.caseCollectionDate, 'MM/dd/yyy');
              var currentDate = this.datePipe.transform(myDate, 'MM/dd/yyyy');
              var currentTimeStamp = this.datePipe.transform(myDate, 'MM/dd/yyyy HH:mm:ss');
              // innerTemp['sentDate'] = currentDate; ////////////// sent date
              innerTemp['sentDate'] = this.datePipe.transform(tableFrom[i].item.sentDate, 'MM/dd/yyyy');; ////////////// sent date
              innerTemp['submittedDate'] = currentDate;////// submitted date
              innerTemp['patientName'] = this.encryptDecrypt.decryptString(tableFrom[i].item.patientLastName)+", "+this.encryptDecrypt.decryptString(tableFrom[i].item.patientFirstName);
              innerTemp['patientDob'] = this.datePipe.transform(tableFrom[i].item.patientDateOfBirth, 'MM/dd/yyyy');
              // innerTemp['patientId'] = this.encryptDecrypt.decryptString(tableFrom[i].item.patientSsn);
              if (typeof tableFrom[i].patientSsn !='undefined') {
                if (tableFrom[i].patientSsn !='') {
                  innerTemp['patientId'] = this.encryptDecrypt.decryptString(tableFrom[i].item.patientSsn);
                }
                else{
                  innerTemp['patientId'] = '';
                }
              }
              else{
                innerTemp['patientId'] = '';
              }
              // innerTemp['intakeId'] = tableFrom[i].item.intakeId;
              if (typeof tableFrom[i].item.patientGender == 'undefined') {
                innerTemp['patientSex'] = '';
              }
              else{
                if (tableFrom[i].item.patientGender == 'M' || tableFrom[i].item.patientGender == "m") {
                  innerTemp['patientSex'] = "Male";

                }
                else if (tableFrom[i].item.patientGender == 'F' || tableFrom[i].item.patientGender == "f") {
                  innerTemp['patientSex'] = "Female";
                }
                else{
                  innerTemp['patientSex'] = "Unspecified";
                }
              }

              if (typeof tableFrom[i].item.phoneNumber == 'undefined') {
                innerTemp['patientTell'] = ''
              }
              else{
                innerTemp['patientTell'] = this.encryptDecrypt.decryptString(tableFrom[i].item.phoneNumber);
              }

              innerTemp['clientName'] = tableFrom[0].item.clientName;
              innerTemp['accountNumber'] = accountNumber;
              innerTemp['attendingPhysicianName'] = attendingPhysicianName;
              innerTemp['physicianTel'] = physicianPhone;
              innerTemp['physicianFax'] = physicianFax;
              innerTemp['address'] = this.compeleteAddressForReport; ///////// whose address?
              innerTemp['comment'] = tableFrom[i].item.interpretationComment;
              requestTempHolder.reportData.push(innerTemp);
              innerTemp={}
              // console.log("requestTempHolder",requestTempHolder);
            }
            // console.log("requestTempHolder",requestTempHolder);



            var consolidatedReportUrl = environment.API_REPORTING_ENDPOINT+'detailedreport'
            var consolidatedRequest   = {...this.consolidatedReportRequest}
            consolidatedRequest.data = requestTempHolder;
            consolidatedRequest.header.userCode = this.logedInUserRoles.userCode;
            consolidatedRequest.header.partnerCode = this.logedInUserRoles.partnerCode;
            consolidatedRequest.header.functionalityCode = "MRMA-MR";

            console.log("consolidatedRequest",consolidatedRequest);

            // this.ngxLoader.stop();return;
            this.globalService.sendReport(consolidatedReportUrl,consolidatedRequest).subscribe(reportResp=>{
              console.log('reportResp',reportResp);
              this.ngxLoader.stop()
              if (reportResp['result'].codeType == 'S') {

                setTimeout(()=>{
                  this.notifier.getConfig().behaviour.autoHide = 5000;
                  this.notifier.notify( "success", "The Detailed report has been delivered to "+this.clientSearchString.clientName+" successfully");

                  // this.notifier.notify( "success", "The system is generating the report, the report shall be sent to the client, once it is ready.");


                  ////////// window.open(reportResp['data']['base64EncodedFile'], "_blank");
                  const downloadLink = document.createElement("a");
                  var fileName="";
                  var fi = reportResp['data']['fileName'].split('/');
                  var le = fi.length-1;
                  fileName = fi[le];
                  // if(reportPackageId == 1){
                  //   fileName = "report.zip";
                  // }
                  // else{
                  //   fileName = "report.pdf";
                  // }

                  downloadLink.href = reportResp['data']['base64EncodedFile'];
                  downloadLink.download = fileName;
                  this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
                    dtElement.dtInstance.then((dtInstance: any) => {
                      // dtInstance.destroy();
                      dtInstance.ajax.reload();
                    })
                  })
                  //downloadLink.click();
                  this.clearCheckBoxes();
                  // this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
                  //   dtElement.dtInstance.then((dtInstance: any) => {
                  //     // dtInstance.destroy();
                  //     dtInstance.ajax.reload();
                  //   })
                  // })
                  // $('#successCOnsolidatedRepot').modal('show')
                  // window.open(reportResp['data']['base64EncodedFile'], "_blank");
                  // let pdfWindow = window.open("")
                  // pdfWindow.document.write(
                  //   "<iframe width='100%' height='100%' src='"+encodeURI(reportResp['data']['base64EncodedFile'])+"'></iframe>"
                  // )
                  if (tableType == 'pending') {
                    for (let i = 0; i < this.pendingReport.length; i++) {
                      this.pendingReport[i].checkBox = false;
                    }
                    this.listForconsolidatedReport = [];
                    $('#check-all1')[0].checked = false;
                  }
                  else if (tableType == 'delivered') {
                    for (let i = 0; i < this.deliveredReports.length; i++) {
                      this.deliveredReports[i].checkBox = false;
                      $('#check-all2')[0].checked = false;
                    }
                    this.listForconsolidatedReportDelivered = [];
                  }
                  else if (tableType == 'notdelivered') {
                    for (let i = 0; i < this.notDeliveredReports.length; i++) {
                      this.notDeliveredReports[i].checkBox = false;
                    }
                    this.listForconsolidatedReportnotDelivered = [];
                    $('#check-all3')[0].checked = false;
                  }
                }, 500);




              }
              else{
                this.ngxLoader.stop()
                this.notifier.notify( "error", "Error while sending report" );
              }
            },error=>{
              this.ngxLoader.stop()
              this.notifier.notify( "error", "Error while sending report check Database connection" );
            })
          }
          else{
            this.ngxLoader.stop();
            this.notifier.notify("error","error While loading User Name")
          }
        },error=>{
          this.ngxLoader.stop();
          this.notifier.notify("error","error While loading User Name")
        })
        // this.ngxLoader.stop();
        // return;

      },
      error =>{
        this.ngxLoader.stop();
        this.notifier.notify('error','No response from server please check your connection');
      });



    }


    detailedReportSingle(reportPackageId,reportTemplate,fileTypeId,deliverMethod,tableType,listForconsolidatedReport){
      this.compeleteAddressForReport ='';
      // console.log('tableType',tableType);


      var tableFrom = [];
      if (tableType == 'pending') {
        tableFrom.push(listForconsolidatedReport);
      }
      else if (tableType == 'delivered') {
        tableFrom.push(listForconsolidatedReport);
      }
      else if (tableType == 'notdelivered') {
        tableFrom.push(listForconsolidatedReport);
      }
      console.log("tableFrom",tableFrom);
      // console.log("tableFrom",tableFrom.length);
      //   return;
      // if (tableFrom.length == 0) {
      //   this.ngxLoader.stop()
      //   alert('please select atleast one case');
      //   return;
      // }
      // for (var check = 0; check < tableFrom.length; check++) {
      //   if (tableFrom[0].item.clientId != tableFrom[check].item.clientId) {
      //     this.ngxLoader.stop()
      //     alert('please select casses associated to same client');
      //
      //     return;
      //   }
      // }

      this.ngxLoader.start()
      var attendingPhysicianName = '';
      var attendingPhysicianPhone ='';
      var defaultPhysicianID     = '';
      var physicianFax     = '';
      var physicianPhone     = '';
      var accountNumber     = '';
      var salesRepEmail     = '';
      var atdPhyUrl   =  environment.API_CLIENT_ENDPOINT + "clientadressinfo";
      var reqTemplate = {
        header:{
          uuid               :"",
          partnerCode        :"",
          userCode           :"",
          referenceNumber    :"",
          systemCode         :"",
          moduleCode         :"CLIM",
          functionalityCode: "CLTA-VC",
          systemHostAddress  :"",
          remoteUserAddress  :"",
          dateTime           :""
        },
        data:{
          clientIds:[tableFrom[0].clientId]
        }
      }
      reqTemplate.header.partnerCode  = this.logedInUserRoles.partnerCode;
      reqTemplate.header.userCode     = this.logedInUserRoles.userCode;
      reqTemplate.header.functionalityCode     = "CLTA-VC";
      // var reqTemplate = {clientIds:[clientId]}
      this.globalService.getClientByIds(atdPhyUrl,reqTemplate).subscribe(respClient => {
        console.log("respClient",respClient);
        if (respClient['statusCode'] == "Internal Server Error") {
          // alert('No Physician Associated to this Client')
          this.ngxLoader.stop();
          this.notifier.notify("error","No response from server please check your connection")
          return;
        }
        if (respClient['data'].length == 0) {
          alert('No record found for this Client')
          this.ngxLoader.stop();
          return;
        }
        else {
          var completeAddress = this.composeAddress(respClient['data'][0]['addressDto']['countryId'],respClient['data'][0]['addressDto']['stateId'],respClient['data'][0]['addressDto']['city'],respClient['data'][0]['addressDto']['street'],respClient['data'][0]['addressDto']['suiteFloorBuilding'],respClient['data'][0]['addressDto']['zip'])
          // console.log('completeAddress',completeAddress);
          var physCount = 0; /////// if there is no default physician then slect 1s as physician
          var arrCount  = 0;
          accountNumber = respClient['data'][0]['accountNumber'];
          defaultPhysicianID = respClient['data'][0]['defaultAttendingPhysician'];
          if (respClient['data'][0]['physicianFax'] != null && respClient['data'][0]['physicianFax'] != '') {
            physicianFax       = this.encryptDecrypt.decryptString(respClient['data'][0]['physicianFax']);
          }

          // for (let i = 0; i < respClient['data'][0]['clientUsers'].length; i++) {
          //
          //   if (respClient['data'][0]['clientUsers'][i]['defaultPhysician'] == 1) {
          //     // attendingPhysicianName = respClient['data'][0]['clientUsers'][i]['userCode'];
          //     defaultPhysicianID = respClient['data'][0]['clientUsers'][i]['userCode'];
          //     for (let n = 0; n < respClient['data'][0]['clientUserContactsByClientId'].length; n++) {
          //       if (respClient['data'][0]['clientUserContactsByClientId'][n]['userCode'] == respClient['data'][0]['clientUsers'][i]['userCode']) {
          //         if (respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['contactTypeId'] == 3) {
          //           physicianFax = this.encryptDecrypt.decryptString(respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['value']);
          //         }
          //         // else if (respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['contactTypeId'] == 1) {
          //         //   // physicianPhone = respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['value']
          //         // }
          //         // else if (respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['contactTypeId'] == 3) {
          //         //   // salesRepEmail = respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['value']
          //         // }
          //       }
          //
          //     }
          //
          //     physCount ++;
          //   }
          //   arrCount = arrCount+ 1;
          //   if (arrCount == respClient['data'][0]['clientUsers'].length) {
          //     if (physCount == 0) {
          //       // attendingPhysicianName = respClient['data'][0]['clientUsers'][0]['userCode'];
          //       defaultPhysicianID = respClient['data'][0]['clientUsers'][0]['userCode'];
          //     }
          //   }
          //
          // }
        }
        console.log("defaultPhysicianID",defaultPhysicianID);
        // console.log("salesRepEmail",salesRepEmail);
        var userRequest = {...this.usersRequest};
        userRequest.data.userCodes = [defaultPhysicianID,respClient['data'][0]['salesRepresentativeId']];
        userRequest.header.partnerCode  = this.logedInUserRoles.partnerCode;
        userRequest.header.userCode     = this.logedInUserRoles.userCode;
        userRequest.header.functionalityCode     = "MRMA-MR";
        var userUrl = environment.API_USER_ENDPOINT + "getusersbyusercodes";
        this.globalService.getUserRecord(userUrl,userRequest).subscribe(getUserResp =>{
          // return getUserResp;
          console.log("getUserResp",getUserResp);
          if (getUserResp['data'] != null) {
            for (let index = 0; index < getUserResp['data'].length; index++) {
              if (defaultPhysicianID == getUserResp['data'][index].userCode) {
                attendingPhysicianName = getUserResp['data'][index].firstName+' '+getUserResp['data'][index].lastName;
                if (typeof getUserResp['data'][index].phone != 'undefined') {
                  physicianPhone = getUserResp['data'][index].phone;
                }
              }
              if (respClient['data'][0]['salesRepresentativeId'] == getUserResp['data'][index].userCode) {
                if (typeof getUserResp['data'][index].email != 'undefined') {
                  salesRepEmail = getUserResp['data'][index].email;
                }

              }

            }

            var myDate = new Date();
            var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy');
            var currentTimeStamp = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');
            // return;
            var requestTempHolder = {
              reportData:[]
            };
            var requestTemp = [];
            requestTempHolder['reportPackageId']  = reportPackageId;
            requestTempHolder['reportTemplateId']  =reportTemplate;
            requestTempHolder['fileTypeId']       =fileTypeId;
            requestTempHolder['reportDeliveryMethodId']    =deliverMethod;
            requestTempHolder['clientName']       =tableFrom[0].clientName;
            requestTempHolder['intakeId'] = tableFrom[0].intakeId;
            // requestTempHolder['clientEmail']      =tableFrom[0].clientEmail;
            console.log("salesRepEmail",salesRepEmail);

            if (salesRepEmail == '' || typeof salesRepEmail == 'undefined') {
              requestTempHolder['clientEmail']      =this.encryptDecrypt.decryptString(tableFrom[0].clientEmail);
            }
            else{
              requestTempHolder['clientEmail']      =this.encryptDecrypt.decryptString(tableFrom[0].clientEmail)+','+salesRepEmail;
            }
            var currentDate = this.datePipe.transform(myDate, 'MM/dd/yyyy');
            var currentTimeStamp = this.datePipe.transform(myDate, 'MM/dd/yyyy HH:mm:ss');
            requestTempHolder['reportDate']       = currentDate;
            requestTempHolder['creationDate']     = currentDate;
            var currentZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
            requestTempHolder['timeZoneId'] = currentZone;
            var innerTemp = {};
            for (let i = 0; i < tableFrom.length; i++) {
              // innerTemp['sNo'] = tableFrom[i].index;
              // innerTemp['lastName'] = tableFrom[i].patientLastName;
              // innerTemp['firstName'] = tableFrom[i].patientFirstName;
              innerTemp['caseNumber'] = tableFrom[i].caseNumber;
              innerTemp['caseId'] = tableFrom[i].caseId;
              var specimen = '';
              var spType = tableFrom[i].specimenTypeId.split(',')
              for (let j = 0; j < this.allSpecimenTypes.length; j++) {
                // console.log('tableFrom[i].specimenTypeId',tableFrom[i].specimenTypeId);

                for (let k = 0; k < spType.length; k++) {
                  if (this.allSpecimenTypes[j].specimenTypeId == spType[k]) {
                    specimen = specimen +' ' + this.allSpecimenTypes[j].specimenTypeName;
                  }

                }
              }
              // for (let j = 0; j < this.allSpecimenTypes.length; j++) {
              //
              //   for (let k = 0; k < tableFrom[i].specimenTypeId.length; k++) {
              //     if (this.allSpecimenTypes[j].specimenTypeId == tableFrom[i].specimenTypeId[k]) {
              //       specimen = specimen +' ' + this.allSpecimenTypes[j].specimenTypeName;
              //     }
              //
              //   }
              // }
              innerTemp['diagnosis']   = specimen;
              if (tableFrom[i].interpretationDiagnosis == undefined) {
                innerTemp['diagnosis'] = innerTemp['diagnosis'] +': '+ null;
              }
              else{
                innerTemp['diagnosis'] = innerTemp['diagnosis'] + tableFrom[i].interpretationDiagnosis;
              }

              if (tableFrom[i].jobOrderId == undefined) {
                innerTemp['jobOrderId'] = null;
              }
              else{
                innerTemp['jobOrderId'] = tableFrom[i].jobOrderId;
              }

              innerTemp['patientRevisionId'] = tableFrom[i].patientRevisionId;
              // if (this.selectedIntakeRecord == null) {
              //   innerTemp['collectedDate'] = this.datePipe.transform(tableFrom[i].caseCollectionDate, 'MM/dd/yyy');
              //
              // }
              // else{
              //   innerTemp['collectedDate'] = this.datePipe.transform(this.selectedIntakeRecord['collectionTimestamp'], 'MM/dd/yyy');
              //
              // }
              innerTemp['collectedDate'] = this.datePipe.transform(tableFrom[i].caseCollectionDate, 'MM/dd/yyy');
              var currentDate = this.datePipe.transform(myDate, 'MM/dd/yyyy');
              var currentTimeStamp = this.datePipe.transform(myDate, 'MM/dd/yyyy HH:mm:ss');
              // innerTemp['sentDate'] = currentDate; ////////////// sent date
              innerTemp['sentDate'] = this.datePipe.transform(tableFrom[i].sentDate, 'MM/dd/yyyy');; ////////////// sent date
              innerTemp['submittedDate'] = currentDate;////// submitted date
              innerTemp['patientName'] = this.encryptDecrypt.decryptString(tableFrom[i].patientLastName)+", "+this.encryptDecrypt.decryptString(tableFrom[i].patientFirstName);
              innerTemp['patientDob'] = this.datePipe.transform(tableFrom[i].patientDateOfBirth, 'MM/dd/yyyy');
              if (typeof tableFrom[i].patientSsn !='undefined') {
                if (tableFrom[i].patientSsn !='') {
                  innerTemp['patientId'] = this.encryptDecrypt.decryptString(tableFrom[i].patientSsn);
                }
                else{
                  innerTemp['patientId'] = '';
                }
              }
              else{
                innerTemp['patientId'] = '';
              }
              innerTemp['receivingDate'] = this.datePipe.transform(tableFrom[i].receivingDate, 'MM/dd/yyy');
              // innerTemp['receivingDate'] = tableFrom[i].receivingDate;
              // innerTemp['intakeId'] = tableFrom[i].intakeId;
              if (typeof tableFrom[i].patientGender == 'undefined') {
                innerTemp['patientSex'] = '';
              }
              else{
                if (tableFrom[i].patientGender == 'M' || tableFrom[i].patientGender == "m") {
                  innerTemp['patientSex'] = "Male";

                }
                else if (tableFrom[i].patientGender == 'F' || tableFrom[i].patientGender == "f") {
                  innerTemp['patientSex'] = "Female";
                }
                else{
                  innerTemp['patientSex'] = "Unspecified";
                }
                // innerTemp['patientSex'] = tableFrom[i].patientGender;
              }

              if (typeof tableFrom[i].phoneNumber == 'undefined') {
                innerTemp['patientTell'] = ''
              }
              else{
                innerTemp['patientTell'] = this.encryptDecrypt.decryptString(tableFrom[i].phoneNumber);
              }

              innerTemp['clientName'] = tableFrom[0].clientName;
              innerTemp['accountNumber'] = accountNumber;
              innerTemp['attendingPhysicianName'] = attendingPhysicianName;
              innerTemp['physicianTel'] = physicianPhone;
              innerTemp['physicianFax'] = physicianFax;
              innerTemp['address'] = this.compeleteAddressForReport; ///////// whose address?
              innerTemp['comment'] = tableFrom[i].interpretationComment;
              requestTempHolder.reportData.push(innerTemp);
              innerTemp={}
              // console.log("requestTempHolder",requestTempHolder);
            }
            console.log("requestTempHolder",requestTempHolder);




            var consolidatedReportUrl = environment.API_REPORTING_ENDPOINT+'detailedreport'
            var consolidatedRequest   = {...this.consolidatedReportRequest}
            consolidatedRequest.data = requestTempHolder;
            consolidatedRequest.header.userCode = this.logedInUserRoles.userCode;
            consolidatedRequest.header.partnerCode = this.logedInUserRoles.partnerCode;
            consolidatedRequest.header.functionalityCode = "MRMA-MR";

            console.log("consolidatedRequest",consolidatedRequest);

            // this.ngxLoader.stop();return;
            this.globalService.sendReport(consolidatedReportUrl,consolidatedRequest).subscribe(reportResp=>{
              console.log('reportResp',reportResp);
              this.ngxLoader.stop()
              if (reportResp['result'].codeType == 'S') {

                setTimeout(()=>{
                  this.notifier.getConfig().behaviour.autoHide = 5000;
                  this.notifier.notify( "success", "The Detailed report has been delivered to "+this.clientSearchString.clientName+" successfully");

                  // this.notifier.notify( "success", "The system is generating the report, the report shall be sent to the client, once it is ready.");
                  ////// this.notifier.notify( "error", "Error while sending report" );
                  ////////this.notifier.notify( "success", reportResp['result'].description);
                  //   var deliverStatus = 2;
                  //   var caseStatus    = 10;
                  //   var testStatus    = 5;
                  //   if (reportResp['data'].reportDeliverStatus == 2) {
                  //     deliverStatus = 2; //////// sent
                  //     caseStatus    = 10;
                  //     testStatus    = 5;
                  //   }
                  //   else if (reportResp['data'].reportDeliverStatus == 3){
                  //     deliverStatus = 3; //////// not sent\
                  //     caseStatus    = 12;
                  //     testStatus    = 8;
                  //   }
                  //   /////this.successFileURL = this.sanitizer.bypassSecurityTrustUrl(reportResp['data']['base64EncodedFile']);
                  //   ///// this.notifier.notify( "error", "Error while sending report" );
                  //   ////// this.notifier.notify( "success", reportResp['result'].description);
                  //   ////// window.open(reportResp['data']['base64EncodedFile'], "_blank");
                  //   var uniqueCaseIDs = []
                  //   const map = new Map();
                  //   for (const item of tableFrom) {
                  //     if(!map.has(item.caseId)){
                  //       map.set(item.caseId, true);    // set any value to Map
                  //       uniqueCaseIDs.push(item.caseId);
                  //     }
                  //   }
                  //   var uniquJobOrderIDs = []
                  //   const map2 = new Map();
                  //
                  //   for (const item of tableFrom) {
                  //     if(!map2.has(item.jobOrderId)){
                  //       map2.set(item.jobOrderId, true);    // set any value to Map
                  //       uniquJobOrderIDs.push(item.jobOrderId);
                  //     }
                  //   }
                  //   var specimenurl = environment.API_SPECIMEN_ENDPOINT + 'updatecasestatusin';
                  //
                  //   var specimenreqBody={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "MRMA-MR",systemHostAddress:"",remoteUserAddress:"",dateTime:""},
                  //   data: {
                  //     caseStatusId:caseStatus,
                  //     caseIds:uniqueCaseIDs,
                  //     updatedBy: this.logedInUserRoles.userCode,
                  //     updatedTimestamp:currentTimeStamp
                  //
                  //   }
                  // }
                  // this.globalService.updateCaseStatus(specimenurl,specimenreqBody).subscribe(specimenReps=>{
                  //   if (specimenReps['data']>0) {
                  //
                  //   }
                  //   else{
                  //     this.notifier.notify("warning","report generated successfully but there was an error while updating case statuses");
                  //     this.ngxLoader.stop();
                  //
                  //   }
                  // },error=>{
                  //   this.notifier.notify("warning","report generated successfully but there was an error while updating case statuses");
                  //   this.ngxLoader.stop();
                  //
                  // })
                  //
                  // var covidurl = environment.API_COVID_ENDPOINT + 'updateteststatus';
                  // var covidRequest = {...this.updateCaseStatusReq}
                  // covidRequest.data.jobOrderId   = uniquJobOrderIDs;
                  // covidRequest.data.testStatusId = testStatus;
                  // covidRequest.data.updatedBy = this.logedInUserRoles.userCode;
                  // covidRequest.data.updatedTimestamp = currentTimeStamp;
                  // this.globalService.updateTestStatus(covidurl,covidRequest).subscribe(covidReps=>{
                  //   if (covidReps['data']>0) {
                  //
                  //   }
                  //   else{
                  //     this.notifier.notify("warning","report generated successfully but there was an error while updating test statuses");
                  //     this.ngxLoader.stop();
                  //
                  //   }
                  // },error=>{
                  //   this.notifier.notify("warning","report generated successfully but there was an error while updating test statuses");
                  //   this.ngxLoader.stop();
                  //
                  // })

                  const downloadLink = document.createElement("a");
                  var fileName="";
                  // if(reportPackageId == 1){
                  //   fileName = "report.zip";
                  // }
                  // else{
                  //   fileName = "report.pdf";
                  // }
                  var fi = reportResp['data']['fileName'].split('/');
                  var le = fi.length-1;
                  fileName = fi[le];


                  downloadLink.href = reportResp['data']['base64EncodedFile'];
                  downloadLink.download = fileName;
                  this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
                    dtElement.dtInstance.then((dtInstance: any) => {
                      // dtInstance.destroy();
                      dtInstance.ajax.reload();
                    })
                  })
                  //downloadLink.click();
                  this.clearCheckBoxes();


                }, 500);

              }
              else{
                this.ngxLoader.stop()
                this.notifier.notify( "error", "Error while sending report" );
              }
            },error=>{
              this.ngxLoader.stop()
              this.notifier.notify( "error", "Error while sending report check Database connection" );
            })
          }
          else{
            this.ngxLoader.stop();
            this.notifier.notify("error","error While loading User Name")
          }
        },error=>{
          this.ngxLoader.stop();
          this.notifier.notify("error","error While loading User Name")
        })
        // this.ngxLoader.stop();
        // return;

      },
      error =>{
        this.ngxLoader.stop()
        this.notifier.notify('error','No response from server please check your connection');
      });

    }
    downloadReport(reportPackageId,reportTemplate,fileTypeId,deliverMethod,tableType,listForconsolidatedReport){
      this.compeleteAddressForReport ='';
      // console.log('tableType',tableType);


      var tableFrom = [];
      if (tableType == 'pending') {
        tableFrom.push(listForconsolidatedReport);
      }
      else if (tableType == 'delivered') {
        tableFrom.push(listForconsolidatedReport);
      }
      else if (tableType == 'notdelivered') {
        tableFrom.push(listForconsolidatedReport);
      }
      console.log("tableFrom",tableFrom);
      // console.log("tableFrom",tableFrom.length);
      //   return;
      // if (tableFrom.length == 0) {
      //   this.ngxLoader.stop()
      //   alert('please select atleast one case');
      //   return;
      // }
      // for (var check = 0; check < tableFrom.length; check++) {
      //   if (tableFrom[0].item.clientId != tableFrom[check].item.clientId) {
      //     this.ngxLoader.stop()
      //     alert('please select casses associated to same client');
      //
      //     return;
      //   }
      // }

      this.ngxLoader.start()
      var attendingPhysicianName = '';
      var attendingPhysicianPhone ='';
      var defaultPhysicianID     = '';
      var physicianFax     = '';
      var physicianPhone     = '';
      var accountNumber     = '';
      var salesRepEmail     = '';
      var atdPhyUrl   =  environment.API_CLIENT_ENDPOINT + "clientadressinfo";
      var reqTemplate = {
        header:{
          uuid               :"",
          partnerCode        :"",
          userCode           :"",
          referenceNumber    :"",
          systemCode         :"",
          moduleCode         :"CLIM",
          functionalityCode: "CLTA-VC",
          systemHostAddress  :"",
          remoteUserAddress  :"",
          dateTime           :""
        },
        data:{
          clientIds:[tableFrom[0].clientId],
          userCodes: []
        }
      }
      reqTemplate.header.partnerCode  = this.logedInUserRoles.partnerCode;
      reqTemplate.header.userCode     = this.logedInUserRoles.userCode;
      reqTemplate.header.functionalityCode     = "CLTA-VC";
      // var reqTemplate = {clientIds:[clientId]}
      this.globalService.getClientByIds(atdPhyUrl,reqTemplate).subscribe(respClient => {
        console.log("respClient",respClient);
        if (respClient['statusCode'] == "Internal Server Error") {
          // alert('No Physician Associated to this Client')
          this.ngxLoader.stop();
          this.notifier.notify("error","No response from server please check your connection")
          return;
        }
        if (respClient['data'].length == 0) {
          alert('No record found for this Client')
          this.ngxLoader.stop();
          return;
        }
        else {
          var completeAddress = this.composeAddress(respClient['data'][0]['addressDto']['countryId'],respClient['data'][0]['addressDto']['stateId'],respClient['data'][0]['addressDto']['city'],respClient['data'][0]['addressDto']['street'],respClient['data'][0]['addressDto']['suiteFloorBuilding'],respClient['data'][0]['addressDto']['zip'])
          // console.log('completeAddress',completeAddress);
          var physCount = 0; /////// if there is no default physician then slect 1s as physician
          var arrCount  = 0;
          accountNumber = respClient['data'][0]['accountNumber'];
          defaultPhysicianID = respClient['data'][0]['defaultAttendingPhysician'];
          if (respClient['data'][0]['physicianFax'] != null && respClient['data'][0]['physicianFax'] != '') {
            physicianFax       = this.encryptDecrypt.decryptString(respClient['data'][0]['physicianFax']);
          }

          // for (let i = 0; i < respClient['data'][0]['clientUsers'].length; i++) {
          //
          //   if (respClient['data'][0]['clientUsers'][i]['defaultPhysician'] == 1) {
          //     // attendingPhysicianName = respClient['data'][0]['clientUsers'][i]['userCode'];
          //     defaultPhysicianID = respClient['data'][0]['clientUsers'][i]['userCode'];
          //     for (let n = 0; n < respClient['data'][0]['clientUserContactsByClientId'].length; n++) {
          //       if (respClient['data'][0]['clientUserContactsByClientId'][n]['userCode'] == respClient['data'][0]['clientUsers'][i]['userCode']) {
          //         if (respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['contactTypeId'] == 3) {
          //           physicianFax = respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['value']
          //         }
          //         // else if (respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['contactTypeId'] == 1) {
          //         //   // physicianPhone = respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['value']
          //         // }
          //         // else if (respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['contactTypeId'] == 3) {
          //         //   // salesRepEmail = respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['value']
          //         // }
          //       }
          //
          //     }
          //
          //     physCount ++;
          //   }
          //   arrCount = arrCount+ 1;
          //   if (arrCount == respClient['data'][0]['clientUsers'].length) {
          //     if (physCount == 0) {
          //       // attendingPhysicianName = respClient['data'][0]['clientUsers'][0]['userCode'];
          //       defaultPhysicianID = respClient['data'][0]['clientUsers'][0]['userCode'];
          //     }
          //   }
          //
          // }
        }
        console.log("defaultPhysicianID",defaultPhysicianID);
        var userRequest = {...this.usersRequest};
        userRequest.header.partnerCode  = this.logedInUserRoles.partnerCode;
        userRequest.header.userCode     = this.logedInUserRoles.userCode;
        userRequest.header.functionalityCode     = "MRMA-MR";
        userRequest.data.userCodes = [defaultPhysicianID,respClient['data'][0]['salesRepresentativeId']];
        var userUrl = environment.API_USER_ENDPOINT + "getusersbyusercodes";
        this.globalService.getUserRecord(userUrl,userRequest).subscribe(getUserResp =>{
          // return getUserResp;
          console.log("getUserResp",getUserResp);
          if (getUserResp['data'] != null) {
            for (let index = 0; index < getUserResp['data'].length; index++) {
              if (defaultPhysicianID == getUserResp['data'][index].userCode) {
                attendingPhysicianName = getUserResp['data'][index].firstName+' '+getUserResp['data'][index].lastName;
                if (typeof getUserResp['data'][index].phone != 'undefined') {
                  physicianPhone = getUserResp['data'][index].phone;
                }
              }
              if (respClient['data'][0]['salesRepresentativeId'] == getUserResp['data'][index].userCode) {
                if (typeof getUserResp['data'][index].email != 'undefined') {
                  salesRepEmail = getUserResp['data'][index].email;
                }

              }

            }

            var myDate = new Date();
            var currentDate = this.datePipe.transform(myDate, 'MM/dd/yyyy');
            var currentTimeStamp = this.datePipe.transform(myDate, 'MM/dd/yyyy HH:mm:ss');
            // return;
            var requestTempHolder = {
              reportData:[]
            };
            var requestTemp = [];
            // requestTempHolder['reportPackageId']  = reportPackageId;
            // requestTempHolder['reportTemplateId']  =reportTemplate;
            // requestTempHolder['fileTypeId']       =fileTypeId;
            // requestTempHolder['reportDeliveryMethodId']    =deliverMethod;
            requestTempHolder['clientName']       =tableFrom[0].clientName;
            if (salesRepEmail == '' || typeof salesRepEmail == 'undefined') {
              requestTempHolder['clientEmail']      =this.encryptDecrypt.decryptString(tableFrom[0].clientEmail);
            }
            else{
              requestTempHolder['clientEmail']      =this.encryptDecrypt.decryptString(tableFrom[0].clientEmail)+','+salesRepEmail;
            }
            // requestTempHolder['clientEmail']      =tableFrom[0].clientEmail;
            var currentDate = this.datePipe.transform(myDate, 'MM/dd/yyyy');
            var currentTimeStamp = this.datePipe.transform(myDate, 'MM/dd/yyyy HH:mm:ss');
            requestTempHolder['reportDate']       = currentDate;
            requestTempHolder['creationDate']     = currentDate;
            var currentZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
            requestTempHolder['timeZoneId'] = currentZone;
            var innerTemp = {};
            for (let i = 0; i < tableFrom.length; i++) {
              // innerTemp['sNo'] = tableFrom[i].index;
              // innerTemp['lastName'] = tableFrom[i].patientLastName;
              // innerTemp['firstName'] = tableFrom[i].patientFirstName;
              innerTemp['caseNumber'] = tableFrom[i].caseNumber;
              innerTemp['caseId'] = tableFrom[i].caseId;
              var specimen = '';
              var spType = tableFrom[i].specimenTypeId.split(',')
              for (let j = 0; j < this.allSpecimenTypes.length; j++) {
                // console.log('tableFrom[i].specimenTypeId',tableFrom[i].specimenTypeId);

                for (let k = 0; k < spType.length; k++) {
                  if (this.allSpecimenTypes[j].specimenTypeId == spType[k]) {
                    specimen = specimen +' ' + this.allSpecimenTypes[j].specimenTypeName;
                  }

                }
              }
              innerTemp['diagnosis']   = specimen;
              if (tableFrom[i].interpretationDiagnosis == undefined) {
                innerTemp['diagnosis'] = innerTemp['diagnosis'] +': '+ null;
              }
              else{
                innerTemp['diagnosis'] = innerTemp['diagnosis'] + tableFrom[i].interpretationDiagnosis;
              }

              if (tableFrom[i].jobOrderId == undefined) {
                innerTemp['jobOrderId'] = null;
              }
              else{
                innerTemp['jobOrderId'] = tableFrom[i].jobOrderId;
              }

              innerTemp['patientRevisionId'] = tableFrom[i].patientRevisionId;
              if (this.selectedIntakeRecord == null) {
                innerTemp['collectedDate'] = this.datePipe.transform(tableFrom[i].caseCollectionDate, 'MM/dd/yyy');

              }
              else{
                innerTemp['collectedDate'] = this.datePipe.transform(this.selectedIntakeRecord['collectionTimestamp'], 'MM/dd/yyy');

              }
              // innerTemp['collectedDate'] = this.datePipe.transform(tableFrom[i].caseCollectionDate, 'MM/dd/yyy');
              var currentDate = this.datePipe.transform(myDate, 'MM/dd/yyyy');
              var currentTimeStamp = this.datePipe.transform(myDate, 'MM/dd/yyyy HH:mm:ss');
              // innerTemp['sentDate'] = currentDate; ////////////// sent date
              innerTemp['sentDate'] = this.datePipe.transform(tableFrom[i].sentDate, 'MM/dd/yyyy');; ////////////// sent date
              innerTemp['submittedDate'] = currentDate;////// submitted date
              innerTemp['patientName'] = this.encryptDecrypt.decryptString(tableFrom[i].patientLastName)+", "+this.encryptDecrypt.decryptString(tableFrom[i].patientFirstName);
              innerTemp['patientDob'] = this.datePipe.transform(tableFrom[i].patientDateOfBirth, 'MM/dd/yyyy');
              // innerTemp['patientId'] = this.encryptDecrypt.decryptString(tableFrom[i].patientSsn);
              if (typeof tableFrom[i].patientSsn !='undefined') {
                if (tableFrom[i].patientSsn !='') {
                  innerTemp['patientId'] = this.encryptDecrypt.decryptString(tableFrom[i].patientSsn);
                }
                else{
                  innerTemp['patientId'] = '';
                }
              }
              else{
                innerTemp['patientId'] = '';
              }
              if (typeof tableFrom[i].patientGender == 'undefined') {
                innerTemp['patientSex'] = '';
              }
              else{
                if (tableFrom[i].patientGender == 'M' || tableFrom[i].patientGender == "m") {
                  innerTemp['patientSex'] = "Male";

                }
                else if (tableFrom[i].patientGender == 'F' || tableFrom[i].patientGender == "f") {
                  innerTemp['patientSex'] = "Female";
                }
                else{
                  innerTemp['patientSex'] = "Unspecified";
                }
                // innerTemp['patientSex'] = tableFrom[i].patientGender;
              }


              if (typeof tableFrom[i].phoneNumber == 'undefined') {
                innerTemp['patientTell'] = ''
              }
              else{
                innerTemp['patientTell'] = this.encryptDecrypt.decryptString(tableFrom[i].phoneNumber);
              }

              innerTemp['clientName'] = tableFrom[0].clientName;
              innerTemp['accountNumber'] = accountNumber;
              innerTemp['attendingPhysicianName'] = attendingPhysicianName;
              innerTemp['physicianTel'] = physicianPhone;
              innerTemp['physicianFax'] = physicianFax;
              innerTemp['address'] = this.compeleteAddressForReport; ///////// whose address?
              innerTemp['comment'] = tableFrom[i].interpretationComment;
              requestTempHolder.reportData.push(innerTemp);
              innerTemp={}
              // console.log("requestTempHolder",requestTempHolder);
            }
            // console.log("requestTempHolder",requestTempHolder);

            var consolidatedReportUrl = environment.API_REPORTING_ENDPOINT+'downloadreport'
            var consolidatedRequest   = {...this.consolidatedReportRequest}
            consolidatedRequest.data = requestTempHolder;
            consolidatedRequest.header.userCode = this.logedInUserRoles.userCode;
            consolidatedRequest.header.partnerCode = this.logedInUserRoles.partnerCode;
            consolidatedRequest.header.functionalityCode = "MRMA-MR";

            console.log("consolidatedRequest",consolidatedRequest);

            // this.ngxLoader.stop();return;
            this.globalService.sendReport(consolidatedReportUrl,consolidatedRequest).subscribe(reportResp=>{
              console.log('download rep',reportResp);
              this.ngxLoader.stop()
              if (reportResp['result'].codeType == 'S') {
                setTimeout(()=>{
                  // this.notifier.notify( "error", "Error while sending report" );
                  this.notifier.notify( "success", reportResp['result'].description);
                  // window.open("data:application/pdf;base64, " + reportResp['data']['base64EncodedFile']);
                  // window.open(reportResp['data']['base64EncodedFile'], "_blank");
                  // let pdfWindow = window.open("")
                  // pdfWindow.document.write(
                  //   "<iframe width='100%' height='100%' src='"+encodeURI(reportResp['data']['base64EncodedFile'])+"'></iframe>"
                  // )
                  const downloadLink = document.createElement("a");
                  var fileName="";
                  if(reportPackageId == 1){
                    fileName = "report.zip";
                  }
                  else{
                    fileName = "report.pdf";
                  }

                  downloadLink.href = reportResp['data']['base64EncodedFile'];
                  downloadLink.download = fileName;
                  downloadLink.click();
                  this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
                    dtElement.dtInstance.then((dtInstance: any) => {
                      // dtInstance.destroy();
                      dtInstance.ajax.reload();
                    })
                  })
                  this.clearCheckBoxes();
                  ///////// $('#successCOnsolidatedRepot').modal('show')

                }, 500);

              }
              else{
                this.ngxLoader.stop()
                this.notifier.notify( "error", "Error while downloading report" );
              }
            },error=>{
              this.ngxLoader.stop()
              this.notifier.notify( "error", "Error while downloading report check Database connection" );
            })
          }
          else{
            this.ngxLoader.stop();
            this.notifier.notify("error","error While loading User Name")
          }
        },error=>{
          this.ngxLoader.stop();
          this.notifier.notify("error","error While loading User Name")
        })
        // this.ngxLoader.stop();
        // return;

      },
      error =>{
        this.ngxLoader.stop()
        this.notifier.notify('error','No response from server please check your connection');
      });



    }
    reloadAjax(){
      this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
        dtElement.dtInstance.then((dtInstance: any) => {
          // dtInstance.destroy();
          dtInstance.ajax.reload();
        })
      })
      this.clearCheckBoxes();
    }

    clientChanged(){
      console.log("this.clientSearchString",this.clientSearchString);
      if (this.clientSearchString != null) {
        this.getIntakeForClient(this.clientSearchString.clientId)

      }
      else{
      }
    }

    getIntakeForClient(clientId){
      var reqAllIntakes = {...this.allIntakesReq}
      reqAllIntakes.header.partnerCode = this.logedInUserRoles.partnerCode;
      reqAllIntakes.header.userCode = this.logedInUserRoles.userCode;
      reqAllIntakes.header.functionalityCode = "MRMA-MR";
      reqAllIntakes.data.clientId                = [clientId] ;
      reqAllIntakes.data.searchString                = 'byclient';
      let intakeUrl    = environment.API_SPECIMEN_ENDPOINT + 'showintakes';
      this.globalService.getIntakes(intakeUrl, reqAllIntakes).subscribe(getIntakeResp=>{
        console.log("getIntakeResp",getIntakeResp);
        if (getIntakeResp['statusCode'] == "Internal Server Error") {
          // alert('No Physician Associated to this Client')
          this.ngxLoader.stop();
          this.notifier.notify("error","No response from server please check your connection")
          return;
        }
        this.allIntakesForClient = getIntakeResp['data']['caseDetails'];
        this.disableIntake = false;

      })
    }
    intakeChange(){
      if (this.selectedIntakeforClient != null || this.selectedIntakeforClient != "" || this.selectedIntakeforClient != "none") {
        for (let i = 0; i < this.allIntakesForClient.length; i++) {
          if (this.allIntakesForClient[i]['intakeId'] == this.selectedIntakeforClient) {
            this.selectedIntakeRecord = this.allIntakesForClient[i];
            console.log("this.selectedIntakeRecordollectionDate",this.selectedIntakeRecord['collectionTimestamp']);

          }

        }
        // if (this.selectedIntakeforClient == "none") {
        //   this.notifier.notify("info","")
        // }
        if (this.clientSearchString !=null || this.clientSearchString !="") {
          this.disableSearchButton = false;
          this.disableDeliveryHistoryButton = false;
        }
        else{
          this.disableSearchButton = false;
          this.disableDeliveryHistoryButton = false;
        }
      }
      else{
        if (this.selectedIntakeforClient == "none") {
          this.selectedIntakeRecord = null;
        }
        this.disableSearchButton = false;
        this.disableDeliveryHistoryButton = false;
      }


    }

    searchButtonClick(){
      if (this.selectedIntakeforClient != null || this.selectedIntakeforClient != "") {
        if (this.clientSearchString !=null || this.clientSearchString !="") {
          this.disableSearchButton = false;
          this.disableDeliveryHistoryButton = false;

          this.showAllCasesReq.data.clientId = this.clientSearchString.clientId;
          this.showAllCasesReq.data.intakeId = this.selectedIntakeforClient;

          this.allCasesReq.data.client = this.clientSearchString.clientId;
          this.allCasesReq.data.intakeId = this.selectedIntakeforClient;

          this.pendingReportReqNew.data['clientId'] = this.clientSearchString.clientId;
          this.pendingReportReqNew.data['intakeId'] = this.selectedIntakeforClient;

          this.searchByCriteria.data.clientId = this.clientSearchString.clientId;
          this.searchByCriteria.data.intakeId = this.selectedIntakeforClient;
          // this.dtTrigger.next();
          this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
            dtElement.dtInstance.then((dtInstance: any) => {
              dtInstance.ajax.reload();
            })
          })
          this.clearCheckBoxes();
        }
        else{
          alert('select a client first');
          this.disableSearchButton = false;
          this.disableDeliveryHistoryButton = false;
        }

      }
      else{
        this.disableSearchButton = false;
        this.disableDeliveryHistoryButton = false;
      }
    }

    reportSelection(from){


      if (from == 'pending') {
        console.log('mrc',this.mrc);
        console.log('dcr',this.dcr);
        console.log('cr',this.cr);
        if (this.mrc == true && this.dcr == true && this.cr == true ) {
          this.multipleReports(1,1,5,1,from,'all');
        }
        else if (this.mrc == true && this.dcr == true && this.cr == false ) {
          this.multipleReports(1,1,5,1,from,'mrdr');
        }
        else if (this.mrc == true && this.dcr == false && this.cr == true ) {
          this.multipleReports(1,1,5,1,from,'mrcr');
        }
        else if (this.mrc == false && this.dcr == true && this.cr == true ) {
          this.multipleReports(1,1,5,1,from,'drcr');
        }
        else if (this.mrc == true && this.dcr == false && this.cr == false ) {
          this.detailedReport(1,1,5,1,from)
        }
        else if (this.mrc == false && this.dcr == true && this.cr == false ) {
          this.detailedReport(2,1,1,1,from);

        }
        else if (this.mrc == false && this.dcr == false && this.cr == true ) {
          this.consolidatedReport(3,2,1,1,from);
        }
        else if (this.mrc == false && this.dcr == false && this.cr == false ) {

        }{
          return;
        }
      }
      else if (from == 'delivered') {
        console.log('mrc1',this.mrc1);
        console.log('dcr1',this.dcr1);
        console.log('cr1',this.cr1);
        if (this.mrc1 == true && this.dcr1 == true && this.cr1 == true ) {
          this.multipleReports(1,1,5,1,from,'all');
        }
        else if (this.mrc1 == true && this.dcr1 == true && this.cr1 == false ) {
          this.multipleReports(1,1,5,1,from,'mrdr');
        }
        else if (this.mrc1 == true && this.dcr1 == false && this.cr1 == true ) {
          this.multipleReports(1,1,5,1,from,'mrcr');
        }
        else if (this.mrc1 == false && this.dcr1 == true && this.cr1 == true ) {
          this.multipleReports(1,1,5,1,from,'drcr');
        }
        else if (this.mrc1 == true && this.dcr1 == false && this.cr1 == false ) {
          this.detailedReport(1,1,5,1,from)
        }
        else if (this.mrc1 == false && this.dcr1 == true && this.cr1 == false ) {
          this.detailedReport(2,1,1,1,from);

        }
        else if (this.mrc1 == false && this.dcr1 == false && this.cr1 == true ) {
          this.consolidatedReport(3,2,1,1,from);
        }
        else if (this.mrc1 == false && this.dcr1 == false && this.cr1 == false ) {

        }{
          return;
        }
      }
      else if (from == 'notdelivered') {
        console.log('mrc2',this.mrc2);
        console.log('dcr2',this.dcr2);
        console.log('cr2',this.cr2);
        if (this.mrc2 == true && this.dcr2 == true && this.cr2 == true ) {
          this.multipleReports(1,1,5,1,from,'all');
        }
        else if (this.mrc2 == true && this.dcr2 == true && this.cr2 == false ) {
          this.multipleReports(1,1,5,1,from,'mrdr');
        }
        else if (this.mrc2 == true && this.dcr2 == false && this.cr2 == true ) {
          this.multipleReports(1,1,5,1,from,'mrcr');
        }
        else if (this.mrc2 == false && this.dcr2 == true && this.cr2 == true ) {
          this.multipleReports(1,1,5,1,from,'drcr');
        }
        else if (this.mrc2 == true && this.dcr2 == false && this.cr2 == false ) {
          this.detailedReport(1,1,5,1,from)
        }
        else if (this.mrc2 == false && this.dcr2 == true && this.cr2 == false ) {
          this.detailedReport(2,1,1,1,from);

        }
        else if (this.mrc2 == false && this.dcr2 == false && this.cr2 == true ) {
          this.consolidatedReport(3,2,1,1,from);
        }
        else if (this.mrc2 == false && this.dcr2 == false && this.cr2 == false ) {

        }{
          return;
        }
      }


    }

    multipleReports(reportPackageId,reportTemplate,fileTypeId,deliverMethod,tableType,selectionType){
      var tableFrom;
      if (tableType == 'pending') {
        tableFrom = this.listForconsolidatedReport;
      }
      else if (tableType == 'delivered') {
        tableFrom = this.listForconsolidatedReportDelivered;
      }
      else if (tableType == 'notdelivered') {
        tableFrom = this.listForconsolidatedReportnotDelivered;
      }
      // console.log("this.listForconsolidatedReportnotDelivered",this.listForconsolidatedReportnotDelivered);
      console.log("tableFrom",tableFrom);
      if (tableFrom.length == 0) {
        this.ngxLoader.stop()
        alert('please select atleast one case');
        return;
      }
      for (var check = 0; check < tableFrom.length; check++) {
        if (tableFrom[0].item.clientId != tableFrom[check].item.clientId) {
          this.ngxLoader.stop()
          alert('please select casses associated to same client');

          return;
        }
      }

      this.ngxLoader.start();
      var summaryReq ;
      var detaliReq  ;
      if(selectionType == 'mrdr'){
        summaryReq = [0];
        detaliReq = this.populateDetailedReportRequest(tableFrom, reportPackageId,reportTemplate,fileTypeId,deliverMethod,tableType,selectionType).then(detailPopResponse=>{
          console.log('detailPopResponse',detailPopResponse);
          return detailPopResponse
        })
      }
      else{
        summaryReq = this.populateSummaryReportRequest(tableFrom, reportPackageId,reportTemplate,fileTypeId,deliverMethod,tableType).then(summaryPopResponse=>{
          console.log('summaryPopResponse',summaryPopResponse);
          return summaryPopResponse

        })
        detaliReq = this.populateDetailedReportRequest(tableFrom, reportPackageId,reportTemplate,fileTypeId,deliverMethod,tableType,selectionType).then(detailPopResponse=>{
          console.log('detailPopResponse',detailPopResponse);
          return detailPopResponse
        })
      }

      forkJoin([summaryReq,detaliReq]).subscribe(allRequest => {
        console.log("allRequest",allRequest);
        var myDate = new Date();
        var currentDate = this.datePipe.transform(myDate, 'MM/dd/yyyy');
        var currentTimeStamp = this.datePipe.transform(myDate, 'MM/dd/yyyy HH:mm:ss');

        var allReqData = {...this.multipleReportRequest}
        allReqData.header.partnerCode = this.logedInUserRoles.partnerCode;
        allReqData.header.userCode    = this.logedInUserRoles.userCode;
        allReqData.header.functionalityCode    = "MRMA-MR";

        var multipleUrl = environment.API_REPORTING_ENDPOINT + 'multiplereports';
        if (selectionType == 'mrdr') {
          allReqData.data.summaryReport = null;
        }
        else{
          allReqData.data.summaryReport = allRequest[0];
        }
        allReqData.data.detailedConsolidatedReport = allRequest[1];
        console.log("allReqData",allReqData);
        // this.ngxLoader.stop();

        var multipleUrl = environment.API_REPORTING_ENDPOINT + 'multiplereports';
        this.globalService.sendReport(multipleUrl,allReqData).subscribe(reportResp=>{
          console.log('reportResp',reportResp);
          this.ngxLoader.stop()
          if (reportResp['result'].codeType == 'S') {
            setTimeout(()=>{
              this.notifier.getConfig().behaviour.autoHide = 5000;
              this.notifier.notify( "success", "The system is generating the report, the report shall be sent to the client, once it is ready.");

              // this.notifier.notify( "success", "The system is generating the report, the report shall be sent to the client, once it is ready.");
              // for (let i = 0; i < reportResp['data']['base64EncodedFiles'].length; i++) {
              var statusArray = []
              var holder = {};

              holder['status']  = false;
              holder['reportId'] = reportResp['data'][0]['reportId'];
              statusArray.push(holder);
              for (let i = 0; i < reportResp['data'].length; i++) {
                // var holder = {};
                //
                // holder['status']  = false;
                // holder['reportId'] = reportResp['data'][i]['reportId'];
                // statusArray.push(holder);
                // this.multipleReportsStatusCheck.push(holder);

                //
                // let mimeType2 = reportResp['data']['base64EncodedFiles'][i].match(/[^:/]\w+(?=;|,)/)[0];
                let mimeType2 = reportResp['data'][i]['base64EncodedFile'].match(/[^:/]\w+(?=;|,)/)[0];
                const downloadLink = document.createElement("a");
                var fileName="";
                var fi = reportResp['data'][i]['fileName'].split('/');
                var le = fi.length-1;
                fileName = fi[le];
                // console.log("ftype",ftype);
                // if(mimeType2 == 'zip'){
                //   fileName = "report.zip";
                // }
                // else{
                //   fileName = "report.pdf";
                // }

                downloadLink.href = reportResp['data'][i]['base64EncodedFile'];
                downloadLink.download = fileName;

                //downloadLink.click();

              }
              var getRecord = [];
              getRecord = JSON.parse(localStorage.getItem('reportStatusArray'));
              if (getRecord != null && getRecord['length'] >0) {
                getRecord.push(statusArray);
                localStorage.setItem('reportStatusArray',JSON.stringify(getRecord));
                this.stratInterval()

              }
              else{
                localStorage.setItem('reportStatusArray',JSON.stringify(statusArray));
                this.stratInterval()
              }

              this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
                dtElement.dtInstance.then((dtInstance: any) => {
                  // dtInstance.destroy();
                  dtInstance.ajax.reload();
                })
              })
              this.clearCheckBoxes();
              this.mrc = false;
              this.dcr = false;
              this.cr  = false;

              //
              //
              // const downloadLink = document.createElement("a");
              // var fileName="";
              // if(reportPackageId == 1){
              //   fileName = "report.zip";
              // }
              // else{
              //   fileName = "report.pdf";
              // }
              //
              // downloadLink.href = reportResp['data']['base64EncodedFile'];
              // downloadLink.download = fileName;
              // this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
              //   dtElement.dtInstance.then((dtInstance: any) => {
              //     // dtInstance.destroy();
              //     dtInstance.ajax.reload();
              //   })
              // })
              // //downloadLink.click();

              if (tableType == 'pending') {
                for (let i = 0; i < this.pendingReport.length; i++) {
                  this.pendingReport[i].checkBox = false;
                }
                this.listForconsolidatedReport = [];
              }
              else if (tableType == 'delivered') {
                for (let i = 0; i < this.deliveredReports.length; i++) {
                  this.deliveredReports[i].checkBox = false;
                }
                this.listForconsolidatedReportDelivered = [];
              }
              else if (tableType == 'notdelivered') {
                for (let i = 0; i < this.notDeliveredReports.length; i++) {
                  this.notDeliveredReports[i].checkBox = false;
                }
                this.listForconsolidatedReportnotDelivered = [];
                $('#check-all3')[0].checked = false;
              }


            }, 500);


          }
          else{
            this.ngxLoader.stop()
            this.notifier.notify( "error", "Error while sending report" );
          }
        },error=>{
          this.ngxLoader.stop()
          this.notifier.notify( "error", "Error while sending report check Database connection" );
        })


      })


      //

    }

    populateSummaryReportRequest(tableFrom,reportPackageId,reportTemplate,fileTypeId,deliverMethod,tableType){
      return new Promise((resolve, reject) => {
        this.ngxLoader.start()
        var myDate = new Date();
        var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy');
        var currentTimeStamp = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');

        var requestTempHolder = {
          caseResults:[]
        };
        var defaultPhysicianID     = '';
        var physicianFax     = '';
        var physicianPhone     = '';
        var salesRepEmail     = '';
        var accountNumber     = '';
        var atdPhyUrl   =  environment.API_CLIENT_ENDPOINT + "clientadressinfo";
        var reqTemplate = {
          header:{
            uuid               :"",
            partnerCode        :"",
            userCode           :"",
            referenceNumber    :"",
            systemCode         :"",
            moduleCode         :"CLIM",
            functionalityCode: "CLTA-VC",
            systemHostAddress  :"",
            remoteUserAddress  :"",
            dateTime           :""
          },
          data:{
            clientIds:[tableFrom[0].item.clientId],
            userCodes: []
          }
        }
        reqTemplate.header.partnerCode  = this.logedInUserRoles.partnerCode;
        reqTemplate.header.userCode     = this.logedInUserRoles.userCode;
        reqTemplate.header.functionalityCode     = "CLTA-VC";
        // var reqTemplate = {clientIds:[clientId]}
        this.globalService.getClientByIds(atdPhyUrl,reqTemplate).subscribe(respClient => {
          console.log("respClient summary",respClient);
          if (respClient['statusCode'] == "Internal Server Error") {
            // alert('No Physician Associated to this Client')
            this.ngxLoader.stop();
            this.notifier.notify("error","No response from server please check your connection")
            return;
          }
          if (respClient['data'].length == 0) {
            alert('No Record for this Client')
            this.ngxLoader.stop();
            return;
          }
          else {
            var physCount = 0; /////// if there is no default physician then slect 1s as physician
            var arrCount  = 0;
            accountNumber = respClient['data'][0]['accountNumber'];
            defaultPhysicianID = respClient['data'][0]['defaultAttendingPhysician'];
            if (respClient['data'][0]['physicianFax'] != null && respClient['data'][0]['physicianFax'] != '') {
              physicianFax       = this.encryptDecrypt.decryptString(respClient['data'][0]['physicianFax']);
            }
            console.log("defaultPhysicianID",defaultPhysicianID);
            var userRequest = {...this.usersRequest};
            userRequest.data.userCodes = [defaultPhysicianID,respClient['data'][0]['salesRepresentativeId']];
            var userUrl = environment.API_USER_ENDPOINT + "getusersbyusercodes";
            this.globalService.getUserRecord(userUrl,userRequest).subscribe(getUserResp =>{
              // return getUserResp;
              console.log("getUserResp",getUserResp);
              if (getUserResp['data'] != null) {
                for (let index = 0; index < getUserResp['data'].length; index++) {
                  if (defaultPhysicianID == getUserResp['data'][index].userCode) {
                    // attendingPhysicianName = getUserResp['data'][index].firstName+' '+getUserResp['data'][index].lastName;
                    if (typeof getUserResp['data'][index].phone != 'undefined') {
                      physicianPhone = getUserResp['data'][index].phone;
                    }
                  }
                  if (respClient['data'][0]['salesRepresentativeId'] == getUserResp['data'][index].userCode) {
                    if (typeof getUserResp['data'][index].email != 'undefined') {
                      salesRepEmail = getUserResp['data'][index].email;
                    }

                  }

                }
                var requestTemp = [];
                var clientIdForStatus = tableFrom[0].item.clientId;
                requestTempHolder['reportPackageId']  = 3;
                // requestTempHolder['reportPackageId']  = reportPackageId;
                requestTempHolder['reportTemplateId']  =reportTemplate;
                requestTempHolder['fileTypeId']         =fileTypeId;
                requestTempHolder['reportDeliveryMethodId']    =deliverMethod;
                requestTempHolder['clientName']       =tableFrom[0].item.clientName;
                // requestTempHolder['clientEmail']      =tableFrom[0].item.clientEmail;
                requestTempHolder['intakeId'] = tableFrom[0].item.intakeId;
                if (salesRepEmail == '' || typeof salesRepEmail == 'undefined') {
                  requestTempHolder['clientEmail']      =this.encryptDecrypt.decryptString(tableFrom[0].item.clientEmail);
                }
                else{
                  requestTempHolder['clientEmail']      =this.encryptDecrypt.decryptString(tableFrom[0].item.clientEmail)+','+salesRepEmail;
                }
                var currentDate = this.datePipe.transform(myDate, 'MM/dd/yyyy');
                var currentTimeStamp = this.datePipe.transform(myDate, 'MM/dd/yyyy HH:mm:ss');
                requestTempHolder['reportDate']       = currentDate;
                requestTempHolder['creationDate']     = currentDate;
                var currentZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
                requestTempHolder['timeZoneId'] = currentZone;
                var innerTemp = {};
                for (let i = 0; i < tableFrom.length; i++) {
                  // innerTemp['sNo'] = tableFrom[i].index+1;
                  innerTemp['sNo'] = i+1;
                  innerTemp['lastName'] = this.encryptDecrypt.decryptString(tableFrom[i].item.patientLastName);
                  innerTemp['firstName'] = this.encryptDecrypt.decryptString(tableFrom[i].item.patientFirstName);
                  innerTemp['patientDob'] = this.datePipe.transform(tableFrom[i].item.patientDateOfBirth, 'MM/dd/yyyy');
                  innerTemp['caseNumber'] = tableFrom[i].item.caseNumber;
                  innerTemp['caseId'] = tableFrom[i].item.caseId;
                  // innerTemp['intakeId'] = tableFrom[i].item.intakeId;
                  var specimen = '';
                  var spType = tableFrom[i].item.specimenTypeId.split(',')

                  for (let j = 0; j < this.allSpecimenTypes.length; j++) {
                    // console.log('tableFrom[i].specimenTypeId',tableFrom[i].item.specimenTypeId);

                    for (let k = 0; k < spType.length; k++) {
                      if (this.allSpecimenTypes[j].specimenTypeId == spType[k]) {
                        specimen = specimen +' ' + this.allSpecimenTypes[j].specimenTypeName;
                      }

                    }
                  }
                  innerTemp['specimenType'] = specimen;
                  if (tableFrom[i].item.interpretationDiagnosis == undefined) {
                    innerTemp['result'] = null;
                  }
                  else{
                    innerTemp['result'] = tableFrom[i].item.interpretationDiagnosis;
                  }

                  if (tableFrom[i].item.jobOrderId == undefined) {
                    innerTemp['jobOrderId'] = null;
                  }
                  else{
                    innerTemp['jobOrderId'] = tableFrom[i].item.jobOrderId;
                  }

                  innerTemp['patientRevisionId'] = tableFrom[i].item.patientRevisionId;
                  if (this.selectedIntakeRecord == null) {
                    innerTemp['collectionDate'] = this.datePipe.transform(tableFrom[i].item.caseCollectionDate, 'MM/dd/yyy');

                  }
                  else{
                    innerTemp['collectionDate'] = this.datePipe.transform(this.selectedIntakeRecord['collectionTimestamp'], 'MM/dd/yyy');

                  }
                  // innerTemp['collectionDate'] = this.datePipe.transform(tableFrom[i].item.caseCollectionDate, 'MM/dd/yyy');
                  requestTempHolder.caseResults.push(innerTemp);
                  innerTemp={}
                  // console.log("requestTempHolder",requestTempHolder);
                }

                // console.log("requestTempHolder",requestTempHolder);

                // var consolidatedReportUrl = environment.API_REPORTING_ENDPOINT+'summaryreport'
                // var consolidatedRequest   = {...this.consolidatedReportRequest}
                // consolidatedRequest.data = requestTempHolder;
                // consolidatedRequest.header.userCode = this.logedInUserRoles.userCode;
                // consolidatedRequest.header.partnerCode = this.logedInUserRoles.partnerCode;
                var summaryRequest = requestTempHolder;
                // console.log("summaryRequest",summaryRequest);
                resolve(summaryRequest)


              }
            },error=>{
              this.ngxLoader.stop();
              this.notifier.notify("error","No response from server please check your connection")

            })

            // for (let i = 0; i < respClient['data'][0]['clientUsers'].length; i++) {
            //
            //   // if (respClient['data'][0]['clientUsers'][i]['defaultPhysician'] == 1) {
            //   //   // attendingPhysicianName = respClient['data'][0]['clientUsers'][i]['userCode'];
            //   //   defaultPhysicianID = respClient['data'][0]['clientUsers'][i]['userCode'];
            //   //   for (let n = 0; n < respClient['data'][0]['clientUserContactsByClientId'].length; n++) {
            //   //     if (respClient['data'][0]['clientUserContactsByClientId'][n]['userCode'] == respClient['data'][0]['clientUsers'][i]['userCode']) {
            //   //       if (respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['contactTypeId'] == 3) {
            //   //         physicianFax = this.encryptDecrypt.decryptString(respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['value']);
            //   //       }
            //   //       else if (respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['contactTypeId'] == 1) {
            //   //         // physicianPhone = respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['value']
            //   //       }
            //   //       else if (respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['contactTypeId'] == 3) {
            //   //         // salesRepEmail = respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['value']
            //   //       }
            //   //     }
            //   //
            //   //   }
            //   //
            //   //   physCount ++;
            //   // }
            //   arrCount = arrCount+ 1;
            //   if (arrCount == respClient['data'][0]['clientUsers'].length) {
            //     // if (physCount == 0) {
            //     //   // attendingPhysicianName = respClient['data'][0]['clientUsers'][0]['userCode'];
            //     //   defaultPhysicianID = respClient['data'][0]['clientUsers'][0]['userCode'];
            //     // }
            //
            //
            //
            //   }
            //
            // }
          }

        },error=>{
          this.ngxLoader.stop();
          this.notifier.notify("error","No response from server please check your connection")
        })


      })
      // console.log("this.listForconsolidatedReport",this.listForconsolidatedReport);

    }

    populateDetailedReportRequest(tableFrom,reportPackageId,reportTemplate,fileTypeId,deliverMethod,tableType,selectionType){
      return new Promise((resolve, reject) => {
        this.compeleteAddressForReport ='';
        var attendingPhysicianName = '';
        var attendingPhysicianPhone ='';
        var defaultPhysicianID     = '';
        var physicianFax     = '';
        var physicianPhone     = '';
        var salesRepEmail     = '';
        var accountNumber     = '';
        var atdPhyUrl   =  environment.API_CLIENT_ENDPOINT + "clientadressinfo";
        var reqTemplate = {
          header:{
            uuid               :"",
            partnerCode        :"",
            userCode           :"",
            referenceNumber    :"",
            systemCode         :"",
            moduleCode         :"CLIM",
            functionalityCode: "CLTA-VC",
            systemHostAddress  :"",
            remoteUserAddress  :"",
            dateTime           :""
          },
          data:{
            clientIds:[tableFrom[0].item.clientId],
            userCodes: []

          }
        }
        reqTemplate.header.partnerCode  = this.logedInUserRoles.partnerCode;
        reqTemplate.header.userCode     = this.logedInUserRoles.userCode;
        reqTemplate.header.functionalityCode     = "CLTA-VC";
        // var reqTemplate = {clientIds:[clientId]}
        this.globalService.getClientByIds(atdPhyUrl,reqTemplate).subscribe(respClient => {
          console.log("respClient",respClient);
          if (respClient['statusCode'] == "Internal Server Error") {
            // alert('No Physician Associated to this Client')
            this.ngxLoader.stop();
            this.notifier.notify("error","No response from server please check your connection")
            return;
          }
          if (respClient['data'].length == 0) {
            alert('No record found for this Client')
            this.ngxLoader.stop();
            return;
          }
          else {
            var completeAddress = this.composeAddress(respClient['data'][0]['addressDto']['countryId'],respClient['data'][0]['addressDto']['stateId'],respClient['data'][0]['addressDto']['city'],respClient['data'][0]['addressDto']['street'],respClient['data'][0]['addressDto']['suiteFloorBuilding'],respClient['data'][0]['addressDto']['zip'])
            // console.log('completeAddress',completeAddress);
            var physCount = 0; /////// if there is no default physician then slect 1s as physician
            var arrCount  = 0;
            accountNumber = respClient['data'][0]['accountNumber'];
            defaultPhysicianID = respClient['data'][0]['defaultAttendingPhysician'];
            if (respClient['data'][0]['physicianFax'] != null && respClient['data'][0]['physicianFax'] != '') {
              physicianFax       = this.encryptDecrypt.decryptString(respClient['data'][0]['physicianFax']);
            }

            // for (let i = 0; i < respClient['data'][0]['clientUsers'].length; i++) {
            //
            //   if (respClient['data'][0]['clientUsers'][i]['defaultPhysician'] == 1) {
            //     // attendingPhysicianName = respClient['data'][0]['clientUsers'][i]['userCode'];
            //     defaultPhysicianID = respClient['data'][0]['clientUsers'][i]['userCode'];
            //     for (let n = 0; n < respClient['data'][0]['clientUserContactsByClientId'].length; n++) {
            //       if (respClient['data'][0]['clientUserContactsByClientId'][n]['userCode'] == respClient['data'][0]['clientUsers'][i]['userCode']) {
            //         if (respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['contactTypeId'] == 3) {
            //           physicianFax = respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['value']
            //         }
            //         else if (respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['contactTypeId'] == 1) {
            //           // physicianPhone = respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['value']
            //         }
            //         else if (respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['contactTypeId'] == 3) {
            //           // salesRepEmail = respClient['data'][0]['clientUserContactsByClientId'][n]['contactByContactId'][0]['value']
            //         }
            //       }
            //
            //     }
            //
            //     physCount ++;
            //   }
            //   arrCount = arrCount+ 1;
            //   if (arrCount == respClient['data'][0]['clientUsers'].length) {
            //     if (physCount == 0) {
            //       // attendingPhysicianName = respClient['data'][0]['clientUsers'][0]['userCode'];
            //       defaultPhysicianID = respClient['data'][0]['clientUsers'][0]['userCode'];
            //     }
            //   }
            //
            // }
          }
          console.log("defaultPhysicianID",defaultPhysicianID);
          var userRequest = {...this.usersRequest};
          userRequest.data.userCodes = [defaultPhysicianID,respClient['data'][0]['salesRepresentativeId']];
          var userUrl = environment.API_USER_ENDPOINT + "getusersbyusercodes";
          this.globalService.getUserRecord(userUrl,userRequest).subscribe(getUserResp =>{
            // return getUserResp;
            // console.log("getUserResp---------",getUserResp);
            if (getUserResp['data'] != null ) {
              // console.log("innnnnnn---",respClient['data'][0]['salesRepresentativeId']);


              for (let index = 0; index < getUserResp['data'].length; index++) {
                // console.log("getUserResp['data'][index].email---",getUserResp['data'][index].email);
                if (defaultPhysicianID == getUserResp['data'][index].userCode) {
                  attendingPhysicianName = getUserResp['data'][index].firstName+' '+getUserResp['data'][index].lastName;
                  if (typeof getUserResp['data'][index].phone != 'undefined') {
                    physicianPhone = getUserResp['data'][index].phone;
                  }
                }
                if (respClient['data'][0]['salesRepresentativeId'] == getUserResp['data'][index].userCode) {
                  if (typeof getUserResp['data'][index].email != 'undefined') {
                    salesRepEmail = getUserResp['data'][index].email;
                  }

                }

              }
              var myDate = new Date();
              var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy');
              var currentTimeStamp = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');
              // return;
              var requestTempHolder = {
                reportData:[]
              };

              var requestTemp = [];
              if (selectionType == 'mrdr') {
                requestTempHolder['reportPackageId']            = 2;
                requestTempHolder['reportPackageIdCompressed']  = 1;
                requestTempHolder['fileTypeId']                 =1;
                requestTempHolder['fileTypeIdCompressed']       =5;
              }
              else if (selectionType == 'mrcr') {
                requestTempHolder['reportPackageId']            = null;
                requestTempHolder['reportPackageIdCompressed']  = 1;
                requestTempHolder['fileTypeId']                 =null;
                requestTempHolder['fileTypeIdCompressed']       =5;
              }
              else if (selectionType == 'drcr') {
                requestTempHolder['reportPackageId']            = 2;
                requestTempHolder['reportPackageIdCompressed']  = null;
                requestTempHolder['fileTypeId']                 =1;
                requestTempHolder['fileTypeIdCompressed']       =null;
              }
              else{
                requestTempHolder['reportPackageId']            = 2;
                requestTempHolder['reportPackageIdCompressed']  = 1;
                requestTempHolder['fileTypeId']                 = 1;
                requestTempHolder['fileTypeIdCompressed']       = 5;
              }

              requestTempHolder['reportTemplateId']  =reportTemplate;
              // requestTempHolder['fileTypeId']       =fileTypeId;
              requestTempHolder['reportDeliveryMethodId']    =deliverMethod;
              requestTempHolder['clientName']       =tableFrom[0].item.clientName;
              if (salesRepEmail == '' || typeof salesRepEmail == 'undefined') {
                requestTempHolder['clientEmail']      =this.encryptDecrypt.decryptString(tableFrom[0].item.clientEmail);
              }
              else{
                requestTempHolder['clientEmail']      =this.encryptDecrypt.decryptString(tableFrom[0].item.clientEmail)+','+salesRepEmail;
              }
              var currentDate = this.datePipe.transform(myDate, 'MM/dd/yyyy');
              var currentTimeStamp = this.datePipe.transform(myDate, 'MM/dd/yyyy HH:mm:ss');

              requestTempHolder['reportDate']       = currentDate;
              requestTempHolder['creationDate']     = currentDate;
              requestTempHolder['intakeId'] = tableFrom[0].item.intakeId;
              var currentZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
              requestTempHolder['timeZoneId'] = currentZone;
              var innerTemp = {};
              for (let i = 0; i < tableFrom.length; i++) {
                // innerTemp['sNo'] = tableFrom[i].index;
                // innerTemp['lastName'] = tableFrom[i].item.patientLastName;
                // innerTemp['firstName'] = tableFrom[i].item.patientFirstName;
                innerTemp['caseNumber'] = tableFrom[i].item.caseNumber;
                innerTemp['caseId'] = tableFrom[i].item.caseId;
                var specimen = '';
                var spType = tableFrom[i].item.specimenTypeId.split(',')

                for (let j = 0; j < this.allSpecimenTypes.length; j++) {
                  // console.log('tableFrom[i].specimenTypeId',tableFrom[i].item.specimenTypeId);

                  for (let k = 0; k < spType.length; k++) {
                    if (this.allSpecimenTypes[j].specimenTypeId == spType[k]) {
                      specimen = specimen +' ' + this.allSpecimenTypes[j].specimenTypeName;
                    }

                  }
                }
                innerTemp['diagnosis']   = specimen;
                if (tableFrom[i].item.interpretationDiagnosis == undefined) {
                  innerTemp['diagnosis'] = innerTemp['diagnosis'] +': '+ null;
                }
                else{
                  innerTemp['diagnosis'] = innerTemp['diagnosis'] + tableFrom[i].item.interpretationDiagnosis;
                }

                if (tableFrom[i].item.jobOrderId == undefined) {
                  innerTemp['jobOrderId'] = null;
                }
                else{
                  innerTemp['jobOrderId'] = tableFrom[i].item.jobOrderId;
                }

                innerTemp['patientRevisionId'] = tableFrom[i].item.patientRevisionId;
                if (this.selectedIntakeRecord == null) {
                  innerTemp['collectedDate'] = this.datePipe.transform(tableFrom[i].item.caseCollectionDate, 'MM/dd/yyy');

                }
                else{
                  innerTemp['collectedDate'] = this.datePipe.transform(this.selectedIntakeRecord['collectionTimestamp'], 'MM/dd/yyy');

                }
                // innerTemp['collectedDate'] = this.datePipe.transform(tableFrom[i].item.caseCollectionDate, 'MM/dd/yyy');
                var currentDate = this.datePipe.transform(myDate, 'MM/dd/yyyy');
                var currentTimeStamp = this.datePipe.transform(myDate, 'MM/dd/yyyy HH:mm:ss');
                // innerTemp['sentDate'] = currentDate; ////////////// sent date
                innerTemp['sentDate'] = this.datePipe.transform(tableFrom[i].item.sentDate, 'MM/dd/yyyy');; ////////////// sent date
                innerTemp['submittedDate'] = currentDate;////// submitted date
                innerTemp['patientName'] = this.encryptDecrypt.decryptString(tableFrom[i].item.patientLastName)+", "+this.encryptDecrypt.decryptString(tableFrom[i].item.patientFirstName);
                innerTemp['patientDob'] = this.datePipe.transform(tableFrom[i].item.patientDateOfBirth, 'MM/dd/yyyy');
                // innerTemp['patientId'] = this.encryptDecrypt.decryptString(tableFrom[i].item.patientSsn);
                if (typeof tableFrom[i].patientSsn !='undefined') {
                  if (tableFrom[i].patientSsn !='') {
                    innerTemp['patientId'] = this.encryptDecrypt.decryptString(tableFrom[i].item.patientSsn);
                  }
                  else{
                    innerTemp['patientId'] = '';
                  }
                }
                else{
                  innerTemp['patientId'] = '';
                }
                // innerTemp['receivingDate'] = tableFrom[i].item.receivingDate;
                innerTemp['receivingDate'] = this.datePipe.transform(tableFrom[i].item.receivingDate, 'MM/dd/yyy');
                if (typeof tableFrom[i].item.patientGender == 'undefined') {
                  innerTemp['patientSex'] = '';
                }
                else{
                  if (tableFrom[i].item.patientGender == 'M' || tableFrom[i].item.patientGender == "m") {
                    innerTemp['patientSex'] = "Male";

                  }
                  else if (tableFrom[i].item.patientGender == 'F' || tableFrom[i].item.patientGender == "f") {
                    innerTemp['patientSex'] = "Female";
                  }
                  else{
                    innerTemp['patientSex'] = "Unspecified";
                  }
                  // innerTemp['patientSex'] = tableFrom[i].item.patientGender;
                }

                if (typeof tableFrom[i].item.phoneNumber == 'undefined') {
                  innerTemp['patientTell'] = ''
                }
                else{
                  innerTemp['patientTell'] = this.encryptDecrypt.decryptString(tableFrom[i].item.phoneNumber);
                }

                innerTemp['clientName'] = tableFrom[0].item.clientName;
                innerTemp['caseId'] = tableFrom[i].item.caseId;
                innerTemp['caseNumber'] = tableFrom[i].item.caseNumber;
                innerTemp['accountNumber'] = accountNumber;
                innerTemp['attendingPhysicianName'] = attendingPhysicianName;
                innerTemp['physicianTel'] = physicianPhone;
                innerTemp['physicianFax'] = physicianFax;
                innerTemp['address'] = this.compeleteAddressForReport; ///////// whose address?
                innerTemp['comment'] = tableFrom[i].item.interpretationComment;
                // innerTemp['intakeId'] = tableFrom[i].item.intakeId;
                requestTempHolder.reportData.push(innerTemp);
                innerTemp={}
                // console.log("requestTempHolder",requestTempHolder);
              }
              // console.log("requestTempHolder",requestTempHolder);

              // var consolidatedReportUrl = environment.API_REPORTING_ENDPOINT+'detailedreport'
              // var consolidatedRequest   = {...this.consolidatedReportRequest}
              // consolidatedRequest.data = requestTempHolder;
              // consolidatedRequest.header.userCode = this.logedInUserRoles.userCode;
              // consolidatedRequest.header.partnerCode = this.logedInUserRoles.partnerCode;
              var repotData = requestTempHolder;

              console.log("-----",repotData);
              resolve(repotData);
            }
            else{
              this.ngxLoader.stop();
              this.notifier.notify("error","error While loading User Name")
            }
          },error=>{
            this.ngxLoader.stop();
            this.notifier.notify("error","error While loading User Name")
          })
          // this.ngxLoader.stop();
          // return;

        },
        error =>{
          this.ngxLoader.stop()
          this.notifier.notify('error','No response from server please check your connection');
        });

        // resolve()

      })
      // console.log("this.listForconsolidatedReport",this.listForconsolidatedReport);

    }

    generateBulkReport(){
      console.log("this.selectedIntakeforClient",this.selectedIntakeforClient);


      if (this.selectedIntakeforClient != "none") {
        $('#delivery-historyModal').modal('show');
        this.triggerHistoryTable = true;
        this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
          dtElement.dtInstance.then((dtInstance: any) => {
            if (dtInstance.table().node().id == 'rep-history' ) {
              dtInstance.ajax.reload();

            }
          })
        })
        // this.clearCheckBoxes();
        return;
      }
      else{
        ////////////////////////// Filter cases from SPCM Module based on clientId and intakeId:
        var casesUrl = environment.API_SPECIMEN_ENDPOINT + 'showreportingcases';
        var casesReq = {...this.LoadCaseforReportingReq}
        casesReq.header.userCode    = this.logedInUserRoles.userCode;
        casesReq.header.partnerCode = this.logedInUserRoles.partnerCode;
        casesReq.header.functionalityCode = "MRMA-MR";
        casesReq.data.intakeId      = this.selectedIntakeforClient;
        casesReq.data.client        = this.clientSearchString.clientId;
        // casesReq.data.caseStatus     = "12";
        this.globalService.showReportingCases(casesUrl,casesReq).subscribe(casesRepsonse=>{
          console.log('casesRepsonse',casesRepsonse);
          if (casesRepsonse['result']['codeType'] == 'S') {
            if (casesRepsonse['data']['length'] > 0) {
              var uniqueCaseId = [];
              const map1 = new Map();
              for (const item of casesRepsonse['data']) {
                if(!map1.has(item.caseId)){
                  map1.set(item.caseId, true);    // set any value to Map
                  uniqueCaseId.push(item.caseId);
                }
              }
              this.fetchJobOrderForReporting(uniqueCaseId).then( jobResponse =>{

              })
            }
            else{
              this.ngxLoader.stop();
              this.notifier.notify('error','No case Found for reporting')
            }

          }
          else{
            this.ngxLoader.stop();
            this.notifier.notify('error','Error while loading Cases')
          }

        },error=>{
          this.ngxLoader.stop();
          this.notifier.notify('error','Error while loading Cases')
        })
      }

    }

    fetchJobOrderForReporting(uniqueCaseId){
      return new Promise ((resolve, reject) => {
        var jobUrl = environment.API_COVID_ENDPOINT + 'showreportingjobs';
        var jobReq = {...this.showReportingJobsReq}
        // console.log('this.showReportingJobsReq',this.showReportingJobsReq);
        // console.log('jobReq',jobReq);

        jobReq.header['userCode']     = this.logedInUserRoles.userCode;
        jobReq.header['partnerCode']  = this.logedInUserRoles.partnerCode;
        jobReq.data.caseIds           = uniqueCaseId;
        jobReq.data.testStatusId      = '3';

        this.globalService.jobOrderForReporting(jobUrl,jobReq).subscribe(jobsRepsonse=>{
          console.log('jobsRepsonse',jobsRepsonse);
          var tempHolder = {};
          var temp = [];
          if (jobsRepsonse['result']['codeType'] == 'S') {
            if (jobsRepsonse['data'].length > 0) {
              var uniqueJobOrderIds = [];
              const map1 = new Map();
              for (let i = 0; i < jobsRepsonse['data'].length; i++) {
                for (let j = 0; j < uniqueCaseId.length; j++) {
                  if (uniqueCaseId[j] == jobsRepsonse['data'][i]['caseId']) {
                    tempHolder['caseId'] = jobsRepsonse['data'][i]['caseId'];
                    tempHolder['jobOrderId'] = jobsRepsonse['data'][i]['jobOrderId'];
                    temp.push(tempHolder);
                    tempHolder = {};
                    if(!map1.has(jobsRepsonse['data'][i]['jobOrderId'])){
                      map1.set(jobsRepsonse['data'][i]['jobOrderId'], true);    // set any value to Map
                      uniqueJobOrderIds.push(jobsRepsonse['data'][i]['jobOrderId']);
                    }
                  }

                }

              }
              console.log("uniqueJobOrderIds",uniqueJobOrderIds);


              this.GLobalUniqueJoBOrderIdsForHistory = uniqueJobOrderIds;
              // this.fetchFinalReport(uniqueJobOrderIds,temp).then( jobResponse =>{
              //
              // })
              console.log("*/*/*/*/*/*");

              $('#delivery-historyModal').modal('show');
              this.triggerHistoryTable = true;
              this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
                dtElement.dtInstance.then((dtInstance: any) => {
                  if (dtInstance.table().node().id == 'rep-history' ) {
                    dtInstance.ajax.reload();

                  }
                })
              })
              this.clearCheckBoxes();
            }
            else{
              this.ngxLoader.stop();
              this.notifier.notify('error','Error while loading Job Order Ids Case does not exists')
            }
          }
          else{
            this.ngxLoader.stop();
            this.notifier.notify('error','Error while loading Job Order Ids')
          }

        },error=>{
          this.ngxLoader.stop();
          this.notifier.notify('error','Error while loading Job Order Ids')
        })
      })
    }

    fetchFinalReport(uniqueJobOrderIds,temp){
      return new Promise ((resolve, reject) => {
        var jobUrl = environment.API_REPORTING_ENDPOINT + 'showreporthistory';

        var jobReq = {...this.finalReportReq}
        jobReq.header.userCode    = this.logedInUserRoles.userCode;
        jobReq.header.partnerCode = this.logedInUserRoles.partnerCode;
        jobReq.header.functionalityCode = "MRMA-MR";
        if (this.selectedIntakeforClient != "none") {
          jobReq.data.intakeId   = uniqueJobOrderIds;
          jobReq.data.jobOrderIds =[];

        }
        else{
          jobReq.data.jobOrderIds   = uniqueJobOrderIds;
          jobReq.data.intakeId   = "";
        }

        jobReq.data.pageSize      = this.paginateLengthHistory;
        jobReq.data.pageNumber    = this.paginatePaheHistory;
        // jobReq.data.   = uniqueJobOrderIds;

        this.globalService.finalReport(jobUrl,jobReq).subscribe(reportsRepsonse=>{
          console.log('reportsRepsonse',reportsRepsonse);
          this.triggerHistoryTable = false;
          var temp = {};
          if (reportsRepsonse['result']['codeType'] == 'S') {
            for (let i = 0; i < reportsRepsonse['data']['history'].length; i++) {
              for (let j = 0; j < this.reportPackages.length; j++) {
                if (reportsRepsonse['data']['history'][i]['reportPackageId'] == this.reportPackages[j]['reportPackageId']) {
                  reportsRepsonse['data']['history'][i]['reportPackageName'] = this.reportPackages[j]['reportPackageName'];
                }
              }
              if (reportsRepsonse['data']['history'][i]['deliveryStatusId'] == 1) {
                reportsRepsonse['data']['history'][i]['deliveryStatusName'] = 'Pending';
              }
              else if (reportsRepsonse['data']['history'][i]['deliveryStatusId'] == 2) {
                reportsRepsonse['data']['history'][i]['deliveryStatusName'] = 'Sent';
              }
              else{
                reportsRepsonse['data']['history'][i]['deliveryStatusName'] = 'Error';
              }

            }
            // console.log('reportsRepsonse',reportsRepsonse);
            const uniqueUserIds = [];
            const map = new Map();
            for (const item of reportsRepsonse['data']['history']) {
              if(!map.has(item.sentBy)){
                map.set(item.sentBy, true);    // set any value to Map
                uniqueUserIds.push(item.sentBy);
              }
            }
            console.log('uniqueUserIds',uniqueUserIds);
            var userRequest = {...this.usersRequest};
            userRequest.data.userCodes = uniqueUserIds;
            userRequest.header.partnerCode  = this.logedInUserRoles.partnerCode;
            userRequest.header.userCode     = this.logedInUserRoles.userCode;
            userRequest.header.functionalityCode     = "CLTA-VC";
            var userUrl = environment.API_USER_ENDPOINT + "clientusers";
            this.globalService.getUserRecord(userUrl,userRequest).subscribe(getUserResp =>{
              // return getUserResp;
              console.log("getUserResp",getUserResp);
              if (getUserResp['data'] != null) {
                for (let k = 0; k < reportsRepsonse['data']['history'].length; k++) {
                  for (let l = 0; l < getUserResp['data'].length; l++) {
                    if(reportsRepsonse['data']['history'][k].sentBy == getUserResp['data'][l].userCode){
                      reportsRepsonse['data']['history'][k].sentByName = getUserResp['data'][l].fullName;
                    }

                  }
                }
                this.allReportsData = reportsRepsonse['data']['history'];
                this.allHistoryDataCount = reportsRepsonse['data']['totalCount'];
                console.log('this.allHistoryData',reportsRepsonse['data']['history']);
                // $('#delivery-historyModal').modal('show');
                resolve();
              }

            },error=>{
              this.ngxLoader.stop();
              this.notifier.notify("error","error While loading sent by")
              resolve()
            })


          }
          else{
            this.ngxLoader.stop();
            // this.notifier.notify('info',reportsRepsonse['result']['description'])
            resolve()
          }

        },error=>{
          this.triggerHistoryTable = false;
          this.ngxLoader.stop();
          this.notifier.notify('error','Error while fetching Report')
          resolve()
        })
      })
    }

    setCaseReorcdForDownloadReport(clientId, intakeId, jobOrderId){

      const found = this.pendingReport.some(el => {
        el.jobOrderId == jobOrderId;
        if (el.jobOrderId == jobOrderId) {
          // console.log('el Pending',el);
          this.downloadReport(4,1,1,1,'pending',el)
        }
      });

      const found1 = this.deliveredReports.some(el => {
        el.jobOrderId == jobOrderId
        if (el.jobOrderId == jobOrderId) {
          // console.log('el Delivered',el);
          this.downloadReport(4,1,1,1,'delivered',el)
        }
      });

      const found2 = this.notDeliveredReports.some(el => {
        el.jobOrderId == jobOrderId;
        if (el.jobOrderId == jobOrderId) {
          // console.log('el Not Delivered',el);
          this.downloadReport(4,1,1,1,'notdelivered',el)
        }
      });
    }

    setCaseReorcdForResendReport(clientId, intakeId, jobOrderId){

      const found = this.pendingReport.some(el => {
        el.jobOrderId == jobOrderId;
        if (el.jobOrderId == jobOrderId) {
          // console.log('el Pending',el);
          this.detailedReportSingle(4,1,1,1,'pending',el)
        }
      });

      const found1 = this.deliveredReports.some(el => {
        el.jobOrderId == jobOrderId
        if (el.jobOrderId == jobOrderId) {
          // console.log('el Delivered',el);
          this.detailedReportSingle(4,1,1,1,'delivered',el)
        }
      });

      const found2 = this.notDeliveredReports.some(el => {
        el.jobOrderId == jobOrderId;
        if (el.jobOrderId == jobOrderId) {
          // console.log('el Not Delivered',el);
          this.detailedReportSingle(4,1,1,1,'notdelivered',el)
        }
      });
    }

    resendReport(reportId, reportPackageId){
      this.ngxLoader.start();
      console.log("item",this.clientSearchString);
      // return;

      var resendRequest = {
        header: {
          uuid: "67ffaebe-6188-476e-86ae-c5deb2effa80",
          partnerCode: "",
          userCode: "sip-12598",
          referenceNumber: "TWMM12032020115628",
          systemCode: "NGLIS",
          moduleCode: "TWMM",
          functionalityCode: "TWMA-UR",
          systemHostAddress: "",
          remoteUserAddress: "",
          dateTime: ""
        },
        data: {
          reportId: 251,
          clientName: "Menna",
          clientEmail:"",
          reportType:"detailed consolidated"
        }
      }
      var packageName = '';

      if (reportPackageId == 1) {
        packageName = 'compressed';
      }
      else if (reportPackageId == 2) {
        packageName = 'detailed consolidated';
      }
      else if (reportPackageId == 3) {
        packageName = 'summary';
      }
      else if (reportPackageId == 4) {
        packageName = 'individual';
      }

      var clientEmail = '';
      var clURL           = environment.API_CLIENT_ENDPOINT + "clientidnameemail"
      var clReqData = {
        header:{
          uuid               :"",
          partnerCode        :"",
          userCode           :"",
          referenceNumber    :"",
          systemCode         :"",
          moduleCode         :"CLIM",
          functionalityCode: "MRMA-MR",
          systemHostAddress  :"",
          remoteUserAddress  :"",
          dateTime           :""
        },
        data:{
          clientIds:[this.clientSearchString.clientId],
          userCodes: []
        }
      }
      this.globalService.getClientByName(clURL,clReqData).then(selectedClient => {
        if (typeof selectedClient['data'] !='undefined') {
          if (selectedClient['data'] != null) {
            clientEmail = selectedClient['data'][0]['email'];
            resendRequest.header.partnerCode = this.logedInUserRoles.partnerCode;
            resendRequest.header.userCode    = this.logedInUserRoles.userCode;
            resendRequest.header.functionalityCode    = "MRMA-MR";
            resendRequest.data.reportId      = reportId;
            resendRequest.data.reportType    = packageName;
            resendRequest.data.clientName    = this.clientSearchString.clientName;
            resendRequest.data.clientEmail   = clientEmail;
            console.log("resendRequest",resendRequest);
            // return;
            var url = environment.API_REPORTING_ENDPOINT+'resendreport';
            this.globalService.resendReport(url,resendRequest).subscribe(resendResponse=>{
              this.ngxLoader.stop();
              console.log("resendResponse",resendResponse);
              if (resendResponse['result']['codeType'] == 'S') {
                // this.notifier.notify('success',"report send successfully")
                this.notifier.notify( "success", "The Detailed report has been delivered to "+this.clientSearchString.clientName+" successfully");
                const downloadLink = document.createElement("a");

                downloadLink.href = resendResponse['data']['base64EncodedFile'];
                downloadLink.download = resendResponse['data']['fileName'];;
                //downloadLink.click();
              }
              else{
                this.ngxLoader.stop();
                this.notifier.notify('error',"Error while resending report")
              }
            }, error=>{
              this.ngxLoader.stop();
              this.notifier.notify('error',"Error while resending report")
            })
          }
        }
      },error=>{
        this.notifier.notify('warning','error while getting client email')
      })
      // if (this.clientSearchString['clientContactsByClientId'].length > 0) {
      //   for (let cl = 0; cl < this.clientSearchString['clientContactsByClientId'].length; cl++) {
      //     if (this.clientSearchString['clientContactsByClientId'][cl]['contactByContactId'].length >0) {
      //       if (this.clientSearchString['clientContactsByClientId'][cl]['contactByContactId'][0]['contactTypeId'] == 2) {
      //         clientEmail = this.clientSearchString['clientContactsByClientId'][cl]['contactByContactId'][0]['value']
      //       }
      //     }
      //   }
      // }


    }

    downloadOldReport(reportId,clientName,packageId){
      console.log("clientName",clientName);
      // console.log("clietnSearchString",this.clientSearchString);
      var type = 'pdf';
      if (packageId == 1) {
        type = 'zip'
      }
      else {
        type = 'pdf'
      }

      var downloadAgainRequest = {
        header: {
          uuid: "67ffaebe-6188-476e-86ae-c5deb2effa80",
          partnerCode: "sip",
          userCode: "sip-12598",
          referenceNumber: "TWMM12032020115628",
          systemCode: "NGLIS",
          moduleCode: "TWMM",
          functionalityCode: "TWMA-UR",
          systemHostAddress: "",
          remoteUserAddress: "",
          dateTime: ""
        },
        data: {
          reportId: 173,
          clientName: "Menna",
          timeZoneId:""
        }
      }
      var currentZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
      downloadAgainRequest.data['timeZoneId'] = currentZone;

      downloadAgainRequest.header.userCode    = this.logedInUserRoles.userCode;
      downloadAgainRequest.header.partnerCode = this.logedInUserRoles.partnerCode;
      downloadAgainRequest.header.functionalityCode = "MRMA-DRI";
      downloadAgainRequest.data.reportId      = reportId;
      downloadAgainRequest.data.clientName    = clientName;

      var redownloadURl = environment.API_REPORTING_ENDPOINT + 'downloadoldreport';
      this.globalService.downloadFiles(redownloadURl,downloadAgainRequest,type).subscribe(redownloadResponse =>{
        console.log("redownloadResponse",redownloadResponse);

      })


    }

    composeAddress(countryId,stateId,cityId,street,floor,zip){
      var stateabb = '';
      var cityName = '';
      var floorName = '';
      var state = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=STATE_CACHE&partnerCode=sip&id='+countryId).then(response =>{
        return response;
        // console.log("states",response);
      })
      var city = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=CITY_CACHE&partnerCode=sip&id='+stateId).then(response =>{
        return response;
        // console.log("states",response);
      })

      forkJoin([state, city]).subscribe(allLookups => {
        console.log("all Address",allLookups);
        for (let i = 0; i < allLookups[0]['length']; i++) {
          if (stateId == allLookups[0][i]['stateId']) {
            stateabb = allLookups[0][i]['stateName']
          }
        }
        for (let j = 0; j < allLookups[1]['length']; j++) {
          if (cityId == allLookups[1][j]['cityId']) {
            cityName = allLookups[1][j]['cityName']
          }
        }

        if (typeof floor != "undefined" || floor != null || floor != '') {
          floorName = floor;
        }

        var compeleteAddress = this.encryptDecrypt.decryptString(floorName)+". "+this.encryptDecrypt.decryptString(street)+". "+cityName+". "+stateabb+". "+zip;
        // var compeleteAddress = floorName+". "+street+". "+cityName+". "+stateabb+". "+zip;

        this.compeleteAddressForReport = compeleteAddress;

      })
    }
    checkReportEmailStatus(){
      this.calledonce = true;
      this.reportMessageCheck = JSON.parse(localStorage.getItem('reportStatusArray'))

      console.log("in interval",this.reportMessageCheck);
      if (this.reportMessageCheck != null && this.reportMessageCheck['length'] > 0) {
        if (this.reportMessageCheck[0]['status'] == false) {
          this.globalService.getLookUps(environment.API_REPORTING_ENDPOINT+'reportstatus/'+this.reportMessageCheck[0]['reportId']).then(response =>{
            console.log('response',response);
            if (response['reportStatus'] != 1) {
              if (response['reportStatus'] == 2) {
                this.notifier.notify('success',"The report has been delivered successfully to "+this.clientSearchString.clientName )
                this.reportMessageCheck.splice(0, 1);
                localStorage.setItem('reportStatusArray',JSON.stringify(this.reportMessageCheck));
                this.calledonce = false;
              }
              else if (response['reportStatus'] == 3) {
                this.notifier.notify('info',"Email was not delivered to "+this.clientSearchString.clientName )
                this.reportMessageCheck.splice(0, 1);
                localStorage.setItem('reportStatusArray',JSON.stringify(this.reportMessageCheck));
                this.calledonce = false;
              }

            }
            else{
              this.calledonce = false;
            }

          },errro=>{
            this.calledonce = false;
          })
        }
      }
      else{
        this.calledonce = false;
        this.subscription.unsubscribe();
      }
      // var count = 0;
      // for (let i = 0; i < this.reportMessageCheck.length; i++) {
      //   count ++;
      //   if (this.reportMessageCheck[i]['status'] == false) {
      //     this.globalService.getLookUps(environment.API_SPECIMEN_ENDPOINT+'reportstatus/'+this.reportMessageCheck[i]['reportId']).then(response =>{
      //       console.log('response',response);
      //       if (response['reportStatus'] != 1) {
      //
      //       }
      //
      //     })
      //   }
      // }


      // this.unsubscribe();

    }
    stratInterval(){
      console.log("here");

      this.subscription = interval(15000).subscribe(x => {

        if (this.calledonce == false) {
          this.checkReportEmailStatus();

        }
      });
    }
    goToEditonCaseNumber(caseNumber){
      var a = caseNumber+';0';
      var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
      var bx = ax.replace('+','xsarm');
      var cx = bx.replace('/','ydan');
      var ciphertext = cx.replace('=','zhar');
      console.log('ciphertext',ciphertext);
      // this.router.navigate(['edit-case', ciphertext]);
      this.router.navigateByUrl('/edit-case/'+ciphertext+'/1')
    }
    handlePatientandCaseFromSearch(caseData,clientData, patientDat, type,searchString){
      return new Promise((resolve, reject) => {
        var uniquePatientId = [];
        const map = new Map();
        for (const item of patientDat['data']['patients']) {
          if(!map.has(item.patientId)){
            map.set(item.patientId, true);    // set any value to Map
            uniquePatientId.push(item.patientId);

          }
        }

        var status = []
        if (type == 'pending') {
          status = [6]
        }
        else if (type == 'delivered') {
          status = [10]
        }
        else if (type == 'notdelivered') {
          status = [12]
        }
        var specUrl = environment.API_SPECIMEN_ENDPOINT + "findbypatientid"
        var reqPid  ={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "MRMA-MR",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{
          patientIds:uniquePatientId,
          intakeId:this.selectedIntakeforClient,client:this.clientSearchString.clientId, statuses:status, userCode:""}}
          if (this.logedInUserRoles.userType == 2) {
            reqPid.data.userCode = this.logedInUserRoles.userCode;
          }
          this.globalService.findByPatientIds(specUrl, reqPid).subscribe(respPID =>{
            console.log("Ptient and case Patient",respPID);
            console.log("caseData",caseData);
            if (respPID['data']['length'] == 0) {
              var a9 = caseData['data']['caseDetails'];
              const uniqueIds = [];
              const uniqueCLIds = [];
              const map = new Map();
              for (const item of a9) {
                if(!map.has(item.clientId)){
                  map.set(item.clientId, true);    // set any value to Map
                  uniqueCLIds.push(item.clientId);
                }
              }
              const map1 = new Map();
              for (const item of a9) {
                if(!map1.has(item.patientId)){
                  map1.set(item.patientId, true);    // set any value to Map
                  uniqueIds.push(item.patientId);
                }
              }
              this.getPatClData(uniqueIds,caseData,uniqueCLIds,type).then(respAlldata => {
                resolve()
              });

            }
            else{

              const combined2 = [...respPID['data'], ...caseData['data']['caseDetails']];
              console.log("combined2",combined2);
              var combinedDataToForward = {
                data:{}
              }
              var a8 = combined2;
              const uniqueIds = [];
              const uniqueCLIds = [];
              const map = new Map();
              for (const item of a8) {
                if(!map.has(item.clientId)){
                  map.set(item.clientId, true);    // set any value to Map
                  uniqueCLIds.push(item.clientId);
                }
              }
              const map1 = new Map();
              for (const item of a8) {
                if(!map1.has(item.patientId)){
                  map1.set(item.patientId, true);    // set any value to Map
                  uniqueIds.push(item.patientId);
                }
              }
              combinedDataToForward.data = combined2

              // getPatReq.data.patientIds = uniqueIds;
              this.getPatClData(uniqueIds,combinedDataToForward,uniqueCLIds,type).then(respAlldata => {
                resolve()
              });

            }



          })
          resolve()
        })
      }

      handleClientandCaseFromSearch(caseData,clientData, patientDat, type,searchString){
        return new Promise((resolve, reject) => {
          var uniqueClientId = [];
          const map1 = new Map();
          for (const item of clientData) {
            if(!map1.has(item.clientId)){
              map1.set(item.clientId, true);    // set any value to Map
              uniqueClientId.push(item.clientId);
            }
          }
          var clientStatuses = []
          if (type == 'pending') {
            clientStatuses = [1,2,3,4,5,7,8,9,10,11,12]
          }
          else if (type == 'delivered') {
            clientStatuses = [1,2,3,4,5,7,8,9,6,11,12]
          }
          else if (type == 'notdelivered') {
            clientStatuses = [1,2,3,4,5,7,8,9,10,11,6]
          }


          let columnCaseReq  = {}
          var columnUrl      = environment.API_SPECIMEN_ENDPOINT + 'searchbyclient';
          columnCaseReq      = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "MRMA-MR",systemHostAddress:"",remoteUserAddress:"",dateTime:""},
          data:{page:"0",size:20,sortColumn:"caseNumber",sortingOrder:"desc",
          clientId: uniqueClientId,
          intakeId: this.selectedIntakeforClient,
          statuses:[],
          userCode:""}}
          if (this.logedInUserRoles.userType == 2) {
            columnCaseReq['data'].userCode = this.logedInUserRoles.userCode;
          }
          this.http.post(columnUrl, columnCaseReq).subscribe(byClientIdsResp=>{
            console.log("Client and Case Resp",byClientIdsResp);
            if (byClientIdsResp['data']['caseDetails'].length == 0) {
              var ab = caseData['data']['caseDetails'];
              const uniqueIds = [];
              const uniqueCLIds = [];
              const map = new Map();
              for (const item of ab) {
                if(!map.has(item.clientId)){
                  map.set(item.clientId, true);    // set any value to Map
                  uniqueCLIds.push(item.clientId);
                }
              }
              const map1 = new Map();
              for (const item of ab) {
                if(!map1.has(item.patientId)){
                  map1.set(item.patientId, true);    // set any value to Map
                  uniqueIds.push(item.patientId);
                }
              }
              this.getPatClData(uniqueIds,caseData,uniqueCLIds,type).then(respAlldata => {
                resolve()
              });
            }
            else{
              const combined2 = [...byClientIdsResp['data']['caseDetails'], ...caseData['data']['caseDetails']];
              console.log("combined2",combined2);
              var combinedDataToForward = {
                data:{}
              }
              var aa = combined2;
              const uniqueIds = [];
              const uniqueCLIds = [];
              const map = new Map();
              for (const item of aa) {
                if(!map.has(item.clientId)){
                  map.set(item.clientId, true);    // set any value to Map
                  uniqueCLIds.push(item.clientId);
                }
              }
              const map1 = new Map();
              for (const item of aa) {
                if(!map1.has(item.patientId)){
                  map1.set(item.patientId, true);    // set any value to Map
                  uniqueIds.push(item.patientId);
                }
              }
              combinedDataToForward.data = combined2

              // getPatReq.data.patientIds = uniqueIds;
              this.getPatClData(uniqueIds,combinedDataToForward,uniqueCLIds,type).then(respAlldata => {
                resolve()
              });

            }



          })
          resolve()
        })
      }

      handleClientPatientandCaseFromSearch(caseData,clientData, patientDat, type,searchString){
        return new Promise((resolve, reject) => {
          var uniqueClientId = [];
          const map1 = new Map();
          for (const item of clientData) {
            if(!map1.has(item.clientId)){
              map1.set(item.clientId, true);    // set any value to Map
              uniqueClientId.push(item.clientId);
            }
          }
          var uniquePatientId = [];
          const map = new Map();
          for (const item of patientDat['data']['patients']) {
            if(!map.has(item.patientId)){
              map.set(item.patientId, true);    // set any value to Map
              uniquePatientId.push(item.patientId);

            }
          }
          var clientStatuses = []
          if (type == 'pending') {
            clientStatuses = [1,2,3,4,5,7,8,9,10,11,12]
          }
          else if (type == 'delivered') {
            clientStatuses = [1,2,3,4,5,7,8,9,6,11,12]
          }
          else if (type == 'notdelivered') {
            clientStatuses = [1,2,3,4,5,7,8,9,10,11,6]
          }


          let columnCaseReq  = {}
          var columnUrl      = environment.API_SPECIMEN_ENDPOINT + 'searchbyclient';
          columnCaseReq      = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "MRMA-MR",systemHostAddress:"",remoteUserAddress:"",dateTime:""},
          data:{page:"0",size:20,sortColumn:"caseNumber",sortingOrder:"desc",
          clientId: uniqueClientId,
          intakeId: this.selectedIntakeforClient,
          statuses:[],
          userCode:""}}
          if (this.logedInUserRoles.userType == 2) {
            columnCaseReq['data'].userCode = this.logedInUserRoles.userCode;
          }
          var clientsout = this.http.post(columnUrl, columnCaseReq).toPromise().then(byClientIdsResp=>{
            console.log("Client and Case Resp",byClientIdsResp);
            return byClientIdsResp;
          })
          var status = []
          if (type == 'pending') {
            status = [6]
          }
          else if (type == 'delivered') {
            status = [10]
          }
          else if (type == 'notdelivered') {
            status = [12]
          }
          var byPidURL = environment.API_SPECIMEN_ENDPOINT + 'findbypatientid';
          var reqPid={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "MRMA-MR",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{
            patientIds:uniquePatientId,
            intakeId:this.selectedIntakeforClient,client:this.clientSearchString.clientId, statuses:status, userCode:""
          }}
          if (this.logedInUserRoles.userType == 2) {
            reqPid.data.userCode = this.logedInUserRoles.userCode;
          }
          var patientout = this.http.put(byPidURL, reqPid).toPromise().then(respPID=>{
            console.log("Ptient and case Patient",respPID);
            return respPID;
          })


          forkJoin([patientout, clientsout]).subscribe(bothresults => {
            var byClientIdsResp = bothresults[1];
            var respPID= bothresults[0];

            if (byClientIdsResp['data']['caseDetails'].length == 0 && respPID['data']['length'] == 0) {
              var a1 = caseData['data']['caseDetails'];
              const uniqueIds = [];
              const uniqueCLIds = [];
              const map = new Map();
              for (const item of a1) {
                if(!map.has(item.clientId)){
                  map.set(item.clientId, true);    // set any value to Map
                  uniqueCLIds.push(item.clientId);
                }
              }
              const map1 = new Map();
              for (const item of a1) {
                if(!map1.has(item.patientId)){
                  map1.set(item.patientId, true);    // set any value to Map
                  uniqueIds.push(item.patientId);
                }
              }
              this.getPatClData(uniqueIds,caseData,uniqueCLIds,type).then(respAlldata => {
                resolve()
              });
            }
            else if (byClientIdsResp['data']['caseDetails'].length > 0 && respPID['data']['length'] == 0) {
              const combined2 = [...byClientIdsResp['data']['caseDetails'], ...caseData['data']['caseDetails']];
              console.log("combined2",combined2);
              var combinedDataToForward = {
                data:{}
              }
              var a2 = combined2;
              const uniqueIds = [];
              const uniqueCLIds = [];
              const map = new Map();
              for (const item of a2) {
                if(!map.has(item.clientId)){
                  map.set(item.clientId, true);    // set any value to Map
                  uniqueCLIds.push(item.clientId);
                }
              }
              const map1 = new Map();
              for (const item of a2) {
                if(!map1.has(item.patientId)){
                  map1.set(item.patientId, true);    // set any value to Map
                  uniqueIds.push(item.patientId);
                }
              }
              combinedDataToForward.data = combined2

              // getPatReq.data.patientIds = uniqueIds;
              this.getPatClData(uniqueIds,combinedDataToForward,uniqueCLIds,type).then(respAlldata => {
                resolve()
              });
            }
            else if (byClientIdsResp['data']['caseDetails'].length == 0 && respPID['data']['length'] > 0) {
              const combined2 = [...respPID['data'], ...caseData['data']['caseDetails']];
              console.log("combined2",combined2);
              var combinedDataToForward = {
                data:{}
              }
              var a3 = combined2;
              const uniqueIds = [];
              const uniqueCLIds = [];
              const map = new Map();
              for (const item of a3) {
                if(!map.has(item.clientId)){
                  map.set(item.clientId, true);    // set any value to Map
                  uniqueCLIds.push(item.clientId);
                }
              }
              const map1 = new Map();
              for (const item of a3) {
                if(!map1.has(item.patientId)){
                  map1.set(item.patientId, true);    // set any value to Map
                  uniqueIds.push(item.patientId);
                }
              }
              combinedDataToForward.data = combined2

              // getPatReq.data.patientIds = uniqueIds;
              this.getPatClData(uniqueIds,combinedDataToForward,uniqueCLIds,type).then(respAlldata => {
                resolve()
              });
            }
            else if (byClientIdsResp['data']['caseDetails'].length > 0 && respPID['data']['length'] > 0) {
              const combined2 = [...respPID['data'], ...caseData['data']['caseDetails']];
              const combined3 = [...byClientIdsResp['data']['caseDetails'], ...combined2];
              console.log("combined3",combined3);
              var combinedDataToForward = {
                data:{}
              }
              var a4 = combined3;
              const uniqueIds = [];
              const uniqueCLIds = [];
              const map = new Map();
              for (const item of a4) {
                if(!map.has(item.clientId)){
                  map.set(item.clientId, true);    // set any value to Map
                  uniqueCLIds.push(item.clientId);
                }
              }
              const map1 = new Map();
              for (const item of a4) {
                if(!map1.has(item.patientId)){
                  map1.set(item.patientId, true);    // set any value to Map
                  uniqueIds.push(item.patientId);
                }
              }
              combinedDataToForward.data = combined3;

              // getPatReq.data.patientIds = uniqueIds;
              this.getPatClData(uniqueIds,combinedDataToForward,uniqueCLIds,type).then(respAlldata => {
                resolve()
              });
            }



            resolve()
          })
        })
      }

      enableSendButton(e,type){
        // console.log('e.target.vlaue',e.target.checked);

        if (e.target.checked == true) {
          if (type == 'pending') {
            this.enableSendButtonPending.push(1);
            this.enableButtonPending = true;
          }
          else if (type == 'delivered') {
            this.enableSendButtonDelivered.push(1);
            this.enableButtonDelivered = true;
          }
          else if (type == 'notdelivered') {
            this.enableSendButtonNotDelivered.push(1);
            this.enableButtonNotDelivered = true;
          }

        }
        else{
          if (type == 'pending') {
            this.enableSendButtonPending.shift();
            if (this.enableSendButtonPending['length'] == 0) {
              this.enableButtonPending = false;
            }
          }
          else if (type == 'delivered') {
            this.enableSendButtonDelivered.shift();
            if (this.enableSendButtonDelivered['length'] == 0) {
              this.enableButtonDelivered = false;
            }
          }
          else if (type == 'notdelivered') {
            this.enableSendButtonNotDelivered.shift();
            if (this.enableSendButtonNotDelivered['length'] == 0) {
              this.enableButtonNotDelivered = false;
            }
          }

        }
      }

      clearCheckBoxes(){
        this.enableButtonPending = false;
        this.enableButtonDelivered = false;
        this.enableButtonNotDelivered = false;
        this.enableSendButtonPending = [];
        this.enableSendButtonDelivered = [];
        this.enableSendButtonNotDelivered = [];
        this.listForconsolidatedReport = [];
        this.enablePendingReportTypeCheckBoxes = false;
        this.listForconsolidatedReportDelivered = [];
        this.enableDeliveredReportTypeCheckBoxes = false;
        this.listForconsolidatedReportnotDelivered = [];
        this.enableNotDeliveredReportTypeCheckBoxes = false;
        this.mrc = false;
        this.dcr = false;
        this.cr = false;

        this.mrc1 = false;
        this.dcr1 = false;
        this.cr1 = false;

        this.mrc2 = false;
        this.dcr2 = false;
        this.cr2 = false;
        $('#check-all1')[0].checked = false;
        $('#check-all2')[0].checked = false;
        $('#check-all3')[0].checked = false;
      }



    }
