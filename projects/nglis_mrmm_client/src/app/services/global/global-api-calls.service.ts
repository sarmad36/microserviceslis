import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse, HttpEvent } from '@angular/common/http';
import { of } from 'rxjs';
import { BehaviorSubject, Observable, throwError, Subject } from 'rxjs';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
// import { catchError, retry, finalize, tap, map, takeUntil, delay } from 'rxjs/operators';
import { catchError, debounceTime, distinctUntilChanged, switchMap, tap, delay, map } from "rxjs/operators";
import { environment } from '../../../../../../src/environments/environment';
//import { saveAs } from 'file-saver';
import * as CryptoJS from 'crypto-js';



@Injectable({
  providedIn: 'root'
})
export class GlobalApiCallsService {

  ////////// for seacrch client
  baseUrl: string = 'https://api.cdnjs.com/libraries';
  queryUrl: string = '?search=';
  public searchClientName = {
    header:{
      uuid               :"",
      partnerCode        :"",
      userCode           :"",
      referenceNumber    :"",
      systemCode         :"",
      moduleCode         :"CLIM",
      functionalityCode  :"MRMA-MR",
      systemHostAddress  :"",
      remoteUserAddress  :"",
      dateTime           :""
    },
    data:{
      sortColumn  :"name",
      sortingOrder:"asc",
      searchString:"",
      userCode    : ""
    }
  }
  public corsHeaders: any = new HttpHeaders({
    'Content-Type': 'application/json',
    'Accept': 'application/json',
  });

  public headerOcta: any = new HttpHeaders({
    'Content-Type': 'application/octet-stream'
  });
  public logedInUserRoles :any = {};

  constructor(
    private http: HttpClient,
    private sanitizer       : DomSanitizer,) {
      let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
      var originalText = bts.toString(CryptoJS.enc.Utf8);
      this.logedInUserRoles = JSON.parse(originalText)

    }

    getLookUps(url){
      return this.http.get(url)
      .pipe()
      .toPromise()
      .then( resp => {
        return resp;
      })
      .catch(error => {
        catchError(this.errorHandler)
      });
    }
    searchMinAccession(url, saveData){
      return this.http.post<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }

    searchClientByName(url, saveData){
      return this.http.post<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    ///////////////////////////// intake
    getIntakes(url, saveData){
      return this.http.post<any>(url, saveData)
      .pipe(
        catchError(this.errorHandler)
      )
    }

    //  getPeople(term: string = null): Observable<any> {
    //    let items = getMockPeople();
    //    if (term) {
    //      items = items.filter(x => x.name.toLocaleLowerCase().indexOf(term.toLocaleLowerCase()) > -1);
    //    }
    //    // console.log("items service",items);

    //    return of(items).pipe(delay(500));
    //  }

    search(terms) {
      return terms.pipe(debounceTime(400))
      .pipe(distinctUntilChanged())
      .pipe(switchMap(term => this.searchEntries(term)));
    }

    searchEntries(term) {
      // if (term.length >=3 || term.length == 0) {
      this.searchClientName.data.searchString = term;
      let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
      var originalText = bts.toString(CryptoJS.enc.Utf8);
      this.logedInUserRoles = JSON.parse(originalText)


      if (this.logedInUserRoles.userType == 2) {
        this.searchClientName.data.userCode = this.logedInUserRoles.userCode;
      }
      // if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-UC") != -1) {
      //   this.searchClientName['header'].functionalityCode    = "CLTA-UC";
      // }
      // else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-VC") != -1){
      //   this.searchClientName['header'].functionalityCode    = "CLTA-VC";
      // }
      // else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-CD") != -1){
      //   this.searchClientName['header'].functionalityCode    = "CLTA-CD";
      // }
      this.searchClientName['header'].userCode = this.logedInUserRoles.userCode;
      this.searchClientName['header'].partnerCode = this.logedInUserRoles.partnerCode;
      this.searchClientName['header'].functionalityCode    = "CLTA-MC";
      const searchURL    = environment.API_CLIENT_ENDPOINT + 'searchclientidnamebyname';
      return this.http
      // .get(this.baseUrl + this.queryUrl + term)
      .post(searchURL,this.searchClientName)
      .pipe(map(res => res)).pipe(
        catchError(this.errorHandler)
      )
      // }


    }

    searchColum(url,data, terms) {
      // var terms =
      return terms.pipe(debounceTime(400))
      .pipe(distinctUntilChanged())
      .pipe(switchMap(term => this.searchEntriesColum(term, url,data)));
    }
    searchEntriesColum(term, url,data) {
      // if (term.length >=3 || term.length == 0) {
      // this.searchClientName.searchString = term;
      // const searchURL    = environment.API_CLIENT_ENDPOINT + 'searchclientbyname';
      return this.http
      // .get(this.baseUrl + this.queryUrl + term)
      .post(url,data)
      .pipe(map(res => res)).pipe(
        catchError(this.errorHandler)
      )
      // }
    }

    searchColumPatient(url,data, terms) {
      // var terms =
      return terms.pipe(debounceTime(400))
      .pipe(distinctUntilChanged())
      .pipe(switchMap(term => this.searchEntriesColumPatient(term, url,data)));
    }
    searchEntriesColumPatient(term, url,data) {
      return this.http
      // .get(this.baseUrl + this.queryUrl + term)
      .put(url,data)
      .pipe(map(res => res)).pipe(
        catchError(this.errorHandler)
      )
      // }


    }
    getPatientsByIds(url, saveData){
      return this.http.put(url,saveData)
      .pipe()
      .toPromise()
      .then( resp => {
        // console.log('resp: ', resp);

        // Set loader false
        return resp;
      })
      .catch(error => {
        catchError(this.errorHandler)
      });
      // return this.http
      // .put(url, saveData)
      // .pipe(
      //   catchError(this.errorHandler)
      // )
    }

    findPatient(url, saveData){
      return this.http.put<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }

    getClientByIds(url, saveData){
      return this.http.post<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    getClientByName(url, saveData){
      return this.http.post(url,saveData)
      .pipe()
      .toPromise()
      .then( resp => {
        // console.log('resp: ', resp);

        // Set loader false
        return resp;
      })
      .catch(error => {
        catchError(this.errorHandler)
      });
      // return this.http.post(url, saveData)
      // .pipe(
      //   catchError(this.errorHandler)
      // )
    }

    getInterpretaion(url, saveData){
      return this.http.post(url,saveData)
      .pipe()
      .toPromise()
      .then( resp => {
        // console.log('resp: ', resp);

        // Set loader false
        return resp;
      })
      .catch(error => {
        catchError(this.errorHandler)
      });
      // return this.http.post(url, saveData)
      // .pipe(
      //   catchError(this.errorHandler)
      // )
    }

    savePatient(url, saveData){
      return this.http.post<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }

    getAttPhysician(url, saveData){
      return this.http.put<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }

    findByPatientIds(url, saveData){
      return this.http.post<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }

    uploadFiles(url, saveData){
      return this.http.post<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    saveAttachments(url, saveData){
      return this.http.post<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }

    updateStatus(url, saveData){
      return this.http.post<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }

    deleteCase(url, saveData){
      return this.http.post<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }

    todayCasesLoadFirstGraph(url, saveData){
      return this.http.post<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    todayCasesLoadBetweenDates(url, saveData){
      return this.http.post<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }

    todayCasesLoadSecondGraph(url, saveData){
      return this.http.post<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    todayCasesLoadBetweenDatesSecondGraph(url, saveData){
      return this.http.post<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }

    CountCasesThirdGraph(url, saveData){
      return this.http.post<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    CountCasesBetweenDatesThirdGraph(url, saveData){
      return this.http.post<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    getPatientsByClietnIds(url, saveData){
      return this.http.put<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    getCasesByids(url, saveData){
      return this.http.post<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    getInterpretaionForSearch(url, saveData){
      return this.http.post<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    sendReport(url, saveData){
      return this.http.put<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    updateCaseStatus(url, saveData){
      return this.http.post<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    showHistory(url, saveData){
      return this.http.post<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    getUserRecord(url, saveData){
      return this.http.put<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    updateTestStatus(url, saveData){
      return this.http.post<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    showReportingCases(url, saveData){
      return this.http.post<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    jobOrderForReporting(url, saveData){
      return this.http.post<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    finalReport(url, saveData){
      return this.http.post<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }

    downloadFiles(url, saveData, type){
      var corsHeaders = new HttpHeaders({
        'Content-Type': 'application/octet-stream',
        'Access-Control-Expose-Headers':"Content-Diposition"
      });
      // return this.http.put(url,saveData,{headers: corsHeaders})
      // return this.http.put<any>(url,saveData,{headers: corsHeaders,responseType: 'blob'})
      return this.http.put(url,saveData, {headers: corsHeaders, responseType: 'blob', observe:"response"}).pipe(
        // map((result:HttpEvent<any>) => {
        map((result:HttpResponse<any>) => {
          // console.log('result',result);

          // console.log("downloadfile result Content-Diposition",result.headers.get('Content-Disposition'));
          var a = result.headers.get('Content-Disposition');
          var b = a.split(';');

          var c= b[1].split('"');



          // console.log("downloadfile result Content-Diposition",result.headers.get('content-disposition'));
          this.downLoadFile(result['body'], type,c[1])
          // saveAs(result, "Quotation.pdf");
          // return result;
        }));

      }
      /**
      * Method is use to download file.
      * @param data - Array Buffer data
      * @param type - type of the document.
      */
      downLoadFile(data: any, type: any, name: any) {
        // console.log('data',data);
        // let blob = new Blob([data], { type: type});\
        console.log("type",type);
        if (type != 'pdf' && type != 'zip') {
          // if (type == 'zip') {
          //   const blob = new Blob([data], { type: 'application/'+type });
          //   let url = window.URL.createObjectURL(blob);
          //    window.open(url);
          // }
          // else{
          const blob = new Blob(this.convertToByteArray(data), { type: 'image/'+type });
          console.log("blob",blob);
          let url = window.URL.createObjectURL(blob);
          // window.open(url);
          const downloadLink = document.createElement("a");
          downloadLink.download = name;

          downloadLink.href = url;
          downloadLink.click();
          // }
        }
        else{
          // let blob: Blob = data.blob();
          // window['saveAs'](blob, 'test.pdf');

          const blob = new Blob([data], { type: 'application/pdf;charset=utf-8' });

          let url = window.URL.createObjectURL(blob);
          // var url = window.URL.createObjectURL(data);
          console.log("url",url);
          const downloadLink = document.createElement("a");
          downloadLink.download = name;

          downloadLink.href = url;
          downloadLink.click();

          // window.open(url);
        }

      }

      convertToByteArray(input) {
        var sliceSize = 512;
        var bytes = [];

        for (var offset = 0; offset < input.length; offset += sliceSize) {
          var slice = input.slice(offset, offset + sliceSize);

          var byteNumbers = new Array(slice.length);

          for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
          }

          const byteArray = new Uint8Array(byteNumbers);

          bytes.push(byteArray);
        }

        return bytes;
      }


      redownloadReport(url, saveData, type){
        // console.log("saveData",saveData);

        var corsHeaders = new HttpHeaders({
          'Content-Type': 'application/octet-stream',
        });
        return this.http.put(url,saveData,{headers: corsHeaders})
        .pipe()
        .toPromise()
        .then( resp => {
          console.log("rep",resp);


          this.downLoadFile(resp, type, "report")

          // Set loader false
          // return resp;
        })
        .catch(error => {
          catchError(this.errorHandler)
        });
      }

      resendReport(url, saveData){
        return this.http.put<any>(url, saveData, this.corsHeaders)
        .pipe(
          catchError(this.errorHandler)
        )
      }

      errorHandler(error) {
        let errorMessage = '';
        if(error.error instanceof ErrorEvent) {
          // Get client-side error
          errorMessage = error.error.message;
        } else {
          // Get server-side error
          errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        console.log(errorMessage);
        return throwError(errorMessage);
      }
    }
