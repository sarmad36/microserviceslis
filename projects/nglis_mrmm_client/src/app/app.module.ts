import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ModuleWithProviders } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { DataTablesModule } from 'angular-datatables';
import { DashboardComponent } from './includes/dashboard/dashboard.component';
import { NavbarComponent } from './includes/navbar/navbar.component';
import { FooterComponent } from './includes/footer/footer.component';
import { SidebarComponent } from './includes/sidebar/sidebar.component';
// import { AddBatchComponent } from './batches/add-batch/add-batch.component';
// import { WorksheetComponent } from './batches/worksheet/worksheet.component';
// import { ManageBatchesComponent } from './batches/manage-batches/manage-batches.component';
import { NgxUiLoaderModule } from  'ngx-ui-loader';
import { NotifierModule, NotifierOptions } from 'angular-notifier';
import { GlobalApiCallsService } from './services/global/global-api-calls.service';
import { SortPipe } from './pipes/sort.pipe';
import { ManageReportsComponent } from './reports/manage-reports/manage-reports.component';
import { GlobalySharedModule } from './../../../../src/app/sharedmodules/globaly-shared/globaly-shared.module'

const providers = [GlobalApiCallsService, SortPipe];

/**
 * Custom angular notifier options
 */
const customNotifierOptions: NotifierOptions = {
  position: {
		horizontal: {
			position: 'left',
			distance: 110
		},
		vertical: {
			position: 'top',
			distance: 100,
			gap: 10
		}
	},
  theme: 'material',
  behaviour: {
    autoHide: 3000,
    onClick: 'hide',
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 300,
      easing: 'ease'
    },
    hide: {
      preset: 'fade',
      speed: 300,
      easing: 'ease',
      offset: 50
    },
    shift: {
      speed: 300,
      easing: 'ease'
    },
    overlap: 150
  }
};
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    DashboardComponent,
    FooterComponent,
    SidebarComponent,
    // AddBatchComponent,
    // ManageBatchesComponent,
    // WorksheetComponent,
    SortPipe,
    ManageReportsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgSelectModule,
    DataTablesModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    NgxUiLoaderModule,
    NotifierModule,
    GlobalySharedModule
  ],
  providers: providers,
  bootstrap: [AppComponent]
})
export class AppModule { }

export class NglisMrmmClientSharedModule{

  static forRoot(): ModuleWithProviders<NglisMrmmClientSharedModule> {
    return {
      ngModule: AppModule,
      providers: providers
    }
  }
}
