import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { DashboardComponent } from './includes/dashboard/dashboard.component';
import { ManageReportsComponent } from './reports/manage-reports/manage-reports.component';
import { HomeLayoutComponent } from '../../../../src/app/includes/layouts/home-layout.component';
import { DashboardComponent } from '../../../../src/app/includes/dashboard/dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: HomeLayoutComponent,
    children: [
      {
        path      : 'dashboard',
        component : DashboardComponent,
      },
      {
        path      : 'manage-reports',
        component : ManageReportsComponent,
      },      
    ]
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
