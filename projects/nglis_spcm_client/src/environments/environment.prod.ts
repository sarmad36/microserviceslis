export const environment = {
  production: true,
  API_USER_ENDPOINT      : 'http://192.168.2.8:8044/user/',
  API_PARTNER_ENDPOINT   : 'http://192.168.2.8:8044/partner/',
  API_ROLE_ENDPOINT      : 'http://192.168.2.8:8044/role/',
  API_CLIENT_ENDPOINT    : 'http://192.168.2.8:9002/clientapi/',
  API_PATIENT_ENDPOINT   : 'http://192.168.2.8:8045/patient/',
  API_SPECIMEN_ENDPOINT  : 'http://192.168.2.8:9004/specimenapi/',
  API_COVID_ENDPOINT     : 'http://localhost:8051/nglis/twmm/',
  API_LOOKUP_ENDPOINT    : 'http://192.168.2.8:8046/nglis/sadm/'
};
