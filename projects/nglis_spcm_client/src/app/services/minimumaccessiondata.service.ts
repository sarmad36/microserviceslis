import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class MinimumAccesionDataService {

  private intakeSource      = new BehaviorSubject('null');
  private ajaxReload        = new BehaviorSubject('null');
  private IntakeCases       = new BehaviorSubject('null');
  private createCase        = new BehaviorSubject('null');
  private editInake         = new BehaviorSubject('null');
  private clientId          = new BehaviorSubject('null');
  private greenColor        = new BehaviorSubject('null');
  private greenColorManageCase        = new BehaviorSubject('null');
  currentIntakeRecord       = this.intakeSource.asObservable();
  successReloadAjax         = this.ajaxReload.asObservable();
  loadIntakeCases           = this.IntakeCases.asObservable();
  updateCaseFromIntake      = this.createCase.asObservable();
  reloadEditInake           = this.editInake.asObservable();
  clientIdforEditIntake     = this.clientId.asObservable();
  getGreenColor             = this.greenColor.asObservable();
  getgreenColorManageCase   = this.greenColorManageCase.asObservable();

  constructor() { }

  changeRecordFromIntake(record: any) {
    this.intakeSource.next(record)
  }
  changeTableAfterCreate(record: any) {
    this.ajaxReload.next(record)
  }
  loadCasesInIntake(record: any) {
    this.IntakeCases.next(record)
  }
  editCasesInIntake(record: any) {
    this.createCase.next(record)
  }
  reloadEditIntakeAfteminAcceesion(record: any) {
    this.editInake.next(record)
  }
  loadClientIdForEditIntake(record: any) {
    this.clientId.next(record)
  }
  setGreenColorAfterMinAccession(record: any) {
    this.greenColor.next(record)
  }
  setGreenColorFormanageCaseOnly(record: any) {
    this.greenColorManageCase.next(record)
  }

}
