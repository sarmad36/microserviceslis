import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import { of } from 'rxjs';
import { BehaviorSubject, Observable, throwError, Subject } from 'rxjs';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
// import { catchError, retry, finalize, tap, map, takeUntil, delay } from 'rxjs/operators';
import { catchError, debounceTime, distinctUntilChanged, switchMap, tap, delay, map } from "rxjs/operators";
// import { environment } from '../../../environments/environment';
import { environment } from '../../../../../../src/environments/environment';
//import { saveAs } from 'file-saver';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class SpecimenApiCallsService {

  ////////// for seacrch client
  baseUrl: string = 'https://api.cdnjs.com/libraries';
  queryUrl: string = '?search=';
  public searchClientName = {
    header:{
      uuid               :"",
      partnerCode        :"",
      userCode           :"",
      referenceNumber    :"",
      systemCode         :"",
      moduleCode         :"CLIM",
      functionalityCode  :"SPCA-MC",
      systemHostAddress  :"",
      remoteUserAddress  :"",
      dateTime           :""
    },
    data:{
      sortColumn  :"name",
      sortingOrder:"asc",
      searchString:"",
      userCode    : ""
    }
  }
    public logedInUserRoles :any = {};

  public searchIntakeId = {
    header:{
      uuid               :"",
      partnerCode        :"",
      userCode           :"",
      referenceNumber    :"",
      systemCode         :"",
      moduleCode         :"CLIM",
      functionalityCode  :"",
      systemHostAddress  :"",
      remoteUserAddress  :"",
      dateTime           :""
    },
    data:{
      searchString:""
    }
  }
  public corsHeaders: any = new HttpHeaders({
    'Content-Type': 'application/json',
    'Accept': 'application/json',
  });

  constructor(
    private http: HttpClient,
    private sanitizer       : DomSanitizer,) { }


  searchMinAccession(url, saveData){
    return this.http.post<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  searchClientByName(url, saveData){
    return this.http.post<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  getPeople(term: string = null): Observable<any> {
    let items = getMockPeople();
    if (term) {
      items = items.filter(x => x.name.toLocaleLowerCase().indexOf(term.toLocaleLowerCase()) > -1);
    }
    // console.log("items service",items);

    return of(items).pipe(delay(500));
  }

  search(terms) {
    return terms.pipe(debounceTime(400))
    .pipe(distinctUntilChanged())
    .pipe(switchMap(term => this.searchEntries(term)));
  }

  searchEntries(term) {
    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
    var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
    var originalText = bts.toString(CryptoJS.enc.Utf8);
    this.logedInUserRoles = JSON.parse(originalText)

    // if (term.length >=3 || term.length == 0) {
    this.searchClientName.data.searchString = term;
    // if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UC") != -1) {
    //   this.searchClientName['header'].functionalityCode    = "SPCA-UC";
    // }
    // else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-VC") != -1){
    //   this.searchClientName['header'].functionalityCode    = "SPCA-VC";
    // }
    // else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-CD") != -1){
    //   this.searchClientName['header'].functionalityCode    = "SPCA-CD";
    // }
    this.searchClientName['header'].userCode = this.logedInUserRoles.userCode;
    this.searchClientName['header'].partnerCode = this.logedInUserRoles.partnerCode;
    this.searchClientName['header'].functionalityCode    = "CLTA-MC";
    

    const searchURL    = environment.API_CLIENT_ENDPOINT + 'searchclientuserbyname';
    return this.http
    // .get(this.baseUrl + this.queryUrl + term)
    .post(searchURL,this.searchClientName)
    .pipe(map(res => res)).pipe(
      catchError(this.errorHandler)
    )
    // }


  }

  searchIntake(terms) {
    return terms.pipe(debounceTime(400))
    .pipe(distinctUntilChanged())
    .pipe(switchMap(term => this.searchEntriesIntake(term)));
  }

  searchEntriesIntake(term) {
    // if (term.length >=3 || term.length == 0) {
    this.searchIntakeId.data.searchString = term;
    const searchURL    = environment.API_SPECIMEN_ENDPOINT + 'searchintakebyId';
    return this.http
    // .get(this.baseUrl + this.queryUrl + term)
    .post(searchURL,this.searchIntakeId)
    .pipe(map(res => res)).pipe(
      catchError(this.errorHandler)
    )
    // }


  }

  searchColum(url,data, terms) {
    // var terms =
    return terms.pipe(debounceTime(400))
    .pipe(distinctUntilChanged())
    .pipe(switchMap(term => this.searchEntriesColum(term, url,data)));
  }
  searchEntriesColum(term, url,data) {
    // if (term.length >=3 || term.length == 0) {
    // this.searchClientName.searchString = term;
    // const searchURL    = environment.API_CLIENT_ENDPOINT + 'searchclientbyname';
    return this.http
    // .get(this.baseUrl + this.queryUrl + term)
    .post(url,data)
    .pipe(map(res => res)).pipe(
      catchError(this.errorHandler)
    )
    // }
  }

  searchColumPatient(url,data, terms) {
    // var terms =
    return terms.pipe(debounceTime(400))
    .pipe(distinctUntilChanged())
    .pipe(switchMap(term => this.searchEntriesColumPatient(term, url,data)));
  }
  searchEntriesColumPatient(term, url,data) {
    return this.http
    // .get(this.baseUrl + this.queryUrl + term)
    .put(url,data)
    .pipe(map(res => res)).pipe(
      catchError(this.errorHandler)
    )
    // }


  }
  getPatientsByIds(url, saveData){
    return this.http.put(url,saveData)
    .pipe()
    .toPromise()
    .then( resp => {
      // console.log('resp: ', resp);

      // Set loader false
      return resp;
    })
    .catch(error => {
      catchError(this.errorHandler)
    });
    // return this.http
    // .put(url, saveData)
    // .pipe(
    //   catchError(this.errorHandler)
    // )
  }

  findPatient(url, saveData){
    return this.http.put<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  getClientByName(url, saveData){
    return this.http.post(url,saveData)
    .pipe()
    .toPromise()
    .then( resp => {
      // console.log('resp: ', resp);

      // Set loader false
      return resp;
    })
    .catch(error => {
      catchError(this.errorHandler)
    });
    // return this.http.post(url, saveData)
    // .pipe(
    //   catchError(this.errorHandler)
    // )
  }

  savePatient(url, saveData){
    return this.http.post<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  updatePatient(url, saveData){
    return this.http.put<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  getAttPhysician(url, saveData){
    return this.http.put<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  findByPatientIds(url, saveData){
    return this.http.post<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  uploadFiles(url, saveData){
    return this.http.post<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  saveAttachments(url, saveData){
    return this.http.post<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  saveAttachmentsIntake(url, saveData){
    return this.http.put<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  updateStatus(url, saveData){
    return this.http.post<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  deleteCase(url, saveData){
    return this.http.post<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  todayCasesLoadFirstGraph(url, saveData){
    return this.http.post<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  todayCasesLoadBetweenDates(url, saveData){
    return this.http.post<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  todayCasesLoadSecondGraph(url, saveData){
    return this.http.post<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  todayCasesLoadBetweenDatesSecondGraph(url, saveData){
    return this.http.post<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  CountCasesThirdGraph(url, saveData){
    return this.http.post<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  CountCasesBetweenDatesThirdGraph(url, saveData){
    return this.http.post<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  getPatientsByClietnIds(url, saveData){
    return this.http.put<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  deleteSpecimen(url, saveData){
    return this.http.post<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  loadInterpretation(url, saveData){
    return this.http.post<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  triageRequest(url, saveData){
    return this.http.post<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  loadCasesFromIntake(url, saveData){
    return this.http.post<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  addRemoveCaseInIntake(url, saveData){
    return this.http.post<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  createBatch(url, saveData){
    return this.http.post<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  printLabel(url, saveData){
    return this.http.post<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  searchReportedByIntakeIds(url, saveData){
    return this.http.put<any>(url, saveData, this.corsHeaders)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  downloadFiles(url, saveData, type){
    // console.log("saveData",saveData);

    var corsHeaders = new HttpHeaders({
      'Content-Type': 'application/octet-stream',
    });
    return this.http.put(url,saveData,{headers: corsHeaders, responseType: 'blob', observe:"response"}).pipe(
      // map((result:HttpEvent<any>) => {
      map((result:HttpResponse<any>) => {
        // console.log("downloadfile result Content-Diposition",result.headers.get('Content-Disposition'));
        var a = result.headers.get('Content-Disposition');
        var b = a.split(';');

        var c= b[1].split('"');


        // console.log("downloadfile result Content-Diposition",result.headers.get('content-disposition'));
        this.downLoadFile(result['body'], type,c[1])
        // saveAs(result, "Quotation.pdf");
        // return result;
      }));
  }
  /**
     * Method is use to download file.
     * @param data - Array Buffer data
     * @param type - type of the document.
     */
    downLoadFile(data: any, type: any, name: any) {
        // let blob = new Blob([data], { type: type});\
        console.log("type",type);
        if (type != 'pdf' && type != 'zip') {
          const blob = new Blob([data], { type: 'image/'+type });
          let url = window.URL.createObjectURL(blob);
           // window.open(url);
           // console.log("url",url);

           const downloadLink = document.createElement("a");
           downloadLink.href = url;
           downloadLink.download =name;
           downloadLink.click();
        }
        else{
          const blob = new Blob([data], { type: 'application/'+type });
          let url = window.URL.createObjectURL(blob);
           // window.open(url);
           const downloadLink = document.createElement("a");
           downloadLink.href = url;
           downloadLink.download = name;
           downloadLink.click();
        }

    }
convertToByteArray(input) {
  var sliceSize = 512;
  var bytes = [];

  for (var offset = 0; offset < input.length; offset += sliceSize) {
    var slice = input.slice(offset, offset + sliceSize);

    var byteNumbers = new Array(slice.length);

    for (var i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }

    const byteArray = new Uint8Array(byteNumbers);

    bytes.push(byteArray);
  }

  return bytes;
}

    ///////////////////////////// intake
    getIntakes(url, saveData){
      return this.http.put<any>(url, saveData)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    getIntakesForCases(url, saveData){
      return this.http.post<any>(url, saveData)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    searchIntakes(url, saveData){
      return this.http.post<any>(url, saveData)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    createIntake(url, saveData){
      return this.http.post<any>(url, saveData)
      .pipe(
        catchError(this.errorHandler)
      )
    }

    getreportingHistory(url){
      return this.http.post(url,"a")
      .pipe()
      .toPromise()
      .then( resp => {
        return resp;
      })
      .catch(error => {
        catchError(this.errorHandler)
      });
    }
    getUserRecord(url, saveData){
      return this.http.put<any>(url, saveData)
      .pipe()
    .toPromise()
    .then( resp => {
      return resp;
    })
    .catch(error => {
      catchError(this.errorHandler)
    });
    }
    finalReport(url, saveData){
      return this.http.post<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }

    getUserRecordReport(url, saveData){
      return this.http.put<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    redownloadReport(url, saveData){
      return this.http.put<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }

    resendReport(url, saveData){
      return this.http.put<any>(url, saveData, this.corsHeaders)
      .pipe(
        catchError(this.errorHandler)
      )
    }


  errorHandler(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
function getMockPeople() {
  return [
    {
      'id': '5a15b13c36e7a7f00cf0d7cb',
      'index': 2,
      'isActive': true,
      'picture': 'http://placehold.it/32x32',
      'age': 23,
      'name': 'Karyn Wright',
      'gender': 'female',
      'company': 'ZOLAR',
      'email': 'karynwright@zolar.com',
      'phone': '+1 (851) 583-2547'
    },
    {
      'id': '5a15b13c2340978ec3d2c0ea',
      'index': 3,
      'isActive': false,
      'picture': 'http://placehold.it/32x32',
      'age': 35,
      'name': 'Rochelle Estes',
      'disabled': true,
      'gender': 'female',
      'company': 'EXTRAWEAR',
      'email': 'rochelleestes@extrawear.com',
      'phone': '+1 (849) 408-2029'
    },
    {
      'id': '5a15b13c663ea0af9ad0dae8',
      'index': 4,
      'isActive': false,
      'picture': 'http://placehold.it/32x32',
      'age': 25,
      'name': 'Mendoza Ruiz',
      'gender': 'male',
      'company': 'ZYTRAX',
      'email': 'mendozaruiz@zytrax.com',
      'phone': '+1 (904) 536-2020'
    },
    {
      'id': '5a15b13cc9eeb36511d65acf',
      'index': 5,
      'isActive': false,
      'picture': 'http://placehold.it/32x32',
      'age': 39,
      'name': 'Rosales Russell',
      'gender': 'male',
      'company': 'ELEMANTRA',
      'email': 'rosalesrussell@elemantra.com',
      'phone': '+1 (868) 473-3073'
    },
    {
      'id': '5a15b13c728cd3f43cc0fe8a',
      'index': 6,
      'isActive': true,
      'picture': 'http://placehold.it/32x32',
      'age': 32,
      'name': 'Marquez Nolan',
      'gender': 'male',
      'company': 'MIRACLIS',
      'disabled': true,
      'email': 'marqueznolan@miraclis.com',
      'phone': '+1 (853) 571-3921'
    },
    {
      'id': '5a15b13ca51b0aaf8a99c05a',
      'index': 7,
      'isActive': false,
      'picture': 'http://placehold.it/32x32',
      'age': 28,
      'name': 'Franklin James',
      'gender': 'male',
      'company': 'CAXT',
      'email': 'franklinjames@caxt.com',
      'phone': '+1 (868) 539-2984'
    },
    {
      'id': '5a15b13cc3b9381ffcb1d6f7',
      'index': 8,
      'isActive': false,
      'picture': 'http://placehold.it/32x32',
      'age': 24,
      'name': 'Elsa Bradley',
      'gender': 'female',
      'company': 'MATRIXITY',
      'email': 'elsabradley@matrixity.com',
      'phone': '+1 (994) 583-3850'
    },
    {
      'id': '5a15b13ce58cb6ff62c65164',
      'index': 9,
      'isActive': true,
      'picture': 'http://placehold.it/32x32',
      'age': 40,
      'name': 'Pearson Thompson',
      'gender': 'male',
      'company': 'EZENT',
      'email': 'pearsonthompson@ezent.com',
      'phone': '+1 (917) 537-2178'
    },
    {
      'id': '5a15b13c90b95eb68010c86e',
      'index': 10,
      'isActive': true,
      'picture': 'http://placehold.it/32x32',
      'age': 32,
      'name': 'Ina Pugh',
      'gender': 'female',
      'company': 'MANTRIX',
      'email': 'inapugh@mantrix.com',
      'phone': '+1 (917) 450-2372'
    },
    {
      'id': '5a15b13c2b1746e12788711f',
      'index': 11,
      'isActive': true,
      'picture': 'http://placehold.it/32x32',
      'age': 25,
      'name': 'Nguyen Elliott',
      'gender': 'male',
      'company': 'PORTALINE',
      'email': 'nguyenelliott@portaline.com',
      'phone': '+1 (905) 491-3377'
    },
    {
      'id': '5a15b13c605403381eec5019',
      'index': 12,
      'isActive': true,
      'picture': 'http://placehold.it/32x32',
      'age': 31,
      'name': 'Mills Barnett',
      'gender': 'male',
      'company': 'FARMEX',
      'email': 'millsbarnett@farmex.com',
      'phone': '+1 (882) 462-3986'
    },
    {
      'id': '5a15b13c67e2e6d1a3cd6ca5',
      'index': 13,
      'isActive': true,
      'picture': 'http://placehold.it/32x32',
      'age': 36,
      'name': 'Margaret Reynolds',
      'gender': 'female',
      'company': 'ROOFORIA',
      'email': 'margaretreynolds@rooforia.com',
      'phone': '+1 (935) 435-2345'
    },
    {
      'id': '5a15b13c947c836d177aa85c',
      'index': 14,
      'isActive': false,
      'picture': 'http://placehold.it/32x32',
      'age': 29,
      'name': 'Yvette Navarro',
      'gender': 'female',
      'company': 'KINETICA',
      'email': 'yvettenavarro@kinetica.com',
      'phone': '+1 (807) 485-3824'
    },
    {
      'id': '5a15b13c5dbbe61245c1fb73',
      'index': 15,
      'isActive': false,
      'picture': 'http://placehold.it/32x32',
      'age': 20,
      'name': 'Elisa Guzman',
      'gender': 'female',
      'company': 'KAGE',
      'email': 'elisaguzman@kage.com',
      'phone': '+1 (868) 594-2919'
    },
    {
      'id': '5a15b13c38fd49fefea8db80',
      'index': 16,
      'isActive': false,
      'picture': 'http://placehold.it/32x32',
      'age': 33,
      'name': 'Jodie Bowman',
      'gender': 'female',
      'company': 'EMTRAC',
      'email': 'jodiebowman@emtrac.com',
      'phone': '+1 (891) 565-2560'
    },
    {
      'id': '5a15b13c9680913c470eb8fd',
      'index': 17,
      'isActive': false,
      'picture': 'http://placehold.it/32x32',
      'age': 24,
      'name': 'Diann Booker',
      'gender': 'female',
      'company': 'LYRIA',
      'email': 'diannbooker@lyria.com',
      'phone': '+1 (830) 555-3209'
    }
  ]
}
