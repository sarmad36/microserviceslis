import { TestBed } from '@angular/core/testing';

import { SpecimenApiCallsService } from './specimen-api-calls.service';

describe('SpecimenApiCallsService', () => {
  let service: SpecimenApiCallsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SpecimenApiCallsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
