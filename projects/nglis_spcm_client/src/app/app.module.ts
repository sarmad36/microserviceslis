import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { DataTablesModule } from 'angular-datatables';
import { DashboardComponent } from './includes/dashboard/dashboard.component';
import { NavbarComponent } from './includes/navbar/navbar.component';
import { FooterComponent } from './includes/footer/footer.component';
import { SidebarComponent } from './includes/sidebar/sidebar.component';
import { CreateCaseComponent } from './specimen/create-case/create-case.component';
import { NgxUiLoaderModule } from  'ngx-ui-loader';
import { NotifierModule, NotifierOptions } from 'angular-notifier';
import { UsssnDirective } from './directives/usssn.directive';
import { ManageCaseComponent } from './specimen/manage-case/manage-case.component';
import { SpecimenApiCallsService } from './services/specimen/specimen-api-calls.service';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { GlobalApiCallsService } from './services/global/global-api-calls.service';
import { SortPipe } from './pipes/sort.pipe';
import { NglisPntmClientSharedModule } from '../../../nglis_pntm_client/src/app/app.module';
import { CreateIntakeComponent } from './intake/create-intake/create-intake.component';
import { EditIntakeComponent } from './intake/edit-intake/edit-intake.component';
import { MinimumCaseComponent } from './specimen/minimum-case/minimum-case.component';
import { MinimumAccesionDataService } from "./services/minimumaccessiondata.service";
import { GlobalySharedModule } from './../../../../src/app/sharedmodules/globaly-shared/globaly-shared.module'

const providers = [SpecimenApiCallsService, GlobalApiCallsService, SortPipe, MinimumAccesionDataService];

/**
 * Custom angular notifier options
 */
const customNotifierOptions: NotifierOptions = {
  position: {
		horizontal: {
			position: 'left',
			distance: 110
		},
		vertical: {
			position: 'top',
			distance: 100,
			gap: 10
		}
	},
  theme: 'material',
  behaviour: {
    autoHide: 3000,
    onClick: 'hide',
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 300,
      easing: 'ease'
    },
    hide: {
      preset: 'fade',
      speed: 300,
      easing: 'ease',
      offset: 50
    },
    shift: {
      speed: 300,
      easing: 'ease'
    },
    overlap: 150
  }
};


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    NavbarComponent,
    FooterComponent,
    SidebarComponent,
    CreateCaseComponent,
    UsssnDirective,
    ManageCaseComponent,
    CreateIntakeComponent,
    EditIntakeComponent,
    MinimumCaseComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NglisPntmClientSharedModule.forRoot(),
    NgSelectModule,
    DataTablesModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    NgxUiLoaderModule,
    NotifierModule.withConfig(customNotifierOptions),
    NgxDaterangepickerMd.forRoot(),
    GlobalySharedModule
  ],
  providers: providers,
  bootstrap: [AppComponent]
})
export class AppModule { }

export class NglisSpcmClientSharedModule{

  static forRoot(): ModuleWithProviders<NglisSpcmClientSharedModule> {
    return {
      ngModule: AppModule,
      providers: providers
    }
  }
}
