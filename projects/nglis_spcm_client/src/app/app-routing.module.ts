import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { DashboardComponent } from './includes/dashboard/dashboard.component';
import { CreateCaseComponent } from './specimen/create-case/create-case.component';
import { ManageCaseComponent } from './specimen/manage-case/manage-case.component';
import { CreateIntakeComponent } from './intake/create-intake/create-intake.component';
import { EditIntakeComponent } from './intake/edit-intake/edit-intake.component';
import { HomeLayoutComponent } from '../../../../src/app/includes/layouts/home-layout.component';
import { DashboardComponent } from '../../../../src/app/includes/dashboard/dashboard.component';
import { AppGuard } from "../../../../src/services/gaurd/app.guard";


const routes: Routes = [
  {
    path: '',
    component: HomeLayoutComponent,
    children: [
      {
        path      : 'add-case',
        canActivate: [AppGuard],
        canDeactivate: [AppGuard],
        component : CreateCaseComponent,
      },
      {
        path      : 'edit-case/:id/:with',
        canActivate: [AppGuard],
        canDeactivate: [AppGuard],
        component : CreateCaseComponent,
      },
      {
        path      : 'manage-cases',
        canActivate: [AppGuard],
        canDeactivate: [AppGuard],
        component : ManageCaseComponent,
      },
      {
        path      : 'create-intake',
        canActivate: [AppGuard],
        canDeactivate: [AppGuard],
        component : CreateIntakeComponent,
      },
      {
        path      : 'edit-intake/:id',
        canActivate: [AppGuard],
        canDeactivate: [AppGuard],
        component : EditIntakeComponent,
      },
    ]
  }

  // {
  //   path       : '',
  //   redirectTo : 'nglis_spcm_client/dashboard',
  //   pathMatch  : 'full'
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
