import { Component,Input,Output, OnInit, OnDestroy,ElementRef, ViewChild, AfterViewInit, ViewChildren, QueryList } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { environment } from '../../../../../../src/environments/environment';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Router, NavigationEnd } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import Swal from 'sweetalert2'
declare var $ :any;
import { NotifierService } from 'angular-notifier';
import { SpecimenApiCallsService } from '../../services/specimen/specimen-api-calls.service';
import { GlobalApiCallsService } from '../../services/global/global-api-calls.service';
import { concat, Observable, of, Subject, forkJoin } from "rxjs";
import * as moment from 'moment';
import * as dymo from '../../../assets/js/dymo-label.js';
declare var dymo: any;
import { SortPipe } from "../../pipes/sort.pipe";
import { DatePipe } from '@angular/common';
import { CreateIntakeComponent } from '../../intake/create-intake/create-intake.component';
import { ManageCaseComponent } from '../manage-case/manage-case.component';
import { MinimumAccesionDataService } from "../../services/minimumaccessiondata.service";
import { filter } from 'rxjs/operators';
import { GlobalySharedModule } from './../../../../../../src/app/sharedmodules/globaly-shared/globaly-shared.module';
import { UrlGuard } from './../../../../../../src/services/gaurd/url.guard'
import * as CryptoJS from 'crypto-js';


@Component({
  selector    : 'app-minimum-case',
  templateUrl : './minimum-case.component.html',
  styleUrls   : ['./minimum-case.component.css'],
  host        : {
    class     :'mainComponentStyle'
  }
})
export class MinimumCaseComponent implements OnDestroy, OnInit {
  @Input() fromCreateIntake: CreateIntakeComponent;


  searchTerm$      = new Subject<string>();
  searchTermIntake$      = new Subject<string>();
  searchTermSearch$= new Subject<string>();
  searchTermSearch2$= new Subject<string>();
  searchTermColum$ = new Subject<string>();
  searchTermColumLnameAct$ = new Subject<string>();
  searchTermColumFnameAct$ = new Subject<string>();
  searchTermColumFnameAll$ = new Subject<string>();
  searchTermColumLnameAll$ = new Subject<string>();
  results     : Object;

  @ViewChildren(DataTableDirective)

  dtElement              : QueryList<any>;
  dtTrigger              : Subject<MinimumCaseComponent> = new Subject();
  public dtOptions       : DataTables.Settings[] = [];
  public paginatePage    : any = 0;
  public paginatePage2   : any = 0;
  public paginateLength  = 20;
  oldCharacterCount      = 0;
  oldCharacterCount2     = 0;
  submitted              = false;

  allCaseCategories         : any = [];
  selectedCaseCat           = null;
  allClientName             : any;
  allClientNameSearch       : any;
  allClientNameSearch2      : any;
  allAtdPhysicians          : any;
  allStatusType             : any;
  public caseForm           : FormGroup;
  insuranceCompany          ;
  saveTypeGlobal            = null;
  saveModalTitle            = '';
  saveModalText             = '';

  clientSearchString          ;
  clientLoading               : any;
  intakeLoading               : any;
  clientLoadingSearch         : any;
  clientLoadingSearch2        : any;
  clientInput$                = new Subject<string>();

  public saveRequest          = {
    header  : {
      dateTime: "",
      functionalityCode: "SPCA-CQ",
      moduleCode: "SPCM",
      partnerCode: "",
      referenceNumber: "",
      remoteUserAddress: "",
      systemCode: "",
      systemHostAddress: "",
      userCode: "",
      uuid: ""
    },
    data:{

      caseCategoryPrefix:"NH",
      caseNumber:"",
      intakeId: null,
      caseStatusId:1,
      caseCategoryId:null,
      clientId:null,
      attendingPhysicianId:"376",
      patientId:null,
      createdTimestamp:"2020-10-22T11:02:03.523+00:00",
      receivingDate:"2020-10-22T07:54:10.485+00:00",
      collectedBy:"1",
      collectionDate:null,
      accessionTimestamp:"2020-10-22T07:54:10.485+00:00",
      createdBy:"abc",
      caseSpecimen:[
        {
          // caseSpecimenId:1,
          specimenTypeId:2,
          specimenSourceId:1,
          bodySiteId:1,
          procedureId:2,
          createdBy:"abc",
          createdTimestamp:"2020-10-22T07:54:10.485+00:00",
          updatedBy:"",
          updatedTimestamp:""
        }
      ]
    }


  }

  allSpecimenTypes        : any = [];
  specimenType            = null;
  allSpecimenSource         : any = [];
  specimenSource            = null;

  allSBodySite              : any = [];
  bodySite                  = null;
  allProcedures             : any = [];
  procedure                 = this.allProcedures[1];
  collectDate = null;

  public allCasesReq = {
    header  : {
      uuid              :"",
      partnerCode       :"",
      userCode          :"",
      referenceNumber   :"",
      systemCode        :"",
      moduleCode        :"SPCM",
      functionalityCode: "SPCA-CQ",
      systemHostAddress :"",
      remoteUserAddress :"",
      dateTime          :""
    },
    data : {
      page         :"0",
      size         :5,
      sortColumn   :"caseNumber",
      sortingOrder :"desc"

    }
  }
  public activeCasesReq = {
    header  : {
      uuid              :"",
      partnerCode       :"",
      userCode          :"",
      referenceNumber   :"",
      systemCode        :"",
      moduleCode        :"SPCM",
      functionalityCode: "SPCA-CQ",
      systemHostAddress :"",
      remoteUserAddress :"",
      dateTime          :""
    },
    data : {
      page         :"0",
      size         :5,
      sortColumn   :"caseId",
      sortingOrder :"desc",
      statuses     :[8,9,11]
    }
  }
  activeCases      = []
  activeCasesCount = 100;
  allCases         = [];
  allCasesCount    = 100;

  public patientByIdsReq   ={
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"",
      functionalityCode: "SPCA-CQ",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      username:"danish",
      patientIds:[]
    }

  }

  patientFName = null;
  patientLName = null;
  patientDOB   = null;
  selectedPatientID = null;
  cNameforSearch;
  cNameforSearch2;
  searchtextHolder;
  searchtextHolder2;
  selectedAttPhysician = null;
  printedCaseNumber= null;
  patientDOBPrint;
  lNamePrint
  fNamePrint;

  // public searchCaseNoReq = {
  //   header  : {
  //     uuid              :"",
  //     partnerCode       :"",
  //     userCode          :"",
  //     referenceNumber   :"",
  //     systemCode        :"",
  //     moduleCode        :"moduleCode",
  //     functionalityCode: "SPCA-CQ",
  //     systemHostAddress :"",
  //     remoteUserAddress :"",
  //     dateTime          :""
  //     },
  //   data : {
  //     page         :"",
  //     size         :20,
  //     sortColumn   :"",
  //     sortingOrder :"asc",
  //     searchDate   :"",
  //     caseNumber   :""
  //
  //
  //   }
  // }
  greenColor = false;
  clientNameToPrint;

  swalStyle              : any;
  public searchByCriteria : any ={
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"SPCM",
      functionalityCode: "SPCA-CQ",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      page:"0",
      size:"20",
      sortColumn:"caseNumber",
      sortingOrder:"desc",
      collectionDate:"",
      caseNumber:"",
      clientId:"",
      collectedBy:"",
      caseStatusId:"",
      statuses:[]

    }

  }
  maxDate;
  defaultAccountTypes : any ;
  public logedInUserRoles :any = {};
  public triageRequest = {
    header  : {
      uuid              :"",
      partnerCode       :"",
      userCode          :"",
      referenceNumber   :"",
      systemCode        :"",
      moduleCode        :"TWMM",
      functionalityCode: "SPCA-CQ",
      systemHostAddress :"",
      remoteUserAddress :"",
      dateTime          :""
    },
    data : {
      caseId:"6",
      testStatusId:1,
      testId:1,
      createdBy:"abc",
      createdTimestamp: "26-11-2020 20:07:25"

    }
  }
  saveRequestCollectionDate = null;
  allIntakesForClient : any = [];
  selectedIntakeId = null;
  public allIntakesReq = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"SPCM",
      functionalityCode: "SPCA-CQ",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
        page:"0",
        size:200,
        sortColumn:"createdTimestamp",
        sortingOrder:"desc",
        clientId:[],
        searchString:""
    }

  }
  hideDraftButton =false;
  intakeFromIntakePage = null;
  previousUrl: string = null;
    currentUrl: string = null;
    patientRecordUpdateafterCancel = true;
      maxDateCollection = null;
      patientLoadedCheck = false;
  constructor(
    private formBuilder     : FormBuilder,
    private http            : HttpClient,
    private router          : Router ,
    private route           : ActivatedRoute,
    private sanitizer       : DomSanitizer,
    private ngxLoader       : NgxUiLoaderService,
    private notifier        : NotifierService,
    private specimenService : SpecimenApiCallsService,
    private globalService   : GlobalApiCallsService,
    private sortPipe        :SortPipe,
    private datePipe: DatePipe,
    private sharedIntakeData : MinimumAccesionDataService,
    private rbac         : UrlGuard,
    private encryptDecrypt : GlobalySharedModule
     ) {

    // this.rbac.checkROle('SPCA-CQ');
    this.notifier        = notifier;
    this.swalStyle       =  Swal.mixin({
      customClass : {
        confirmButton : 'btn btn-success my-swal-success',
        cancelButton  : 'btn btn-danger'
      },
      buttonsStyling  : true
    })

  }
  fromDashboard(searchType, clientId) {
    return new Promise((resolve, reject) => {
      // console.log('f1');
      // console.log("searchType999999999999999",searchType);

      if (searchType =="caseDraft") {
        $('#searchStatus').val(1);
        $("#searchStatus").trigger('change');
      }
      else if(searchType =="caseAccess"){
        $('#searchStatus').val(2);
        $("#searchStatus").trigger('change');
      }
      else if(searchType =="totalDraft"){
        $('#searchStatus').val(1);
        $("#searchStatus").trigger('change');
      }
      else if(searchType =="totalAccess"){
        $('#searchStatus').val(2);
        $("#searchStatus").trigger('change');
      }
      resolve();
    });
  }

  ngOnInit(): void {
    this.router.events.pipe(filter(event => event instanceof NavigationEnd))
    .subscribe((event: NavigationEnd) => {

      this.previousUrl = this.currentUrl;
      this.currentUrl = event.url;
      if (this.previousUrl != null) {


      if (this.previousUrl.indexOf( "/edit-intake/") !== -1 || this.previousUrl.indexOf( "/add-intake/") !== -1) {
        this.sharedIntakeData.changeRecordFromIntake('null');
        this.intakeFromIntakePage = null;
        this.hideDraftButton = false;

      }
      }
      else{
        this.sharedIntakeData.changeRecordFromIntake('null');
        this.intakeFromIntakePage = null;
        this.hideDraftButton = false;

        // console.log("nullllll");

      }
      // console.log('this.previousUrl:', this.previousUrl);

      // console.log('this.currentUrl:', this.currentUrl);

    });
    // console.log("fromCreateIntake",this.fromCreateIntake);



    var dtToday = new Date();

   var m = dtToday.getMonth()+1;
   var d = dtToday.getDate();
   var year = dtToday.getFullYear();

   var month;
    var day;
   if(m < 10)
       month = '0' + m.toString();
  else
     month = m
   if(d < 10)
       day = '0' + d.toString();
  else
   day = d

   var maxDate = year + '-' + month + '-' + day;
   this.maxDate = maxDate;
   this.maxDateCollection = maxDate+'T00:00:00';


    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
var originalText = bts.toString(CryptoJS.enc.Utf8);
this.logedInUserRoles = JSON.parse(originalText)
    console.log("------",this.logedInUserRoles);
    if (this.logedInUserRoles.userType == 1) {
      this.saveRequest.data.collectedBy = "1"
    }
    else{
      this.saveRequest.data.collectedBy = "2"
    }

    // this.logedInUserRoles.userName    = "snazirkhanali";
    // this.logedInUserRoles.partnerCode = 1;
    this.getLookups().then(lookupsLoaded => {

      this.ngxLoader.stop();
      this.sharedIntakeData.currentIntakeRecord.subscribe(intakeRecord => {
        console.log('---intakeRecord',intakeRecord);
        if (intakeRecord != 'null') {
          this.hideDraftButton = true;
          this.intakeFromIntakePage = intakeRecord['intakeId'];
          this.getSelectedRecord(intakeRecord).then(selectedCase => {

          })

        }

      })
    });
    // console.log("SARMADNAZIRABBASI",history.state.fromCreate);

    if(typeof history.state.fromCreate != "undefined"){
      this.greenColor = true;
      // this.setOnce = true;
    }
    else{
      if (localStorage.getItem('FromCreate')=='true') {
        // console.log("in set once --------------");

        this.greenColor = true;
      }
    }
    // var fromDash = history.state;
    // console.log("fromDash",fromDash);
    // if (typeof fromDash.by != "undefined") {
    //   this.fromDashboard(fromDash.by,fromDash.id).then(resDash => {
    //
    //   });
    //
    // }


    // var dateTime = new Date().toLocaleString();
    // this.collectDate = moment(dateTime).format("MM-dd-yyyY HH:MM");
    // console.log("collectDate",this.collectDate);
    // this.f1().then(res => this.f2());
    this.specimenService.search(this.searchTerm$)
    .subscribe(results => {
      // console.log("results",results);
      this.allClientName = results['data'];
      this.clientLoading = false;
    },
    error => {
      this.notifier.notify( "error", "Error while fetching clients backend connect aborted" );
      this.clientLoading = false;
      return;
      // console.log("error",error);
    });

    /////////////// load client Name for search active
    this.specimenService.search(this.searchTermSearch$)
    .subscribe(resultsSearch => {
      // console.log("results",resultsSearch);
      this.allClientNameSearch = resultsSearch['data'];
      this.clientLoadingSearch = false;
    },
    error => {
      this.notifier.notify( "error", "Error while fetching clients backend connect aborted" );
      this.clientLoadingSearch = false;
      return;
      // console.log("error",error);
    });

    /////////////// load intake id for search active
    this.specimenService.searchIntake(this.searchTermIntake$)
    .subscribe(resultsSearch => {
      // console.log("intake",resultsSearch);
      this.allIntakesForClient = resultsSearch['data'];
      this.intakeLoading = false;
    },
    error => {
      this.notifier.notify( "error", "Error while fetching intakes backend connect aborted" );
      this.intakeLoading = false;
      return;
      // console.log("error",error);
    });

    /////////////// load client Name for search
    this.specimenService.search(this.searchTermSearch2$)
    .subscribe(resultsSearch2 => {
      console.log("results",resultsSearch2);
      this.allClientNameSearch2 = resultsSearch2['data'];
      this.clientLoadingSearch2 = false;
    },
    error => {
      this.notifier.notify( "error", "Error while fetching clients backend connect aborted" );
      this.clientLoadingSearch2 = false;
      return;
      // console.log("error",error);
    });
    this.caseForm       = this.formBuilder.group({
      caseCategory      : ['', [Validators.required]],
      clientName        : ['', [Validators.required]],
      atdPhysician      : ['', [Validators.required]],
      collectionDate    : ['', [Validators.required]],
      collectedBy       : ['1', [Validators.required]],
      pLastName         : ['', [Validators.required]],
      pFirstName        : ['', [Validators.required]],
      pDOB              : ['', [Validators.required]],
      specimenType      : ['', [Validators.required]],
      intakeid          : [''],
    })



    //////////////////////////// Eligible Patients

  }
  // f1() {
  //     return new Promise((resolve, reject) => {
  //         console.log('f1');
  //         resolve();
  //     });
  // }
  //
  // f2() {
  //    console.log('f2');
  // }
  // patientSortSearch(criteriaUrl,searchCriteria,columnUrl,columnCaseReq,type,value){
  //   return new Promise((resolve, reject) =>{
  //
  //   })
  // }


  popSearch(){
    this.searchtextHolder = this.cNameforSearch;
    $("#searchClient").trigger('change');
  }
  popSearch2(){
    this.searchtextHolder2 = this.cNameforSearch2;
    $("#searchClient2").trigger('change');
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  get f() { return this.caseForm.controls; }
  saveCase(saveType){
    $('#submitModal').modal('hide');
    console.log("saveType", saveType);
    this.submitted                  = true;
    if (this.caseForm.invalid) {

      return;
    }
    if (saveType == 0) {
      this.saveRequest.data.caseStatusId = 1;
    }
    else if(saveType == 1){
      this.saveRequest.data.caseStatusId = 3;
    }
    else if(saveType == 2){
      this.saveRequest.data.caseStatusId = 3;
    }
    if (this.selectedPatientID != null ) {
      this.saveMinCase()
    }
    else{
      this.savePatient().then(savePatientResp => {
        // console.log("savePatientResp",savePatientResp);

        if (savePatientResp == true) {
          this.saveMinCase()
        }
      })
    }
    // this.savePatient().then(savePatientResp => {
    //   // console.log("savePatientResp",savePatientResp);
    //
    //   if (savePatientResp == true) {
    //     this.saveMinCase()
    //   }
    // })


  }

  saveMinCase(){
    this.ngxLoader.start();
    var myDate = new Date();
    var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyy');
    var currentTimeStamp = this.datePipe.transform(myDate, 'MM-dd-yyy HH:mm:ss');
    this.saveRequest.data.patientId = this.selectedPatientID;
    this.saveRequest.data.attendingPhysicianId = this.selectedAttPhysician.userCode;
    this.saveRequest.data.accessionTimestamp = currentTimeStamp;
    this.saveRequest.data.createdTimestamp = currentTimeStamp;
    this.saveRequest['data'].createdBy = this.logedInUserRoles.userCode;
    // if (this.saveTypeGlobal == 0) {
    //   this.saveRequest.header.functionalityCode = "SPCA-SD"
    // }
    // else if (this.saveTypeGlobal == 1) {
    //   this.saveRequest.header.functionalityCode = "SPCA-SN"
    // }
    // else if (this.saveTypeGlobal == 2) {
    //   this.saveRequest.header.functionalityCode = "SPCA-SP"
    // }
    // this.saveRequest['header'].userCode = this.logedInUserRoles.userCode;

    this.saveRequest.data.collectionDate = this.datePipe.transform(this.saveRequestCollectionDate, 'MM-dd-yyy HH:mm:ss');
    this.saveRequest.data.receivingDate = this.saveRequest.data.collectionDate;
    // this.saveRequest.data.caseSpecimen[0].specimenTypeId = this.specimenType.specimenTypeId;
    this.saveRequest.data.caseSpecimen[0].createdTimestamp = currentTimeStamp;
    this.saveRequest.data.caseSpecimen[0].createdBy = this.logedInUserRoles.userCode;
    this.saveRequest['header'].partnerCode = this.logedInUserRoles.partnerCode;
    this.saveRequest['header'].userCode = this.logedInUserRoles.userCode;
    this.saveRequest['header'].functionalityCode = "SPCA-CQ";
    if (this.saveTypeGlobal == 0) {
      this.saveRequest.data.intakeId = null;
    }
    console.log("this.saveRequest",this.saveRequest);
    // this.ngxLoader.stop(); return;
    var saveminUrl = environment.API_SPECIMEN_ENDPOINT + "minaccession";
    this.specimenService.searchMinAccession(saveminUrl,this.saveRequest).subscribe(saveResp =>{
      this.greenColor = false;
      console.log("saveResp",saveResp);
      if (this.saveTypeGlobal == 0) {
        this.notifier.notify( "success", "Case saved successfully But it is not assigned to intake as Case is save as Draft.");
      }
      else{
        this.notifier.notify( "success", "Case saved successfully");
      }
      // if (this.intakeFromIntakePage != null) {
      //   $('.modal').modal('hide');
      // }

      this.printedCaseNumber = saveResp['data'].caseNumber;
      this.submitted = false;
      // this.caseForm.reset();
      this.triageCase(saveResp['data'].caseId);
      this.resetPatient();
      console.log("22222222222222",this.intakeFromIntakePage);


      if(this.intakeFromIntakePage != null){
        this.sharedIntakeData.setGreenColorAfterMinAccession('null');
        this.sharedIntakeData.setGreenColorFormanageCaseOnly("true");
        this.sharedIntakeData.reloadEditIntakeAfteminAcceesion(this.intakeFromIntakePage);
      }
      else{
        this.sharedIntakeData.setGreenColorFormanageCaseOnly("true");
      }

      // this.triageCase(saveResp['data'].caseid);


      this.greenColor = true;


      // saveResp['data'].greenColor = true;
      // this.activeCases.unshift(saveResp['data']);
      this.ngxLoader.stop();
      this.sharedIntakeData.changeTableAfterCreate('reload');
      // this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
      //   dtElement.dtInstance.then((dtInstance: any) => {
      //     dtInstance.ajax.reload();
      //   })
      // })

      // this.dtTrigger.next();
      // console.log("--------->",this.saveTypeGlobal);

      if (this.saveTypeGlobal == 2) {
        $('.modal').modal('hide');

        // this.printLabel(saveResp['data'].caseNumber,this.patientLName,this.patientFName,this.patientDOB)
        this.printLabel(saveResp['data'].caseNumber,this.lNamePrint,this.fNamePrint,this.patientDOBPrint,this.clientNameToPrint,saveResp['data'].specimenTypeId,saveResp['data'].clientId)
        this.resetPatient();
        this.resetFields();
      }
      if (this.saveTypeGlobal == 0) {
        $('.modal').modal('hide');
        this.resetPatient();
        this.resetFields();
      }
      // this.getLookups().then(lookupsLoaded => {
      //
      //   this.ngxLoader.stop();
      // });
    },
    error=>{
      this.ngxLoader.stop();
      this.notifier.notify( "error", "Error while saving case try changing patient information" );
    })



  }

  collectDateChange(e){
    console.log("ee",e.target.value);
    if (e.target.value > (this.maxDate+'T24:00')) {
      // alert("date greater then current"+e.target.value)
      // this.saveRequest.data.collectionDate = null;
      this.saveRequestCollectionDate = null;

    }
    // console.log("collectDate",this.collectDate);

  }

  clientChanged(){
    this.clientLoading = false;
    console.log("this.clientSearchString",this.clientSearchString);
    if (this.clientSearchString != null) {
      this.saveRequest.data.intakeId = null;
      this.getIntakeForClient(this.saveRequest.data.clientId);
      this.saveRequest.data.clientId = this.clientSearchString.clientId;
      this.clientNameToPrint = this.clientSearchString.name;

      console.log("client Changed",this.clientSearchString);
      if (typeof this.clientSearchString.userCode != 'undefined') {
      // if (this.clientSearchString.clientUsers.length >0) {

        const uniqueUIds = [];
        const map = new Map();
        if (this.clientSearchString.active == 1 && this.clientSearchString.isVisibleInCases == 1) {
          // var code = this.encryptDecrypt.encryptString(item.userCode)
           uniqueUIds.push(this.clientSearchString.userCode)
        }
        // for (const item of this.clientSearchString.clientUsers) {
        //   if (item.active == 1 && item.visibleInCases == 1) {
        //     // var code = this.encryptDecrypt.encryptString(item.userCode)
        //     if(!map.has(item.userCode)){
        //       console.log('item.userCode',item.userCode);
        //
        //       map.set(item.userCode, true);    // set any value to Map
        //       uniqueUIds.push(item.userCode);
        //     }
        //   }
        // }
        if (uniqueUIds.length == 0) {
          alert("There is no active physician this client or the physician cannot access the cases please select a diffirent client")
          this.saveRequest.data.clientId = null;
          this.clientSearchString = null;
          return;
        }

        console.log("physicianIDs",uniqueUIds);
        var attphysUrl = environment.API_USER_ENDPOINT + 'attendingphysicians'
        var reqData={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"",functionalityCode: "SPCA-CQ",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{userCodes:uniqueUIds}}
        reqData.header.userCode    = this.logedInUserRoles.userCode
        reqData.header.partnerCode = this.logedInUserRoles.partnerCode
        reqData.header.functionalityCode = "SPCA-CQ"
        // console.log('reqData',reqData);
        this.specimenService.getAttPhysician(attphysUrl, reqData).subscribe(attResp => {
          console.log("att Physicain Resp",attResp);
          if (attResp['data'].length > 0) {

            attResp['data'].map((i) => { i.name = i.firstName + ' ' + i.lastName; return i; });

            // console.log('this.allAtdPhysicians',this.allAtdPhysicians);
            this.allAtdPhysicians = attResp['data'];
            this.selectedAttPhysician = this.allAtdPhysicians[0];

            // for (let i = 0; i < this.clientSearchString.clientUsers.length; i++) {
            //   for (let j = 0; j <   this.allAtdPhysicians.length; j++) {
            //     if (this.clientSearchString.clientUsers[i].defaultPhysician == 1) {
            //       if (  this.allAtdPhysicians[j].userCode == this.clientSearchString.clientUsers[i].userCode) {
            //         this.selectedAttPhysician = this.allAtdPhysicians[j];
            //         // console.log('here');
            //         break;
            //       }
            //     }
            //
            //   }
            // }
          }
          else{
            alert("no Physician found for this client please select a diffirent client")
            this.saveRequest.data.clientId = null;
            this.clientSearchString = null;
          }
          this.checkPatient();

        },
        error =>{
          this.ngxLoader.stop();
          this.notifier.notify( "error", "Error while loading Attending Physicians for the client")
        })


      }
      else{
        alert('client doesnot have a user please select a different client 222')
        this.clientSearchString = null;
        this.saveRequest.data.clientId = null;
      }

      ////////////////// select case category based on acc type\
      // console.log('1111',this.clientSearchString.clientTypes[0].clientTypeId);
      // console.log('2222',this.defaultAccountTypes);
      for (let m = 0; m < this.defaultAccountTypes.length; m++) {
        if (typeof this.clientSearchString.clientTypeDto[0] != 'undefined') {
          if (this.clientSearchString.clientTypeDto[0].clientTypeId != null) {
            if (this.defaultAccountTypes[m].clientTypeId == this.clientSearchString.clientTypeDto[0].clientTypeId) {
              // console.log('-----',this.defaultAccountTypes[m]);

              if (this.defaultAccountTypes[m].name.toLowerCase().indexOf('nursing')>-1 ) {
                for (let i = 0; i < this.allCaseCategories.length; i++) {
                  if (this.allCaseCategories[i].caseCategoryName.toLowerCase().indexOf('nursing')>-1) {
                    this.selectedCaseCat = this.allCaseCategories[i]
                    this.saveRequest.data.caseCategoryId     = this.selectedCaseCat.caseCategoryId;
                    this.saveRequest.data.caseCategoryPrefix = this.selectedCaseCat.caseCategoryPrefix;
                  }
                }
              }
              else if (this.defaultAccountTypes[m].name.toLowerCase().indexOf('surgical')>-1 ) {
                for (let i = 0; i < this.allCaseCategories.length; i++) {
                  if (this.allCaseCategories[i].caseCategoryName.toLowerCase().indexOf('surgical')>-1) {
                    this.selectedCaseCat = this.allCaseCategories[i]
                    this.saveRequest.data.caseCategoryId     = this.selectedCaseCat.caseCategoryId;
                    this.saveRequest.data.caseCategoryPrefix = this.selectedCaseCat.caseCategoryPrefix;
                  }
                }
              }
              else if (this.defaultAccountTypes[m].name.toLowerCase().indexOf('covid-19')>-1 ){
                ///covid
                for (let i = 0; i < this.allCaseCategories.length; i++) {
                  if (this.allCaseCategories[i].caseCategoryName.toLowerCase().indexOf('covid-19')>-1) {
                    this.selectedCaseCat = this.allCaseCategories[i]
                    this.saveRequest.data.caseCategoryId     = this.selectedCaseCat.caseCategoryId;
                    this.saveRequest.data.caseCategoryPrefix = this.selectedCaseCat.caseCategoryPrefix;
                  }
                }
              }
              else if (this.defaultAccountTypes[m].name.toLowerCase().indexOf('Covid General Case')>-1 ){
                ///covid
                for (let i = 0; i < this.allCaseCategories.length; i++) {
                  if (this.allCaseCategories[i].caseCategoryName.toLowerCase().indexOf('Covid General Case')>-1) {
                    this.selectedCaseCat = this.allCaseCategories[i]
                    this.saveRequest.data.caseCategoryId     = this.selectedCaseCat.caseCategoryId;
                    this.saveRequest.data.caseCategoryPrefix = this.selectedCaseCat.caseCategoryPrefix;
                  }
                }
              }
              else if (this.defaultAccountTypes[m].name.toLowerCase().indexOf('Covid Employee Case')>-1 ){
                ///covid
                for (let i = 0; i < this.allCaseCategories.length; i++) {
                  if (this.allCaseCategories[i].caseCategoryName.toLowerCase().indexOf('Covid Employee Case')>-1) {
                    this.selectedCaseCat = this.allCaseCategories[i]
                    this.saveRequest.data.caseCategoryId     = this.selectedCaseCat.caseCategoryId;
                    this.saveRequest.data.caseCategoryPrefix = this.selectedCaseCat.caseCategoryPrefix;
                  }
                }
              }
              else{
                ///covid
                for (let i = 0; i < this.allCaseCategories.length; i++) {
                  if (this.allCaseCategories[i].caseCategoryName.toLowerCase().indexOf('covid-19')>-1) {
                    this.selectedCaseCat = this.allCaseCategories[i]
                    this.saveRequest.data.caseCategoryId     = this.selectedCaseCat.caseCategoryId;
                    this.saveRequest.data.caseCategoryPrefix = this.selectedCaseCat.caseCategoryPrefix;
                  }
                }
              }
            }
          }
        }


      }
      if (this.saveRequest.data.clientId != null) {
        // this.clientSearchString = null;
        this.saveRequest.data.intakeId = null;
        this.getIntakeForClient(this.saveRequest.data.clientId);

      }

    }
    else{
      this.saveRequest.data.clientId = null;
    }
  }

  // selectedPhysician(){
  //   console.log("attphysincan changed",this.selectedAttPhysician);
  //
  //   this.saveRequest.data.attendingPhysicianId = this.selectedAttPhysician.userCode;
  // }

  showSaveModal(saveType){
    this.saveTypeGlobal = saveType;
    if (saveType == 0) {
      this.saveModalTitle = "Save as Draft";
      this.saveModalText  = "The case has been saved as Draft";
      this.saveCase(this.saveTypeGlobal);
      // Swal.fire({
      //   title: this.saveModalTitle,
      //   text: this.saveModalText,
      //   icon: 'warning',
      //   showCancelButton: true,
      //   confirmButtonColor: '#3085d6',
      //   cancelButtonColor: '#d33',
      //   confirmButtonText: 'Yes, do it!',
      //   allowOutsideClick:false
      // }).then((result) => {
      //   if (result.value) {
      //     this.saveCase(this.saveTypeGlobal);
      //   } else if (result.dismiss === Swal.DismissReason.cancel) {
      //   }
      //
      // })

      // $('#submitModal').modal('show');
    }
    else if(saveType == 1){
      this.saveModalTitle = "Submit";
      this.saveModalText  = "Are you sure to submit, the case cannot be deleted after submission";
      this.saveCase(this.saveTypeGlobal);

      // this.saveModalText  = "Are you sure to submit, the case cannot be deleted after submission";
      // $('#submitModal').modal('show');
      // Swal.fire({
      //   title: this.saveModalTitle,
      //   text: this.saveModalText,
      //   icon: 'warning',
      //   showCancelButton: true,
      //   confirmButtonColor: '#3085d6',
      //   cancelButtonColor: '#d33',
      //   confirmButtonText: 'Yes, do it!',
      //   allowOutsideClick:false
      // }).then((result) => {
      //   if (result.value) {
      //     this.saveCase(this.saveTypeGlobal);
      //   } else if (result.dismiss === Swal.DismissReason.cancel) {
      //   }

      // })
    }
    else if(saveType == 2){
      this.saveModalTitle = "Submit";
      this.saveModalText  = "Are you sure to submit, the case cannot be deleted after submission";
      this.saveCase(this.saveTypeGlobal);

      // $('#submitModal').modal('show');
      // Swal.fire({
      //   title: this.saveModalTitle,
      //   text: this.saveModalText,
      //   icon: 'warning',
      //   showCancelButton: true,
      //   confirmButtonColor: '#3085d6',
      //   cancelButtonColor: '#d33',
      //   confirmButtonText: 'Yes, do it!',
      //   allowOutsideClick:false
      // }).then((result) => {
      //   if (result.value) {
      //     this.saveCase(this.saveTypeGlobal);
      //   } else if (result.dismiss === Swal.DismissReason.cancel) {
      //   }

      // })
    }

  }

  checkPatient(){
    var ageDB, monthDB, dayDB, ageTocheck;
    this.patientRecordUpdateafterCancel = true;
    if (this.patientDOB != null) {
      if (this.patientDOB > (this.maxDate)) {
        this.patientDOB = null;
        if (this.patientFName != null) {
          this.patientFName = this.patientFName.replace(/[^a-zA-Z0-9\s]/g, '');
          this.patientFName = this.patientFName.trim();

        }
        if (this.patientLName != null) {
          this.patientLName = this.patientLName.replace(/[^a-zA-Z0-9\s]/g, '');
          this.patientLName = this.patientLName.trim();
        }
        return;
      }
      var birthDate = new Date(this.patientDOB);

      if ((birthDate.getMonth()+1) < 10) {
        monthDB = "0"+(birthDate.getMonth()+1);
      }
      else{
        monthDB = (birthDate.getMonth()+1);
      }
      // monthDB = (birthDate.getMonth()+1);
      if (birthDate.getDate() < 10) {
        dayDB = "0"+birthDate.getDate();
      }
      else{
        dayDB = birthDate.getDate();
      }
      ageDB = monthDB + "-" + dayDB + "-" + birthDate.getFullYear();
      console.log("ageDB",ageDB);

      ageTocheck = ageDB;
      if (this.patientFName != null) {
        this.patientFName = this.patientFName.replace(/[^a-zA-Z0-9\s]/g, '');
        this.patientFName = this.patientFName.trim();

      }
      if (this.patientLName != null) {
        this.patientLName = this.patientLName.replace(/[^a-zA-Z0-9\s]/g, '');
        this.patientLName = this.patientLName.trim();
      }
    }
    if (this.patientFName != null) {
      this.patientFName = this.patientFName.replace(/[^a-zA-Z0-9\s]/g, '');
      this.patientFName = this.patientFName.trim();

    }
    if (this.patientLName != null) {
      this.patientLName = this.patientLName.replace(/[^a-zA-Z0-9\s]/g, '');
      this.patientLName = this.patientLName.trim();
    }
    // return;

    // this.saveRequest.data.clientId = 2
    if (this.patientFName != null && this.patientLName != null && this.patientDOB != null && this.saveRequest.data.clientId != null) {
      $('.existCheck-loadergif').css('display',"inline-block");
      var reqPatBP  = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"",functionalityCode:"PNTA-MP",systemHostAddress:"",remoteUserAddress:"",dateTime:""},
      data:{username:"danish",firstName:this.patientFName,lastName:this.patientLName,dateOfBirth:ageDB,clientId:this.saveRequest.data.clientId}}
      // data:{username:"danish",firstName:this.encryptDecrypt.encryptString(this.patientFName),lastName:this.encryptDecrypt.encryptString(this.patientLName),dateOfBirth:ageDB,clientId:this.saveRequest.data.clientId}}
      var findPTURL = environment.API_PATIENT_ENDPOINT + 'findpatient';
      this.specimenService.findPatient(findPTURL, reqPatBP).subscribe(reps => {
        if (reps['data'] == null) {
          ////// patient not found add patient
          this.patientDOBPrint = this.patientDOB;
          this.lNamePrint = this.patientLName;
          this.fNamePrint = this.patientFName;
          // var savePURL = environment.API_PATIENT_ENDPOINT + 'save';
          // var saveReq  = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"",functionalityCode: "SPCA-CQ",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{accountNumber:"ABC-21",clientId:this.saveRequest.data.clientId,createdSource:4,patientDemographics:{firstName:this.patientFName.trim(),lastName:this.patientLName.trim(),dateOfBirth:ageDB,createdBy:this.logedInUserRoles.userCode,createdTimestamp:currentTimeStamp,},
          // createdBy:this.logedInUserRoles.userCode,createdTimestamp:currentTimeStamp,userName:this.logedInUserRoles.userCode}}
          // this.specimenService.savePatient(savePURL, saveReq).subscribe(savePmin =>{
          //   // console.log("Save with 3",saveReq);
          //   // return;
          //   $('.existCheck-loadergif').css('display',"none");
          //
          //   if (savePmin['result'].codeType == "S") {
          //     this.patientFName = savePmin['data'].patientDemographics.firstName;
          //     this.patientLName = savePmin['data'].patientDemographics.lastName;
          //     // this.patientDOB   = savePmin['data'].patientDemographics.dateOfBirth;
          //     this.selectedPatientID = savePmin['data'].patientId;
          //     this.notifier.notify( "success", "Patient Created Successfully")
          //
          //   }
          //   else{
          //     console.log("error");
              $('.existCheck-loadergif').css('display',"none");
          //     this.notifier.notify( "error", "Error while saving patient")
          //
          //   }
          // }, error =>{
          //   this.ngxLoader.stop();
          //   $('.existCheck-loadergif').css('display',"none");
          //   this.notifier.notify( "error", "Error while saving Patient Connection Check")
          // })
        }
        else if(reps['data'] != null) {
          Swal.fire({
            title: 'Hi!',
            text: "The Patient Already Exist Do you want to load it?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, load it!',
            allowOutsideClick:false
          }).then((result) => {
            if (result.value) {

              if (reps['data'].status != 1) {
                this.notifier.notify('warning','The selected patien is inactive you will not be able to create case using this patient.')
                $('.existCheck-loadergif').css('display',"none");
                this.selectedPatientID = null;
                // this.patientRecordUpdateafterCancel = false;
                // this.patientLoadedCheck = false;
              }
              else{

              //////// patient Found Save ID
              this.selectedPatientID = reps['data'].patientId;
              this.patientDOBPrint   = reps['data'].dateOfBirth;
              this.lNamePrint        = reps['data'].lastName;
              this.fNamePrint        = reps['data'].firstName;
              $('.existCheck-loadergif').css('display',"none");
              this.patientLoadedCheck = true;
              this.patientRecordUpdateafterCancel = true
              // For more information about handling dismissals please visit
              // https://sweetalert2.github.io/#handling-dismissals
            }
            } else if (result.dismiss === Swal.DismissReason.cancel) {
              this.selectedPatientID = null;
              this.patientRecordUpdateafterCancel = false;
              this.patientLoadedCheck = false;
              // this.saveAgain = false;
              this.ngxLoader.stopBackground();
              $('.existCheck-loadergif').css('display',"none");
              // this.resetPatient();
            }
            // //////// patient Found Save ID
            // this.selectedPatientID = reps['data'].patientId;
            // this.patientDOBPrint   = reps['data'].dateOfBirth;
            // this.lNamePrint        = reps['data'].lastName;
            // this.fNamePrint        = reps['data'].firstName;
            // $('.existCheck-loadergif').css('display',"none");
          })
          //////// patient Found Save ID
          // this.selectedPatientID = reps['data'].patientId;
          // this.patientDOBPrint   = reps['data'].dateOfBirth;
          // this.lNamePrint        = reps['data'].lastName;
          // this.fNamePrint        = reps['data'].firstName;
          // $('.existCheck-loadergif').css('display',"none");
        }
      },error =>{
        this.ngxLoader.stop();
        $('.existCheck-loadergif').css('display',"none");
        this.resetPatient();
        this.notifier.notify( "error", "Error while loading Patient Connection Check")

      })
    }

  }

  savePatient(){
    if (this.patientRecordUpdateafterCancel == false) {
      // this.patType = []
      // this.patGender = null
      alert('please change pateint first name, last name or date of birth another patient exist with same record.')
      this.ngxLoader.stopBackground();
      // resolve(false);
      return;
    }
    var ageDB, monthDB, dayDB, ageTocheck;
    var birthDate = new Date(this.patientDOB);

      if ((birthDate.getMonth()+1) < 10) {
        monthDB = "0"+(birthDate.getMonth()+1);
      }
      else{
        monthDB = (birthDate.getMonth()+1);
      }
      // monthDB = (birthDate.getMonth()+1);
      if (birthDate.getDate() < 10) {
        dayDB = "0"+birthDate.getDate();
      }
      else{
        dayDB = birthDate.getDate();
      }
      ageDB = monthDB + "-" + dayDB + "-" + birthDate.getFullYear();
      console.log("ageDB",ageDB);

      ageTocheck = ageDB;
    var myDate = new Date();
    var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyy');
    var currentTimeStamp = this.datePipe.transform(myDate, 'MM-dd-yyy HH:mm:ss');
    return new Promise((resolve, reject) => {
      var savePURL = environment.API_PATIENT_ENDPOINT + 'save';
      var saveReq  = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"PNTM",functionalityCode:"SPCA-CQ",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{accountNumber:"",clientId:this.saveRequest.data.clientId,createdSource:4,patientDemographics:{firstName:this.encryptDecrypt.encryptString(this.patientFName.trim()),lastName:this.encryptDecrypt.encryptString(this.patientLName.trim()),dateOfBirth:ageDB,createdBy:this.logedInUserRoles.userCode,createdTimestamp:currentTimeStamp,},
      createdBy:this.logedInUserRoles.userCode,createdTimestamp:currentTimeStamp,userName:this.logedInUserRoles.userName}}
      this.specimenService.savePatient(savePURL, saveReq).subscribe(savePmin =>{
        // console.log("Save with 3",saveReq);
        // return;
        $('.existCheck-loadergif').css('display',"none");

        if (savePmin['result'].codeType == "S") {
          this.patientFName = savePmin['data'].patientDemographics.firstName;
          this.patientLName = savePmin['data'].patientDemographics.lastName;
          // this.patientDOB   = savePmin['data'].patientDemographics.dateOfBirth;
          this.selectedPatientID = savePmin['data'].patientId;
          this.notifier.notify( "success", "Patient Created Successfully")
          resolve(true);

        }
        else{
          console.log("error");
          $('.existCheck-loadergif').css('display',"none");
          this.notifier.notify( "error", "Error while saving patient");
          resolve(false);

        }
      }, error =>{
        this.ngxLoader.stop();
        $('.existCheck-loadergif').css('display',"none");
        this.notifier.notify( "error", "Error while saving Patient Connection Check")
        resolve(false);
      })

    })

  }

  printLabel(caseNumber,lName,fName,dob,clientName,casePrefix,clientId){
    if(typeof dymo == "undefined"){
      this.ngxLoader.stop();
      return;
    }

    this.ngxLoader.start();
    if (typeof lName == 'undefined') {lName = "No Name"}
    if (typeof fName == 'undefined') {fName = "No Name"}
    if (typeof dob == 'undefined') {dob = "No dob"}
    dob = this.datePipe.transform(dob, 'MM/dd/yyy');
    // this.ngxLoader.stop();
    console.log("caseNumber",caseNumber);
    console.log("lName",lName);
    console.log("fName",fName);
    console.log("dob",dob);
    console.log("clientName",clientName);
    console.log("casePrefix",casePrefix);
    // return;
    var cPrifix = '';
    if (typeof casePrefix == "undefined" || casePrefix == null || casePrefix.length == 0) {
      cPrifix = "NP"
    }
    else{
      var printTypeHolder = [];
      for (let j = 0; j < this.allSpecimenTypes.length; j++) {
        for (let k = 0; k < casePrefix.length; k++) {
          if (casePrefix[k]== this.allSpecimenTypes[j].specimenTypeId) {
            cPrifix = cPrifix+' '+this.allSpecimenTypes[j]['specimenTypePrefix'];
            printTypeHolder.push(this.allSpecimenTypes[j]['specimenTypePrefix'])
          }

        }


      }
      // for (let i = 0; i < this.allSpecimenTypes.length; i++) {
      //   for (let j = 0; j < casePrefix.length; j++) {
      //     if (casePrefix[j] == this.allSpecimenTypes[i]['specimenTypeId']) {
      //       cPrifix = cPrifix +' '+ this.allSpecimenTypes[i]['specimenTypePrefix']
      //     }
      //
      //   }
      //
      // }
    }
    console.log("cPrifix",cPrifix);
    console.log("printTypeHolder[0]",printTypeHolder[0]);

    // console.log("cPrifix",cPrifix);

    var labelXml = '<DieCutLabel Version="8.0" Units="twips" MediaType="Default"> <PaperOrientation>Landscape</PaperOrientation> <Id>Small30332</Id> <IsOutlined>false</IsOutlined> <PaperName>30332 1 in x 1 in</PaperName> <DrawCommands> <RoundRectangle X="0" Y="0" Width="1440" Height="1440" Rx="180" Ry="180" /> </DrawCommands> <ObjectInfo> <TextObject> <Name>FirstLast</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <HorizontalAlignment>Left</HorizontalAlignment> <VerticalAlignment>Top</VerticalAlignment> <TextFitMode>ShrinkToFit</TextFitMode> <UseFullFontHeight>True</UseFullFontHeight> <Verticalized>False</Verticalized> <StyledText> <Element> <String xml:space="preserve">First Last</String> <Attributes> <Font Family="Arial" Size="5" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" HueScale="100" /> </Attributes> </Element> </StyledText> </TextObject> <Bounds X="144" Y="199.393133236425" Width="1186.49076398256" Height="120" /> </ObjectInfo> <ObjectInfo> <BarcodeObject> <Name>BARCODE</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <Text>12345</Text> <Type>QRCode</Type> <Size>Medium</Size> <TextPosition>None</TextPosition> <TextFont Family="Arial" Size="8" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <CheckSumFont Family="Arial" Size="8" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <TextEmbedding>None</TextEmbedding> <ECLevel>0</ECLevel> <HorizontalAlignment>Center</HorizontalAlignment> <QuietZonesPadding Left="0" Top="0" Right="0" Bottom="0" /> </BarcodeObject> <Bounds X="381.704497509707" Y="771.219004098847" Width="541.292842170495" Height="526.701837189909" /> </ObjectInfo> <ObjectInfo> <TextObject> <Name>Facility</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <HorizontalAlignment>Left</HorizontalAlignment> <VerticalAlignment>Top</VerticalAlignment> <TextFitMode>ShrinkToFit</TextFitMode> <UseFullFontHeight>True</UseFullFontHeight> <Verticalized>False</Verticalized> <StyledText> <Element> <String xml:space="preserve">Facility</String> <Attributes> <Font Family="Arial" Size="4" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" HueScale="100" /> </Attributes> </Element> </StyledText> </TextObject> <Bounds X="164.897098682172" Y="651.471024372647" Width="1183.87862664729" Height="120" /> </ObjectInfo> <ObjectInfo> <TextObject> <Name>DOB</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <HorizontalAlignment>Left</HorizontalAlignment> <VerticalAlignment>Top</VerticalAlignment> <TextFitMode>ShrinkToFit</TextFitMode> <UseFullFontHeight>True</UseFullFontHeight> <Verticalized>False</Verticalized> <StyledText> <Element> <String xml:space="preserve">DOB</String> <Attributes> <Font Family="Arial" Size="5" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" HueScale="100" /> </Attributes> </Element> </StyledText> </TextObject> <Bounds X="144" Y="310.963055930226" Width="1210" Height="120" /> </ObjectInfo> <ObjectInfo> <TextObject> <Name>Client</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <HorizontalAlignment>Left</HorizontalAlignment> <VerticalAlignment>Top</VerticalAlignment> <TextFitMode>ShrinkToFit</TextFitMode> <UseFullFontHeight>True</UseFullFontHeight> <Verticalized>False</Verticalized> <StyledText> <Element> <String xml:space="preserve">Client</String> <Attributes> <Font Family="Arial" Size="5" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" HueScale="100" /> </Attributes> </Element> </StyledText> </TextObject> <Bounds X="144" Y="418.060686676358" Width="1210" Height="120" /> </ObjectInfo> <ObjectInfo> <TextObject> <Name>CaseCode</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <HorizontalAlignment>Left</HorizontalAlignment> <VerticalAlignment>Top</VerticalAlignment> <TextFitMode>ShrinkToFit</TextFitMode> <UseFullFontHeight>True</UseFullFontHeight> <Verticalized>False</Verticalized> <StyledText> <Element> <String xml:space="preserve">Case Code</String> <Attributes> <Font Family="Arial" Size="5" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" HueScale="100" /> </Attributes> </Element> </StyledText> </TextObject> <Bounds X="144" Y="525.15831742249" Width="1210" Height="120" /> </ObjectInfo> </DieCutLabel>';

    var label = dymo.label.framework.openLabelXml(labelXml);

    // var barcodeData = 'Si Paradigm'
    label.setObjectText('BARCODE', caseNumber);
    // set label text
    label.setObjectText("FirstLast",lName +','+ fName);
    label.setObjectText("DOB",  dob);
    // label.setObjectText("Client", clientName.substring(0, 10));
    label.setObjectText("Facility", printTypeHolder[0]);
    label.setObjectText("CaseCode", caseNumber);
    label.setObjectText("Client", clientName.substring(0, 10));


    var printers = dymo.label.framework.getPrinters();
    if (printers.length == 0){

      this.notifier.notify("error","No DYMO printers are installed. Install DYMO printers.")
      this.ngxLoader.stop()
      if (this.saveTypeGlobal == 2) {
        $('.modal').modal('hide');

        // this.printLabel(saveResp['data'].caseNumber,this.patientLName,this.patientFName,this.patientDOB)
        this.resetPatient();
        this.resetFields();
      }
      throw "No DYMO printers are installed. Install DYMO printers.";

    }
    else{
      var myDate = new Date();
      var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyy HH:mm:ss');
      var saveLabeRequest = {
        header: {
          uuid: "",
          partnerCode: "sip",
          userCode: "usyduysad",
          referenceNumber: "",
          systemCode: "",
          moduleCode: "SPCM",
          functionalityCode: "SPCA-CQ",
          systemHostAddress: "",
          remoteUserAddress: "",
          dateTime: ""
        },
        data:
        {
          caseNumber: "",
          clientId: 1522,
          createdTimestamp: "",
          printedBy: ""
        }
      }

      saveLabeRequest.data.caseNumber = caseNumber;
      saveLabeRequest.data.clientId = clientId;
      saveLabeRequest.data.createdTimestamp = currentDate;
      saveLabeRequest.data.printedBy = this.logedInUserRoles.userCode;
      saveLabeRequest.header.userCode = this.logedInUserRoles.userCode;

      var saveLabelUrl = environment.API_SPECIMEN_ENDPOINT+'savelabel';
      this.specimenService.printLabel(saveLabelUrl,saveLabeRequest).subscribe(saveLabelResponse =>{
        console.log('saveLabelResponse',saveLabelResponse);

      },error=>{
        this.notifier.notify("error","Error while saveing print history.")
      })

    }


    var printerName = "";
    for (var i = 0; i < printers.length; ++i)
    {
      var printer = printers[i];
      if (printer.printerType == "LabelWriterPrinter")
      {
        printerName = printer.name;
        break;
      }
    }

    label.print(printerName);
    this.printedCaseNumber = caseNumber;
    console.log("this.printedCaseNumber--",this.printedCaseNumber);
    // $('#pecimenlabel-Modal').modal('show');
    $('#pecimenlabel-Modal').modal('show');
    this.ngxLoader.stop();

  }
  goToEditonCaseId(caseId){
    var a = caseId+';2';
    var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
    var bx = ax.replace('+','xsarm');
    var cx = bx.replace('/','ydan');
    var ciphertext = cx.replace('=','zhar');
    console.log('ciphertext',ciphertext);

    this.router.navigateByUrl('/edit-case/'+ciphertext+'/1')
    // this.router.navigateByUrl('/edit-case/'+caseId+'/1;from='+ 2)
  }

  getLookups(){
    this.ngxLoader.start();
    return new Promise((resolve, reject) => {
      var caseCat = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=CASE_CATEGORY_CACHE&partnerCode=sip').then(response =>{
        return response;
      })
      var specType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=SPECIMEN_TYPE_CACHE&partnerCode=sip').then(response =>{
        return response;
      })
      var statusType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=CASE_STATUS_CACHE&partnerCode=sip').then(response =>{
        return response;
      })
      var accType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=CLIENT_TYPE_CACHE&partnerCode=sip').then(response =>{
      return response;
    })
    var specSource = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=SPECIMEN_SOURCE_CACHE&partnerCode=sip').then(response =>{
      return response;
    })
    var bodySite = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=BODY_SITE_CACHE&partnerCode=sip').then(response =>{
      return response;
    })
    var procedure = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=PROCEDURE_CACHE&partnerCode=sip').then(response =>{
      return response;
    })


      forkJoin([caseCat, specType, statusType, accType,specSource, bodySite, procedure]).subscribe(allLookups => {
        console.log("allLookups",allLookups);
        this.allCaseCategories     = this.sortPipe.transform(allLookups[0], "asc", "caseCategoryName");
        // this.allSpecimenTypes      = this.sortPipe.transform(allLookups[1], "asc", "specimenTypeName");
        this.allSpecimenTypes      = allLookups[1];
        // this.specimenType          = this.allSpecimenTypes[1];
        this.allStatusType         = this.sortPipe.transform(allLookups[2], "asc", "caseStatusName");
        this.allSpecimenSource     = allLookups[4];
        // this.allSBodySite          = this.sortPipe.transform(allLookups[5], "asc", "bodySiteName");
        this.allSBodySite          = allLookups[5];
        this.allProcedures         = allLookups[6];
        console.log("this.allSpecimenTypes",this.allSpecimenTypes);
        this.defaultAccountTypes     = this.sortPipe.transform(allLookups[3], "asc", "name");
        for (let i = 0; i < this.allCaseCategories.length; i++) {
          if (this.allCaseCategories[i].caseCategoryDefault == 1) {
            this.selectedCaseCat = this.allCaseCategories[i];
            this.saveRequest.data.caseCategoryId     = this.allCaseCategories[i].caseCategoryId;
            this.saveRequest.data.caseCategoryPrefix = this.allCaseCategories[i].caseCategoryPrefix;
          }
        }
        for (let j = 0; j < this.allSpecimenTypes.length; j++) {
          this.allSpecimenTypes[j]['labelToShow'] = this.allSpecimenTypes[j]['specimenTypePrefix']+' - '+this.allSpecimenTypes[j]['specimenTypeName'];
          if (this.allSpecimenTypes[j]['specimenTypePrefix'] == 'NP') {
            this.specimenType          = this.allSpecimenTypes[j];
            var myDate = new Date();
            var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyy HH:mm:ss');
            this.saveRequest.data.caseSpecimen[0].createdTimestamp = currentDate;
            this.saveRequest.data.caseSpecimen[0].createdBy        = this.logedInUserRoles.userCode;
            this.saveRequest.data.caseSpecimen[0].specimenTypeId     = this.specimenType.specimenTypeId;
            this.saveRequest.data.caseSpecimen[0].specimenSourceId   = this.allSpecimenSource[1]['specimenSourceId'];
            this.saveRequest.data.caseSpecimen[0].bodySiteId         = this.allSBodySite[2]['bodySiteId'];
            this.saveRequest.data.caseSpecimen[0].procedureId        = this.allProcedures[0]['procedureId'];
          }
        }
        resolve();

      })
    })

  }
  setCaseCatPrefix(){
    this.saveRequest.data.caseCategoryId     = this.selectedCaseCat.caseCategoryId;
    this.saveRequest.data.caseCategoryPrefix = this.selectedCaseCat.caseCategoryPrefix;
    // console.log("this.saveRequest",this.saveRequest);

  }
  // changespecimenType(){
  //   var myDate = new Date();
  //   var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyy HH:mm:ss');
  //   this.saveRequest.data.caseSpecimen[0].specimenTypeId = this.specimenType.specimenTypeId;
  //   this.saveRequest.data.caseSpecimen[0].createdTimestamp = currentDate;
  //   this.saveRequest.data.caseSpecimen[0].createdBy = this.logedInUserRoles.userCode;
  //   // console.log("this.saveRequest",this.saveRequest);
  //
  // }
  changespecimenType(){
    // console.log("this.specimenType",this.specimenType);
    console.log("this.allSpecimenTypes",this.allSpecimenTypes);
    // console.log("this.allSpecimenSource",this.allSpecimenSource);

    if (this.specimenType != null) {
        var myDate = new Date();
        var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyy HH:mm:ss');
        this.saveRequest.data.caseSpecimen[0].createdTimestamp = currentDate;
        this.saveRequest.data.caseSpecimen[0].createdBy        = this.logedInUserRoles.userCode;
      if (this.specimenType.specimenTypeId   == this.allSpecimenTypes[0].specimenTypeId) {
         this.saveRequest.data.caseSpecimen[0].specimenTypeId   = this.specimenType.specimenTypeId;
         this.saveRequest.data.caseSpecimen[0].specimenSourceId   = this.allSpecimenSource[3]['specimenSourceId'];
         this.saveRequest.data.caseSpecimen[0].bodySiteId         = this.allSBodySite[0]['bodySiteId'];
         this.saveRequest.data.caseSpecimen[0].procedureId        = this.allProcedures[0]['procedureId'];
      }
      else if (this.specimenType.specimenTypeId == this.allSpecimenTypes[1].specimenTypeId) {
        this.saveRequest.data.caseSpecimen[0].specimenTypeId   = this.specimenType.specimenTypeId;
        this.saveRequest.data.caseSpecimen[0].specimenSourceId   = this.allSpecimenSource[1]['specimenSourceId'];
        this.saveRequest.data.caseSpecimen[0].bodySiteId         = this.allSBodySite[2]['bodySiteId'];
        this.saveRequest.data.caseSpecimen[0].procedureId        = this.allProcedures[0]['procedureId'];
      }
      else if (this.specimenType.specimenTypeId   == this.allSpecimenTypes[2].specimenTypeId) {
        this.saveRequest.data.caseSpecimen[0].specimenTypeId   = this.specimenType.specimenTypeId;
        this.saveRequest.data.caseSpecimen[0].specimenSourceId   = this.allSpecimenSource[2]['specimenSourceId'];
        this.saveRequest.data.caseSpecimen[0].bodySiteId         = this.allSBodySite[3]['bodySiteId']
        this.saveRequest.data.caseSpecimen[0].procedureId        = this.allProcedures[0]['procedureId'];
      }
      else if (this.specimenType.specimenTypeId   == this.allSpecimenTypes[3].specimenTypeId) {
        this.saveRequest.data.caseSpecimen[0].specimenTypeId     = this.specimenType.specimenTypeId;
        this.saveRequest.data.caseSpecimen[0].specimenSourceId   = this.allSpecimenSource[0]['specimenSourceId'];
        this.saveRequest.data.caseSpecimen[0].bodySiteId         = this.allSBodySite[1]['bodySiteId'];
        this.saveRequest.data.caseSpecimen[0].procedureId        = this.allProcedures[1]['procedureId'];

      }

      // console.log('this.saveRequest.data.caseSpecimen[0]',this.saveRequest.data.caseSpecimen[0]);


    }
  }
  resetPatient(){
    this.patientFName                     = null;
    this.patientLName                     = null;
    this.patientDOB                       = null;
    this.selectedPatientID                = null;
    return;
  }
  resetFields(){
    console.log('reset fields---');

    this.caseForm.controls['collectionDate'].reset();
    this.allIntakesForClient = [];
    this.saveRequest.data.intakeId = null;
    if (this.logedInUserRoles.userType == 1) {
      this.saveRequest.data.collectedBy = "1"
    }
    else{
      this.saveRequest.data.collectedBy = "2"
    }
    this.allClientName = [];
    this.clientSearchString = null;
    this.clientLoading = false;
    this.allAtdPhysicians = [];
    this.selectedAttPhysician= null;
    for (let i = 0; i < this.allCaseCategories.length; i++) {
      if (this.allCaseCategories[i].caseCategoryDefault == 1) {
        this.selectedCaseCat = this.allCaseCategories[i];
        this.saveRequest.data.caseCategoryId     = this.allCaseCategories[i].caseCategoryId;
        this.saveRequest.data.caseCategoryPrefix = this.allCaseCategories[i].caseCategoryPrefix;
      }
    }
    this.specimenType          = this.allSpecimenTypes[1];
    this.patientFName                     = null;
    this.patientLName                     = null;
    this.patientDOB                       = null;
  }

  triageCase(caseId){
    console.log("casid",caseId);

    var myDate = new Date();
    var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyy HH:mm:ss');
    var triageUrl = environment.API_COVID_ENDPOINT + 'triage';
    var triageRequest = {...this.triageRequest}
    triageRequest.data.caseId = caseId;
    triageRequest.data.testStatusId = 1;
    triageRequest.data.testId = 2;
    triageRequest.data.createdTimestamp = currentDate;
    triageRequest.data.createdBy = this.logedInUserRoles.userCode;
    triageRequest.header.userCode = this.logedInUserRoles.userCode;
    triageRequest.header.functionalityCode = "SPCA-CQ";
    this.specimenService.triageRequest(triageUrl,triageRequest).subscribe(triageResp=>{
      console.log('triageResp',triageResp);
      this.ngxLoader.stop();

    },error=>{
      this.notifier.notify('error','error while triaging')
      this.ngxLoader.stop();
    })


  }
  zeroPadded(val) {
    if (val >= 10)
      return val;
    else
      return '0' + val;
  }
  zeroPaddedTime(val) {
    if (val >= 10)
      return val;
    else
      return '0' + val;
  }

  getSelectedRecord(intakeRecord) {

      return new Promise((resolve, reject) => {
        var d = new Date(intakeRecord['collectionTimestamp'])
        // console.log("d",d);
        // console.log("intakeRecord['collectedBy']",intakeRecord['collectedBy']);
        this.caseForm.get('intakeid').disable();
        if (typeof intakeRecord['manageIntake'] == 'undefined') {  ///////// not comming from intake Page so disable specimen type
          this.caseForm.get('specimenType').disable();
        }

        this.caseForm.get('clientName').disable();

        var collectionDate = d.getFullYear()+"-"+this.zeroPadded(d.getMonth() + 1)+"-"+this.zeroPadded(d.getDate())+"T"+this.zeroPaddedTime(d.getHours())+":"+this.zeroPaddedTime(d.getMinutes());
        this.saveRequestCollectionDate = collectionDate;
        // this.saveRequestCollectionDate = intakeRecord['collectionTimestamp'];
        if (intakeRecord['collectedBy'] == "SIP") {
          this.saveRequest.data.collectedBy = "1";
        }
        if (intakeRecord['collectedBy'] == 1) {
          this.saveRequest.data.collectedBy = "1";
        }
        if (intakeRecord['collectedBy'] == 2){
            this.saveRequest.data.collectedBy = "2";
        }
        if (intakeRecord['collectedBy'] == "CLIENT"){
            this.saveRequest.data.collectedBy = "2";
        }
        // this.saveRequest.data.collectedBy = intakeRecord['collectedBy'].toString();
        this.saveRequest.data.clientId = intakeRecord['clientId'];
        var clURL           = environment.API_CLIENT_ENDPOINT + "clientwithuser"
        var clReqData       = {
          header:{
            uuid               :"",
            partnerCode        :"",
            userCode           :"",
            referenceNumber    :"",
            systemCode         :"",
            moduleCode         :"CLIM",
            functionalityCode: "SPCA-CQ",
            systemHostAddress  :"",
            remoteUserAddress  :"",
            dateTime           :""
          },
          data:{
            clientIds:[intakeRecord['clientId']],
            userCodes: []}
          }
          clReqData['header'].partnerCode = this.logedInUserRoles.partnerCode;
          clReqData['header'].userCode = this.logedInUserRoles.userCode;
          clReqData['header'].functionalityCode    = "CLTA-VC";

        this.http.post(clURL,clReqData).toPromise().then(clientResp =>{
          // console.log("clientFromIntakeRecord",clientResp);
          this.allClientName = clientResp['data'];
          this.clientLoading = false;
         this.clientSearchString = clientResp['data'][0];
         this.clientNameToPrint = clientResp['data'][0].name;
          if (clientResp['data'].length >0) {

            const uniqueUIds = [];
            const map = new Map();
            if (clientResp['data'][0].active == 1) {
              // var code = this.encryptDecrypt.encryptString(item.userCode)
            uniqueUIds.push(clientResp['data'][0].userCode);
          }
            // for (const item of clientResp['data'][0].clientUsers) {
            //   if (item.active == 1) {
            //     // var code = this.encryptDecrypt.encryptString(item.userCode)
            //   if(!map.has(item.userCode)){
            //     map.set(item.userCode, true);    // set any value to Map
            //     uniqueUIds.push(item.userCode);
            //   }
            // }
            // }
            if (uniqueUIds.length == 0) {
              alert("no active Physician found for this client please select a diffirent client")
              this.saveRequest.data.clientId = null;
              this.clientSearchString = null;
              return;
            }
            // console.log("uniqueUIds----",uniqueUIds);

            /////// get Attending Physician name and att physician
            var attphysUrl = environment.API_USER_ENDPOINT + 'attendingphysicians'
            var reqData={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"",functionalityCode: "SPCA-CQ",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{userCodes:uniqueUIds}}
            reqData.header.userCode    = this.logedInUserRoles.userCode
            reqData.header.partnerCode = this.logedInUserRoles.partnerCode
            reqData.header.functionalityCode = "SPCA-CQ"
            // console.log('reqData',reqData);
            this.specimenService.getAttPhysician(attphysUrl, reqData).subscribe(attResp => {
              console.log("att Physicain Resp",attResp);
              if (attResp['data'].length > 0) {

                attResp['data'].map((i) => { i.name = i.firstName + ' ' + i.lastName; return i; });

                // console.log('this.allAtdPhysicians',this.allAtdPhysicians);
                this.allAtdPhysicians = attResp['data'];
                this.selectedAttPhysician = this.allAtdPhysicians[0];
                // for (let i = 0; i < clientResp['data'][0].clientUsers.length; i++) {
                //   for (let j = 0; j <   this.allAtdPhysicians.length; j++) {
                //     if (clientResp['data'][0].clientUsers[i].defaultPhysician == 1) {
                //       if (  this.allAtdPhysicians[j].userCode == clientResp['data'][0].clientUsers[i].userCode) {
                //         this.selectedAttPhysician = this.allAtdPhysicians[j];
                //         // console.log('here');
                //         break;
                //       }
                //     }
                //
                //   }
                // }
              }
              else{
                alert("no Physician found for this client please select a diffirent client")
                this.saveRequest.data.clientId = null;
                this.clientSearchString = null;
              }
              this.checkPatient();

            },
            error =>{
              this.ngxLoader.stop();
              this.notifier.notify( "error", "Error while loading Attending Physicians for the client")
            })
          }
          else{
            alert('client doesnot have a user please select a different client 111')
            this.clientSearchString = null;
            this.saveRequest.data.clientId = null;
          }

          // return clientResp;

          ////////////////// select case category based on acc type
          for (let m = 0; m < this.defaultAccountTypes.length; m++) {
            if (typeof this.clientSearchString.clientTypeDto[0] != 'undefined') {
              if (this.clientSearchString.clientTypeDto[0].clientTypeId != null) {
                if (this.defaultAccountTypes[m].clientTypeId == this.clientSearchString.clientTypeDto[0].clientTypeId) {
                  // console.log('-----',this.defaultAccountTypes[m]);

                  if (this.defaultAccountTypes[m].name.toLowerCase().indexOf('nursing')>-1 ) {
                    for (let i = 0; i < this.allCaseCategories.length; i++) {
                      if (this.allCaseCategories[i].caseCategoryName.toLowerCase().indexOf('nursing')>-1) {
                        this.selectedCaseCat = this.allCaseCategories[i]
                        this.saveRequest.data.caseCategoryId     = this.selectedCaseCat.caseCategoryId;
                        this.saveRequest.data.caseCategoryPrefix = this.selectedCaseCat.caseCategoryPrefix;
                      }
                    }
                  }
                  else if (this.defaultAccountTypes[m].name.toLowerCase().indexOf('surgical')>-1 ) {
                    for (let i = 0; i < this.allCaseCategories.length; i++) {
                      if (this.allCaseCategories[i].caseCategoryName.toLowerCase().indexOf('surgical')>-1) {
                        this.selectedCaseCat = this.allCaseCategories[i]
                        this.saveRequest.data.caseCategoryId     = this.selectedCaseCat.caseCategoryId;
                        this.saveRequest.data.caseCategoryPrefix = this.selectedCaseCat.caseCategoryPrefix;
                      }
                    }
                  }
                  else if (this.defaultAccountTypes[m].name.toLowerCase().indexOf('covid-19')>-1 ){
                    ///covid
                    for (let i = 0; i < this.allCaseCategories.length; i++) {
                      if (this.allCaseCategories[i].caseCategoryName.toLowerCase().indexOf('covid-19')>-1) {
                        this.selectedCaseCat = this.allCaseCategories[i]
                        this.saveRequest.data.caseCategoryId     = this.selectedCaseCat.caseCategoryId;
                        this.saveRequest.data.caseCategoryPrefix = this.selectedCaseCat.caseCategoryPrefix;
                      }
                    }
                  }
                  else if (this.defaultAccountTypes[m].name.toLowerCase().indexOf('Covid General Case')>-1 ){
                    ///covid
                    for (let i = 0; i < this.allCaseCategories.length; i++) {
                      if (this.allCaseCategories[i].caseCategoryName.toLowerCase().indexOf('Covid General Case')>-1) {
                        this.selectedCaseCat = this.allCaseCategories[i]
                        this.saveRequest.data.caseCategoryId     = this.selectedCaseCat.caseCategoryId;
                        this.saveRequest.data.caseCategoryPrefix = this.selectedCaseCat.caseCategoryPrefix;
                      }
                    }
                  }
                  else if (this.defaultAccountTypes[m].name.toLowerCase().indexOf('Covid Employee Case')>-1 ){
                    ///covid
                    for (let i = 0; i < this.allCaseCategories.length; i++) {
                      if (this.allCaseCategories[i].caseCategoryName.toLowerCase().indexOf('Covid Employee Case')>-1) {
                        this.selectedCaseCat = this.allCaseCategories[i]
                        this.saveRequest.data.caseCategoryId     = this.selectedCaseCat.caseCategoryId;
                        this.saveRequest.data.caseCategoryPrefix = this.selectedCaseCat.caseCategoryPrefix;
                      }
                    }
                  }
                  else{
                    ///covid
                    for (let i = 0; i < this.allCaseCategories.length; i++) {
                      if (this.allCaseCategories[i].caseCategoryName.toLowerCase().indexOf('covid-19')>-1) {
                        this.selectedCaseCat = this.allCaseCategories[i]
                        this.saveRequest.data.caseCategoryId     = this.selectedCaseCat.caseCategoryId;
                        this.saveRequest.data.caseCategoryPrefix = this.selectedCaseCat.caseCategoryPrefix;
                      }
                    }
                  }
                }
              }
            }

          }

          for (let i = 0; i < this.allSpecimenTypes.length; i++) {
            if (this.allSpecimenTypes[i].specimenTypeId == intakeRecord['specimenTypeId']) {
              this.specimenType          = this.allSpecimenTypes[i];
              var myDate = new Date();
              var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyy HH:mm:ss');
              this.saveRequest.data.caseSpecimen[0].specimenTypeId = this.allSpecimenTypes[i].specimenTypeId;
              this.saveRequest.data.caseSpecimen[0].createdTimestamp = currentDate;
              this.saveRequest.data.caseSpecimen[0].createdBy = this.logedInUserRoles.userCode;
            }

          }


        },error=>{
          this.ngxLoader.stop();
          this.notifier.notify( "error", "Error while loading client name")
        })
        if (this.saveRequest.data.clientId != null) {
          this.getIntakeForClient(this.saveRequest.data.clientId)

        }

      });
  }

  getIntakeForClient(clientId){
    var reqAllIntakes = {...this.allIntakesReq}
    reqAllIntakes.header.partnerCode = this.logedInUserRoles.partnerCode;
    reqAllIntakes.header.userCode = this.logedInUserRoles.userCode;
    reqAllIntakes.header.functionalityCode = "SPCA-MI";
    reqAllIntakes.data.clientId                = [clientId] ;
    reqAllIntakes.data.searchString                = 'byclient';
    let intakeUrl    = environment.API_SPECIMEN_ENDPOINT + 'showintakes';
    this.specimenService.getIntakesForCases(intakeUrl, reqAllIntakes).subscribe(getIntakeResp=>{
      console.log("getIntakeResp",getIntakeResp);
      this.allIntakesForClient = getIntakeResp['data']['caseDetails'];
      // console.log('this.intakeFromIntakePage',this.intakeFromIntakePage);
      if(this.intakeFromIntakePage != null){

        this.saveRequest.data.intakeId = this.intakeFromIntakePage;
      }



     })
  }





}
