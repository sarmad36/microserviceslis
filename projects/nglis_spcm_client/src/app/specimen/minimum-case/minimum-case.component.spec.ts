import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MinimumCaseComponent } from './minimum-case.component';

describe('MinimumCaseComponent', () => {
  let component: MinimumCaseComponent;
  let fixture: ComponentFixture<MinimumCaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MinimumCaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MinimumCaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
