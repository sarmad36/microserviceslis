import { Component, OnInit, ElementRef, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { environment } from '../../../../../../src/environments/environment';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Router, NavigationEnd  } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import Swal from 'sweetalert2'
declare var $ :any;
import { NotifierService } from 'angular-notifier';
import { SpecimenApiCallsService } from '../../services/specimen/specimen-api-calls.service';
import { GlobalApiCallsService } from '../../services/global/global-api-calls.service';
import { concat, Observable, of, Subject, forkJoin } from "rxjs";
// import { catchError, debounceTime, distinctUntilChanged, switchMap, tap } from "rxjs/operators";
import * as dymo from '../../../assets/js/dymo-label.js';
declare var dymo: any;
import { SortPipe } from "../../pipes/sort.pipe";
import { DatePipe } from '@angular/common';
import { DataService } from "../../../../../../src/services/data/data.service";
import { MinimumAccesionDataService } from "../../services/minimumaccessiondata.service";
import { filter } from 'rxjs/operators';
import { GlobalySharedModule } from './../../../../../../src/app/sharedmodules/globaly-shared/globaly-shared.module';
import { UrlGuard } from './../../../../../../src/services/gaurd/url.guard'
import { DataTableDirective } from 'angular-datatables';
import * as CryptoJS from 'crypto-js';


@Component({
  selector    : 'app-create-case',
  templateUrl : './create-case.component.html',
  styleUrls   : ['./create-case.component.css'],
  host        : {
    class     :'mainComponentStyle'
  }
})
export class CreateCaseComponent implements OnInit {

  @ViewChildren(DataTableDirective)

  dtElement              : QueryList<any>;
  dtTrigger              : Subject<CreateCaseComponent> = new Subject();

  public dtOptions       : DataTables.Settings[] = [];
  searchTerm$ = new Subject<string>();
  results     : Object;
  caseAttOldLength = 0;
  searchTermIntake$      = new Subject<string>();
  intakeLoading               : any;

  allCaseCategories         : any =  [];
  selectedCaseCat           = null;
  allpattypes               : any = [];
  allClientName             : any = [];
  // allClientName             = [];
  // allClientName             = [
  //   {id: 1,  name: "Client A"},
  //   {id: 2,  name: "Client B"}
  // ]
  allSpecimenTypes          : any = [];
  specimenType              = null;

  allSpecimenSource         : any = [];
  specimenSource            = null;

  allSBodySite              : any = [];
  bodySite                  = null;

  allStatusType             : any = [];

  allProcedures             : any = [];
  procedure                 = this.allProcedures[1];

  allAtdPhysicians         = []
  allInsuraneCompanies     : any = []
  allAttachmentTypes       : any = [];
  public caseForm           : FormGroup;
  submitted                 = false;
  insuranceCompany          ;
  saveTypeGlobal            = 0;
  saveModalTitle            = '';
  saveModalText             = '';

  public searchClientName = {
    sortColumn  :"name",
    sortingOrder:"asc",
    searchString:"",
    userCode    : ""
  }
  clientSearchString          ;
  clientLoading               : any;
  clientInput$                = new Subject<string>();

  public saveRequest          = {
    header  : {
      dateTime: "",
      functionalityCode: "SPCA-CD",
      moduleCode: "SPCM",
      partnerCode: "",
      referenceNumber: "",
      remoteUserAddress: "",
      systemCode: "",
      systemHostAddress: "",
      userCode: "",
      uuid: ""
    },
    data    :{
      caseCategoryPrefix   :"",
      caseNumber           :"",
      intakeId             : null,
      caseStatusId         :1,
      caseCategoryId       :2,
      clientId             :null,
      attendingPhysicianId :"376",
      patientId            :null,
      createdTimestamp     :"2020-10-22T11:02:03.523+00:00",
      receivingDate        :null,
      collectedBy          :"1",
      collectionDate       :null,
      sendDate       :null,
      accessionTimestamp   :"2020-10-22T07:54:10.485+00:00",
      createdBy            :"abc",
      caseAttachments :[
        {
          caseAttachmentId   :1,
          attachmentTypeId   :1,
          createdBy          :"abc",
          createdTimestamp   :"2020-10-22T07:19:19.384+00:00",
          file               :"c://test"
        }
      ],
      caseSpecimen   :[
        {
          caseSpecimenId    :1,
          specimenTypeId    :1,
          specimenSourceId  :1,
          bodySiteId        :1,
          procedureId       :2,
          createdBy         :"abc",
          createdTimestamp  :"2020-10-22T07:54:10.485+00:00",
          updatedBy         :"",
          updatedTimestamp  :""
        }
      ]

    }
  }

  pageType                     = 'add';
  selectedCase                 : any = []
  patLName                     = null;
  patFName                     = null;
  patDob                       = null;
  patGender                    = null;
  patType                      = null;
  patSSN                       = null;
  selectedPatientID            = null;
  specimenTable                = [];
  specToRemove ;
  selectattachmentTypeId       =null;
  attachmentTable              = [];
  selectedAttPhysician         = null;
  printedCaseNumber            = null;
  patientDOBPrint;
  lNamePrint
  fNamePrint;
  clientNameToPrint;
  viewCaseAction               = false;

  swalStyle              : any;
  public logedInUserRoles :any = {};
  loadedCasesOrID ;
  defaultAccountTypes : any;
  disableAll = false;
  pInsuranceID ;
  maxDate;
  maxDateRecieve;
  minDateRecieve;
  minDateCollection;
  maxDateCollection = null;
  maxSentDate;
  ageToSaveinDB;
  patientRecordUpdateafterCancel = true;
  patientLoadedCheck = false;
  saveAgain = false

  public deleteSpecimenRequest = {
    header  : {
      dateTime: "",
      functionalityCode: "SPCA-CD",
      moduleCode: "SPCM",
      partnerCode: "",
      referenceNumber: "",
      remoteUserAddress: "",
      systemCode: "",
      systemHostAddress: "",
      userCode: "",
      uuid: ""
    },
    data    :{
      caseSpecimenId: null

    }
  }
  DeletecaseSpecimenId = null;
  public loadResultsRequest = {
    header  : {
      dateTime: "",
      functionalityCode: "SPCA-CD",
      moduleCode: "TWMM",
      partnerCode: "",
      referenceNumber: "",
      remoteUserAddress: "",
      systemCode: "",
      systemHostAddress: "",
      userCode: "",
      uuid: ""
    },
    data    :{
      caseIds: []

    }
  }
  resultsTableData : any =[];
  covidTestLookup : any;
  testStatusLookup: any;
  allInterpretations: any;
  public triageRequest = {
    header  : {
      uuid              :"",
      partnerCode       :"",
      userCode          :"",
      referenceNumber   :"",
      systemCode        :"",
      moduleCode        :"TWMM",
      functionalityCode :"",
      systemHostAddress :"",
      remoteUserAddress :"",
      dateTime          :""
    },
    data : {
      caseId:"6",
      testStatusId:1,
      testId:1,
      createdBy:"abc",
      createdTimestamp: "26-11-2020 20:07:25"

    }
  }
  onlyPrint = false;
  allIntakesForClient : any = [];
  selectedIntakeId = null;
  public allIntakesReq = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"SPCM",
      functionalityCode:"SPCA-CD",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      page:"0",
      size:200,
      sortColumn:"createdTimestamp",
      sortingOrder:"desc",
      clientId:[],
      searchString:""
    }
  }
  showCrumbs = true;
  previousUrl: string = null;
  currentUrl: string = null;
  originalPatient : any ={}
  originalClient  : any ={}
  caseFromIntake = null;
  labelHistory : any = [];
  oldIntakeId;

  public finalReportReq=
  {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"TWMM",
      functionalityCode:"SPCA-CD",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      jobOrderIds:[],
      // deliveryStatusId:3
      pageSize:20,
      pageNumber:0
    }
  }
  public usersRequest: any = {
    header              : {
      uuid              : "",
      partnerCode       : "",
      userCode          : "",
      referenceNumber   : "",
      systemCode        : "",
      moduleCode        : "",
      functionalityCode : "",
      systemHostAddress : "",
      remoteUserAddress : "",
      dateTime          : ""
    },
    data                : {
      userCodes           :[]
    }
  }
  allReportsData : any = [];
  collectionDateToShow;
  recievingDateToShow;
  public paginatePaheHistory : any = 0;
  public paginateLengthHistory : any = 20;
  triggerHistoryTable = false;
  GLobalUniqueJoBOrderIdsForHistory = [];
  allHistoryDataCount = 0;
  pateientChanged = false;

  selectedPatientAddress :any ;
  selectedPatientContact :any ;
  sendDateToShow ;
  constructor(
    private formBuilder     : FormBuilder,
    private http            : HttpClient,
    private router          : Router ,
    private route           : ActivatedRoute,
    private sanitizer       : DomSanitizer,
    private ngxLoader       : NgxUiLoaderService,
    private notifier        : NotifierService,
    private specimenService : SpecimenApiCallsService,
    private globalService   : GlobalApiCallsService,
    private sortPipe        :SortPipe,
    private datePipe: DatePipe,
    private sharedData: DataService,
    private sharedIntakeData : MinimumAccesionDataService,
    private rbac         : UrlGuard,
    private encryptDecrypt : GlobalySharedModule
  ) {

    // this.rbac.checkROle('SPCA-CD');
    this.notifier        = notifier;
    this.swalStyle       =  Swal.mixin({
      customClass : {
        confirmButton : 'btn btn-success my-swal-success',
        cancelButton  : 'btn btn-danger'
      },
      buttonsStyling  : true
    })

  }

  ngOnInit(): void {
    this.router.events.pipe(filter(event => event instanceof NavigationEnd))
    .subscribe((event: NavigationEnd) => {

      this.previousUrl = this.currentUrl;
      this.currentUrl = event.url;
      console.log('this.previousUrl',this.previousUrl);
      console.log('this.currentUrl',this.currentUrl);

      if (this.previousUrl != null) {


        if (this.previousUrl.indexOf( "/edit-intake/") !== -1) {
          this.sharedIntakeData.loadCasesInIntake('null');
          this.sharedIntakeData.editCasesInIntake('null');

          // console.log("innnnnnnnn");

        }
      }
      else{
        this.sharedIntakeData.loadCasesInIntake('null');
        this.sharedIntakeData.editCasesInIntake('null');

        // console.log("nullllll");

      }


    });
    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
    var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
    var originalText = bts.toString(CryptoJS.enc.Utf8);
    this.logedInUserRoles = JSON.parse(originalText)

    // if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-UC") != -1) {
    //   // this.getClientUsers.header.functionalityCode    = "CLTA-UC";
    // }
    // else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-VC") != -1){
    //   // this.getClientUsers.header.functionalityCode    = "CLTA-VC";
    // }


    var dtToday = new Date();

    var m = dtToday.getMonth()+1;
    var d = dtToday.getDate();
    var year = dtToday.getFullYear();
    var month;
    var day;
    if(m < 10)
    month = '0' + m.toString();
    else
    month = m
    if(d < 10)
    day = '0' + d.toString();
    else
    day = d

    var maxDate = year + '-' + month + '-' + day;
    this.maxDate = maxDate;
    this.maxDateRecieve = maxDate+'T00:00';
    this.maxDateCollection = maxDate+'T00:00:00';
    this.clearDate();
    // this.popupDateClose();
    // this.minDateRecieve = maxDate+'T00:00';
    // this.minDateCollection = maxDate+'T00:00:00';

    // let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
    var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
    var originalText = bts.toString(CryptoJS.enc.Utf8);
    this.logedInUserRoles = JSON.parse(originalText)
    // this.ngxLoader.start();
    this.getLookups().then(lookupsLoaded => {

      this.route.params.subscribe(params => {
        // console.log('params',params['id']);
        if (typeof params['with'] == "undefined") {
          this.sharedIntakeData.updateCaseFromIntake.subscribe(intakeRecordCaseID => {
            console.log("--intakeRecordCaseID",intakeRecordCaseID);

            if (intakeRecordCaseID != 'null') {
              this.caseFromIntake = intakeRecordCaseID;
              this.showCrumbs = false;
              this.pageType = 'edit';
              if(this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UI") == -1 ){
                if(this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UC") != -1 ){
                  this.rbac.checkROle('SPCA-UC');
                }
                if(this.logedInUserRoles['allowedRoles'].indexOf("SPCA-VC") != -1 ){
                  this.rbac.checkROle('SPCA-VC');
                  this.viewCaseAction = true;
                  this.caseForm.disable();
                  // this.rbac.checkROle('SPCA-UC');
                }

              }

              let columnCaseReq  = {}
              var selectURL = environment.API_SPECIMEN_ENDPOINT + 'searchbycaseid';
              columnCaseReq      = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-CD",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{caseId: intakeRecordCaseID}}
              if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UC") != -1) {
                columnCaseReq['header'].functionalityCode    = "SPCA-UC";
              }
              else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-VC") != -1){
                columnCaseReq['header'].functionalityCode    = "SPCA-VC";
              }
              else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-CD") != -1){
                columnCaseReq['header'].functionalityCode    = "SPCA-CD";
              }
              this.getSelectedRecord(selectURL,columnCaseReq).then(selectedCase => {
                // console.log("Selected Case response",selectedCase);
                // this.chec();
                this.oldIntakeId = this.selectedCase.intakeId;
                console.log("selectedCase",this.selectedCase);
                console.log("saveRequest",this.saveRequest);
                // this.ngxLoader.stop();
                this.changespecimenType()
              })

            }else{
              this.pageType = 'add';
              if(this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UI") == -1 ){
                this.rbac.checkROle('SPCA-CD');
              }

              if (this.logedInUserRoles.userType == 1) {
                this.saveRequest.data.collectedBy = "1"
              }
              else{
                this.saveRequest.data.collectedBy = "2"
              }
              this.changespecimenType()
            }

          })
        }
        else{
          this.pageType = 'edit';
          if(this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UI") == -1 ){
            if(this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UC") != -1 ){
              this.rbac.checkROle('SPCA-UC');
            }
            if(this.logedInUserRoles['allowedRoles'].indexOf("SPCA-VC") != -1 ){
              this.rbac.checkROle('SPCA-VC');
              this.viewCaseAction = true;
              this.caseForm.disable();
            }
          }
          // var notGen = history.state;
          console.log("params----",params);
          var coverted = params['id'].replace('xsarm','+').replace('ydan','/').replace('zhar','=');
          var bytes  = CryptoJS.AES.decrypt(coverted, 'nglis');
          var originalText = bytes.toString(CryptoJS.enc.Utf8).toString();
          console.log("originalText",originalText);
          var test = originalText.split(';')
          console.log("test",test);

          let from = test[1];
          // let from = params['from']
          console.log("notGen",from);
          // if (notGen.navigationId == 1 && typeof notGen.id == 'undefined') {
          //   // this.router.navigateByUrl('/manage-cases');
          // }
          if(from == 2){
            let columnCaseReq  = {}
            var selectURL = environment.API_SPECIMEN_ENDPOINT + 'searchbycaseid';
            columnCaseReq      = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-CD",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{caseId: test[0]}}
            if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UC") != -1) {
              columnCaseReq['header'].functionalityCode    = "SPCA-UC";
            }
            else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-VC") != -1){
              columnCaseReq['header'].functionalityCode    = "SPCA-VC";
            }
            else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-CD") != -1){
              columnCaseReq['header'].functionalityCode    = "SPCA-CD";
            }
            // columnCaseReq      = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-CD",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{caseId: params['id']}}
            this.getSelectedRecord(selectURL,columnCaseReq).then(selectedCase => {
              // console.log("Selected Case response",selectedCase);
              // this.chec();
              console.log("selectedCase",this.selectedCase);
              console.log("saveRequest",this.saveRequest);
              // this.ngxLoader.stop();
              this.changespecimenType()


            });
          }
          else{
            if (from == 3) {
              this.caseForm.get('attType').clearValidators();
              this.caseForm.controls["attType"].updateValueAndValidity();
              this.caseForm.get('attFile').clearValidators();
              this.caseForm.controls["attFile"].updateValueAndValidity();
            }
            let columnCaseReq  = {}
            var selectURL = environment.API_SPECIMEN_ENDPOINT + 'searchbycasenumber';

            columnCaseReq      = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-CD",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{page:"0",size:20,sortColumn:"caseNumber",sortingOrder:"desc",searchDate:"",caseNumber: test[0]}}
            if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UC") != -1) {
              columnCaseReq['header'].functionalityCode    = "SPCA-UC";
            }
            else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-VC") != -1){
              columnCaseReq['header'].functionalityCode    = "SPCA-VC";
            }
            else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-CD") != -1){
              columnCaseReq['header'].functionalityCode    = "SPCA-CD";
            }
            // columnCaseReq      = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-CD",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{page:"0",size:20,sortColumn:"caseNumber",sortingOrder:"desc",searchDate:"",caseNumber: params['id']}}
            this.getSelectedRecord(selectURL,columnCaseReq).then(selectedCase => {
              // console.log("Selected Case response",selectedCase);
              // this.chec();
              // console.log("selectedCase",this.selectedCase);
              console.log("saveRequest",this.saveRequest);
              // this.ngxLoader.stop();
              this.changespecimenType()


            });
          }


        }
        console.log('pageType:  ',this.pageType);
        // if(this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UC") != -1 && this.logedInUserRoles['allowedRoles'].indexOf("SPCA-VC") != -1){
        //   console.log("Selected Haris",this.pageType);
        //   this.caseForm.disable();
        // }
        // if(this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UC") != -1 && this.logedInUserRoles['allowedRoles'].indexOf("SPCA-VC") != -1){
        //   console.log("Selected Haris2",this.pageType);
        //   this.caseForm.enable();
        // }

      })

      // this.ngxLoader.stop();
    });
    this.sharedIntakeData.loadIntakeCases.subscribe(intakeCases => {
      if (intakeCases != 'null') {
        this.caseForm.get('clientName').disable();
        this.caseForm.get('collectionDate').disable();
      }
      else{

      }

    })
    this.specimenService.search(this.searchTerm$)
    .subscribe(results => {
      console.log("results",results);
      this.allClientName = results['data'];
      this.clientLoading = false;
    },
    error => {
      this.notifier.notify( "error", "Error while fetching clients backend connect aborted" );
      this.clientLoading = false;
      return;
      // console.log("error",error);
    });
    /////////////// load intake id for search active
    this.specimenService.searchIntake(this.searchTermIntake$)
    .subscribe(resultsSearch => {
      // console.log("intake",resultsSearch);
      this.allIntakesForClient = resultsSearch['data'];
      this.intakeLoading = false;
    },
    error => {
      this.notifier.notify( "error", "Error while fetching intakes backend connect aborted" );
      this.intakeLoading = false;
      return;
      // console.log("error",error);
    });
    this.caseForm       = this.formBuilder.group({
      caseCategory      : ['', [Validators.required]],
      clientName        : ['', [Validators.required]],
      atdPhysician      : ['', [Validators.required]],
      collectionDate    : ['', [Validators.required]],
      collectedBy       : ['1', [Validators.required]],
      recievingDate     : ['', [Validators.required]],
      sendDate          : ['', [Validators.required]],
      pLastName         : ['', [Validators.required]],
      pFirstName        : ['', [Validators.required]],
      pDOB              : ['', [Validators.required]],
      pGender           : ['', [Validators.required]],
      pType             : ['', [Validators.required]],
      pSSN              : [''],
      pInsuranceCompany : [{value: '', disabled: true}],
      pInsuranceID      : [{value: '', disabled: true}],
      specimenType      : ['', [Validators.required]],
      specimenSource    : ['', [Validators.required]],
      bodySite          : ['', [Validators.required]],
      procedure         : ['', [Validators.required]],
      attType           : [''],
      attFile           : [''],
      intakeid          : ['']
    })


    // if (this.pageType == 'add') {
    //   this.rbac.checkROle('SPCA-CD');
    //
    // }else{
    //   this.rbac.checkROle('SPCA-UC');
    //
    // }
    this.dtOptions[3] = {
      pagingType   : 'full_numbers',
      pageLength   : this.paginateLengthHistory,
      serverSide   : true,
      processing   : true,
      ordering     : false,
      searching   : false,
      "lengthMenu": [20, 50, 75, 100 ],
      ajax: (dataTablesParameters: any, callback) => {
        if (this.triggerHistoryTable == true) {


          this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {

            dtElement.dtInstance.then((dtInstance: any) => {
              if (dtInstance.table().node().id == 'rep-history' ) {
                this.paginatePaheHistory           = dtElement['dt'].page.info().page;
                this.paginateLengthHistory         = dtElement['dt'].page.len();
                // console.log('this.GLobalUniqueJoBOrderIdsForHistory',this.GLobalUniqueJoBOrderIdsForHistory);
                // return;

                this.fetchFinalReport(this.GLobalUniqueJoBOrderIdsForHistory).then( jobResponse =>{
                  callback({
                    recordsTotal    :  this.allHistoryDataCount,
                    recordsFiltered :  this.allHistoryDataCount,
                    data: []
                  });
                })
              }
            })
          })
        }
      }
    };


  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  chec(e,type){
    console.log('e',e.target.value);
    setTimeout(()=>{
      // if (this.recievingDateToShow == null) {
      //
      // }
      // else{
      //   var a = this.datePipe.transform(new Date(this.recievingDateToShow), 'MM/dd/yyyy');
      //   var b = this.datePipe.transform(new Date(this.collectionDateToShow), 'MM/dd/yyyy');
      //   if (this.compare_dates(a, b) == 2) {
      //   // if (this.recievingDateToShow < this.collectionDateToShow) {
      //     // this.recievingDateToShow = null;
      //
      //   }
      // }

      if (type == 'c') {
        var a = this.datePipe.transform(new Date(e.target.value), 'MM/dd/yyyy');
        var b = this.datePipe.transform(new Date(), 'MM/dd/yyyy');
        if (this.compare_dates(a, b) == 1) {
          // if (e.target.value > (this.maxDateCollection)) {
          alert("collection date greater than current")
          // this.collectionDateToShow = null;
          // this.recievingDateToShow = null;
          if (this.pageType == 'edit') {
            this.recievingDateToShow  = this.selectedCase.receivingDate;
            this.collectionDateToShow = this.selectedCase.collectionDate;
          }
          else{
            this.collectionDateToShow = null;
            this.recievingDateToShow = null;

          }

        }
        else{
          console.log('e.target.value',e.target.value);

          this.minDateRecieve = e.target.value;
        }
        // this.maxSentDate = e.target.value;
      }
      else if (type == 'r') {
        var a = this.datePipe.transform(new Date(e.target.value), 'MM/dd/yyyy');
        var b = this.datePipe.transform(this.minDateRecieve, 'MM/dd/yyyy');
        if (this.compare_dates(a, b) == 2) {
          // if (this.compare_dates(new Date(e.target.value), new Date(this.minDateRecieve)) == 2) {
          // if (e.target.value < (this.minDateRecieve)) {
          alert("recieving date less than collection")
          // return;
          if (this.pageType == 'edit') {
            this.recievingDateToShow = this.selectedCase.receivingDate;
          }
          else{
            this.recievingDateToShow = null;

          }
        }
        var c = this.datePipe.transform(this.sendDateToShow, 'MM/dd/yyyy');
        if (this.compare_dates(a, c) == 2) {
          this.sendDateToShow = null;
        }
        this.maxSentDate = e.target.value;
        console.log('maxSentDate',this.maxSentDate);


      }


    }, 800)





  }
  sentDateValidation(e){
    console.log('e',e.target.value);
    setTimeout(()=>{
        var a = this.datePipe.transform(new Date(e.target.value), 'MM/dd/yyyy');
        var b = this.datePipe.transform(this.recievingDateToShow, 'MM/dd/yyyy');
        if (this.compare_dates(a, b) == 1) {
          // if (e.target.value > (this.maxDateCollection)) {
          alert("sent date date greater than collection date")
          // this.collectionDateToShow = null;
          // this.recievingDateToShow = null;
          if (this.pageType == 'edit') {
            this.sendDateToShow  = this.selectedCase.sendDate;
          }
          else{
            this.sendDateToShow = null;

          }

        }

    }, 800)


  }
  zeroPadded(val) {
    if (val >= 10)
    return val;
    else
    return '0' + val;
  }
  zeroPaddedTime(val) {
    if (val >= 10)
    return val;
    else
    return '0' + val;
  }
  getSelectedRecord(url,data) {

    return new Promise((resolve, reject) => {
      this.http.post(url,data).toPromise()
      .then( resp => {
        if (typeof resp['result'] != 'undefined') {
          if (typeof resp['result']['description'] != 'undefined') {
            if (resp['result']['description'].indexOf('UnAuthorized') != -1) {
              this.ngxLoader.stop()
              this.notifier.notify('warning',resp['result']['description'])
              this.router.navigateByUrl('/page-restrict');
            }
          }
        }
        this.ngxLoader.start();

        console.log('resolve Selected',resp);
        var d = new Date(resp['data'].collectionDate)
        // console.log('d',d);

        var collectionDate = d.getFullYear()+"-"+this.zeroPadded(d.getMonth() + 1)+"-"+this.zeroPadded(d.getDate())+"T"+this.zeroPaddedTime(d.getHours())+":"+this.zeroPaddedTime(d.getMinutes());
        // var dateTime = d.getFullYear()+"-"+this.zeroPadded(d.getMonth() + 1)+"-"+this.zeroPadded(d.getDate())+"T"+this.zeroPaddedTime(d.getHours())+":"+this.zeroPaddedTime(d.getMinutes())+":"+this.zeroPaddedTime(d.getSeconds())
        resp['data'].collectionDate = collectionDate;
        this.collectionDateToShow = collectionDate;
        var d2 = new Date(resp['data'].receivingDate)
        this.minDateRecieve = this.collectionDateToShow;
        this.minDateCollection = this.collectionDateToShow;
        // this.maxSentDate   = this.collectionDateToShow;

        var recievingDate = d2.getFullYear()+"-"+this.zeroPadded(d2.getMonth() + 1)+"-"+this.zeroPadded(d2.getDate())+"T"+this.zeroPaddedTime(d2.getHours())+":"+this.zeroPaddedTime(d2.getMinutes());
        // var recievingDate = d2.getFullYear()+"-"+this.zeroPadded(d2.getMonth() + 1)+"-"+this.zeroPadded(d2.getDate())
        resp['data'].receivingDate = recievingDate;
        this.recievingDateToShow = recievingDate;
        this.maxSentDate   = this.recievingDateToShow;



        if (resp['data'].sendDate !=null) {
          var d3 = new Date(resp['data'].sendDate)
          var sendDate = d3.getFullYear()+"-"+this.zeroPadded(d3.getMonth() + 1)+"-"+this.zeroPadded(d3.getDate())+"T"+this.zeroPaddedTime(d3.getHours())+":"+this.zeroPaddedTime(d3.getMinutes());
          var recievingDate = d2.getFullYear()+"-"+this.zeroPadded(d2.getMonth() + 1)+"-"+this.zeroPadded(d2.getDate())
          resp['data'].sendDate = sendDate;
          this.sendDateToShow = sendDate;
        }

        // this.sendDateToShow = this.datePipe.transform(resp['data'].sendDate, 'yyyy-MM-dd hh:mm a');
        // console.log("  this.sendDateToShow ",this.sendDateToShow );


        // $(".clearable__clear").show("");
        // this.maxDateCollection   = this.recievingDateToShow;
        /////// get client name and att physician
        // this.ngxLoader.start();
        this.originalClient = resp['data'].clientId;
        var clURL           = environment.API_CLIENT_ENDPOINT + "clientwithuser"
        var clReqData       = {
          header:{
            uuid               :"",
            partnerCode        :"",
            userCode           :"",
            referenceNumber    :"",
            systemCode         :"",
            moduleCode         :"CLIM",
            functionalityCode  :"",
            systemHostAddress  :"",
            remoteUserAddress  :"",
            dateTime           :""
          },
          data:{
            clientIds:[resp['data'].clientId],
            userCodes: []}
          }
          clReqData['header'].partnerCode = this.logedInUserRoles.partnerCode;
          clReqData['header'].userCode = this.logedInUserRoles.userCode;
          clReqData['header'].functionalityCode    = "CLTA-VC";
          // if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-UC") != -1) {
          //   clReqData['header'].functionalityCode    = "CLTA-UC";
          // }
          // else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-VC") != -1){
          //   clReqData['header'].functionalityCode    = "CLTA-VC";
          // }
          // else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-CD") != -1){
          //   clReqData['header'].functionalityCode    = "CLTA-CD";
          // }
          // clReqData['header'].functionalityCode = "SPCA-CD";

          var getClient = this.http.post(clURL,clReqData).toPromise().then(clientResp =>{
            // this.allClientName = clientResp;
            return clientResp;
            //////call to get all phsicians
          })
          this.getIntakeForClient(resp['data'].clientId);

          /////// get Attending Physician name and att physician
          var attphysUrl   = environment.API_USER_ENDPOINT + 'attendingphysicians'
          var reqData={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"",functionalityCode:"SPCA-CD",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{userCodes:[resp['data']['attendingPhysicianId']]}}
          reqData.header.userCode    = this.logedInUserRoles.userCode
          reqData.header.partnerCode = this.logedInUserRoles.partnerCode
          reqData['header'].functionalityCode    = "USRA-MUE";
          
          // if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-UC") != -1) {
          //   reqData['header'].functionalityCode    = "CLTA-UC";
          // }
          // else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-VC") != -1){
          //   reqData['header'].functionalityCode    = "CLTA-VC";
          // }
          // else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-CD") != -1){
          //   reqData['header'].functionalityCode    = "CLTA-CD";
          // }
          // reqData.header.functionalityCode = "SPCA-CD"
          var getPhysician = this.http.put(attphysUrl,reqData).toPromise().then(physicianResp =>{
            return physicianResp;
          })

          //////////////// get patient info
          var selectURL = environment.API_PATIENT_ENDPOINT + 'patientbyid';
          var getPatientReq={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"",functionalityCode:"PNTA-MP",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{username:"danish",patientId:resp['data']['patientId']}}
          // if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-UC") != -1) {
          //   getPatientReq['header'].functionalityCode    = "CLTA-UC";
          // }
          // else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-VC") != -1){
          //   getPatientReq['header'].functionalityCode    = "CLTA-VC";
          // }
          // else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-CD") != -1){
          //   getPatientReq['header'].functionalityCode    = "CLTA-CD";
          // }
          var getPatient = this.http.put(selectURL,getPatientReq).toPromise().then(patientResp =>{
            this.originalPatient = patientResp;
            this.selectedPatientContact = this.originalPatient.data.patientDemographics.contacts;
            this.selectedPatientAddress = this.originalPatient.data.patientDemographics.address;
            this.selectedPatientID = this.originalPatient.data.patientId;
            return patientResp;
          })

          //////////////// get files
          var attData   = []
          var attHolder = {}
          var filesURL = environment.API_SPECIMEN_ENDPOINT + 'loadfiles';
          var caseId = '';
          var caseNum = '';
          caseId = resp['data'].caseId;
          // if (resp['data'].caseNumber == null) {
          //   caseId  = resp['data'].caseId;
          //   caseNum = '';
          // }
          // else{
          //   caseNum = resp['data'].caseNumber;
          //   caseId  = '';
          // }
          var getFilesReq={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-CA",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{caseId:caseId}}
          getFilesReq['header'].functionalityCode    = "SPCA-MC";
          // if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UC") != -1) {
          //   getFilesReq['header'].functionalityCode    = "SPCA-UC";
          // }
          // else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-VC") != -1){
          //   getFilesReq['header'].functionalityCode    = "SPCA-VC";
          // }
          // else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-CD") != -1){
          //   getFilesReq['header'].functionalityCode    = "SPCA-CD";
          // }
          this.http.post(filesURL,getFilesReq).toPromise().then(fileResp =>{
            console.log("fileResp",fileResp);
            if (fileResp['data'] != null) {
              this.caseAttOldLength = fileResp['data'].length;
              if (fileResp['data'] != null) {
                for (let m = 0; m < fileResp['data'].length; m++) {
                  attHolder['payLoad'] = '';
                  // var fi = fileResp['data'][m].file.split('/');
                  // var le = fi.length-1;
                  var ft = fileResp['data'][m].fileName.split('.');
                  // var ft = fi[le].split(".");
                  var mi = ft.length - 1;
                  // var ftype = ft[mi]
                  var ftype = ft[1]

                  // attHolder['file'] = fi[le];
                  attHolder['file'] = fileResp['data'][m].fileName;
                  attHolder['fileType'] = ftype;
                  attHolder['fileUrl']  = fileResp['data'][m].file;
                  attHolder['fileId']   = fileResp['data'][m].caseAttachmentId;
                  for (let n = 0; n < this.allAttachmentTypes.length; n++) {
                    if (this.allAttachmentTypes[n].attachmentTypeId == fileResp['data'][m].attachmentTypeId) {
                      attHolder['type'] = this.allAttachmentTypes[n].attachmentTypeName;
                    }

                  }
                  attData.push(attHolder);
                  attHolder = {}
                }

              }
            }
            else{
              this.attachmentTable              = [];
              this.selectattachmentTypeId       =null;
            }



            // this.attachmentTable = attData;
          })

          // if (resp['data'].caseStatusId != 1) {
          var loadResultsEndpoint = environment.API_COVID_ENDPOINT + 'showresult';
          var loadResultsRequest  = {...this.loadResultsRequest}
          loadResultsRequest.data.caseIds = [resp['data'].caseId]
          if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UC") != -1) {
            loadResultsRequest['header'].functionalityCode    = "SPCA-UC";
          }
          else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-VC") != -1){
            loadResultsRequest['header'].functionalityCode    = "SPCA-VC";
          }
          // loadResultsRequest['header'].functionalityCode    = "TWMA-MBC";
          // if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-UC") != -1) {
          //   loadResultsRequest['header'].functionalityCode    = "CLTA-UC";
          // }
          // else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-VC") != -1){
          //   loadResultsRequest['header'].functionalityCode    = "CLTA-VC";
          // }
          // else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-CD") != -1){
          //   loadResultsRequest['header'].functionalityCode    = "CLTA-CD";
          // }
          this.specimenService.loadInterpretation(loadResultsEndpoint,loadResultsRequest).subscribe(resultsResponse=>{
            console.log("resultsResponse",resultsResponse);
            var resultsResp = {...resultsResponse}

            if (resultsResp['data'] == null) {
              this.GLobalUniqueJoBOrderIdsForHistory = [];
              this.triggerHistoryTable = true;
              this.allHistoryDataCount = 0;
              this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
                dtElement.dtInstance.then((dtInstance: any) => {
                  if (dtInstance.table().node().id == 'rep-history' ) {
                    dtInstance.ajax.reload();

                  }
                })
              })
              return
            }
            else{
              var tempHold = {};
              var temp     = [];
              for (let i = 0; i < resultsResp['data'].length; i++) {
                for (let j = 0; j < this.covidTestLookup.length; j++) {
                  if (this.covidTestLookup[j]['testId'] == resultsResp['data'][i]['testId']) {
                    resultsResp['data'][i]['test'] = this.covidTestLookup[j]['name'];
                  }
                }
                for (let k = 0; k < this.testStatusLookup.length; k++) {
                  if (this.testStatusLookup[k]['caseStatusId'] == resultsResp['data'][i]['testStatusId']) {
                    resultsResp['data'][i]['status'] = this.testStatusLookup[k]['testStatusName'];
                  }
                }
                for (let l = 0; l < this.allInterpretations.length; l++) {
                  if (this.allInterpretations[l]['testInterpretationId'] == resultsResp['data'][i]['interpretationId']) {
                    resultsResp['data'][i]['result'] = this.allInterpretations[l]['value'];
                  }
                }
                // temp.push(tempHold);
                // tempHold = {}
              }
              this.resultsTableData =   resultsResp['data'];
              // if (resp['data'].caseStatusId > 6) {
              this.GLobalUniqueJoBOrderIdsForHistory = resultsResp['data'];
              this.triggerHistoryTable = true;
              // console.log('this.GLobalUniqueJoBOrderIdsForHistory',this.GLobalUniqueJoBOrderIdsForHistory);

              this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
                dtElement.dtInstance.then((dtInstance: any) => {
                  if (dtInstance.table().node().id == 'rep-history' ) {
                    dtInstance.ajax.reload();

                  }
                })
              })

              // }
              // console.log("----temp",  resultsResp);

            }
          },error=>{
            this.notifier.notify('error','Error while loading results')
          })
          // }


          var specData    = []
          var specTholder = {};
          var printTypeHolder = [];
          var max = 0;
          for (let i = 0; i < resp['data']['caseSpecimen'].length; i++) {
            /////////////// specimen Type
            // console.log("1111111-sepcimenType",resp['data']['caseSpecimen']);
            if (resp['data']['caseSpecimen'][i].caseSpecimenId > max) {
              /////////////////////to assign lates to print label button
              max = resp['data']['caseSpecimen'][i].caseSpecimenId;
              for (let j = 0; j < this.allSpecimenTypes.length; j++) {
                if (resp['data']['caseSpecimen'][i].specimenTypeId == this.allSpecimenTypes[j].specimenTypeId) {
                  specTholder['spType']        =  this.allSpecimenTypes[j]['specimenTypeName'];
                  specTholder['spTypeID']      =  this.allSpecimenTypes[j]['specimenTypeId'];
                  printTypeHolder = this.allSpecimenTypes[j]['specimenTypePrefix'];
                  // printTypeHolder.push(this.allSpecimenTypes[j]['specimenTypePrefix'])
                }

              }
            }
            else{
              for (let j = 0; j < this.allSpecimenTypes.length; j++) {
                if (resp['data']['caseSpecimen'][i].specimenTypeId == this.allSpecimenTypes[j].specimenTypeId) {
                  specTholder['spType']        =  this.allSpecimenTypes[j]['specimenTypeName'];
                  specTholder['spTypeID']      =  this.allSpecimenTypes[j]['specimenTypeId'];
                }

              }
            }


            resp['data']['caseSpecimenTypePRE']=printTypeHolder;
            console.log("resp['data']['caseSpecimenTypePRE']",resp['data']['caseSpecimenTypePRE']);



            /////////////// specimen Source
            for (let j = 0; j < this.allSpecimenSource.length; j++) {
              if (resp['data']['caseSpecimen'][i].specimenSourceId == this.allSpecimenSource[j].specimenSourceId) {
                specTholder['spSource']        =  this.allSpecimenSource[j]['specimenSourceName'];
                specTholder['spSourceID']      =  this.allSpecimenSource[j]['specimenSourceId'];
              }

            }
            /////////////// specimen BodySite
            for (let j = 0; j < this.allSBodySite.length; j++) {
              if (resp['data']['caseSpecimen'][i].bodySiteId == this.allSBodySite[j].bodySiteId) {
                specTholder['spBody']        =  this.allSBodySite[j]['bodySiteName'];
                specTholder['spBodyID']      =  this.allSBodySite[j]['bodySiteId'];
              }

            }
            /////////////// specimen Procedure
            for (let j = 0; j < this.allProcedures.length; j++) {
              if (resp['data']['caseSpecimen'][i].procedureId == this.allProcedures[j].procedureId) {
                specTholder['spProcedure']        =  this.allProcedures[j]['procedureName'];
                specTholder['spProcedureID']      =  this.allProcedures[j]['procedureId'];
              }
              specTholder['caseSpecimenId'] =  resp['data']['caseSpecimen'][i]['caseSpecimenId']

            }

            specData.push(specTholder);
            specTholder = [];

          }
          this.specimenTable = specData;
          console.log('this.specimenTable',this.specimenTable);
          // var attData   = []
          // var attHolder = {}
          // this.caseAttOldLength = resp['data']['caseAttachments'].length;
          if (attData.length > 0) {
            if (resp['data']['caseAttachments'].length > 0) {
              // console.log("attData",attData);

              for (let k = 0; k < resp['data']['caseAttachments'].length; k++) {
                for (let l = 0; l < this.allAttachmentTypes.length; l++) {
                  if (resp['data']['caseAttachments'][k].attachmentTypeId == this.allAttachmentTypes[l].attachmentTypeId) {
                    attData[k]['attachId'] = resp['data']['caseAttachments'][k].attachmentTypeId;
                    attData[k]['type']     = this.allAttachmentTypes[l].attachmentTypeName;
                  }
                }

                attData[k]['payLoad'] = '';

              }

            }
          }
          this.attachmentTable = attData;
          resp['data'].collectedBy = resp['data'].collectedBy.toString();
          resp['data'].caseCategoryPrefix = "NH";
          for (let i = 0; i < this.allCaseCategories.length; i++) {
            // console.log('resp[].caseCategoryId',resp['data'].caseCategoryId);
            // console.log('this.allCaseCategories[i].caseCategoryId',this.allCaseCategories[i].caseCategoryId);

            if (resp['data'].caseCategoryId == this.allCaseCategories[i].caseCategoryId) {
              this.selectedCaseCat = this.allCaseCategories[i]
            }
          }
          console.log('00000-this.allStatusType',this.allStatusType);

          for (let j = 0; j < this.allStatusType.length; j++) {
            // console.log("0000-resp['data'].caseStatusId",resp['data'].caseStatusId);
            // console.log("0000-this.allStatusType[j].caseStatusId",this.allStatusType[j].caseStatusId);
            if (resp['data'].caseStatusId == this.allStatusType[j].caseStatusId) {
              resp['data'].selectedCaseStatusName = this.allStatusType[j].caseStatusName;
              // console.log("0000-In selectedCaseStatusName",resp['data'].selectedCaseStatusName);
              break;
            }
          }
          // console.log("resp['data'].caseCategoryId",resp['data'].caseStatusId);

          if (resp['data'].caseStatusId == 11) {
            this.disableAll = true;
            this.caseForm.disable();
          }
          else{
            this.disableAll = false;
          }

          //////// accessionby
          var usersRequest = {
            header              : {
              uuid              : "",
              partnerCode       : "",
              userCode          : "",
              referenceNumber   : "",
              systemCode        : "",
              moduleCode        : "",
              functionalityCode : "",
              systemHostAddress : "",
              remoteUserAddress : "",
              dateTime          : ""
            },
            data                : {
              userCodes           :[]
            }
          }
          usersRequest.data.userCodes = [resp['data'].createdBy];
          var userUrl = environment.API_USER_ENDPOINT + "clientusers";
          usersRequest.header.partnerCode  = this.logedInUserRoles.partnerCode;
          usersRequest.header.userCode     = this.logedInUserRoles.userCode;
          usersRequest.header.functionalityCode     = "CLTA-VC";
          // if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-UC") != -1) {
          //   usersRequest['header'].functionalityCode    = "CLTA-UC";
          // }
          // else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-VC") != -1){
          //   usersRequest['header'].functionalityCode    = "CLTA-VC";
          // }
          // else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-CD") != -1){
          //   usersRequest['header'].functionalityCode    = "CLTA-CD";
          // }
          this.specimenService.getUserRecord(userUrl,usersRequest).then(getUserRespAccesion =>{

            if (getUserRespAccesion['data'][0].userCode == resp['data'].createdBy) {
              resp['data'].accessionedByName = getUserRespAccesion['data'][0].fullName;
            }



          })


          forkJoin([getClient, getPhysician, getPatient]).subscribe(allresults => {

            var client    = allresults[0];
            var physician = allresults[1];
            var patient   = allresults[2];

            console.log("client",client);
            this.allClientName = client['data'];
            this.clientSearchString = client['data'][0];
            this.clientNameToPrint  = client['data'][0].name;

            console.log("physician",physician);
            //
            if (physician['data']['length'] == 0) {
              this.selectedAttPhysician = 1224;
              this.saveRequest.data.attendingPhysicianId = '1224';

            }
            else{
              physician['data'][0]['name'] = physician['data'][0]['fullName'];
              this.selectedAttPhysician = physician['data'][0];
              this.allAtdPhysicians = physician['data'];
            }

            console.log("patient",patient);
            if (patient['data'] == null) {
              alert("No Patient Found please fill in the patient Data manually")
            }
            else{
              this.patFName  = this.encryptDecrypt.decryptString(patient['data'].patientDemographics.firstName);
              this.patLName  = this.encryptDecrypt.decryptString(patient['data'].patientDemographics.lastName);
              var d2 = new Date(patient['data'].patientDemographics.dateOfBirth)
              var year = 1999;
              if (d2.getFullYear()< 1111) {
                year = 1999
              }
              else{
                year = d2.getFullYear()
              }

              var dob = year+"-"+this.zeroPadded(d2.getMonth() + 1)+"-"+this.zeroPadded(d2.getDate())
              this.patDob    = dob;
              this.patGender = patient['data'].patientDemographics.gender;
              if (patient['data'].patientDemographics.gender != 'm' && patient['data'].patientDemographics.gender != 'f' && patient['data'].patientDemographics.gender != 'u') {
                this.patGender = 'u';
              }
              else{
                this.patGender = patient['data'].patientDemographics.gender;
              }
              this.patType   = patient['data'].patientDemographics.patientTypeId;
              if (typeof patient['data'].patientDemographics.ssn != "undefined") {
                this.patSSN    = this.encryptDecrypt.decryptString(patient['data'].patientDemographics.ssn);
              }
              else{
                this.patSSN    = "";
              }
            }
            this.selectedCase      = resp['data'];
            console.log("this.selectedCase",this.selectedCase);

            this.saveRequest.data  = resp['data'];
            this.selectedPatientID = resp['data'].patientId;
            if (resp['data'].caseStatusId != 1) {
              this.caseForm.get('caseCategory').disable();
            }

            if (resp['data'].caseStatusId > 3) {
              this.caseForm.get('intakeid').disable();
            }

            if (resp['data']['intakeId'] != null) {
              this.caseForm.get('clientName').disable();
            }

            this.ngxLoader.stop();
            resolve();

          })


          // return true;

        })
        .catch(error => {
          // return false;
          // console.log("error: ",error);
          // this.notifier.notify( "error", "Error while loading case number search results")
          reject();
        });
        // resolve();
      });
    }
    // checkatt(){
    //   console.log("selectedAttPhysician",this.selectedAttPhysician);
    //
    // }

    get f() { return this.caseForm.controls; }
    saveCase(saveType){
      if (saveType == 0 || this.attachmentTable.length > 0) {
        this.caseForm.get('attType').clearValidators();
        this.caseForm.controls["attType"].updateValueAndValidity();
        this.caseForm.get('attFile').clearValidators();
        this.caseForm.controls["attFile"].updateValueAndValidity();
      }
      else{
        if (this.pageType == 'edit') {
          if (saveType != 0) {
            this.caseForm.get('attType').setValidators([Validators.required, Validators.maxLength(50)])
            this.caseForm.controls["attType"].updateValueAndValidity();
            this.caseForm.get('attFile').setValidators([Validators.required, Validators.maxLength(50)])
            this.caseForm.controls["attFile"].updateValueAndValidity();
          }
          else{
            this.caseForm.get('attType').clearValidators();
            this.caseForm.controls["attType"].updateValueAndValidity();
            this.caseForm.get('attFile').clearValidators();
            this.caseForm.controls["attFile"].updateValueAndValidity();
          }

        }
        else{
          this.caseForm.get('attType').setValidators([Validators.required, Validators.maxLength(50)])
          this.caseForm.controls["attType"].updateValueAndValidity();
          this.caseForm.get('attFile').setValidators([Validators.required, Validators.maxLength(50)])
          this.caseForm.controls["attFile"].updateValueAndValidity();
        }

      }
      $('#submitModal').modal('hide');
      // console.log("saveType", saveType);
      this.submitted                  = true;
      if (this.caseForm.invalid) {
        // alert('form invalid if error is not visible please check attachment tab');
        // alert('form invalid');
        // this.submitted = false;
        this.scrollToError();
        return;
      }
      else if (this.specimenTable.length <= 0) {
        alert('Please add atleast one specimen type');
        this.ngxLoader.stop();
        return;
      }


      if (this.pageType == 'add') {
        this.saveRequest.header.functionalityCode = "SPCA-CD"
        // if (saveType == 0) {
        //   this.saveRequest.data.caseStatusId = 1;
        //   this.saveRequest.header.functionalityCode = "SPCA-SD"
        // }
        // else if(saveType == 1){
        //   this.saveRequest.data.caseStatusId = 3;
        //   this.saveRequest.header.functionalityCode = "SPCA-SN"
        // }
        // else if(saveType == 2){
        //   this.saveRequest.data.caseStatusId = 3;
        //   this.saveRequest.header.functionalityCode = "SPCA-SP"
        // }
        console.log("this.saveRequest",this.saveRequest);
        console.log('this.selectedPatientID',this.selectedPatientID);

        // this.ngxLoader.stop();
        // return

        if (this.selectedPatientID != null ) {
          this.saveMaxCase()
        }
        else{
          this.SavePatient().then(savePatientResp => {
            // console.log("savePatientResp",savePatientResp);

            if (savePatientResp == true) {
              this.saveMaxCase()
            }
          })
        }

      }
      else{
        this.saveRequest.header.functionalityCode = "SPCA-UC"
        if (this.saveTypeGlobal == 1) {
          this.saveRequest.data.caseStatusId = 3;

        }
        if (this.pateientChanged == true) {
          this.updatePatient().then(savePatientResp => {
            // console.log("savePatientResp",savePatientResp);

            if (savePatientResp == true) {
              this.saveMaxCase();
            }
          })
        }
        else{
          this.saveMaxCase();
        }

        // this.saveMaxCase()
        // console.log("originalPatient",this.originalPatient);
        // console.log("this.ageToSaveinDB",this.ageToSaveinDB);

        // if (this.originalPatient.data.clientId != this.saveRequest.data.clientId
        //    || this.originalPatient.data.patientDemographics.firstName != this.patFName || this.originalPatient.data.patientDemographics.lastName != this.patLName
        //    || this.originalPatient.data.patientDemographics.gender != this.patGender || typeof this.ageToSaveinDB != 'undefined'
        //    || this.originalPatient.data.patientDemographics.patientTypeId != this.patType
        //    || this.patSSN != null) {
        //      this.updatePatient().then(savePatientResp => {
        //        // console.log("savePatientResp",savePatientResp);
        //
        //        if (savePatientResp == true) {
        //          this.saveMaxCase()
        //        }
        //      })
        //    }
        // // return;
        // // this.saveMaxCase()
      }


    }

    saveMaxCase(){
      this.ngxLoader.start();
      var myDate = new Date();
      var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyy');
      var currentTimeStamp = this.datePipe.transform(myDate, 'MM-dd-yyy HH:mm:ss');
      if (this.patientRecordUpdateafterCancel == false || this.selectedPatientID == null) {
        this.notifier.notify( "error", "please change pateint first name, last name or date of birth another patient exist with same record." );
        // alert('please change pateint first name, last name or date of birth another patient exist with same record.')
        this.ngxLoader.stop();
        return;
      }
      this.saveRequest.data.patientId = this.selectedPatientID;
      // if (this.pageType == 'add') {
      //   this.saveRequest.data.patientId = this.selectedPatientID;
      // }
      // else if (this.pageType == 'edit') {
      //   // this.saveRequest.data.patientId = this.selectedPatientID;
      //   // console.log("this.saveRequest",this.saveRequest);
      //   // console.log("this.specimenTable",this.specimenTable);
      //   // return;
      //
      // }

      /////// Specimen Info Set
      var specreqHolder = [];
      var specFormat    = {};
      console.log("this.specimenTable",this.specimenTable);

      for (let i = 0; i < this.specimenTable.length; i++) {
        if (this.pageType == 'add') {
          specFormat["bodySiteId"]      = this.specimenTable[i]['spBodyID']
          specFormat["caseSpecimenId"]  = null;
          specFormat["createdBy"]       = this.logedInUserRoles.userCode;
          specFormat["createdTimestamp"]= currentTimeStamp;
          specFormat["procedureId"]     = this.specimenTable[i]['spProcedureID']
          specFormat["specimenTypeId"]  = this.specimenTable[i]['spTypeID']
          specFormat["specimenSourceId"]= this.specimenTable[i]['spSourceID']
          specFormat["updatedBy"]       = ""
          specFormat["updatedTimestamp"]= ""
          specreqHolder.push(specFormat);
          specFormat    = {}
        }
        else{
          // console.log("this.specimenTable[i]['caseSpecimenId']",this.specimenTable[i]['caseSpecimenId']);

          specFormat["bodySiteId"]      = this.specimenTable[i]['spBodyID'];
          specFormat["caseSpecimenId"]  = this.specimenTable[i]['caseSpecimenId'];
          if (this.specimenTable[i]['caseSpecimenId'] == null) {
            specFormat["createdBy"]       = this.logedInUserRoles.userCode;
            specFormat["createdTimestamp"]= currentTimeStamp;
          }
          else{
            specFormat["createdBy"]       = this.selectedCase.createdBy;
            specFormat["createdTimestamp"]= this.selectedCase.createdTimestamp;
          }

          specFormat["procedureId"]     = this.specimenTable[i]['spProcedureID']
          specFormat["specimenTypeId"]  = this.specimenTable[i]['spTypeID']
          specFormat["specimenSourceId"]= this.specimenTable[i]['spSourceID']
          specFormat["updatedBy"]       = this.logedInUserRoles.userCode;
          specFormat["updatedTimestamp"]= currentTimeStamp
          specreqHolder.push(specFormat);
          specFormat    = {}
        }
      }
      // console.log("---specreqHolder------",specreqHolder);
      // this.ngxLoader.stop();
      // return;

      this.saveRequest.data.caseSpecimen = specreqHolder;
      this.saveRequest.data.caseAttachments = [];
      if (typeof this.selectedAttPhysician.userCode == 'undefined') {

      }
      else{
        this.saveRequest.data.attendingPhysicianId = this.selectedAttPhysician.userCode;
      }
      // this.saveRequest.data.attendingPhysicianId = this.selectedAttPhysician.userCode;
      // console.log("this.specimenTable",this.specimenTable);
      if (this.pageType == 'add') {
        this.saveRequest.data.accessionTimestamp = this.datePipe.transform(this.recievingDateToShow, 'MM-dd-yyy HH:mm:ss');
        // this.saveRequest.data.accessionTimestamp = currentTimeStamp;
        this.saveRequest.data.createdTimestamp = currentTimeStamp;
        this.saveRequest.data.createdBy = this.logedInUserRoles.userCode;
      }
      else{
        if (this.selectedCase.caseStatusId == 1 && this.saveTypeGlobal != 0) {
          this.saveRequest.data.accessionTimestamp = this.datePipe.transform(this.recievingDateToShow, 'MM-dd-yyy HH:mm:ss');
          // this.saveRequest.data.accessionTimestamp = currentTimeStamp;
        }

        if (this.selectedCase['caseStatusId'] == 1 && this.saveTypeGlobal == 3) {
          this.saveRequest.data.caseStatusId = 3;
        }
      }


      this.saveRequest.data.collectionDate = this.datePipe.transform(this.collectionDateToShow, 'MM-dd-yyy HH:mm:ss');
      this.saveRequest.data.receivingDate = this.datePipe.transform(this.recievingDateToShow, 'MM-dd-yyy HH:mm:ss');
      this.saveRequest.data.sendDate = this.datePipe.transform(this.sendDateToShow, 'MM-dd-yyy HH:mm:ss');
      if (this.saveTypeGlobal == 0) {
        this.saveRequest.data.intakeId = null;
      }

      this.saveRequest.data.caseCategoryId = this.selectedCaseCat.caseCategoryId;
      this.saveRequest.data.caseCategoryPrefix = this.selectedCaseCat.caseCategoryPrefix;
      this.saveRequest.header.partnerCode = this.logedInUserRoles.partnerCode;
      this.saveRequest.header.userCode    = this.logedInUserRoles.userCode;
      // this.saveRequest.header.functionalityCode    = "SPCA-CD";
      console.log("this.saveRequest",this.saveRequest);
      console.log("this.saveTypeGlobal",this.saveTypeGlobal);
      console.log("this.DeletecaseSpecimenId",this.DeletecaseSpecimenId);
      // this.ngxLoader.stop();return;
      var saveminUrl = environment.API_SPECIMEN_ENDPOINT + "maxaccession";
      this.specimenService.searchMinAccession(saveminUrl,this.saveRequest).subscribe(saveResp =>{
        this.submitted  = false;
        // this.caseForm.reset();
        // this.ngxLoader.stop();
        console.log("saveResp",saveResp);
        if (saveResp['result']['codeType'] != 'S') {
          this.ngxLoader.stop();
          $('.modal').modal('hide')
          this.notifier.notify( "error", "Error while updating Case");

          return;
        }
        var caseNumforFIles = saveResp['data'].caseNumber;
        var caseIdforFiles  = saveResp['data'].caseId ;
        // this.triageCase(saveResp['data'].caseId);
        console.log('this.pageType',this.pageType);

        if (this.pageType == 'edit') {
          console.log("-------------this.DeletecaseSpecimenId",this.DeletecaseSpecimenId);

          if (this.DeletecaseSpecimenId != null) {
            this.deletSPecimen();
          }
          if (this.saveTypeGlobal == 3) {
            if (this.caseAttOldLength == this.attachmentTable.length) {
              this.ngxLoader.stop()
              this.notifier.notify( "success", "Case updated successfully ");
              if (this.selectedCase.caseStatusId == 1) {
                this.triageCase(saveResp['data'].caseId);
              }
              if (this.caseFromIntake == null) {
                $('.modal').modal('hide')
                // this.router.navigateByUrl('/manage-cases');
                var fromBatch = JSON.parse(localStorage.getItem('caseFromEditBatch'));

                if (fromBatch != null) {
                  localStorage.removeItem('caseFromEditBatch');
                  var a = fromBatch;

                  // var ciphertext = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
                  var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
                  var bx = ax.replace('+','xsarm');
                  var cx = bx.replace('/','ydan');
                  var ciphertext = cx.replace('=','zhar');
                  console.log('ciphertext',ciphertext);
                  this.router.navigate(['edit-batch', ciphertext]);
                }
                else{
                  localStorage.removeItem('caseFromEditBatch');
                  this.router.navigateByUrl('/manage-cases');
                }
              }
              else{
                this.sharedIntakeData.loadCasesInIntake(this.oldIntakeId );
                $('.modal').modal('hide')
              }
              return;

            }
            else{
              this.saveAttachments(caseNumforFIles,caseIdforFiles,this.saveRequest.data.caseCategoryPrefix).then(res => {
                this.notifier.notify( "success", "Case Updated successfully" );
                this.ngxLoader.stop()
                // this.notifier.notify( "success", "Case updated successfully ");
                if (this.selectedCase.caseStatusId == 1) {
                  this.triageCase(saveResp['data'].caseId);
                }
                if (this.caseFromIntake == null) {
                  $('.modal').modal('hide')
                  // this.router.navigateByUrl('/manage-cases');
                  var fromBatch = JSON.parse(localStorage.getItem('caseFromEditBatch'));

                  if (fromBatch != null) {
                    localStorage.removeItem('caseFromEditBatch');
                    var a = fromBatch;

                    // var ciphertext = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
                    var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
                    var bx = ax.replace('+','xsarm');
                    var cx = bx.replace('/','ydan');
                    var ciphertext = cx.replace('=','zhar');
                    console.log('ciphertext',ciphertext);
                    this.router.navigate(['edit-batch', ciphertext]);
                  }
                  else{
                    localStorage.removeItem('caseFromEditBatch');
                    this.router.navigateByUrl('/manage-cases');
                  }
                }
                else{
                  this.sharedIntakeData.loadCasesInIntake(this.oldIntakeId );
                  $('.modal').modal('hide')
                }
                return;
              });
            }
            return;
            // this.ngxLoader.stop()
            // this.notifier.notify( "success", "Case updated successfully ");

          }
          if (this.saveTypeGlobal == 1) {
            if (this.caseAttOldLength == this.attachmentTable.length) {
              this.ngxLoader.stop()
              this.notifier.notify( "success", "Case updated successfully ");
              if (this.selectedCase.caseStatusId == 1) {
                this.triageCase(saveResp['data'].caseId);
              }

            }
            else{
              this.saveAttachments(caseNumforFIles,caseIdforFiles,this.saveRequest.data.caseCategoryPrefix).then(res => {
                this.notifier.notify( "success", "Case Updated successfully" );
                this.ngxLoader.stop()
                this.notifier.notify( "success", "Case updated successfully ");
                if (this.selectedCase.caseStatusId == 1) {
                  this.triageCase(saveResp['data'].caseId);
                }
              });
            }

            // if (this.caseFromIntake == null) {
            //   $('.modal').modal('hide')
            //   this.router.navigateByUrl('/manage-cases');
            // }
            // else{
            //   this.sharedIntakeData.loadCasesInIntake(this.oldIntakeId );
            //   $('.modal').modal('hide')
            // }
            return;
          }
          // console.log("this.caseAttOldLength",this.caseAttOldLength);
          // console.log("this.this.attachmentTable.length",this.attachmentTable.length);
          this.lNamePrint      = this.patLName;
          this.fNamePrint      = this.patFName
          this.patientDOBPrint = this.patDob;
          this.selectedPatientID = saveResp['data'].patientId;



          if (this.caseAttOldLength == this.attachmentTable.length) {
            // console.log("--------------------");
            this.notifier.notify( "success", "Case updated successfully ");
            if (this.saveTypeGlobal == 2) {
              // console.log("--------------------");
              // var specTholder = {};
              var printTypeHolder = [];
              for (let j = 0; j < this.allSpecimenTypes.length; j++) {
                for (let k = 0; k < saveResp['data'].specimenTypeId.length; k++) {
                  if (saveResp['data'].specimenTypeId[k]== this.allSpecimenTypes[j].specimenTypeId) {
                    printTypeHolder.push(this.allSpecimenTypes[j]['specimenTypePrefix'])
                  }

                }


              }


              // console.log("***************************q****************",this.caseFromIntake);
              this.printLabel(saveResp['data'].caseNumber,this.lNamePrint,this.fNamePrint,this.patientDOBPrint, this.clientNameToPrint, printTypeHolder,saveResp['data'].clientId)
              // this.printLabel(saveResp['data'].caseNumber,this.lNamePrint,this.fNamePrint,this.patientDOBPrint, this.clientNameToPrint, saveResp['data'].specimenTypeId)

              if (this.caseFromIntake == null) {
                $('.modal').modal('hide')
                // this.router.navigateByUrl('/manage-cases');
                var fromBatch = JSON.parse(localStorage.getItem('caseFromEditBatch'));

                if (fromBatch != null) {
                  localStorage.removeItem('caseFromEditBatch');
                  var a = fromBatch;

                  // var ciphertext = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
                  var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
                  var bx = ax.replace('+','xsarm');
                  var cx = bx.replace('/','ydan');
                  var ciphertext = cx.replace('=','zhar');
                  console.log('ciphertext',ciphertext);
                  this.router.navigate(['edit-batch', ciphertext]);
                }
                else{
                  localStorage.removeItem('caseFromEditBatch');
                  this.router.navigateByUrl('/manage-cases');
                }
              }
              else{
                this.sharedIntakeData.loadCasesInIntake(this.oldIntakeId );
                $('.modal').modal('hide')
              }

              ///// print and redirect
              // this.submitted  = false;
              // this.caseForm.reset();
            }
            else{
              if (this.saveTypeGlobal == 2) {
                // this.resetPatient();
                // this.resetFields();
                if (this.caseFromIntake == null) {
                  $('.modal').modal('hide')
                  var data = {fromEdit:'create',id:null}
                  // this.router.navigateByUrl('/manage-cases', { state: data });
                  var fromBatch = JSON.parse(localStorage.getItem('caseFromEditBatch'));

                  if (fromBatch != null) {
                    localStorage.removeItem('caseFromEditBatch');
                    var a = fromBatch;

                    // var ciphertext = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
                    var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
                    var bx = ax.replace('+','xsarm');
                    var cx = bx.replace('/','ydan');
                    var ciphertext = cx.replace('=','zhar');
                    console.log('ciphertext',ciphertext);
                    this.router.navigate(['edit-batch', ciphertext]);
                  }
                  else{
                    localStorage.removeItem('caseFromEditBatch');
                    this.router.navigateByUrl('/manage-cases', { state: data });
                  }
                }
                else{
                  this.sharedIntakeData.loadCasesInIntake(this.oldIntakeId );
                  $('.modal').modal('hide')
                }

              }
              else if(this.saveTypeGlobal == 0){
                let columnCaseReq  = {}
                var selectURL = environment.API_SPECIMEN_ENDPOINT + 'searchbycaseid';
                columnCaseReq      = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-CD",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{caseId:saveResp['data'].caseId}}
                if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UC") != -1) {
                  columnCaseReq['header'].functionalityCode    = "SPCA-UC";
                }
                else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-VC") != -1){
                  columnCaseReq['header'].functionalityCode    = "SPCA-VC";
                }
                else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-CD") != -1){
                  columnCaseReq['header'].functionalityCode    = "SPCA-CD";
                }
                this.getSelectedRecord(selectURL,columnCaseReq).then(selectedCase => {
                  // console.log("Selected Case response",selectedCase);
                  // this.chec();
                  // console.log("selectedCase",this.selectedCase);
                  console.log("saveRequest",this.saveRequest);
                  // this.ngxLoader.stop();
                  this.changespecimenType()


                });
              }
              // this.caseForm.reset();
            }
          }
          else{
            // console.log("//////////////");
            this.saveAttachments(caseNumforFIles,caseIdforFiles,this.saveRequest.data.caseCategoryPrefix).then(res => {



              this.notifier.notify( "success", "Case Updated successfully" );

              if (this.saveTypeGlobal == 2) {
                var printTypeHolder = [];
                for (let j = 0; j < this.allSpecimenTypes.length; j++) {
                  for (let k = 0; k < saveResp['data'].specimenTypeId.length; k++) {
                    if (saveResp['data'].specimenTypeId[k]== this.allSpecimenTypes[j].specimenTypeId) {
                      printTypeHolder.push(this.allSpecimenTypes[j]['specimenTypePrefix'])
                    }

                  }


                }
                // console.log("******************************************* 2222222");
                this.printLabel(saveResp['data'].caseNumber,this.lNamePrint,this.fNamePrint,this.patientDOBPrint, this.clientNameToPrint, printTypeHolder,saveResp['data'].clientId)

                if (this.caseFromIntake == null) {
                  $('.modal').modal('hide')
                  // this.router.navigateByUrl('/manage-cases');
                  var fromBatch = JSON.parse(localStorage.getItem('caseFromEditBatch'));

                  if (fromBatch != null) {
                    localStorage.removeItem('caseFromEditBatch');
                    var a = fromBatch;

                    // var ciphertext = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
                    var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
                    var bx = ax.replace('+','xsarm');
                    var cx = bx.replace('/','ydan');
                    var ciphertext = cx.replace('=','zhar');
                    console.log('ciphertext',ciphertext);
                    this.router.navigate(['edit-batch', ciphertext]);
                  }
                  else{
                    localStorage.removeItem('caseFromEditBatch');
                    this.router.navigateByUrl('/manage-cases');
                  }
                }
                else{
                  this.sharedIntakeData.loadCasesInIntake(this.oldIntakeId );
                  $('.modal').modal('hide')
                }


                // this.printLabel(saveResp['data'].caseNumber,this.lNamePrint,this.fNamePrint,this.patientDOBPrint, this.clientNameToPrint, saveResp['data'].specimenTypeId)
                ///// print and redirect
                // this.submitted  = false;
                // this.caseForm.reset();
              }
              else{
                if (this.saveTypeGlobal == 1) {
                  // this.resetPatient();
                  // this.resetFields();
                  // var data = {fromCreate:'create',id:null}
                  // this.router.navigateByUrl('/manage-cases', { state: data });
                }
                else if(this.saveTypeGlobal == 0){
                  let columnCaseReq  = {}
                  var selectURL = environment.API_SPECIMEN_ENDPOINT + 'searchbycaseid';
                  columnCaseReq      = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-CD",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{caseId:saveResp['data'].caseId}}
                  if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UC") != -1) {
                    columnCaseReq['header'].functionalityCode    = "SPCA-UC";
                  }
                  else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-VC") != -1){
                    columnCaseReq['header'].functionalityCode    = "SPCA-VC";
                  }
                  else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-CD") != -1){
                    columnCaseReq['header'].functionalityCode    = "SPCA-CD";
                  }
                  this.getSelectedRecord(selectURL,columnCaseReq).then(selectedCase => {
                    // console.log("Selected Case response",selectedCase);
                    // this.chec();
                    // console.log("selectedCase",this.selectedCase);
                    console.log("saveRequest",this.saveRequest);
                    // this.ngxLoader.stop();
                    this.changespecimenType()


                  });
                }
                // this.caseForm.reset();
              }

            });
          }



        }
        else{
          if (this.caseFromIntake == null) {
            this.triageCase(saveResp['data'].caseId);

          }
          if (this.attachmentTable.length<=0) {
            if (this.saveTypeGlobal == 1) {
              this.caseForm.controls['attType'].reset();
              this.caseForm.controls['attFile'].reset();
              $('#upload-att').val('');
              this.attachmentTable = [];
              this.resetPatient();
              // this.resetFields();
              // var data = {fromEdit:'create',id:null}
              // this.router.navigateByUrl('/manage-cases', { state: data });
            }
            else{
              var data1 = {fromCreate:'create',id:null}
              this.router.navigateByUrl('/manage-cases', { state: data1 });
            }

          }
          else{
            this.saveAttachments(caseNumforFIles,caseIdforFiles,this.saveRequest.data.caseCategoryPrefix).then(res => {
              if (this.saveTypeGlobal == 0) {
                this.notifier.notify( "success", "Case saved successfully But it is not assigned to intake as Case is save as Draft.");
              }
              else{
                this.notifier.notify( "success", "Case saved successfully");
              }
              // this.notifier.notify( "success", "Case saved successfully" );

              if (this.saveTypeGlobal == 2) {
                var printTypeHolder = [];
                for (let j = 0; j < this.allSpecimenTypes.length; j++) {
                  for (let k = 0; k < saveResp['data'].specimenTypeId.length; k++) {
                    if (saveResp['data'].specimenTypeId[k]== this.allSpecimenTypes[j].specimenTypeId) {
                      printTypeHolder.push(this.allSpecimenTypes[j]['specimenTypePrefix'])
                    }

                  }


                }

                this.printLabel(saveResp['data'].caseNumber,this.lNamePrint,this.fNamePrint,this.patientDOBPrint, this.clientNameToPrint,printTypeHolder,saveResp['data'].clientId)
                // this.printLabel(saveResp['data'].caseNumber,this.lNamePrint,this.fNamePrint,this.patientDOBPrint, this.clientNameToPrint,saveResp['data'].specimenTypeId)
                ///// print and redirect
                // this.submitted  = false;
                if (this.caseFromIntake == null) {
                  $('.modal').modal('hide')
                  this.router.navigateByUrl('/manage-cases');
                }
                else{
                  this.sharedIntakeData.loadCasesInIntake(this.oldIntakeId );
                  $('.modal').modal('hide')
                }

              }
              else if (this.saveTypeGlobal == 1) {
                this.resetPatient();
                this.resetFields();
                // var data = {fromEdit:'create',id:null}
                // this.router.navigateByUrl('/manage-cases', { state: data });
              }
              else{
                this.caseForm.reset();
                var data = {fromCreate:'create',id:null}
                this.router.navigateByUrl('/manage-cases', { state: data });
              }

            });
          }

        }

      },
      error=>{
        this.notifier.notify( "error", "Error while saving case backend exception" );
        this.ngxLoader.stop();
      })
    }
    saveAttachments(caseNumber,caseIdforFiles,caseCategoryPrefix){
      // if (caseNumber == null) {
      //   caseNumber = caseIdforFiles;
      // }
      return new Promise((resolve, reject) => {
        var saveFileUrl = environment.API_SPECIMEN_ENDPOINT + "uploadfile";
        var saveFileReq = {
          header:{
            uuid:"",
            partnerCode:"",
            userCode:"",
            referenceNumber:"",
            systemCode:"",
            moduleCode:"SPCM",
            functionalityCode:"SPCA-MC",
            systemHostAddress:"",
            remoteUserAddress:"",
            dateTime:""
          },
          data:{
            caseNumber:"",
          }
        }
        saveFileReq['header'].partnerCode = this.logedInUserRoles.partnerCode;
        saveFileReq['header'].userCode = this.logedInUserRoles.userCode;
        saveFileReq['header'].functionalityCode = "SPCA-MC";
        // if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UC") != -1) {
        //   saveFileReq['header'].functionalityCode    = "SPCA-UC";
        // }
        // else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-VC") != -1){
        //   saveFileReq['header'].functionalityCode    = "SPCA-VC";
        // }
        // else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-CD") != -1){
        //   saveFileReq['header'].functionalityCode    = "SPCA-CD";
        // }

        var saveMaxBody    = [];
        var saveBodyHolder = {}
        var loopCount = 0;
        console.log('***********this.attachmentTable',this.attachmentTable);

        for (let i = 0; i < this.attachmentTable.length; i++) {

          if (this.attachmentTable[i]['payLoad'] == ''){
            ////// no need to upload
            if (i == (this.attachmentTable.length -1)) {
              resolve();
            }
            loopCount ++;


          }else{
            saveFileReq.data.caseNumber = caseIdforFiles;
            var myDate = new Date();
            var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyy');
            var currentTimeStamp = this.datePipe.transform(myDate, 'MM-dd-yyy HH:mm:ss');
            // saveFileReq.data.caseAttachments['caseAttachmentId'] = this.attachmentTable[i]['attachId'];
            let formRequestData = new FormData();
            formRequestData.append('file', this.attachmentTable[i]['payLoad']);
            var reqString = JSON.stringify(saveFileReq);
            formRequestData.append('specimentRequest', reqString);

            this.specimenService.uploadFiles(saveFileUrl,formRequestData).subscribe(fileResp =>{
              console.log("*fileResp",fileResp);

              if (fileResp['data'] != null) {
                saveBodyHolder['caseId']           = caseIdforFiles;
                saveBodyHolder['attachmentTypeId'] = this.attachmentTable[i]['attachId'];
                saveBodyHolder['file']         = fileResp['data']['fileName'];
                saveBodyHolder['createdBy']        = this.logedInUserRoles.userCode;
                saveBodyHolder['createdTimestamp'] = currentTimeStamp;
                saveMaxBody.push(saveBodyHolder);
                saveBodyHolder = {};
                // loopCount = loopCount + 1;

              }
              loopCount ++;
              if(loopCount == this.attachmentTable.length){

                ////// call save attachment end pointtriageRequest.header.functionalityCode = "SPCA-CD";
                var saveAttReqBody={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-CD",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{caseAttachments:saveMaxBody,caseCategoryPrefix:caseCategoryPrefix}}
                var saveAttUrl    = environment.API_SPECIMEN_ENDPOINT + "saveattachment";
                this.specimenService.saveAttachments(saveAttUrl, saveAttReqBody).subscribe(attResp =>{
                  console.log("attResp",attResp);
                  resolve(true);
                  // this.ngxLoader.stop();
                },
                error=>{
                  this.ngxLoader.stop();
                  this.notifier.notify('error',"error while saving files")
                })

              }
            },
            error=>{
              this.ngxLoader.stop();
              this.notifier.notify('error',"error while saving files")
            })


          }


        }



        // console.log('f1');
        // resolve();
      });
    }

    toggleInsurance(){
      if (this.insuranceCompany == null || this.insuranceCompany == "" || typeof this.insuranceCompany == "undefined") {
        this.caseForm.get('pInsuranceID').clearValidators();
        this.caseForm.controls["pInsuranceID"].updateValueAndValidity();

      }
      else{
        this.caseForm.get('pInsuranceID').setValidators([Validators.required])
        this.caseForm.controls["pInsuranceID"].updateValueAndValidity();
      }

    }

    showSaveModal(saveType){
      this.saveTypeGlobal = saveType;
      if (saveType == 0) {
        this.saveModalTitle = "Save as Draft";
        this.saveModalText  = "The case has been saved as Draft";
        $('#submitModal').modal('show');
      }
      else if(saveType == 1){
        this.saveModalTitle = "Submit";
        this.saveModalText  = "Are you sure to submit, the case cannot be deleted after submission";
        $('#submitModal').modal('show');
      }
      else if(saveType == 2){
        this.saveModalTitle = "Submit";
        this.saveModalText  = "Are you sure to submit, the case cannot be deleted after submission";

        if (this.caseFromIntake == null) {
          $('#submitModal').modal('show');
        }
        else{
          Swal.fire({
            title: this.saveModalTitle,
            text: this.saveModalText,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, do it!',
            allowOutsideClick:false
          }).then((result) => {
            if (result.value) {
              this.saveCase(this.saveTypeGlobal);
            } else if (result.dismiss === Swal.DismissReason.cancel) {
            }

          })
        }

      }
      else if(saveType == 3){
        this.saveModalTitle = "Submit";
        this.saveModalText  = "Are you sure to submit, the case cannot be deleted after submission";

        if (this.caseFromIntake == null) {
          $('#submitModal').modal('show');
        }
        else{
          Swal.fire({
            title: this.saveModalTitle,
            text: this.saveModalText,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, do it!',
            allowOutsideClick:false
          }).then((result) => {
            if (result.value) {
              this.saveCase(this.saveTypeGlobal);
            } else if (result.dismiss === Swal.DismissReason.cancel) {
            }

          })
        }

      }

    }


    loadSearchClients(e){

      //
      if (e.target.value.length >=3) {
        this.clientLoading = true
        const searchURL = environment.API_CLIENT_ENDPOINT + 'searchclientbyname';
        this.searchClientName.searchString = e.target.value;
        this.specimenService.getPeople(e.target.value).subscribe(resp =>{
          this.allClientName = resp;
          this.clientLoading = false;

        })
      }
      else{
        console.log("length not match");

      }


    }

    clientChanged(){
      // console.log("this.saveRequest",this.saveRequest);
      if (this.clientSearchString != null) {
        this.saveRequest.data.clientId = this.clientSearchString.clientId;
        this.clientNameToPrint = this.clientSearchString.name;

        console.log("client Changed",this.clientSearchString);
        if (typeof this.clientSearchString.userCode != 'undefined') {
        // if (this.clientSearchString.clientUsers.length >0) {

          const uniqueUIds = [];
          const map = new Map();
          if (this.clientSearchString.active == 1 && this.clientSearchString.isVisibleInCases == 1) {
            // var code = this.encryptDecrypt.encryptString(item.userCode)
             uniqueUIds.push(this.clientSearchString.userCode)
          }
          // for (const item of this.clientSearchString.clientUsers) {
          //   if (item.active == 1 && item.visibleInCases == 1) {
          //     // var code = this.encryptDecrypt.encryptString(item.userCode)
          //     if(!map.has(item.userCode)){
          //
          //       map.set(item.userCode, true);    // set any value to Map
          //       uniqueUIds.push(item.userCode)
          //     }
          //   }
          //
          // }
          if (uniqueUIds.length == 0) {
            alert("There is no active physician this client or the physician cannot access the cases please select a diffirent client")
            // alert("no active Physician found for this client please select a diffirent client")
            this.saveRequest.data.clientId = null;
            this.clientSearchString = null;
            return;
          }
          console.log("physicianIDs",uniqueUIds);
          var attphysUrl = environment.API_USER_ENDPOINT + 'attendingphysicians'
          var reqData={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"",functionalityCode:"SPCA-CD",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{userCodes:uniqueUIds}}
          reqData.header.userCode    = this.logedInUserRoles.userCode
          reqData.header.partnerCode = this.logedInUserRoles.partnerCode
          reqData.header.functionalityCode = "USRA-MUE"
          // if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-UC") != -1) {
          //   reqData['header'].functionalityCode    = "CLTA-UC";
          // }
          // else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-VC") != -1){
          //   reqData['header'].functionalityCode    = "CLTA-VC";
          // }
          // else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-CD") != -1){
          //   reqData['header'].functionalityCode    = "CLTA-CD";
          // }
          // console.log('reqData',reqData);
          this.specimenService.getAttPhysician(attphysUrl, reqData).subscribe(attResp => {
            console.log("att Physicain Resp",attResp);
            if (attResp['data'].length > 0) {

              attResp['data'].map((i) => { i.name = i.firstName + ' ' + i.lastName; return i; });

              // console.log('this.allAtdPhysicians',this.allAtdPhysicians);
              this.allAtdPhysicians = attResp['data'];
              this.selectedAttPhysician = this.allAtdPhysicians[0];
              // for (let i = 0; i < this.clientSearchString.clientUsers.length; i++) {
              //   for (let j = 0; j <   this.allAtdPhysicians.length; j++) {
              //     if (this.clientSearchString.clientUsers[i].defaultPhysician == 1) {
              //       if (  this.allAtdPhysicians[j].userCode == this.clientSearchString.clientUsers[i].userCode) {
              //         this.selectedAttPhysician = this.allAtdPhysicians[j];
              //         // console.log('here',this.selectedAttPhysician);
              //         break;
              //       }
              //     }
              //
              //   }
              // }
            }
            else{
              alert("no Physician found for this client please select a diffirent client")
              this.saveRequest.data.clientId = null;
              this.clientSearchString = null;
            }
            this.checkPatient();

          },
          error =>{
            this.ngxLoader.stop();
            this.notifier.notify( "error", "Error while loading Attending Physicians for the client")
          })


        }
        else{
          alert('client doesnot have a user please select a different client')
          this.clientSearchString = null;
          this.saveRequest.data.clientId = null;
        }
        for (let m = 0; m < this.defaultAccountTypes.length; m++) {
          if (typeof this.clientSearchString.clientTypeDto[0] != 'undefined') {
            if (this.clientSearchString.clientTypeDto[0].clientTypeId != null) {
              if (this.defaultAccountTypes[m].clientTypeId == this.clientSearchString.clientTypeDto[0].clientTypeId) {
                // console.log('-----',this.defaultAccountTypes[m]);

                if (this.defaultAccountTypes[m].name.toLowerCase().indexOf('nursing')>-1 ) {
                  for (let i = 0; i < this.allCaseCategories.length; i++) {
                    if (this.allCaseCategories[i].caseCategoryName.toLowerCase().indexOf('nursing')>-1) {
                      this.selectedCaseCat = this.allCaseCategories[i]
                      this.saveRequest.data.caseCategoryId     = this.selectedCaseCat.caseCategoryId;
                      this.saveRequest.data.caseCategoryPrefix = this.selectedCaseCat.caseCategoryPrefix;
                    }
                  }
                }
                else if (this.defaultAccountTypes[m].name.toLowerCase().indexOf('surgical')>-1 ) {
                  for (let i = 0; i < this.allCaseCategories.length; i++) {
                    if (this.allCaseCategories[i].caseCategoryName.toLowerCase().indexOf('surgical')>-1) {
                      this.selectedCaseCat = this.allCaseCategories[i]
                      this.saveRequest.data.caseCategoryId     = this.selectedCaseCat.caseCategoryId;
                      this.saveRequest.data.caseCategoryPrefix = this.selectedCaseCat.caseCategoryPrefix;
                    }
                  }
                }
                else if (this.defaultAccountTypes[m].name.toLowerCase().indexOf('covid-19')>-1 ){
                  ///covid
                  for (let i = 0; i < this.allCaseCategories.length; i++) {
                    if (this.allCaseCategories[i].caseCategoryName.toLowerCase().indexOf('covid-19')>-1) {
                      this.selectedCaseCat = this.allCaseCategories[i]
                      this.saveRequest.data.caseCategoryId     = this.selectedCaseCat.caseCategoryId;
                      this.saveRequest.data.caseCategoryPrefix = this.selectedCaseCat.caseCategoryPrefix;
                    }
                  }
                }
                else if (this.defaultAccountTypes[m].name.toLowerCase().indexOf('Covid General Case')>-1 ){
                  ///covid
                  for (let i = 0; i < this.allCaseCategories.length; i++) {
                    if (this.allCaseCategories[i].caseCategoryName.toLowerCase().indexOf('Covid General Case')>-1) {
                      this.selectedCaseCat = this.allCaseCategories[i]
                      this.saveRequest.data.caseCategoryId     = this.selectedCaseCat.caseCategoryId;
                      this.saveRequest.data.caseCategoryPrefix = this.selectedCaseCat.caseCategoryPrefix;
                    }
                  }
                }
                else if (this.defaultAccountTypes[m].name.toLowerCase().indexOf('Covid Employee Case')>-1 ){
                  ///covid
                  for (let i = 0; i < this.allCaseCategories.length; i++) {
                    if (this.allCaseCategories[i].caseCategoryName.toLowerCase().indexOf('Covid Employee Case')>-1) {
                      this.selectedCaseCat = this.allCaseCategories[i]
                      this.saveRequest.data.caseCategoryId     = this.selectedCaseCat.caseCategoryId;
                      this.saveRequest.data.caseCategoryPrefix = this.selectedCaseCat.caseCategoryPrefix;
                    }
                  }
                }
                else{
                  ///covid
                  for (let i = 0; i < this.allCaseCategories.length; i++) {
                    if (this.allCaseCategories[i].caseCategoryName.toLowerCase().indexOf('covid-19')>-1) {
                      this.selectedCaseCat = this.allCaseCategories[i]
                      this.saveRequest.data.caseCategoryId     = this.selectedCaseCat.caseCategoryId;
                      this.saveRequest.data.caseCategoryPrefix = this.selectedCaseCat.caseCategoryPrefix;
                    }
                  }
                }
              }
            }
          }


        }
        if (this.saveRequest.data.clientId != null) {
          this.saveRequest.data.intakeId = null;
          this.getIntakeForClient(this.saveRequest.data.clientId);

        }

      }
      else{
        this.saveRequest.data.clientId = null;
      }
    }

    // clientChanged(){
    //   // console.log("this.saveRequest",this.saveRequest);
    //   this.saveRequest.data.clientId = this.clientSearchString.clientId;
    //   console.log("client Changed",this.clientSearchString);
    //   this.checkPatient();
    //
    // }

    changespecimenType(){
      // console.log("this.specimenType",this.specimenType);
      console.log("this.allSpecimenTypes",this.allSpecimenTypes);
      // console.log("this.allSpecimenSource",this.allSpecimenSource);

      if (this.specimenType == null) {
        // this.specimenType   = this.allSpecimenTypes[2];
        // this.specimenSource = this.allSpecimenSource[2];
        // this.bodySite       = this.allSBodySite[2];
        // this.procedure      = this.allProcedures[1];
      }
      else{
        if (this.specimenType.specimenTypeId   == this.allSpecimenTypes[0].specimenTypeId) {
          this.specimenSource = this.allSpecimenSource[3];
          this.bodySite       = this.allSBodySite[0];
          this.procedure      = this.allProcedures[0];
        }
        else if (this.specimenType.specimenTypeId == this.allSpecimenTypes[1].specimenTypeId) {
          console.log('this.allSpecimenTypes[1].specimenTypeId',this.allSpecimenTypes[1]);

          this.specimenSource = this.allSpecimenSource[1];
          this.bodySite       = this.allSBodySite[2];
          this.procedure      = this.allProcedures[0];
        }
        else if (this.specimenType.specimenTypeId   == this.allSpecimenTypes[2].specimenTypeId) {
          this.specimenSource = this.allSpecimenSource[2];
          this.bodySite       = this.allSBodySite[3];
          this.procedure      = this.allProcedures[0];
        }
        else if (this.specimenType.specimenTypeId   == this.allSpecimenTypes[3].specimenTypeId) {
          this.specimenSource = this.allSpecimenSource[0];
          this.bodySite       = this.allSBodySite[1]
          this.procedure      = this.allProcedures[1];

        }

      }
    }
    changeSpecimenSource(){

    }
    changebodySite(){

    }
    changeProcedure(){

    }

    checkPatient(){
      this.pateientChanged = true;
      this.patientRecordUpdateafterCancel = true;
      console.log('patType',this.patType);
      var ageDB, monthDB, dayDB, ageTocheck;
      if (this.patDob != null) {
        if (this.patDob > (this.maxDate)) {
          this.patDob = null;
          return;
        }
        var birthDate = new Date(this.patDob);

        if ((birthDate.getMonth()+1) < 10) {
          monthDB = "0"+(birthDate.getMonth()+1);
        }
        else{
          monthDB = (birthDate.getMonth()+1);
        }
        if (birthDate.getDate() < 10) {
          dayDB = "0"+birthDate.getDate();
        }
        else{
          dayDB = birthDate.getDate();
        }
        ageDB = monthDB + "-" + dayDB + "-" + birthDate.getFullYear();
        console.log("ageDB",ageDB);
        ageTocheck = ageDB;
        this.ageToSaveinDB = ageDB;
      }
      else{
        if (this.patFName != null) {
          this.patFName = this.patFName.replace(/[^a-zA-Z0-9\s]/g, '');
          this.patFName = this.patFName.trim();

        }
        if (this.patLName != null) {
          console.log('this.patLName',this.patLName);

          this.patLName = this.patLName.replace(/[^a-zA-Z0-9\s]/g, '');
          this.patLName = this.patLName.trim();
        }
        return;
      }
      if (this.patFName != null) {
        this.patFName = this.patFName.trim();

      }
      if (this.patLName != null) {
        this.patLName = this.patLName.trim();
      }


      // console.log('patFName',this.patLName);
      // console.log('patFName',this.patFName);
      // console.log('patGender',this.patGender);
      // // /////// return;
      // console.log('patDOB',this.patDob);
      // console.log("clientId",this.saveRequest.data.clientId);
      ////// return;
      // this.saveRequest.data.clientId = 2
      if (this.patFName != null && this.patLName != null && this.patDob != null && this.saveRequest.data.clientId != null) {

        // console.log("log here");

        this.ngxLoader.startBackground();
        var reqPatBP  = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"",functionalityCode:"PNTA-MP",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{username:"danish",firstName:this.patFName,lastName:this.patLName,dateOfBirth:ageDB,clientId:this.saveRequest.data.clientId}}
        // if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UC") != -1) {
        //   reqPatBP['header'].functionalityCode    = "SPCA-UC";
        // }
        // else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-VC") != -1){
        //   reqPatBP['header'].functionalityCode    = "SPCA-VC";
        // }
        // else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-CD") != -1){
        //   reqPatBP['header'].functionalityCode    = "SPCA-CD";
        // }
        var findPTURL = environment.API_PATIENT_ENDPOINT + 'findpatient';
        this.patientDOBPrint = this.patDob
        this.lNamePrint = this.patLName;
        this.fNamePrint = this.patFName;
        this.ageToSaveinDB = ageDB;
        this.specimenService.findPatient(findPTURL, reqPatBP).subscribe(reps => {
          // console.log('reps',reps);

          if (reps['data'] == null) {
            if (this.pageType == 'edit') {
              this.selectedPatientID = this.originalPatient.data.patientId;
              this.selectedPatientContact = this.originalPatient.data.patientDemographics.contacts;
              this.selectedPatientAddress = this.originalPatient.data.patientDemographics.address;
            }
            else{
              this.selectedPatientID = null;

            }
            //
            this.ngxLoader.stopBackground();
            // this.saveAgain = true;
            //
            // if (this.patientLoadedCheck == true || this.pageType == 'edit') {
            //
            //   this.SavePatient();
            // }
            //


          }
          else if(reps['data'] != null) {
            Swal.fire({
              title: 'Hi!',
              text: "The Patient Already Exist Do you want to load it?",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, load it!',
              allowOutsideClick:false
            }).then((result) => {
              if (result.value) {
                if (reps['data'].status != 1) {
                  this.notifier.notify('warning','The selected patien is inactive you will not be able to create case using this patient.')
                  $('.existCheck-loadergif').css('display',"none");
                  // this.selectedPatientID = null;
                  if (this.pageType == 'edit') {
                    this.selectedPatientID = this.originalPatient.data.patientId;
                    this.selectedPatientContact = this.originalPatient.data.patientDemographics.contacts;
                    this.selectedPatientAddress = this.originalPatient.data.patientDemographics.address;
                  }
                  else{
                    this.selectedPatientID = null;

                  }
                  this.patientRecordUpdateafterCancel = false;
                  this.patientLoadedCheck = false;
                }
                else{
                  this.caseForm.get('pType').clearValidators()
                  this.caseForm.controls["pType"].updateValueAndValidity();
                  //////// patient Found Save ID
                  this.selectedPatientID = reps['data'].patientId;
                  this.patFName = this.encryptDecrypt.decryptString(reps['data'].firstName);
                  this.patLName = this.encryptDecrypt.decryptString(reps['data'].lastName);
                  this.lNamePrint = this.encryptDecrypt.decryptString(reps['data'].lastName);
                  this.fNamePrint = this.encryptDecrypt.decryptString(reps['data'].firstName);
                  var ar = reps['data'].dateOfBirth.split('-');
                  // console.log("ar",ar);
                  this.patDob = ar[2]+"-"+ar[0]+"-"+ar[1];
                  if (typeof reps['data'].gender != "undefined") {
                    this.patGender = reps['data'].gender;
                  }


                  // this.patType   = reps['data'].patientTypeId;
                  if (typeof reps['data'].ssn != "undefined") {
                    this.patSSN    = this.encryptDecrypt.decryptString(reps['data'].ssn);
                  }
                  this.patientLoadedCheck = true;
                  this.saveAgain = false;

                  this.pateientChanged == false;  ////////// a new patien is loaded
                }

                this.ngxLoader.stopBackground();
              }
              else if (result.dismiss === Swal.DismissReason.cancel) {
                $('.existCheck-loadergif').css('display',"none");
                this.selectedPatientID = null;
                this.patientRecordUpdateafterCancel = false;
                this.patientLoadedCheck = false;
                // this.saveAgain = false;
                this.ngxLoader.stopBackground();
                // this.resetPatient();

              }
            })
          }
        },error =>{
          this.ngxLoader.stop();
          this.ngxLoader.stopBackground();
          this.notifier.notify( "error", "Error while loading Patient Connection Check")

        })
      }

    }
    SavePatient(){
      return new Promise((resolve, reject) => {
        if (this.patGender == null || this.patType == null) {
          this.ngxLoader.stopBackground();
          resolve(false);
          return;
        }
        if (this.patientRecordUpdateafterCancel == false) {
          // this.patType = []
          // this.patGender = null
          alert('please change pateint first name, last name or date of birth another patient exist with same record.')
          this.ngxLoader.stopBackground();
          resolve(false);
          return;
        }
        // if (this.saveAgain == false) {
        //   return;
        // }
        this.ngxLoader.startBackground();
        var myDate = new Date();
        var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyy HH:mm:ss');



        // const ssnTouch = this.caseForm.get('pSSN');
        // const typeTouch = this.caseForm.get('pType');
        // if (ssnTouch.touched ) {
        //   console.log('ssnTOuch',ssnTouch);
        //
        // }
        // else{
        //   console.log('ssnNot TOuch',ssnTouch);
        // }
        ////// patient not found add patient
        var myDate = new Date();
        var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyy');
        var currentTimeStamp = this.datePipe.transform(myDate, 'MM-dd-yyy HH:mm:ss');
        var savePURL = environment.API_PATIENT_ENDPOINT + 'save';
        // var saveReq  = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"",functionalityCode:"SPCA-CD",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{accountNumber:"ABC-21",clientId:2,createdSource:4,patientDemographics:{firstName:this.patFName,lastName:this.patLName,dateOfBirth:this.patDob,createdBy:"sip-C-001",createdTimestamp:"09-15-2020",},createdBy:"sip-C-001",createdTimestamp:"09-15-2020",userName:"danish"}}
        var ssnTOsend = '';

        if (this.patSSN != null && this.patSSN != '') {
          ssnTOsend = this.encryptDecrypt.encryptString(this.patSSN)
        }

        var saveReq = {
          header: {
            uuid: "",
            partnerCode: "",
            userCode: "",
            referenceNumber: "",
            systemCode: "",
            moduleCode: "",
            functionalityCode: "PNTA-AP",
            systemHostAddress: "",
            remoteUserAddress: "",
            dateTime: ""
          },
          data: {
            accountNumber: "",
            clientId: this.saveRequest.data.clientId,
            createdSource: 4,
            patientInsurance: 123423234,
            patientDemographics: {
              patientTypeId: this.patType,
              patientTypeStr: this.patType,
              firstName: this.encryptDecrypt.encryptString(this.patFName.trim()),
              lastName: this.encryptDecrypt.encryptString(this.patLName.trim()),
              gender: this.patGender.trim(),
              dateOfBirth: this.ageToSaveinDB,
              ssn: ssnTOsend,
              createdBy: this.logedInUserRoles.userCode,
              createdTimestamp: currentTimeStamp,
            },
            createdBy: this.logedInUserRoles.userCode,
            createdTimestamp: currentTimeStamp,
            userName: this.logedInUserRoles.userName
          }
        }
        saveReq.data.patientDemographics.createdBy = this.logedInUserRoles.userCode;
        saveReq.data.patientDemographics.createdTimestamp = currentTimeStamp;
        saveReq.data.createdBy = this.logedInUserRoles.userCode;
        saveReq.data.createdTimestamp = currentTimeStamp;
        saveReq.data.userName = this.logedInUserRoles.userCode;
        saveReq['header'].partnerCode = this.logedInUserRoles.partnerCode;
        saveReq['header'].userCode = this.logedInUserRoles.userCode;
        // saveReq['header'].functionalityCode = "PNTA-AP";
        if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UC") != -1) {
          saveReq['header'].functionalityCode    = "SPCA-UC";
        }
        // else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-VC") != -1){
        //   saveReq['header'].functionalityCode    = "SPCA-VC";
        // }
        else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-CD") != -1){
          saveReq['header'].functionalityCode    = "SPCA-CD";
        }
        console.log('saveReq patient',saveReq);

        // this.ngxLoader.stopBackground();return;

        this.specimenService.savePatient(savePURL, saveReq).subscribe(savePmin =>{
          console.log("Save with 6",savePmin);
          this.ngxLoader.stopBackground();

          if (savePmin['result'].codeType == "S") {
            // this.patFName = savePmin['data'].patientDemographics.firstName;
            // this.patLName = savePmin['data'].patientDemographics.lastName;
            // this.patDob   = savePmin['data'].patientDemographics.dateOfBirth;
            // this.patGender = reps['data'].patientDemographics.gender;
            // this.patType   = reps['data'].patientTypeId;
            // this.patSSN    = reps['data'].patientDemographics.ssn;
            this.selectedPatientID = savePmin['data'].patientId;
            // this.patientDOBPrint = savePmin['data'].patientDemographics.dateOfBirth;
            this.notifier.notify( "success", "Patient Created Successfully")
            this.patientLoadedCheck = false;
            resolve(true);

          }
          else{
            console.log("error");
            this.ngxLoader.stopBackground();
            this.notifier.notify( "error", "Error while saving patient")
            resolve(false);

          }
        }, error =>{
          this.ngxLoader.stop();
          this.notifier.notify( "error", "Error while saving Patient Connection Check")
          resolve(false);
        })
      })



    }

    updatePatient(){
      return new Promise((resolve, reject) => {
        var myDate = new Date();
        var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyy HH:mm:ss');
        // console.log("ssn",this.patSSN);
        // console.log("patGender",this.patGender);
        // console.log("this.patType.patientTypeId",this.patType);
        // if (this.patGender == null || this.patType == null) {
        if (this.selectedPatientID != null && this.patFName != null && this.patLName != null && this.patDob != null && this.saveRequest.data.clientId != null && this.patGender == null || this.patType == null) {

          this.ngxLoader.stopBackground();
          return;
        }
        this.ngxLoader.startBackground();
        var checkAssociationUrl = environment.API_SPECIMEN_ENDPOINT + 'casebyclientpatientid';
        var checkData   = {
          header: {
            uuid: "",
            partnerCode: "",
            userCode: "",
            referenceNumber: "",
            systemCode: "",
            moduleCode: "SPCM",
            functionalityCode: "",
            systemHostAddress: "",
            remoteUserAddress: "",
            dateTime: ""
          },
          data: {
            clientId: '',
            patientId: ""
          }
        }
        checkData['header'].partnerCode = this.logedInUserRoles.partnerCode;
        checkData['header'].userCode    = this.logedInUserRoles.userCode;
        checkData['header'].functionalityCode    = "SPCA-MC";
        
        // if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-UC") != -1) {
        //   checkData['header'].functionalityCode    = "CLTA-UC";
        // }
        // else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-VC") != -1){
        //   checkData['header'].functionalityCode    = "CLTA-VC";
        // }
        // else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-CD") != -1){
        //   checkData['header'].functionalityCode    = "CLTA-CD";
        // }
        checkData['data'].clientId      = this.clientSearchString.clientId;
        checkData['data'].patientId     = this.selectedPatientID;

        this.http.put(checkAssociationUrl,checkData).subscribe(checkResp =>{
          console.log('checkResp',checkResp);
          if (checkResp['data'] == false) {
            var savePURL = environment.API_PATIENT_ENDPOINT + 'update';
            console.log(" this.selectedPatientID", this.selectedPatientID);

            var saveReq = {
              header: {
                uuid: "",
                partnerCode: "",
                userCode: "",
                referenceNumber: "",
                systemCode: "",
                moduleCode: "",
                functionalityCode: "SPCA-CD",
                systemHostAddress: "",
                remoteUserAddress: "",
                dateTime: ""
              },
              data: {
                createdTimestamp: this.originalPatient.data.createdTimestamp,
                createdBy: this.originalPatient.data.createdBy,

                patientId: this.selectedPatientID,
                clientId: this.clientSearchString.clientId,
                // clientId: this.saveRequest.data.clientId,
                patientDemographics: {
                  createdTimestamp: this.originalPatient.data.createdTimestamp,
                  createdBy: this.originalPatient.data.createdBy,
                  patientTypeId: this.patType,
                  firstName: this.patFName.trim(),
                  lastName: this.patLName.trim(),
                  // fullName: this.patFName.trim()+ " "+ this.patLName.trim(),
                  gender: this.patGender,
                  dateOfBirth: this.datePipe.transform(this.patDob, 'MM-dd-yyy'),
                  // dateOfBirth: this.ageToSaveinDB,patDob
                  ssn: this.patSSN,
                  updatedBy: this.logedInUserRoles.userCode,
                  updatedTimestamp: currentDate,
                  contacts:[]
                },
                userName: "danish"
              }
            }
            saveReq.data.patientDemographics.updatedBy = this.logedInUserRoles.userCode;
            saveReq.data.patientDemographics.updatedTimestamp = currentDate;
            saveReq.data.userName = this.logedInUserRoles.userCode;
            saveReq['header'].partnerCode = this.logedInUserRoles.partnerCode;
            saveReq['header'].userCode = this.logedInUserRoles.userCode;
            // saveReq['header'].functionalityCode = "SPCA-UC";
            if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-UC") != -1) {
              saveReq['header'].functionalityCode    = "CLTA-UC";
            }
            else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-VC") != -1){
              saveReq['header'].functionalityCode    = "CLTA-VC";
            }
            else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-CD") != -1){
              saveReq['header'].functionalityCode    = "CLTA-CD";
            }
            // saveReq['data'] = this.originalPatient.data;
            this.specimenService.updatePatient(savePURL, saveReq).subscribe(savePmin =>{
              console.log("update patient",savePmin);
              this.ngxLoader.stopBackground();

              if (savePmin['result'].codeType == "S") {
                resolve(true);
                // this.selectedPatientID = savePmin['data'].patientId;
                // this.patientDOBPrint = savePmin['data'].patientDemographics.dateOfBirth;
                // this.notifier.notify( "success", "Patient updated Successfully")

              }
              else{
                console.log("error");
                this.ngxLoader.stopBackground();
                this.notifier.notify( "error", "Error while updating patient")
                resolve(false)

              }
            }, error =>{
              this.ngxLoader.stop();
              this.notifier.notify( "error", "Error while updating Patient Connection Check")
              resolve(false)
            })
          }
          else{
            this.SavePatient().then(savePatientResp => {
              // console.log("savePatientResp",savePatientResp);

              if (savePatientResp == true) {
                // this.selectedPatientID =
                resolve(true);
                // this.saveMaxCase()

              }
            })
          }

        },error=>{
          console.log('error',error);
          resolve(false)
        })



      })
      // if (this.patientLoadedCheck == true) {
      //   return;
      // }


    }
    resetPatient(){
      this.patLName                     = null;
      this.patFName                     = null;
      this.patDob                       = null;
      this.patGender                    = null;
      this.patType                      = null;
      this.patSSN                       = null;
      this.selectedPatientID            = null;
      this.insuranceCompany             = null;
      this.pInsuranceID                 = null;
      return;
    }
    resetFields(){
      // this.caseForm.controls['attType'].reset();
      // this.caseForm.controls['attFile'].reset();
      // this.specimenTable   = [];
      this.attachmentTable = [];
      this.caseForm.controls['attType'].reset();
      this.caseForm.controls['attFile'].reset();
      $('#upload-att').val('');
      this.attachmentTable = [];
      this.resetPatient();
    }

    scrollTo(el: Element): void {
      if (el) {
        el.scrollIntoView({ behavior: 'smooth', block: 'center' });
      }
      // this.submitted = false;
    }
    scrollToError(): void {
      const firstElementWithError = document.querySelector('.ng-invalid[formControlName]');
      // const firstElementWithError1 = $('.ng-invalid');
      // console.log("aa---:", firstElementWithError);
      // console.log("attachment-select",$('#attachment-select'));
      // console.log("has class",$('#attachment-select').hasClass( "is-invalid" ));
      // console.log("has class",$('#attachment-select').hasClass( "dropdown-arrow" ));
      if (this.attachmentTable['length'] == 0) {

        // $('#caseattach_tab').tab('show');
        $('#att_button').trigger('click');
        // $('#att_button')[0].click();
      }


      this.scrollTo(firstElementWithError);

    }

    popSpecTable(){
      if(this.specimenType == null || this.specimenSource == null || this.bodySite == null || this.procedure == null){
        alert("Provide all the fields")
      }
      else{
        var specTholder = {};
        specTholder['spType']        =  this.specimenType['specimenTypeName'];
        specTholder['spTypeID']      =  this.specimenType['specimenTypeId'];
        specTholder['spSource']      =  this.specimenSource['specimenSourceName'];
        specTholder['spSourceID']    =  this.specimenSource['specimenSourceId'];
        specTholder['spBody']        =  this.bodySite['bodySiteName'];
        specTholder['spBodyID']      =  this.bodySite['bodySiteId'];
        specTholder['spProcedure']   =  this.procedure['procedureName'];
        specTholder['spProcedureID'] =  this.procedure['procedureId'];
        specTholder['caseSpecimenId'] =  null;
        specTholder['deleteBackend'] =  null;
        this.specimenTable.push(specTholder);
      }
    }

    removeSpec(){
      console.log("specToRemove",this.specToRemove);
      this.specimenTable.splice(this.specToRemove, 1);
      $('#removespecModal').modal('hide');
    }

    popFile(e){
      var input = e.target;
      console.log("input",input.files);
      // console.log("this.selectattachmentTypeId",this.selectattachmentTypeId);
      if (typeof input.files[0] == 'undefined') {
        return;
      }

      var attTemp = {}
      var optionSelected = $('#attachment-select option:selected').attr('attname');

      // this.saveRequest.data.caseAttachments[0].attachmentTypeId = this.selectattachmentTypeId;
      if (this.selectattachmentTypeId != null) {
        var mb = input.files[0].size/1048576;
        // console.log("mb",mb);
        if (mb <= 10) {
          if (input.files[0].type != 'image/jpeg' && input.files[0].type != 'image/jpg' && input.files[0].type != 'image/png' && input.files[0].type != 'application/pdf') {
            $('#upload-att').val('');
            alert('invalid file type');
            return;
          }
          attTemp['payLoad']  = input.files[0];
          attTemp['attachId'] = this.selectattachmentTypeId;
          var reader = new FileReader();
          reader.onload = (e: any) => {
            // console.log('Got here: ', e.target.result);
            attTemp['fileurl'] = this.sanitizer.bypassSecurityTrustUrl(e.target.result);
            // this.obj.photoUrl = e.target.result;
          }
          reader.readAsDataURL(input.files[0]);
          var fileName = input.files[0].name;
          attTemp['type'] = optionSelected;
          // attTemp['type'] = this.selectattachmentTypeId;
          attTemp['file'] = fileName;
          attTemp['notDownload'] = true;
          this.attachmentTable.push(attTemp);
          // console.log("this.attachmentTable",this.attachmentTable);

        }
        else{
          $('#upload-att').val('');
          alert('The uploaded file size is greater than 10MB')
        }

      }
      else{
        $('#upload-att').val('');
        alert('Select Attacment Type');
      }

    }

    downloadFile(caseNumber,file,type,fileId){


      this.ngxLoader.start();
      // console.log('environment.production',environment.production);
      var a = [];
      var b = '';

      // if (environment.production ==  false) {
      //   a = file.split('\\');
      //   var l = a.length;
      //   b = a[l-2]+'\\'+a[l-1];
      // }else{
      //   a = file.split('/');
      //   var l = a.length;
      //   b = a[l-2]+'/'+a[l-1];
      // }
      var downloadUrl = environment.API_SPECIMEN_ENDPOINT+"getfile"
      var downloadReq={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-CA",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:
      {caseNumber:caseNumber,fileUrl:'',fileId:fileId
      }}
      if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UC") != -1) {
        downloadReq['header'].functionalityCode    = "SPCA-UC";
      }
      else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-VC") != -1){
        downloadReq['header'].functionalityCode    = "SPCA-VC";
      }
      else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-CD") != -1){
        downloadReq['header'].functionalityCode    = "SPCA-CD";
      }
      this.specimenService.downloadFiles(downloadUrl,downloadReq,type).subscribe(downloadResp=>{
        console.log("downloadResp",downloadResp);
        this.ngxLoader.stop();

      },
      error=>{
        this.ngxLoader.stop();
        this.notifier.notify("error","Error while downloading the file.")
      })

    }


    printLabel(caseNumber,lName,fName,dob,clientName,casePrefix,clientId){
      if(typeof dymo == "undefined"){
        this.ngxLoader.stop();
        return;
      }
      this.ngxLoader.start();
      // this.ngxLoader.stop();
      if (typeof lName == 'undefined') {lName = "No Name"}
      if (typeof fName == 'undefined') {fName = "No Name"}
      if (typeof dob == 'undefined') {dob = "No dob"}
      dob = this.datePipe.transform(dob, 'MM/dd/yyy');

      // console.log("caseNumber",caseNumber);
      // console.log("lName",lName);
      // console.log("fName",fName);
      // console.log("dob",dob);
      // console.log("clientName",clientName);
      // console.log("casePrefix",casePrefix);

      var cPrifix = '';
      if (typeof casePrefix == "undefined" || casePrefix == null || casePrefix.length == 0) {
        cPrifix = "NP"
      }
      else{
        for (let i = 0; i < casePrefix.length; i++) {
          cPrifix = cPrifix+''+casePrefix[i];

        }
        // for (let i = 0; i < this.allSpecimenTypes.length; i++) {
        //   for (let j = 0; j < casePrefix.length; j++) {
        //     if (casePrefix[j] == this.allSpecimenTypes[i]['specimenTypeId']) {
        //       cPrifix = cPrifix +' '+ this.allSpecimenTypes[i]['specimenTypePrefix']
        //     }
        //
        //   }
        //
        // }
      }
      console.log("cPrifix",cPrifix);
      // return;
      this.ngxLoader.start();
      console.log("caseNumber",caseNumber);
      console.log("caseNumber",caseNumber);
      console.log("lName",lName);
      console.log("fName",fName);
      console.log("dob",dob);
      console.log("clientName",clientName);
      console.log("casePrefix",casePrefix);
      console.log("cPrifix",cPrifix);
      // this.ngxLoader.stop();return;
      var labelXml = '<DieCutLabel Version="8.0" Units="twips" MediaType="Default"> <PaperOrientation>Landscape</PaperOrientation> <Id>Small30332</Id> <IsOutlined>false</IsOutlined> <PaperName>30332 1 in x 1 in</PaperName> <DrawCommands> <RoundRectangle X="0" Y="0" Width="1440" Height="1440" Rx="180" Ry="180" /> </DrawCommands> <ObjectInfo> <TextObject> <Name>FirstLast</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <HorizontalAlignment>Left</HorizontalAlignment> <VerticalAlignment>Top</VerticalAlignment> <TextFitMode>ShrinkToFit</TextFitMode> <UseFullFontHeight>True</UseFullFontHeight> <Verticalized>False</Verticalized> <StyledText> <Element> <String xml:space="preserve">First Last</String> <Attributes> <Font Family="Arial" Size="5" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" HueScale="100" /> </Attributes> </Element> </StyledText> </TextObject> <Bounds X="144" Y="199.393133236425" Width="1186.49076398256" Height="120" /> </ObjectInfo> <ObjectInfo> <BarcodeObject> <Name>BARCODE</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <Text>12345</Text> <Type>QRCode</Type> <Size>Medium</Size> <TextPosition>None</TextPosition> <TextFont Family="Arial" Size="8" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <CheckSumFont Family="Arial" Size="8" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <TextEmbedding>None</TextEmbedding> <ECLevel>0</ECLevel> <HorizontalAlignment>Center</HorizontalAlignment> <QuietZonesPadding Left="0" Top="0" Right="0" Bottom="0" /> </BarcodeObject> <Bounds X="381.704497509707" Y="771.219004098847" Width="541.292842170495" Height="526.701837189909" /> </ObjectInfo> <ObjectInfo> <TextObject> <Name>Facility</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <HorizontalAlignment>Left</HorizontalAlignment> <VerticalAlignment>Top</VerticalAlignment> <TextFitMode>ShrinkToFit</TextFitMode> <UseFullFontHeight>True</UseFullFontHeight> <Verticalized>False</Verticalized> <StyledText> <Element> <String xml:space="preserve">Facility</String> <Attributes> <Font Family="Arial" Size="4" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" HueScale="100" /> </Attributes> </Element> </StyledText> </TextObject> <Bounds X="164.897098682172" Y="651.471024372647" Width="1183.87862664729" Height="120" /> </ObjectInfo> <ObjectInfo> <TextObject> <Name>DOB</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <HorizontalAlignment>Left</HorizontalAlignment> <VerticalAlignment>Top</VerticalAlignment> <TextFitMode>ShrinkToFit</TextFitMode> <UseFullFontHeight>True</UseFullFontHeight> <Verticalized>False</Verticalized> <StyledText> <Element> <String xml:space="preserve">DOB</String> <Attributes> <Font Family="Arial" Size="5" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" HueScale="100" /> </Attributes> </Element> </StyledText> </TextObject> <Bounds X="144" Y="310.963055930226" Width="1210" Height="120" /> </ObjectInfo> <ObjectInfo> <TextObject> <Name>Client</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <HorizontalAlignment>Left</HorizontalAlignment> <VerticalAlignment>Top</VerticalAlignment> <TextFitMode>ShrinkToFit</TextFitMode> <UseFullFontHeight>True</UseFullFontHeight> <Verticalized>False</Verticalized> <StyledText> <Element> <String xml:space="preserve">Client</String> <Attributes> <Font Family="Arial" Size="5" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" HueScale="100" /> </Attributes> </Element> </StyledText> </TextObject> <Bounds X="144" Y="418.060686676358" Width="1210" Height="120" /> </ObjectInfo> <ObjectInfo> <TextObject> <Name>CaseCode</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <HorizontalAlignment>Left</HorizontalAlignment> <VerticalAlignment>Top</VerticalAlignment> <TextFitMode>ShrinkToFit</TextFitMode> <UseFullFontHeight>True</UseFullFontHeight> <Verticalized>False</Verticalized> <StyledText> <Element> <String xml:space="preserve">Case Code</String> <Attributes> <Font Family="Arial" Size="5" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" HueScale="100" /> </Attributes> </Element> </StyledText> </TextObject> <Bounds X="144" Y="525.15831742249" Width="1210" Height="120" /> </ObjectInfo> </DieCutLabel>';
      var label = dymo.label.framework.openLabelXml(labelXml);

      // var barcodeData = 'Si Paradigm'
      label.setObjectText('BARCODE', caseNumber);
      // set label text
      label.setObjectText("FirstLast",lName +','+ fName);
      label.setObjectText("DOB",  dob);
      // label.setObjectText("Client", clientName.substring(0, 10));
      label.setObjectText("Facility", cPrifix);
      label.setObjectText("CaseCode", caseNumber);
      label.setObjectText("Client", clientName.substring(0, 10));

      var printers = dymo.label.framework.getPrinters();
      if (printers.length == 0){


        this.notifier.notify("error","No DYMO printers are installed. Install DYMO printers.")
        this.ngxLoader.stop()
        if(this.saveTypeGlobal == 2){
          if (this.caseFromIntake == null) {
            $('.modal').modal('hide')
            this.router.navigateByUrl('/manage-cases');
          }
          else{
            // console.log('*-*-*-*-*-*-*-*this.caseFromIntake',this.saveRequest.data.intakeId);

            this.sharedIntakeData.loadCasesInIntake(this.oldIntakeId );
            $('.modal').modal('hide')
          }
        }
        throw "No DYMO printers are installed. Install DYMO printers.";

      }
      else{
        var myDate = new Date();
        var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyy HH:mm:ss');
        var saveLabeRequest = {
          header: {
            uuid: "",
            partnerCode: "sip",
            userCode: "usyduysad",
            referenceNumber: "",
            systemCode: "",
            moduleCode: "SPCM",
            functionalityCode: "SPCA-CD",
            systemHostAddress: "",
            remoteUserAddress: "",
            dateTime: ""
          },
          data:
          {
            caseNumber: "",
            clientId: 1522,
            createdTimestamp: "",
            printedBy: ""
          }
        }

        saveLabeRequest.data.caseNumber = caseNumber;
        saveLabeRequest.data.clientId = clientId;
        saveLabeRequest.data.createdTimestamp = currentDate;
        saveLabeRequest.data.printedBy = this.logedInUserRoles.userCode;
        saveLabeRequest.header.userCode = this.logedInUserRoles.userCode;
        saveLabeRequest.header.partnerCode = this.logedInUserRoles.partnerCode;
        // saveLabeRequest.header.functionalityCode = "SPCA-CD";
        if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-UC") != -1) {
          saveLabeRequest['header'].functionalityCode    = "CLTA-UC";
        }
        else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-VC") != -1){
          saveLabeRequest['header'].functionalityCode    = "CLTA-VC";
        }
        else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-CD") != -1){
          saveLabeRequest['header'].functionalityCode    = "CLTA-CD";
        }

        var saveLabelUrl = environment.API_SPECIMEN_ENDPOINT+'savelabel';
        this.specimenService.printLabel(saveLabelUrl,saveLabeRequest).subscribe(saveLabelResponse =>{
          console.log('saveLabelResponse',saveLabelResponse);

        },error=>{
          this.notifier.notify("error","Error while saveing print history.")
        })

      }

      var printerName = "";
      for (var i = 0; i < printers.length; ++i)
      {
        var printer = printers[i];
        if (printer.printerType == "LabelWriterPrinter")
        {
          printerName = printer.name;
          break;
        }
      }

      label.print(printerName);
      this.printedCaseNumber = caseNumber;
      console.log("this.printedCaseNumber--",this.printedCaseNumber);
      // $('#pecimenlabel-Modal').modal('show');
      $('#pecimenlabel-Modal').modal('show');
      this.ngxLoader.stop();
      // console.log('this.onlyPrint',this.onlyPrint);
      // console.log('this.pageType',this.pageType);

      if (this.onlyPrint == true) {
        return;
      }
      if (this.pageType == 'edit') {
        // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        this.route.params.subscribe(params => {
          // console.log('params',params['id']);
          if (typeof params['id'] == "undefined") {
            this.pageType = 'add';
            this.changespecimenType()
          }
          else{
            this.pageType = 'edit';
            // var notGen = history.state;
            var coverted = params['id'].replace('xsarm','+').replace('ydan','/').replace('zhar','=');
            var bytes  = CryptoJS.AES.decrypt(coverted, 'nglis');
            var originalText = bytes.toString(CryptoJS.enc.Utf8).toString();
            console.log("originalText",originalText);
            var test = originalText.split(';')
            console.log("test",test);

            let from = test[1];
            // let from = params['from']
            console.log("notGen",from);
            // if (notGen.navigationId == 1 && typeof notGen.id == 'undefined') {
            //   // this.router.navigateByUrl('/manage-cases');
            // }
            console.log('this.saveTypeGlobal',this.saveTypeGlobal);


            if(this.saveTypeGlobal == 0){
              let columnCaseReq  = {}
              var selectURL = environment.API_SPECIMEN_ENDPOINT + 'searchbycaseid';
              // columnCaseReq      = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-CD",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{caseId: params['id']}}
              columnCaseReq      = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-CD",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{caseId: test[0]}}
              if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UC") != -1) {
                columnCaseReq['header'].functionalityCode    = "SPCA-UC";
              }
              else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-VC") != -1){
                columnCaseReq['header'].functionalityCode    = "SPCA-VC";
              }
              else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-CD") != -1){
                columnCaseReq['header'].functionalityCode    = "SPCA-CD";
              }
              this.getSelectedRecord(selectURL,columnCaseReq).then(selectedCase => {
                // console.log("Selected Case response",selectedCase);
                // this.chec();
                // console.log("selectedCase",this.selectedCase);
                console.log("saveRequest",this.saveRequest);
                // this.ngxLoader.stop();
                this.changespecimenType()


              });
            }
            else{
              if (from == 3) {
                this.caseForm.get('attType').clearValidators();
                this.caseForm.controls["attType"].updateValueAndValidity();
                this.caseForm.get('attFile').clearValidators();
                this.caseForm.controls["attFile"].updateValueAndValidity();
              }
              let columnCaseReq  = {}
              var selectURL = environment.API_SPECIMEN_ENDPOINT + 'searchbycasenumber';
              columnCaseReq      = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-CD",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{page:"0",size:20,sortColumn:"caseNumber",sortingOrder:"desc",searchDate:"",caseNumber: caseNumber}}
              if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UC") != -1) {
                columnCaseReq['header'].functionalityCode    = "SPCA-UC";
              }
              else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-VC") != -1){
                columnCaseReq['header'].functionalityCode    = "SPCA-VC";
              }
              else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-CD") != -1){
                columnCaseReq['header'].functionalityCode    = "SPCA-CD";
              }
              this.getSelectedRecord(selectURL,columnCaseReq).then(selectedCase => {
                // console.log("Selected Case response",selectedCase);
                // this.chec();
                // console.log("selectedCase",this.selectedCase);
                console.log("saveRequest",this.saveRequest);
                // this.ngxLoader.stop();
                this.changespecimenType()


              });
            }


          }
          console.log('pageType:  ',this.pageType);

        })
      }

    }

    getLookups(){
      // this.ngxLoader.start();
      return new Promise((resolve, reject) => {
        var caseCat = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=CASE_CATEGORY_CACHE&partnerCode=sip').then(response =>{
          return response;
        })
        var patType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=PATIENT_TYPE_CACHE&partnerCode=sip').then(response =>{
          return response;
        })
        var insComp = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=INSURANCE_COMPANY_CACHE&partnerCode=sip').then(response =>{
          return response;
        })
        var specType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=SPECIMEN_TYPE_CACHE&partnerCode=sip').then(response =>{
          return response;
        })
        var specSource = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=SPECIMEN_SOURCE_CACHE&partnerCode=sip').then(response =>{
          return response;
        })
        var bodySite = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=BODY_SITE_CACHE&partnerCode=sip').then(response =>{
          return response;
        })
        var procedure = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=PROCEDURE_CACHE&partnerCode=sip').then(response =>{
          return response;
        })
        var attachType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=ATTACHMENT_TYPE_CACHE&partnerCode=sip').then(response =>{
          return response;
        })
        var statusType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=CASE_STATUS_CACHE&partnerCode=sip').then(response =>{
          return response;
        })
        var accType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=CLIENT_TYPE_CACHE&partnerCode=sip').then(response =>{
          return response;
        })
        var covidTest = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=TEST_CACHE&partnerCode=sip').then(response =>{
          return response;
        })
        var testStatus = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=TEST_STATUS_CACHE&partnerCode=sip').then(response =>{
          return response;
        })
        var interpretation = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=TEST_INTERPRETATION_CACHE&partnerCode=sip').then(response =>{
          return response;
        })


        forkJoin([caseCat, patType, insComp, specType, specSource, bodySite, procedure, attachType, statusType, accType,covidTest,testStatus,interpretation]).subscribe(allLookups => {
          console.log("allLookups",allLookups);
          this.allCaseCategories     = this.sortPipe.transform(allLookups[0], "asc", "caseCategoryName");
          this.allpattypes           = this.sortPipe.transform(allLookups[1], "asc", "patientTypeName");
          this.allInsuraneCompanies  = this.sortPipe.transform(allLookups[2], "asc", "insuranceName");
          this.allSpecimenTypes      = allLookups[3];
          // this.allSpecimenTypes      = this.sortPipe.transform(allLookups[3], "asc", "specimenTypeName");
          // this.allSpecimenSource     = this.sortPipe.transform(allLookups[4], "asc", "specimenSourceName");
          this.allSpecimenSource     = allLookups[4];
          // this.allSBodySite          = this.sortPipe.transform(allLookups[5], "asc", "bodySiteName");
          this.allSBodySite          = allLookups[5];
          this.allProcedures         = allLookups[6];
          // this.allProcedures         = this.sortPipe.transform(allLookups[6], "asc", "procedureName");
          var att  = this.sortPipe.transform(allLookups[7], "asc", "attachmentTypeName");
          // this.allAttachmentTypes    = this.sortPipe.transform(allLookups[7], "asc", "attachmentTypeName");
          this.allStatusType         = this.sortPipe.transform(allLookups[8], "asc", "caseStatusName");
          this.defaultAccountTypes   = this.sortPipe.transform(allLookups[9], "asc", "name");
          this.covidTestLookup       = allLookups[10];
          this.testStatusLookup      = allLookups[11];
          this.allInterpretations    = allLookups[12];

          for (let i = 0; i < this.allCaseCategories.length; i++) {
            if (this.allCaseCategories[i].caseCategoryDefault == 1) {
              this.selectedCaseCat = this.allCaseCategories[i];
              this.saveRequest.data.caseCategoryId     = this.allCaseCategories[i].caseCategoryId;
              this.saveRequest.data.caseCategoryPrefix = this.allCaseCategories[i].caseCategoryPrefix;
            }
          }
          for (let j = 0; j < this.allSpecimenTypes.length; j++) {
            this.allSpecimenTypes[j]['labelToShow'] = this.allSpecimenTypes[j]['specimenTypePrefix']+' - '+this.allSpecimenTypes[j]['specimenTypeName'];
            if (this.allSpecimenTypes[j]['specimenTypePrefix'] == 'NP') {
              this.specimenType          = this.allSpecimenTypes[j];
              this.specimenSource        = this.allSpecimenSource[1];
              this.bodySite              = this.allSBodySite[2];
              this.procedure             = this.allProcedures[0];
            }

          }

          for (let index = 0; index < att.length; index++) {
            if (att[index]['attachmentTypeName'] == "Insurance Card" || att[index]['attachmentTypeName'] == "Consent Form") {
              this.allAttachmentTypes.push(att[index]);
            }
          }
          // console.log('this.allSpecimenTypes',this.allSpecimenTypes);
          // console.log('this.allSpecimenSource',this.allSpecimenSource);
          // console.log('this.allSBodySite',this.allSBodySite);
          // console.log('this.allProcedures',this.allProcedures);


          resolve();
        })
      })

    }

    setCaseCatPrefix(){
      this.saveRequest.data.caseCategoryId     = this.selectedCaseCat.caseCategoryId;
      this.saveRequest.data.caseCategoryPrefix = this.selectedCaseCat.caseCategoryPrefix;


    }

    showDelModal(caseSpecimenId){
      if (this.specimenTable['length'] <= 1) {
        this.notifier.notify('warning','There must be atleast one specimen per case');
      }
      else{
        this.DeletecaseSpecimenId = caseSpecimenId;
        for (let i = 0; i < this.specimenTable['length']; i++) {
          if (this.specimenTable[i]['caseSpecimenId'] == this.DeletecaseSpecimenId) {
            this.specimenTable.splice(i,1);

          }

        }
      }

      // $('#removespecModalSavedSpecimen').modal('show');
    }
    deletSPecimen(){
      // this.DeletecaseSpecimenId;
      // console.log("caseSpecimenId",this.DeletecaseSpecimenId);
      // console.log("this.specimenTable",this.specimenTable);
      // this.ngxLoader.stop()
      // return;

      var urlDel = environment.API_SPECIMEN_ENDPOINT + 'deletespecimen'
      var request = {...this.deleteSpecimenRequest}
      request.data.caseSpecimenId = this.DeletecaseSpecimenId;

      // console.log('request',request);
      // return;
      this.specimenService.deleteSpecimen(urlDel,request).subscribe(delResp=>{
        if (delResp['result'].codeType == 'S') {
          $('#removespecModalSavedSpecimen').modal('hide');
          this.notifier.notify('success','specimen deleted successfully');
          // for (let i = 0; i < this.specimenTable['length']; i++) {
          //   if (this.specimenTable[i]['caseSpecimenId'] == this.DeletecaseSpecimenId) {
          //     this.specimenTable.splice(i,1);
          //
          //   }
          //
          // }
          // this.specimenTableData.splice(this.removeCaseIndex,1);

        }
        else{
          this.notifier.notify('error','error while deleting specimen');
        }

      },error=>{
        this.notifier.notify('error','error while deleting specimen');
      })


    }

    setClientIdForPatient(patientID) {
      console.log('this.selectedClientIDofPatient',patientID);
      // this.sharedData.changeMessage("Hello from Sibling")
      this.sharedData.changePatientId(patientID);
      $('#update-patient-Modal').modal('show');
    }
    clearPatienId(){
      this.sharedData.changePatientId('null');
      $('#update-patient-Modal').modal('show');
    }

    triageCase(caseId){
      console.log("casid",caseId);

      var myDate = new Date();
      var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyy HH:mm:ss');
      var triageUrl = environment.API_COVID_ENDPOINT + 'triage';
      var triageRequest = {...this.triageRequest}
      triageRequest.data.caseId = caseId;
      triageRequest.data.testStatusId = 1;
      triageRequest.data.testId = 2;
      triageRequest.data.createdTimestamp = currentDate;
      triageRequest.data.createdBy = this.logedInUserRoles.userCode;
      triageRequest.header.userCode = this.logedInUserRoles.userCode;
      // triageRequest.header.functionalityCode = "SPCA-CD";
      if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-UC") != -1) {
        triageRequest['header'].functionalityCode    = "CLTA-UC";
      }
      else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-VC") != -1){
        triageRequest['header'].functionalityCode    = "CLTA-VC";
      }
      else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-CD") != -1){
        triageRequest['header'].functionalityCode    = "CLTA-CD";
      }
      this.specimenService.triageRequest(triageUrl,triageRequest).subscribe(triageResp=>{
        console.log('triageResp',triageResp);
        this.ngxLoader.stop();
        if (this.pageType == 'edit' && this.saveTypeGlobal == 1) {
          if (this.caseFromIntake == null) {
            $('.modal').modal('hide')
            this.router.navigateByUrl('/manage-cases');
          }
          else{
            this.sharedIntakeData.loadCasesInIntake(this.oldIntakeId );
            $('.modal').modal('hide')
          }
        }

      },error=>{
        this.notifier.notify('error','error while triaging')
        this.ngxLoader.stop();
      })


    }
    getIntakeForClient(clientId){
      var reqAllIntakes = {...this.allIntakesReq}
      reqAllIntakes.header.partnerCode = this.logedInUserRoles.partnerCode;
      reqAllIntakes.header.userCode = this.logedInUserRoles.userCode;
      reqAllIntakes.header.functionalityCode = "SPCA-MI";
      // if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-UC") != -1) {
      //   reqAllIntakes['header'].functionalityCode    = "CLTA-UC";
      // }
      // else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-VC") != -1){
      //   reqAllIntakes['header'].functionalityCode    = "CLTA-VC";
      // }
      // else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-CD") != -1){
      //   reqAllIntakes['header'].functionalityCode    = "CLTA-CD";
      // }
      reqAllIntakes.data.clientId                = [clientId] ;
      reqAllIntakes.data.searchString                = 'byclient';
      let intakeUrl    = environment.API_SPECIMEN_ENDPOINT + 'showintakes';
      this.specimenService.getIntakesForCases(intakeUrl, reqAllIntakes).subscribe(getIntakeResp=>{
        console.log("getIntakeResp",getIntakeResp);
        this.allIntakesForClient = getIntakeResp['data']['caseDetails'];



      })
    }

    printHistory(caseNumber){
      this.ngxLoader.start();
      this.specimenService.getreportingHistory(environment.API_RTL_ENDPOINT+'labelhostory?actionCode=SPCA-PL&entityCodeCaseNumber='+caseNumber).then(response =>{
        console.log("response history",response);
        this.ngxLoader.stop();
        if (response['length'] > 0) {
          var uniqueUserIds = []
          for (let i = 0; i < response['length']; i++) {
            if (uniqueUserIds.indexOf(response[i].printedBy) == -1) {
              uniqueUserIds.push(response[i].printedBy);
            }
          }
          var usersRequest = {
            header              : {
              uuid              : "",
              partnerCode       : "",
              userCode          : "",
              referenceNumber   : "",
              systemCode        : "",
              moduleCode        : "",
              functionalityCode : "",
              systemHostAddress : "",
              remoteUserAddress : "",
              dateTime          : ""
            },
            data                : {
              userCodes           :[]
            }
          }
          usersRequest.data.userCodes = uniqueUserIds;
          var userUrl = environment.API_USER_ENDPOINT + "clientusers";
          usersRequest.header.partnerCode  = this.logedInUserRoles.partnerCode;
          usersRequest.header.userCode     = this.logedInUserRoles.userCode;
          usersRequest.header.functionalityCode     = "CLTA-VC";
          this.specimenService.getUserRecord(userUrl,usersRequest).then(getUserResp =>{
            for (let i = 0; i < response['length']; i++) {
              for (let j = 0; j < getUserResp['data'].length; j++) {
                if (getUserResp['data'][j].userCode == response[i].printedBy) {
                  response[i].printedByName = getUserResp['data'][j].fullName;
                }

              }
            }

          })

          this.labelHistory = response;
          $('#spec-labelhistory-Modal').modal('show');
        }
        else{
          this.labelHistory = [];
          this.notifier.notify('warning','No history found')
        }
      },error=>{
        this.labelHistory = [];
        this.ngxLoader.stop();
        this.notifier.notify('error','Error while loading history')
      })
    }

    fetchFinalReport(resultData){
      return new Promise ((resolve, reject) => {

        // jobReq.data.   = uniqueJobOrderIds;
        const uniqueJobOrderIds = [];
        const map = new Map();
        for (const item of resultData) {
          if(!map.has(item.jobOrderId)){
            map.set(item.jobOrderId, true);    // set any value to Map
            uniqueJobOrderIds.push(item.jobOrderId);
          }
        }

        var jobUrl = environment.API_REPORTING_ENDPOINT + 'showreporthistory';
        var jobReq = {...this.finalReportReq}
        jobReq.header.userCode    = this.logedInUserRoles.userCode;
        jobReq.header.partnerCode = this.logedInUserRoles.partnerCode;
        jobReq.header.functionalityCode = "MRMA-MR";
        // if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UC") != -1) {
        //   jobReq['header'].functionalityCode    = "SPCA-UC";
        // }
        // else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-VC") != -1){
        //   jobReq['header'].functionalityCode    = "SPCA-VC";
        // }
        // else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-CD") != -1){
        //   jobReq['header'].functionalityCode    = "SPCA-CD";
        // }
        jobReq.data.jobOrderIds   = uniqueJobOrderIds;
        jobReq.data.pageSize      = this.paginateLengthHistory;
        jobReq.data.pageNumber    = this.paginatePaheHistory;

        this.specimenService.finalReport(jobUrl,jobReq).subscribe(reportsRepsonse=>{
          console.log('reportsRepsonse',reportsRepsonse);
          var temp = {};
          if (reportsRepsonse['result']['codeType'] == 'S') {

            // console.log('reportsRepsonse',reportsRepsonse);
            const uniqueUserIds = [];
            const map1 = new Map();
            for (const item of reportsRepsonse['data']['history']) {
              if(!map1.has(item.sentBy)){
                map1.set(item.sentBy, true);    // set any value to Map
                uniqueUserIds.push(item.sentBy);
              }
            }
            console.log('uniqueUserIds',uniqueUserIds);
            var userRequest = {...this.usersRequest};
            userRequest.data.userCodes = uniqueUserIds;
            userRequest.header.partnerCode  = this.logedInUserRoles.partnerCode;
            userRequest.header.userCode     = this.logedInUserRoles.userCode;
            userRequest.header.functionalityCode     = "CLTA-VC";
            var userUrl = environment.API_USER_ENDPOINT + "clientusers";
            this.specimenService.getUserRecordReport(userUrl,userRequest).subscribe(getUserResp =>{
              // return getUserResp;
              console.log("getUserResp",getUserResp);
              if (getUserResp['data'] != null) {
                for (let k = 0; k < reportsRepsonse['data']['history'].length; k++) {

                  for (let l = 0; l < getUserResp['data'].length; l++) {
                    if(reportsRepsonse['data']['history'][k].sentBy == getUserResp['data'][l].userCode){
                      reportsRepsonse['data']['history'][k].sentByName = getUserResp['data'][l].fullName;
                    }

                  }


                }
                this.allReportsData = reportsRepsonse['data']['history'];
                this.allHistoryDataCount = reportsRepsonse['data']['totalCount'];

                resolve()
                // $('#delivery-historyModal').modal('show');
              }
              else{
                resolve()
              }

            },error=>{
              this.ngxLoader.stop();
              this.notifier.notify("error","error While loading sent by")
              resolve()
            })


          }
          else{
            this.ngxLoader.stop();
            // this.notifier.notify('error','Error while Report')
            resolve()
          }

        },error=>{
          this.ngxLoader.stop();
          this.notifier.notify('error','Error while Report')
          resolve()
        })
      })
    }

    downloadOldReport(reportId,clientName,packageId){
      console.log("packageId",packageId);
      var type = 'pdf';
      if (packageId == 1) {
        type = 'zip'
      }
      else {
        type = 'pdf'
      }

      var downloadAgainRequest = {
        header: {
          uuid: "67ffaebe-6188-476e-86ae-c5deb2effa80",
          partnerCode: "sip",
          userCode: "sip-12598",
          referenceNumber: "TWMM12032020115628",
          systemCode: "NGLIS",
          moduleCode: "TWMM",
          functionalityCode: "TWMA-UR",
          systemHostAddress: "",
          remoteUserAddress: "",
          dateTime: ""
        },
        data: {
          reportId: 173,
          clientName: "Menna",
          timeZoneId:""
        }
      }
      var currentZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
      downloadAgainRequest.data['timeZoneId'] = currentZone;

      downloadAgainRequest.header.userCode    = this.logedInUserRoles.userCode;
      downloadAgainRequest.header.partnerCode = this.logedInUserRoles.partnerCode;
      downloadAgainRequest.header.functionalityCode = "MRMA-DRI";
      downloadAgainRequest.data.reportId      = reportId;
      downloadAgainRequest.data.clientName    = clientName;

      var redownloadURl = environment.API_REPORTING_ENDPOINT + 'downloadoldreport';
      this.specimenService.downloadFiles(redownloadURl,downloadAgainRequest,type).subscribe(redownloadResponse =>{
        console.log("redownloadResponse",redownloadResponse);

      })


    }

    resendReport(reportId, reportPackageId){
      this.ngxLoader.start();
      console.log("item",this.clientSearchString);
      // return;

      var resendRequest = {
        header: {
          uuid: "67ffaebe-6188-476e-86ae-c5deb2effa80",
          partnerCode: "",
          userCode: "sip-12598",
          referenceNumber: "TWMM12032020115628",
          systemCode: "NGLIS",
          moduleCode: "TWMM",
          functionalityCode: "TWMA-UR",
          systemHostAddress: "",
          remoteUserAddress: "",
          dateTime: ""
        },
        data: {
          reportId: 251,
          clientName: "Menna",
          clientEmail:"",
          reportType:"detailed consolidated"
        }
      }
      var packageName = '';

      if (reportPackageId == 1) {
        packageName = 'compressed';
      }
      else if (reportPackageId == 2) {
        packageName = 'detailed consolidated';
      }
      else if (reportPackageId == 3) {
        packageName = 'summary';
      }
      else if (reportPackageId == 4) {
        packageName = 'individual';
      }

      var clientEmail = this.clientSearchString['email'];
      // if (this.clientSearchString['clientContactsByClientId'].length > 0) {
      //   for (let cl = 0; cl < this.clientSearchString['clientContactsByClientId'].length; cl++) {
      //     if (this.clientSearchString['clientContactsByClientId'][cl]['contactByContactId'].length >0) {
      //       if (this.clientSearchString['clientContactsByClientId'][cl]['contactByContactId'][0]['contactTypeId'] == 2) {
      //         clientEmail = this.clientSearchString['clientContactsByClientId'][cl]['contactByContactId'][0]['value']
      //       }
      //     }
      //   }
      // }
      resendRequest.header.partnerCode = this.logedInUserRoles.partnerCode;
      resendRequest.header.userCode    = this.logedInUserRoles.userCode;
      resendRequest.header.functionalityCode    = "SPCA-CD";
      resendRequest.data.reportId      = reportId;
      resendRequest.data.reportType    = packageName;
      resendRequest.data.clientName    = this.clientSearchString.name;
      resendRequest.data.clientEmail   = clientEmail;
      console.log("resendRequest",resendRequest);
      // return;
      var url = environment.API_REPORTING_ENDPOINT+'resendreport';
      this.specimenService.resendReport(url,resendRequest).subscribe(resendResponse=>{
        this.ngxLoader.stop();
        console.log("resendResponse",resendResponse);
        if (resendResponse['result']['codeType'] == 'S') {
          this.notifier.notify('success',"report send successfully");
          const downloadLink = document.createElement("a");

          downloadLink.href = resendResponse['data']['base64EncodedFile'];
          downloadLink.download = resendResponse['data']['fileName'];;
          downloadLink.click();
        }
        else{
          this.notifier.notify('error',"Error while resending report")
        }
      }, error=>{
        this.ngxLoader.stop();
        this.notifier.notify('error',"Error while resending report")
      })

    }
    // popupDateClose(){

    //   $(".clearable").on("touchstart click", function() {

    //     const $inp = $(this).find("input"),
    //     $cle = $(this).find(".clearable__clear"),
    //     $popCle = $(this).find(".popup__clear");

    //     $inp.on("input", function(){
    //       $cle.toggle(!!this.value);
    //       $popCle.toggle(!!this.value);
    //     });

    //     // $popCle.on("touchstart click", function(e) {
    //     //   e.preventDefault();
    //     //   $(".popup__clear").hide("");
    //     // });
    //     // $cle.on("touchstart click", function(e) {
    //     //   e.preventDefault();
    //     //   $inp.val("").trigger("change");
    //     //   $inp.val("").trigger("input");
    //     // });

    //   });

    // }
    clearDate(){

      $(".clearable").on("touchstart click", function() {

        const $inp = $(this).find("input"),
        $cle = $(this).find(".clearable__clear");

        $inp.on("input", function(){
          $cle.toggle(!!this.value);
        });

        // $cle.on("touchstart click", function(e) {
        //   e.preventDefault();
        //   if (this.pageType == 'add') {
        //     $inp.val("").trigger("change");
        //
        //   }
        //   // $inp.val("").trigger("input");
        // });

      });

    }
    cleanDate(e,from){
      // console.log("from",from);

      e.preventDefault();
      if (this.pageType == 'add') {
        if (from == 'rec') {
          $('#rec-date').val("");
        }
        if (from == 'sent') {
          $('#send-date').val("");
        }
        else if (from == 'col') {
          $('#colDate').val("");
        }

        // e.target.value = "";

      }
      else{
        if (from == 'col') {
          this.collectionDateToShow = this.selectedCase.collectionDate;
        }
        if (from == 'sent') {
          this.sendDateToShow = this.selectedCase.sendDate;
        }
        else if (from == 'rec') {
          this.recievingDateToShow = this.selectedCase.receivingDate;
        }
      }
    }
    compare_dates(date1,date2){
      // console.log("date1",date1);
      // console.log("date2",date2);

      if (date1>date2) return 1;
      else if (date1<date2) return 2;
      else return 3;
    }

  }
