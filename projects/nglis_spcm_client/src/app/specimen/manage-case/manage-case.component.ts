import { Component, OnInit, OnDestroy,ElementRef, ViewChild, AfterViewInit, ViewChildren, QueryList } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { environment } from '../../../../../../src/environments/environment';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Router, NavigationEnd } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import Swal from 'sweetalert2'
declare var $ :any;
import { NotifierService } from 'angular-notifier';
import { SpecimenApiCallsService } from '../../services/specimen/specimen-api-calls.service';
import { GlobalApiCallsService } from '../../services/global/global-api-calls.service';
import { concat, Observable, of, Subject, forkJoin, } from "rxjs";
import * as moment from 'moment';
import * as dymo from '../../../assets/js/dymo-label.js';
declare var dymo: any;
import { SortPipe } from "../../pipes/sort.pipe";
import { DatePipe } from '@angular/common';
import { MinimumAccesionDataService } from "../../services/minimumaccessiondata.service";
import { filter } from 'rxjs/operators';
import { GlobalySharedModule } from './../../../../../../src/app/sharedmodules/globaly-shared/globaly-shared.module';
import { UrlGuard } from './../../../../../../src/services/gaurd/url.guard'
import * as CryptoJS from 'crypto-js';

@Component({
  selector    : 'app-manage-case',
  templateUrl : './manage-case.component.html',
  styleUrls   : ['./manage-case.component.css'],
  host        : {
    class     :'mainComponentStyle'
  }
})
export class ManageCaseComponent implements OnDestroy, OnInit {
  searchTerm$      = new Subject<string>();
  searchTermSearch$= new Subject<string>();
  searchTermSearch2$= new Subject<string>();
  searchTermColum$ = new Subject<string>();
  searchTermColumLnameAct$ = new Subject<string>();
  searchTermColumFnameAct$ = new Subject<string>();
  searchTermColumFnameAll$ = new Subject<string>();
  searchTermColumLnameAll$ = new Subject<string>();
  results     : Object;

  @ViewChildren(DataTableDirective)

  dtElement              : QueryList<any>;
  dtTrigger              : Subject<ManageCaseComponent> = new Subject();
  public dtOptions       : DataTables.Settings[] = [];
  public paginatePage    : any = 0;
  public paginatePage2   : any = 0;
  public paginateLength  = 20;
  oldCharacterCount      = 0;
  oldCharacterCount2     = 0;
  submitted              = false;

  allCaseCategories         : any = [];
  selectedCaseCat           = null;
  allClientName             : any;
  allClientNameSearch       : any;
  allClientNameSearch2      : any;
  allAtdPhysicians          : any;
  allStatusType             : any;
  allStatusTypeForActive             : any;
  public caseForm           : FormGroup;
  insuranceCompany          ;
  saveTypeGlobal            = null;
  saveModalTitle            = '';
  saveModalText             = '';

  clientSearchString          ;
  clientLoading               : any;
  clientLoadingSearch         : any;
  clientLoadingSearch2        : any;
  clientInput$                = new Subject<string>();

  public saveRequest          = {
    header  : {
      dateTime: "",
      functionalityCode: "SPCA-MC",
      moduleCode: "SPCM",
      partnerCode: "",
      referenceNumber: "",
      remoteUserAddress: "",
      systemCode: "",
      systemHostAddress: "",
      userCode: "",
      uuid: ""
    },
    data:{

      caseCategoryPrefix:"NH",
      caseNumber:"",
      caseStatusId:1,
      caseCategoryId:null,
      clientId:null,
      attendingPhysicianId:"376",
      patientId:null,
      createdTimestamp:"2020-10-22T11:02:03.523+00:00",
      receivingDate:"2020-10-22T07:54:10.485+00:00",
      collectedBy:"1",
      collectionDate:null,
      accessionTimestamp:"2020-10-22T07:54:10.485+00:00",
      createdBy:"abc",
      caseSpecimen:[
        {
          // caseSpecimenId:1,
          specimenTypeId:2,
          specimenSourceId:1,
          bodySiteId:1,
          procedureId:2,
          createdBy:"abc",
          createdTimestamp:"2020-10-22T07:54:10.485+00:00",
          updatedBy:"",
          updatedTimestamp:""
        }
      ]
    }


  }
  allSpecimenTypes        : any = [];
  specimenType            = null;
  collectDate = null;

  public allCasesReq = {
    header  : {
      uuid              :"",
      partnerCode       :"",
      userCode          :"",
      referenceNumber   :"",
      systemCode        :"",
      moduleCode        :"SPCM",
      functionalityCode :"SPCA-MC",
      systemHostAddress :"",
      remoteUserAddress :"",
      dateTime          :""
    },
    data : {
      page         :"0",
      size         :5,
      sortColumn   :"caseNumber",
      sortingOrder :"desc",
      userCode     :""

    }
  }
  public activeCasesReq = {
    header  : {
      uuid              :"",
      partnerCode       :"",
      userCode          :"",
      referenceNumber   :"",
      systemCode        :"",
      moduleCode        :"SPCM",
      functionalityCode :"SPCA-MC",
      systemHostAddress :"",
      remoteUserAddress :"",
      dateTime          :""
    },
    data : {
      page         :"0",
      size         :5,
      sortColumn   :"caseId",
      sortingOrder :"desc",
      statuses     :[8,9,10,11],
      caseStatus:"",
      intakeId:"",
      client:"",
      userCode: '',
    }
  }

  public newAllCasesReq = {
    header  : {
      uuid              :"",
      partnerCode       :"",
      userCode          :"",
      referenceNumber   :"",
      systemCode        :"",
      moduleCode        :"SPCM",
      functionalityCode :"SPCA-MC",
      systemHostAddress :"",
      remoteUserAddress :"",
      dateTime          :""
    },
    data : {
    }
  }
  activeCases      = []
  activeCasesCount = 100;
  allCases         = [];
  allCasesCount    = 100;

  public patientByIdsReq   ={
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"",
      functionalityCode:"SPCA-MC",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      username:"danish",
      patientIds:[]
    }

  }

  patientFName = null;
  patientLName = null;
  patientDOB   = null;
  selectedPatientID = null;
  cNameforSearch;
  cNameforSearch2;
  searchtextHolder;
  searchtextHolder2;
  selectedAttPhysician = null;
  printedCaseNumber= null;
  patientDOBPrint;
  lNamePrint
  fNamePrint;

  // public searchCaseNoReq = {
  //   header  : {
  //     uuid              :"",
  //     partnerCode       :"",
  //     userCode          :"",
  //     referenceNumber   :"",
  //     systemCode        :"",
  //     moduleCode        :"moduleCode",
  //     functionalityCode :"SPCA-MC",
  //     systemHostAddress :"",
  //     remoteUserAddress :"",
  //     dateTime          :""
  //     },
  //   data : {
  //     page         :"",
  //     size         :20,
  //     sortColumn   :"",
  //     sortingOrder :"asc",
  //     searchDate   :"",
  //     caseNumber   :""
  //
  //
  //   }
  // }
  greenColor = false;
  clientNameToPrint;

  swalStyle              : any;
  public searchByCriteria : any ={
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"SPCM",
      functionalityCode:"SPCA-MC",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      page:"0",
      size:"20",
      sortColumn:"caseNumber",
      sortingOrder:"desc",
      collectionDate:"",
      caseNumber:"",
      clientId:"",
      collectedBy:"",
      caseStatusId:"",
      statuses:[],
      intakeId : "",
      specimenTypeId : "",
      userCode: ""

    }

  }
  maxDate;
  defaultAccountTypes : any ;
  public logedInUserRoles :any = {};
  public triageRequest = {
    header  : {
      uuid              :"",
      partnerCode       :"",
      userCode          :"",
      referenceNumber   :"",
      systemCode        :"",
      moduleCode        :"TWMM",
      functionalityCode :"SPCA-MC",
      systemHostAddress :"",
      remoteUserAddress :"",
      dateTime          :""
    },
    data : {
      caseId:"6",
      testStatusId:1,
      testId:1,
      createdBy:"abc",
      createdTimestamp: "26-11-2020 20:07:25"

    }
  }
  saveRequestCollectionDate = null;
  dataForCreateCaseFromIntake : any = [];
  selectedIntakeIdForCases = null;
  previousUrl: string = null;
  currentUrl: string = null;
  public intakeCasesReq = {
    header  : {
      uuid              :"",
      partnerCode       :"",
      userCode          :"",
      referenceNumber   :"",
      systemCode        :"",
      moduleCode        :"SPCM",
      functionalityCode :"SPCA-MC",
      systemHostAddress :"",
      remoteUserAddress :"",
      dateTime          :""
    },
    data : {
      page         :"0",
      size         :5,
      sortColumn   :"caseId",
      sortingOrder :"desc",
      intakeId     :""
    }
  }
  selectedClientIdForCases = null;
  labelHistory : any = [];
  viewCaseAction = false;
  timeCount ;
  timeCount0 ;
  timeCount1 ;
  timeCount2 ;
  showBatchHyperLink = false
  activecasesTabPan = true;
  allcasesTabPan;

  constructor(
    private formBuilder     : FormBuilder,
    private http            : HttpClient,
    private router          : Router ,
    private route           : ActivatedRoute,
    private sanitizer       : DomSanitizer,
    private ngxLoader       : NgxUiLoaderService,
    private notifier        : NotifierService,
    private specimenService : SpecimenApiCallsService,
    private globalService   : GlobalApiCallsService,
    private sortPipe        :SortPipe,
    private datePipe: DatePipe,
    private sharedIntakeData : MinimumAccesionDataService,
    private rbac         : UrlGuard
  ) {

    //   if(this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UC") != -1 || this.logedInUserRoles['allowedRoles'].indexOf("SPCA-VC") != -1){
    //     this.viewCaseAction = true;
    // }
    // this.rbac.checkROle('SPCA-CQ');
    // this.rbac.checkROle('SPCA-MC');

    this.notifier        = notifier;
    this.swalStyle       =  Swal.mixin({
      customClass : {
        confirmButton : 'btn btn-success my-swal-success',
        cancelButton  : 'btn btn-danger'
      },
      buttonsStyling  : true
    })

  }
  fromDashboard(searchType, clientId,intakeId,specimenId,clientName) {

    return new Promise((resolve, reject) => {
      // console.log('f1');
      // console.log("searchType999999999999999",searchType);


      if(searchType =="totalDraft"){
        $('#searchStatus').val('Draft');
        $('#searchClient').val(clientName);
        $('#searchClient2').val(clientName);
        $("#searchStatus").trigger('change');
        $("#searchStatus2").trigger('change');
        resolve();
      }
      else if(searchType =="totalAccess"){
        $('#searchStatus').val('Accessioned');
        $('#searchClient').val(clientName);
        $('#searchClient2').val(clientName);
        $("#searchStatus").trigger('change');
        $("#searchStatus2").trigger('change');
        resolve();
      }
      else if (searchType =="specimentTypeIntake") {
        $('#intakeIdFilter').val(intakeId);
        $('#searchSpecimenType').val(specimenId);
        $("#intakeIdFilter").trigger('change');
        // $("#searchSpecimenType").trigger('change');
        $('#intakeIdFilter2').val(intakeId);
        $('#searchSpecimenType2').val(specimenId);
        $("#intakeIdFilter2").trigger('change');
        resolve();
      }
      else {
        // console.log('searchType specimen',searchType);

        $('#searchStatus').val(searchType);
        $('#searchStatus2').val(searchType);
        $('#searchClient').val(clientName);
        $('#searchClient2').val(clientName);
        $("#searchStatus").trigger('change');
        $("#searchStatus2").trigger('change');
        resolve();
      }

    });
  }

  ngOnInit(): void {

    this.sharedIntakeData.setGreenColorFormanageCaseOnly("null");


    this.router.events.pipe(filter(event => event instanceof NavigationEnd))
    .subscribe((event: NavigationEnd) => {

      this.previousUrl = this.currentUrl;
      this.currentUrl = event.url;
      if (this.previousUrl != null) {


        if (this.previousUrl.indexOf( "/edit-intake/") !== -1) {
          this.sharedIntakeData.loadCasesInIntake('null');
          this.selectedIntakeIdForCases = null;
          this.sharedIntakeData.reloadEditIntakeAfteminAcceesion('null');
          this.selectedIntakeIdForCases = null;
          this.sharedIntakeData.loadClientIdForEditIntake('null');
          this.selectedClientIdForCases = null;
          // console.log("innnnnnnnn");

        }
      }
      else{
        this.sharedIntakeData.loadCasesInIntake('null');
        this.selectedIntakeIdForCases = null;
        this.sharedIntakeData.reloadEditIntakeAfteminAcceesion('null');
        this.selectedIntakeIdForCases = null;
        this.sharedIntakeData.loadClientIdForEditIntake('null');
        this.selectedClientIdForCases = null;
        // console.log("nullllll");

      }
      // console.log('this.previousUrl:', this.previousUrl);

      // console.log('this.currentUrl:', this.currentUrl);

    });

    var dtToday = new Date();

    var m = dtToday.getMonth()+1;
    var d = dtToday.getDate();
    var year = dtToday.getFullYear();

    var month;
    var day;
    if(m < 10)
    month = '0' + m.toString();
    else
    month = m
    if(d < 10)
    day = '0' + d.toString();
    else
    day = d

    var maxDate = year + '-' + month + '-' + day;
    this.maxDate = maxDate;


    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
    var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
    var originalText = bts.toString(CryptoJS.enc.Utf8);
    this.logedInUserRoles = JSON.parse(originalText)
    // console.log("------",this.logedInUserRoles);

    // this.logedInUserRoles.userName    = "snazirkhanali";
    // this.logedInUserRoles.partnerCode = 1;
    if(this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UC") != -1 || this.logedInUserRoles['allowedRoles'].indexOf("SPCA-VC") != -1){
      this.viewCaseAction = true;
    }
    if(this.logedInUserRoles['allowedRoles'].indexOf("TWMA-UB") != -1){
      this.showBatchHyperLink = true;
    }


    this.getLookups().then(lookupsLoaded => {


      this.ngxLoader.stop();
      this.sharedIntakeData.successReloadAjax.subscribe(intakeRecord => {
        console.log('---intakeRecord reload ajax 11',intakeRecord);
        if (intakeRecord != 'null') {
          this.sharedIntakeData.setGreenColorAfterMinAccession('null');
          this.greenColor = true;
          this.ngxLoader.stop();
          this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
            dtElement.dtInstance.then((dtInstance: any) => {
              dtInstance.ajax.reload();
            })
          })

        }
        else{
          // this.greenColor = true;
        }

      })
      this.sharedIntakeData.loadIntakeCases.subscribe(intakeCases => {
        console.log('---intakeCases',intakeCases);
        // console.log('---intakeCases ID',typeof this.selectedIntakeIdForCases);

        if (intakeCases != 'null') {

          this.selectedIntakeIdForCases = intakeCases;
          this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
            dtElement.dtInstance.then((dtInstance: any) => {
              dtInstance.ajax.reload();
            })
          })


        }

      })
      this.sharedIntakeData.clientIdforEditIntake.subscribe(intakeCasesClientID => {
        console.log('---intakeCasesClientID',intakeCasesClientID);
        // console.log('---intakeCases ID',typeof this.selectedIntakeIdForCases);

        if (intakeCasesClientID != 'null') {

          this.selectedClientIdForCases = intakeCasesClientID;



        }

      })
    });
    // console.log("SARMADNAZIRABBASI",history.state.fromCreate);

    if(typeof history.state.fromCreate != "undefined"){
      this.greenColor = true;
      // this.setOnce = true;
    }
    else{
      if (localStorage.getItem('FromCreate')=='true') {
        // console.log("in set once --------------");

        this.greenColor = true;
      }
    }
    // var fromDash = history.state;
    // console.log("fromDash",fromDash);
    // if (typeof fromDash.searchBy != "undefined") {
    //   this.fromDashboard(fromDash.searchBy,fromDash.id).then(resDash => {
    //
    //   });
    //
    // }


    // var dateTime = new Date().toLocaleString();
    // this.collectDate = moment(dateTime).format("MM-dd-yyyY HH:MM");
    // console.log("collectDate",this.collectDate);
    // this.f1().then(res => this.f2());
    this.specimenService.search(this.searchTerm$)
    .subscribe(results => {
      console.log("results",results);
      this.allClientName = results['data'];
      this.clientLoading = false;
    },
    error => {
      this.notifier.notify( "error", "Error while fetching clients backend connect aborted" );
      this.clientLoading = false;
      return;
      // console.log("error",error);
    });

    /////////////// load client Name for search active
    this.specimenService.search(this.searchTermSearch$)
    .subscribe(resultsSearch => {
      console.log("results",resultsSearch);
      this.allClientNameSearch = resultsSearch['data'];
      this.clientLoadingSearch = false;
    },
    error => {
      this.notifier.notify( "error", "Error while fetching clients backend connect aborted" );
      this.clientLoadingSearch = false;
      return;
      // console.log("error",error);
    });

    /////////////// load client Name for search
    this.specimenService.search(this.searchTermSearch2$)
    .subscribe(resultsSearch2 => {
      console.log("results",resultsSearch2);
      this.allClientNameSearch2 = resultsSearch2['data'];
      this.clientLoadingSearch2 = false;
    },
    error => {
      this.notifier.notify( "error", "Error while fetching clients backend connect aborted" );
      this.clientLoadingSearch2 = false;
      return;
      // console.log("error",error);
    });
    this.caseForm       = this.formBuilder.group({
      caseCategory      : ['', [Validators.required]],
      clientName        : ['', [Validators.required]],
      atdPhysician      : ['', [Validators.required]],
      collectionDate    : ['', [Validators.required]],
      collectedBy       : ['1', [Validators.required]],
      pLastName         : ['', [Validators.required]],
      pFirstName        : ['', [Validators.required]],
      pDOB              : ['', [Validators.required]],
      specimenType      : ['', [Validators.required]],
    })


    // this.dtOptions[0] = {
    //   pagingType   : 'full_numbers',
    //   pageLength   : 50,
    //   serverSide   : false,
    //   processing   : false,
    //   ordering     : false,
    //   "lengthMenu": [20, 50, 75, 100 ],
    //
    // };
    this.dtOptions[0] = {
      pagingType   : 'full_numbers',
      pageLength   : this.paginateLength,
      serverSide   : true,
      processing   : true,
      // ordering     : false,
      "order": [[12, "desc" ]],
      columnDefs: [
        { orderable: true, className: 'reorder', targets: [0,4,5,6,7,8,9,10] },
        { orderable: false, targets: [11] },
        {
          targets: [12],
          visible: false,
          orderable: true,
        },
      ],
      "lengthMenu": [20, 50, 75, 100 ],
      ajax: (dataTablesParameters: any, callback) => {

        if(this.timeCount0) {
          clearTimeout(this.timeCount0);
          // console.log("here",this.timeCount);

          this.timeCount0 = null;
        }
        this.timeCount0 = setTimeout(()=>{
          this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
            // this.activeCases = [];
            // this.activeCasesCount = 0;


            dtElement.dtInstance.then((dtInstance: any) => {
              if (dtInstance.table().node().id == 'addactive_cases' ) {
                console.log("dataTablesParameters--",dataTablesParameters);

                var sortColumn = dataTablesParameters.order[0]['column'];
                var sortOrder  = dataTablesParameters.order[0]['dir'];
                var sortArray  = ["caseNumber",'patientLastName','patientFirstName','patientDateOfBirth','clientName','intakeId','batchId','specimenType','collectionDate','collectedBy','caseStatusId','createdTimestamp','createdTimestamp']
                // var url    = environment.API_ACDM_ENDPOINT + 'allcases';


                // if (dataTablesParameters) {
                // console.log("dataTablesParameters",dataTablesParameters);
                //   return;
                // }
                this.paginatePage           = dtElement['dt'].page.info().page;
                this.paginateLength         = dtElement['dt'].page.len();

                var columnToSearch          = "";
                var searchValue             = "";
                var caseNumValue            = "";
                var clientValue             = "";
                var collectDateValue        = "";
                var collectedByValue        = "";
                var caseStatusValue         = "";
                var intakeValue             = "";
                var specimenTypeId          = "";
                var reqAllCases = {}
                let url    = environment.API_ACDM_ENDPOINT + 'allcases';
                reqAllCases = {...this.newAllCasesReq}
                // var sortColumn = dataTablesParameters.order[0]['column'];
                // var sortOrder  = dataTablesParameters.order[0]['dir'];
                // var sortArray  = ["patientId",'patientId','firstName','lastName','dateOfBirth','ssn','patientId','patientId','patientId','patientId','patientId','patientId']
                if (dataTablesParameters.columns[0].search.value != "") {
                  // console.log("*********",dataTablesParameters.columns[0].search.value);

                  if (dataTablesParameters.columns[0].search.value.length>=3 || dataTablesParameters.columns[0].search.value.length == 0) {
                    reqAllCases['data']['caseNumber'] = dataTablesParameters.columns[0].search.value;
                    searchValue = "true";
                    // columnToSearch = "searchbycasenumber"; searchValue = dataTablesParameters.columns[0].search.value;
                    // caseNumValue = dataTablesParameters.columns[0].search.value;

                  }
                  else{
                    return;
                  }
                }
                if (dataTablesParameters.columns[8].search.value != "") {
                  reqAllCases['data']['collectionDate'] = dataTablesParameters.columns[8].search.value;
                  searchValue = "true";
                  // columnToSearch = "searchbydate"; collectDateValue = dataTablesParameters.columns[8].search.value;       searchValue = dataTablesParameters.columns[8].search.value
                }
                if (dataTablesParameters.columns[9].search.value != "") {
                  reqAllCases['data']['collectedBy'] = dataTablesParameters.columns[9].search.value;
                  searchValue = "true";
                  // columnToSearch = "searchcollectedby"; collectedByValue = dataTablesParameters.columns[9].search.value;  searchValue = dataTablesParameters.columns[9].search.value
                }
                if (dataTablesParameters.columns[10].search.value != "") {
                  reqAllCases['data']['status'] = dataTablesParameters.columns[10].search.value;
                  searchValue = "true";
                  // columnToSearch = "searchbycasesstatus";
                  // caseStatusValue = dataTablesParameters.columns[10].search.value; searchValue = dataTablesParameters.columns[10].search.value;


                  // if (history.state.id != null || typeof history.state.id != 'undefined') {
                  //   if (dataTablesParameters.columns[4].search.value == '') {
                  //     // reqAllCases['data']['status'] = dataTablesParameters.columns[0].search.value;
                  //
                  //     reqAllCases['data']['clientName'] = history.state.clientName;
                  //     // searchValue  = history.state.id;
                  //   }
                  //   else{
                  //     reqAllCases['data']['clientName'] = this.cNameforSearch;
                  //     // searchValue  = this.cNameforSearch;
                  //   }
                  //
                  // }
                }
                if (dataTablesParameters.columns[5].search.value != "") {
                  // columnToSearch = "searchbyintakeid"; intakeValue = dataTablesParameters.columns[5].search.value;
                  // searchValue = dataTablesParameters.columns[5].search.value;
                  reqAllCases['data']['intakeId'] = dataTablesParameters.columns[5].search.value;
                  searchValue = "true";

                  // if (history.state.intakeId != null || typeof history.state.intakeId != 'undefined') {
                  //   if (dataTablesParameters.columns[7].search.value == '') {
                  //     reqAllCases['data']['specimenType'] = history.state.specimenId;
                  //   }
                  //   else{
                  //     reqAllCases['data']['specimenType'] = dataTablesParameters.columns[7].search.value;
                  //   }
                  //
                  // }
                }
                if (dataTablesParameters.columns[7].search.value != "") {
                  reqAllCases['data']['specimenType'] = dataTablesParameters.columns[7].search.value;
                  searchValue = "true";
                  // columnToSearch = "searchbyspecimenid"; specimenTypeId = dataTablesParameters.columns[7].search.value; searchValue = dataTablesParameters.columns[7].search.value
                }
                // console.log("---------",this.cNameforSearch);
                // if (this.cNameforSearch != null) {
                if (dataTablesParameters.columns[4].search.value != "") {
                  reqAllCases['data']['clientName'] = dataTablesParameters.columns[4].search.value;
                  searchValue = "true";

                  // columnToSearch = "searchbyclientname"; clientValue = this.cNameforSearch;searchValue  = this.cNameforSearch
                }
                if (dataTablesParameters.columns[6].search.value != "") {
                  reqAllCases['data']['batchId'] = dataTablesParameters.columns[6].search.value;
                  searchValue = "true";

                  // columnToSearch = "searchbyclientname"; clientValue = this.cNameforSearch;searchValue  = this.cNameforSearch
                }
                // }

                if (dataTablesParameters.columns[1].search.value != "") {
                  // console.log("------------------------------------------------------",dataTablesParameters.columns[1].search.value);

                  // if (dataTablesParameters.columns[1].search.value.length>=3 || dataTablesParameters.columns[1].search.value.length == 0) {
                    reqAllCases['data']['patientLastName'] = dataTablesParameters.columns[1].search.value;
                    searchValue = "true";

                    // columnToSearch = "searchbylastname"; searchValue = dataTablesParameters.columns[1].search.value
                  // }
                  // else{
                  //   return;
                  // }
                }

                if (dataTablesParameters.columns[2].search.value != "") {
                  // if (dataTablesParameters.columns[2].search.value.length>=3 || dataTablesParameters.columns[2].search.value.length == 0) {
                    reqAllCases['data']['patientFirstName'] = dataTablesParameters.columns[2].search.value;
                    searchValue = "true";

                    // columnToSearch = "searchbyfirstname"; searchValue = dataTablesParameters.columns[2].search.value
                  // }
                  // else{
                  //   return;
                  // }

                }
                if (dataTablesParameters.columns[3].search.value != "") {
                  reqAllCases['data']['patientDateOfBirth'] = dataTablesParameters.columns[3].search.value;
                  searchValue = "true";

                  // columnToSearch = "searchbydob";searchValue = dataTablesParameters.columns[3].search.value
                }

                this.paginatePage           = dtElement['dt'].page.info().page;
                this.paginateLength         = dtElement['dt'].page.len();
                // return;
                // if (dataTablesParameters.search.value == "" && (this.oldCharacterCount == 0 || this.oldCharacterCount == 3) && searchValue != "") {
                //
                //
                // }

                // else if (dataTablesParameters.search.value == "" && (this.oldCharacterCount == 0 || this.oldCharacterCount == 3)) {
                if (dataTablesParameters.search.value != "" && (this.oldCharacterCount == 0 || this.oldCharacterCount == 3)) {
                  if (this.oldCharacterCount == 3) {
                  }
                  reqAllCases['data']['mainSearch'] = dataTablesParameters.search.value;
                }
                else{
                  $('.dataTables_processing').css('display',"none");
                  if (this.oldCharacterCount == 3) {
                    this.oldCharacterCount     = 3
                  }
                  else{
                    this.oldCharacterCount     = 2;
                  }
                }
                if (this.selectedIntakeIdForCases != null) {
                  // reqAllCases = {...this.intakeCasesReq}
                  // reqAllCases['data'].intakeId =  this.selectedIntakeIdForCases;
                  // url    = environment.API_SPECIMEN_ENDPOINT + 'showintakebyid';
                  reqAllCases['data']['intakeId'] = this.selectedIntakeIdForCases;

                  var addDeliveredForIntake = {
                    caseStatusId: 10,
                    caseStatusName: "Delivered",
                    createdBy: "1",
                    createdTimestamp: "10-19-2020",
                    partnerCode: "sip",
                    updatedBy: null,
                    updatedTimestamp: null,
                  }
                  const found = this.allStatusTypeForActive.some(el => el.caseStatusName === "Delivered");
                  if (found == false) {
                    this.allStatusTypeForActive.push(addDeliveredForIntake);
                    this.allStatusTypeForActive         = this.sortPipe.transform(this.allStatusTypeForActive, "asc", "caseStatusName");
                  }


                }
                reqAllCases['data']['pageNumber']           = dtElement['dt'].page.info().page;
                reqAllCases['data']['pageSize']            = dtElement['dt'].page.len();
                reqAllCases['data']['statuses']            = [1,2,3,4,5,6,7,8,9,12];
                reqAllCases['header'].partnerCode  = this.logedInUserRoles.partnerCode;
                reqAllCases['header'].userCode     = this.logedInUserRoles.userCode;
                reqAllCases['header'].functionalityCode     = "SPCA-ALC";
                // if (sortColumn != 1 && sortColumn != 2 && sortColumn != 3) {
                  if (this.greenColor == true) {
                    if (this.saveTypeGlobal == 0) {
                      reqAllCases['data'].sortColumn     = 'caseId';
                      reqAllCases['data'].sortType   = 'desc';
                      // reqAllCases.data.sortColumn     = sortArray[sortColumn];
                      // reqAllCases.data.sortingOrder   = sortOrder;
                    }
                    else{
                      reqAllCases['data'].sortColumn     = 'caseId';
                      reqAllCases['data'].sortType   = 'desc';
                    }
                    // reqAllCases.data.sortColumn     = 'caseId';
                    // reqAllCases.data.sortingOrder   = 'desc';
                  }
                  else{
                    reqAllCases['data'].sortColumn     = sortArray[sortColumn];
                    reqAllCases['data'].sortType   = sortOrder;
                  }
                  // reqAllCases.data.patientType   = 1;
                  this.oldCharacterCount     = 3;
                  // let url    = environment.API_USER_ENDPOINT + 'partnerusers?partnerId=1&pageNumber='+ this.paginatePage +'&pageSize=' + this.paginateLength;
                  // let url    = environment.API_SPECIMEN_ENDPOINT + 'showactivecases';

                  if (this.selectedIntakeIdForCases != null) {
                    reqAllCases['data'].sortColumn     = 'updatedTimestamp';
                    reqAllCases['data'].sortType   = 'desc';
                    reqAllCases['data']['statuses']            = [1,2,3,4,5,6,7,8,9,10,11,12];
                  }
                  var fromDash = history.state;
                  console.log('fromDash',fromDash);



                  if (typeof fromDash.searchBy != "undefined" ) {
                    if (fromDash.searchBy != null) {

                      this.greenColor = false;

                      if (fromDash.searchBy != 'specimentTypeIntake' && fromDash.searchBy != 'printed' && fromDash.searchBy != 'notPrinted') {
                        reqAllCases['data']['status'] = fromDash.searchBy;
                        if (fromDash.searchBy == 'Delivered' || fromDash.searchBy == 'Cancelled' || fromDash.searchBy == 'Not Delivered') {
                          $('#searchStatus2').val(fromDash.searchBy);
                        }
                        else{
                          $('#searchStatus').val(fromDash.searchBy);

                        }


                      }
                      // if (fromDash.searchBy == 'specimentTypeIntake') {
                      //   if (fromDash.specimenId != '') {
                      //     reqAllCases['data']['specimenType'] = fromDash.specimenId;
                      //
                      //   }
                      // }

                      if (fromDash.id !== null) {
                        reqAllCases['data']['clientId'] = fromDash.id;
                      }
                      if (fromDash.searchBy == 'printed') {
                        reqAllCases['data']['printed'] = '1';

                      }
                      if (fromDash.searchBy == 'notPrinted') {
                        reqAllCases['data']['printed'] = '0';

                      }
                      if (typeof fromDash.intakeId != "undefined" || fromDash.intakeId != null) {
                        reqAllCases['data']['intakeId'] = fromDash.intakeId;
                        $('#intakeIdFilter').val(fromDash.intakeId);
                      }
                      if (typeof fromDash.specimenId != "undefined") {
                        if (fromDash.specimenId != null && fromDash.specimenId != '') {
                          reqAllCases['data']['specimenType'] = fromDash.specimenId;
                          $('#searchSpecimenType').val(fromDash.specimenId);
                        }

                      }
                      if (fromDash.clientName !== null) {
                        reqAllCases['data']['clientName'] = fromDash.clientName;
                        $('#searchClient').val(fromDash.clientName);
                      }

                      if (typeof fromDash.creationDate != 'undefined') {
                        if (fromDash.creationDate !== null) {
                          reqAllCases['data']['createdTimestamp'] = fromDash.creationDate;
                        }
                      }

                      // this.fromDashboard(fromDash.searchBy,fromDash.id,intakeId,specimenId,fromDash.clientName).then(resDash => {
                      //   fromDash.searchBy = null;
                      //   console.log("******************************* fromdash",fromDash);
                      //
                      //   callback({
                      //     recordsTotal    :  this.activeCasesCount,
                      //     recordsFiltered :  this.activeCasesCount,
                      //     data: []
                      //   });
                      //
                      //   // return;
                      // });
                    }



                  }
                  // console.log("-----------sarmad-------------",url);
                  // console.log("-----------sarmad-------------",reqAllCases);
                  this.http.post(url,reqAllCases).subscribe( resp => {
                    // console.log('--------------------resp',resp['result']);
                    if (resp['result']['description'].indexOf('UnAuthorized') != -1) {
                      this.ngxLoader.stop();
                      this.notifier.notify('warning',resp['result']['description'])
                      this.router.navigateByUrl('/');
                    }

                    console.log('resp Active Cases 1: ', resp);
                    // if (fromDash.searchBy == 'Delivered' || fromDash.searchBy == 'Cancelled' || fromDash.searchBy == 'Not Delivered') {
                    //   // $('#all_casestab').tab('show');
                    //   this.activecasesTabPan  = false;
                    //   this.allcasesTabPan = true;
                    // }
                    // else{
                    //   // $('#active_casestab').tab('show');
                    //   this.activecasesTabPan  = true;
                    //   this.allcasesTabPan = false;
                    // }
                    if (resp['data']['cases'] != null) {
                      for (let i = 0; i < resp['data']['cases'].length; i++) {
                        if (typeof resp['data']['cases'][i]['batchId'] != 'undefined') {
                          if (resp['data']['cases'][i]['batchId'] != '' && resp['data']['cases'][i]['batchId'] != null) {
                            resp['data']['cases'][i]['batchIdToShow'] = resp['data']['cases'][i]['batchId'].split(',');
                            resp['data']['cases'][i]['batchIdToShowStatus'] = resp['data']['cases'][i]['batchStatusId'].split(',');
                          }
                        }

                      }
                      console.log("resp['data']['cases']",resp['data']);

                      this.activeCases = resp['data']['cases'];
                      this.activeCasesCount = resp['data']['totalCases'];
                      if (this.greenColor == true) {
                        this.activeCases[0].greenColor = true;
                      }

                      callback({
                        recordsTotal    :  this.activeCasesCount,
                        recordsFiltered :  this.activeCasesCount,
                        data: []
                      });
                      this.newAllCasesReq.data = {};


                      // fromDash.searchBy = null;


                    }
                    else{
                      callback({
                        recordsTotal    :  0,
                        recordsFiltered :  0,
                        data: []
                      });
                      this.newAllCasesReq.data = {};

                      // fromDash.searchBy = null;
                    }
                    this.notifier.hideAll();

                  },error => {

                    console.log("error: ",error);
                    this.notifier.notify( "error", "Error while loading active cases");
                  });

                // }

              }
              // console.log(`The DataTable ${index} instance ID is: ${dtInstance.table().node().id}`);
            });
          });

        }, 500)




      },
      // columns  : [
      //   { data : 'SPD-002' },
      //   { data : 'firstName' },
      //   { data : 'lastName' },
      //   { data : 'username' },
      //   { data : 'phone' },
      //   { data : 'email' },
      //   { data : 'action' }
      // ]
      ///for search
    };

    this.dtOptions[1] = {
      pagingType   : 'full_numbers',
      pageLength   : this.paginateLength,
      serverSide   : true,
      processing   : true,
      // ordering     : false,
      "order": [[ 12, "desc" ]],
      columnDefs: [
        { orderable: true, className: 'reorder', targets: [0,4,5,6,7,8,9,10, 'desc'] },
        { orderable: false, targets: [11] },
        {
          targets: [12],
          visible: false,
          searchable: false,
          orderable: true,
        },
      ],
      "lengthMenu": [20, 50, 75, 100 ],
      ajax: (dataTablesParameters: any, callback) => {
        if(this.timeCount1) {
          clearTimeout(this.timeCount1);
          // console.log("here",this.timeCount);

          this.timeCount1 = null;
        }
        this.timeCount1 = setTimeout(()=>{
          this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {

            dtElement.dtInstance.then((dtInstance: any) => {
              if(dtInstance.table().node().id == 'addall_cases'){
                console.log("dataTablesParameters--",dataTablesParameters);

                var sortColumn = dataTablesParameters.order[0]['column'];
                var sortOrder  = dataTablesParameters.order[0]['dir'];
                var sortArray  = ["caseNumber",'patientLastName','patientFirstName','patientDateOfBirth','clientName','intakeId','batchId','specimenType','collectionDate','collectedBy','caseStatusId','createdTimestamp','createdTimestamp']


                // if (dataTablesParameters) {
                // console.log("dataTablesParameters",dataTablesParameters);
                //   return;
                // }
                this.paginatePage           = dtElement['dt'].page.info().page;
                this.paginateLength         = dtElement['dt'].page.len();

                var columnToSearch          = "";
                var searchValue             = "";
                var caseNumValue            = "";
                var clientValue             = "";
                var collectDateValue        = "";
                var collectedByValue        = "";
                var caseStatusValue         = "";
                var intakeValue             = "";
                var specimenTypeId          = "";
                var reqAllCases = {}
                let url    = environment.API_ACDM_ENDPOINT + 'allcases';;
                reqAllCases = {...this.newAllCasesReq}
                // var sortColumn = dataTablesParameters.order[0]['column'];
                // var sortOrder  = dataTablesParameters.order[0]['dir'];
                // var sortArray  = ["patientId",'patientId','firstName','lastName','dateOfBirth','ssn','patientId','patientId','patientId','patientId','patientId','patientId']
                if (dataTablesParameters.columns[0].search.value != "") {
                  // console.log("*********",dataTablesParameters.columns[0].search.value);

                  if (dataTablesParameters.columns[0].search.value.length>=3 || dataTablesParameters.columns[0].search.value.length == 0) {
                    reqAllCases['data']['caseNumber'] = dataTablesParameters.columns[0].search.value;
                    searchValue = "true";
                    // columnToSearch = "searchbycasenumber"; searchValue = dataTablesParameters.columns[0].search.value;
                    // caseNumValue = dataTablesParameters.columns[0].search.value;

                  }
                  else{
                    return;
                  }
                }
                if (dataTablesParameters.columns[8].search.value != "") {
                  reqAllCases['data']['collectionDate'] = dataTablesParameters.columns[8].search.value;
                  searchValue = "true";
                  // columnToSearch = "searchbydate"; collectDateValue = dataTablesParameters.columns[8].search.value;       searchValue = dataTablesParameters.columns[8].search.value
                }
                if (dataTablesParameters.columns[9].search.value != "") {
                  reqAllCases['data']['collectedBy'] = dataTablesParameters.columns[9].search.value;
                  searchValue = "true";
                  // columnToSearch = "searchcollectedby"; collectedByValue = dataTablesParameters.columns[9].search.value;  searchValue = dataTablesParameters.columns[9].search.value
                }
                if (dataTablesParameters.columns[10].search.value != "") {
                  reqAllCases['data']['status'] = dataTablesParameters.columns[10].search.value;
                  searchValue = "true";
                  // columnToSearch = "searchbycasesstatus";
                  // caseStatusValue = dataTablesParameters.columns[10].search.value; searchValue = dataTablesParameters.columns[10].search.value;


                  // if (history.state.id != null || typeof history.state.id != 'undefined') {
                  //   if (dataTablesParameters.columns[4].search.value == '') {
                  //     reqAllCases['data']['status'] = dataTablesParameters.columns[0].search.value;
                  //
                  //     reqAllCases['data']['clientName'] = history.state.clientName;
                  //     // searchValue  = history.state.id;
                  //   }
                  //   else{
                  //     reqAllCases['data']['clientName'] = this.cNameforSearch;
                  //     // searchValue  = this.cNameforSearch;
                  //   }
                  //
                  // }
                }
                if (dataTablesParameters.columns[5].search.value != "") {
                  // columnToSearch = "searchbyintakeid"; intakeValue = dataTablesParameters.columns[5].search.value;
                  // searchValue = dataTablesParameters.columns[5].search.value;
                  reqAllCases['data']['intakeId'] = dataTablesParameters.columns[5].search.value;
                  searchValue = "true";

                  // if (history.state.intakeId != null || typeof history.state.intakeId != 'undefined') {
                  //   if (dataTablesParameters.columns[7].search.value == '') {
                  //     reqAllCases['data']['specimenType'] = history.state.specimenId;
                  //   }
                  //   else{
                  //     reqAllCases['data']['specimenType'] = dataTablesParameters.columns[7].search.value;
                  //   }
                  //
                  // }
                }
                if (dataTablesParameters.columns[7].search.value != "") {
                  reqAllCases['data']['specimenType'] = dataTablesParameters.columns[7].search.value;
                  searchValue = "true";
                  // columnToSearch = "searchbyspecimenid"; specimenTypeId = dataTablesParameters.columns[7].search.value; searchValue = dataTablesParameters.columns[7].search.value
                }
                // console.log("---------",this.cNameforSearch);
                // if (this.cNameforSearch != null) {
                if (dataTablesParameters.columns[4].search.value != "") {
                  reqAllCases['data']['clientName'] = dataTablesParameters.columns[4].search.value;
                  searchValue = "true";

                  // columnToSearch = "searchbyclientname"; clientValue = this.cNameforSearch;searchValue  = this.cNameforSearch
                }
                if (dataTablesParameters.columns[6].search.value != "") {
                  reqAllCases['data']['batchId'] = dataTablesParameters.columns[6].search.value;
                  searchValue = "true";

                  // columnToSearch = "searchbyclientname"; clientValue = this.cNameforSearch;searchValue  = this.cNameforSearch
                }
                // }

                if (dataTablesParameters.columns[1].search.value != "") {
                  // console.log("------------------------------------------------------",dataTablesParameters.columns[1].search.value);

                  // if (dataTablesParameters.columns[1].search.value.length>=3 || dataTablesParameters.columns[1].search.value.length == 0) {
                    reqAllCases['data']['patientLastName'] = dataTablesParameters.columns[1].search.value;
                    searchValue = "true";

                    // columnToSearch = "searchbylastname"; searchValue = dataTablesParameters.columns[1].search.value
                  // }
                  // else{
                  //   return;
                  // }
                }

                if (dataTablesParameters.columns[2].search.value != "") {
                  // if (dataTablesParameters.columns[2].search.value.length>=3 || dataTablesParameters.columns[2].search.value.length == 0) {
                    reqAllCases['data']['patientFirstName'] = dataTablesParameters.columns[2].search.value;
                    searchValue = "true";

                    // columnToSearch = "searchbyfirstname"; searchValue = dataTablesParameters.columns[2].search.value
                  // }
                  // else{
                  //   return;
                  // }

                }
                if (dataTablesParameters.columns[3].search.value != "") {
                  reqAllCases['data']['patientDateOfBirth'] = dataTablesParameters.columns[3].search.value;
                  searchValue = "true";

                  // columnToSearch = "searchbydob";searchValue = dataTablesParameters.columns[3].search.value
                }

                this.paginatePage           = dtElement['dt'].page.info().page;
                this.paginateLength         = dtElement['dt'].page.len();
                // return;
                // if (dataTablesParameters.search.value == "" && (this.oldCharacterCount == 0 || this.oldCharacterCount == 3) && searchValue != "") {
                //
                //
                // }

                // else if (dataTablesParameters.search.value == "" && (this.oldCharacterCount == 0 || this.oldCharacterCount == 3)) {
                if (dataTablesParameters.search.value != "" && (this.oldCharacterCount == 0 || this.oldCharacterCount == 3)) {
                  if (this.oldCharacterCount == 3) {
                  }
                  reqAllCases['data']['mainSearch'] = dataTablesParameters.search.value;
                }
                else{
                  $('.dataTables_processing').css('display',"none");
                  if (this.oldCharacterCount == 3) {
                    this.oldCharacterCount     = 3
                  }
                  else{
                    this.oldCharacterCount     = 2;
                  }
                }
                if (this.selectedIntakeIdForCases != null) {
                  // reqAllCases = {...this.intakeCasesReq}
                  // reqAllCases['data'].intakeId =  this.selectedIntakeIdForCases;
                  // url    = environment.API_SPECIMEN_ENDPOINT + 'showintakebyid';
                  reqAllCases['data']['intakeId'] = this.selectedIntakeIdForCases;
                  var addDeliveredForIntake = {
                    caseStatusId: 10,
                    caseStatusName: "Delivered",
                    createdBy: "1",
                    createdTimestamp: "10-19-2020",
                    partnerCode: "sip",
                    updatedBy: null,
                    updatedTimestamp: null,
                  }
                  const found = this.allStatusTypeForActive.some(el => el.caseStatusName === "Delivered");
                  if (found == false) {
                    this.allStatusTypeForActive.push(addDeliveredForIntake);
                    this.allStatusTypeForActive         = this.sortPipe.transform(this.allStatusTypeForActive, "asc", "caseStatusName");
                  }


                }
                reqAllCases['data']['pageNumber']           = dtElement['dt'].page.info().page;
                reqAllCases['data']['pageSize']            = dtElement['dt'].page.len();
                reqAllCases['data']['statuses']            = [1,2,3,4,5,6,7,8,9,10,11,12];
                reqAllCases['header'].partnerCode  = this.logedInUserRoles.partnerCode;
                reqAllCases['header'].userCode     = this.logedInUserRoles.userCode;
                reqAllCases['header'].functionalityCode     = "SPCA-ALC";
                // if (sortColumn != 1 && sortColumn != 2 && sortColumn != 3) {
                  if (this.greenColor == true) {
                    if (this.saveTypeGlobal == 0) {
                      reqAllCases['data'].sortColumn     = 'caseId';
                      reqAllCases['data'].sortType   = 'desc';
                      // reqAllCases.data.sortColumn     = sortArray[sortColumn];
                      // reqAllCases.data.sortingOrder   = sortOrder;
                    }
                    else{
                      reqAllCases['data'].sortColumn     = 'caseId';
                      reqAllCases['data'].sortType   = 'desc';
                    }
                    // reqAllCases.data.sortColumn     = 'caseId';
                    // reqAllCases.data.sortingOrder   = 'desc';
                  }
                  else{
                    reqAllCases['data'].sortColumn     = sortArray[sortColumn];
                    reqAllCases['data'].sortType   = sortOrder;
                  }
                  // reqAllCases.data.patientType   = 1;
                  this.oldCharacterCount     = 3;
                  // let url    = environment.API_USER_ENDPOINT + 'partnerusers?partnerId=1&pageNumber='+ this.paginatePage +'&pageSize=' + this.paginateLength;
                  // let url    = environment.API_SPECIMEN_ENDPOINT + 'showactivecases';

                  if (this.selectedIntakeIdForCases != null) {
                    reqAllCases['data'].sortColumn     = 'updatedTimestamp';
                    reqAllCases['data'].sortType   = 'desc';
                    reqAllCases['data']['statuses']            = [1,2,3,4,5,6,7,8,9,10,11,12];
                  }

                  var fromDash = history.state;


                  if (typeof fromDash.searchBy != "undefined" ) {
                    if (fromDash.searchBy != null) {


                      this.greenColor = false;
                      // reqAllCases['data']['status'] = fromDash.searchBy;
                      if (fromDash.searchBy != 'specimentTypeIntake' && fromDash.searchBy != 'printed' && fromDash.searchBy != 'notPrinted') {
                        reqAllCases['data']['status'] = fromDash.searchBy;
                        console.log('fromDash.searchBy111',fromDash.searchBy);

                        $('#searchStatus2').val(fromDash.searchBy);

                      }
                      if (fromDash.id !== null) {
                        reqAllCases['data']['clientId'] = fromDash.id;
                      }
                      if (fromDash.searchBy == 'printed') {
                        reqAllCases['data']['printed'] = '1';

                      }
                      if (fromDash.searchBy == 'notPrinted') {
                        reqAllCases['data']['printed'] = '0';

                      }
                      if (typeof fromDash.intakeId != "undefined" || fromDash.intakeId != null) {
                        reqAllCases['data']['intakeId'] = fromDash.intakeId;
                        $('#intakeIdFilter2').val(fromDash.intakeId);
                      }
                      if (typeof fromDash.specimenId != "undefined") {
                        if (fromDash.specimenId != null && fromDash.specimenId != '') {
                          reqAllCases['data']['specimenType'] = fromDash.specimenId;
                          $('#searchSpecimenType2').val(fromDash.specimenId);
                        }

                      }
                      if (fromDash.clientName !== null) {
                        reqAllCases['data']['clientName'] = fromDash.clientName;
                        $('#searchClient2').val(fromDash.clientName);
                      }
                      if (typeof fromDash.creationDate != 'undefined') {
                        if (fromDash.creationDate !== null) {
                          reqAllCases['data']['createdTimestamp'] = fromDash.creationDate;
                        }
                      }

                    }



                  }
                  // console.log("-----------sarmad-------------",url);
                  // console.log("-----------sarmad-------------",reqAllCases);
                  this.http.post(url,reqAllCases).subscribe( resp => {
                    if (resp['result']['description'].indexOf('UnAuthorized') != -1) {
                      this.ngxLoader.stop();
                      this.notifier.notify('warning',resp['result']['description'])
                      this.router.navigateByUrl('/');
                    }
                    // console.log('--------------------selectedIntakeIdForCases',this.selectedIntakeIdForCases);

                    console.log('resp Active Cases 1: ', resp);
                    console.log("fromDash.searchBy",fromDash.searchBy);


                    if (resp['data']['cases'] != null) {
                      for (let i = 0; i < resp['data']['cases'].length; i++) {
                        if (typeof resp['data']['cases'][i]['batchId'] != 'undefined') {
                          if (resp['data']['cases'][i]['batchId'] != '' && resp['data']['cases'][i]['batchId'] != null) {
                            resp['data']['cases'][i]['batchIdToShow'] = resp['data']['cases'][i]['batchId'].split(',');
                          }
                        }


                      }
                      this.allCases = resp['data']['cases'];
                      this.allCasesCount = resp['data']['totalCases'];
                      setTimeout(()=>{
                        if (typeof fromDash.searchBy != undefined) {
                          if (fromDash.searchBy == 'Delivered' || fromDash.searchBy == 'Cancelled' || fromDash.searchBy == 'Not Delivered' || fromDash.searchBy == 'specimentTypeIntake') {
                            // console.log('innnn',fromDash.searchBy);

                            $('#all_casestab').show();
                            $('#active_casestab').hide();
                            this.activecasesTabPan  = false;
                            this.allcasesTabPan = true;
                            // $('#all_casestab').trigger('click')
                          }
                          else{
                            $('#all_casestab').hide();
                            $('#active_casestab').show();
                            this.activecasesTabPan  = true;
                            this.allcasesTabPan = false;
                            // $('#active_casestab').trigger('click')
                          }
                        }



                      }, 1000)

                      if (this.greenColor == true) {
                        this.allCasesCount[0].greenColor = true;
                      }

                      callback({
                        recordsTotal    :  this.allCasesCount,
                        recordsFiltered :  this.allCasesCount,
                        data: []
                      });
                      this.newAllCasesReq.data = {};

                      // fromDash.searchBy = null;


                    }
                    else{
                      callback({
                        recordsTotal    :  0,
                        recordsFiltered :  0,
                        data: []
                      });
                      this.newAllCasesReq.data = {};

                      // fromDash.searchBy = null;
                    }

                  },error => {

                    console.log("error: ",error);
                    this.notifier.notify( "error", "Error while loading all cases");
                  });;
                // }


                // else{
                //   if (dataTablesParameters.search.value.length >=3) {
                //
                //     var columnUrl     = environment.API_ACDM_ENDPOINT + 'allcases';
                //     var columnCaseReq  = {...this.newAllCasesReq};
                //     columnCaseReq['data']['pageNumber']           = dtElement['dt'].page.info().page;
                //     columnCaseReq['data']['pageSize']             = dtElement['dt'].page.len();
                //     columnCaseReq['data']['statuses']             = [1,2,3,4,5,6,7,8,9,12];
                //     columnCaseReq['data']['mainSearch']           = dataTablesParameters.search.value;
                //     columnCaseReq['header'].partnerCode  = this.logedInUserRoles.partnerCode;
                //     columnCaseReq['header'].userCode     = this.logedInUserRoles.userCode;
                //     columnCaseReq['header'].functionalityCode     = "SPCA-MC";
                //
                //     this.http.post(columnUrl,columnCaseReq).toPromise()
                //     .then( resp => {
                //       // console.log('--------------------selectedIntakeIdForCases',this.selectedIntakeIdForCases);
                //
                //       console.log('resp Active Cases Search: ', resp);
                //       for (let i = 0; i < resp['data']['cases'].length; i++) {
                //         if (resp['data']['cases'][i]['batchId'] != '') {
                //           resp['data']['cases'][i]['batchId'] = resp['data']['cases'][i]['batchId'].split(',');
                //         }
                //
                //       }
                //       this.activeCases = resp['data']['cases'];
                //       this.activeCasesCount = resp['data']['totalCases'];
                //       callback({
                //         recordsTotal    :  this.activeCasesCount,
                //         recordsFiltered :  this.activeCasesCount,
                //         data: []
                //       });
                //     })
                //     .catch(error => {
                //
                //       console.log("error: ",error);
                //       this.notifier.notify( "error", "Error while loading active cases");
                //     });
                //
                //
                //
                //   }
                //   else{
                //     $('.dataTables_processing').css('display',"none");
                //     if (this.oldCharacterCount == 3) {
                //       this.oldCharacterCount     = 3
                //     }
                //     else{
                //       this.oldCharacterCount     = 2;
                //     }
                //   }
                // }

                }

                // console.log(`The DataTable ${index} instance ID is: ${dtInstance.table().node().id}`);
              });
            });


          }, 500)


        },
        // columns  : [
        //   { data : 'SPD-002' },
        //   { data : 'firstName' },
        //   { data : 'lastName' },
        //   { data : 'username' },
        //   { data : 'phone' },
        //   { data : 'email' },
        //   { data : 'action' }
        // ]
        ///for search
      };

      //////////////////////////// Eligible Patients

    }
    // f1() {
    //     return new Promise((resolve, reject) => {
    //         console.log('f1');
    //         resolve();
    //     });
    // }
    //
    // f2() {
    //    console.log('f2');
    // }
    // patientSortSearch(criteriaUrl,searchCriteria,columnUrl,columnCaseReq,type,value){
    //   return new Promise((resolve, reject) =>{
    //
    //   })
    // }


    megaSearch(clientSearchUrl,patienttSearchUrl,specimenSearchUrl,clientReq,patitentReq,casesReq,type,searchString) {
      this.ngxLoader.start();
      return new Promise((resolve, reject) => {
        var clientResult = this.http.post(clientSearchUrl,clientReq).toPromise().then(clientresp=>{
          return clientresp['data'];
        })
        var patientResult = this.http.put(patienttSearchUrl,patitentReq).toPromise().then(patientresp=>{
          // console.log("patientresp",patientresp);
          var holder = {data:{
            patients:[]
          }}
          if (patientresp['data'].length > 0) {
            holder.data.patients = patientresp['data'];
          }
          else{
            holder.data.patients = null;
          }
          return holder;
        })
        var casesResult = this.http.post(specimenSearchUrl,casesReq).toPromise().then(casesresp=>{
          return casesresp;
        })
        forkJoin([clientResult, patientResult, casesResult]).subscribe(allresults => {
          var client  = allresults[0];
          var patient = allresults[1];
          var casses  = allresults[2];

          console.log("client",client);
          console.log("patient",patient);
          console.log("casses",casses);

          //////////////////////////////////////////
          /////////// Scenario 1 all 0/////////////
          ///////////////////////////////////////
          if (client['length'] == 0 && patient['data']['patients'] == null && casses['data']['caseDetails']['length'] == 0) {
            console.log('ALL Zero');
            if (type == 'active') {
              this.activeCasesCount = casses['data']['caseDetails']['length'];
              this.activeCases = casses['data']['caseDetails'];
            }
            else{
              this.allCasesCount = casses['data']['caseDetails']['length'];
              this.allCases = casses['data']['caseDetails'];
            }

            this.ngxLoader.stop();
            resolve();
          }
          //////////////////////////////////////////
          /////////// Scenario 2 Casess 0/////////////
          ///////////////////////////////////////
          else if(client['length'] > 0 || patient['data']['patients'] != null || casses['data']['caseDetails']['length'] > 0){


            this.getMegaSearchPatientClientRecord(casses,client,patient,type,searchString).then(megapatclientresp=>{
              resolve();
            })
          }

          //////////////////////////////////////////
          /////////// Scenario 3 Casess > 0 and all >0 /////////////
          ///////////////////////////////////////
          // else if(client['length'] > 0 && patient['data']['patients']['length'] > 0 && casses['data']['caseDetails']['length'] > 0){
          //
          //   this.getMegaSearchPatientClientRecord(casses,client,patient,type).then(megapatclientresp=>{
          //     resolve();
          //   })
          // }

        })
        // console.log('f1');
        // resolve();
      });
    }


    getPatClData(userids,resp,clientids,type) {
      // this.ngxLoader.start();
      const patienByidURL = environment.API_PATIENT_ENDPOINT + 'patientbyids';
      var getPatReq       = {...this.patientByIdsReq}
      getPatReq.header.partnerCode  = this.logedInUserRoles.partnerCode;
      getPatReq.header.userCode     = this.logedInUserRoles.userCode;
      getPatReq.header.functionalityCode     = "SPCA-MC";
      var clURL           = environment.API_CLIENT_ENDPOINT + "clientidsnames"
      var clReqData = {
        header:{
          uuid               :"",
          partnerCode        :"",
          userCode           :"",
          referenceNumber    :"",
          systemCode         :"",
          moduleCode         :"CLIM",
          functionalityCode  :"SPCA-MC",
          systemHostAddress  :"",
          remoteUserAddress  :"",
          dateTime           :""
        },
        data:{
          clientIds:clientids
        }
      }
      clReqData['header'].partnerCode = this.logedInUserRoles.partnerCode;
      clReqData['header'].userCode = this.logedInUserRoles.userCode;
      clReqData['header'].functionalityCode = "SPCA-MC";
      // var clReqData       = {clientIds:clientids}
      getPatReq.data.patientIds = userids;
      return new Promise((resolve, reject) => {
        var patientout = this.specimenService.getPatientsByIds(patienByidURL, getPatReq).then(patient=>{
          return patient;
        })
        var clientsout = this.specimenService.getClientByName(clURL,clReqData).then(selectedClient => {
          // console.log('selectedClient[data]',selectedClient['data']);

          return selectedClient['data'];
        });
        forkJoin([patientout, clientsout]).subscribe(bothresults => {
          // console.log('bothresults FOrk Join------',bothresults);
          var patientresp = bothresults[0];
          var clients      = bothresults[1];
          // console.log("resp['data']---------",resp['data']);

          if (patientresp['result'].codeType == "S") {
            if (typeof resp['data']['caseDetails'] != "undefined") {
              for (let i = 0; i < resp['data']['caseDetails'].length; i++) {
                for (let j = 0; j < patientresp['data'].length; j++) {
                  // console.log("loop main data",resp['data']['caseDetails'][i]);
                  //   console.log("loop patient data",patientresp['data'][j]);
                  if (resp['data']['caseDetails'][i].patientId == patientresp['data'][j].patientId) {
                    resp['data']['caseDetails'][i].pFName = patientresp['data'][j].firstName;
                    resp['data']['caseDetails'][i].pLName = patientresp['data'][j].lastName;
                    resp['data']['caseDetails'][i].pDob   = patientresp['data'][j].dateOfBirth;
                  }
                  else{
                    // console.log("Elseeeeee",resp['data']['caseDetails'][i]);
                  }

                }

              }
              // this.allCases       = resp['data']['caseDetails'];
              // this.allCasesCount  = resp['data']['totalCount'];
            }
            else{

              for (let i = 0; i < resp['data'].length; i++) {
                for (let j = 0; j < patientresp['data'].length; j++) {
                  if (resp['data'][i].patientId == patientresp['data'][j].patientId) {
                    resp['data'][i].pFName = patientresp['data'][j].firstName;
                    resp['data'][i].pLName = patientresp['data'][j].lastName;
                    resp['data'][i].pDob   = patientresp['data'][j].dateOfBirth;
                  }

                }

              }
            }
          }
          else{
            this.notifier.notify( "error", "Error while fetching patients" );
            // return;
          }

          if (clients['length'] > 0) {
            if (typeof resp['data']['caseDetails'] != "undefined") {
              // for (let i = 0; i < resp['data']['caseDetails'].length; i++) {
              for (let i = 0; i < resp['data']['caseDetails'].length; i++) {
                for (let j = 0; j < clients['length']; j++) {
                  // console.log("loop main Data Client Id",resp['data']['caseDetails'][i].clientId);
                  //   console.log("loop patientData Client ID",patientresp['data'][j].clientId);
                  if (resp['data']['caseDetails'][i].clientId   == clients[j].clientId) {
                    resp['data']['caseDetails'][i].clientName   = clients[j].clientName;
                  }

                }

              }
            }
            else{
              // for (let i = 0; i < resp['data']['caseDetails'].length; i++) {
              for (let i = 0; i < resp['data'].length; i++) {
                for (let j = 0; j < clients['length']; j++) {
                  if (resp['data'][i].clientId   == clients[j].clientId) {
                    resp['data'][i].clientName   = clients[j].clientName;
                  }

                }

              }
            }

          }
          else{
            // this.notifier.notify( "error", "No Client Found" );
            // return;
          }
          if (type == 'active') {
            console.log("resp['data']",resp['data']);

            if (typeof resp['data']['caseDetails'] != "undefined") {
              console.log("resp['data']['totalCount']",resp['data']['totalCount']);

              this.activeCases       = resp['data']['caseDetails'];
              this.activeCasesCount  = resp['data']['totalCount'];
              // resolve();
            }
            else{
              console.log('-----',resp['data']);

              this.activeCases       = resp['data'];
              this.activeCasesCount  = resp['data']['length'];
            }
          }
          else{
            if (typeof resp['data']['caseDetails'] != "undefined") {
              this.allCases       = resp['data']['caseDetails'];
              this.allCasesCount  = resp['data']['totalCount'];
            }
            else{
              this.allCases       = resp['data'];
              this.allCasesCount  = resp['data']['length'];
            }
          }



          resolve();
        });
        // resolve();
      });
    }
    //////////////////when case = 0 but other are there
    getMegaSearchPatientClientRecord(caseData,clientData, patientDat, type,searchString){
      var casesDataBlueprint = { caseNumber: 'null', caseId: 'null', pFName: 'null', pLName: 'null', pDob: 'null', clientName: 'null', collectionDate: 'null', collectedBy: 'null', caseStatusId: 'null', }
      return new Promise((resolve, reject) => {
        this.ngxLoader.start();
        ////////// check if patien is greatr or client
        ////////// if patient is great then search for client ids in pat is exist then fine
        ///////// else need to find cases bases of patient id
        // if (searchString.toLowerCase() == 'client' || searchString.toLowerCase() == 'sip') {
        //   var a = caseData['data']['caseDetails'];
        //   const uniqueIds = [];
        //   const uniqueCLIds = [];
        //   const map = new Map();
        //   for (const item of a) {
        //     if(!map.has(item.clientId)){
        //       map.set(item.clientId, true);    // set any value to Map
        //       uniqueCLIds.push(item.clientId);
        //     }
        //   }
        //   const map1 = new Map();
        //   for (const item of a) {
        //     if(!map1.has(item.patientId)){
        //       map1.set(item.patientId, true);    // set any value to Map
        //       uniqueIds.push(item.patientId);
        //     }
        //   }
        //
        //   // getPatReq.data.patientIds = uniqueIds;
        //   this.getPatClData(uniqueIds,caseData,uniqueCLIds,type).then(respAlldata => {
        //     resolve()
        //
        //
        //   });
        // }
        // else{
        if (clientData['length'] > 0 && patientDat['data']['patients'] != null && caseData['data']['caseDetails']['length'] == 0) {
          /////////////// if client has mo data Yaha pe new endpoint ko call karni he

          if (clientData['length'] > patientDat['data']['patients'].length) {
            var uniqIdPatient = []
            var uniqueClientId = [];
            const map1 = new Map();
            for (const item of clientData) {
              if(!map1.has(item.clientId)){
                map1.set(item.clientId, true);    // set any value to Map
                uniqueClientId.push(item.clientId);
              }
            }

            const map = new Map();
            for (const item of patientDat['data']['patients']) {
              if(!map.has(item.patientId)){
                map.set(item.patientId, true);    // set any value to Map
                uniqIdPatient.push(item.patientId);

              }
            }
            var intakeForSearch = ''
            // var clientForSearch = ''
            if (this.selectedIntakeIdForCases != null) {
              intakeForSearch = this.selectedIntakeIdForCases
              // clientForSearch = this.selectedClientIdForCases
            }
            else{
              intakeForSearch = ""
              // clientForSearch = ""
            }

            var columnUrl      = environment.API_SPECIMEN_ENDPOINT + 'searchbyclient';
            var columnCaseReq      = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-MC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},
            data:{page:"0",size:20,sortColumn:"caseNumber",sortingOrder:"desc",
            clientId: uniqueClientId,
            intakeId: intakeForSearch,
            statuses:[],
            userCode:""
          }}
          this.http.post(columnUrl, columnCaseReq).subscribe(byClientIdsResp=>{
            console.log("---byClientIdsResp",byClientIdsResp);
            //////////////////////////////// If no case found for clients
            if (byClientIdsResp['data']['caseDetails'].length > 0) {
              for (let i = 0; i < byClientIdsResp['data']['caseDetails'].length; i++) {
                for (let j = 0; j < patientDat['data']['patients'].length; j++) {
                  if (byClientIdsResp['data']['caseDetails'][i].patientId == patientDat['data']['patients'][j].patientId) {
                    if(uniqIdPatient.indexOf(byClientIdsResp['data']['caseDetails'][i].patientId) !== -1){
                      // alert("Value exists!")
                    } else{
                      uniqIdPatient.push(byClientIdsResp['data']['caseDetails'][i].patientId);
                    }
                  }
                }
              }

              var tempH    = {}
              var tempHold = []
              var intakeForSearch = ''
              var clientForSearch = ''
              var statuses  = [];
              if (this.selectedIntakeIdForCases != null) {
                intakeForSearch = this.selectedIntakeIdForCases
                clientForSearch = this.selectedClientIdForCases
                statuses        = [3,2,6,12,10,8,9,7,4,5]
              }
              else{
                intakeForSearch = ""
                clientForSearch = ""
                statuses  = []
              }
              var specUrl = environment.API_SPECIMEN_ENDPOINT + "findbypatientid"
              var reqPid  ={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-MC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{
                patientIds:uniqIdPatient,intakeId:intakeForSearch,client:clientForSearch,statuses:statuses, userCode:""
              }}
              this.specimenService.findByPatientIds(specUrl, reqPid).subscribe(casesBypatientIds =>{
                console.log("*******casesBypatientIds",casesBypatientIds);
                uniqueClientId = []

                for (var c = 0; c < byClientIdsResp['data']['caseDetails'].length; c++) {
                  for (let d = 0; d < casesBypatientIds['data'].length; d++) {
                    if (byClientIdsResp['data']['caseDetails'][c].patientId == casesBypatientIds['data'][d].patientId) {

                    }
                    else{
                      if(uniqIdPatient.indexOf(casesBypatientIds['data'][d].patientId) !== -1){
                        // alert("Value exists!")
                      } else{
                        uniqIdPatient.push(casesBypatientIds['data'][d].patientId);
                      }
                    }
                    // if (byClientIdsResp['data']['caseDetails'][c].caseId == casesBypatientIds['data'].caseId) {
                    //
                    // }
                    // else{
                    //   tempH[c] = casesBypatientIds['data'][d];
                    //   if (tempH[c].patientId == byClientIdsResp['data']['caseDetails'][c].patientId) {
                    //     tempH[c].pFName = casesBypatientIds['data'][d].firstName;
                    //     tempH[c].pLName = casesBypatientIds['data'][d].lastName;
                    //     tempH[c].pDob   = casesBypatientIds['data'][d].dateOfBirth;
                    //   }
                    //   tempHold.push(tempH);
                    //   tempH = {}
                    // }




                  }
                }
                var patientIdforName = [];
                const map2 = new Map();
                for (const item of byClientIdsResp['data']['caseDetails']) {
                  if(!map2.has(item.patientId)){
                    map2.set(item.patientId, true);    // set any value to Map
                    patientIdforName.push(item.patientId);

                  }
                }

                var uniqClientNames = [];
                const map3 = new Map();
                for (const item of byClientIdsResp['data']['caseDetails']) {
                  if(!map3.has(item.clientId)){
                    map3.set(item.clientId, true);    // set any value to Map
                    uniqClientNames.push(item.clientId);

                  }
                }
                const patienByidURL = environment.API_PATIENT_ENDPOINT + 'patientbyids';
                var getPatReq       = {...this.patientByIdsReq}
                getPatReq.data.patientIds = patientIdforName;
                getPatReq.header.partnerCode  = this.logedInUserRoles.partnerCode;
                getPatReq.header.userCode     = this.logedInUserRoles.userCode;
                getPatReq.header.functionalityCode     = "SPCA-MC";

                this.http.put(patienByidURL, getPatReq).subscribe(forPatientName=>{
                  console.log("forPatientName",forPatientName);

                  for (let index = 0; index < byClientIdsResp['data']['caseDetails'].length; index++) {
                    for (let i = 0; i < forPatientName['data'].length; i++) {
                      if (byClientIdsResp['data']['caseDetails'][index].patientId == forPatientName['data'][i].patientId) {

                        // this.activeCases = [];
                        byClientIdsResp['data']['caseDetails'][index].pFName = forPatientName['data'][i].firstName;
                        byClientIdsResp['data']['caseDetails'][index].pLName = forPatientName['data'][i].lastName;
                        byClientIdsResp['data']['caseDetails'][index].pDob   = forPatientName['data'][i].dateOfBirth;
                        // TempTableHolder.push(casesBypatientIds['data'][c]);
                        // uniqueClientId.push(casesBypatientIds['data'][c].clientId)


                      }
                      else{
                        if (typeof byClientIdsResp['data']['caseDetails'][index].pFName == 'undefined') {
                          byClientIdsResp['data']['caseDetails'][index].pFName = "no Name";
                          byClientIdsResp['data']['caseDetails'][index].pLName = "no Name";
                          byClientIdsResp['data']['caseDetails'][index].pDob   = "02-02-2021 19:10:00";
                        }

                        // console.log("Id Doesnot match",casesBypatientIds['data'][c].patientId);
                        // casesBypatientIds['data'].splice(c, 1);
                        // caselen=casesBypatientIds['data'];

                      }

                    }

                  }
                  console.log("byClientIdsResp['data']['caseDetails']",byClientIdsResp['data']['caseDetails']);

                  // const combined2 = [...tempHold, ...byClientIdsResp['data']['caseDetails']];
                  // console.log("combined2",combined2);
                  var clURL           = environment.API_CLIENT_ENDPOINT + "clientidsnames"
                  // var clReqData       = {clientIds:uniqClientNames}
                  var clReqData = {
                    header:{
                      uuid               :"",
                      partnerCode        :"",
                      userCode           :"",
                      referenceNumber    :"",
                      systemCode         :"",
                      moduleCode         :"CLIM",
                      functionalityCode  :"SPCA-MC",
                      systemHostAddress  :"",
                      remoteUserAddress  :"",
                      dateTime           :""
                    },
                    data:{
                      clientIds:uniqClientNames
                    }
                  }
                  clReqData['header'].partnerCode = this.logedInUserRoles.partnerCode;
                  clReqData['header'].userCode = this.logedInUserRoles.userCode;
                  clReqData['header'].functionalityCode = "SPCA-MC";
                  this.specimenService.getClientByName(clURL,clReqData).then(selectedClient => {
                    // console.log("selectedClient",selectedClient);
                    for (let f = 0; f < selectedClient['data']['length']; f++) {
                      for (let e = 0; e <  byClientIdsResp['data']['caseDetails'].length; e++) {

                        if ( byClientIdsResp['data']['caseDetails'][e].clientId == selectedClient['data'][f].clientId) {
                          // console.log("in if");
                          byClientIdsResp['data']['caseDetails'][e].clientName = selectedClient['data'][f].clientName;

                          // casesBypatientIds['data'].map((i) => { i.clientName = selectedClient[f].name; return i; });
                        }
                        else{
                          if (typeof byClientIdsResp['data']['caseDetails'][e].clientName == 'undefined') {
                            byClientIdsResp['data']['caseDetails'][e].clientName ="No Name"
                          }
                          // console.log("in else");

                          // casesBypatientIds['data'][e].clientName="No Name"
                        }

                      }

                    }
                    // var finalData = [];
                    var finalDataCount = 0;
                    for (let index = 0; index < byClientIdsResp['data']['caseDetails']['length']; index++) {
                      finalDataCount = finalDataCount +1
                      // console.log("byClientIdsResp['data']['caseDetails'][index]---",byClientIdsResp['data']['caseDetails'][index]);


                      if (byClientIdsResp['data']['caseDetails'][index]['pFName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || byClientIdsResp['data']['caseDetails'][index]['pLName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || this.datePipe.transform( byClientIdsResp['data']['caseDetails'][index]['pDob'].toLowerCase(), 'MM/dd/yyyy').indexOf(searchString.toLowerCase()) !== -1 || byClientIdsResp['data']['caseDetails'][index]['clientName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || this.datePipe.transform( byClientIdsResp['data']['caseDetails'][index]['collectionDate'].toLowerCase(), 'MM/dd/yyyy').indexOf(searchString.toLowerCase()) !== -1) {

                        if (searchString.toLowerCase() == 'client' || searchString.toLowerCase() == 'sip') {
                          if (byClientIdsResp['data']['caseDetails'][index]['pFName'].toLowerCase().indexOf('client') === -1
                          && byClientIdsResp['data']['caseDetails'][index]['pLName'].toLowerCase().indexOf('client') === -1
                          && byClientIdsResp['data']['caseDetails'][index]['clientName'].toLowerCase().indexOf('client') === -1) {
                            byClientIdsResp['data']['caseDetails'] = byClientIdsResp['data']['caseDetails'].filter(function(value, index, arr){
                              // console.log("value.collectedBy",value.collectedBy);
                              return value.collectedBy == 2;
                            });

                          }
                          else if (byClientIdsResp['data']['caseDetails'][index]['pFName'].toLowerCase().indexOf('sip') === -1
                          && byClientIdsResp['data']['caseDetails'][index]['pLName'].toLowerCase().indexOf('sip') === -1
                          && byClientIdsResp['data']['caseDetails'][index]['clientName'].toLowerCase().indexOf('sip') === -1) {
                            byClientIdsResp['data']['caseDetails'] = byClientIdsResp['data']['caseDetails'].filter(function(value, index, arr){
                              // console.log("value.collectedBy",value.collectedBy);
                              return value.collectedBy == 1;
                            });
                          }
                          if (type == 'active') {
                            this.activeCases.push(byClientIdsResp['data']['caseDetails'][index]);

                          }
                          else if(type == 'all'){
                            this.allCases.push(byClientIdsResp['data']['caseDetails'][index]);
                          }
                        }
                        else{
                          if (type == 'active') {
                            this.activeCases.push(byClientIdsResp['data']['caseDetails'][index]);

                          }
                          else if(type == 'all'){
                            this.allCases.push(byClientIdsResp['data']['caseDetails'][index]);
                          }
                        }
                        // if (type == 'active') {
                        //   this.activeCases.push(byClientIdsResp['data']['caseDetails'][index]);
                        //
                        // }
                        // else if(type == 'all'){
                        //   this.allCases.push(byClientIdsResp['data']['caseDetails'][index]);
                        // }
                      }



                      if (finalDataCount == byClientIdsResp['data']['caseDetails']['length']) {
                        // console.log("this.activeCases------",this.activeCases);

                        if (type == 'active') {
                          // this.activeCases = [];
                          this.activeCasesCount = this.activeCases['length'];
                          // this.activeCases = finalData;
                          this.ngxLoader.stop();
                          resolve()
                        }
                        else if(type == 'all'){
                          // this.allCases = [];
                          this.allCasesCount = this.allCases['length'];
                          // this.allCases = finalData;
                          this.ngxLoader.stop();
                          resolve()
                        }


                      }
                    }
                    // console.log("finalData",finalData);
                    //
                    //
                    // if (type == 'active') {
                    //   // this.activeCases = [];
                    //   this.activeCasesCount =finalData['length'];
                    //   this.activeCases = finalData;
                    //   this.ngxLoader.stop();
                    //   resolve()
                    // }
                    // else if(type == 'all'){
                    //   // this.allCases = [];
                    //   this.allCasesCount = finalData['length'];
                    //   this.allCases = finalData;
                    //   this.ngxLoader.stop();
                    //   resolve()
                    // }

                  });

                })

              })
            }
            else{
              /////////////////////////// if no cases found in client sthen we look in patient
              for (let i = 0; i < byClientIdsResp['data']['caseDetails'].length; i++) {
                for (let j = 0; j < patientDat['data']['patients'].length; j++) {
                  if (byClientIdsResp['data']['caseDetails'][i].patientId == patientDat['data']['patients'][j].patientId) {
                    if(uniqIdPatient.indexOf(byClientIdsResp['data']['caseDetails'][i].patientId) !== -1){
                      // alert("Value exists!")
                    } else{
                      uniqIdPatient.push(byClientIdsResp['data']['caseDetails'][i].patientId);
                    }
                  }
                }
              }

              var tempH    = {}
              var tempHold = []
              var intakeForSearch = ''
              var clientForSearch = ''
              var statuses        = []
              if (this.selectedIntakeIdForCases != null) {
                intakeForSearch = this.selectedIntakeIdForCases
                clientForSearch = this.selectedClientIdForCases
                statuses        = [3,2,6,12,10,8,9,7,4,5]
              }
              else{
                intakeForSearch = ""
                clientForSearch = ""
                statuses        = []
              }
              var specUrl = environment.API_SPECIMEN_ENDPOINT + "findbypatientid"
              var reqPid1  ={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-MC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{
                patientIds:uniqIdPatient,intakeId:intakeForSearch,client:clientForSearch,statuses:statuses, userCode:""}}
                this.specimenService.findByPatientIds(specUrl, reqPid1).subscribe(casesBypatientIds =>{
                  console.log("*******casesBypatientIds",casesBypatientIds);
                  if (casesBypatientIds['data'].length == 0) {
                    if (caseData['data']['caseDetails']['length'] >0) {
                      console.log("hereeeeeeeee---");

                      var a = caseData['data']['caseDetails'];
                      const uniqueIds = [];
                      const uniqueCLIds = [];
                      const map = new Map();
                      for (const item of a) {
                        if(!map.has(item.clientId)){
                          map.set(item.clientId, true);    // set any value to Map
                          uniqueCLIds.push(item.clientId);
                        }
                      }
                      const map1 = new Map();
                      for (const item of a) {
                        if(!map1.has(item.patientId)){
                          map1.set(item.patientId, true);    // set any value to Map
                          uniqueIds.push(item.patientId);
                        }
                      }

                      // getPatReq.data.patientIds = uniqueIds;
                      this.getPatClData(uniqueIds,caseData,uniqueCLIds,type).then(respAlldata => {
                        resolve()


                      });
                    }
                    else{
                      if (type == 'active') {
                        this.activeCases = [];
                        this.activeCasesCount = 0;
                        // this.activeCases = finalData;
                        this.ngxLoader.stop();
                        resolve()
                        return
                      }
                      else if(type == 'all'){
                        this.allCases = [];
                        this.allCasesCount = 0;
                        // this.allCases = finalData;
                        this.ngxLoader.stop();
                        resolve()
                        return
                      }
                    }

                  }
                  uniqueClientId = []

                  // for (var c = 0; c< byClientIdsResp['data']['caseDetails'].length; c++) {
                  //   for (let d = 0; d < casesBypatientIds['data'].length; d++) {
                  //     // if (byClientIdsResp['data']['caseDetails'][c].patientId == casesBypatientIds['data'][j].patientId) {
                  //     //
                  //     // }
                  //     // else{
                  //     //   if(uniqIdPatient.indexOf(casesBypatientIds['data'][d].patientId) !== -1){
                  //     //     // alert("Value exists!")
                  //     //   } else{
                  //     //     uniqIdPatient.push(casesBypatientIds['data'][d].patientId);
                  //     //   }
                  //     // }
                  //
                  //   }
                  // }
                  var patientIdforName = [];
                  const map2 = new Map();
                  for (const item of casesBypatientIds['data']) {
                    if(!map2.has(item.patientId)){
                      map2.set(item.patientId, true);    // set any value to Map
                      patientIdforName.push(item.patientId);

                    }
                  }

                  var uniqClientNames = [];
                  const map3 = new Map();
                  for (const item of casesBypatientIds['data']) {
                    if(!map3.has(item.clientId)){
                      map3.set(item.clientId, true);    // set any value to Map
                      uniqClientNames.push(item.clientId);

                    }
                  }
                  const patienByidURL = environment.API_PATIENT_ENDPOINT + 'patientbyids';
                  var getPatReq       = {...this.patientByIdsReq}
                  getPatReq.data.patientIds = patientIdforName;
                  getPatReq.header.partnerCode  = this.logedInUserRoles.partnerCode;
                  getPatReq.header.userCode     = this.logedInUserRoles.userCode;
                  getPatReq.header.functionalityCode     = "SPCA-MC";

                  this.http.put(patienByidURL, getPatReq).subscribe(forPatientName=>{
                    console.log("forPatientName",forPatientName);


                    for (let index = 0; index < casesBypatientIds['data'].length; index++) {
                      for (let i = 0; i < forPatientName['data'].length; i++) {
                        if (casesBypatientIds['data'][index].patientId == forPatientName['data'][i].patientId) {

                          // this.activeCases = [];
                          casesBypatientIds['data'][index].pFName = forPatientName['data'][i].firstName;
                          casesBypatientIds['data'][index].pLName = forPatientName['data'][i].lastName;
                          casesBypatientIds['data'][index].pDob   = forPatientName['data'][i].dateOfBirth;
                          // TempTableHolder.push(casesBypatientIds['data'][c]);
                          // uniqueClientId.push(casesBypatientIds['data'][c].clientId)


                        }
                        else{
                          if (typeof casesBypatientIds['data'][index].pFName == 'undefined') {
                            casesBypatientIds['data'][index].pFName = "no Name";
                            casesBypatientIds['data'][index].pLName = "no Name";
                            casesBypatientIds['data'][index].pDob   = "02-02-2021 19:10:00";
                          }
                          // casesBypatientIds['data'][index].pFName = "no Name";
                          // casesBypatientIds['data'][index].pLName = "no Name";
                          // casesBypatientIds['data'][index].pDob   = "02-02-2021 19:10:00";
                          // console.log("Id Doesnot match",casesBypatientIds['data'][c].patientId);
                          // casesBypatientIds['data'].splice(c, 1);
                          // caselen=casesBypatientIds['data'];

                        }

                      }

                    }
                    // console.log("byClientIdsResp['data']['caseDetails']",byClientIdsResp['data']['caseDetails']);

                    // const combined2 = [...tempHold, ...byClientIdsResp['data']['caseDetails']];
                    // console.log("combined2",combined2);
                    var clURL           = environment.API_CLIENT_ENDPOINT + "clientidsnames"
                    var clReqData = {
                      header:{
                        uuid               :"",
                        partnerCode        :"",
                        userCode           :"",
                        referenceNumber    :"",
                        systemCode         :"",
                        moduleCode         :"CLIM",
                        functionalityCode  :"SPCA-MC",
                        systemHostAddress  :"",
                        remoteUserAddress  :"",
                        dateTime           :""
                      },
                      data:{
                        clientIds:uniqClientNames
                      }
                    }
                    clReqData['header'].partnerCode = this.logedInUserRoles.partnerCode;
                    clReqData['header'].userCode = this.logedInUserRoles.userCode;
                    clReqData['header'].functionalityCode = "SPCA-MC";
                    // var clReqData       = {clientIds:uniqClientNames}
                    this.specimenService.getClientByName(clURL,clReqData).then(selectedClient => {
                      // console.log("selectedClient",selectedClient);
                      for (let f = 0; f < selectedClient['data']['length']; f++) {
                        for (let e = 0; e < casesBypatientIds['data'].length; e++) {

                          if (casesBypatientIds['data'][e].clientId == selectedClient['data'][f].clientId) {
                            // console.log("in if");
                            casesBypatientIds['data'][e].clientName = selectedClient['data'][f].clientName;

                            // casesBypatientIds['data'].map((i) => { i.clientName = selectedClient[f].name; return i; });
                          }
                          else{
                            // console.log("in else");
                            if (typeof casesBypatientIds['data'][e].clientName == 'undefined') {
                              casesBypatientIds['data'][e].clientName="No Name"
                            }

                            // casesBypatientIds['data'][e].clientName="No Name"
                          }

                        }

                      }
                      // var finalData = [];
                      var finalDataCount = 0;
                      for (let index = 0; index < casesBypatientIds['data']['length']; index++) {
                        finalDataCount = finalDataCount +1
                        // console.log("casesBypatientIds['data'][index]",casesBypatientIds['data'][index]);

                        // if (typeof casesBypatientIds['data'] != 'undefined') {
                        if (casesBypatientIds['data'][index]['pFName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || casesBypatientIds['data'][index]['pLName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || this.datePipe.transform( casesBypatientIds['data'][index]['pDob'].toLowerCase(), 'MM/dd/yyyy').indexOf(searchString.toLowerCase()) !== -1
                        || casesBypatientIds['data'][index]['clientName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || this.datePipe.transform( casesBypatientIds['data'][index]['collectionDate'].toLowerCase(), 'MM/dd/yyyy').indexOf(searchString.toLowerCase()) !== -1) {
                          // finalData[index] = casesBypatientIds['data'][index];
                          if (searchString.toLowerCase() == 'client' || searchString.toLowerCase() == 'sip') {
                            if (casesBypatientIds['data'][index]['pFName'].toLowerCase().indexOf('client') === -1
                            && casesBypatientIds['data'][index]['pLName'].toLowerCase().indexOf('client') === -1
                            && casesBypatientIds['data'][index]['clientName'].toLowerCase().indexOf('client') === -1) {
                              casesBypatientIds['data'] = casesBypatientIds['data'].filter(function(value, index, arr){
                                // console.log("value.collectedBy",value.collectedBy);
                                return value.collectedBy == 2;
                              });

                            }
                            else if (casesBypatientIds['data'][index]['pFName'].toLowerCase().indexOf('sip') === -1
                            && casesBypatientIds['data'][index]['pLName'].toLowerCase().indexOf('sip') === -1
                            && casesBypatientIds['data'][index]['clientName'].toLowerCase().indexOf('sip') === -1) {
                              casesBypatientIds['data'] = casesBypatientIds['data'].filter(function(value, index, arr){
                                // console.log("value.collectedBy",value.collectedBy);
                                return value.collectedBy == 1;
                              });
                            }
                            if (type == 'active') {
                              this.activeCases.push(casesBypatientIds['data'][index]);
                              // resolve()
                            }
                            else if(type == 'all'){
                              this.allCases.push(casesBypatientIds['data'][index]);
                            }
                          }
                          else{
                            if (type == 'active') {
                              this.activeCases.push(casesBypatientIds['data'][index]);
                              // resolve()
                            }
                            else if(type == 'all'){
                              this.allCases.push(casesBypatientIds['data'][index]);
                            }
                          }
                          // if (type == 'active') {
                          //   this.activeCases.push(casesBypatientIds['data'][index]);
                          //   // resolve()
                          // }
                          // else if(type == 'all'){
                          //   this.allCases.push(casesBypatientIds['data'][index]);
                          // }
                        }
                        // }


                        if (finalDataCount == casesBypatientIds['data']['length']) {
                          if (type == 'active') {
                            // this.activeCases = [];
                            this.activeCasesCount = this.activeCases['length'];
                            // this.activeCases = finalData;
                            this.ngxLoader.stop();
                            resolve()
                          }
                          else if(type == 'all'){
                            // this.allCases = [];
                            this.allCasesCount = this.allCases['length'];
                            // this.allCases = finalData;
                            this.ngxLoader.stop();
                            resolve()
                          }


                        }
                      }
                      // console.log("finalData",finalData);
                      //
                      //
                      // if (type == 'active') {
                      //   // this.activeCases = [];
                      //   this.activeCasesCount =finalData['length'];
                      //   this.activeCases = finalData;
                      //   this.ngxLoader.stop();
                      //   resolve()
                      // }
                      // else if(type == 'all'){
                      //   // this.allCases = [];
                      //   this.allCasesCount = finalData['length'];
                      //   this.allCases = finalData;
                      //   this.ngxLoader.stop();
                      //   resolve()
                      // }

                    });

                  })

                })
              }

            })




          }
          else{
            var uniqueClientId = [];
            var uniquePatientId = [];
            const map = new Map();
            for (const item of clientData) {
              if(!map.has(item.clientId)){
                map.set(item.clientId, true);    // set any value to Map
                uniqueClientId.push(item.clientId);

              }
              console.log("uniqueClientId----",uniqueClientId);


              // tempForTable.push(casesDataBlueprint);
              // casesDataBlueprint = { caseNumber: null, caseId: null, pFName: null, pLName: null, pDob: null, clientName: null, collectionDate: null, collectedBy: null, caseStatusId: null, }
            }
            const map2 = new Map();
            for (const item of patientDat['data']['patients']) {
              if(!map2.has(item.patientId)){
                map2.set(item.patientId, true);    // set any value to Map
                uniquePatientId.push(item.patientId);

              }

              // tempForTable.push(casesDataBlueprint);
              // casesDataBlueprint = { caseNumber: null, caseId: null, pFName: null, pLName: null, pDob: null, clientName: null, collectionDate: null, collectedBy: null, caseStatusId: null, }
            }
            // console.log("uniquePatientId",uniquePatientId);

            for (var j = 0, len2 = clientData['length']; j < len2; j++) {
              for (var i = 0, len = patientDat['data']['patients']['length']; i < len; i++) {
                if (patientDat['data']['patients'][i].clientId == clientData[j].clientId) {

                }
                else{
                  console.log("clientData[j].clientId",clientData[j].clientId);
                  for (let k = 0; k < uniqueClientId.length; k++) {
                    if (uniqueClientId[k] == patientDat['data']['patients'][i].clientId) {

                    }
                    else{
                      if(uniqueClientId.indexOf(patientDat['data']['patients'][i].clientId) !== -1){
                        // alert("Value exists!")
                      } else{
                        uniqueClientId.push(patientDat['data']['patients'][i].clientId);
                      }

                    }

                  }

                }
              }
            }
            console.log("uniqueClientId222222",uniqueClientId);
            var clURL           = environment.API_PATIENT_ENDPOINT + "patientbyclientids"
            var reqPatBP  = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"",functionalityCode:"SPCA-MC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{userName:"danish",clientIds:uniqueClientId}}
            this.specimenService.getPatientsByClietnIds(clURL,reqPatBP).subscribe(patientByClientIds => {
              console.log("------patientByClientIds",patientByClientIds);
              this.activeCases = [];
              for (let dum = 0; dum < patientByClientIds['data'].length; dum++) {

                // casesDataBlueprint['pFName'] = patientByClientIds['data'][dum].firstName
                // casesDataBlueprint['pLName'] = patientByClientIds['data'][dum].lastName
                // casesDataBlueprint['pDob']   = patientByClientIds['data'][dum].dateOfBirth
                // casesDataBlueprint['caseNumber'] = "null"; casesDataBlueprint['caseId'] = "null"; casesDataBlueprint['clientName']= "null", casesDataBlueprint['collectionDate']="null"; casesDataBlueprint['collectedBy']="null"; casesDataBlueprint['caseStatusId']= "1";
                // if (type == 'active') {
                //   this.activeCases.push(casesDataBlueprint);
                //   casesDataBlueprint = { caseNumber: '', caseId: '', pFName: '', pLName: '', pDob: '', clientName: '', collectionDate: '', collectedBy: '', caseStatusId: '', }
                //
                // }
                // else if(type == 'all'){
                //   // this.activeCases = [];
                //   this.allCases.push(casesDataBlueprint);
                //   casesDataBlueprint = { caseNumber: '', caseId: '', pFName: '', pLName: '', pDob: '', clientName: '', collectionDate: '', collectedBy: '', caseStatusId: '', }
                // }

              }



              var tempForTable    = [];
              const map = new Map();
              for (const item of patientByClientIds['data']) {
                if(!map.has(item.patientId)){
                  map.set(item.patientId, true);    // set any value to Map
                  uniquePatientId.push(item.patientId);

                }

                // tempForTable.push(casesDataBlueprint);
                // casesDataBlueprint = { caseNumber: null, caseId: null, pFName: null, pLName: null, pDob: null, clientName: null, collectionDate: null, collectedBy: null, caseStatusId: null, }
              }




              var TempTableHolder = [];
              var intakeForSearch = ''
              var clientForSearch = ''
              var statuses        = []
              if (this.selectedIntakeIdForCases != null) {
                intakeForSearch = this.selectedIntakeIdForCases
                clientForSearch = this.selectedClientIdForCases
                statuses        = [3,2,6,12,10,8,9,7,4,5]
              }
              else{
                intakeForSearch = ""
                clientForSearch = ""
                statuses        = []
              }
              var specUrl = environment.API_SPECIMEN_ENDPOINT + "findbypatientid"
              var reqPid  ={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-MC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{
                patientIds:uniquePatientId,intakeId:intakeForSearch,client:clientForSearch,statuses:statuses, userCode:""}}
                this.specimenService.findByPatientIds(specUrl, reqPid).subscribe(casesBypatientIds =>{
                  console.log("*******casesBypatientIds",casesBypatientIds);
                  if (casesBypatientIds['data'].length == 0) {
                    if (type == 'active') {
                      this.activeCases = [];
                      this.activeCasesCount = 0;
                      // this.activeCases = finalData;
                      this.ngxLoader.stop();
                      resolve()
                    }
                    else if(type == 'all'){
                      this.allCases = [];
                      this.allCasesCount = 0;
                      // this.allCases = finalData;
                      this.ngxLoader.stop();
                      resolve()
                    }
                    return;
                  }
                  // uniqueClientId = []

                  for (var c = 0;c < casesBypatientIds['data'].length; c++) {
                    for (let j = 0; j < patientByClientIds['data'].length; j++) {
                      if (casesBypatientIds['data'][c].patientId == patientByClientIds['data'][j].patientId) {
                        console.log("patientByClientIds['data'][j].firstName",patientByClientIds['data'][j]);

                        // this.activeCases = [];
                        casesBypatientIds['data'][c].pFName = patientByClientIds['data'][j].firstName;
                        casesBypatientIds['data'][c].pLName = patientByClientIds['data'][j].lastName;
                        casesBypatientIds['data'][c].pDob   = patientByClientIds['data'][j].dateOfBirth;
                        console.log("33232323232",casesBypatientIds['data'][c]);
                      }
                      else{
                        // console.log("else-----",patientByClientIds['data'][j].firstName);

                        // casesBypatientIds['data'][c].pFName = "No Name";
                        // casesBypatientIds['data'][c].pLName = "No Name";
                        // casesBypatientIds['data'][c].pDob   = "02-02-2021 19:10:00";
                      }
                      if(uniqueClientId.indexOf(casesBypatientIds['data'][c].clientId) !== -1){
                        // alert("Value exists!")
                      } else{
                        uniqueClientId.push(casesBypatientIds['data'][c].clientId);
                      }

                    }
                    // for (let d = 0; d < patientByClientIds['data'].length; d++) {
                    //   if (casesBypatientIds['data'][c].patientId == patientByClientIds['data'][d].patientId) {
                    //     // this.activeCases = [];
                    //     casesBypatientIds['data'][c].pFName = patientByClientIds['data'][d].firstName;
                    //     casesBypatientIds['data'][c].pLName = patientByClientIds['data'][d].lastName;
                    //     casesBypatientIds['data'][c].pDob   = patientByClientIds['data'][d].dateOfBirth;
                    //     // TempTableHolder.push(casesBypatientIds['data'][c]);
                    //     if(uniqueClientId.indexOf(casesBypatientIds['data'][c].clientId) !== -1){
                    //       // alert("Value exists!")
                    //     } else{
                    //       uniqueClientId.push(casesBypatientIds['data'][c].clientId);
                    //     }
                    //     // uniqueClientId.push(casesBypatientIds['data'][c].clientId)
                    //   }
                    //   else{
                    //     // console.log("Id Doesnot match",casesBypatientIds['data'][c].patientId);
                    //     // casesBypatientIds['data'].splice(c, 1);
                    //     // caselen=casesBypatientIds['data'];
                    //     casesBypatientIds['data'][c].pFName = "No Name";
                    //     casesBypatientIds['data'][c].pLName = "No Name";
                    //     casesBypatientIds['data'][c].pDob   = "02-02-2021 19:10:00";
                    //
                    //   }
                    //
                    // }

                  }
                  console.log("uniqueClientId3333",uniqueClientId);
                  var clURL           = environment.API_CLIENT_ENDPOINT + "clientidsnames"
                  var clReqData = {
                    header:{
                      uuid               :"",
                      partnerCode        :"",
                      userCode           :"",
                      referenceNumber    :"",
                      systemCode         :"",
                      moduleCode         :"CLIM",
                      functionalityCode  :"SPCA-MC",
                      systemHostAddress  :"",
                      remoteUserAddress  :"",
                      dateTime           :""
                    },
                    data:{
                      clientIds:uniqueClientId
                    }
                  }
                  clReqData['header'].partnerCode = this.logedInUserRoles.partnerCode;
                  clReqData['header'].userCode = this.logedInUserRoles.userCode;
                  clReqData['header'].functionalityCode = "SPCA-MC";
                  // var clReqData       = {clientIds:uniqueClientId}
                  this.specimenService.getClientByName(clURL,clReqData).then(selectedClient => {
                    console.log("selectedClient",selectedClient);
                    for (let f = 0; f < selectedClient['data']['length']; f++) {
                      for (let e = 0; e < casesBypatientIds['data'].length; e++) {

                        if (casesBypatientIds['data'][e].clientId == selectedClient['data'][f].clientId) {
                          // console.log("in if");
                          casesBypatientIds['data'][e].clientName = selectedClient['data'][f].clientName;

                          // casesBypatientIds['data'].map((i) => { i.clientName = selectedClient[f].name; return i; });
                        }
                        else{
                          // console.log("in else");

                          // casesBypatientIds['data'][e].clientName="No Name"
                        }

                      }
                      // console.log('=========');


                    }
                    // for (let i = 0; i < casesBypatientIds['data'].length; i++) {
                    //   for (let j = 0; j < patientDat['data']['patients'].length; j++) {
                    //     if (casesBypatientIds['data'][i].patientId == patientDat['data']['patients'][j].patientId) {
                    //       // this.activeCases = [];
                    //       casesBypatientIds['data'][i].pFName = patientDat['data']['patients'][j].firstName;
                    //       casesBypatientIds['data'][i].pLName = patientDat['data']['patients'][j].lastName;
                    //       casesBypatientIds['data'][i].pDob   = patientDat['data']['patients'][j].dateOfBirth;
                    //     }
                    //
                    //   }
                    //
                    // }
                    // var finalData = [];
                    // var finalDataHolder = {};
                    var finalDataCount = 0;
                    console.log("casesBypatientIds['data']",casesBypatientIds['data']);


                    for (let index = 0; index < casesBypatientIds['data']['length']; index++) {
                      finalDataCount = finalDataCount +1;
                      // console.log("casesBypatientIds['data'][index]",casesBypatientIds['data'][index]);

                      if (casesBypatientIds['data'][index]['pFName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || casesBypatientIds['data'][index]['pLName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || this.datePipe.transform(casesBypatientIds['data'][index]['pDob'].toLowerCase(), 'MM/dd/yyyy').indexOf(searchString.toLowerCase()) !== -1
                      || casesBypatientIds['data'][index]['clientName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || this.datePipe.transform(casesBypatientIds['data'][index]['collectionDate'].toLowerCase(), 'MM/dd/yyyy').indexOf(searchString.toLowerCase()) !== -1) {
                        // finalData[index] = casesBypatientIds['data'][index];
                        if (searchString.toLowerCase() == 'client' || searchString.toLowerCase() == 'sip') {
                          if (casesBypatientIds['data'][index]['pFName'].toLowerCase().indexOf('client') === -1
                          && casesBypatientIds['data'][index]['pLName'].toLowerCase().indexOf('client') === -1
                          && casesBypatientIds['data'][index]['clientName'].toLowerCase().indexOf('client') === -1) {
                            casesBypatientIds['data'] = casesBypatientIds['data'].filter(function(value, index, arr){
                              // console.log("value.collectedBy",value.collectedBy);
                              return value.collectedBy == 2;
                            });

                          }
                          else if (casesBypatientIds['data'][index]['pFName'].toLowerCase().indexOf('sip') === -1
                          && casesBypatientIds['data'][index]['pLName'].toLowerCase().indexOf('sip') === -1
                          && casesBypatientIds['data'][index]['clientName'].toLowerCase().indexOf('sip') === -1) {
                            casesBypatientIds['data'] = casesBypatientIds['data'].filter(function(value, index, arr){
                              // console.log("value.collectedBy",value.collectedBy);
                              return value.collectedBy == 1;
                            });
                          }
                          if (type == 'active') {
                            this.activeCases.push(casesBypatientIds['data'][index]);
                            // resolve()
                          }
                          else if(type == 'all'){
                            this.allCases.push(casesBypatientIds['data'][index]);
                          }
                        }
                        else{
                          if (type == 'active') {
                            this.activeCases.push(casesBypatientIds['data'][index]);
                            // resolve()
                          }
                          else if(type == 'all'){
                            this.allCases.push(casesBypatientIds['data'][index]);
                          }
                        }
                        // if (type == 'active') {
                        //   this.activeCases.push(casesBypatientIds['data'][index]);
                        //   // resolve()
                        // }
                        // else if(type == 'all'){
                        //   this.allCases.push(casesBypatientIds['data'][index]);
                        // }
                      }

                      if (finalDataCount == casesBypatientIds['data']['length']) {
                        if (type == 'active') {
                          // this.activeCases = [];
                          this.activeCasesCount = this.activeCases['length'];
                          // this.activeCases = finalData;
                          this.ngxLoader.stop();
                          resolve()
                        }
                        else if(type == 'all'){
                          // this.allCases = [];
                          this.allCasesCount = this.allCases['length'];
                          // this.allCases = finalData;
                          this.ngxLoader.stop();
                          resolve()
                        }


                      }

                    }


                  });

                  // this.getPatClData(uniquePatientId,casesBypatientIds,uniqueCLIds,type).then(respAlldata => {
                  //
                  // })
                })

              });
            }
            // console.log('--------------------');
            // tempForTable.push(casesDataBlueprint);
            // casesDataBlueprint = { caseNumber: null, caseId: null, pFName: null, pLName: null, pDob: null, clientName: null, collectionDate: null, collectedBy: null, caseStatusId: null, }



          }
          else if(clientData['length'] == 0 && patientDat['data']['patients'] != null && caseData['data']['caseDetails']['length'] == 0){
            //////////////////////get uniqe patient Ids
            var uniquePatientId = [];
            var tempForTable    = [];
            const map = new Map();
            for (const item of patientDat['data']['patients']) {
              if(!map.has(item.patientId)){
                map.set(item.patientId, true);    // set any value to Map
                uniquePatientId.push(item.patientId);

              }
            }
            var intakeForSearch = ''
            var clientForSearch = ''
            var statuses        = []
            if (this.selectedIntakeIdForCases != null) {
              intakeForSearch = this.selectedIntakeIdForCases
              clientForSearch = this.selectedClientIdForCases
              statuses        = [3,2,6,12,10,8,9,7,4,5]
            }
            else{
              intakeForSearch = ""
              clientForSearch = ""
              statuses        = []
            }

            var byPidURL = environment.API_SPECIMEN_ENDPOINT + 'findbypatientid';
            var reqPid={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-MC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{
              patientIds:uniquePatientId,intakeId:intakeForSearch,client:clientForSearch,statuses:statuses, userCode:""}}
              this.specimenService.findByPatientIds(byPidURL, reqPid).subscribe(respPID=>{
                console.log("by Patien ID Active Cases",respPID);
                if (respPID['data']['length'] == 0) {
                  if (caseData['data']['caseDetails']['length'] >0) {
                    console.log("hereeeeeeeee---");

                    var a = caseData['data']['caseDetails'];
                    const uniqueIds = [];
                    const uniqueCLIds = [];
                    const map = new Map();
                    for (const item of a) {
                      if(!map.has(item.clientId)){
                        map.set(item.clientId, true);    // set any value to Map
                        uniqueCLIds.push(item.clientId);
                      }
                    }
                    const map1 = new Map();
                    for (const item of a) {
                      if(!map1.has(item.patientId)){
                        map1.set(item.patientId, true);    // set any value to Map
                        uniqueIds.push(item.patientId);
                      }
                    }

                    // getPatReq.data.patientIds = uniqueIds;
                    this.getPatClData(uniqueIds,caseData,uniqueCLIds,type).then(respAlldata => {
                      resolve()


                    });
                  }
                  else{
                    if (type == 'active') {
                      this.activeCases = [];
                      this.activeCasesCount = 0;
                      // this.activeCases = finalData;
                      this.ngxLoader.stop();
                      resolve()
                    }
                    else if(type == 'all'){
                      this.allCases = [];
                      this.allCasesCount = 0;
                      // this.allCases = finalData;
                      this.ngxLoader.stop();
                      resolve()
                    }
                  }
                }
                else{
                  const uniqueCLIds = []
                  const map = new Map();
                  for (let i = 0; i < respPID['data'].length; i++) {
                    for (let j = 0; j < patientDat['data']['patients'].length; j++) {
                      if(respPID['data'][i].patientId == patientDat['data']['patients'].patientId){

                      }
                      for (const item of respPID['data']) {
                        if(!map.has(item.clientId)){
                          map.set(item.clientId, true);    // set any value to Map
                          uniqueCLIds.push(item.clientId);
                        }
                      }

                    }

                  }
                  this.getPatClDataMegaSearch(uniquePatientId,respPID,uniqueCLIds,type,searchString).then(respAlldata => {
                    // console.log("activeCases",this.activeCases);
                    resolve();

                  });
                }



              })

              // console.log("----uniqueClientId",uniqueClientId);


              // }
            }
            else if(clientData['length'] > 0 && patientDat['data']['patients'] == null && caseData['data']['caseDetails']['length'] == 0){
              var uniqIdPatient = []
              var uniqueClientId = [];
              const map1 = new Map();
              for (const item of clientData) {
                if(!map1.has(item.clientId)){
                  map1.set(item.clientId, true);    // set any value to Map
                  uniqueClientId.push(item.clientId);
                }
              }
              var intakeForSearch = ''
              // var clientForSearch = ''
              if (this.selectedIntakeIdForCases != null) {
                intakeForSearch = this.selectedIntakeIdForCases
                // clientForSearch = this.selectedClientIdForCases
              }
              else{
                intakeForSearch = ""
                // clientForSearch = ""
              }

              let columnCaseReq  = {}
              var columnUrl      = environment.API_SPECIMEN_ENDPOINT + 'searchbyclient';
              columnCaseReq      = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-MC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},
              data:{page:"0",size:20,sortColumn:"caseNumber",sortingOrder:"desc",
              clientId: uniqueClientId,
              intakeId: intakeForSearch,
              statuses:[],
              userCode:""}}
              this.http.post(columnUrl, columnCaseReq).subscribe(byClientIdsResp=>{
                console.log("---byClientIdsResp",byClientIdsResp);
                if (byClientIdsResp['data']['caseDetails'].length == 0) {
                  if (caseData['data']['caseDetails']['length'] >0) {
                    console.log("hereeeeeeeee---");

                    var a = caseData['data']['caseDetails'];
                    const uniqueIds = [];
                    const uniqueCLIds = [];
                    const map = new Map();
                    for (const item of a) {
                      if(!map.has(item.clientId)){
                        map.set(item.clientId, true);    // set any value to Map
                        uniqueCLIds.push(item.clientId);
                      }
                    }
                    const map1 = new Map();
                    for (const item of a) {
                      if(!map1.has(item.patientId)){
                        map1.set(item.patientId, true);    // set any value to Map
                        uniqueIds.push(item.patientId);
                      }
                    }

                    // getPatReq.data.patientIds = uniqueIds;
                    this.getPatClData(uniqueIds,caseData,uniqueCLIds,type).then(respAlldata => {
                      resolve()


                    });
                  }
                  else{
                    if (type == 'active') {
                      this.activeCases = [];
                      this.activeCasesCount = 0;
                      // this.activeCases = finalData;
                      this.ngxLoader.stop();
                      resolve()
                    }
                    else if(type == 'all'){
                      this.allCases = [];
                      this.allCasesCount = 0;
                      // this.allCases = finalData;
                      this.ngxLoader.stop();
                      resolve()
                    }
                  }

                }
                for (let i = 0; i < byClientIdsResp['data']['caseDetails'].length; i++) {
                  for (let j = 0; j < clientData['length']; j++) {
                    if (byClientIdsResp['data']['caseDetails'][i].clientId == clientData[j].clientId) {
                      if(uniqueClientId.indexOf(byClientIdsResp['data']['caseDetails'][i].clientId) !== -1){
                        // alert("Value exists!")
                      } else{
                        uniqueClientId.push(byClientIdsResp['data']['caseDetails'][i].clientId);
                      }
                    }
                  }
                }

                ///////////////
                //////if case is not null/////////
                ///////////////////////////////////
                // const combined2
                if (caseData['data']['caseDetails']['length'] > 0) {
                  var userTemporaryhold   = [];
                  for (let cd = 0; cd < caseData['data']['caseDetails'].length; cd++) {
                    for (let cc = 0; cc < byClientIdsResp['data']['caseDetails'].length; cc++) {

                      if (caseData['data']['caseDetails'][cd].caseId == byClientIdsResp['data']['caseDetails'][cc].caseId) {
                        // console.log("equal");

                        ///////////////////////no need to change
                      }
                      else{
                        userTemporaryhold[cd]   = caseData['data']['caseDetails'][cd];
                        // byClientIdsResp['data']['caseDetails'].push(caseData['data']['caseDetails'][cd]);
                        if(uniqueClientId.indexOf(caseData['data']['caseDetails'][cd].clientId) !== -1){
                          // alert("Value exists!")
                        } else{
                          uniqueClientId.push(caseData['data']['caseDetails'][cd].clientId);
                        }
                      }

                    }

                  }
                  byClientIdsResp['data']['caseDetails'] = [...userTemporaryhold, ...byClientIdsResp['data']['caseDetails']];
                  // console.log("after case involvement",byClientIdsResp);
                  // console.log("after case involvement uniqueClientId",uniqueClientId);
                  // console.log("combined2",combined2);
                }


                // uniqueClientId = []
                var clURL           = environment.API_PATIENT_ENDPOINT + "patientbyclientids"
                var reqPatBP  = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"",functionalityCode:"SPCA-MC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{userName:"danish",clientIds:uniqueClientId}}
                this.specimenService.getPatientsByClietnIds(clURL,reqPatBP).subscribe(patientByClientIds => {
                  console.log("*******patientByClientIds",patientByClientIds);
                  if (byClientIdsResp['data']['caseDetails']['length'] > 0) {
                    for (var c = 0; c< byClientIdsResp['data']['caseDetails']['length']; c++) {
                      for (let d = 0; d < patientByClientIds['data']['length']; d++) {

                        if (byClientIdsResp['data']['caseDetails'][c].clientId == patientByClientIds['data'][d].clientId) {

                          byClientIdsResp['data']['caseDetails'][c].pFName = patientByClientIds['data'][d].firstName;
                          byClientIdsResp['data']['caseDetails'][c].pLName = patientByClientIds['data'][d].lastName;
                          byClientIdsResp['data']['caseDetails'][c].pDob   = patientByClientIds['data'][d].dateOfBirth;
                          // TempTableHolder.push(casesBypatientIds['data'][c]);
                          // uniqueClientId.push(patientByClientIds['data'][c].clientId)
                          if(uniqueClientId.indexOf(patientByClientIds['data'][d].clientId) !== -1){
                            // alert("Value exists!")
                          } else{
                            uniqueClientId.push(patientByClientIds['data'][d].clientId);
                          }
                        }
                        else{
                          // console.log("else",byClientIdsResp['data']['caseDetails'][c].pFName);
                          if (typeof byClientIdsResp['data']['caseDetails'][c].pFName == 'undefined') {
                            // byClientIdsResp['data']['caseDetails'][c].pFName = patientByClientIds['data'][d].firstName;
                            // byClientIdsResp['data']['caseDetails'][c].pLName = patientByClientIds['data'][d].lastName;
                            // byClientIdsResp['data']['caseDetails'][c].pDob   = patientByClientIds['data'][d].dateOfBirth;
                            byClientIdsResp['data']['caseDetails'][c].pFName = "no Name";
                            byClientIdsResp['data']['caseDetails'][c].pLName = "no Name";
                            byClientIdsResp['data']['caseDetails'][c].pDob   = "02-02-2021 19:10:00";
                            console.log(" patientByClientIds['data'][d].dateOfBirth", patientByClientIds['data'][d].dateOfBirth);

                            if(uniqueClientId.indexOf(byClientIdsResp['data']['caseDetails'][c].clientId) !== -1){
                              // alert("Value exists!")
                            } else{
                              uniqueClientId.push(byClientIdsResp['data']['caseDetails'][c].clientId);
                            }
                          }


                          // console.log("Id Doesnot match",casesBypatientIds['data'][c].patientId);
                          // casesBypatientIds['data'].splice(c, 1);
                          // caselen=casesBypatientIds['data'];

                          // byClientIdsResp['data']['caseDetails'][c].pFName = "no Name";
                          // byClientIdsResp['data']['caseDetails'][c].pLName = "no Name";
                          // byClientIdsResp['data']['caseDetails'][c].pDob   = "02-02-2021 19:10:00";


                        }

                      }

                    }

                    var clURL           = environment.API_CLIENT_ENDPOINT + "clientidsnames"
                    var clReqData = {
                      header:{
                        uuid               :"",
                        partnerCode        :"",
                        userCode           :"",
                        referenceNumber    :"",
                        systemCode         :"",
                        moduleCode         :"CLIM",
                        functionalityCode  :"SPCA-MC",
                        systemHostAddress  :"",
                        remoteUserAddress  :"",
                        dateTime           :""
                      },
                      data:{
                        clientIds:uniqueClientId
                      }
                    }
                    clReqData['header'].partnerCode = this.logedInUserRoles.partnerCode;
                    clReqData['header'].userCode = this.logedInUserRoles.userCode;
                    clReqData['header'].functionalityCode = "SPCA-MC";
                    // var clReqData       = {clientIds:uniqueClientId}
                    this.specimenService.getClientByName(clURL,clReqData).then(selectedClient => {
                      console.log("selectedClient",selectedClient);
                      for (let f = 0; f < selectedClient['data']['length']; f++) {
                        for (let e = 0; e < byClientIdsResp['data']['caseDetails'].length; e++) {

                          if (byClientIdsResp['data']['caseDetails'][e].clientId == selectedClient['data'][f].clientId) {
                            // console.log("in if");
                            byClientIdsResp['data']['caseDetails'][e].clientName = selectedClient['data'][f].clientName;

                            // casesBypatientIds['data'].map((i) => { i.clientName = selectedClient[f].name; return i; });
                          }
                          else{
                            // console.log("in else");
                            if (typeof byClientIdsResp['data']['caseDetails'][e].clientName == 'undefined') {
                              byClientIdsResp['data']['caseDetails'][e].clientName="No Name";
                            }


                          }

                        }

                      }
                      // var finalData = [];
                      var finalDataCount = 0;
                      for (let index = 0; index < byClientIdsResp['data']['caseDetails']['length']; index++) {
                        finalDataCount = finalDataCount +1
                        // if (1) {
                        // if (byClientIdsResp['data']['caseDetails'][index]['pFName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || byClientIdsResp['data']['caseDetails'][index]['pLName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || byClientIdsResp['data']['caseDetails'][index]['pDob'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || byClientIdsResp['data']['caseDetails'][index]['clientName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1) {
                        // finalData[index] = byClientIdsResp['data'][index];
                        if (searchString.toLowerCase() == 'client' || searchString.toLowerCase() == 'sip') {
                          if (byClientIdsResp['data']['caseDetails'][index]['pFName'].toLowerCase().indexOf('client') === -1
                          && byClientIdsResp['data']['caseDetails'][index]['pLName'].toLowerCase().indexOf('client') === -1
                          && byClientIdsResp['data']['caseDetails'][index]['clientName'].toLowerCase().indexOf('client') === -1) {
                            byClientIdsResp['data']['caseDetails'] = byClientIdsResp['data']['caseDetails'].filter(function(value, index, arr){
                              // console.log("value.collectedBy",value.collectedBy);
                              return value.collectedBy == 2;
                            });

                          }
                          else if (byClientIdsResp['data']['caseDetails'][index]['pFName'].toLowerCase().indexOf('sip') === -1
                          && byClientIdsResp['data']['caseDetails'][index]['pLName'].toLowerCase().indexOf('sip') === -1
                          && byClientIdsResp['data']['caseDetails'][index]['clientName'].toLowerCase().indexOf('sip') === -1) {
                            byClientIdsResp['data']['caseDetails'] = byClientIdsResp['data']['caseDetails'].filter(function(value, index, arr){
                              // console.log("value.collectedBy",value.collectedBy);
                              return value.collectedBy == 1;
                            });
                          }
                          if (type == 'active') {
                            this.activeCases.push(byClientIdsResp['data']['caseDetails'][index]);
                            // resolve()
                          }
                          else if(type == 'all'){
                            this.allCases.push(byClientIdsResp['data']['caseDetails'][index]);
                          }
                        }
                        else{
                          if (type == 'active') {
                            this.activeCases.push(byClientIdsResp['data']['caseDetails'][index]);
                            // resolve()
                          }
                          else if(type == 'all'){
                            this.allCases.push(byClientIdsResp['data']['caseDetails'][index]);
                          }
                        }
                        // if (type == 'active') {
                        //   this.activeCases.push(byClientIdsResp['data']['caseDetails'][index]);
                        //   // resolve()
                        // }
                        // else if(type == 'all'){
                        //   this.allCases.push(byClientIdsResp['data']['caseDetails'][index]);
                        // }
                        // }

                        if (finalDataCount == byClientIdsResp['data']['caseDetails']['length']) {
                          if (type == 'active') {
                            // this.activeCases = [];
                            this.activeCasesCount = this.activeCases['length'];
                            // this.activeCases = finalData;
                            this.ngxLoader.stop();
                            resolve()
                          }
                          else if(type == 'all'){
                            // this.allCases = [];
                            this.allCasesCount = this.allCases['length'];
                            // this.allCases = finalData;
                            this.ngxLoader.stop();
                            resolve()
                          }


                        }

                      }
                      // console.log("finalData",finalData);
                      //
                      // if (type == 'active') {
                      //   // this.activeCases = [];
                      //   this.activeCasesCount = finalData['length'];
                      //   this.activeCases = finalData;
                      //   this.ngxLoader.stop();
                      //   resolve()
                      // }
                      // else if(type == 'all'){
                      //   // this.allCases = [];
                      //   this.allCasesCount = finalData['length'];
                      //   this.allCases = finalData;
                      //   this.ngxLoader.stop();
                      //   resolve()
                      // }

                    });
                  }
                  else{

                  }
                  /////// idar
                })



              })



            }
            else if(clientData['length'] == 0 && patientDat['data']['patients'] == null && caseData['data']['caseDetails']['length'] >0 ){
              var a = caseData['data']['caseDetails'];
              const uniqueIds = [];
              const uniqueCLIds = [];
              const map = new Map();
              for (const item of a) {
                if(!map.has(item.clientId)){
                  map.set(item.clientId, true);    // set any value to Map
                  uniqueCLIds.push(item.clientId);
                }
              }
              const map1 = new Map();
              for (const item of a) {
                if(!map1.has(item.patientId)){
                  map1.set(item.patientId, true);    // set any value to Map
                  uniqueIds.push(item.patientId);
                }
              }

              // getPatReq.data.patientIds = uniqueIds;
              this.getPatClData(uniqueIds,caseData,uniqueCLIds,type).then(respAlldata => {
                resolve()


              });


            }
            else if (clientData['length'] == 0 && patientDat['data']['patients'] != null && caseData['data']['caseDetails']['length'] >0){
              this.handlePatientandCaseFromSearch(caseData,clientData, patientDat, type,searchString).then(handlePatientandCaseFromSearch=>{
                resolve()
                // console.log("handlePatientandCaseFromSearch",handlePatientandCaseFromSearch);
              })
            }
            else if (clientData['length'] > 0 && patientDat['data']['patients'] == null && caseData['data']['caseDetails']['length'] >0){
              this.handleClientandCaseFromSearch(caseData,clientData, patientDat, type,searchString).then(handlePatientandCaseFromSearch=>{
                resolve()
                // console.log("handlePatientandCaseFromSearch",handlePatientandCaseFromSearch);
              })
            }
            else if (clientData['length'] > 0 && patientDat['data']['patients'] != null && caseData['data']['caseDetails']['length'] >0){
              this.handleClientPatientandCaseFromSearch(caseData,clientData, patientDat, type,searchString).then(handlePatientandCaseFromSearch=>{
                resolve()
                // console.log("handlePatientandCaseFromSearch",handlePatientandCaseFromSearch);
              })
            }

            // }



          })
        }



        getPatClDataMegaSearch(userids,resp,clientids,type,searchString) {
          // this.ngxLoader.start();
          const patienByidURL = environment.API_PATIENT_ENDPOINT + 'patientbyids';
          var getPatReq       = {...this.patientByIdsReq}
          getPatReq.header.partnerCode  = this.logedInUserRoles.partnerCode;
          getPatReq.header.userCode     = this.logedInUserRoles.userCode;
          getPatReq.header.functionalityCode     = "SPCA-MC";
          var clURL           = environment.API_CLIENT_ENDPOINT + "clientidsnames"
          var clReqData = {
            header:{
              uuid               :"",
              partnerCode        :"",
              userCode           :"",
              referenceNumber    :"",
              systemCode         :"",
              moduleCode         :"CLIM",
              functionalityCode  :"SPCA-MC",
              systemHostAddress  :"",
              remoteUserAddress  :"",
              dateTime           :""
            },
            data:{
              clientIds:clientids
            }
          }
          clReqData['header'].partnerCode = this.logedInUserRoles.partnerCode;
          clReqData['header'].userCode = this.logedInUserRoles.userCode;
          clReqData['header'].functionalityCode = "SPCA-MC";
          // var clReqData       = {clientIds:clientids}
          getPatReq.data.patientIds = userids;
          return new Promise((resolve, reject) => {
            var patientout = this.specimenService.getPatientsByIds(patienByidURL, getPatReq).then(patient=>{
              return patient;
            })
            var clientsout = this.specimenService.getClientByName(clURL,clReqData).then(selectedClient => {
              return selectedClient['data'];
            });
            forkJoin([patientout, clientsout]).subscribe(bothresults => {
              console.log('bothresults FOrk Join------',bothresults);
              var patientresp = bothresults[0];
              var clients      = bothresults[1];
              console.log("resp['data']---------",resp['data']);

              if (patientresp['result'].codeType == "S") {
                if (typeof resp['data'] != "undefined") {
                  for (let i = 0; i < resp['data'].length; i++) {
                    for (let j = 0; j < patientresp['data'].length; j++) {
                      if (resp['data'][i].patientId == patientresp['data'][j].patientId) {
                        resp['data'][i].pFName = patientresp['data'][j].firstName;
                        resp['data'][i].pLName = patientresp['data'][j].lastName;
                        resp['data'][i].pDob   = patientresp['data'][j].dateOfBirth;
                      }
                      else{
                        if (typeof resp['data'][i].pFName == 'undefined') {
                          resp['data'][i].pFName = "no Name";
                          resp['data'][i].pLName = "no Name";
                          resp['data'][i].pDob   = "02-02-2021 19:10:00";
                        }
                        // resp['data'][i].pFName = "no Name";
                        // resp['data'][i].pLName = "no Name";
                        // resp['data'][i].pDob   = "02-02-2021 19:10:00";
                      }

                    }

                  }
                  // this.allCases       = resp['data'];
                  // this.allCasesCount  = resp['data']['totalCount'];
                }
                else{

                  // for (let i = 0; i < resp['data'].length; i++) {
                  //   for (let j = 0; j < patientresp['data'].length; j++) {
                  //     if (resp['data'][i].patientId == patientresp['data'][j].patientId) {
                  //       resp['data'][i].pFName = patientresp['data'][j].firstName;
                  //       resp['data'][i].pLName = patientresp['data'][j].lastName;
                  //       resp['data'][i].pDob   = patientresp['data'][j].dateOfBirth;
                  //     }
                  //
                  //   }
                  //
                  // }
                }
              }
              else{
                this.notifier.notify( "error", "Error while fetching patients" );
                // return;
              }

              if (clients['length'] > 0) {
                if (typeof resp['data'] != "undefined") {
                  // for (let i = 0; i < resp['data'].length; i++) {
                  for (let i = 0; i < resp['data'].length; i++) {
                    for (let j = 0; j < clients['length']; j++) {
                      if (resp['data'][i].clientId   == clients[j].clientId) {
                        resp['data'][i].clientName   = clients[j].clientName;
                      }
                      else{
                        if (typeof resp['data'][i].clientName =='undefined') {
                          resp['data'][i].clientName   = "no Name";
                        }

                      }

                    }

                  }
                }
                else{

                  // for (let i = 0; i < resp['data'].length; i++) {
                  // for (let i = 0; i < resp['data'].length; i++) {
                  //   for (let j = 0; j < clients['length']; j++) {
                  //     if (resp['data'][i].clientId   == clients[j].clientId) {
                  //       resp['data'][i].clientName   = clients[j].name;
                  //     }
                  //
                  //   }
                  //
                  // }
                }

              }
              else{
                // this.notifier.notify( "error", "No Client Found" );
                // return;
              }
              // var finalData = [];
              var finalDataCount = 0;
              // console.log("---resp['data']",resp['data']);

              for (let index = 0; index < resp['data']['length']; index++) {
                finalDataCount = finalDataCount +1
                if (resp['data'][index]['pFName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || resp['data'][index]['pLName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || this.datePipe.transform(resp['data'][index]['pDob'].toLowerCase(), 'MM/dd/yyyy').indexOf(searchString.toLowerCase()) !== -1
                || resp['data'][index]['clientName'].toLowerCase().indexOf(searchString.toLowerCase()) !== -1 || this.datePipe.transform(resp['data'][index]['collectionDate'].toLowerCase(), 'MM/dd/yyyy').indexOf(searchString.toLowerCase()) !== -1) {

                  if (searchString.toLowerCase() == 'client' || searchString.toLowerCase() == 'sip') {
                    if (resp['data'][index]['pFName'].toLowerCase().indexOf('client') === -1
                    && resp['data'][index]['pLName'].toLowerCase().indexOf('client') === -1
                    && resp['data'][index]['clientName'].toLowerCase().indexOf('client') === -1) {
                      resp['data'] = resp['data'].filter(function(value, index, arr){
                        // console.log("value.collectedBy",value.collectedBy);
                        return value.collectedBy == 2;
                      });

                    }
                    else if (resp['data'][index]['pFName'].toLowerCase().indexOf('sip') === -1
                    && resp['data'][index]['pLName'].toLowerCase().indexOf('sip') === -1
                    && resp['data'][index]['clientName'].toLowerCase().indexOf('sip') === -1) {
                      resp['data'] = resp['data'].filter(function(value, index, arr){
                        // console.log("value.collectedBy",value.collectedBy);
                        return value.collectedBy == 1;
                      });
                    }
                    if (type == 'active') {
                      this.activeCases.push(resp['data'][index]);
                      // resolve()
                    }
                    else if(type == 'all'){
                      this.allCases.push(resp['data'][index]);
                    }
                  }
                  else{
                    if (type == 'active') {
                      this.activeCases.push(resp['data'][index]);
                      // resolve()
                    }
                    else if(type == 'all'){
                      this.allCases.push(resp['data'][index]);
                    }
                  }
                  // if (type == 'active') {
                  //   this.activeCases.push(resp['data'][index]);
                  //   // resolve()
                  // }
                  // else if(type == 'all'){
                  //   this.allCases.push(resp['data'][index]);
                  // }
                }

                if (finalDataCount == resp['data']['length']) {
                  console.log('resp[dat]',resp['data']);

                  if (type == 'active') {
                    // this.activeCases = [];
                    this.activeCasesCount = this.activeCases['length'];
                    // this.activeCases = finalData;
                    this.ngxLoader.stop();
                    resolve()
                  }
                  else if(type == 'all'){
                    // this.allCases = [];
                    this.allCasesCount = this.allCases['length'];
                    // this.allCases = finalData;
                    this.ngxLoader.stop();
                    resolve()
                  }


                }

              }
              // console.log("finalData",finalData);
              // if (type == 'active') {
              //   if (typeof resp['data'] != "undefined") {
              //     this.activeCases       = finalData;
              //     this.activeCasesCount  = finalData['length'];
              //   }
              //   else{
              //     // this.activeCases       = resp['data'];
              //     // this.activeCasesCount  = resp['data']['totalCount'];
              //   }
              // }
              // else{
              //   if (typeof resp['data'] != "undefined") {
              //     this.allCases       = finalData;
              //     this.allCasesCount  = finalData['length'];
              //   }
              //   else{
              //     // this.allCases       = resp['data'];
              //     // this.allCasesCount  = resp['data']['totalCount'];
              //   }
              // }



              resolve();
            });
            // resolve();
          });
        }

        getindividualrColumnSearch (url, data,type) {
          // console.log("data",data);
          // console.log("url",url);
          return new Promise((resolve, reject) => {
            // console.log('f1');
            this.http.post(url,data).toPromise()
            .then( resp => {
              console.log('resp Active Cases Search: ', resp);
              if (resp['data']['caseDetails'].length == 0) {
                if (type == 'active') {
                  this.activeCases       = resp['data']['caseDetails'];
                  this.activeCasesCount  = resp['data']['totalCount'];
                }
                else if (type == 'all') {
                  this.allCases       = resp['data']['caseDetails'];
                  this.allCasesCount  = resp['data']['totalCount'];
                }

                resolve();
              }
              else{
                var a = resp['data']['caseDetails'];
                const uniqueIds = [];
                const uniqueCLIds = [];
                const map = new Map();
                for (const item of a) {
                  if(!map.has(item.clientId)){
                    map.set(item.clientId, true);    // set any value to Map
                    uniqueCLIds.push(item.clientId);
                  }
                }
                const map1 = new Map();
                for (const item of a) {
                  if(!map1.has(item.patientId)){
                    map1.set(item.patientId, true);    // set any value to Map
                    uniqueIds.push(item.patientId);
                  }
                }
                console.log("uniqueIds",uniqueIds);
                console.log("uniqueCLIds",uniqueCLIds);

                // getPatReq.data.patientIds = uniqueIds;
                this.getPatClData(uniqueIds,resp,uniqueCLIds,type).then(respAlldata => {

                  // callback({
                  //   recordsTotal    :  this.activeCasesCount,
                  //   recordsFiltered :  this.activeCasesCount,
                  //   data: []
                  // });
                  resolve();

                });
              }
              // return resp;
              // this.activeCases                 = resp['data']['caseDetails'];
              // this.activeCasesCount            = resp['data']['totalCount'];
              // return true;

            })
            .catch(error => {
              // return false;
              // console.log("error: ",error);
              this.notifier.notify( "error", "Error while loading search results DB connection")
              reject();
            });
            // resolve();
          });



        }
        getindividualrColumnSearchallCase(url, data,type) {
          return new Promise((resolve, reject) => {
            // console.log('f1');
            this.http.post(url,data).toPromise()
            .then( resp => {
              console.log('resp Active Cases Search: ', resp);
              // this.allCases                 = resp['data']['caseDetails'];
              // this.allCasesCount            = resp['data']['totalCount'];
              // // return true;
              // resolve();
              console.log('resp Active Cases Search: ', resp);
              if (resp['data']['caseDetails'].length == 0) {
                if (type == 'active') {
                  this.activeCases       = resp['data']['caseDetails'];
                  this.activeCasesCount  = resp['data']['totalCount'];
                }
                else if (type == 'all') {
                  this.allCases       = resp['data']['caseDetails'];
                  this.allCasesCount  = resp['data']['totalCount'];
                }

                resolve();
              }
              else{
                var a = resp['data']['caseDetails'];
                const uniqueIds = [];
                const uniqueCLIds = [];
                const map = new Map();
                for (const item of a) {
                  if(!map.has(item.clientId)){
                    map.set(item.clientId, true);    // set any value to Map
                    uniqueCLIds.push(item.clientId);
                  }
                }
                const map1 = new Map();
                for (const item of a) {
                  if(!map1.has(item.patientId)){
                    map1.set(item.patientId, true);    // set any value to Map
                    uniqueIds.push(item.patientId);
                  }
                }
                console.log("uniqueIds",uniqueIds);
                console.log("uniqueCLIds",uniqueCLIds);

                // getPatReq.data.patientIds = uniqueIds;
                this.getPatClData(uniqueIds,resp,uniqueCLIds,type).then(respAlldata => {

                  // callback({
                  //   recordsTotal    :  this.activeCasesCount,
                  //   recordsFiltered :  this.activeCasesCount,
                  //   data: []
                  // });
                  resolve();

                });
              }
              // return resp;
              // this.activeCases                 = resp['data']['caseDetails'];
              // this.activeCasesCount            = resp['data']['totalCount'];
              // return true;

            })
            .catch(error => {
              // return false;
              // console.log("error: ",error);
              this.notifier.notify( "error", "Error while loading case number search results")
              reject();
            });
            // resolve();
          });
        }

        getindividualrColumnSearchpatient (url, data,type) {
          // console.log("data",data);
          // console.log("url",url);
          return new Promise((resolve, reject) => {
            // console.log('f1');
            this.http.put(url,data).toPromise()
            .then( resultsColumn => {
              console.log('resp Active Cases Patiend Search:', resultsColumn);
              if (resultsColumn['data']['patients'] != null) {
                const uniqueIds = [];
                var b = resultsColumn['data']['patients'];
                const map1 = new Map();
                for (const item of b) {
                  if(!map1.has(item.patientId)){
                    map1.set(item.patientId, true);    // set any value to Map
                    uniqueIds.push(item.patientId);
                  }
                }
                console.log("inoquePateintId",uniqueIds);
                var intakeForSearch = ''
                var clientForSearch = ''
                var statuses        = []
                if (this.selectedIntakeIdForCases != null) {
                  intakeForSearch = this.selectedIntakeIdForCases
                  clientForSearch = this.selectedClientIdForCases
                  statuses        = [3,2,6,12,10,8,9,7,4,5]
                }
                else{
                  intakeForSearch = ""
                  clientForSearch = ""
                  statuses        = []
                }
                // var byPidURL = environment.API_SPECIMEN_ENDPOINT + 'findbypatientid';
                var byPidURL = environment.API_SPECIMEN_ENDPOINT + 'findcasesbypatientid';
                var reqPid={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-MC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},
                data:{
                  page:"0",
                  size:"20",
                  sortColumn:"createdTimestamp",
                  sortingOrder:"desc",
                  patientIds:uniqueIds,
                  intakeId:intakeForSearch,
                  client:clientForSearch,
                  statuses: statuses,
                  userCode:""
                }}
                this.specimenService.findByPatientIds(byPidURL, reqPid).subscribe(respPID=>{
                  console.log("by Patien ID Active Cases",respPID);
                  const uniqueCLIds = []
                  const map = new Map();
                  for (let i = 0; i < respPID['data']['caseDetails'].length; i++) {
                    for (let j = 0; j < resultsColumn['data']['patients'].length; j++) {
                      if(respPID['data']['caseDetails'][i].patientId == resultsColumn['data']['patients'].patientId){

                      }
                      for (const item of respPID['data']['caseDetails']) {
                        if(!map.has(item.clientId)){
                          map.set(item.clientId, true);    // set any value to Map
                          uniqueCLIds.push(item.clientId);
                        }
                      }

                    }

                  }
                  console.log("uniqueCLIds",uniqueCLIds);
                  // getPatReq.data.patientIds = uniqueIds;
                  this.getPatClData(uniqueIds,respPID,uniqueCLIds,type).then(respAlldata => {
                    // console.log("activeCases",this.activeCases);
                    resolve();

                    // callback({
                    //   recordsTotal    :  this.activeCasesCount,
                    //   recordsFiltered :  this.activeCasesCount,
                    //   data: []
                    // });

                  });


                },
                error=>{
                  this.notifier.notify( "error", "Error while loading Patient Connection Check");

                })

              }
              else{
                if (type == 'active') {
                  this.activeCases      = resultsColumn['data']['patients'];
                  this.activeCasesCount = 0
                }
                else if(type == 'all'){
                  this.allCases      = resultsColumn['data']['patients'];
                  this.allCasesCount = 0
                }

                resolve();
                // callback({
                //     recordsTotal    :  this.activeCasesCount,
                //     recordsFiltered :  this.activeCasesCount,
                //     data: []
                //   });
              }


            })
            .catch(error => {
              // return false;
              // console.log("error: ",error);
              this.notifier.notify( "error", "Error while loading search results DB connection")
              reject();
            });
            // resolve();
          });



        }

        // getindividualrColumnSearchpatient (url, data, type) {
        //   // console.log("data",data);
        //   // console.log("url",url);
        //   return new Promise((resolve, reject) => {
        //     // console.log('f1');
        //     this.http.put(url,data).toPromise()
        //     .then( resultsColumn => {
        //       console.log('resp Active Cases Patiend Search: DOB', resultsColumn);
        //       if (resultsColumn['data']['patients'] != null) {
        //         const uniqueIds = [];
        //         var b = resultsColumn['data']['patients'];
        //         const map1 = new Map();
        //         for (const item of b) {
        //           if(!map1.has(item.patientId)){
        //             map1.set(item.patientId, true);    // set any value to Map
        //             uniqueIds.push(item.patientId);
        //           }
        //         }
        //         console.log("inoquePateintId",uniqueIds);
        //         var byPidURL = environment.API_SPECIMEN_ENDPOINT + 'findbypatientid';
        //         var reqPid={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-MC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{patientIds:uniqueIds}}
        //         this.specimenService.findByPatientIds(byPidURL, reqPid).subscribe(respPID=>{
        //           console.log("by Patien ID Active Cases",respPID);
        //           const uniqueCLIds = []
        //           const map = new Map();
        //           for (let i = 0; i < respPID['data'].length; i++) {
        //             for (let j = 0; j < resultsColumn['data']['patients'].length; j++) {
        //               if(respPID['data'][i].patientId == resultsColumn['data']['patients'].patientId){
        //                 for (const item of respPID['data']) {
        //                   if(!map.has(item.clientId)){
        //                     map.set(item.clientId, true);    // set any value to Map
        //                     uniqueCLIds.push(item.clientId);
        //                   }
        //                 }
        //               }
        //
        //             }
        //
        //           }
        //           console.log("uniqueCLIds",uniqueCLIds);
        //           // getPatReq.data.patientIds = uniqueIds;
        //           this.getPatClData(uniqueIds,respPID,uniqueCLIds,type).then(respAlldata => {
        //             // console.log("activeCases",this.activeCases);
        //             resolve();
        //
        //             // callback({
        //             //   recordsTotal    :  this.activeCasesCount,
        //             //   recordsFiltered :  this.activeCasesCount,
        //             //   data: []
        //             // });
        //
        //           });
        //
        //
        //         },
        //         error=>{
        //           this.notifier.notify( "error", "Error while loading Patient Connection Check");
        //
        //         })
        //
        //       }
        //       else{
        //         this.allCases      = resultsColumn['data']['patients'];
        //         this.allCasesCount = resultsColumn['data']['totalPatients']
        //         resolve();
        //         // callback({
        //         //     recordsTotal    :  this.activeCasesCount,
        //         //     recordsFiltered :  this.activeCasesCount,
        //         //     data: []
        //         //   });
        //       }
        //
        //
        //     })
        //     .catch(error => {
        //       // return false;
        //       // console.log("error: ",error);
        //       this.notifier.notify( "error", "Error while loading search results DB connection")
        //       reject();
        //     });
        //     // resolve();
        //   });
        //
        //
        //
        // }


        ngAfterViewInit(): void {
          this.dtTrigger.next();
          this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
            dtElement.dtInstance.then((dtInstance: any) => {
              if (dtInstance.table().node().id == 'addactive_cases') {
                dtInstance.columns().every(function () {
                  const that = this;
                  ///////// case number search
                  $('#caseNoSearch', this.footer()).on('keyup', function () {
                    // console.log("herwe",this['value']);
                    // let url             = environment.API_SPECIMEN_ENDPOINT + 'searchbycasenumber';
                    // var searchCaseNoReq = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-MC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{page:"0",size:20,sortColumn:"",sortingOrder:"asc",searchString:""}}
                    // searchCaseNoReq.data.caseNumber = this['value'];
                    // this.getindividualrColumnSearch(url, searchCaseNoReq, that)

                    // console.log("searchCaseNoReq",searchCaseNoReq);

                    // if (that.search() !== this['value']) {
                    if (this['value'].length >=3 || this['value'].length == 0) {
                      that
                      .search(this['value'])
                      .draw();
                    }

                    // }
                  });
                  ////////////// Collected By
                  $('#collectedBySearch', this.footer()).on('change', function () {
                    that
                    .search(this['value'])
                    .draw();
                  });
                  /////// collection date filter
                  $('#collectionDateFilter', this.footer()).on('change', function () {
                    // var darr = this['value'].split("-");
                    // console.log("dar-----",darr);

                    // var searchString = darr[1]+"/"+darr[2]+"/"+darr[0];
                    // $('#pFnameSearch').val('');
                    // $('#pLnameSearch').val('');
                    // $('#dobFilter').val('');
                    var ar = this['value'].split('-');
                    console.log("ar",ar);
                    var newd = "";
                    if (ar[0] == "") {

                    }
                    else{
                      newd = ar[1]+"-"+ar[2]+"-"+ar[0];
                    }
                    that
                    .search(newd)
                    // .search(this['value'])
                    .draw();

                  });

                  /////// Select filter client name
                  $('#searchClient', this.footer()).on('keyup', function () {
                    console.log("this['value']",this['value']);

                    // console.log("SearchClient",this['value']);
                    if (this['value'].length >=3 || this['value'].length == 0) {
                    that
                    .search(this['value'])
                    .draw();
                    }


                  });

                  /////// Select Status
                  $('#searchStatus', this.footer()).on('change', function () {
                    that
                    .search(this['value'])
                    .draw();

                  });

                  /////// Patient First Name
                  $('#pFnameSearch', this.footer()).on('keyup', function () {


                    if (this['value'].length>=3 || this['value'].length==0) {
                      that
                      .search(this['value'])
                      .draw();
                    }

                  });

                  /////// Patient Last Name
                  $('#pLnameSearch', this.footer()).on('keyup', function () {
                    // console.log("this['value']",this['value']);

                    if (this['value'].length>=3 || this['value'].length==0) {
                      that
                      .search(this['value'])
                      .draw();
                    }


                  });

                  /////// Patient DOB
                  $('#dobFilter', this.footer()).on('keyup change', function () {
                    console.log("this['value']",this['value']);
                    var ar = this['value'].split('-');
                    console.log("ar",ar);
                    var newd = "";
                    if (ar[0] == "") {

                    }
                    else{
                      newd = ar[1]+"-"+ar[2]+"-"+ar[0];
                    }
                    // console.log("newd",newd);

                    // var newd = ar[1]+"-"+ar[2]+"-"+ar[0];

                    that
                    .search(newd)
                    .draw();

                  });
                  ///////// intake id search
                  $('#intakeIdFilter', this.footer()).on('keyup', function () {
                    if (this['value'].length >=3 || this['value'].length == 0) {
                      that
                      .search(this['value'])
                      .draw();
                    }
                  });
                  $('#batchIdFilter', this.footer()).on('keyup', function () {
                    if (this['value'].length >=3 || this['value'].length == 0) {
                      that
                      .search(this['value'])
                      .draw();
                    }
                  });

                  ///////// specimen type id search
                  $('#searchSpecimenType', this.footer()).on('change', function () {
                    // if (this['value'].length >=3 || this['value'].length == 0) {
                    that
                    .search(this['value'])
                    .draw();
                    // }
                  });


                });

              }
              else if (dtInstance.table().node().id == 'addall_cases') {
                dtInstance.columns().every(function () {
                  const that = this;
                  ///////// case number search
                  $('#caseNoSearch2', this.footer()).on('keyup', function () {
                    // let url             = environment.API_SPECIMEN_ENDPOINT + 'searchbycasenumber';
                    // var searchCaseNoReq = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-MC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{page:"0",size:20,sortColumn:"",sortingOrder:"asc",searchString:""}}
                    // searchCaseNoReq.data.caseNumber = this['value'];
                    // this.getindividualrColumnSearch(url, searchCaseNoReq, that)

                    // console.log("searchCaseNoReq",searchCaseNoReq);

                    // if (that.search() !== this['value']) {
                    if (this['value'].length >=3 || this['value'].length == 0) {
                      that
                      .search(this['value'])
                      .draw();
                    }

                    // }
                  });
                  ////////////// Collected By
                  $('#collectedBySearch2', this.footer()).on('change', function () {
                    that
                    .search(this['value'])
                    .draw();
                  });
                  /////// collection date filter
                  $('#collectionDateFilter2', this.footer()).on('change', function () {
                    // var darr = this['value'].split("-");
                    // var searchString = darr[1]+"/"+darr[2]+"/"+darr[0];
                    var ar = this['value'].split('-');
                    console.log("ar",ar);
                    var newd = "";
                    if (ar[0] == "") {

                    }
                    else{
                      newd = ar[1]+"-"+ar[2]+"-"+ar[0];
                    }
                    that
                    .search(newd)
                    // .search(this['value'])
                    .draw();

                  });

                  /////// Select filter client name
                  $('#searchClient2', this.footer()).on('keyup', function () {
                    if (this['value'].length >=3 || this['value'].length == 0) {
                    that
                    .search(this['value'])
                    .draw();
                    }


                  });

                  /////// Select Status
                  $('#searchStatus2', this.footer()).on('keyup change', function () {
                    that
                    .search(this['value'])
                    .draw();

                  });

                  /////// Patient First Name
                  $('#pFnameSearch2', this.footer()).on('keyup', function () {
                    if (this['value'].length>=3 || this['value'].length==0) {
                      that
                      .search(this['value'])
                      .draw();
                    }

                  });

                  /////// Patient Last Name
                  $('#pLnameSearch2', this.footer()).on('keyup', function () {
                    if (this['value'].length>=3 || this['value'].length==0) {
                      that
                      .search(this['value'])
                      .draw();
                    }

                  });

                  /////// Patient DOB
                  $('#dobFilter2', this.footer()).on('keyup change', function () {
                    // console.log("this['value']",this['value']);
                    var ar = this['value'].split('-');
                    // console.log("ar",ar);
                    // var newd = ar[1]+"-"+ar[2]+"-"+ar[0];
                    var newd = "";
                    if (ar[0] == "") {

                    }
                    else{
                      newd = ar[1]+"-"+ar[2]+"-"+ar[0];
                    }

                    that
                    .search(newd)
                    .draw();

                  });
                  ///////// case number search
                  $('#intakeIdFilter2', this.footer()).on('keyup', function () {
                    if (this['value'].length >=3 || this['value'].length == 0) {
                      that
                      .search(this['value'])
                      .draw();
                    }
                  });
                  ///////// specimen type id search
                  $('#searchSpecimenType2', this.footer()).on('change', function () {
                    // if (this['value'].length >=3 || this['value'].length == 0) {
                    that
                    .search(this['value'])
                    .draw();
                    // }
                  });
                  $('#batchIdFilter2', this.footer()).on('keyup', function () {
                    if (this['value'].length >=3 || this['value'].length == 0) {
                      that
                      .search(this['value'])
                      .draw();
                    }
                  });


                });

              }
            });
          });


          // this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          //   dtInstance.columns().every(function () {
          //     const that = this;
          //     $('input', this.footer()).on('keyup change', function () {
          //       if (that.search() !== this['value']) {
          //         that
          //         .search(this['value'])
          //         .draw();
          //       }
          //     });
          //     /////// collection date filter
          //     $('#collectionDateFilter', this.footer()).on('keyup change', function () {
          //       // console.log("that.search()",that.search());
          //       var darr = that.search().split("-");
          //       var searchString = darr[1]+"/"+darr[2]+"/"+darr[0];
          //       that
          //       .search(searchString)
          //       .draw();
          //
          //     });
          //
          //     /////// Select filter
          //     $('select', this.footer()).on('keyup change', function () {
          //       // console.log("this['value']",this['value']);
          //
          //       // var darr = that.search().split("-");
          //       // var searchString = darr[1]+"/"+darr[2]+"/"+darr[0];
          //       that
          //       .search(this['value'])
          //       .draw();
          //
          //     });
          //
          //
          //   });
          // });
        }
        popSearch(){
          this.searchtextHolder = this.cNameforSearch;
          $("#searchClient").trigger('change');
        }
        popSearch2(){
          this.searchtextHolder2 = this.cNameforSearch2;
          $("#searchClient2").trigger('change');
        }

        ngOnDestroy(): void {
          // Do not forget to unsubscribe the event
          this.dtTrigger.unsubscribe();
        }

        get f() { return this.caseForm.controls; }
        saveCase(saveType){
          $('#submitModal').modal('hide');
          console.log("saveType", saveType);
          this.submitted                  = true;
          if (this.caseForm.invalid) {
            // var casesDataBlueprint = { greenColor:true, caseNumber: 'null', caseId: 'null', pFName: 'null', pLName: 'null', pDob: 'null', clientName: 'null', collectionDate: 'null', collectedBy: 'null', caseStatusId: 'null', }
            // this.activeCases.unshift(casesDataBlueprint);
            // console.log("this.patientDOB",this.patientDOB);

            // this.printLabel("cccc",this.patientLName,this.patientFName,this.patientDOB)
            // alert('form invalid');
            // this.scrollToError();
            return;
          }
          if (saveType == 0) {
            this.saveRequest.data.caseStatusId = 1;
          }
          else if(saveType == 1){
            this.saveRequest.data.caseStatusId = 3;
          }
          else if(saveType == 2){
            this.saveRequest.data.caseStatusId = 3;
          }
          this.saveMinCase()

        }

        saveMinCase(){
          this.ngxLoader.start();
          var myDate = new Date();
          var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy');
          var currentTimeStamp = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');
          this.saveRequest.data.patientId = this.selectedPatientID;
          this.saveRequest.data.attendingPhysicianId = this.selectedAttPhysician.userCode;
          this.saveRequest.data.accessionTimestamp = currentTimeStamp;
          this.saveRequest.data.createdTimestamp = currentTimeStamp;

          this.saveRequest.data.collectionDate = this.datePipe.transform(this.saveRequestCollectionDate, 'MM-dd-yyyy HH:mm:ss');
          this.saveRequest.data.receivingDate = this.saveRequest.data.collectionDate;
          // this.saveRequest.data.caseSpecimen[0].specimenTypeId = this.specimenType.specimenTypeId;
          this.saveRequest.data.caseSpecimen[0].createdTimestamp = currentTimeStamp;
          this.saveRequest.data.caseSpecimen[0].createdBy = this.logedInUserRoles.userCode;
          this.saveRequest['header'].partnerCode = this.logedInUserRoles.partnerCode;
          this.saveRequest['header'].userCode = this.logedInUserRoles.userCode;
          this.saveRequest['header'].functionalityCode = "SPCA-MC";
          console.log("this.saveRequest",this.saveRequest);
          // this.ngxLoader.stop(); return;
          var saveminUrl = environment.API_SPECIMEN_ENDPOINT + "minaccession";
          this.specimenService.searchMinAccession(saveminUrl,this.saveRequest).subscribe(saveResp =>{
            this.greenColor = false;
            console.log("saveResp",saveResp);
            this.notifier.notify( "success", "Case saved successfully" );
            this.printedCaseNumber = saveResp['data'].caseNumber;
            this.submitted = false;
            // this.caseForm.reset();
            this.triageCase(saveResp['data'].caseId);
            this.resetPatient();
            // this.triageCase(saveResp['data'].caseid);

            this.greenColor = true;
            // saveResp['data'].greenColor = true;
            // this.activeCases.unshift(saveResp['data']);
            this.ngxLoader.stop();
            this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
              dtElement.dtInstance.then((dtInstance: any) => {
                // dtInstance.destroy();
                dtInstance.ajax.reload();
              })
            })
            // this.dtTrigger.next();
            if (this.saveTypeGlobal == 2) {

              // this.printLabel(saveResp['data'].caseNumber,this.patientLName,this.patientFName,this.patientDOB)
              this.printLabel(saveResp['data'].caseNumber,this.lNamePrint,this.fNamePrint,this.patientDOBPrint,this.clientNameToPrint,saveResp['data'].specimenTypeId,saveResp['data'].clientId,[],'empty')
            }
            // this.getLookups().then(lookupsLoaded => {
            //
            //   this.ngxLoader.stop();
            // });
          },
          error=>{
            this.ngxLoader.stop();
            this.notifier.notify( "error", "Error while saving case try changing patient information" );
          })



        }

        collectDateChange(e){
          console.log("ee",e.target.value);
          if (e.target.value > (this.maxDate+'T24:00')) {
            // alert("date greater then current"+e.target.value)
            // this.saveRequest.data.collectionDate = null;
            this.saveRequestCollectionDate = null;

          }
          // console.log("collectDate",this.collectDate);

        }

        clientChanged(){
          console.log("this.clientSearchString",this.clientSearchString);
          if (this.clientSearchString != null) {
            this.saveRequest.data.clientId = this.clientSearchString.clientId;
            this.clientNameToPrint = this.clientSearchString.name;

            console.log("client Changed",this.clientSearchString);
            if (this.clientSearchString.clientUsers.length >0) {

              const uniqueUIds = [];
              const map = new Map();
              for (const item of this.clientSearchString.clientUsers) {
                if(!map.has(item.userCode)){
                  map.set(item.userCode, true);    // set any value to Map
                  uniqueUIds.push(item.userCode);
                }
              }
              // console.log("physicianIDs",uniqueUIds);
              var attphysUrl = environment.API_USER_ENDPOINT + 'attendingphysicians'
              var reqData={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"",functionalityCode:"SPCA-MC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{userCodes:uniqueUIds}}
              reqData.header.userCode    = this.logedInUserRoles.userCode
              reqData.header.partnerCode = this.logedInUserRoles.partnerCode
              reqData.header.functionalityCode = "SPCA-MC"
              // console.log('reqData',reqData);
              this.specimenService.getAttPhysician(attphysUrl, reqData).subscribe(attResp => {
                console.log("att Physicain Resp",attResp);
                if (attResp['data'].length > 0) {

                  attResp['data'].map((i) => { i.name = i.firstName + ' ' + i.lastName; return i; });

                  // console.log('this.allAtdPhysicians',this.allAtdPhysicians);
                  this.allAtdPhysicians = attResp['data'];
                  for (let i = 0; i < this.clientSearchString.clientUsers.length; i++) {
                    for (let j = 0; j <   this.allAtdPhysicians.length; j++) {
                      if (this.clientSearchString.clientUsers[i].defaultPhysician == 1) {
                        if (  this.allAtdPhysicians[j].userCode == this.clientSearchString.clientUsers[i].userCode) {
                          this.selectedAttPhysician = this.allAtdPhysicians[j];
                          // console.log('here');
                          break;
                        }
                      }

                    }
                  }
                }
                else{
                  alert("no Physician found for this client please select a diffirent client")
                  this.saveRequest.data.clientId = null;
                  this.clientSearchString = null;
                }
                this.checkPatient();

              },
              error =>{
                this.ngxLoader.stop();
                this.notifier.notify( "error", "Error while loading Attending Physicians for the client")
              })


            }
            else{
              alert('client doesnot have a user please select a different client')
              this.clientSearchString = null;
              this.saveRequest.data.clientId = null;
            }

            ////////////////// select case category based on acc type\
            // console.log('1111',this.clientSearchString.clientTypes[0].clientTypeId);
            // console.log('2222',this.defaultAccountTypes);
            for (let m = 0; m < this.defaultAccountTypes.length; m++) {
              if (this.clientSearchString.clientTypes[0].clientTypeId != null) {
                if (this.defaultAccountTypes[m].clientTypeId == this.clientSearchString.clientTypes[0].clientTypeId) {
                  // console.log('-----',this.defaultAccountTypes[m]);

                  if (this.defaultAccountTypes[m].name.toLowerCase().indexOf('nursing')>-1 ) {
                    for (let i = 0; i < this.allCaseCategories.length; i++) {
                      if (this.allCaseCategories[i].caseCategoryName.toLowerCase().indexOf('nursing')>-1) {
                        this.selectedCaseCat = this.allCaseCategories[i]
                        this.saveRequest.data.caseCategoryId     = this.selectedCaseCat.caseCategoryId;
                        this.saveRequest.data.caseCategoryPrefix = this.selectedCaseCat.caseCategoryPrefix;
                      }
                    }
                  }
                  else if (this.defaultAccountTypes[m].name.toLowerCase().indexOf('surgical')>-1 ) {
                    for (let i = 0; i < this.allCaseCategories.length; i++) {
                      if (this.allCaseCategories[i].caseCategoryName.toLowerCase().indexOf('surgical')>-1) {
                        this.selectedCaseCat = this.allCaseCategories[i]
                        this.saveRequest.data.caseCategoryId     = this.selectedCaseCat.caseCategoryId;
                        this.saveRequest.data.caseCategoryPrefix = this.selectedCaseCat.caseCategoryPrefix;
                      }
                    }
                  }
                  else{
                    ///covid
                    for (let i = 0; i < this.allCaseCategories.length; i++) {
                      if (this.allCaseCategories[i].caseCategoryName.toLowerCase().indexOf('covid')>-1) {
                        this.selectedCaseCat = this.allCaseCategories[i]
                        this.saveRequest.data.caseCategoryId     = this.selectedCaseCat.caseCategoryId;
                        this.saveRequest.data.caseCategoryPrefix = this.selectedCaseCat.caseCategoryPrefix;
                      }
                    }
                  }
                }
              }

            }

          }
          else{
            this.saveRequest.data.clientId = null;
          }
        }

        // selectedPhysician(){
        //   console.log("attphysincan changed",this.selectedAttPhysician);
        //
        //   this.saveRequest.data.attendingPhysicianId = this.selectedAttPhysician.userCode;
        // }

        showSaveModal(saveType){
          this.saveTypeGlobal = saveType;
          if (saveType == 0) {
            this.saveModalTitle = "Save as Draft";
            this.saveModalText  = "The case has been saved as Draft";
            $('#submitModal').modal('show');
          }
          else if(saveType == 1){
            this.saveModalTitle = "Submit";
            this.saveModalText  = "Are you sure to submit, the case cannot be deleted after submission";
            $('#submitModal').modal('show');
          }
          else if(saveType == 2){
            this.saveModalTitle = "Submit";
            this.saveModalText  = "Are you sure to submit, the case cannot be deleted after submission";
            $('#submitModal').modal('show');
          }

        }

        checkPatient(){
          var ageDB, monthDB, dayDB, ageTocheck;
          if (this.patientDOB != null) {
            if (this.patientDOB > (this.maxDate)) {
              this.patientDOB = null;
              if (this.patientFName != null) {
                this.patientFName = this.patientFName.trim();

              }
              if (this.patientLName != null) {
                this.patientLName = this.patientLName.trim();
              }
              return;
            }
            var birthDate = new Date(this.patientDOB);

            // if ((birthDate.getMonth()+1) < 10) {
            //   monthDB = "0"+(birthDate.getMonth()+1);
            // }
            // else{
            //   monthDB = (birthDate.getMonth()+1);
            // }
            monthDB = (birthDate.getMonth()+1);
            if (birthDate.getDate() < 10) {
              dayDB = "0"+birthDate.getDate();
            }
            else{
              dayDB = birthDate.getDate();
            }
            ageDB = monthDB + "-" + dayDB + "-" + birthDate.getFullYear();
            // console.log("ageDB",ageDB);
            ageTocheck = ageDB;
            if (this.patientFName != null) {
              this.patientFName = this.patientFName.trim();

            }
            if (this.patientLName != null) {
              this.patientLName = this.patientLName.trim();
            }
          }
          if (this.patientFName != null) {
            this.patientFName = this.patientFName.trim();

          }
          if (this.patientLName != null) {
            this.patientLName = this.patientLName.trim();
          }
          var myDate = new Date();
          var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy');
          var currentTimeStamp = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');
          // console.log('patientFName',this.patientFName);
          // console.log('patientFName',this.patientLName);
          // console.log('patientDOB',this.patientDOB);
          // console.log("clientId",this.saveRequest.data.clientId);
          // this.saveRequest.data.clientId = 2
          if (this.patientFName != null && this.patientLName != null && this.patientDOB != null && this.saveRequest.data.clientId != null) {
            $('.existCheck-loadergif').css('display',"inline-block");
            var reqPatBP  = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"",functionalityCode:"SPCA-MC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{username:"danish",firstName:this.patientFName,lastName:this.patientLName,dateOfBirth:ageDB,clientId:this.saveRequest.data.clientId}}
            var findPTURL = environment.API_PATIENT_ENDPOINT + 'findpatient';
            this.specimenService.findPatient(findPTURL, reqPatBP).subscribe(reps => {
              if (reps['data'] == null) {
                ////// patient not found add patient
                this.patientDOBPrint = this.patientDOB;
                this.lNamePrint = this.patientLName;
                this.fNamePrint = this.patientFName;
                var savePURL = environment.API_PATIENT_ENDPOINT + 'save';
                var saveReq  = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"",functionalityCode:"SPCA-MC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{accountNumber:"ABC-21",clientId:this.saveRequest.data.clientId,createdSource:4,patientDemographics:{firstName:this.patientFName.trim(),lastName:this.patientLName.trim(),dateOfBirth:ageDB,createdBy:this.logedInUserRoles.userCode,createdTimestamp:currentTimeStamp,},
                createdBy:this.logedInUserRoles.userCode,createdTimestamp:currentTimeStamp,userName:this.logedInUserRoles.userCode}}
                this.specimenService.savePatient(savePURL, saveReq).subscribe(savePmin =>{
                  // console.log("Save with 3",saveReq);
                  // return;
                  $('.existCheck-loadergif').css('display',"none");

                  if (savePmin['result'].codeType == "S") {
                    this.patientFName = savePmin['data'].patientDemographics.firstName;
                    this.patientLName = savePmin['data'].patientDemographics.lastName;
                    // this.patientDOB   = savePmin['data'].patientDemographics.dateOfBirth;
                    this.selectedPatientID = savePmin['data'].patientId;
                    this.notifier.notify( "success", "Patient Created Successfully")

                  }
                  else{
                    console.log("error");
                    $('.existCheck-loadergif').css('display',"none");
                    this.notifier.notify( "error", "Error while saving patient")

                  }
                }, error =>{
                  this.ngxLoader.stop();
                  $('.existCheck-loadergif').css('display',"none");
                  this.notifier.notify( "error", "Error while saving Patient Connection Check")
                })
              }
              else if(reps['data'] != null) {
                Swal.fire({
                  title: 'Hi!',
                  text: "The Patient Already Exist Do you want to load it?",
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Yes, load it!',
                  allowOutsideClick:false
                }).then((result) => {
                  if (result.value) {
                    if (reps['data'].status != 1) {
                      this.notifier.notify('warning','The selected patien is inactive you will not be able to create case using this patient.')
                      $('.existCheck-loadergif').css('display',"none");
                      this.selectedPatientID = null;
                      // this.patientRecordUpdateafterCancel = false;
                      // this.patientLoadedCheck = false;
                    }
                    else{
                      //////// patient Found Save ID
                      this.selectedPatientID = reps['data'].patientId;
                      this.patientDOBPrint   = reps['data'].dateOfBirth;
                      this.lNamePrint        = reps['data'].lastName;
                      this.fNamePrint        = reps['data'].firstName;
                      $('.existCheck-loadergif').css('display',"none");
                      // For more information about handling dismissals please visit
                      // https://sweetalert2.github.io/#handling-dismissals
                    }
                  } else if (result.dismiss === Swal.DismissReason.cancel) {
                    $('.existCheck-loadergif').css('display',"none");
                    // this.resetPatient();
                  }
                  // //////// patient Found Save ID
                  // this.selectedPatientID = reps['data'].patientId;
                  // this.patientDOBPrint   = reps['data'].dateOfBirth;
                  // this.lNamePrint        = reps['data'].lastName;
                  // this.fNamePrint        = reps['data'].firstName;
                  // $('.existCheck-loadergif').css('display',"none");
                })
                //////// patient Found Save ID
                // this.selectedPatientID = reps['data'].patientId;
                // this.patientDOBPrint   = reps['data'].dateOfBirth;
                // this.lNamePrint        = reps['data'].lastName;
                // this.fNamePrint        = reps['data'].firstName;
                // $('.existCheck-loadergif').css('display',"none");
              }
            },error =>{
              this.ngxLoader.stop();
              $('.existCheck-loadergif').css('display',"none");
              this.resetPatient();
              this.notifier.notify( "error", "Error while loading Patient Connection Check")

            })
          }

        }

        submitCase(caseId, statusId,caseCate){
          var category = '';
          for (let i = 0; i < this.allCaseCategories.length; i++) {
            if (this.allCaseCategories[i].caseCategoryId == caseCate) {
              category = this.allCaseCategories[i].caseCategoryPrefix;
            }
          }
          var url = environment.API_SPECIMEN_ENDPOINT + 'updatecasestatus';
          var reqBody = {
            header: {
              uuid: "",
              partnerCode: "",
              userCode: "",
              referenceNumber: "",
              systemCode: "",
              moduleCode: "SPCM",
              functionalityCode: "SPCA-MC",
              systemHostAddress: "",
              remoteUserAddress: "",
              dateTime: ""
            },
            data: {
              caseId: caseId,
              caseNumber: "",
              caseStatusId: statusId,
              caseCategoryId: '',
              caseCategoryPrefix: category,
              updatedBy: "",
              updatedTimestamp: ""
            }
          }
          reqBody['header'].partnerCode = this.logedInUserRoles.partnerCode;
          reqBody['header'].userCode = this.logedInUserRoles.userCode;
          reqBody['header'].functionalityCode = "SPCA-MC";

          // console.log("reqBody",reqBody);
          //
          // return;
          var mes    = ""
          var text    = ""
          if (statusId == 11) {
            mes = "cancel"
            text = "Are you sure to "+ mes +" the case."
          }
          else if(statusId == 3) {
            mes = "submit"
            text = "Are you sure to "+ mes +" the case, the case cannot be deleted after submission."
          }
          Swal.fire({
            title: 'Are you sure?',
            text: text,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, do it!',
            allowOutsideClick:false
          }).then((result) => {
            if (result.value) {
              this.ngxLoader.start();
              this.specimenService.updateStatus(url, reqBody).subscribe(resp => {
                console.log('disable resp:', resp);
                this.ngxLoader.stop();
                if (resp['data'] > 0) {
                  this.greenColor = false;
                  this.notifier.notify( "success", "status changed successfully");
                  this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
                    dtElement.dtInstance.then((dtInstance: any) => {
                      dtInstance.ajax.reload();
                    })
                  })



                  // this.dtTrigger.next();
                  // this.resetFilters();


                }
                else{
                  this.ngxLoader.stop();
                  this.notifier.notify( "error", "Error while changing status");

                }
                // this.router.navigateByUrl('/edit-user', { state: resp });
                // this.router.navigate(['/editlisting', JSON.stringify(resp)]);
              },
              error =>{
                this.ngxLoader.stop();
                this.notifier.notify( "error", "Error while changing status Backend Connection");
              })
            }
            else{
              this.ngxLoader.stop();
            }
          })
        }

        updatecasestatus(caseId, statusId,caseCate){
          var category = '';
          for (let i = 0; i < this.allCaseCategories.length; i++) {
            if (this.allCaseCategories[i].caseCategoryId == caseCate) {
              category = this.allCaseCategories[i].caseCategoryPrefix;
            }
          }
          var url = environment.API_SPECIMEN_ENDPOINT + 'updatecasestatusin';
          var myDate = new Date();
          var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');
          var reqBody = {
            header: {
              uuid: "",
              partnerCode: "",
              userCode: "",
              referenceNumber: "",
              systemCode: "",
              moduleCode: "SPCM",
              functionalityCode: "SPCA-MC",
              systemHostAddress: "",
              remoteUserAddress: "",
              dateTime: ""
            },
            data: {
              caseStatusId:statusId,
              caseIds:[caseId],
              updatedBy: this.logedInUserRoles.userCode,
              updatedTimestamp:currentDate
            }
          }
          reqBody['header'].partnerCode = this.logedInUserRoles.partnerCode;
          reqBody['header'].userCode = this.logedInUserRoles.userCode;
          reqBody['header'].functionalityCode = "SPCA-MC";

          console.log("reqBody",reqBody);
          //
          // return;
          // return;
          var mes    = ""
          var text    = ""
          if (statusId == 11) {
            mes = "cancel"
            text = "Are you sure to "+ mes +" the case."
          }
          else if(statusId == 3) {
            mes = "submit"
            text = "Are you sure to "+ mes +" the case, the case cannot be deleted after submission."
          }
          Swal.fire({
            title: 'Are you sure?',
            text: text,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, do it!',
            allowOutsideClick:false
          }).then((result) => {
            if (result.value) {
              this.ngxLoader.start();
              this.specimenService.updateStatus(url, reqBody).subscribe(resp => {
                console.log('disable resp:', resp);
                this.ngxLoader.stop();
                if (resp['data'] > 0) {
                  this.greenColor = false;
                  this.notifier.notify( "success", "status changed successfully");
                  this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
                    dtElement.dtInstance.then((dtInstance: any) => {
                      dtInstance.ajax.reload();
                    })
                  })
                  if (statusId == 11) {
                    console.log('this.selectedIntakeIdForCases',this.selectedIntakeIdForCases);

                    this.sharedIntakeData.reloadEditIntakeAfteminAcceesion(this.selectedIntakeIdForCases)
                  }
                  // this.dtTrigger.next();
                  // this.resetFilters();


                }
                else{
                  this.ngxLoader.stop();
                  this.notifier.notify( "error", "Error while changing status");

                }
                // this.router.navigateByUrl('/edit-user', { state: resp });
                // this.router.navigate(['/editlisting', JSON.stringify(resp)]);
              },
              error =>{
                this.ngxLoader.stop();
                this.notifier.notify( "error", "Error while changing status Backend Connection");
              })
            }
            else{
              this.ngxLoader.stop();
            }
          })
        }


        deleteCase(caseId){
          var url = environment.API_SPECIMEN_ENDPOINT + 'deletecase';
          var reqBody={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-MC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{caseId:caseId}}
          Swal.fire({
            title: 'Are you sure?',
            text: "Are you sure to remove the case, it will no longer be accessible.",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, do it!',
            allowOutsideClick:false
          }).then((result) => {
            if (result.value) {
              this.ngxLoader.start();
              this.specimenService.deleteCase(url, reqBody).subscribe(resp => {
                console.log('disable resp:', resp);
                this.ngxLoader.stop();
                if (resp['result'].codeType == 'S') {
                  this.greenColor = false;
                  this.notifier.notify( "success", "case deleted successfully");
                  this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
                    dtElement.dtInstance.then((dtInstance: any) => {
                      // dtInstance.destroy();
                      dtInstance.ajax.reload();
                    })
                  })
                  // this.dtTrigger.next();


                }
                else{
                  this.notifier.notify( "error", resp['result'].description);

                }
                // this.router.navigateByUrl('/edit-user', { state: resp });
                // this.router.navigate(['/editlisting', JSON.stringify(resp)]);
              },
              error =>{
                this.ngxLoader.stop();
                this.notifier.notify( "error", "Error while changing status Backend Connection");
              })
            }
          })
        }

        resetFilters(){
          console.log('here');

          $('#pFnameSearch').val('');
          $('#pLnameSearch').val('');
          $('#dobFilter').val('');
          $('#caseNoSearch').val('');
          $('#collectedBySearch').val('');
          $('#collectionDateFilter').val('');
          $('#searchClient').val('');
          $('#searchStatus').val('');

          $('#pFnameSearch2').val('');
          $('#pLnameSearch2').val('');
          $('#dobFilter2').val('');
          $('#caseNoSearch2').val('');
          $('#collectedBySearch2').val('');
          $('#collectionDateFilter2').val('');
          $('#searchClient2').val('');
          $('#searchStatus2').val('');
        }

        printLabel(caseNumber,lName,fName,dob,clientName,casePrefix,clientId,specimenArray,specimenTypeName){
          if(typeof dymo == "undefined"){
            this.ngxLoader.stop();
            return;
          }

          this.ngxLoader.start();
          if (typeof lName == 'undefined') {lName = "No Name"}
          if (typeof fName == 'undefined') {fName = "No Name"}
          if (typeof dob == 'undefined') {dob = "No dob"}
          dob = this.datePipe.transform(dob, 'MM/dd/yyy');
          // this.ngxLoader.stop();
          console.log("caseNumber",caseNumber);
          console.log("lName",lName);
          console.log("fName",fName);
          console.log("dob",dob);
          console.log("clientName",clientName);
          console.log("casePrefix",casePrefix);
          console.log("specimenArray",specimenArray);
          console.log("specimenTypeName",specimenTypeName);
          // var maxDate=new Date(Math.max.apply(null,specimenArray));
          // console.log("maxDate",maxDate);



          var printTypeHolder = [];
          if (specimenTypeName != 'empty') {
            printTypeHolder.push(specimenTypeName);
          }
          else{
            const result = specimenArray.sort((a,b) => a.creationDate.localeCompare(b.creationDate))

            for (let j = 0; j < this.allSpecimenTypes.length; j++) {
              for (let k = 0; k < casePrefix.length; k++) {
                if (result[0]['specimenId'] == this.allSpecimenTypes[j].specimenTypeId) {
                  // cPrifix = cPrifix+' '+this.allSpecimenTypes[j]['specimenTypePrefix'];
                  printTypeHolder.push(this.allSpecimenTypes[j]['specimenTypePrefix'])
                }

              }


            }
          }

          console.log('printTypeHolder',printTypeHolder);


          // console.log("cPrifix",cPrifix);

          var labelXml = '<DieCutLabel Version="8.0" Units="twips" MediaType="Default"> <PaperOrientation>Landscape</PaperOrientation> <Id>Small30332</Id> <IsOutlined>false</IsOutlined> <PaperName>30332 1 in x 1 in</PaperName> <DrawCommands> <RoundRectangle X="0" Y="0" Width="1440" Height="1440" Rx="180" Ry="180" /> </DrawCommands> <ObjectInfo> <TextObject> <Name>FirstLast</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <HorizontalAlignment>Left</HorizontalAlignment> <VerticalAlignment>Top</VerticalAlignment> <TextFitMode>ShrinkToFit</TextFitMode> <UseFullFontHeight>True</UseFullFontHeight> <Verticalized>False</Verticalized> <StyledText> <Element> <String xml:space="preserve">First Last</String> <Attributes> <Font Family="Arial" Size="5" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" HueScale="100" /> </Attributes> </Element> </StyledText> </TextObject> <Bounds X="144" Y="199.393133236425" Width="1186.49076398256" Height="120" /> </ObjectInfo> <ObjectInfo> <BarcodeObject> <Name>BARCODE</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <Text>12345</Text> <Type>QRCode</Type> <Size>Medium</Size> <TextPosition>None</TextPosition> <TextFont Family="Arial" Size="8" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <CheckSumFont Family="Arial" Size="8" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <TextEmbedding>None</TextEmbedding> <ECLevel>0</ECLevel> <HorizontalAlignment>Center</HorizontalAlignment> <QuietZonesPadding Left="0" Top="0" Right="0" Bottom="0" /> </BarcodeObject> <Bounds X="381.704497509707" Y="771.219004098847" Width="541.292842170495" Height="526.701837189909" /> </ObjectInfo> <ObjectInfo> <TextObject> <Name>Facility</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <HorizontalAlignment>Left</HorizontalAlignment> <VerticalAlignment>Top</VerticalAlignment> <TextFitMode>ShrinkToFit</TextFitMode> <UseFullFontHeight>True</UseFullFontHeight> <Verticalized>False</Verticalized> <StyledText> <Element> <String xml:space="preserve">Facility</String> <Attributes> <Font Family="Arial" Size="4" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" HueScale="100" /> </Attributes> </Element> </StyledText> </TextObject> <Bounds X="164.897098682172" Y="651.471024372647" Width="1183.87862664729" Height="120" /> </ObjectInfo> <ObjectInfo> <TextObject> <Name>DOB</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <HorizontalAlignment>Left</HorizontalAlignment> <VerticalAlignment>Top</VerticalAlignment> <TextFitMode>ShrinkToFit</TextFitMode> <UseFullFontHeight>True</UseFullFontHeight> <Verticalized>False</Verticalized> <StyledText> <Element> <String xml:space="preserve">DOB</String> <Attributes> <Font Family="Arial" Size="5" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" HueScale="100" /> </Attributes> </Element> </StyledText> </TextObject> <Bounds X="144" Y="310.963055930226" Width="1210" Height="120" /> </ObjectInfo> <ObjectInfo> <TextObject> <Name>Client</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <HorizontalAlignment>Left</HorizontalAlignment> <VerticalAlignment>Top</VerticalAlignment> <TextFitMode>ShrinkToFit</TextFitMode> <UseFullFontHeight>True</UseFullFontHeight> <Verticalized>False</Verticalized> <StyledText> <Element> <String xml:space="preserve">Client</String> <Attributes> <Font Family="Arial" Size="5" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" HueScale="100" /> </Attributes> </Element> </StyledText> </TextObject> <Bounds X="144" Y="418.060686676358" Width="1210" Height="120" /> </ObjectInfo> <ObjectInfo> <TextObject> <Name>CaseCode</Name> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" /> <BackColor Alpha="0" Red="255" Green="255" Blue="255" /> <LinkedObjectName /> <Rotation>Rotation0</Rotation> <IsMirrored>False</IsMirrored> <IsVariable>False</IsVariable> <GroupID>-1</GroupID> <IsOutlined>False</IsOutlined> <HorizontalAlignment>Left</HorizontalAlignment> <VerticalAlignment>Top</VerticalAlignment> <TextFitMode>ShrinkToFit</TextFitMode> <UseFullFontHeight>True</UseFullFontHeight> <Verticalized>False</Verticalized> <StyledText> <Element> <String xml:space="preserve">Case Code</String> <Attributes> <Font Family="Arial" Size="5" Bold="False" Italic="False" Underline="False" Strikeout="False" /> <ForeColor Alpha="255" Red="0" Green="0" Blue="0" HueScale="100" /> </Attributes> </Element> </StyledText> </TextObject> <Bounds X="144" Y="525.15831742249" Width="1210" Height="120" /> </ObjectInfo> </DieCutLabel>';

          var label = dymo.label.framework.openLabelXml(labelXml);

          // var barcodeData = 'Si Paradigm'
          label.setObjectText('BARCODE', caseNumber);
          // set label text
          label.setObjectText("FirstLast",lName +','+ fName);
          label.setObjectText("DOB",  dob);
          // label.setObjectText("Client", clientName.substring(0, 10));
          label.setObjectText("Facility", printTypeHolder[0]);
          label.setObjectText("CaseCode", caseNumber);
          label.setObjectText("Client", clientName.substring(0, 10));


          var printers = dymo.label.framework.getPrinters();
          if (printers.length == 0){

            // var myDate = new Date();
            // var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyy HH:mm:ss');
            // var saveLabeRequest = {
            //   header: {
            //     uuid: "",
            //     partnerCode: "sip",
            //     userCode: "",
            //     referenceNumber: "",
            //     systemCode: "",
            //     moduleCode: "SPCM",
            //     functionalityCode: "SPCA-MC",
            //     systemHostAddress: "",
            //     remoteUserAddress: "",
            //     dateTime: ""
            //   },
            //   data:
            //   {
            //     caseNumber: "",
            //     clientId: 1522,
            //     createdTimestamp: "",
            //     printedBy: ""
            //   }
            // }
            //
            // saveLabeRequest.data.caseNumber = caseNumber;
            // saveLabeRequest.data.clientId = clientId;
            // saveLabeRequest.data.createdTimestamp = currentDate;
            // saveLabeRequest.data.printedBy = this.logedInUserRoles.userCode;
            // saveLabeRequest.header.userCode = this.logedInUserRoles.userCode;
            // saveLabeRequest.header.partnerCode = this.logedInUserRoles.partnerCode;
            // saveLabeRequest.header.functionalityCode = "SPCA-MC";
            //
            // var saveLabelUrl = environment.API_SPECIMEN_ENDPOINT+'savelabel';
            // this.specimenService.printLabel(saveLabelUrl,saveLabeRequest).subscribe(saveLabelResponse =>{
            //   console.log('saveLabelResponse',saveLabelResponse);
            //
            // },error=>{
            //   this.notifier.notify("error","Error while saveing print history.")
            // })


            this.notifier.notify("error","No DYMO printers are installed. Install DYMO printers.")
            this.ngxLoader.stop()
            throw "No DYMO printers are installed. Install DYMO printers.";

          }
          else{
            var myDate = new Date();
            var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyy HH:mm:ss');
            var saveLabeRequest = {
              header: {
                uuid: "",
                partnerCode: "sip",
                userCode: "",
                referenceNumber: "",
                systemCode: "",
                moduleCode: "SPCM",
                functionalityCode: "SPCA-MC",
                systemHostAddress: "",
                remoteUserAddress: "",
                dateTime: ""
              },
              data:
              {
                caseNumber: "",
                clientId: 1522,
                createdTimestamp: "",
                printedBy: ""
              }
            }

            saveLabeRequest.data.caseNumber = caseNumber;
            saveLabeRequest.data.clientId = clientId;
            saveLabeRequest.data.createdTimestamp = currentDate;
            saveLabeRequest.data.printedBy = this.logedInUserRoles.userCode;
            saveLabeRequest.header.userCode = this.logedInUserRoles.userCode;
            saveLabeRequest.header.partnerCode = this.logedInUserRoles.partnerCode;
            saveLabeRequest.header.functionalityCode = "SPCA-MC";

            var saveLabelUrl = environment.API_SPECIMEN_ENDPOINT+'savelabel';
            this.specimenService.printLabel(saveLabelUrl,saveLabeRequest).subscribe(saveLabelResponse =>{
              console.log('saveLabelResponse',saveLabelResponse);

            },error=>{
              this.notifier.notify("error","Error while saveing print history.")
            })

          }

          var printerName = "";
          for (var i = 0; i < printers.length; ++i)
          {
            var printer = printers[i];
            if (printer.printerType == "LabelWriterPrinter")
            {
              printerName = printer.name;
              break;
            }
          }

          label.print(printerName);
          this.printedCaseNumber = caseNumber;
          console.log("this.printedCaseNumber--",this.printedCaseNumber);
          // $('#pecimenlabel-Modal').modal('show');
          $('#pecimenlabel-Modal').modal('show');
          this.ngxLoader.stop();

        }
        goToEditonCaseId(caseId){
          var a = caseId+';2';
          var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
          var bx = ax.replace('+','xsarm');
          var cx = bx.replace('/','ydan');
          var ciphertext = cx.replace('=','zhar');
          console.log('ciphertext',ciphertext);

          this.router.navigateByUrl('/edit-case/'+ciphertext+'/1')
          // this.router.navigateByUrl('/edit-case/'+caseId+'/1;from='+ 2)
        }
        goToEditonCaseNumber(caseNumber){
          var a = caseNumber+';0';
          var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
          var bx = ax.replace('+','xsarm');
          var cx = bx.replace('/','ydan');
          var ciphertext = cx.replace('=','zhar');
          console.log('ciphertext',ciphertext);
          // this.router.navigate(['edit-case', ciphertext]);
          this.router.navigateByUrl('/edit-case/'+ciphertext+'/1')
        }

        getLookups(){
          this.ngxLoader.start();
          return new Promise((resolve, reject) => {
            var caseCat = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=CASE_CATEGORY_CACHE&partnerCode=sip').then(response =>{
              return response;
            })
            var specType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=SPECIMEN_TYPE_CACHE&partnerCode=sip').then(response =>{
              return response;
            })
            var statusType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=CASE_STATUS_CACHE&partnerCode=sip').then(response =>{
              return response;
            })
            var accType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=CLIENT_TYPE_CACHE&partnerCode=sip').then(response =>{
              return response;
            })


            forkJoin([caseCat, specType, statusType, accType]).subscribe(allLookups => {
              console.log("allLookups",allLookups);
              this.allCaseCategories     = this.sortPipe.transform(allLookups[0], "asc", "caseCategoryName");;
              this.allSpecimenTypes      = this.sortPipe.transform(allLookups[1], "asc", "specimenTypeName");;
              this.specimenType          = this.allSpecimenTypes[1];
              this.allStatusType         = this.sortPipe.transform(allLookups[2], "asc", "caseStatusName");;
              console.log("this.allSpecimenTypes",this.allSpecimenTypes);
              this.defaultAccountTypes     = this.sortPipe.transform(allLookups[3], "asc", "name");
              for (let i = 0; i < this.allCaseCategories.length; i++) {
                if (this.allCaseCategories[i].caseCategoryDefault == 1) {
                  this.selectedCaseCat = this.allCaseCategories[i];
                  this.saveRequest.data.caseCategoryId     = this.allCaseCategories[i].caseCategoryId;
                  this.saveRequest.data.caseCategoryPrefix = this.allCaseCategories[i].caseCategoryPrefix;
                }
              }
              for (let j = 0; j < this.allSpecimenTypes.length; j++) {
                this.allSpecimenTypes[j]['labelToShow']   = this.allSpecimenTypes[j]['specimenTypePrefix']+' - '+this.allSpecimenTypes[j]['specimenTypeName'];
                this.allSpecimenTypes[j]['labelToSearch'] = this.allSpecimenTypes[j]['specimenTypePrefix'];

              }
              var forActive = [];
              for (let index = 0; index < this.allStatusType.length; index++) {
                if (this.allStatusType[index]['caseStatusId'] != 11 && this.allStatusType[index]['caseStatusId'] != 10) {
                  forActive.push(this.allStatusType[index]);
                }
                // else{
                //   console.log("-*-*-*this.allStatusType[index]['caseStatusId']",this.allStatusType[index]['caseStatusId']);
                //   console.log("-*-*-*this.selectedIntakeIdForCases",this.selectedIntakeIdForCases);
                //
                //
                //   if (this.allStatusType[index]['caseStatusId'] == 10) {
                //     if (this.selectedIntakeIdForCases != null) {
                //       forActive.push(this.allStatusType[index]);
                //     }
                //   }
                //
                // }

              }
              this.allStatusTypeForActive = forActive;
              resolve();

            })
          })

        }
        setCaseCatPrefix(){
          this.saveRequest.data.caseCategoryId     = this.selectedCaseCat.caseCategoryId;
          this.saveRequest.data.caseCategoryPrefix = this.selectedCaseCat.caseCategoryPrefix;
          // console.log("this.saveRequest",this.saveRequest);

        }
        changespecimenType(){
          var myDate = new Date();
          var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');
          this.saveRequest.data.caseSpecimen[0].specimenTypeId = this.specimenType.specimenTypeId;
          this.saveRequest.data.caseSpecimen[0].createdTimestamp = currentDate;
          this.saveRequest.data.caseSpecimen[0].createdBy = this.logedInUserRoles.userCode;
          // console.log("this.saveRequest",this.saveRequest);

        }
        resetPatient(){
          this.patientFName                     = null;
          this.patientLName                     = null;
          this.patientDOB                       = null;
          return;
        }

        triageCase(caseId){
          console.log("casid",caseId);

          var myDate = new Date();
          var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');
          var triageUrl = environment.API_COVID_ENDPOINT + 'triage';
          var triageRequest = {...this.triageRequest}
          triageRequest.data.caseId = caseId;
          triageRequest.data.testStatusId = 1;
          triageRequest.data.testId = 2;
          triageRequest.data.createdTimestamp = currentDate;
          triageRequest.data.createdBy = this.logedInUserRoles.userCode;
          triageRequest.header.functionalityCode = "SPCA-CQ";
          this.specimenService.triageRequest(triageUrl,triageRequest).subscribe(triageResp=>{
            console.log('triageResp',triageResp);
            this.ngxLoader.stop();

          },error=>{
            this.notifier.notify('error','error while triaging')
            this.ngxLoader.stop();
          })


        }

        deleteCaseFromIntake(caseId){
          // console.log("here",caseId);

          var url = environment.API_SPECIMEN_ENDPOINT + 'addremovecasesintake';
          var myDate = new Date();
          var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');
          var reqBody={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-MC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:
          {
            caseIds:[caseId],
            intakeId:this.selectedIntakeIdForCases,
            updatedBy:this.logedInUserRoles.userCode,
            updatedTimestamp:currentDate,
            operation:"remove"
          }
        }
        Swal.fire({
          title: 'Are you sure?',
          text: "Are you sure to remove the case from this intake.",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, do it!',
          allowOutsideClick:false
        }).then((result) => {
          if (result.value) {
            this.ngxLoader.start();
            this.specimenService.deleteCase(url, reqBody).subscribe(resp => {
              console.log('disable resp:', resp);
              this.ngxLoader.stop();
              if (resp['data'] >0) {
                this.greenColor = false;
                this.notifier.notify( "success", "case removed from intake successfully");
                this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
                  dtElement.dtInstance.then((dtInstance: any) => {
                    // dtInstance.destroy();
                    dtInstance.ajax.reload();
                  })
                })
                this.sharedIntakeData.reloadEditIntakeAfteminAcceesion(this.selectedIntakeIdForCases)
                // this.dtTrigger.next();


              }
              else{
                this.notifier.notify( "error", 'Error while removing case');

              }
              // this.router.navigateByUrl('/edit-user', { state: resp });
              // this.router.navigate(['/editlisting', JSON.stringify(resp)]);
            },
            error =>{
              this.ngxLoader.stop();
              this.notifier.notify( "error", "Error while removing case Backend Connection");
            })
          }
        })
      }

      openEditModalFromIntake(caseId){
        this.sharedIntakeData.editCasesInIntake(caseId);
        $('#createdetailcaseModal').modal('show');

      }

      printHistory(caseNumber){
        this.ngxLoader.start();
        this.specimenService.getreportingHistory(environment.API_RTL_ENDPOINT+'labelhostory?actionCode=SPCA-PL&entityCodeCaseNumber='+caseNumber).then(response =>{
          console.log("response history",response);
          this.ngxLoader.stop();
          if (response['length'] > 0) {
            var uniqueUserIds = []
            for (let i = 0; i < response['length']; i++) {
              if (uniqueUserIds.indexOf(response[i].printedBy) == -1) {
                uniqueUserIds.push(response[i].printedBy);
              }
            }
            var usersRequest = {
              header              : {
                uuid              : "",
                partnerCode       : "",
                userCode          : "",
                referenceNumber   : "",
                systemCode        : "",
                moduleCode        : "",
                functionalityCode : "",
                systemHostAddress : "",
                remoteUserAddress : "",
                dateTime          : ""
              },
              data                : {
                userCodes           :[]
              }
            }
            usersRequest.data.userCodes = uniqueUserIds;
            var userUrl = environment.API_USER_ENDPOINT + "clientusers";
            usersRequest.header.partnerCode  = this.logedInUserRoles.partnerCode;
            usersRequest.header.userCode     = this.logedInUserRoles.userCode;
            usersRequest.header.functionalityCode     = "SPCA-MC";
            this.specimenService.getUserRecord(userUrl,usersRequest).then(getUserResp =>{
              for (let i = 0; i < response['length']; i++) {
                for (let j = 0; j < getUserResp['data'].length; j++) {
                  if (getUserResp['data'][j].userCode == response[i].printedBy) {
                    response[i].printedByName = getUserResp['data'][j].fullName;
                  }

                }
              }

            })

            this.labelHistory = response;
            $('#spec-labelhistory-Modal').modal('show');
          }
          else{
            this.labelHistory = [];
            this.notifier.notify('warning','No history found')
          }
        },error=>{
          this.labelHistory = [];
          this.ngxLoader.stop();
          this.notifier.notify('error','Error while loading history')
        })
      }

      handlePatientandCaseFromSearch(caseData,clientData, patientDat, type,searchString){
        return new Promise((resolve, reject) => {
          var uniquePatientId = [];
          const map = new Map();
          for (const item of patientDat['data']['patients']) {
            if(!map.has(item.patientId)){
              map.set(item.patientId, true);    // set any value to Map
              uniquePatientId.push(item.patientId);

            }
          }
          var intakeForSearch = ''
          var clientForSearch = ''
          var statuses        = []
          if (this.selectedIntakeIdForCases != null) {
            intakeForSearch = this.selectedIntakeIdForCases
            clientForSearch = this.selectedClientIdForCases
            statuses        = [3,2,6,12,10,8,9,7,4,5]
          }
          else{
            intakeForSearch = ""
            clientForSearch = ""
            statuses        = []
          }
          var byPidURL = environment.API_SPECIMEN_ENDPOINT + 'findbypatientid';
          var reqPid={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-MC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{
            patientIds:uniquePatientId,intakeId:intakeForSearch,client:clientForSearch,statuses:statuses, userCode:""}}
            this.specimenService.findByPatientIds(byPidURL, reqPid).subscribe(respPID=>{
              console.log("Ptient and case Patient",respPID);
              console.log("caseData",caseData);
              if (respPID['data']['length'] == 0) {
                var a9 = caseData['data']['caseDetails'];
                const uniqueIds = [];
                const uniqueCLIds = [];
                const map = new Map();
                for (const item of a9) {
                  if(!map.has(item.clientId)){
                    map.set(item.clientId, true);    // set any value to Map
                    uniqueCLIds.push(item.clientId);
                  }
                }
                const map1 = new Map();
                for (const item of a9) {
                  if(!map1.has(item.patientId)){
                    map1.set(item.patientId, true);    // set any value to Map
                    uniqueIds.push(item.patientId);
                  }
                }
                this.getPatClData(uniqueIds,caseData,uniqueCLIds,type).then(respAlldata => {
                  resolve()
                });

              }
              else{
                for (let i = 0; i < respPID['data'].length; i++) {
                  for (let j = 0; j < caseData['data']['caseDetails'].length; j++) {
                    if (respPID['data'][i]['caseId'] == caseData['data']['caseDetails'][j]['caseId']) {
                      respPID['data'].splice(i, 1);
                      i--;
                    }

                  }

                }
                const combined2 = [...respPID['data'], ...caseData['data']['caseDetails']];
                console.log("combined2",combined2);
                var combinedDataToForward = {
                  data:{}
                }
                var a8 = combined2;
                const uniqueIds = [];
                const uniqueCLIds = [];
                const map = new Map();
                for (const item of a8) {
                  if(!map.has(item.clientId)){
                    map.set(item.clientId, true);    // set any value to Map
                    uniqueCLIds.push(item.clientId);
                  }
                }
                const map1 = new Map();
                for (const item of a8) {
                  if(!map1.has(item.patientId)){
                    map1.set(item.patientId, true);    // set any value to Map
                    uniqueIds.push(item.patientId);
                  }
                }
                combinedDataToForward.data = combined2

                // getPatReq.data.patientIds = uniqueIds;
                this.getPatClData(uniqueIds,combinedDataToForward,uniqueCLIds,type).then(respAlldata => {
                  resolve()
                });

              }



            })
            resolve()
          })
        }

        handleClientandCaseFromSearch(caseData,clientData, patientDat, type,searchString){
          return new Promise((resolve, reject) => {
            var uniqueClientId = [];
            const map1 = new Map();
            for (const item of clientData) {
              if(!map1.has(item.clientId)){
                map1.set(item.clientId, true);    // set any value to Map
                uniqueClientId.push(item.clientId);
              }
            }
            var intakeForSearch = ''
            // var clientForSearch = ''
            if (this.selectedIntakeIdForCases != null) {
              intakeForSearch = this.selectedIntakeIdForCases
              // clientForSearch = this.selectedClientIdForCases
            }
            else{
              intakeForSearch = ""
              // clientForSearch = ""
            }
            let columnCaseReq  = {}
            var columnUrl      = environment.API_SPECIMEN_ENDPOINT + 'searchbyclient';
            columnCaseReq      = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-MC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},
            data:{page:"0",size:20,sortColumn:"caseNumber",sortingOrder:"desc",
            clientId: uniqueClientId,
            intakeId: intakeForSearch,
            statuses:[],
            userCode:""}}
            this.http.post(columnUrl, columnCaseReq).subscribe(byClientIdsResp=>{
              console.log("Client and Case Resp",byClientIdsResp);
              if (byClientIdsResp['data']['caseDetails'].length == 0) {
                var ab = caseData['data']['caseDetails'];
                const uniqueIds = [];
                const uniqueCLIds = [];
                const map = new Map();
                for (const item of ab) {
                  if(!map.has(item.clientId)){
                    map.set(item.clientId, true);    // set any value to Map
                    uniqueCLIds.push(item.clientId);
                  }
                }
                const map1 = new Map();
                for (const item of ab) {
                  if(!map1.has(item.patientId)){
                    map1.set(item.patientId, true);    // set any value to Map
                    uniqueIds.push(item.patientId);
                  }
                }
                this.getPatClData(uniqueIds,caseData,uniqueCLIds,type).then(respAlldata => {
                  resolve()
                });
              }
              else{
                const combined2 = [...byClientIdsResp['data']['caseDetails'], ...caseData['data']['caseDetails']];
                console.log("combined2",combined2);
                var combinedDataToForward = {
                  data:{}
                }
                var aa = combined2;
                const uniqueIds = [];
                const uniqueCLIds = [];
                const map = new Map();
                for (const item of aa) {
                  if(!map.has(item.clientId)){
                    map.set(item.clientId, true);    // set any value to Map
                    uniqueCLIds.push(item.clientId);
                  }
                }
                const map1 = new Map();
                for (const item of aa) {
                  if(!map1.has(item.patientId)){
                    map1.set(item.patientId, true);    // set any value to Map
                    uniqueIds.push(item.patientId);
                  }
                }
                combinedDataToForward.data = combined2

                // getPatReq.data.patientIds = uniqueIds;
                this.getPatClData(uniqueIds,combinedDataToForward,uniqueCLIds,type).then(respAlldata => {
                  resolve()
                });

              }



            },error=>{
              resolve()
            })
            // resolve()
          })
        }

        handleClientPatientandCaseFromSearch(caseData,clientData, patientDat, type,searchString){
          return new Promise((resolve, reject) => {
            var uniqueClientId = [];
            const map1 = new Map();
            for (const item of clientData) {
              if(!map1.has(item.clientId)){
                map1.set(item.clientId, true);    // set any value to Map
                uniqueClientId.push(item.clientId);
              }
            }
            var uniquePatientId = [];
            const map = new Map();
            for (const item of patientDat['data']['patients']) {
              if(!map.has(item.patientId)){
                map.set(item.patientId, true);    // set any value to Map
                uniquePatientId.push(item.patientId);

              }
            }
            var intakeForSearch = ''
            // var clientForSearch = ''
            if (this.selectedIntakeIdForCases != null) {
              intakeForSearch = this.selectedIntakeIdForCases
              // clientForSearch = this.selectedClientIdForCases
            }
            else{
              intakeForSearch = ""
              // clientForSearch = ""
            }
            let columnCaseReq  = {}
            var columnUrl      = environment.API_SPECIMEN_ENDPOINT + 'searchbyclient';
            columnCaseReq      = {header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-MC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},
            data:{page:"0",size:20,sortColumn:"caseNumber",sortingOrder:"desc",
            clientId: uniqueClientId,
            intakeId: intakeForSearch,
            statuses:[],
            userCode:""}}
            var clientsout = this.http.post(columnUrl, columnCaseReq).toPromise().then(byClientIdsResp=>{
              console.log("Client and Case Resp",byClientIdsResp);
              return byClientIdsResp;
            })
            var intakeForSearch1 = ''
            var clientForSearch1 = ''
            var statuses1        = []
            if (this.selectedIntakeIdForCases != null) {
              intakeForSearch1 = this.selectedIntakeIdForCases
              clientForSearch1 = this.selectedClientIdForCases
              statuses1        = [3,2,6,12,10,8,9,7,4,5]
            }
            else{
              intakeForSearch1 = ""
              clientForSearch1 = ""
              statuses1        = []
            }
            var byPidURL = environment.API_SPECIMEN_ENDPOINT + 'findbypatientid';
            var reqPid={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"SPCA-MC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{
              patientIds:uniquePatientId,intakeId:intakeForSearch1,client:clientForSearch1,statuses:statuses1, userCode:""}}
              var patientout = this.http.put(byPidURL, reqPid).toPromise().then(respPID=>{
                console.log("Ptient and case Patient",respPID);
                if (typeof respPID['message'] != 'undefined') {
                  var respPID1 = [];
                  respPID1['data'] = [];
                  return respPID1;
                }
                else{
                  return respPID;

                }
              },error=>{
                var respPID = [];
                respPID['data'] = [];
                return respPID;
              })


              forkJoin([patientout, clientsout]).subscribe(bothresults => {
                var byClientIdsResp = bothresults[1];
                var respPID= bothresults[0];

                if (byClientIdsResp['data']['caseDetails'].length == 0 && respPID['data']['length'] == 0) {
                  var a1 = caseData['data']['caseDetails'];
                  const uniqueIds = [];
                  const uniqueCLIds = [];
                  const map = new Map();
                  for (const item of a1) {
                    if(!map.has(item.clientId)){
                      map.set(item.clientId, true);    // set any value to Map
                      uniqueCLIds.push(item.clientId);
                    }
                  }
                  const map1 = new Map();
                  for (const item of a1) {
                    if(!map1.has(item.patientId)){
                      map1.set(item.patientId, true);    // set any value to Map
                      uniqueIds.push(item.patientId);
                    }
                  }
                  this.getPatClData(uniqueIds,caseData,uniqueCLIds,type).then(respAlldata => {
                    resolve()
                  });
                }
                else if (byClientIdsResp['data']['caseDetails'].length > 0 && respPID['data']['length'] == 0) {
                  const combined2 = [...byClientIdsResp['data']['caseDetails'], ...caseData['data']['caseDetails']];
                  console.log("combined2",combined2);
                  var combinedDataToForward = {
                    data:{}
                  }
                  var a2 = combined2;
                  const uniqueIds = [];
                  const uniqueCLIds = [];
                  const map = new Map();
                  for (const item of a2) {
                    if(!map.has(item.clientId)){
                      map.set(item.clientId, true);    // set any value to Map
                      uniqueCLIds.push(item.clientId);
                    }
                  }
                  const map1 = new Map();
                  for (const item of a2) {
                    if(!map1.has(item.patientId)){
                      map1.set(item.patientId, true);    // set any value to Map
                      uniqueIds.push(item.patientId);
                    }
                  }
                  combinedDataToForward.data = combined2

                  // getPatReq.data.patientIds = uniqueIds;
                  this.getPatClData(uniqueIds,combinedDataToForward,uniqueCLIds,type).then(respAlldata => {
                    resolve()
                  });
                }
                else if (byClientIdsResp['data']['caseDetails'].length == 0 && respPID['data']['length'] > 0) {
                  const combined2 = [...respPID['data'], ...caseData['data']['caseDetails']];
                  console.log("combined2",combined2);
                  var combinedDataToForward = {
                    data:{}
                  }
                  var a3 = combined2;
                  const uniqueIds = [];
                  const uniqueCLIds = [];
                  const map = new Map();
                  for (const item of a3) {
                    if(!map.has(item.clientId)){
                      map.set(item.clientId, true);    // set any value to Map
                      uniqueCLIds.push(item.clientId);
                    }
                  }
                  const map1 = new Map();
                  for (const item of a3) {
                    if(!map1.has(item.patientId)){
                      map1.set(item.patientId, true);    // set any value to Map
                      uniqueIds.push(item.patientId);
                    }
                  }
                  combinedDataToForward.data = combined2

                  // getPatReq.data.patientIds = uniqueIds;
                  this.getPatClData(uniqueIds,combinedDataToForward,uniqueCLIds,type).then(respAlldata => {
                    resolve()
                  });
                }
                else if (byClientIdsResp['data']['caseDetails'].length > 0 && respPID['data']['length'] > 0) {
                  const combined2 = [...respPID['data'], ...caseData['data']['caseDetails']];
                  const combined3 = [...byClientIdsResp['data']['caseDetails'], ...combined2];
                  console.log("combined3",combined3);
                  var combinedDataToForward = {
                    data:{}
                  }
                  var a4 = combined3;
                  const uniqueIds = [];
                  const uniqueCLIds = [];
                  const map = new Map();
                  for (const item of a4) {
                    if(!map.has(item.clientId)){
                      map.set(item.clientId, true);    // set any value to Map
                      uniqueCLIds.push(item.clientId);
                    }
                  }
                  const map1 = new Map();
                  for (const item of a4) {
                    if(!map1.has(item.patientId)){
                      map1.set(item.patientId, true);    // set any value to Map
                      uniqueIds.push(item.patientId);
                    }
                  }
                  combinedDataToForward.data = combined3;

                  // getPatReq.data.patientIds = uniqueIds;
                  this.getPatClData(uniqueIds,combinedDataToForward,uniqueCLIds,type).then(respAlldata => {
                    resolve()
                  });
                }



                resolve()
              })
            })
          }

          goToIntake(intakeId){
            var ax = CryptoJS.AES.encrypt(intakeId, 'nglis', { outputLength: 224 }).toString();
            var bx = ax.replace('+','xsarm');
            var cx = bx.replace('/','ydan');
            var ciphertext = cx.replace('=','zhar');

            this.router.navigate(['edit-intake', ciphertext]);

          }

          goToBatch(batchId){
            var ax = CryptoJS.AES.encrypt(batchId, 'nglis', { outputLength: 224 }).toString();
            var bx = ax.replace('+','xsarm');
            var cx = bx.replace('/','ydan');
            var ciphertext = cx.replace('=','zhar');

            this.router.navigate(['edit-batch', ciphertext]);

          }

          activeClick(){

            $('#all_casestab').hide();
            $('#active_casestab').show();
            this.activecasesTabPan  = true;
            this.allcasesTabPan = false;
            // $('#active_casestab').trigger('click')

          }
          allClick(){
            // console.log("allhere");

            $('#all_casestab').show();
            $('#active_casestab').hide();
            this.activecasesTabPan  = false;
            this.allcasesTabPan = true;
            // $('#all_casestab').trigger('click')
          }



        }
