import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { environment } from '../../../environments/environment';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import Swal from 'sweetalert2'
declare var $ :any;
import { NotifierService } from 'angular-notifier';
import { SpecimenApiCallsService } from '../../services/specimen/specimen-api-calls.service';
import { concat, Observable, of, Subject } from "rxjs";
// import { catchError, debounceTime, distinctUntilChanged, switchMap, tap } from "rxjs/operators";
import * as dymo from '../../../assets/js/dymo-label.js';
declare var dymo: any;
import * as CanvasJS from '../../../assets/js/canvasjs.min.js';
import * as moment from 'moment';

@Component({
  selector   : 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls  : ['./dashboard.component.css'],
  host       : {
    class    :'mainComponentStyle'
  }
})
export class DashboardComponent implements OnInit {

  firstGraphData        = [];
  footerDraftTotal      = 0;
  footerAccessTotal     = 0;
  footerPrintedTotal    = 0;
  footernotPrintedTotal = 0;
  firstSelected         ={startDate: moment(), endDate: moment()};
  secondSelected        ={startDate: moment(), endDate: moment()};

  swalStyle              : any;
  constructor(
    private formBuilder     : FormBuilder,
    private http            : HttpClient,
    private router          : Router ,
    private route           : ActivatedRoute,
    private sanitizer       : DomSanitizer,
    private ngxLoader       : NgxUiLoaderService,
    private notifier        : NotifierService,
    private specimenService : SpecimenApiCallsService
  ) {
    this.notifier        = notifier;
    this.swalStyle       =  Swal.mixin({
      customClass : {
        confirmButton : 'btn btn-success my-swal-success',
        cancelButton  : 'btn btn-danger'
      },
      buttonsStyling  : true
    })

  }

  ngOnInit(): void {
    var today = new Date();
    var ageDB = this.convertAge(today) ;

    console.log("ageDB",ageDB);

    var todayCaseFirstReq={header:{uuid:"",partnerCode:"",userCode:"",referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{searchDate:"2020-10-22",statuses:[1,2]}}
    var firstReqUrl      = environment.API_SPECIMEN_ENDPOINT + 'dashboardtoday';
    this.loadFirstGraph(todayCaseFirstReq, firstReqUrl);

    var todayCaseSecondReq={header:{uuid:"",partnerCode:"",userCode:"",referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{searchDate:"2020-10-22"}}
    var secondReqUrl      = environment.API_SPECIMEN_ENDPOINT + 'casestoday';
    this.loadSecondGraph(todayCaseSecondReq,secondReqUrl);

    var todayCaseThirdReq={header:{uuid:"",partnerCode:"",userCode:"",referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{}}
    var thirdReqUrl      = environment.API_SPECIMEN_ENDPOINT + 'casesforlastsevendays';
    this.loadThirdGraph(todayCaseThirdReq, thirdReqUrl);
  }

  loadFirstGraph(todayCaseFirstReq, firstReqUrl){
    this.ngxLoader.start();

    this.specimenService.todayCasesLoadFirstGraph(firstReqUrl,todayCaseFirstReq).subscribe(firstGraph=>{
      console.log("firstGraph",firstGraph);
      if (firstGraph['data'].length > 0) {
      const clientId = [];
      var tableData = [];
      const map = new Map();
      for (const item of firstGraph['data']) {
        if(!map.has(item.clientId)){
          map.set(item.clientId, true);    // set any value to Map
          clientId.push(item.clientId);
          tableData.push(item);
        }
      }
      var clURL           = environment.API_CLIENT_ENDPOINT + "clientsname"
      var clReqData       = {
        header:{
          uuid               :"",
          partnerCode        :"",
          userCode           :"",
          referenceNumber    :"",
          systemCode         :"",
          moduleCode         :"CLIM",
          functionalityCode  :"",
          systemHostAddress  :"",
          remoteUserAddress  :"",
          dateTime           :""
        },
        data:{
          clientIds:clientId}

        }
      this.specimenService.getClientByName(clURL,clReqData).then(selectedClient => {
        //console.log("selectedClient",selectedClient);
        for (let i = 0; i < firstGraph['data'].length; i++) {
          for (let j = 0; j < selectedClient['data']['length']; j++) {
            if (firstGraph['data'][i].clientId == selectedClient['data'][j].clientId) {
              firstGraph['data'][i].clientName = selectedClient['data'][j].name;
            }
            else{
              ////////////////// client not exist
              firstGraph['data'][i].clientName = 'no name'

            }

          }

        }

        ///////////////// draw table
        // var tableData = [];
        var tableHold = {};
        // var firstGraph = {...firstGraph};
        // console.log("firstGraph['data']",firstGraph);

          var loopCount = 0;
          for (let j = 0; j < tableData['length']; j++) {
            for (let i = 0; i < firstGraph['data'].length; i++) {
              if (firstGraph['data'][i].clientId == clientId[j]) {
                tableData[j]['clientId']   = firstGraph['data'][i].clientId;
                tableData[j]['clientName'] = firstGraph['data'][i].clientName;
                switch (firstGraph['data'][i]['isLabelPrinted']) {
                  case 1: /////////////printed labels
                  if (firstGraph['data'][i]['status'] == 1) {///////// Draft
                    var printCount = 0;
                    // if (typeof firstGraph['data'][i]['isLabelPrintedCount'] == 'undefined') {
                    //   firstGraph['data'][i]['isLabelPrintedCount']
                    // }
                    // var caseCount  = 0;
                    tableData[j]['DraftName']      = "Draft";
                    tableData[j]['DraftStatusId']   = firstGraph['data'][i]['status'];
                    printCount                     = printCount + firstGraph['data'][i]['isLabelPrintedCount'];
                    tableData[j]['Draftprinted']   = printCount;
                    // tableData[j]['DraftTotal']     = printCount;
                    // tableData[j]['printed']    = tableData[j]['clientName']['printed'] + firstGraph['data'][i]['isLabelPrintedCount'];

                  }
                  else if (firstGraph['data'][i]['status'] == 2) {///////// Accessioned
                    var printCount = 0;
                    tableData[j]['AccessionedName']       = "Accessioned";
                    tableData[j]['AccessionedStatusId']   = firstGraph['data'][i]['status'];
                    printCount                            = printCount + firstGraph['data'][i]['isLabelPrintedCount'];
                    tableData[j]['Accessionedprinted']    = printCount;
                    // tableData[j]['printed']    = tableData[j]['printed'] + firstGraph['data'][i]['isLabelPrintedCount'];
                  }
                  break;

                  case 0: /////////////not printed labels
                  if (firstGraph['data'][i]['status'] == 1) {///////// Draft
                    var printCount = 0;
                    tableData[j]['DraftName']    = "Draft";
                    tableData[j]['DraftStatusId'] = firstGraph['data'][i]['status'];
                    printCount                   = printCount + firstGraph['data'][i]['isLabelPrintedCount'];
                    tableData[j]['DraftnotPrinted']    = printCount;
                    // tableData[j]['notPrinted'] = tableData[j]['notPrinted'] + firstGraph['data'][i]['isLabelPrintedCount'];
                  }
                  else if (firstGraph['data'][i]['status'] == 2) {///////// Accessioned
                    var printCount = 0;
                    tableData[j]['AccessionedName']       = "Accessioned";
                    tableData[j]['AccessionedStatusId']   = firstGraph['data'][i]['status'];
                    printCount                            = printCount + firstGraph['data'][i]['isLabelPrintedCount'];
                    tableData[j]['AccessionedNotPrinted'] = printCount;
                    // tableData[j]['notPrinted'] = tableData[j]['notPrinted'] + firstGraph['data'][i]['isLabelPrintedCount'];
                  }
                  break;

                  default:
                  break;
                }
                // tableData.push(tableHold);
                // tableHold = {};
                var dp  = 0;
                var dnp = 0;
                var ap  = 0;
                var anp = 0;
                if (typeof tableData[j]['Draftprinted']== 'undefined') {
                  dp = 0;
                  // console.log("111");
                }
                else{
                  dp = tableData[j]['Draftprinted'];
                }
                if(typeof tableData[j]['DraftnotPrinted']== 'undefined') {
                  dnp = 0;
                  // console.log("222");
                }
                else{
                  dnp = tableData[j]['DraftnotPrinted']
                }
                if(typeof tableData[j]['Accessionedprinted']== 'undefined') {
                  ap = 0;
                }
                else{
                  ap = tableData[j]['Accessionedprinted'];
                }
                if(typeof tableData[j]['AccessionedNotPrinted']== 'undefined') {
                  anp = 0;
                  // console.log("444");
                }
                else{
                  anp = tableData[j]['AccessionedNotPrinted'];
                }
                tableData[j]['DraftTotal']       = dp  + dnp;
                tableData[j]['AccessionedTotal'] = ap  + anp;
                tableData[j]['printedTotal']     = dp  + ap;
                tableData[j]['notPrintedTotal']  = dnp + anp;
              }


            }
            loopCount = loopCount +1;
            if (loopCount == tableData['length']) {
              for (let k = 0; k < tableData['length']; k++) {
                this.footerDraftTotal       = this.footerDraftTotal      + tableData[k]['DraftTotal'];
                this.footerAccessTotal      = this.footerAccessTotal     + tableData[k]['AccessionedTotal'];
                this.footerPrintedTotal     = this.footerPrintedTotal    + tableData[k]['printedTotal'];
                this.footernotPrintedTotal  = this.footernotPrintedTotal + tableData[k]['notPrintedTotal'];

              }
            }


          }
          // console.log("firstGraph['data']",firstGraph);

          this.firstGraphData = tableData;
          this.ngxLoader.stop();

      });
    }
    else{
      this.ngxLoader.stop();
      this.firstGraphData = []
      this.footerDraftTotal       = 0;
      this.footerAccessTotal      = 0;
      this.footerPrintedTotal     = 0;
      this.footernotPrintedTotal  = 0;
    }

    },
    error=>{
      this.notifier.notify( "error", "Error while loading First Graph Backend Connection")
    })
  }

  loadSecondGraph(todayCaseFirstReq, firstReqUrl){
    this.ngxLoader.start();

    this.specimenService.todayCasesLoadFirstGraph(firstReqUrl,todayCaseFirstReq).subscribe(secondGraph=>{
      // console.log("secondGraph",secondGraph);
      if (secondGraph['data'].length > 0) {
        const clientId = [];
        const map = new Map();
        for (const item of secondGraph['data']) {
          if(!map.has(item.clientId)){
            map.set(item.clientId, true);    // set any value to Map
            clientId.push(item.clientId);
          }
        }
        var clURL           = environment.API_CLIENT_ENDPOINT + "clientsname"
        var clReqData       = {
          header:{
            uuid               :"",
            partnerCode        :"",
            userCode           :"",
            referenceNumber    :"",
            systemCode         :"",
            moduleCode         :"CLIM",
            functionalityCode  :"",
            systemHostAddress  :"",
            remoteUserAddress  :"",
            dateTime           :""
          },
          data:{
            clientIds:clientId}

          }
        // var clReqData       = {clientIds:clientId}
        var dataPoints      = [];
        var dpHolder        = {};
        this.specimenService.getClientByName(clURL,clReqData).then(selectedClient => {
          //console.log("selectedClient",selectedClient);
          for (let i = 0; i < secondGraph['data'].length; i++) {
            for (let j = 0; j < selectedClient['data']['length']; j++) {
              if (secondGraph['data'][i].clientId == selectedClient['data'][j].clientId) {
                secondGraph['data'][i].clientName = selectedClient['data'][j].name;
              }
              else{
                ////////////////// client not exist
                secondGraph['data'][i].clientName = 'no name'

              }

            }
            dpHolder['y']          = secondGraph['data'][i].totalCount;
            dpHolder['label']      = secondGraph['data'][i].clientName;
            dpHolder['legendText'] = secondGraph['data'][i].clientName;

            dataPoints.push(dpHolder);
            dpHolder = {};

          }
          // console.log("dpHolder",dpHolder);

          let chart = new CanvasJS.Chart("secondChartContainer", {
            animationEnabled: true,
            // exportEnabled: true,
            axisY:{
              interlacedColor: "rgb(255,250,250)",
              gridColor: "#FFBFD5"
            },

            dataPointWidth: 30,
            legend: {
              cursor: "pointer",
              // itemclick: this.toggleDataSeries,
              horizontalAlign: "right", // left, center ,right
              verticalAlign: "top",  // top, center, bottom
            },
            // title: {
            //   text: "Basic Column Chart in Angular"
            // },
            data: [{
              color: "#3366cc",
              type: "column",
              showInLegend: true,
              legendText: "Cases",
              // indexLabel: "{label}, {y}",
              indexLabel: "{y}",
              indexLabelPlacement: "inside",
              indexLabelOrientation: "horizontal",
              indexLabelFontColor  : "#fff",
              indexLabelFontStyle  : "bold",
              // dataPoints: dataPoints
              dataPoints: dataPoints
            }]
          });

          chart.render();
          this.ngxLoader.stop();




        });
      }
      else{
        dataPoints = [{'y':0,"label":'',"legendText":''}]
        let chart = new CanvasJS.Chart("secondChartContainer", {
          animationEnabled: true,
          // exportEnabled: true,
          axisY:{
            interlacedColor: "rgb(255,250,250)",
            gridColor: "#FFBFD5"
          },

          dataPointWidth: 30,
          legend: {
            cursor: "pointer",
            // itemclick: this.toggleDataSeries,
            horizontalAlign: "right", // left, center ,right
            verticalAlign: "top",  // top, center, bottom
          },
          // title: {
          //   text: "Basic Column Chart in Angular"
          // },
          data: [{
            color: "#3366cc",
            type: "column",
            showInLegend: true,
            legendText: "Cases",
            // indexLabel: "{label}, {y}",
            indexLabel: "{y}",
            indexLabelPlacement: "inside",
            indexLabelOrientation: "horizontal",
            indexLabelFontColor  : "#fff",
            indexLabelFontStyle  : "bold",
            // dataPoints: dataPoints
            dataPoints: dataPoints
          }]
        });

        chart.render();
        this.ngxLoader.stop();
      }


    },
    error=>{
      this.notifier.notify( "error", "Error while loading First Graph Backend Connection")
    })
  }

  loadThirdGraph(todayCaseFirstReq, firstReqUrl){

    this.specimenService.todayCasesLoadFirstGraph(firstReqUrl,todayCaseFirstReq).subscribe(thirdGraph=>{
      // console.log("thirdGraph",thirdGraph);
      const clientId = [];
      const map = new Map();
      for (const item of thirdGraph['data']) {
        if(!map.has(item.clientId)){
          map.set(item.clientId, true);    // set any value to Map
          clientId.push(item.clientId);
        }
      }
      var clURL           = environment.API_CLIENT_ENDPOINT + "clientsname"
      var clReqData       = {
        header:{
          uuid               :"",
          partnerCode        :"",
          userCode           :"",
          referenceNumber    :"",
          systemCode         :"",
          moduleCode         :"CLIM",
          functionalityCode  :"",
          systemHostAddress  :"",
          remoteUserAddress  :"",
          dateTime           :""
        },
        data:{
          clientIds:clientId}

        }
      // var clReqData       = {clientIds:clientId}
      var dataPoints      = [];
      var dpHolder        = {};
      this.specimenService.getClientByName(clURL,clReqData).then(selectedClient => {
        //console.log("selectedClient",selectedClient);
        for (let i = 0; i < thirdGraph['data'].length; i++) {
          for (let j = 0; j < selectedClient['data']['length']; j++) {
            if (thirdGraph['data'][i].clientId == selectedClient['data'][j].clientId) {
              thirdGraph['data'][i].clientName = selectedClient['data'][j].name;
            }
            else{
              ////////////////// client not exist
              thirdGraph['data'][i].clientName = 'no name'

            }

          }
          var today = new Date(thirdGraph['data'][i].date);
          var date = this.convertAge(today) ;

          dpHolder['y']          = thirdGraph['data'][i].totalCount;
          dpHolder['label']      = date;
          dpHolder['legendText'] = date;

          dataPoints.push(dpHolder);
          dpHolder = {};

        }
        // console.log("dataPoints",dataPoints);

        // console.log("thirdGraph['data']",thirdGraph['data']);
        var chart = new CanvasJS.Chart("thirdChartContainer", {
          animationEnabled: true,
          // theme: "light2",
          // title: {
          //   text: "Monthly Sales Data"
          // },
          // axisX: {
          //   valueFormatString: "MMM"
          // },
          axisY: {
            interlacedColor: "rgb(255,250,250)",
            gridColor: "#FFBFD5"
            // prefix: "$",
            // labelFormatter: this.addSymbols
          },
          dataPointWidth: 30,
          toolTip: {
            shared: false,
            content: "{label}, Cases: {y}"
          },
          legend: {
            cursor: "pointer",
            itemclick: this.toggleDataSeries,
            horizontalAlign: "right", // left, center ,right
            verticalAlign: "top",  // top, center, bottom
          },
          data: [
            {
              color: "#3366cc",
              type: "column",
              showInLegend: true,
              legendText: "Cases",
              // indexLabel: "{label}, {y}",
              indexLabel: "{y}",
              indexLabelPlacement: "inside",
              indexLabelOrientation: "horizontal",
              indexLabelFontColor  : "#fff",
              indexLabelFontStyle  : "bold",
              // xValueFormatString: "MMMM YYYY",
              // yValueFormatString: "$#,##0",
              dataPoints        : dataPoints
              // dataPoints: [
              //   { x: new Date(2016, 0), y: 20000 },
              //   { x: new Date(2016, 1), y: 30000 },
              //   { x: new Date(2016, 2), y: 25000 },
              //   { x: new Date(2016, 3), y: 70000, indexLabel: "High Renewals" },
              //   { x: new Date(2016, 4), y: 50000 },
              //   { x: new Date(2016, 5), y: 35000 },
              //   { x: new Date(2016, 6), y: 30000 },
              //   { x: new Date(2016, 7), y: 43000 },
              //   { x: new Date(2016, 8), y: 35000 },
              //   { x: new Date(2016, 9), y:  30000},
              //   { x: new Date(2016, 10), y: 40000 },
              //   { x: new Date(2016, 11), y: 50000 }
              // ]
            },
            {
              type: "line",
              name: "Average",
              showInLegend: true,
              legendText: "Average",
              // yValueFormatString: "$#,##0",
              dataPoints: dataPoints
            },
            // {
            //   type: "area",
            //   name: "Profit",
            //   markerBorderColor: "white",
            //   markerBorderThickness: 2,
            //   showInLegend: true,
            //   yValueFormatString: "$#,##0",
            //   dataPoints: dataPoints
            // }
          ]
          });
          chart.render();




        });

      },
      error=>{
        this.notifier.notify( "error", "Error while loading First Graph Backend Connection")
      })
    }
    addSymbols(e) {
      var suffixes = ["", "K", "M", "B"];
      var order = Math.max(Math.floor(Math.log(e.value) / Math.log(1000)), 0);

      if(order > suffixes.length - 1)
      order = suffixes.length - 1;

      var suffix = suffixes[order];
      return CanvasJS.formatNumber(e.value / Math.pow(1000, order)) + suffix;
    }

    toggleDataSeries(e) {
      if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
        e.dataSeries.visible = false;
      } else {
        e.dataSeries.visible = true;
      }
      e.chart.render();
    }

    convertAge(today){
      var monthDB ;
      var dayDB ;
      if ((today.getMonth()+1) < 10) {
        monthDB = "0"+(today.getMonth()+1);
      }
      else{
        monthDB = (today.getMonth()+1);
      }
      if (today.getDate() < 10) {
        dayDB = "0"+today.getDate();
      }
      else{
        dayDB = today.getDate();
      }
      return (today.getFullYear()  + "-" + monthDB + "-" + dayDB)
    }

    firstGraphBetween(e){
      console.log("----e",e);
      if (e.startDate == null || e.endDate == null) {
        return;
      }

      var date = new Date(e.startDate);
      var end  = new Date(e.endDate);
      var startDate = this.convertAge(date);
      var endDate   = this.convertAge(end);
      var todayCaseFirstReq={header:{uuid:"",partnerCode:"",userCode:"",referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{searchDate:startDate,toDate:endDate,statuses:[1,2]}}
      var firstReqUrl      = environment.API_SPECIMEN_ENDPOINT + 'dashboardbetweendates';
      this.loadFirstGraph(todayCaseFirstReq, firstReqUrl);
      //
      // // this.firstSelected.startDate

    }

    secondGraphBetween(e){
      // console.log("----e",e);
      if (e.startDate == null || e.endDate == null) {
        return;
      }

      var date = new Date(e.startDate);
      var end  = new Date(e.endDate);
      var startDate = this.convertAge(date);
      var endDate   = this.convertAge(end);
      var todayCaseSecondReq={header:{uuid:"",partnerCode:"",userCode:"",referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode:"",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{searchDate:startDate,toDate:endDate}}
      var secondReqUrl      = environment.API_SPECIMEN_ENDPOINT + 'countcasesbetweendates';
      this.loadSecondGraph(todayCaseSecondReq,secondReqUrl);
      //
      // // this.firstSelected.startDate

    }

    byName(clientId){
      // var data = {by:'clientId',id:clientId}
      // this.router.navigateByUrl('/manage-cases', { state: data });
    }
    caseDraft(clientId){
      var data = {by:'caseDraft',id:clientId}
      this.router.navigateByUrl('/manage-cases', { state: data });
    }
    caseAccessioned(clientId){
      var data = {by:'caseAccess',id:clientId}
      this.router.navigateByUrl('/manage-cases', { state: data });
    }
    byPrinted(clientId){
      // var data = {by:'caseAccess',id:clientId}
      // this.router.navigateByUrl('/manage-cases', { state: data });
    }
    byNotPrinted(clientId){

    }
    byTotalDraft(clientId){
      var data = {by:'totalDraft', id:null}
      this.router.navigateByUrl('/manage-cases', { state: data });
    }
    byTotalAccessioned(clientId){
      var data = {by:'totalAccess', id:null}
      this.router.navigateByUrl('/manage-cases', { state: data });
    }
    byTotalPrinted(clientId){

    }
    byTotalNotPrinted(clientId){

    }

  }
