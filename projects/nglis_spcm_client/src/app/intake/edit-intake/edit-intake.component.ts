import { Component, OnInit, OnDestroy,ElementRef, ViewChild, AfterViewInit, ViewChildren, QueryList } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { environment } from '../../../../../../src/environments/environment';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import Swal from 'sweetalert2'
declare var $ :any;
import { NotifierService } from 'angular-notifier';
import { SpecimenApiCallsService } from '../../services/specimen/specimen-api-calls.service';
import { GlobalApiCallsService } from '../../services/global/global-api-calls.service';
import { concat, Observable, of, Subject, forkJoin } from "rxjs";
import * as moment from 'moment';
import * as dymo from '../../../assets/js/dymo-label.js';
declare var dymo: any;
import { SortPipe } from "../../pipes/sort.pipe";
import { DatePipe } from '@angular/common';
import { MinimumAccesionDataService } from "../../services/minimumaccessiondata.service";
import { GlobalySharedModule } from './../../../../../../src/app/sharedmodules/globaly-shared/globaly-shared.module'
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-edit-intake',
  templateUrl: './edit-intake.component.html',
  styleUrls: ['./edit-intake.component.css'],
  host        : {
    class     :'mainComponentStyle'
  }
})
export class EditIntakeComponent implements OnInit {

  public getIntakeByIdRequest = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"SPCM",
      functionalityCode: "SPCA-UI",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      page:"0",
      size:"100",
      sortColumn:"caseNumber",
      sortingOrder:"desc",
      intakeId:""
    }
  }
  allSpecimenTypes        : any = [];
  allAttachmentTypes       : any = [];

  public intakeForm           : FormGroup;
  allClientName             : any;
  clientSearchString          ;
  clientLoading               : any;
  clientLoadingSearch         : any;
  maxDate;
  maxDateRecieve;
  allStatusType             : any = [];
  public clientNameRequest = {
    header:{
      uuid               :"",
      partnerCode        :"",
      userCode           :"",
      referenceNumber    :"",
      systemCode         :"",
      moduleCode         :"CLIM",
      functionalityCode: "SPCA-UI",
      systemHostAddress  :"",
      remoteUserAddress  :"",
      dateTime           :""
    },
    data:{
      clientIds:[]
    }
  }
  saveRequestCollectionDate = null;
  totalSpecimen = 0;
  selectattachmentTypeId       =null;
  attachmentTable              = [];
  attachmentTableSelected      = [];
  allemptySpecError = true;
  swabNP = null;
  swabN  = null;
  swabO  = null;
  swabS  = null;

  swabNPA = null;
  swabNA  = null;
  swabOA  = null;
  swabSA  = null;
  totalSpecimenAccessioned = 0;
  dataForCreateCaseFromIntake : any = [];
  searchTerm$      = new Subject<string>();
  public saveRequest          = {
    header  : {
      dateTime: "",
      functionalityCode: "SPCA-UI",
      moduleCode: "SPCM",
      partnerCode: "",
      referenceNumber: "",
      remoteUserAddress: "",
      systemCode: "",
      systemHostAddress: "",
      userCode: "",
      uuid: ""
    },
    data:{
      intakeId: null,
      clientId:null,
      notes:"",
      countKits:"",
      countKitsLeft:"",
      collectedBy:"1",
      collectionTimestamp:"",
      createdBy:"",
       createdTimestamp:"",
       updatedBy:"",
       updatedTimestamp:"",
      intakeSpecimen:[
        // {
        //   specimenId:"205",
        //   count:46
        // },{
        //   specimenId:"206",
        //   count:36
        // },{
        //   specimenId:"207",
        //   count:26
        // },{
        //   specimenId:"208",
        //   count:16
        // }
      ],
      intakeAttachments:[
        // {
        //   attachmentTypeId:"1",
        //   fileTypeId:"1",
        //   file:"sdsadasdas",
        //   createdBy:"abc",
        //   createdTimestamp:"12-29-2020 11:18:10"
        // }
      ],
      caseIds:[]
    }


  }

  submitted = false;
  public logedInUserRoles :any = {};
  swalStyle              : any;
  selectedIntake         : any = {};
  selectedIntakeID         = null;
  caseAttOldLength = 0;
  scanedCaseNumber = null;
  public addremoveIntakeReq = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"moduleCode",
      functionalityCode: "SPCA-UI",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      caseIds:[],
      intakeId:"",
      updatedBy:"",
      updatedTimestamp:"",
      operation:""

    }
  }
  public createBatchRequest = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"SPCM",
      functionalityCode: "SPCA-UI",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      page:"",
      size:"",
      sortColumn:"",
      sortingOrder:"asc",
      searchDate:"",
      caseNumber:"",
      caseStatus:"3"
    }
  }
  disableBatchButton = false;
  caseNumber         : any;
  timeCount ;
  allCasesInIntake = [];
  oldClientBeforSave;
  totalCaseCount = 0;
  constructor(
    private formBuilder     : FormBuilder,
    private http            : HttpClient,
    private router          : Router ,
    private route           : ActivatedRoute,
    private sanitizer       : DomSanitizer,
    private ngxLoader       : NgxUiLoaderService,
    private notifier        : NotifierService,
    private specimenService : SpecimenApiCallsService,
    private globalService   : GlobalApiCallsService,
    private sortPipe        :SortPipe,
    private datePipe        : DatePipe,
    private sharedIntakeData: MinimumAccesionDataService,
    private elementRef: ElementRef,
  ) {
    this.notifier        = notifier;
    this.swalStyle       =  Swal.mixin({
      customClass : {
        confirmButton : 'btn btn-success my-swal-success',
        cancelButton  : 'btn btn-danger'
      },
      buttonsStyling  : true
    })
   }

  ngOnInit(): void {
    this.sharedIntakeData.setGreenColorFormanageCaseOnly("null");

    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
var originalText = bts.toString(CryptoJS.enc.Utf8);
this.logedInUserRoles = JSON.parse(originalText);
    this.specimenService.search(this.searchTerm$)
    .subscribe(results => {
      console.log("results",results);
      this.allClientName = results['data'];
      this.clientLoading = false;
    },
    error => {
      this.notifier.notify( "error", "Error while fetching clients backend connect aborted" );
      this.clientLoading = false;
      return;
      // console.log("error",error);
    });
    var dtToday = new Date();

    var m = dtToday.getMonth()+1;
    var d = dtToday.getDate();
    var year = dtToday.getFullYear();
    var month;
    var day;
    if(m < 10)
    month = '0' + m.toString();
    else
    month = m
    if(d < 10)
    day = '0' + d.toString();
    else
    day = d

    var maxDate = year + '-' + month + '-' + day;
    this.maxDate = maxDate;
    this.maxDateRecieve = maxDate+'T00:00';
    this.getLookups().then(lookupsLoaded => {
      this.ngxLoader.start()
      this.route.params.subscribe(params => {
        if (typeof params['id'] != "undefined") {
          var coverted = params['id'].replace('xsarm','+').replace('ydan','/').replace('zhar','=');
          var bytes  = CryptoJS.AES.decrypt(coverted, 'nglis');
          var originalText = bytes.toString(CryptoJS.enc.Utf8).toString();
          this.sharedIntakeData.loadCasesInIntake(originalText);
          // this.sharedIntakeData.loadCasesInIntake(params['id']);
          let columnCaseReq  = {...this.getIntakeByIdRequest}
          columnCaseReq.header.partnerCode = this.logedInUserRoles.partnerCode;
          columnCaseReq.header.userCode    = this.logedInUserRoles.userCode;
          columnCaseReq.header.functionalityCode    = "SPCA-UI";
          columnCaseReq.data.intakeId      = originalText;
          // columnCaseReq.data.intakeId      = params['id'];
          var selectURL = environment.API_SPECIMEN_ENDPOINT + 'showintakebyid';
          this.getSelectedRecord(selectURL,columnCaseReq).then(selectedCase => {
            this.ngxLoader.stop()
          })
        }
        else{
          this.ngxLoader.stop()
          this.router.navigateByUrl('/create-intake');

        }
      })
      })

      this.intakeForm       = this.formBuilder.group({
        notes             : [''],
        clientName        : ['', [Validators.required]],
        countKits         : ['', [Validators.required, Validators.maxLength(3)]],
        countKitsLeft     : ['', [Validators.maxLength(3)]],
        collectionDate    : ['', [Validators.required]],
        collectedBy       : ['1', [Validators.required]],
        nP                : [''],
        n                 : [''],
        s                 : [''],
        o                 : [''],
        attType           : [''],
        attFile           : [''],
      })
      this.sharedIntakeData.reloadEditInake.subscribe(editIntakeFromMinAccession => {
        console.log('*****editIntakeFromMinAccession',editIntakeFromMinAccession);
        if (editIntakeFromMinAccession != 'null') {
          let columnCaseReq  = {...this.getIntakeByIdRequest}
          columnCaseReq.header.partnerCode = this.logedInUserRoles.partnerCode;
          columnCaseReq.header.userCode    = this.logedInUserRoles.userCode;
          columnCaseReq.header.functionalityCode    = "SPCA-UI";
          columnCaseReq.data.intakeId      = editIntakeFromMinAccession;
          var selectURL = environment.API_SPECIMEN_ENDPOINT + 'showintakebyid';
          this.getSelectedRecord(selectURL,columnCaseReq).then(selectedCase => {
            this.ngxLoader.stop()
            this.sharedIntakeData.reloadEditIntakeAfteminAcceesion('null');
          })

        }

      })
      this.selectattachmentTypeId = 6;
  }
  ngAfterViewInit(): void {
    const input = this.elementRef.nativeElement.querySelector(".scaneverywhere");
    console.log('input',input);

    if (input) {
      input.focus();
    }
  }

  get f() { return this.intakeForm.controls; }
  saveIntake(){
    this.ngxLoader.start();
    this.submitted                  = true;
    if (this.intakeForm.invalid) {
      console.log("this.allemptySpecError",this.allemptySpecError);
      $('#submitModalIntake').modal('hide');
      this.ngxLoader.stop();

      return;
    }
    if (this.allemptySpecError == true) {
      console.log('111');

      $('#submitModalIntake').modal('hide');
      this.ngxLoader.stop();
      return;
    }
    var myDate = new Date();
    var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyy');
    var currentTimeStamp = this.datePipe.transform(myDate, 'MM-dd-yyy HH:mm:ss');

    this.saveRequest.data.createdTimestamp  = this.selectedIntake.createdTimestamp;
    this.saveRequest.data.createdBy         = this.logedInUserRoles.userCode;
    this.saveRequest.data.updatedBy         = this.logedInUserRoles.userCode;
    this.saveRequest.data.updatedTimestamp  = currentTimeStamp;
    this.saveRequest.data.collectionTimestamp = this.datePipe.transform(this.saveRequestCollectionDate, 'MM-dd-yyy HH:mm:ss');

    var np = 0;
    var n  = 0;
    var o  = 0;
    var s  = 0;
    this.saveRequest.data.intakeSpecimen = [];
    if (this.swabNP == null || this.swabNP == "") {

    }
    else{
      np = parseInt(this.swabNP);
    }
    var holder= {};
    holder['specimenId'] =2; ///////////NP
    holder['count'] =np; ///////////NP
    holder['intakeId'] =this.selectedIntake.intakeId;
    this.saveRequest.data.intakeSpecimen.push(holder)

    if (this.swabN == null || this.swabN == "") {

    }
    else{
      n = parseInt(this.swabN);
    }
    holder= {};
    holder['specimenId'] =1; ///////////N
    holder['count'] =n; ///////////N
    holder['intakeId'] =this.selectedIntake.intakeId;
    this.saveRequest.data.intakeSpecimen.push(holder)

    if (this.swabO == null || this.swabO == "") {

    }
    else{
      o = parseInt(this.swabO);
    }
    holder= {};
    holder['specimenId'] =3; ///////////O
    holder['count'] =o; ///////////O
    holder['intakeId'] =this.selectedIntake.intakeId;
    this.saveRequest.data.intakeSpecimen.push(holder)

    if (this.swabS == null || this.swabS == "") {

    }
    else{
      s = parseInt(this.swabS);
    }
    holder= {};
    holder['specimenId'] =4; ///////////S
    holder['count'] =s; ///////////S
    holder['intakeId'] =this.selectedIntake.intakeId;
    this.saveRequest.data.intakeSpecimen.push(holder)
    this.saveRequest.data.notes.replace(/[^a-zA-Z0-9]/g, '');
    this.saveRequest.header.userCode = this.logedInUserRoles.userCode;

    // console.log("this.selectedIntake",this.selectedIntake);
    console.log("this.saveRequest",this.saveRequest);

    // this.ngxLoader.stop();
    // return;
    var saveIntakeUrl = environment.API_SPECIMEN_ENDPOINT+'updateintake';

    this.specimenService.createIntake(saveIntakeUrl,this.saveRequest).subscribe(saveResp=>{
      if (saveResp['result'].codeType == 'S') {
        console.log('this.attachmentTable',this.attachmentTable);

        if (this.attachmentTable.length>0) {
          // console.log('here');

          this.saveAttachments(saveResp['data'].intakeId).then(res => {
            this.ngxLoader.stop();
            this.notifier.notify('success','Intake updated successfully')
            this.submitted = false;
            let columnCaseReq  = {...this.getIntakeByIdRequest}
            columnCaseReq.header.partnerCode = this.logedInUserRoles.partnerCode;
            columnCaseReq.header.userCode    = this.logedInUserRoles.userCode;
            columnCaseReq.header.functionalityCode    = "SPCA-UI";
            columnCaseReq.data.intakeId      = saveResp['data'].intakeId;
            var selectURL = environment.API_SPECIMEN_ENDPOINT + 'showintakebyid';
            this.getSelectedRecord(selectURL,columnCaseReq).then(selectedCase => {
              this.ngxLoader.stop()
            })

          })
        }
        else{
          // this.ngxLoader.start();
          this.notifier.notify('success','Intake updated successfully')
          this.submitted = false;
          let columnCaseReq  = {...this.getIntakeByIdRequest}
          columnCaseReq.header.partnerCode = this.logedInUserRoles.partnerCode;
          columnCaseReq.header.userCode    = this.logedInUserRoles.userCode;
          columnCaseReq.header.functionalityCode    = "SPCA-UI";
          columnCaseReq.data.intakeId      = saveResp['data'].intakeId;
          var selectURL = environment.API_SPECIMEN_ENDPOINT + 'showintakebyid';
          this.getSelectedRecord(selectURL,columnCaseReq).then(selectedCase => {
            this.ngxLoader.stop()
          })
        }
          $('#submitModalIntake').modal('hide');

      }
      else{
        this.ngxLoader.stop();
        this.notifier.notify('error','Error while creating Intake')
      }
    })

    return;


  }


  getSelectedRecord(url,data) {

      return new Promise((resolve, reject) => {
        this.http.post(url,data).toPromise()
        .then( resp => {
          console.log('resp**********',resp);
          if (typeof resp['result'] != 'undefined') {
            if (typeof resp['result']['description'] != 'undefined') {
              if (resp['result']['description'].indexOf('UnAuthorized') != -1) {
                this.notifier.notify('warning',resp['result']['description'])
                this.router.navigateByUrl('/page-restrict');
              }
            }
          }
          this.allCasesInIntake = resp['data']['caseDetails'];
          resp['data']['intakeDetails']['totalCaseCount'] = resp['data']['totalCases'];
          this.totalCaseCount = resp['data']['totalCases'];
          var d = new Date(resp['data']['intakeDetails'].collectionTimestamp)
          var collectionDate = d.getFullYear()+"-"+this.zeroPadded(d.getMonth() + 1)+"-"+this.zeroPadded(d.getDate())+"T"+this.zeroPaddedTime(d.getHours())+":"+this.zeroPaddedTime(d.getMinutes());
          this.saveRequestCollectionDate = collectionDate;
          this.saveRequest.data.collectedBy = resp['data']['intakeDetails'].collectedBy.toString();
          var clURL           = environment.API_CLIENT_ENDPOINT + "clientidsnames"
          var clReqData       = {
            header:{
              uuid               :"",
              partnerCode        :"",
              userCode           :"",
              referenceNumber    :"",
              systemCode         :"",
              moduleCode         :"CLIM",
              functionalityCode: "CLTA-VC",
              systemHostAddress  :"",
              remoteUserAddress  :"",
              dateTime           :""
            },
            data:{
              clientIds:[resp['data']['intakeDetails'].clientId]}
            }
            clReqData['header'].partnerCode = this.logedInUserRoles.partnerCode;
            clReqData['header'].userCode = this.logedInUserRoles.userCode;
            clReqData['header'].functionalityCode = "CLTA-VC";
            this.http.post(clURL,clReqData).toPromise().then(clientResp =>{
              if (clientResp['data'] != []) {
                this.allClientName = clientResp['data'];
                this.clientSearchString = clientResp['data'][0];
                this.oldClientBeforSave = clientResp['data'][0].clientId;
                this.saveRequest.data.clientId = clientResp['data'][0].clientId;
                this.sharedIntakeData.loadClientIdForEditIntake(clientResp['data'][0].clientId);
              }
              else{
                this.notifier.notify('error','Error while loading client name');
              }
              //////call to get all phsicians
            },error =>{
              this.notifier.notify('error','Error while loading client name');
            })
            this.saveRequest.data.notes         = resp['data']['intakeDetails'].notes;
            this.saveRequest.data.countKits     = resp['data']['intakeDetails'].countKits;
            this.saveRequest.data.countKitsLeft = resp['data']['intakeDetails'].countKitsLeft;
            this.selectedIntakeID               = resp['data']['intakeDetails'].intakeId;
            this.saveRequest.data.intakeId      = resp['data']['intakeDetails'].intakeId;
            var np = 0;
            var n  = 0;
            var o  = 0;
            var s  = 0;
            for (let i = 0; i < resp['data']['intakeDetails'].intakeSpecimen.length; i++) {
              if (resp['data']['intakeDetails'].intakeSpecimen[i].specimenId == 2) {
                np = resp['data']['intakeDetails'].intakeSpecimen[i].count;
              }
              else if (resp['data']['intakeDetails'].intakeSpecimen[i].specimenId == 1) {
                n = resp['data']['intakeDetails'].intakeSpecimen[i].count;
              }
              else if (resp['data']['intakeDetails'].intakeSpecimen[i].specimenId == 3) {
                o = resp['data']['intakeDetails'].intakeSpecimen[i].count;
              }
              else if (resp['data']['intakeDetails'].intakeSpecimen[i].specimenId == 4) {
                s = resp['data']['intakeDetails'].intakeSpecimen[i].count;
              }

            }
            this.swabNP = np;
            this.swabN  = n;
            this.swabO  = o;
            this.swabS  = s;
            this.totalSpecimen = np+n+o+s;
            if (this.totalSpecimen > 0) {
              this.allemptySpecError = false;
            }

            var anp = 0;
            var an  = 0;
            var ao  = 0;
            var as  = 0;
            for (let j = 0; j < resp['data']['intakeDetails'].specimentWiseAccessionedCount.length; j++) {
              if (resp['data']['intakeDetails'].specimentWiseAccessionedCount[j].specimenId == 2) {
                anp = resp['data']['intakeDetails'].specimentWiseAccessionedCount[j].accessionedCount;
              }
              else if (resp['data']['intakeDetails'].specimentWiseAccessionedCount[j].specimenId == 1) {
                an = resp['data']['intakeDetails'].specimentWiseAccessionedCount[j].accessionedCount;
              }
              else if (resp['data']['intakeDetails'].specimentWiseAccessionedCount[j].specimenId == 3) {
                ao = resp['data']['intakeDetails'].specimentWiseAccessionedCount[j].accessionedCount;
              }
              else if (resp['data']['intakeDetails'].specimentWiseAccessionedCount[j].specimenId == 4) {
                as = resp['data']['intakeDetails'].specimentWiseAccessionedCount[j].accessionedCount;
              }

            }
            this.swabNPA = anp;
            this.swabNA  = an;
            this.swabOA  = ao;
            this.swabSA  = as;
            this.totalSpecimenAccessioned = anp+an+ao+as;

            //////////////// get files
            var attData   = []
            var attHolder = {}
            var filesURL = environment.API_SPECIMEN_ENDPOINT + 'loadfiles';

            var intakeId = resp['data']['intakeDetails'].intakeId;
            var getFilesReq={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "SPCA-MI",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{intakeId:intakeId}}
            this.http.post(filesURL,getFilesReq).toPromise().then(fileResp =>{
              console.log("fileResp",fileResp);
              if (fileResp['data'] != null) {
                this.caseAttOldLength = fileResp['data'].length;
                for (let m = 0; m < fileResp['data'].length; m++) {
                  attHolder['payLoad'] = '';

                  // var fi = fileResp['data'][m].file.split('/');
                  // var le = fi.length-1;
                  var ft = fileResp['data'][m].fileName.split('.');
                  // var ft = fi[le].split(".");
                  var mi = ft.length - 1;
                  // var ftype = ft[mi]
                  var ftype = ft[1]

                  // attHolder['file'] = fi[le];
                  attHolder['file'] = fileResp['data'][m].fileName;
                  attHolder['fileType'] = ftype;
                  attHolder['fileUrl'] = fileResp['data'][m].file;
                  attHolder['fileId'] = fileResp['data'][m].caseAttachmentId;
                  attHolder['intakeId'] = fileResp['data'][m].intakeId;
                  for (let n = 0; n < this.allAttachmentTypes.length; n++) {
                    if (this.allAttachmentTypes[n].attachmentTypeId == fileResp['data'][m].attachmentTypeId) {
                      attHolder['type'] = this.allAttachmentTypes[n].attachmentTypeName;
                    }

                  }
                  attData.push(attHolder);
                  attHolder = {}
                }
                this.attachmentTable = attData;

              }
              else{
                this.attachmentTable              = [];
                this.selectattachmentTypeId       =null;
              }

            },error =>{
              this.notifier.notify( "error", "Error while loading case number search results")
              this.ngxLoader.stop();
            })
            const found = resp['data']['caseDetails'].some(el => el.caseStatusId == 3);
            // console.log("resp['data']['caseDetails']",resp['data']['caseDetails']);
            // console.log("found",found);
            if (found == true) {
              this.disableBatchButton = false;
            }
            else{
              this.disableBatchButton = true;
            }
            // console.log('  this.disableBatchButton',  this.disableBatchButton);


            this.selectedIntake = resp['data']['intakeDetails'];
            console.log('this.selectedIntake',this.selectedIntake);


            if (resp['data']['totalCases'] > 0) {
              this.intakeForm.get('clientName').disable()
            }else{
              this.intakeForm.get('clientName').enable()
            }

          resolve()
        })
        .catch(error => {
          // return false;
          // console.log("error: ",error);
          this.notifier.notify( "error", "Error while loading case number search results")
          reject();
        });
      })
    }

    collectDateChange(e){
      // console.log("ee",e.target.value);
      if (e.target.value > (this.maxDate+'T24:00')) {
        // alert("date greater then current"+e.target.value)
        // this.saveRequest.data.collectionDate = null;
        this.saveRequestCollectionDate = null;

      }
      // console.log("collectDate",this.collectDate);

    }
    popFile(e){
      var input = e.target;
      // console.log("input",input.files);
      // console.log("this.selectattachmentTypeId",this.selectattachmentTypeId);
      if (typeof input.files[0] == 'undefined') {
        return;
      }
      if (this.attachmentTable.length ==3) {
        alert("Only three files are allowed")
        return;
      }

      var attTemp = {}
      var optionSelected = $('#attachment-select101 option:selected').attr('attname');

      // this.saveRequest.data.caseAttachments[0].attachmentTypeId = this.selectattachmentTypeId;
      if (this.selectattachmentTypeId != null) {
        var mb = input.files[0].size/1048576;
        var sl = input.files[0].name.length
        // console.log("mb",mb);
        if (mb <= 30 && sl <= 40) {
          if (input.files[0].type != 'image/jpeg' && input.files[0].type != 'image/jpg' && input.files[0].type != 'image/png' && input.files[0].type != 'application/pdf') {
            $('#upload-att11').val('');
            alert('invalid file type');
            return;
          }
          attTemp['payLoad']  = input.files[0];
          if (input.files[0].type == 'image/jpeg') {attTemp['fileTypeId']  = '3';}
          else if(input.files[0].type == 'image/jpg') {attTemp['fileTypeId']  = '3';}
          else if(input.files[0].type == 'image/png') {attTemp['fileTypeId']  = '4';}
          else if(input.files[0].type == 'application/pdf') {attTemp['fileTypeId']  = '1';}
          attTemp['fileType']  = input.files[0].type;
          attTemp['attachId'] = this.selectattachmentTypeId;
          var reader = new FileReader();
          reader.onload = (e: any) => {
            // console.log('Got here: ', e.target.result);
            attTemp['fileurl'] = this.sanitizer.bypassSecurityTrustUrl(e.target.result);
            // this.obj.photoUrl = e.target.result;
          }
          reader.readAsDataURL(input.files[0]);
          var fileName = input.files[0].name;
          attTemp['type'] = optionSelected;
          // attTemp['type'] = this.selectattachmentTypeId;
          attTemp['file'] = fileName;
          attTemp['notDownload'] = true;
          this.attachmentTable.push(attTemp);
          if (this.attachmentTable.length >0) {
            this.intakeForm.get('attType').clearValidators();
            this.intakeForm.controls["attType"].updateValueAndValidity();
            this.intakeForm.get('attFile').clearValidators();
            this.intakeForm.controls["attFile"].updateValueAndValidity();
          }
          else{
            this.intakeForm.get('attType').setValidators([Validators.required])
            this.intakeForm.controls["attType"].updateValueAndValidity();
            this.intakeForm.get('attFile').setValidators([Validators.required])
            this.intakeForm.controls["attFile"].updateValueAndValidity();
          }
          // console.log("this.attachmentTable",this.attachmentTable);

        }
        else{
          $('#upload-att11').val('');
          alert('provide file less than 30 MB')
        }

      }
      else{
        $('#upload-att11').val('');
        alert('Select Attacment Type');
      }

    }

    swabChange(e){
      var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})/);
      e.target.value = !x[2] ? x[1] :+'';
      console.log("this.swabNP",this.swabNP);
      // console.log("this.swabNP",typeof this.swabNP);
      console.log("this.swabN",this.swabN);
      // console.log("this.swabN",typeof this.swabN);
      console.log("this.swabo",this.swabO);
      // console.log("this.swabo",typeof this.swabO);
      console.log("this.swabs",this.swabS);
      // console.log("this.swabs",typeof this.swabS);
      var np = 0;
      var n  = 0;
      var o  = 0;
      var s  = 0;
      if (this.swabNP == null) {

      }
      else{
        if (this.swabNP == "") {
          np = 0
        }
        else{
          np = parseInt(this.swabNP)
        }

      }
      if (this.swabN == null) {

      }
      else{
        if (this.swabN == "") {
          n = 0
        }
        else{
          n = parseInt(this.swabN)
        }

      }
      if (this.swabO == null) {

      }
      else{
        if (this.swabO == "") {
          o = 0
        }
        else{
          o = parseInt(this.swabO)
        }

      }
      if (this.swabS == null) {

      }
      else{
        if (this.swabS == "") {
          s = 0
        }
        else{
          s = parseInt(this.swabS)
        }

      }

      this.totalSpecimen = np + n +o + s;
      // console.log('this.totalSpecimen',this.totalSpecimen);
      if (this.totalSpecimen >0) {
        this.allemptySpecError = false

      }
      else{
        this.allemptySpecError = true;

      }



    }
    clientChanged(){
      // console.log("this.saveRequest",this.saveRequest);
      if (this.clientSearchString != null) {
        this.oldClientBeforSave = this.saveRequest.data.clientId;
        this.saveRequest.data.clientId = this.clientSearchString.clientId;
        this.sharedIntakeData.loadClientIdForEditIntake(this.clientSearchString.clientId);
        console.log("client Changed",this.clientSearchString);
      }
      else{
        this.saveRequest.data.clientId = null;
        this.sharedIntakeData.loadClientIdForEditIntake('null');
      }
    }
    saveAttachments(intakeId){
      // if (caseNumber == null) {
      //   caseNumber = caseIdforFiles;
      // }
        return new Promise((resolve, reject) => {
          // console.log('intakeId',intakeId);

          var saveFileUrl = environment.API_SPECIMEN_ENDPOINT + "uploadfile";
          var saveFileReq = {
            header:{
              uuid:"",
              partnerCode:"",
              userCode:"",
              referenceNumber:"",
              systemCode:"",
              moduleCode:"SPCM",
              functionalityCode: "SPCA-UI",
              systemHostAddress:"",
              remoteUserAddress:"",
              dateTime:""
            },
            data:{
              caseNumber:"",
            }
          }
          saveFileReq['header'].partnerCode = this.logedInUserRoles.partnerCode;
          saveFileReq['header'].userCode = this.logedInUserRoles.userCode;
          saveFileReq['header'].functionalityCode = "SPCA-UI";

            var saveMaxBody    = [];
            var saveBodyHolder = {}
            var loopCount = 0;
            // console.log('this.attachmentTable',this.attachmentTable);
            // this.ngxLoader.stop();
            // return;

            for (let i = 0; i < this.attachmentTable.length; i++) {


              if (this.attachmentTable[i]['payLoad'] != '') {
                console.log("this.attachmentTable*********",this.attachmentTable[i]);


                saveFileReq.data.caseNumber = intakeId;
                var myDate = new Date();
                var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyy');
                var currentTimeStamp = this.datePipe.transform(myDate, 'MM-dd-yyy HH:mm:ss');
                // saveFileReq.data.caseAttachments['caseAttachmentId'] = this.attachmentTable[i]['attachId'];
                let formRequestData = new FormData();
                formRequestData.append('file', this.attachmentTable[i]['payLoad']);
                var reqString = JSON.stringify(saveFileReq);
                formRequestData.append('specimentRequest', reqString);
                console.log('this.attachmentTable', this.attachmentTable);

                this.specimenService.uploadFiles(saveFileUrl,formRequestData).subscribe(fileResp =>{
                  console.log("*fileResp",fileResp);

                  if (fileResp['data'] != null) {
                    saveBodyHolder['intakeId']           = intakeId;
                    if (typeof this.attachmentTable[i]['attachId'] != 'undefined') {
                      saveBodyHolder['attachmentTypeId'] = this.attachmentTable[i]['attachId'];
                    }


                    saveBodyHolder['file']             = fileResp['data'];
                    saveBodyHolder['fileTypeId']       = this.attachmentTable[i]['fileTypeId'];
                    // saveBodyHolder['createdBy']        = this.logedInUserRoles.userCode;
                    // saveBodyHolder['createdTimestamp'] = currentTimeStamp;
                    saveMaxBody.push(saveBodyHolder);
                    saveBodyHolder = {};
                    loopCount = loopCount + 1;
                  }

                  if(loopCount == this.attachmentTable.length){
                    // console.log("loopCount",loopCount);
                    // this.ngxLoader.stop()
                    // return;


                    ////// call save attachment end point
                    var saveAttReqBody={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "SPCA-UI",systemHostAddress:"",remoteUserAddress:"",dateTime:""},
                    data:saveMaxBody}
                    var saveAttUrl    = environment.API_SPECIMEN_ENDPOINT + "saveintakeattachment";
                    this.specimenService.saveAttachmentsIntake(saveAttUrl, saveAttReqBody).subscribe(attResp =>{
                      console.log("attResp",attResp);
                      resolve();
                    },
                  error=>{
                    this.ngxLoader.stop();
                    this.notifier.notify('error',"error while saving files")
                  })

                  }
                },
                error=>{
                  this.ngxLoader.stop();
                  this.notifier.notify('error',"error while saving files")
                })
              }else{
                loopCount = loopCount + 1;
                if(loopCount == this.attachmentTable.length){
                  resolve()
                }
              }


            }



            // console.log('f1');
            // resolve();
        });
    }


  getLookups(){
    this.ngxLoader.start();
    return new Promise((resolve, reject) => {

      var specType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=SPECIMEN_TYPE_CACHE&partnerCode=sip').then(response =>{
        return response;
      })
      var attachType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=ATTACHMENT_TYPE_CACHE&partnerCode=sip').then(response =>{
        return response;
      })
      var statusType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=CASE_STATUS_CACHE&partnerCode=sip').then(response =>{
        return response;
      })

      forkJoin([specType,attachType,statusType]).subscribe(allLookups => {
        // console.log("allLookups",allLookups);
        this.allSpecimenTypes      = allLookups[0];
        this.allAttachmentTypes    = this.sortPipe.transform(allLookups[1], "asc", "attachmentTypeName");
        this.allStatusType         = this.sortPipe.transform(allLookups[2], "asc", "caseStatusName");
        resolve();

      })
    })

  }
  zeroPadded(val) {
    if (val >= 10)
      return val;
    else
      return '0' + val;
  }
  zeroPaddedTime(val) {
    if (val >= 10)
      return val;
    else
      return '0' + val;
  }

  loadCreateMinAccession(intakeId,clientId,collectionTimestamp,collectedBy,specimenTypeId){
    var holder = {};
    holder['intakeId']            = intakeId;
    holder['clientId']            = clientId;
    holder['collectionTimestamp'] = collectionTimestamp;
    holder['collectedBy']         = collectedBy;
    holder['specimenTypeId']         = specimenTypeId;
    this.dataForCreateCaseFromIntake = holder;
    // console.log('dataForCreateCaseFromIntake',this.dataForCreateCaseFromIntake);
    this.sharedIntakeData.changeRecordFromIntake(this.dataForCreateCaseFromIntake);
    $('#createnewcaseModal').modal('show');

  }

  downloadFile(intakeId,file,type,fileId){
    console.log("intakeId",intakeId);
    // console.log("file",file);
    // console.log("type",type);
    var a = [];
    var b = '';

    // if (environment.production ==  false) {
    //   a = file.split('\\');
    //   var l = a.length;
    //   b = a[l-2]+'\\'+a[l-1];
    // }else{
    //   a = file.split('/');
    //   var l = a.length;
    //   b = a[l-2]+'/'+a[l-1];
    // }

    this.ngxLoader.start();
    var downloadUrl = environment.API_SPECIMEN_ENDPOINT+"getfile"
    var downloadReq={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "SPCA-UI",systemHostAddress:"",remoteUserAddress:"",dateTime:""},
    data:{intakeId:this.selectedIntakeID,fileUrl:'',fileId:fileId}}
    this.specimenService.downloadFiles(downloadUrl,downloadReq,type).subscribe(downloadResp=>{
      console.log("downloadResp",downloadResp);
      this.ngxLoader.stop();

    },
  error=>{
    this.ngxLoader.stop();
    this.notifier.notify("error","Error while downloading the file.")
  })

  }
  addCaseInIntake(e){
    // console.log("e",e);

    if (e.ctrlKey == true) {
      // console.log('CTRL');
      return;
    }
    else{
      // this.timeCount = 1
      var regexp = new RegExp(/([a-zA-Z]{2}[0-9]{2}-[0-9]{1})/);
      var reg = regexp.test(this.scanedCaseNumber);

      if (reg != false) {
        if(this.timeCount) {
          clearTimeout(this.timeCount);
          // console.log("here",this.timeCount);

          this.timeCount = null;
        }

        // this.timeCount = setTimeout(this.addCaseInternal(e), 5000)
        this.timeCount = setTimeout(()=>{
          this.loadCase();
        }, 400)

      }
    }
  }

  loadCase(){
    if (this.scanedCaseNumber == "" || this.scanedCaseNumber == null) {
      this.ngxLoader.stop();
      return;
    }
    else{
      console.log("this.allCasesInIntake",this.allCasesInIntake);
      // return;
      const found = this.allCasesInIntake.some(el => el.caseNumber === this.scanedCaseNumber.toUpperCase());
      // console.log("found",found);
      // console.log("this.caseNumber",this.caseNumber);
      // console.log("this.specimenTableData",this.specimenTableData);

      if (found == true) {
        this.notifier.notify('info','This Case already exists in the selected intake');
        this.scanedCaseNumber = null;
        this.ngxLoader.stop();
        return;
      }
      else{
        this.ngxLoader.start();
        var CreateRequest = {...this.createBatchRequest};
        CreateRequest.data.caseNumber = this.scanedCaseNumber.toUpperCase();
        // console.log("batchId", CreateRequest);
        console.log("CreateRequest", CreateRequest);
        // return;
        // this.covidService.createBatch;
        var allData =[];
        var batchUrl = environment.API_SPECIMEN_ENDPOINT + "searchbycovidcasenumber";
        CreateRequest.header.partnerCode  = this.logedInUserRoles.partnerCode;
        CreateRequest.header.userCode     = this.logedInUserRoles.userCode;
        CreateRequest.header.functionalityCode     = "SPCA-UI";
        this.specimenService.createBatch(batchUrl,CreateRequest).subscribe(getResp =>{
          console.log("load case resp",getResp);
          // return;

          if (getResp['data'] == null) {
            this.notifier.notify("warning","No Case Found");
            this.scanedCaseNumber = null;
            this.ngxLoader.stop();
          }
          else{
            if (getResp['data']['caseStatusId'] != 3) {
              this.notifier.notify("warning","Only the case with status triaged can be added to intake");
              this.scanedCaseNumber = null;
              this.ngxLoader.stop();
              return;
            }
            else{
              if (getResp['data']['intakeId'] != null) {
                this.ngxLoader.stop();
                Swal.fire({
                  title: "This case already included in intake "+getResp['data']['intakeId']+".",
                  text: "Are you sure you want to replace it with that intake?",
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Yes, do it!',
                  allowOutsideClick:false
                }).then((result) => {
                  if (result.value) {
                    this.ngxLoader.start();
                    this.scanedCaseNumber = null;
                    if (getResp['data']['clientId'] != this.oldClientBeforSave) {
                      this.ngxLoader.stop();
                      this.notifier.notify("warning","Client should be same for case and intake");
                    }
                    else{
                      if (getResp['data']['caseStatusId'] == 3) {
                        this.addCase(getResp['data']['caseId'])
                      }
                      else{
                        this.notifier.notify("warning","Only the case with status triaged can be added to intake");
                        this.scanedCaseNumber = null;
                        this.ngxLoader.stop();
                      }

                    }

                  } else if (result.dismiss === Swal.DismissReason.cancel) {
                    this.scanedCaseNumber = null;
                    this.ngxLoader.stop()
                    return;
                  }

                })
              }
              else{
                this.scanedCaseNumber = null;
                if (getResp['data']['clientId'] != this.oldClientBeforSave) {
                  this.ngxLoader.stop();
                  this.notifier.notify("warning","Client should be same for case and intake");
                }
                else{
                  if (getResp['data']['caseStatusId'] == 3) {
                    this.addCase(getResp['data']['caseId'])
                  }
                  else{
                    this.notifier.notify("warning","Only the case with status triaged can be added to intake");
                    this.scanedCaseNumber = null;
                    this.ngxLoader.stop();
                  }

                }
              }
            }



          }
        });
      }

    }
  }

  addCase(caseId){
    // this.ngxLoader.start();
    var myDate = new Date();
    var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyy');
    var currentTimeStamp = this.datePipe.transform(myDate, 'MM-dd-yyy HH:mm:ss');
    console.log("caseId", caseId);
    console.log("selectedIntake?.intakeId", this.selectedIntake.intakeId);
    // return;


      var url = environment.API_SPECIMEN_ENDPOINT + 'addremovecasesintake';
      var request = {...this.addremoveIntakeReq};
      request.data.caseIds = [caseId];
      request.data.intakeId = this.selectedIntake.intakeId;
      request.data.operation = "add";
      request.data.updatedBy = this.logedInUserRoles.userCode;
      request.data.updatedTimestamp = currentTimeStamp;
      request.header.userCode = this.logedInUserRoles.userCode;
      request.header.partnerCode = this.logedInUserRoles.partnerCode;
      request.header.functionalityCode = "SPCA-UI";
      // console.log('request',request);
      // this.ngxLoader.stop();
      // return;
      this.specimenService.addRemoveCaseInIntake(url,request).subscribe(addIntakeResponse =>{
        console.log('addIntakeResponse',addIntakeResponse);
        this.ngxLoader.stop();
        if (addIntakeResponse['data']>0) {
          this.notifier.notify('success','Case added successfully');
          this.disableBatchButton = false;
          this.sharedIntakeData.reloadEditIntakeAfteminAcceesion(this.selectedIntake.intakeId);
          this.sharedIntakeData.loadCasesInIntake(this.selectedIntake.intakeId);
          this.sharedIntakeData.setGreenColorAfterMinAccession(caseId);

        }
        else{
          this.ngxLoader.stop();
          this.notifier.notify('error','Error while adding case to intake');
        }

      }, error =>{
        this.ngxLoader.stop();
        this.notifier.notify('error','Error while adding case to intake');
      })

    }
    toNumeric(e){
      var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})/);
      e.target.value = !x[2] ? x[1] :+'';
    }

    loadSepcimenSearch(specimenId,intakeId){
      var searchBy = 'specimentTypeIntake';
      // var spec = '';
      // if (specimenId == 1) {spec = 'Draft'}
      // if (specimenId == 2) {spec = 'Triaged'}
      // if (specimenId == 3) {spec = 'Accessioned'}
      // if (specimenId == 4) {spec = 'In Progress'}
      // if (specimenId == 5) {spec = 'Partial Under Reporting'}
      // if (specimenId == 6) {spec = 'Under Reporting'}
      // if (specimenId == 7) {spec = 'Partial Signed Off'}
      // if (specimenId == 8) {spec = 'Signed Off'}
      // if (specimenId == 9) {spec = 'Partial Delivered'}
      // if (specimenId == 10) {spec = 'Delivered'}
      // if (specimenId == 11) {spec = 'Cancelled'}
      // if (specimenId == 12) {spec = 'Not Delivered'}
      var data = {specimenId:specimenId,intakeId:intakeId,searchBy:searchBy}
      console.log("data",data);

      this.router.navigateByUrl('/manage-cases', { state: data })
      // window.open( `/manage-cases`, data , '_blank');
      // this.router.navigateByUrl('/manage-cases', { state: data }).then(result => {  window.open( `/manage-cases${{ state: data }}`, '_blank'); });
    }

    goToAddBatch(intakeId){
      var a = intakeId;

      // var ciphertext = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
      var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
      var bx = ax.replace('+','xsarm');
      var cx = bx.replace('/','ydan');
      var ciphertext = cx.replace('=','zhar');
      console.log('ciphertext',ciphertext);
      var url = location.origin +'/add-batch/'+ciphertext;
      window.open( url , '_blank');
      // this.router.navigate(['add-batch', ciphertext]);
    }




}
