import { Component, OnInit, OnDestroy,ElementRef, ViewChild, AfterViewInit, ViewChildren, QueryList } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { environment } from '../../../../../../src/environments/environment';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import Swal from 'sweetalert2'
declare var $ :any;
import { NotifierService } from 'angular-notifier';
import { SpecimenApiCallsService } from '../../services/specimen/specimen-api-calls.service';
import { GlobalApiCallsService } from '../../services/global/global-api-calls.service';
import { concat, Observable, of, Subject, forkJoin } from "rxjs";
import * as moment from 'moment';
import * as dymo from '../../../assets/js/dymo-label.js';
declare var dymo: any;
import { SortPipe } from "../../pipes/sort.pipe";
import { DatePipe } from '@angular/common';
import { MinimumAccesionDataService } from "../../services/minimumaccessiondata.service";
import { GlobalySharedModule } from './../../../../../../src/app/sharedmodules/globaly-shared/globaly-shared.module';
import { UrlGuard } from './../../../../../../src/services/gaurd/url.guard'
import * as CryptoJS from 'crypto-js';

// import { MinimumCaseComponent } from '../../specimen/minimum-case/minimum-case.component';
@Component({
  selector: 'app-create-intake',
  templateUrl: './create-intake.component.html',
  styleUrls: ['./create-intake.component.css'],
  host        : {
    class     :'mainComponentStyle'
  }
})
export class CreateIntakeComponent implements OnInit {
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};
  dtTrigger              : Subject<CreateIntakeComponent> = new Subject();
    // dtElement              : DataTableDirective;
  // public dtOptions       : DataTables.Settings = {};
  public paginatePage    : any = 0;
  public paginateLength  = 20;
  oldCharacterCount      = 0;
  submitted              = false;
  allIntakes           : any = [];
  allIntakesCount          : any = [];
  searchTerm$      = new Subject<string>();
  searchTermSearch$= new Subject<string>();
  cNameforSearch;
  updateIntakeAction = false;

  public allIntakesReq = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"SPCM",
      functionalityCode: "SPCA-MI",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
        pageNumber:"0",
        pageSize:20,
        sortColumn:"createdTimestamp",
        sortingOrder:"desc",
        clientId:[],
        searchString:""
    }

  }
  public allIntakesReqNew = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"SPCM",
      functionalityCode: "SPCA-MI",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
        // pageNumber:"0",
        // pageSize:20,
        // sortColumn:"createdTimestamp",
        // sortingOrder:"desc",
        // clientId:[],
        // searchString:""
    }

  }
  public searchAllIntakesReq = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"SPCM",
      functionalityCode: "SPCA-MI",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
        page:"0",
        size:20,
        sortColumn:"createdTimestamp",
        sortingOrder:"desc",
        searchString:""
    }

  }
  allSpecimenTypes        : any = [];

  public clientNameRequest = {
    header:{
      uuid               :"",
      partnerCode        :"",
      userCode           :"",
      referenceNumber    :"",
      systemCode         :"",
      moduleCode         :"CLIM",
      functionalityCode: "SPCA-MI",
      systemHostAddress  :"",
      remoteUserAddress  :"",
      dateTime           :""
    },
    data:{
      clientIds:[]
    }
  }
  public logedInUserRoles :any = {};
  swalStyle              : any;
  public intakeForm           : FormGroup;
  allClientName             : any;
  clientSearchString          ;
  clientLoading               : any;
  clientLoadingSearch         : any;
  maxDate;
  saveRequestCollectionDate = null;
  public saveRequest          = {
    header  : {
      dateTime: "",
      functionalityCode: "SPCA-MI",
      moduleCode: "SPCM",
      partnerCode: "",
      referenceNumber: "",
      remoteUserAddress: "",
      systemCode: "",
      systemHostAddress: "",
      userCode: "",
      uuid: ""
    },
    data:{

      clientId:null,
      notes:"",
      countKits:"",
      countKitsLeft:"",
      collectedBy:"1",
      collectionTimestamp:"",
      createdBy:"",
      createdTimestamp:"",
      intakeSpecimen:[
        // {
        //   specimenId:"205",
        //   count:46
        // },{
        //   specimenId:"206",
        //   count:36
        // },{
        //   specimenId:"207",
        //   count:26
        // },{
        //   specimenId:"208",
        //   count:16
        // }

      ],
      intakeAttachments:[
        // {
        //   attachmentTypeId:"1",
        //   fileTypeId:"1",
        //   file:"sdsadasdas",
        //   createdBy:"abc",
        //   createdTimestamp:"12-29-2020 11:18:10"
        // }
      ]
    }


  }
  totalSpecimen = 0;
  allAttachmentTypes       : any = [];
  selectattachmentTypeId       =6;
  attachmentTable              = [];
  attachmentTableSelected      = [];
  allemptySpecError = true;
  swabNP = null;
  swabN  = null;
  swabO  = null;
  swabS  = null;
  greenColor = false;
  searchtextHolder;
  allClientNameSearch       : any;
  public searchByCriteria : any ={
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"SPCM",
      functionalityCode: "SPCA-MI",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      page:"0",
      size:"20",
      sortColumn:"createdTimestamp",
      sortingOrder:"desc",
      collectionTimestamp:"",
      intakeId:"",
      clientId:"",
      collectedBy:"",
      createdTimestamp:"",
      statuses:[],
      creationDate:""

    }

  }
  dataForCreateCaseFromIntake : any = [];
  public usersRequest: any = {
    header              : {
      uuid              : "",
      partnerCode       : "",
      userCode          : "",
      referenceNumber   : "",
      systemCode        : "",
      moduleCode        : "",
      functionalityCode: "SPCA-MI",
      systemHostAddress : "",
      remoteUserAddress : "",
      dateTime          : ""
    },
    data                : {
      userCodes           :[]
    }
  }
    timeCount0 ;
    intakeIdToDownloadFile = null;

  constructor(
    private formBuilder     : FormBuilder,
    private http            : HttpClient,
    private router          : Router ,
    private route           : ActivatedRoute,
    private sanitizer       : DomSanitizer,
    private ngxLoader       : NgxUiLoaderService,
    private notifier        : NotifierService,
    private specimenService : SpecimenApiCallsService,
    private globalService   : GlobalApiCallsService,
    private sortPipe        :SortPipe,
    private datePipe        : DatePipe,
    private sharedIntakeData: MinimumAccesionDataService,
    private rbac         : UrlGuard
  ) {

    this.rbac.checkROle('SPCA-MI');
    this.notifier        = notifier;
    this.swalStyle       =  Swal.mixin({
      customClass : {
        confirmButton : 'btn btn-success my-swal-success',
        cancelButton  : 'btn btn-danger'
      },
      buttonsStyling  : true
    })
   }

   zeroPadded(val) {
     if (val >= 10)
       return val;
     else
       return '0' + val;
   }
   zeroPaddedTime(val) {
     if (val >= 10)
       return val;
     else
       return '0' + val;
   }

  ngOnInit(): void {

    this.ngxLoader.stop();
    this.specimenService.search(this.searchTerm$)
    .subscribe(results => {
      console.log("results",results);
      this.allClientName = results['data'];
      this.clientLoading = false;
    },
    error => {
      this.notifier.notify( "error", "Error while fetching clients backend connect aborted" );
      this.clientLoading = false;
      return;
      // console.log("error",error);
    });
    /////////////// load client Name for search active
    this.specimenService.search(this.searchTermSearch$)
    .subscribe(resultsSearch => {
      console.log("results",resultsSearch);
      this.allClientNameSearch = resultsSearch['data'];
      this.clientLoadingSearch = false;
    },
    error => {
      this.notifier.notify( "error", "Error while fetching clients backend connect aborted" );
      this.clientLoadingSearch = false;
      return;
      // console.log("error",error);
    });
    var dtToday = new Date();

   var m = dtToday.getMonth()+1;
   var d = dtToday.getDate();
   var year = dtToday.getFullYear();
   var hour = this.zeroPaddedTime(dtToday.getHours());
   var min = this.zeroPaddedTime(dtToday.getMinutes());
   var sec = this.zeroPaddedTime(dtToday.getSeconds());
   var month;
    var day;
   if(m < 10)
       month = '0' + m.toString();
  else
     month = m
   if(d < 10)
       day = '0' + d.toString();
  else
   day = d

   var maxDate = year + '-' + month + '-' + day;
   this.maxDate = maxDate;
   this.saveRequestCollectionDate = year + '-' + month + '-' + day+'T'+hour+':'+min+':'+sec;
   // console.log("this.saveRequestCollectionDat",this.saveRequestCollectionDate);

    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
var originalText = bts.toString(CryptoJS.enc.Utf8);
this.logedInUserRoles = JSON.parse(originalText)
    if (this.logedInUserRoles.userType == 1) {
      this.saveRequest.data.collectedBy = "1"
    }
    else{
      this.saveRequest.data.collectedBy = "2"
    }

    if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UI") != -1) {
      this.searchAllIntakesReq['header'].functionalityCode    = "SPCA-UI";
      this.allIntakesReq['header'].functionalityCode    = "SPCA-UI";
      this.clientNameRequest['header'].functionalityCode    = "SPCA-UI";
      this.searchByCriteria['header'].functionalityCode    = "SPCA-UI";
    }
    else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-CI") != -1){
      this.searchAllIntakesReq['header'].functionalityCode    = "SPCA-CI";
      this.allIntakesReq['header'].functionalityCode    = "SPCA-CI";
      this.clientNameRequest['header'].functionalityCode    = "SPCA-CI";
      this.searchByCriteria['header'].functionalityCode    = "SPCA-CI";
    }
    else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-MI") != -1){
      this.searchAllIntakesReq['header'].functionalityCode    = "SPCA-MI";
      this.allIntakesReq['header'].functionalityCode    = "SPCA-MI";
      this.clientNameRequest['header'].functionalityCode    = "SPCA-MI";
      this.searchByCriteria['header'].functionalityCode    = "SPCA-MI";
    }

    // console.log("this.logedInUserRoles",this.logedInUserRoles);


    this.getLookups().then(lookupsLoaded => {

      this.ngxLoader.stop();
    });
    this.intakeForm       = this.formBuilder.group({
      notes             : [''],
      clientName        : ['', [Validators.required]],
      countKits         : ['', [Validators.required, Validators.maxLength(3)]],
      countKitsLeft     : ['', [Validators.maxLength(3)]],
      collectionDate    : ['', [Validators.required]],
      collectedBy       : ['1', [Validators.required]],
      nP                : [''],
      n                 : [''],
      s                 : [''],
      o                 : [''],
      attType           : [''],
      attFile           : [''],
    })
    if(this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UI") != -1){
      this.updateIntakeAction = true;

    }
    this.dtOptions = {
      pagingType   : 'full_numbers',
      pageLength   : this.paginateLength,
      serverSide   : true,
      processing   : true,
      // ordering     : false,
      "order": [[ 4, "desc" ]],
      columnDefs: [
            { orderable: true, className: 'reorder', targets: [0,1,2,3,4,5,6,7,8,11,12] },
            { orderable: false, targets: '_all' }
        ],
      "lengthMenu": [20, 50, 75, 100 ],
      // "language"   : { processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '},
      ajax: (dataTablesParameters: any, callback) => {
        if(this.timeCount0) {
          clearTimeout(this.timeCount0);
          // console.log("here",this.timeCount);

          this.timeCount0 = null;
        }
        this.timeCount0 = setTimeout(()=>{
          if(typeof this.dtElement['dt'] != 'undefined'){
              this.paginatePage           = this.dtElement['dt'].page.info().page;
              this.paginateLength         = this.dtElement['dt'].page.len();
              // console.log("this.dtElement['dt']: ", this.dtElement['dt']);
          }
          console.log("this.oldCharacterCount",this.oldCharacterCount);
          var ids          = [];
          var sortColumn = dataTablesParameters.order[0]['column'];

          var sortOrder  = dataTablesParameters.order[0]['dir'];
          var sortArray  = ['intakeId','clientName','collectedBy','collectionDate','intakeCreationDate','np','n','o','s','intakeSpecimen','lastReportedBy','lastReportedBy','lastReportedDate'];
          var columnToSearch          = "";
          var searchValue             = "";
          var caseNumValue            = "";
          var clientValue             = "";
          var collectDateValue        = "";
          var collectedByValue        = "";
          var creationDateValue       = "";
          var lastReportedByValue       = "";
          var lastReportedDateValue       = "";
          var reqAllIntakes = {...this.allIntakesReqNew}
          if (dataTablesParameters.columns[0].search.value != "") {

            if (dataTablesParameters.columns[0].search.value.length>=3 || dataTablesParameters.columns[0].search.value.length == 0) {
              reqAllIntakes.data['intakeId']   = dataTablesParameters.columns[0].search.value;
              caseNumValue = dataTablesParameters.columns[0].search.value;

            }
            else{
              return;
            }
          }
          if (dataTablesParameters.columns[3].search.value != "") {reqAllIntakes.data['collectionDate'] = dataTablesParameters.columns[3].search.value = dataTablesParameters.columns[3].search.value;       searchValue = dataTablesParameters.columns[3].search.value}
          else if (dataTablesParameters.columns[2].search.value != "") {reqAllIntakes.data['collectedBy'] = dataTablesParameters.columns[2].search.value;  searchValue = dataTablesParameters.columns[2].search.value}
          else if (dataTablesParameters.columns[4].search.value != "") {reqAllIntakes.data['intakeCreationDate'] = dataTablesParameters.columns[4].search.value;  searchValue = dataTablesParameters.columns[4].search.value}
          else if (dataTablesParameters.columns[11].search.value != "") { reqAllIntakes.data['lastReportedBy'] = dataTablesParameters.columns[11].search.value;  searchValue = dataTablesParameters.columns[11].search.value}
          else if (dataTablesParameters.columns[12].search.value != "") {reqAllIntakes.data['lastReportedDate'] = dataTablesParameters.columns[12].search.value;  searchValue = dataTablesParameters.columns[12].search.value}
          // if (this.cNameforSearch != null) {
            if (dataTablesParameters.columns[1].search.value != "") {reqAllIntakes.data['clientName'] = dataTablesParameters.columns[1].search.value; clientValue = this.cNameforSearch;searchValue  = this.cNameforSearch}
          // }

          // console.log("this.oldCharacterCount",this.oldCharacterCount);
            console.log("(dataTablesParameters.search",dataTablesParameters);

          if (dataTablesParameters.search.value != "" && (this.oldCharacterCount == 0 || this.oldCharacterCount == 3)) {
            reqAllIntakes['data']['mainSearch'] = dataTablesParameters.search.value;


            this.oldCharacterCount     = 3;
            }
            else{
              $('.dataTables_processing').css('display',"none");
              if (this.oldCharacterCount == 3) {
                this.oldCharacterCount     = 3
              }
              else{
                this.oldCharacterCount     = 2;
              }
            }
            this.oldCharacterCount     = 3;
            reqAllIntakes.header.partnerCode = this.logedInUserRoles.partnerCode;
            reqAllIntakes.header.userCode = this.logedInUserRoles.userCode;
            reqAllIntakes.header.functionalityCode = "SPCA-MI";
            reqAllIntakes.data['sortColumn']          = sortArray[sortColumn];
            reqAllIntakes.data['sortType']        = sortOrder;
            reqAllIntakes.data['pageNumber']                = this.paginatePage ;
            reqAllIntakes.data['pageSize']                = this.paginateLength;
            // reqAllIntakes.data.clientId            = [];
            // reqAllIntakes.data.searchString        = "";
            // console.log('*-*----*--*-*-***--*-**-',reqAllIntakes);
            // reqAllIntakes.data.clientId            = [];
            let intakeUrl    = environment.API_ACDM_ENDPOINT + 'allintakes';
            this.specimenService.getIntakes(intakeUrl, reqAllIntakes).subscribe(getIntakeResp=>{
              console.log("getIntakeResp",getIntakeResp);
              // return;
              if (typeof getIntakeResp['message'] != 'undefined') {
                this.notifier.notify("error",getIntakeResp['message']);
                callback({
                  recordsTotal    :  0,
                  recordsFiltered :  0,
                  data: []
                });
                this.allIntakesReqNew.data={};
                return;
              }
              if (getIntakeResp['data'] == null) {
                callback({
                  recordsTotal    :  0,
                  recordsFiltered :  0,
                  data: []
                });
                this.allIntakesReqNew.data={};
                return;

              }
              if (getIntakeResp['data']['intakes'] == null) {
                callback({
                  recordsTotal    :  0,
                  recordsFiltered :  0,
                  data: []
                });
                this.allIntakesReqNew.data={};
                return;

              }
              this.allIntakes = getIntakeResp['data']['intakes'];
              this.allIntakesCount = getIntakeResp['data'].totalIntakes;
              if (this.greenColor == true) {
                this.allIntakes[0].greenColor = true;
              }
              callback({
                recordsTotal    :  this.allIntakesCount,
                recordsFiltered :  this.allIntakesCount,
                data: []
              });
              this.allIntakesReqNew.data={};
              return;
              // this.populateIntakeTable(getIntakeResp,dataTablesParameters.search.value,[]).then(popres =>{
              //   if (this.greenColor == true) {
              //     this.allIntakes[0].greenColor = true;
              //   }
              //   callback({
              //     recordsTotal    :  this.allIntakesCount,
              //     recordsFiltered :  this.allIntakesCount,
              //     data: []
              //   });
              // })


             })


          }, 500)

      }
    }

    this.sharedIntakeData.reloadEditInake.subscribe(editIntakeFromMinAccession => {
      console.log('*****editIntakeFromMinAccession',editIntakeFromMinAccession);
      if (editIntakeFromMinAccession != 'null') {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.ajax.reload();
        });

      }

    })



  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
      this.dtElement.dtInstance.then((dtInstance: any) => {
          dtInstance.columns().every(function () {
            const that = this;
            ///////// case number search
            $('#intakeNoSearch', this.footer()).on('keyup', function () {

              if (this['value'].length >=3 || this['value'].length == 0) {
                that
                .search(this['value'].toLowerCase())
                .draw();
              }
            });
            /////// Select filter client name
            $('#searchClient', this.footer()).on('keyup', function () {
              // console.log("SearchClient",this['value']);
              if (this['value'].length >=3 || this['value'].length == 0) {
              that
              .search(this['value'])
              .draw();
              }
            });
            ////////////// Collected By
            $('#collectedBySearch', this.footer()).on('change', function () {
              that
              .search(this['value'])
              .draw();
            });
            /////// collection date filter
            $('#collectionDateFilter', this.footer()).on('change', function () {
              // var darr = this['value'].split("-");
              // var searchString = darr[1]+"/"+darr[2]+"/"+darr[0];
              // console.log("this['value']",this['value']);
              var ar = this['value'].split('-');
              console.log("ar",ar);
              var newd = "";
              if (ar[0] == "") {

              }
              else{
                newd = ar[1]+"-"+ar[2]+"-"+ar[0];
              }
              that
              .search(newd)
              .draw();

            });
            /////// collection date filter
            $('#createdDateFilter', this.footer()).on('change', function () {
              // var darr = this['value'].split("-");
              // var searchString = darr[1]+"/"+darr[2]+"/"+darr[0];
              var ar = this['value'].split('-');
              console.log("ar",ar);
              var newd = "";
              if (ar[0] == "") {

              }
              else{
                newd = ar[1]+"-"+ar[2]+"-"+ar[0];
              }
              that
              .search(newd)
              .draw();

            });
            ///////// case number search
            $('#reportedbySearch', this.footer()).on('keyup', function () {

              if (this['value'].length >=3 || this['value'].length == 0) {
                // $('#reportedDateSearch').val('');
                that
                .search(this['value'].toLowerCase())
                .draw();
              }
            });
            /////// collection date filter
            $('#reportedDateSearch', this.footer()).on('change', function () {
              // var darr = this['value'].split("-");
              // var searchString = darr[1]+"/"+darr[2]+"/"+darr[0];
              // console.log("searchString",searchString);
              var ar = this['value'].split('-');
              console.log("ar",ar);
              var newd = "";
              if (ar[0] == "") {

              }
              else{
                newd = ar[1]+"-"+ar[2]+"-"+ar[0];
              }
              // $('#reportedbySearch').val('');

              that
              .search(newd)
              // .search(searchString)
              .draw();

            });





          });



      });

  }
  popSearch(){
    this.searchtextHolder = this.cNameforSearch;
    $("#searchClient").trigger('change');
  }
  setCollectionTime(){
    var dtToday = new Date();

   var m = dtToday.getMonth()+1;
   var d = dtToday.getDate();
   var year = dtToday.getFullYear();
   var hour = this.zeroPaddedTime(dtToday.getHours());
   var min = this.zeroPaddedTime(dtToday.getMinutes());
   var sec = this.zeroPaddedTime(dtToday.getSeconds());
   var month;
    var day;
   if(m < 10)
       month = '0' + m.toString();
  else
     month = m
   if(d < 10)
       day = '0' + d.toString();
  else
   day = d

   var maxDate = year + '-' + month + '-' + day;
   this.maxDate = maxDate;
   this.saveRequestCollectionDate = year + '-' + month + '-' + day+'T'+hour+':'+min+':'+sec;
  }


  populateIntakeTable(getIntakeResp,searchString,reportByIntakeAndUsers){
    // this.ngxLoader.start();
    return new Promise((resolve, reject) => {
      var uniqueClientId = [];
      const map1 = new Map();
      for (const item of getIntakeResp.data.caseDetails) {
        if(!map1.has(item.clientId)){
          map1.set(item.clientId, true);    // set any value to Map
          uniqueClientId.push(item.clientId);
        }
      }

      var uniqueIntakeId = [];
      const map2 = new Map();
      for (const item of getIntakeResp.data.caseDetails) {
        if(!map2.has(item.intakeId)){
          map2.set(item.intakeId, true);    // set any value to Map
          uniqueIntakeId.push(item.intakeId);
        }
      }


      var clURL           = environment.API_CLIENT_ENDPOINT + "clientidsnames"
      var clReqData       = {...this.clientNameRequest};
      clReqData.data.clientIds = uniqueClientId;
      clReqData.header.partnerCode = this.logedInUserRoles.partnerCode;
      clReqData.header.userCode = this.logedInUserRoles.userCode;
      clReqData.header.functionalityCode = "CLTA-VC";
      // if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UI") != -1) {
      //   clReqData['header'].functionalityCode    = "SPCA-UI";
      // }
      // else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-CI") != -1){
      //   clReqData['header'].functionalityCode    = "SPCA-CI";
      // }
      // else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-MI") != -1){
      //   clReqData['header'].functionalityCode    = "SPCA-MI";
      // }

      var clientResult = this.specimenService.getClientByName(clURL,clReqData).then(client => {
      // var clientResult = this.specimenService.getClientByName(clURL,clReqData).then(selectedClient => {
        // console.log("selectedClient",selectedClient);

        return client;
      //   if (selectedClient['data']['length'] > 0) {
      //     for (let i = 0; i < getIntakeResp['data']['caseDetails'].length; i++) {
      //       for (let j = 0; j < selectedClient['data']['length']; j++) {
      //         if (getIntakeResp['data']['caseDetails'][i].clientId   == selectedClient['data'][j].clientId) {
      //           getIntakeResp['data']['caseDetails'][i].clientName   = selectedClient['data'][j].name;
      //         }
      //
      //       }
      //
      //     }
      //   }
      //
      //   ////////// populate Specimen Record
      //   for (let l = 0; l < getIntakeResp['data']['caseDetails'].length; l++) {
      //     // console.log("getIntakeResp['data']['caseDetails']['specimenWiseCountForEachIntake']",getIntakeResp['data']['caseDetails'][l]['specimenWiseCountForEachIntake']);
      //     getIntakeResp['data']['caseDetails'][l].specimenNP = 0;
      //     getIntakeResp['data']['caseDetails'][l].specimenN  = 0;
      //     getIntakeResp['data']['caseDetails'][l].specimenO  = 0;
      //     getIntakeResp['data']['caseDetails'][l].specimenS  = 0;
      //     getIntakeResp['data']['caseDetails'][l].specimenS  = 0;
      //     getIntakeResp['data']['caseDetails'][l].specimenTotal  = 0;
      //     for (let m = 0; m < getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'].length; m++) {
      //
      //       for (let k = 0; k < this.allSpecimenTypes.length; k++) {
      //         if (getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].specimenId == this.allSpecimenTypes[k].specimenTypeId) {
      //
      //           if (this.allSpecimenTypes[k].specimenTypePrefix == 'NP') {
      //             getIntakeResp['data']['caseDetails'][l].specimenNP = getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].accessionedCount;
      //             getIntakeResp['data']['caseDetails'][l].specimenTotal  = getIntakeResp['data']['caseDetails'][l].specimenTotal + getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].accessionedCount;
      //           }
      //           else if (this.allSpecimenTypes[k].specimenTypePrefix == 'N' || this.allSpecimenTypes[k].specimenTypePrefix == 'N ') {
      //             getIntakeResp['data']['caseDetails'][l].specimenN  = getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].accessionedCount;
      //             getIntakeResp['data']['caseDetails'][l].specimenTotal  = getIntakeResp['data']['caseDetails'][l].specimenTotal + getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].accessionedCount;
      //           }
      //           else if (this.allSpecimenTypes[k].specimenTypePrefix == 'OP') {
      //             getIntakeResp['data']['caseDetails'][l].specimenO  = getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].accessionedCount;
      //             getIntakeResp['data']['caseDetails'][l].specimenTotal  = getIntakeResp['data']['caseDetails'][l].specimenTotal + getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].accessionedCount;
      //           }
      //           else if (this.allSpecimenTypes[k].specimenTypePrefix == 'S' || this.allSpecimenTypes[k].specimenTypePrefix == 'S ') {
      //             getIntakeResp['data']['caseDetails'][l].specimenS  = getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].accessionedCount;
      //             getIntakeResp['data']['caseDetails'][l].specimenTotal  = getIntakeResp['data']['caseDetails'][l].specimenTotal + getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].accessionedCount;
      //           }
      //           // getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].specimenPrefix = this.allSpecimenTypes[k].specimenTypePrefix;
      //         }
      //       }
      //
      //     }
      //     //////////////////// specimenWiseAccessionedCountForEachIntake
      //     var accessionedCount = 0;
      //     for (let n = 0; n < getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'].length;n++) {
      //       accessionedCount = accessionedCount + getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][n]['accessionedCount'];
      //     }
      //     getIntakeResp['data']['caseDetails'][l].accessionedCount = accessionedCount;
      //     ////////////////////// specimenWiseAccessionedCountForEachIntake
      //     // var accessionedCount = 0;
      //     // for (let n = 0; n < getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'].length;n++) {
      //     //   accessionedCount = accessionedCount + getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][n]['accessionedCount'];
      //     // }
      //     // getIntakeResp['data']['caseDetails'][l].accessionedCount = accessionedCount;
      //   }
      //
      //   this.allIntakes = getIntakeResp.data.caseDetails;
      //   console.log("this.allIntakes",this.allIntakes);
      //
      //
      //     resolve();
      });

      // console.log("reportByIntakeAndUsers in function",reportByIntakeAndUsers);

      var lastUrl = environment.API_REPORTING_ENDPOINT+'lastreportbyintakes';
      var requestBody = {
        header: {
          uuid: "",
          partnerCode: "sip",
          userCode: this.logedInUserRoles.userCode,
          referenceNumber: "",
          systemCode: "NGLIS",
          moduleCode: "TWMM",
          functionalityCode: "SPCA-MI",
          systemHostAddress: "",
          remoteUserAddress: "",
          dateTime: ""
        },
        data: {
          intakeIds: [],
          // intakeIds: uniqueIntakeId,
          reportByIntakeAndUsers:[
            // {
            // userCode:"1202",
            // intakeId:"int-02182021-3"
            // }
          ]
        }
      }
      if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UI") != -1) {
        requestBody['header'].functionalityCode    = "SPCA-UI";
      }
      else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-CI") != -1){
        requestBody['header'].functionalityCode    = "SPCA-CI";
      }
      else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-MI") != -1){
        requestBody['header'].functionalityCode    = "SPCA-MI";
      }
      // requestBody.data.reportByIntakeAndUsers = reportByIntakeAndUsers;
      if (reportByIntakeAndUsers['length'] == 0) {
        requestBody.data.reportByIntakeAndUsers = [];
        requestBody.data.intakeIds = uniqueIntakeId;
      }
      else{
        requestBody.data.reportByIntakeAndUsers = reportByIntakeAndUsers;
        // requestBody.data.intakeIds = uniqueIntakeId;
        requestBody.data.intakeIds = [];
      }
      var reportedByIntake = this.http.put(lastUrl,requestBody).toPromise().then(lastReported => {
        return lastReported;
      })

      forkJoin([clientResult, reportedByIntake]).subscribe(allresults => {
        console.log("allresults",allresults);
        var selectedClient = allresults[0];
        var lastRepotedBy  = allresults[1];




        if (selectedClient['data']['length'] > 0) {
          for (let i = 0; i < getIntakeResp['data']['caseDetails'].length; i++) {
            for (let j = 0; j < selectedClient['data']['length']; j++) {
              if (getIntakeResp['data']['caseDetails'][i].clientId   == selectedClient['data'][j].clientId) {
                getIntakeResp['data']['caseDetails'][i].clientName   = selectedClient['data'][j].clientName;
              }

            }

          }
        }

        ////////// populate Specimen Record
        for (let l = 0; l < getIntakeResp['data']['caseDetails'].length; l++) {
          // console.log("getIntakeResp['data']['caseDetails']['specimenWiseCountForEachIntake']",getIntakeResp['data']['caseDetails'][l]['specimenWiseCountForEachIntake']);
          getIntakeResp['data']['caseDetails'][l].specimenNP = 0;
          getIntakeResp['data']['caseDetails'][l].specimenN  = 0;
          getIntakeResp['data']['caseDetails'][l].specimenO  = 0;
          getIntakeResp['data']['caseDetails'][l].specimenS  = 0;
          getIntakeResp['data']['caseDetails'][l].specimenS  = 0;
          getIntakeResp['data']['caseDetails'][l].specimenTotal  = 0;
          for (let m = 0; m < getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'].length; m++) {

            for (let k = 0; k < this.allSpecimenTypes.length; k++) {
              if (getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].specimenId == this.allSpecimenTypes[k].specimenTypeId) {

                if (this.allSpecimenTypes[k].specimenTypePrefix == 'NP') {
                  getIntakeResp['data']['caseDetails'][l].specimenNP = getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].accessionedCount;
                  getIntakeResp['data']['caseDetails'][l].specimenTotal  = getIntakeResp['data']['caseDetails'][l].specimenTotal + getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].accessionedCount;
                }
                else if (this.allSpecimenTypes[k].specimenTypePrefix == 'N' || this.allSpecimenTypes[k].specimenTypePrefix == 'N ') {
                  getIntakeResp['data']['caseDetails'][l].specimenN  = getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].accessionedCount;
                  getIntakeResp['data']['caseDetails'][l].specimenTotal  = getIntakeResp['data']['caseDetails'][l].specimenTotal + getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].accessionedCount;
                }
                else if (this.allSpecimenTypes[k].specimenTypePrefix == 'OP') {
                  getIntakeResp['data']['caseDetails'][l].specimenO  = getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].accessionedCount;
                  getIntakeResp['data']['caseDetails'][l].specimenTotal  = getIntakeResp['data']['caseDetails'][l].specimenTotal + getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].accessionedCount;
                }
                else if (this.allSpecimenTypes[k].specimenTypePrefix == 'S' || this.allSpecimenTypes[k].specimenTypePrefix == 'S ') {
                  getIntakeResp['data']['caseDetails'][l].specimenS  = getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].accessionedCount;
                  getIntakeResp['data']['caseDetails'][l].specimenTotal  = getIntakeResp['data']['caseDetails'][l].specimenTotal + getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].accessionedCount;
                }
                // getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].specimenPrefix = this.allSpecimenTypes[k].specimenTypePrefix;
              }
            }

          }
          //////////////////// specimenWiseAccessionedCountForEachIntake
          var accessionedCount = 0;
          for (let n = 0; n < getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'].length;n++) {
            accessionedCount = accessionedCount + getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][n]['accessionedCount'];
          }
          getIntakeResp['data']['caseDetails'][l].accessionedCount = accessionedCount;


          ////////////////////// specimenWiseAccessionedCountForEachIntake
          // var accessionedCount = 0;
          // for (let n = 0; n < getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'].length;n++) {
          //   accessionedCount = accessionedCount + getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][n]['accessionedCount'];
          // }
          // getIntakeResp['data']['caseDetails'][l].accessionedCount = accessionedCount;

          // if (searchString.toLowerCase() == 'client' || searchString.toLowerCase() == 'sip') {
          //   if (getIntakeResp['data']['caseDetails'][l]['clientName'].toLowerCase().indexOf('client') === -1) {
          //           getIntakeResp['data']['caseDetails'] = getIntakeResp['data']['caseDetails'].filter(function(value, l, arr){
          //             // console.log("value.collectedBy",value.collectedBy);
          //             return value.collectedBy == 2;
          //           });
          //
          //   }
          //   else if (getIntakeResp['data']['caseDetails'][l]['clientName'].toLowerCase().indexOf('sip') === -1) {
          //           getIntakeResp['data']['caseDetails'] = getIntakeResp['data']['caseDetails'].filter(function(value, index, arr){
          //             // console.log("value.collectedBy",value.collectedBy);
          //             return value.collectedBy == 1;
          //           });
          //   }
          //
          //
          // }
        }
        ///////////////////// last reported date
        if (lastRepotedBy['data'] != null) {
          if (lastRepotedBy['data']['length'] > 0) {
            var lastreportedData = this.populateLastRepoterdData(getIntakeResp,lastRepotedBy,searchString).then(popLastResp=>{
              // console.log('popLastResp',popLastResp);
              resolve();
            });



          }
          else{
              this.allIntakes = getIntakeResp.data.caseDetails;
              console.log("this.allIntakes",this.allIntakes);
              resolve()
          }
        }
        else{
            this.allIntakes = getIntakeResp.data.caseDetails;
            console.log("this.allIntakes",this.allIntakes);
            resolve()
        }



      })



      })

  }

  lastReportedpopulateIntakeTable(getIntakeResp,searchString,lastReported){

    // this.ngxLoader.start();
    return new Promise((resolve, reject) => {
      var uniqueClientId = [];
      const map1 = new Map();
      for (const item of getIntakeResp.data.caseDetails) {
        if(!map1.has(item.clientId)){
          map1.set(item.clientId, true);    // set any value to Map
          uniqueClientId.push(item.clientId);
        }
      }




      var clURL           = environment.API_CLIENT_ENDPOINT + "clientidsnames"
      var clReqData       = {...this.clientNameRequest};
      clReqData.data.clientIds = uniqueClientId;
      clReqData.header.partnerCode = this.logedInUserRoles.partnerCode;
      clReqData.header.userCode = this.logedInUserRoles.userCode;
      clReqData.header.functionalityCode = "CLTA-VC";
      this.specimenService.getClientByName(clURL,clReqData).then(selectedClient => {
        if (selectedClient['data']['length'] > 0) {
          for (let i = 0; i < getIntakeResp['data']['caseDetails'].length; i++) {
            for (let j = 0; j < selectedClient['data']['length']; j++) {
              if (getIntakeResp['data']['caseDetails'][i].clientId   == selectedClient['data'][j].clientId) {
                getIntakeResp['data']['caseDetails'][i].clientName   = selectedClient['data'][j].clientName;
              }

            }

          }

          ////////// populate Specimen Record
          for (let l = 0; l < getIntakeResp['data']['caseDetails'].length; l++) {
            // console.log("getIntakeResp['data']['caseDetails']['specimenWiseCountForEachIntake']",getIntakeResp['data']['caseDetails'][l]['specimenWiseCountForEachIntake']);
            getIntakeResp['data']['caseDetails'][l].specimenNP = 0;
            getIntakeResp['data']['caseDetails'][l].specimenN  = 0;
            getIntakeResp['data']['caseDetails'][l].specimenO  = 0;
            getIntakeResp['data']['caseDetails'][l].specimenS  = 0;
            getIntakeResp['data']['caseDetails'][l].specimenS  = 0;
            getIntakeResp['data']['caseDetails'][l].specimenTotal  = 0;
            for (let m = 0; m < getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'].length; m++) {

              for (let k = 0; k < this.allSpecimenTypes.length; k++) {
                if (getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].specimenId == this.allSpecimenTypes[k].specimenTypeId) {

                  if (this.allSpecimenTypes[k].specimenTypePrefix == 'NP') {
                    getIntakeResp['data']['caseDetails'][l].specimenNP = getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].accessionedCount;
                    getIntakeResp['data']['caseDetails'][l].specimenTotal  = getIntakeResp['data']['caseDetails'][l].specimenTotal + getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].accessionedCount;
                  }
                  else if (this.allSpecimenTypes[k].specimenTypePrefix == 'N' || this.allSpecimenTypes[k].specimenTypePrefix == 'N ') {
                    getIntakeResp['data']['caseDetails'][l].specimenN  = getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].accessionedCount;
                    getIntakeResp['data']['caseDetails'][l].specimenTotal  = getIntakeResp['data']['caseDetails'][l].specimenTotal + getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].accessionedCount;
                  }
                  else if (this.allSpecimenTypes[k].specimenTypePrefix == 'OP') {
                    getIntakeResp['data']['caseDetails'][l].specimenO  = getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].accessionedCount;
                    getIntakeResp['data']['caseDetails'][l].specimenTotal  = getIntakeResp['data']['caseDetails'][l].specimenTotal + getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].accessionedCount;
                  }
                  else if (this.allSpecimenTypes[k].specimenTypePrefix == 'S' || this.allSpecimenTypes[k].specimenTypePrefix == 'S ') {
                    getIntakeResp['data']['caseDetails'][l].specimenS  = getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].accessionedCount;
                    getIntakeResp['data']['caseDetails'][l].specimenTotal  = getIntakeResp['data']['caseDetails'][l].specimenTotal + getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].accessionedCount;
                  }
                  // getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][m].specimenPrefix = this.allSpecimenTypes[k].specimenTypePrefix;
                }
              }

            }
            //////////////////// specimenWiseAccessionedCountForEachIntake
            var accessionedCount = 0;
            for (let n = 0; n < getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'].length;n++) {
              accessionedCount = accessionedCount + getIntakeResp['data']['caseDetails'][l]['specimenWiseAccessionedCountForEachIntake'][n]['accessionedCount'];
            }
            getIntakeResp['data']['caseDetails'][l].accessionedCount = accessionedCount;
          }
          ///////////////////// last reported date
          if (lastReported['data']['length'] > 0) {
            var lastreportedData = this.populateLastRepoterdData(getIntakeResp,lastReported,searchString).then(popLastResp=>{
              // console.log('popLastResp',popLastResp);
              resolve();
            });



          }
          else{

              this.allIntakes = getIntakeResp.data.caseDetails;
              console.log("this.allIntakes",this.allIntakes);
              resolve()


          }
        }

      });
      })

  }
  megaSearch(intakeUrl,reqSearchAllIntakes,clientSearchUrl,clientReq,searchString){
    return new Promise((resolve, reject)=>{
      var  searchUSerData      =
      {
        header              : {
          uuid              : "",
          partnerCode       : "",
          userCode          : "",
          referenceNumber   : "",
          systemCode        : "",
          moduleCode        : "",
          functionalityCode: "SPCA-MI",
          systemHostAddress : "",
          remoteUserAddress : "",
          dateTime          : ""
        },
        data                : {
          partnerCode:'sip',
          searchQuery:""
        }
      }
      if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UI") != -1) {
        searchUSerData['header'].functionalityCode    = "SPCA-UI";
      }
      else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-CI") != -1){
        searchUSerData['header'].functionalityCode    = "SPCA-CI";
      }
      else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-MI") != -1){
        searchUSerData['header'].functionalityCode    = "SPCA-MI";
      }
      searchUSerData.data.partnerCode = this.logedInUserRoles.partnerCode;
      searchUSerData.data.searchQuery = searchString;
      searchUSerData.header.partnerCode = this.logedInUserRoles.partnerCode;
      searchUSerData.header.userCode = this.logedInUserRoles.userCode;
      // searchUSerData.header.functionalityCode = "SPCA-CI";
      var userUrl = environment.API_USER_ENDPOINT + 'searchallusers';
      var lastUrl = environment.API_REPORTING_ENDPOINT+'searchintakereportsbydate';
      var requestBody = {
        header: {
          uuid: "",
          partnerCode: "sip",
          userCode: "sip-12598",
          referenceNumber: "",
          systemCode: "NGLIS",
          moduleCode: "TWMM",
          functionalityCode: "SPCA-MI",
          systemHostAddress: "",
          remoteUserAddress: "",
          dateTime: ""
        },
        data: {
          searchQuery: "",
          userCodes  :[]
        }
      }
      requestBody.data.searchQuery = searchString;
      if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UI") != -1) {
        requestBody['header'].functionalityCode    = "SPCA-UI";
      }
      else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-CI") != -1){
        requestBody['header'].functionalityCode    = "SPCA-CI";
      }
      else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-MI") != -1){
        requestBody['header'].functionalityCode    = "SPCA-MI";
      }

      var clientResult = this.http.post(clientSearchUrl,clientReq).toPromise().then(clientresp=>{
        return clientresp;
      })
      var intakeResult = this.http.post(intakeUrl,reqSearchAllIntakes).toPromise().then(patientresp=>{
        return patientresp;
      })
      // var userResult = this.http.put(userUrl,searchUSerData).toPromise().then(userresp=>{
      //   return userresp;
      // })
      // var reportedDateResult = this.http.put(lastUrl,requestBody).toPromise().then(reportedresp=>{
      //   return reportedresp;
      // })

      forkJoin([clientResult, intakeResult]).subscribe(allresults => {
        var client  = allresults[0];
        var intake  = allresults[1];

        console.log("client",client);
        console.log("intake",intake);

        if (intake['data']['caseDetails'].length == 0 && client['data'].length == 0) {
          this.allIntakesCount = 0;
          resolve();
        }
        else if(intake['data']['caseDetails'].length > 0 && client['data'].length == 0){
          ///////////////// client returns zero but client greater then zer
          // this.allIntakesCount = intake['data'].totalCount;
          if (searchString.toLowerCase() == 'client') {
            intake['data']['caseDetails'] = intake['data']['caseDetails'].filter(function(value, index, arr){
              // console.log("value.collectedBy",value.collectedBy);

              return value.collectedBy == 2;
            });

          }
          else if(searchString.toLowerCase() == 'sip'){
            intake['data']['caseDetails'] = intake['data']['caseDetails'].filter(function(value, index, arr){
              // console.log("value.collectedBy",value.collectedBy);

              return value.collectedBy == 1;
            });
          }
          this.allIntakesCount = intake['data'].totalCount;
            this.populateIntakeTable(intake,searchString,[]).then(popres =>{
              if (this.greenColor == true) {
                this.allIntakes[0].greenColor = true;
              }
              resolve()
            })

        }
        else if(intake['data']['caseDetails'].length == 0 && client['data'].length > 0){
          ///////////////// intake returns zero but client retuns greater then zero
          var uniqueClientId = [];
          const map1 = new Map();
          for (const item of client['data']) {
            if(!map1.has(item.clientId)){
              map1.set(item.clientId, true);    // set any value to Map
              uniqueClientId.push(item.clientId);
            }
          }

          var searchUrl = environment.API_SPECIMEN_ENDPOINT + 'showintakes';
          var reqAllIntakes = {...this.allIntakesReq}
          reqAllIntakes.data.searchString = "byclient";
          reqAllIntakes.data.clientId     = uniqueClientId;
          reqAllIntakes.header.partnerCode = this.logedInUserRoles.partnerCode;
          reqAllIntakes.header.userCode = this.logedInUserRoles.userCode;
          reqAllIntakes.header.functionalityCode = "SPCA-MI";
          // if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UI") != -1) {
          //   reqAllIntakes['header'].functionalityCode    = "SPCA-UI";
          // }
          // else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-CI") != -1){
          //   reqAllIntakes['header'].functionalityCode    = "SPCA-CI";
          // }
          // else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-MI") != -1){
          //   reqAllIntakes['header'].functionalityCode    = "SPCA-MI";
          // }
          this.specimenService.getIntakes(searchUrl, reqAllIntakes).subscribe(getIntakeByClientIdResp=>{
            console.log("getIntakeByClientIdResp",getIntakeByClientIdResp);
            this.allIntakesCount = getIntakeByClientIdResp['data'].totalCount;
            this.populateIntakeTable(getIntakeByClientIdResp,searchString,[]).then(popres =>{
              if (this.greenColor == true) {
                this.allIntakes[0].greenColor = true;
              }
              resolve();
            })
           })


        }
        else if(intake['data']['caseDetails'].length > 0 && client['data'].length > 0){
          ///////////////// both return record.
          if (searchString.toLowerCase() == 'client' || searchString.toLowerCase() == 'sip') {
            this.allIntakesCount = intake['data'].totalCount;
            this.populateIntakeTable(intake,searchString,[]).then(popres =>{
              if (this.greenColor == true) {
                this.allIntakes[0].greenColor = true;
              }
              resolve();
            })
            return;

          }
          else{
            var uniqueClientId = [];
            const map1 = new Map();
            for (const item of client['data']) {
              if(!map1.has(item.clientId)){
                map1.set(item.clientId, true);    // set any value to Map
                uniqueClientId.push(item.clientId);
              }
            }
            for (let i = 0; i < intake['data']['caseDetails'].length; i++) {
              if (uniqueClientId.indexOf(intake['data']['caseDetails'][i].clientId) !== -1) {
                    ////////// already exist
                  }
                  else{
                    uniqueClientId.push(intake['data']['caseDetails'][i].clientId)
                  }
            }
            console.log('uniqueClientId----',uniqueClientId);
            var searchUrl = environment.API_SPECIMEN_ENDPOINT + 'showintakes';
            var reqAllIntakes = {...this.allIntakesReq}
            reqAllIntakes.data.searchString = "byclient";
            reqAllIntakes.data.clientId     = uniqueClientId;
            reqAllIntakes.header.partnerCode = this.logedInUserRoles.partnerCode;
            reqAllIntakes.header.userCode = this.logedInUserRoles.userCode;
            reqAllIntakes.header.functionalityCode = "SPCA-MI";

            this.specimenService.getIntakes(searchUrl, reqAllIntakes).subscribe(getIntakeByClientIdResp=>{
              console.log("getIntakeByClientIdResp",getIntakeByClientIdResp);
              // if (searchString.toLowerCase() == 'client') {
              //   getIntakeByClientIdResp['data']['caseDetails'] = getIntakeByClientIdResp['data']['caseDetails'].filter(function(value, index, arr){
              //     // console.log("value.collectedBy",value.collectedBy);
              //
              //     return value.collectedBy == 2;
              //   });
              //
              // }
              // else if(searchString.toLowerCase() == 'sip'){
              //   getIntakeByClientIdResp['data']['caseDetails'] = getIntakeByClientIdResp['data']['caseDetails'].filter(function(value, index, arr){
              //     // console.log("value.collectedBy",value.collectedBy);
              //
              //     return value.collectedBy == 1;
              //   });
              // }
              this.allIntakesCount = getIntakeByClientIdResp['data'].totalCount;
              this.populateIntakeTable(getIntakeByClientIdResp,searchString,[]).then(popres =>{
                if (this.greenColor == true) {
                  this.allIntakes[0].greenColor = true;



                }
                // for (let i = 0; i < this.allIntakes.length; i++) {
                //   console.log("this.allIntakes",this.allIntakes[i]);
                //   console.log("this.allIntakes[clientId]",this.allIntakes[i].clientId);
                //   console.log("this.allIntakes[clientName]",this.allIntakes[i].clientName);
                //   console.log("this.allIntakes[collectionTimestamp]",this.allIntakes[i].collectionTimestamp);
                //   if (this.allIntakes[i]['clientName'].toLowerCase().indexOf( searchString.toLowerCase()) !== -1 || this.allIntakes[i]['intakeId'].toLowerCase().indexOf( searchString.toLowerCase()) !== -1
                // || this.allIntakes[i]['collectedBy'].toLowerCase().indexOf( searchString.toLowerCase()) !== -1 || this.allIntakes[i]['collectionTimestamp'].toLowerCase().indexOf( searchString.toLowerCase()) !== -1
                // || this.allIntakes[i]['createdTimestamp'].toLowerCase().indexOf( searchString.toLowerCase()) !== -1){
                //     this.allIntakes.splice(i, 1);
                //   }
                //
                //
                // }
                resolve();
              })
             })
          }

        }

      })
    })
  }

  get f() { return this.intakeForm.controls; }
  saveIntake(){
    this.ngxLoader.start();
    this.submitted                  = true;
    if (this.intakeForm.invalid) {
      // console.log("this.allemptySpecError",this.allemptySpecError);
      $('#submitModal').modal('hide');
      this.ngxLoader.stop();

      return;
    }
    if (this.allemptySpecError == true) {
      $('#submitModal').modal('hide');
      this.ngxLoader.stop();
      return;
    }
    if (this.attachmentTable.length == 0) {
      alert('Please provide atleast one attachment')
      $('#submitModal').modal('hide');
      this.ngxLoader.stop();
      return;
    }
    var myDate = new Date();
    var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyy');
    var currentTimeStamp = this.datePipe.transform(myDate, 'MM-dd-yyy HH:mm:ss');
    this.saveRequest.data.clientId = this.clientSearchString.clientId;
    this.saveRequest.data.collectionTimestamp = this.datePipe.transform(this.saveRequestCollectionDate, 'MM-dd-yyy HH:mm:ss');
    this.saveRequest.data.createdBy = this.logedInUserRoles.userCode;
    this.saveRequest.header.userCode = this.logedInUserRoles.userCode;
    this.saveRequest.header.partnerCode = this.logedInUserRoles.partnerCode;
    this.saveRequest.header.functionalityCode = "SPCA-CI";
    this.saveRequest.data.createdTimestamp = currentTimeStamp;
    var np = 0;
    var n  = 0;
    var o  = 0;
    var s  = 0;
    if (this.swabNP == null || this.swabNP == "") {

    }
    else{
      np = parseInt(this.swabNP);
    }
    var holder= {};
    holder['specimenId'] =2; ///////////NP
    holder['count'] =np; ///////////NP
    this.saveRequest.data.intakeSpecimen.push(holder)

    if (this.swabN == null || this.swabN == "") {

    }
    else{
      n = parseInt(this.swabN);
    }
    holder= {};
    holder['specimenId'] =1; ///////////N
    holder['count'] =n; ///////////N
    this.saveRequest.data.intakeSpecimen.push(holder)

    if (this.swabO == null || this.swabO == "") {

    }
    else{
      o = parseInt(this.swabO);
    }
    holder= {};
    holder['specimenId'] =3; ///////////O
    holder['count'] =o; ///////////O
    this.saveRequest.data.intakeSpecimen.push(holder)

    if (this.swabS == null || this.swabS == "") {

    }
    else{
      s = parseInt(this.swabS);
    }
    holder= {};
    holder['specimenId'] =4; ///////////S
    holder['count'] =s; ///////////S
    this.saveRequest.data.intakeSpecimen.push(holder)
    if (this.saveRequest.data.notes == null || this.saveRequest.data.notes == '') {
        // this.saveRequest.data.notes = "";
    }else{
      this.saveRequest.data.notes.replace(/[^a-zA-Z0-9]/g, '');
    }


    console.log("this.saveRequest",this.saveRequest);
    // console.log("this.attachmentTable",this.attachmentTable);

    $('#submitModal').modal('hide');
    // return;
    var saveIntakeUrl = environment.API_SPECIMEN_ENDPOINT+'createintake';

    this.specimenService.createIntake(saveIntakeUrl,this.saveRequest).subscribe(saveResp=>{
      if (saveResp['result'].codeType == 'S') {
        this.greenColor = false;
        if (this.attachmentTable.length>0) {
          this.saveAttachments(saveResp['data'].intakeId).then(res => {
            this.ngxLoader.stop();
            this.notifier.notify('success',"Intake "+saveResp['data'].intakeId+" created successfully")
            this.submitted = false;
            this.greenColor = true;

            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.ajax.reload();
            });
            $('#createnewintakeModal').modal('hide')
            this.resetFields()
            this.clientSearchString = null;
            this.searchTerm$.next(null)
          })
        }
        else{
          this.ngxLoader.stop();
          this.notifier.notify('success','Intake created successfully')
          this.submitted = false;
          this.greenColor = true;

          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.ajax.reload();
          });
          $('#createnewintakeModal').modal('hide')
          this.resetFields();
          this.clientSearchString = null;
          this.searchTerm$.next(null)
        }

      }
      else{
        this.ngxLoader.stop();
        this.notifier.notify('error','Error while creating Intake')
      }
    })

    return;


  }

  collectDateChange(e){
    // console.log("ee",e.target.value);
    if (e.target.value > (this.maxDate+'T24:00')) {
      // alert("date greater then current"+e.target.value)
      // this.saveRequest.data.collectionDate = null;
      this.saveRequestCollectionDate = null;

    }
    // console.log("collectDate",this.collectDate);

  }
  popFile(e){
    // this.ngxLoader.start();
    var input = e.target;
    // console.log("input",input.files);
    // console.log("this.selectattachmentTypeId",this.selectattachmentTypeId);
    if (typeof input.files[0] == 'undefined') {
      return;
    }
    if (this.attachmentTable.length ==3) {
      alert("Only three files are allowed")
      return;
    }

    var attTemp = {}
    var optionSelected = $('#attachment-select option:selected').attr('attname');

    // this.saveRequest.data.caseAttachments[0].attachmentTypeId = this.selectattachmentTypeId;
    if (this.selectattachmentTypeId != null) {
      var mb = input.files[0].size/1048576;
      var sl = input.files[0].name.length;
      attTemp['nameToDownload'] = input.files[0].name;
      // console.log("input.files[0].name",input.files[0].name);
      if (mb <= 30 && sl <= 40) {
        if (input.files[0].type != 'image/jpeg' && input.files[0].type != 'image/jpg' && input.files[0].type != 'image/png' && input.files[0].type != 'application/pdf') {
          $('#upload-att').val('');
          alert('invalid file type');
          return;
        }
        attTemp['payLoad']  = input.files[0];
        if (input.files[0].type == 'image/jpeg') {attTemp['fileTypeId']  = '3';}
        else if(input.files[0].type == 'image/jpg') {attTemp['fileTypeId']  = '3';}
        else if(input.files[0].type == 'image/png') {attTemp['fileTypeId']  = '4';}
        else if(input.files[0].type == 'application/pdf') {attTemp['fileTypeId']  = '1';}
        attTemp['fileType']  = input.files[0].type;
        attTemp['attachId'] = this.selectattachmentTypeId;
        var reader = new FileReader();
        reader.onload = (e: any) => {
          // console.log('Got here: ', e.target.result);
          attTemp['fileurl'] = this.sanitizer.bypassSecurityTrustUrl(e.target.result);
          // this.obj.photoUrl = e.target.result;
        }
        reader.readAsDataURL(input.files[0]);
        var fileName = input.files[0].name;
        attTemp['type'] = optionSelected;
        // attTemp['type'] = this.selectattachmentTypeId;
        attTemp['file'] = fileName;
        attTemp['notDownload'] = true;
        this.attachmentTable.push(attTemp);
        this.ngxLoader.stop();
        if (this.attachmentTable.length >0) {
          this.intakeForm.get('attType').clearValidators();
          this.intakeForm.controls["attType"].updateValueAndValidity();
          this.intakeForm.get('attFile').clearValidators();
          this.intakeForm.controls["attFile"].updateValueAndValidity();
        }
        else{
          this.intakeForm.get('attType').setValidators([Validators.required])
          this.intakeForm.controls["attType"].updateValueAndValidity();
          this.intakeForm.get('attFile').setValidators([Validators.required])
          this.intakeForm.controls["attFile"].updateValueAndValidity();
        }
        // console.log("this.attachmentTable",this.attachmentTable);

      }
      else{
        this.ngxLoader.stop();
        $('#upload-att').val('');
        alert('provide file less than 30 MB')
      }

    }
    else{
      this.ngxLoader.stop();
      $('#upload-att').val('');
      alert('Select Attacment Type');
    }

  }
  toNumeric(e){
    var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})/);
    e.target.value = !x[2] ? x[1] :+'';
  }
  swabChange(e){
    var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})/);
    e.target.value = !x[2] ? x[1] :+'';
    // console.log("this.swabNP",this.swabNP);
    // console.log("this.swabNP",typeof this.swabNP);
    // console.log("this.swabN",this.swabN);
    // console.log("this.swabN",typeof this.swabN);
    // console.log("this.swabo",this.swabO);
    // console.log("this.swabo",typeof this.swabO);
    // console.log("this.swabs",this.swabS);
    // console.log("this.swabs",typeof this.swabS);
    var np = 0;
    var n  = 0;
    var o  = 0;
    var s  = 0;
    if (this.swabNP == null) {

    }
    else{
      if (this.swabNP == "" || typeof this.swabNP !='string') {
        np = 0
      }
      else{
        np = parseInt(this.swabNP)
      }

    }
    if (this.swabN == null) {

    }
    else{
      if (this.swabN == "" || typeof this.swabN !='string') {
        n = 0
      }
      else{
        n = parseInt(this.swabN)
      }

    }
    if (this.swabO == null) {

    }
    else{
      if (this.swabO == "" || typeof this.swabO !='string') {
        o = 0
      }
      else{
        o = parseInt(this.swabO)
      }

    }
    if (this.swabS == null) {

    }
    else{
      if (this.swabS == ""  || typeof this.swabS !='string') {
        s = 0
      }
      else{
        s = parseInt(this.swabS)
      }

    }

    this.totalSpecimen = np + n +o + s;
    // console.log('this.totalSpecimen',this.totalSpecimen);
    if (this.totalSpecimen >0) {
      this.allemptySpecError = false
      // this.intakeForm.get('np').clearValidators();
      // this.intakeForm.controls["np"].updateValueAndValidity();
      // this.intakeForm.get('n').clearValidators();
      // this.intakeForm.controls["n"].updateValueAndValidity();
      // this.intakeForm.get('s').clearValidators();
      // this.intakeForm.controls["s"].updateValueAndValidity();
      // this.intakeForm.get('o').clearValidators();
      // this.intakeForm.controls["o"].updateValueAndValidity();
    }
    else{
      this.allemptySpecError = true;

      // this.intakeForm.get('np').setValidators([Validators.required, Validators.maxLength(3)])
      // this.intakeForm.controls["np"].updateValueAndValidity();
      // this.intakeForm.get('n').setValidators([Validators.required, Validators.maxLength(3)])
      // this.intakeForm.controls["n"].updateValueAndValidity();
      // this.intakeForm.get('s').setValidators([Validators.required, Validators.maxLength(3)])
      // this.intakeForm.controls["s"].updateValueAndValidity();
      // this.intakeForm.get('o').setValidators([Validators.required, Validators.maxLength(3)])
      // this.intakeForm.controls["o"].updateValueAndValidity();
    }



  }

  saveAttachments(intakeId){
    // if (caseNumber == null) {
    //   caseNumber = caseIdforFiles;
    // }
      return new Promise((resolve, reject) => {
        var saveFileUrl = environment.API_SPECIMEN_ENDPOINT + "uploadfile";
        var saveFileReq = {
          header:{
            uuid:"",
            partnerCode:"",
            userCode:"",
            referenceNumber:"",
            systemCode:"",
            moduleCode:"SPCM",
            functionalityCode: "SPCA-MI",
            systemHostAddress:"",
            remoteUserAddress:"",
            dateTime:""
          },
          data:{
            caseNumber:"",
          }
        }
        saveFileReq['header'].partnerCode = this.logedInUserRoles.partnerCode;
        saveFileReq['header'].userCode = this.logedInUserRoles.userCode;
        saveFileReq['header'].functionalityCode = "SPCA-CI";

          var saveMaxBody    = [];
          var saveBodyHolder = {}
          var loopCount = 0;
          for (let i = 0; i < this.attachmentTable.length; i++) {

            console.log('out loopCount',loopCount);
            // console.log('out this.attachmentTable.length',this.attachmentTable);
            console.log("out saveMaxBody",saveMaxBody);

            if (this.attachmentTable[i]['payLoad'] == '') {

              ////// no need to upload
              if (i == (this.attachmentTable.length -1)) {
                resolve();
              }
              loopCount ++;


            }
            else{
              saveFileReq.data.caseNumber = intakeId;
              var myDate = new Date();
              var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyy');
              var currentTimeStamp = this.datePipe.transform(myDate, 'MM-dd-yyy HH:mm:ss');
              // saveFileReq.data.caseAttachments['caseAttachmentId'] = this.attachmentTable[i]['attachId'];
              let formRequestData = new FormData();
              formRequestData.append('file', this.attachmentTable[i]['payLoad']);
              var reqString = JSON.stringify(saveFileReq);
              formRequestData.append('specimentRequest', reqString);

              this.specimenService.uploadFiles(saveFileUrl,formRequestData).subscribe(fileResp =>{
                console.log("*fileResp",fileResp);

                if (fileResp['data'] != null) {
                  saveBodyHolder['intakeId']         = intakeId;
                  saveBodyHolder['attachmentTypeId'] = this.attachmentTable[i]['attachId'];
                  saveBodyHolder['file']             = fileResp['data']['fileName'];
                  saveBodyHolder['fileTypeId']       = this.attachmentTable[i]['fileTypeId'];
                  // saveBodyHolder['createdBy']        = this.logedInUserRoles.userCode;
                  // saveBodyHolder['createdTimestamp'] = currentTimeStamp;
                  saveMaxBody.push(saveBodyHolder);
                  saveBodyHolder = {};

                }
                loopCount ++;

                // console.log('IN loopCount',loopCount);
                // console.log('IN this.attachmentTable.length',this.attachmentTable.length);
                // console.log("IN saveMaxBody",saveMaxBody);


                if(loopCount == this.attachmentTable.length){
                  // console.log('IN LOOP loopCount',loopCount);
                  // console.log('IN LOOP this.attachmentTable.length',this.attachmentTable.length);
                  // console.log("IN LOOP saveMaxBody",saveMaxBody);


                  ////// call save attachment end point
                  var saveAttReqBody={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "SPCA-MI",systemHostAddress:"",remoteUserAddress:"",dateTime:""},
                  data:saveMaxBody}
                  var saveAttUrl    = environment.API_SPECIMEN_ENDPOINT + "saveintakeattachment";
                  this.specimenService.saveAttachmentsIntake(saveAttUrl, saveAttReqBody).subscribe(attResp =>{
                    console.log("attResp",attResp);
                    resolve();
                  },
                error=>{
                  this.ngxLoader.stop();
                  this.notifier.notify('error',"error while saving files")
                })

                }
              },
              error=>{
                this.ngxLoader.stop();
                this.notifier.notify('error',"error while saving files")
              })
            }


          }



          // console.log('f1');
          // resolve();
      });
  }
  loadCreateMinAccession(intakeId,clientId,collectionTimestamp,collectedBy,specimenTypeId){
    var holder = {};
    holder['intakeId']            = intakeId;
    holder['clientId']            = clientId;
    holder['collectionTimestamp'] = collectionTimestamp;
    holder['collectedBy']         = collectedBy;
    holder['specimenTypeId']         = specimenTypeId;
    holder['manageIntake']         = true;
    this.dataForCreateCaseFromIntake = holder;
    // console.log('dataForCreateCaseFromIntake',this.dataForCreateCaseFromIntake);
    this.sharedIntakeData.changeRecordFromIntake(this.dataForCreateCaseFromIntake);
    $('#createnewcaseModal').modal('show');

  }

  loadFiles(intakeId){
    this.intakeIdToDownloadFile = intakeId;
    var filesURL = environment.API_SPECIMEN_ENDPOINT + 'loadfiles';
    var attData   = []
    var attHolder = {}
    var getFilesReq={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "SPCA-MI",systemHostAddress:"",remoteUserAddress:"",dateTime:""},data:{intakeId:intakeId}}
    if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-MC") != -1) {
      getFilesReq['header'].functionalityCode    = "SPCA-MC";
    }
    else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-MI") != -1){
      getFilesReq['header'].functionalityCode    = "SPCA-MI";
    }
    this.http.post(filesURL,getFilesReq).toPromise().then(fileResp =>{
      console.log("fileResp",fileResp);
      if (fileResp['data'] != null) {
          for (let m = 0; m < fileResp['data'].length; m++) {
            attHolder['payLoad'] = '';
            attHolder['payLoad'] = '';
            // var fi = fileResp['data'][m].file.split('/');
            // var le = fi.length-1;
            var ft = fileResp['data'][m].fileName.split('.');
            // var ft = fi[le].split(".");
            var mi = ft.length - 1;
            // var ftype = ft[mi]
            var ftype = ft[1]

            // attHolder['file'] = fi[le];
            attHolder['file'] = fileResp['data'][m].fileName;
            attHolder['fileType'] = ftype;
            attHolder['fileUrl']  = fileResp['data'][m].file;
            attHolder['fileId']   = fileResp['data'][m].caseAttachmentId;
            for (let n = 0; n < this.allAttachmentTypes.length; n++) {
              if (this.allAttachmentTypes[n].attachmentTypeId == fileResp['data'][m].attachmentTypeId) {
                attHolder['type'] = this.allAttachmentTypes[n].attachmentTypeName;
              }

            }
            attData.push(attHolder);
            attHolder = {}
          }
          this.attachmentTableSelected = attData;
          console.log('this.attachmentTableSelected',this.attachmentTableSelected);



      }
      else{
        this.attachmentTableSelected              = [];
        // this.selectattachmentTypeId       =null;
      }
      $('#attachment-modal').modal('show');



     // this.attachmentTable = attData;
    })
  }

  downloadFile(intakeId,file,type,fileId){
    console.log("intakeId",intakeId);
    // console.log("file",file);
    // console.log("type",type);
    console.log("fileId",fileId);

    this.ngxLoader.start();
    var downloadUrl = environment.API_SPECIMEN_ENDPOINT+"getfile"
    var downloadReq={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "SPCA-UI",systemHostAddress:"",remoteUserAddress:"",dateTime:""},
    data:{intakeId:this.intakeIdToDownloadFile,fileUrl:'',fileId:fileId}}
    if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UI") != -1) {
      downloadReq['header'].functionalityCode    = "SPCA-UI";
    }
    else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-CI") != -1){
      downloadReq['header'].functionalityCode    = "SPCA-CI";
    }
    else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-MI") != -1){
      downloadReq['header'].functionalityCode    = "SPCA-MI";
    }
    this.specimenService.downloadFiles(downloadUrl,downloadReq,type).subscribe(downloadResp=>{
      console.log("downloadResp",downloadResp);
      this.ngxLoader.stop();

    },
  error=>{
    this.ngxLoader.stop();
    this.notifier.notify("error","Error while downloading the file.")
  })

  }

  getLookups(){
    this.ngxLoader.start();
    return new Promise((resolve, reject) => {

      var specType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=SPECIMEN_TYPE_CACHE&partnerCode=sip').then(response =>{
        return response;
      })
      var attachType = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=ATTACHMENT_TYPE_CACHE&partnerCode=sip').then(response =>{
        return response;
      })

      forkJoin([specType,attachType]).subscribe(allLookups => {
        // console.log("allLookups",allLookups);
        this.allSpecimenTypes      = allLookups[0];
        this.allAttachmentTypes    = this.sortPipe.transform(allLookups[1], "asc", "attachmentTypeName");

        resolve();

      })
    })

  }
  clientChanged(){
    // console.log("this.saveRequest",this.saveRequest);
    if (this.clientSearchString != null) {
      this.saveRequest.data.clientId = this.clientSearchString.clientId;
      console.log("client Changed",this.clientSearchString);
    }
    else{
      this.saveRequest.data.clientId = null;
    }
  }

  downloadStatic(fileurl,name){
    // console.log("",fileurl);
    // console.log("--",fileurl.changingThisBreaksApplicationSecurity);


    const downloadLink = document.createElement("a");
    // var fileName="";
    // if (input.files[0].type == 'image/jpeg') {fileName  = '';}
    // else if(input.files[0].type == 'image/jpg') {fileName  = '3';}
    // else if(input.files[0].type == 'image/png') {fileName  = '4';}
    // else if(input.files[0].type == 'application/pdf') {fileName  = '1';}
    downloadLink.href = fileurl.changingThisBreaksApplicationSecurity;
    downloadLink.download = name;

    downloadLink.click();
  }

  resetFields(){
    var dtToday = new Date();

   var m = dtToday.getMonth()+1;
   var d = dtToday.getDate();
   var year = dtToday.getFullYear();
   var hour = this.zeroPaddedTime(dtToday.getHours());
   var min = this.zeroPaddedTime(dtToday.getMinutes());
   var sec = this.zeroPaddedTime(dtToday.getSeconds());
   var month;
    var day;
   if(m < 10)
       month = '0' + m.toString();
  else
     month = m
   if(d < 10)
       day = '0' + d.toString();
  else
   day = d

   var maxDate = year + '-' + month + '-' + day;

   // this.intakeForm.reset();
   this.intakeForm.controls['notes'].reset();
   this.intakeForm.controls['countKits'].reset();
   this.intakeForm.controls['countKitsLeft'].reset();
   this.intakeForm.controls['countKits'].reset();
   this.intakeForm.controls['nP'].reset();
   this.intakeForm.controls['n'].reset();
   this.intakeForm.controls['s'].reset();
   this.intakeForm.controls['o'].reset();
   // this.intakeForm.controls['attType'].reset();
   this.intakeForm.controls['attFile'].reset();
   this.allClientName = [];
   this.clientSearchString = null;
   this.saveRequestCollectionDate = year + '-' + month + '-' + day+'T'+hour+':'+min+':'+sec;
   // console.log('this.saveRequestCollectionDate',this.saveRequestCollectionDate);

   this.selectattachmentTypeId = 6;
   if (this.logedInUserRoles.userType == 1) {
     this.saveRequest.data.collectedBy = "1"
   }
   else{
     this.saveRequest.data.collectedBy = "2"
   }
   this.attachmentTable = [];
   this.totalSpecimen = 0;
  }

  loadSepcimenSearch(specimenId,intakeId){
    var spec = '';
    if (specimenId == 1) {spec = 'Draft'}
    if (specimenId == 2) {spec = 'Triaged'}
    if (specimenId == 3) {spec = 'Accessioned'}
    if (specimenId == 4) {spec = 'In Progress'}
    if (specimenId == 5) {spec = 'Partial Under Reporting'}
    if (specimenId == 6) {spec = 'Under Reporting'}
    if (specimenId == 7) {spec = 'Partial Signed Off'}
    if (specimenId == 8) {spec = 'Signed Off'}
    if (specimenId == 9) {spec = 'Partial Delivered'}
    if (specimenId == 10) {spec = 'Delivered'}
    if (specimenId == 11) {spec = 'Cancelled'}
    if (specimenId == 12) {spec = 'Not Delivered'}
    console.log();

    var searchBy = 'specimentTypeIntake';
    var data = {specimenId:spec,intakeId:intakeId,searchBy:searchBy}
    console.log("data",data);

    this.router.navigateByUrl('/manage-cases', { state: data })
  }

  populateLastRepoterdData(getIntakeResp,lastRepotedBy,searchString){
    return new Promise((resolve, reject) => {
      var uniqueUserIds = [];
      const map1 = new Map();
      for (const item of lastRepotedBy['data']) {
        if(!map1.has(item.createdBy)){
          map1.set(item.createdBy, true);    // set any value to Map
          uniqueUserIds.push(item.createdBy);
        }
      }
      var userRequest = {...this.usersRequest};
      userRequest.data.userCodes = uniqueUserIds;
      userRequest.header.partnerCode  = this.logedInUserRoles.partnerCode;
      userRequest.header.userCode     = this.logedInUserRoles.userCode;
      // userRequest.header.functionalityCode     = "SPCA-CI";
      if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-UI") != -1) {
        userRequest['header'].functionalityCode    = "SPCA-UI";
      }
      else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-CI") != -1){
        userRequest['header'].functionalityCode    = "SPCA-CI";
      }
      else if (this.logedInUserRoles['allowedRoles'].indexOf("SPCA-MI") != -1){
        userRequest['header'].functionalityCode    = "SPCA-MI";
      }
      var userUrl = environment.API_USER_ENDPOINT + "getusersbyusercodes";
      this.specimenService.getUserRecord(userUrl,userRequest).then(getUserResp =>{
        // return getUserResp;
        console.log("getUserResp",getUserResp);
        if (getUserResp['data'] != null) {
          var count = 0;
          for (let index = 0; index < getUserResp['data'].length; index++) {
            for (let k = 0; k < lastRepotedBy['data'].length; k++) {

              if (getUserResp['data'][index]['userCode'] == lastRepotedBy['data'][k]['createdBy']) {
                lastRepotedBy['data'][k]['createdByName'] = getUserResp['data'][index]['fullName'];
                // count++;
              }
              else{
                if (typeof lastRepotedBy['data'][k]['createdByName'] == 'undefined') {
                  lastRepotedBy['data'][k]['createdByName'] = '';
                }
                // count++;
              }

            }
            count++;
            if (count == getUserResp['data'].length) {
              for (let index = 0; index < getIntakeResp['data']['caseDetails'].length; index++) {
                for (let k = 0; k < lastRepotedBy['data'].length; k++) {
                  if (getIntakeResp['data']['caseDetails'][index]['intakeId'] == lastRepotedBy['data'][k]['intakeId']) {
                    getIntakeResp['data']['caseDetails'][index]['lastReportedDate'] = lastRepotedBy['data'][k]['createdTimestamp'];
                    getIntakeResp['data']['caseDetails'][index]['lastReportedBy'] = lastRepotedBy['data'][k]['createdByName'];
                  }

                }
                // if (searchString.toLowerCase() == 'client' || searchString.toLowerCase() == 'sip') {
                //   if (getIntakeResp['data']['caseDetails'][index]['clientName'].toLowerCase().indexOf('client') === -1) {
                //           getIntakeResp['data']['caseDetails'] = getIntakeResp['data']['caseDetails'].filter(function(value, index, arr){
                //             // console.log("value.collectedBy",value.collectedBy);
                //             return value.collectedBy == 2;
                //           });
                //
                //   }
                //   else if (getIntakeResp['data']['caseDetails'][index]['clientName'].toLowerCase().indexOf('sip') === -1) {
                //           getIntakeResp['data']['caseDetails'] = getIntakeResp['data']['caseDetails'].filter(function(value, index, arr){
                //             // console.log("value.collectedBy",value.collectedBy);
                //             return value.collectedBy == 1;
                //           });
                //   }
                //
                //
                // }

              }

              //   // if (searchString.toLowerCase() == 'client') {
              //   //   getIntakeResp['data']['caseDetails'] = getIntakeResp['data']['caseDetails'].filter(function(value, index, arr){
              //   //     // console.log("value.collectedBy",value.collectedBy);
              //   //     return value.collectedBy == 2;
              //   //   });
              //   //   this.allIntakes = getIntakeResp.data.caseDetails;
              //   //   console.log("this.allIntakes",this.allIntakes);
              //   //   resolve();
              //   // }
              //   // else if (searchString.toLowerCase() == 'sip') {
              //   //   getIntakeResp['data']['caseDetails'] = getIntakeResp['data']['caseDetails'].filter(function(value, index, arr){
              //   //     // console.log("value.collectedBy",value.collectedBy);
              //   //     return value.collectedBy == 1;
              //   //   });
              //   //   this.allIntakes = getIntakeResp.data.caseDetails;
              //   //   console.log("this.allIntakes",this.allIntakes);
              //   //   resolve();
              //   // }
              //
              // }
              // else{
                this.allIntakes = getIntakeResp.data.caseDetails;
                console.log("this.allIntakes",this.allIntakes);
                resolve();

              // }

              // this.allIntakes = getIntakeResp.data.caseDetails;
              // resolve();
            }

          }


        }
        else{
          this.notifier.notify('error','Error while loading reported by')
          resolve();
        }
      },error=>{
        this.notifier.notify('error','Error while loading reported by')
        resolve();
      })

    })


  }

  searchLastReportedBy(searchValue,paginatePage,paginateLength,searchString,from){
    return new Promise((resolve, reject) => {
      var lastUrl = environment.API_REPORTING_ENDPOINT+'searchintakereportsbydate';
      var requestBody = {
        header: {
          uuid: "",
          partnerCode: "sip",
          userCode: "sip-12598",
          referenceNumber: "",
          systemCode: "NGLIS",
          moduleCode: "TWMM",
          functionalityCode: "SPCA-MI",
          systemHostAddress: "",
          remoteUserAddress: "",
          dateTime: ""
        },
        data: {
          searchQuery: "",
          userCodes  :[]
        }
      }
      if (from == 'date') {
        requestBody.data.searchQuery =  searchValue;
      }
      else if(from == 'user'){
          requestBody.data.userCodes = searchValue;
      }
      this.http.put(lastUrl,requestBody).subscribe(lastReported => {
        console.log('lastReported Search',lastReported);
        if (lastReported['data']['length']>0) {
          var uniqueIntakeIds = [];
          var reportByIntakeAndUsers = [];
          const map1 = new Map();
          for (const item of lastReported['data']) {

            if(!map1.has(item.intakeId)){
              map1.set(item.intakeId, true);    // set any value to Map
              if (item.intakeId != null) {
                var tempHolder = {};
                uniqueIntakeIds.push(item.intakeId);
                tempHolder['intakeId'] = item.intakeId;
                tempHolder['userCode'] = item.createdBy;
                reportByIntakeAndUsers.push(tempHolder);
              }
            }
          }
          console.log('reportByIntakeAndUsers',reportByIntakeAndUsers);

          var searchIdUrl = environment.API_SPECIMEN_ENDPOINT+'intakesbyintakesid';
          var reqByid = {
            header: {
              uuid: "67ffaebe-6188-476e-86ae-c5deb2effa80",
              partnerCode: "",
              userCode: "",
              referenceNumber: "",
              systemCode: "",
              moduleCode: "SPCM",
              functionalityCode: "SPCA-MI",
              systemHostAddress: "",
              remoteUserAddress: "",
              dateTime: ""
            },
            data: {
              intakesIds: [],
              pageSize:20,
              pageNumber:0
            }
          }
          reqByid.data.intakesIds = uniqueIntakeIds;
          reqByid.data.pageSize = paginateLength;
          reqByid.data.pageNumber = paginatePage;
          this.specimenService.searchReportedByIntakeIds(searchIdUrl,reqByid).subscribe(searcIdhResp=>{
            console.log("searcIdhResp--",searcIdhResp);
            if (searcIdhResp['data'] == null) {
              this.allIntakesCount = 0;
              this.allIntakes      = [];
                resolve()
            }
            else{
              if (searcIdhResp['data']['caseDetails']['length'] > 0) {
                this.allIntakesCount = searcIdhResp['data']['totalCount'];
                this.lastReportedpopulateIntakeTable(searcIdhResp,searchString,lastReported).then(popres =>{

                  resolve()
                })
              }
              else{
                this.allIntakesCount = 0;
                this.allIntakes      = [];
                  resolve()
              }
            }


          })
        }
        else{
          this.allIntakesCount = 0;
          this.allIntakes      = [];
           resolve();
        }


      })

    })
  }

  goToAddBatch(intakeId){
    var a = intakeId;

    // var ciphertext = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
    var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
    var bx = ax.replace('+','xsarm');
    var cx = bx.replace('/','ydan');
    var ciphertext = cx.replace('=','zhar');
    console.log('ciphertext',ciphertext);
    var url = location.origin +'/add-batch/'+ciphertext;
    window.open( url , '_blank');
    // this.router.navigate(['add-batch', ciphertext]);
  }
  goToEditIntake(intakeId){
    var a = intakeId;

    // var ciphertext = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
    var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
    var bx = ax.replace('+','xsarm');
    var cx = bx.replace('/','ydan');
    var ciphertext = cx.replace('=','zhar');
    console.log('ciphertext',ciphertext);

    this.router.navigate(['edit-intake', ciphertext]);
  }

}
