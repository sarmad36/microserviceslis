import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { DashboardComponent } from './includes/dashboard/dashboard.component';
import { AddBatchComponent } from './batches/add-batch/add-batch.component';
import { ManageBatchesComponent } from './batches/manage-batches/manage-batches.component';
import { WorksheetComponent } from './batches/worksheet/worksheet.component';

import { HomeLayoutComponent } from '../../../../src/app/includes/layouts/home-layout.component';
import { DashboardComponent } from '../../../../src/app/includes/dashboard/dashboard.component';
import { AppGuard } from "../../../../src/services/gaurd/app.guard";

const routes: Routes = [
  {
    path: '',
    component: HomeLayoutComponent,
    children: [
      // {
      //   path      : 'dashboard',
      //   component : DashboardComponent,
      // },
      {
        path      : 'add-batch/:with',
        canActivate: [AppGuard],
        canDeactivate: [AppGuard],
        component : AddBatchComponent,
      },
      {
        path      : 'manage-batches',
        canActivate: [AppGuard],
        canDeactivate: [AppGuard],
        component : ManageBatchesComponent,
      },
      {
        path      : 'edit-batch/:id',
        canActivate: [AppGuard],
        canDeactivate: [AppGuard],
        component : AddBatchComponent, pathMatch: 'full'
      },
      {
        path      : 'worksheet/:id',
        canActivate: [AppGuard],
        canDeactivate: [AppGuard],
        component : WorksheetComponent,
      },
      // {
      //   path      : '',
      //   component : DashboardComponent
      // },
    ]
  }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
