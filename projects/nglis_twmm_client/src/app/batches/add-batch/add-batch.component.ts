import { Component, HostListener, OnInit, ElementRef, ViewChild, QueryList } from '@angular/core';
import { environment } from '../../../../../../src/environments/environment';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NotifierService } from 'angular-notifier';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { GlobalApiCallsService } from '../../services/global/global-api-calls.service';
import { CovidApiCallsService } from '../../services/covid/covid-api-calls.service';
import { concat, Observable, of, Subject, forkJoin } from "rxjs";
import { SortPipe } from "../../pipes/sort.pipe";
import { DataTableDirective } from 'angular-datatables';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2'
import { TableDragService } from "../../services/drag-drop/table-drag.service";
import { Subscription } from "rxjs";
import { moveItemInArray, CdkDragDrop, transferArrayItem } from "@angular/cdk/drag-drop";

declare var $ :any;
import { GlobalySharedModule } from './../../../../../../src/app/sharedmodules/globaly-shared/globaly-shared.module';
import { UrlGuard } from './../../../../../../src/services/gaurd/url.guard'
import * as moment from 'moment';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-add-batch',
  templateUrl: './add-batch.component.html',
  styleUrls: ['./add-batch.component.css'],
  host        : {
    class     :'mainComponentStyle'
  }
})
export class AddBatchComponent implements OnInit {
  // @ViewChild('aform') casenumberInputElement: ElementRef;
  @ViewChild(DataTableDirective, {static: false})



  dtElement              : DataTableDirective;

  batchId         : any =  [];
  caseNumber         : any
  // allBatches         : any =  [];
  allWelNo              : any =  [];
  constantWelNo         : any =  [];
  dtTrigger              : Subject<AddBatchComponent> = new Subject();
  public dtOptions       : any =  {};
  // public dtOptions       : DataTables.Settings = {};

  public batchForm           : FormGroup;
  submitted                 = false;
  disableBtnCheck = true;
  public createBatchRequest = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"SPCM",
      functionalityCode: "TWMA-CNB",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      page:"",
      size:"",
      sortColumn:"",
      sortingOrder:"asc",
      searchDate:"",
      caseNumber:"OX20-6",
      caseStatus:"3"
    }
  }
  public patientRecordRequest = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"PMTM",
      functionalityCode: "TWMA-CNB",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      username: "danish",
      patientIds: []
    }
  }
  public clientRecordRequest = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"CLTM",
      functionalityCode: "TWMA-CNB",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      clientIds : []
    }
  }
  selectedWellNo          = null;
  specimenTableData : any = [];

  public saveBtachRequest = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"TWMM",
      functionalityCode: "TWMA-CNB",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      batchId:"9",
      batchStatusId:"1",
      testStatusId:"2",
      createdBy:"",
      createdTimestamp: "26-11-2020 20:07:25",
      listOfBatchJobOrderIds:[
        // {
        //   jobOrderId:5,
        //   wellMachineId:4,
        //
        // }
      ],
      jobOrderId:[]
    }
  }

  public checktestRequest = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"TWMM",
      functionalityCode: "TWMA-CNB",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      caseIds:[],
      testStatusId:"1",
      testId:2

    }
  }
  public viewBatchRequest = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"TWMM",
      functionalityCode: "TWMA-CNB",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      page:"0",
      size:"3",
      sortColumn:"batchId",
      sortingOrder:"desc",
      batchId:""
    }
  }
  public removeRequest = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"TWMM",
      functionalityCode: "TWMA-CNB",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      batchId:"9",
      testStatusId:"1",
      updatedBy:"",
      updatedTimestamp: "30-11-2020 20:07:25",
      jobOrderId:[]

    }
  }
  public updatePatientRequest = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"PNTM",
      functionalityCode: "TWMA-CNB",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      patientId: null,
      patientDemographics:{
        // firstName:"",
        // updatedtedBy:"",
        // updatedtedTimestamp:"30-11-2020 20:07:25"
        // "contacts":[
        //   {
        //     "contactTypeId":1,
        //     "value":"+92 345 1234567",
        //     "moduleCode":"PNTM",
        //     "createdBy":"sip-C-0011",
        //     "createdTimestamp":"30-11-2020 20:07:25"
        //   }
        //
        // ]
      },
      userName:"danish"

    }
  }
  public updateClientRequest = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"CLIM",
      functionalityCode: "TWMA-CNB",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      clientId:752,
      name:"",
      updatedBy:"bbc"
    }
  }
  public updateSpecimenRequest = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"SPCM",
      functionalityCode: "TWMA-CNB",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      caseId:"",
      collectedBy:1,
      collectionDate:"12-04-2020 20:07:25",
      receivingDate:"12-04-2020 20:07:25",
      updatedBy:"bbc",
      clientId : null

    }
  }
public logedInUserRoles :any = {};
pageType = 'add';
removeCaseData = {};
removeCaseIndex = '';
selectedBatch : any;

public usersRequest: any = {
  header              : {
    uuid              : "",
    partnerCode       : "",
    userCode          : "",
    referenceNumber   : "",
    systemCode        : "",
    moduleCode        : "",
    functionalityCode: "TWMA-CNB",
    systemHostAddress : "",
    remoteUserAddress : "",
    dateTime          : ""
  },
  data                : {
    userCodes           :[]
  }
}
public getCasesRequest = {
  header:{
    uuid:"",
    partnerCode:"",
    userCode:"",
    referenceNumber:"",
    systemCode:"",
    moduleCode:"TWMM",
    functionalityCode: "TWMA-CNB",
    systemHostAddress:"",
    remoteUserAddress:"",
    dateTime:""
  },
  data:{
     caseIds:[]
  }
}
swalStyle              : any;
public updateBtachRequest = {
  header:{
    uuid:"",
    partnerCode:"",
    userCode:"",
    referenceNumber:"",
    systemCode:"",
    moduleCode:"TWMM",
    functionalityCode: "TWMA-CNB",
    systemHostAddress:"",
    remoteUserAddress:"",
    dateTime:""
  },
  data:{
    batchId:"",
    batchStatusId:"",
    testStatusId:"",
    createdBy:"",
    createdTimestamp: "26-11-2020 20:07:22",
    updatedBy:"",
    updatedTimestamp: "3-12-2020 12:07:22",
    listOfBatchJobOrderIds:[
      // {
      //   jobOrderId:5,
      //   wellMachineId:3
      // }
    ]

  }
}
diasbleBack = false;
alreadyExistError = false;
pageLength = 100;
// abstract  canDeactivate(): boolean;
public loadIntakeCaseRequest = {
  header:{
    uuid:"",
    partnerCode:"",
    userCode:"",
    referenceNumber:"",
    systemCode:"",
    moduleCode:"SPCM",
    functionalityCode: "TWMA-CNB",
    systemHostAddress:"",
    remoteUserAddress:"",
    dateTime:""
  },
  data:{
    intakeId:"",
    caseStatusId:"3"
  }
}
timeCount ;
clientLoading               : any;
searchTerm$ = new Subject<string>();
clientSearchString          ;
clientsDropdown              : any = [];
sendOnce = false
  constructor(
    private formBuilder     : FormBuilder,
    private router          : Router,
    private http            : HttpClient,
    private notifier        : NotifierService,
    private ngxLoader       : NgxUiLoaderService,
    private globalService   : GlobalApiCallsService,
    private covidService   : CovidApiCallsService,
    private sortPipe        : SortPipe,
    private elementRef: ElementRef,
    private route        : ActivatedRoute,
    private datePipe: DatePipe,
    private rbac         : UrlGuard,
    private encryptDecrypt : GlobalySharedModule
    // private element: ElementRef
     ) {

    // this.rbac.checkROle('TWMA-CNB');
    this.notifier        = notifier;
    this.swalStyle       =  Swal.mixin({
      customClass : {
        confirmButton : 'btn btn-success my-swal-success',
        cancelButton  : 'btn btn-danger'
      },
      buttonsStyling  : true
    })
  }

  clearStorage(){
    // localStorage.clear();
    localStorage.removeItem('createCovidBatchData');
    this.diasbleBack = false;
  }
  stopUiLoader(uniqueWellArray){
    // console.log('----------');
    var count = 0;

    for (let k = 0; k < uniqueWellArray.length; k++) {
      count++ ;
      var c  = document.getElementsByClassName('WELROW-'+uniqueWellArray[k]['caseId']);
      var well = uniqueWellArray[k]['well'];
      // console.log("c",c);
      c[0]['value'] = well;
      if (count == uniqueWellArray.length) {

        // $('div.dataTables_length select').val(20);
        // $('div.dataTables_length select').trigger('change')

      }


      // console.log("c.val",c[0]['value']);
    }
    // this.specimenTableData = this.sortPipe.transform(this.specimenTableData, "asc", "sellectedWellOrderId");
    console.log("check sorting",this.specimenTableData);


    this.ngxLoader.stop();

  }
  ngOnInit(): void {
    this.covidService.search(this.searchTerm$)
    .subscribe(results => {
      console.log("results",results);
      this.clientsDropdown = results['data'];
      this.clientLoading = false;
    },
    error => {
      this.ngxLoader.stop();
      this.notifier.notify( "error", "Error while fetching clients backend connect aborted" );
      this.clientLoading = false;
      return;
      // console.log("error",error);
        });
    window.addEventListener("beforeunload", function (e) {
      var oldData = JSON.parse(localStorage.getItem('createCovidBatchData'));
      console.log("oldData",oldData);

      if (oldData != null) {
        var confirmationMessage = "o/";
       e.returnValue = ''
       return '';
      }

    });
    // HostListener('window:beforeunload', ['$event'])
    // var oldData = JSON.parse(localStorage.getItem('createCovidBatchData'));
    // if (oldData != null) {
    //     if (!this.canDeactivate()) {
    //         return =true;
    //     }
    // }
    localStorage.removeItem('createCovidBatchData');
    this.diasbleBack = false;
    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
var originalText = bts.toString(CryptoJS.enc.Utf8);
this.logedInUserRoles = JSON.parse(originalText)
    // console.log("this.logedInUserRoles",this.logedInUserRoles);

    // this.logedInUserRoles.userName    = "snazirkhanali";
    // this.logedInUserRoles.partnerCode = 1;
    // this.logedInUserRoles.userCode    = "sip-12598";


    this.route.params.subscribe(params => {
      console.log("params",params);
      if (typeof params['id'] == "undefined") {

        this.pageType = 'add';
        this.rbac.checkROle('TWMA-CNB');

        this.getLookups().then(lookupsLoaded => {
          // this.ngxLoader.stop();
            this.ngxLoader.start();
            var coverted = params['with'].replace('xsarm','+').replace('ydan','/').replace('zhar','=');
            var bytes  = CryptoJS.AES.decrypt(coverted, 'nglis');
            var originalText = bytes.toString(CryptoJS.enc.Utf8).toString();
            console.log("originalText",originalText);


            if (originalText != "") {
            // if (params['with'] != "1") {

              this.getIntakeCasesRecord(originalText).then(intakeRespomse=>{
              // this.getIntakeCasesRecord(params['with']).then(intakeRespomse=>{
                this.populateWells()
              })
            }
            else{
              this.populateWells()
            }



        });
      }
      else{
        // var bytes  = CryptoJS.AES.decrypt(params['id'], 'nglis');
        //
        // var originalText = bytes.toString(CryptoJS.enc.Utf8);
        var coverted = params['id'].replace('xsarm','+').replace('ydan','/').replace('zhar','=');
        var bytes  = CryptoJS.AES.decrypt(coverted, 'nglis');
        var originalText = bytes.toString(CryptoJS.enc.Utf8).toString();
        this.pageType = 'edit';
        this.rbac.checkROle('TWMA-UB');

        this.getLookups().then(lookupsLoaded => {
          var selectURL = environment.API_COVID_ENDPOINT + 'viewbatch';
          var getBatchReq = {...this.viewBatchRequest}
          getBatchReq.data.batchId = originalText;
          // getBatchReq.data.batchId = params['id'];
          getBatchReq.header.partnerCode  = this.logedInUserRoles.partnerCode;
          getBatchReq.header.userCode     = this.logedInUserRoles.userCode;
          getBatchReq.header.functionalityCode     = "TWMA-MBC";
          this.ngxLoader.start();
          this.getSelectedRecord(selectURL,getBatchReq).then(selectedPatient => {
            // this.ngxLoader.stop();
          });


        });

      }
      // console.log("this.pageType",this.pageType);

    })


    this.dtOptions = {
      pagingType   : 'full_numbers',
      // pageLength   : 100,
      pageLength   : this.pageLength,
      // serverSide   : false,
      // processing   : true,
      ordering     : true,
      // rowReorder: true,
      searching: true,
      "order": [[19, "asc" ]],

      columnDefs: [
        {
          targets: [4,6,8,11,14,16,17,18,19],
          visible: false,
          searchable: true,
        },
        {
             orderable: true, className: 'reorder', targets: [2,3,5,7,9,10,13,15,19]
        },
        {
             orderable: false, targets: [0,1,17]
        },
        {
          targets: [12],
          searchable: false,
          orderable: true
        },
        {
          targets: [9],
          searchable: true,
          orderable: true
        }

        // {
        //     "targets": ["_all"],
        //     "visible": true
        // }

      ],
      "lengthMenu": [100],

      rowCallback: (row: Node, data: any[] | Object, index: number) => {
        const self = this;
        // Unbind first in order to avoid any duplicate handler
        // (see https://github.com/l-lin/angular-datatables/issues/87)
        $('td', row).unbind('click');
        $('td', row).bind('click', () => {
          // self.someClickHandler(data);
          // console.log("row",row);
          // console.log("data",data);
          // console.log("index",index);

        });
        return row;
      }

    };



    // this.getLookups().then(lookupsLoaded => {
    //   // this.ngxLoader.stop();
    //   if (this.pageType == 'add') {
    //     this.ngxLoader.start();
    //     this.populateWells()
    //   }
    //   else{
    //
    //   }
    //
    // });


    this.batchForm       = this.formBuilder.group({
      batchId      : ['', [Validators.required]],
      caseNumber   : ['']

    })


  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
    const input = this.elementRef.nativeElement.querySelector(".scaneverywhere");;
    // console.log('input',input);

    if (input) {
      input.focus();
    }



  }
  //
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }


  getSelectedRecord(url,data) {
    // this.ngxLoader.start();
    return new Promise((resolve, reject) => {
      this.covidService.getBatchById(url, data).subscribe(getByIdResp=>{
        console.log("getByIdResp",getByIdResp);
        if (typeof getByIdResp['result'] != 'undefined') {
          if (typeof getByIdResp['result']['description'] != 'undefined') {
            if (getByIdResp['result']['description'].indexOf('UnAuthorized') != -1) {
              this.notifier.notify('warning',getByIdResp['result']['description'])
              this.router.navigateByUrl('/page-restrict');
            }
          }
        }
        if (getByIdResp['result']['codeType'] == 'T') {
          this.notifier.notify("error","No Record Found");
          this.ngxLoader.stop();
          this.router.navigate(['manage-batches']);
          resolve()
          return;
        }
        var userRequest = {...this.usersRequest};
        userRequest.data.userCodes = [getByIdResp['data']['batchDetails']['createdBy']];
        var userUrl = environment.API_USER_ENDPOINT + "clientusers";
        userRequest.header.partnerCode  = this.logedInUserRoles.partnerCode;
        userRequest.header.userCode     = this.logedInUserRoles.userCode;
        userRequest.header.functionalityCode     = "CLTA-VC";
        this.covidService.getUserRecord(userUrl,userRequest).then(getUserResp =>{
          // return getUserResp;
          // console.log("getUserResp",getUserResp);
          getByIdResp['data']['batchDetails'].createdByName = getUserResp['data'][0].fullName;
          this.selectedBatch = getByIdResp['data'];

          const uniqueCaseIds = [];
          const map = new Map();
          for (const item of getByIdResp['data']['jobOrderDetails']) {
            if(!map.has(item.caseId)){
              map.set(item.caseId, true);    // set any value to Map
              uniqueCaseIds.push(item.caseId);
            }
          }

          var casesUrl     = environment.API_SPECIMEN_ENDPOINT + 'findbycaseidin';
          var casesRequest = {...this.getCasesRequest}
          casesRequest.data.caseIds = uniqueCaseIds;
          this.covidService.getCasesByIds(casesUrl,casesRequest).subscribe(allData=>{
            for (let i = 0; i < allData['data'].length; i++) {
              for (let j = 0; j < getByIdResp['data']['jobOrderDetails'].length; j++) {
                if (getByIdResp['data']['jobOrderDetails'][j]['caseId'] == allData['data'][i]['caseId']) {
                  allData['data'][i]['jobOrderId']    = getByIdResp['data']['jobOrderDetails'][j]['jobOrderId']
                  allData['data'][i]['testId']        = getByIdResp['data']['jobOrderDetails'][j]['testId']
                  allData['data'][i]['testStatusId']  = getByIdResp['data']['jobOrderDetails'][j]['testStatusId']
                  allData['data'][i]['wellMachineId'] = getByIdResp['data']['jobOrderDetails'][j]['wellMachineId']
                  allData['data'][i]['sellectedWell'] = getByIdResp['data']['jobOrderDetails'][j]['wellMachineId']
                  allData['data'][i]['batchId']       = getByIdResp['data']['batchDetails']['batchId']
                  // allData['data'][i]['collectionDate']  =  '2-11-2022 20:07:25';
                  // allData['data'][i]['receivingDate']  =  '02-11-2020';
                  // allData['data'][i]['collectedBy']  =  1;
                }

              }

            }
            console.log("allData",allData);
            const uniquePatientIds = [];
            const map = new Map();
            for (const item of allData['data']) {
              if(!map.has(item.patientId)){
                map.set(item.patientId, true);    // set any value to Map
                uniquePatientIds.push(item.patientId);
              }
            }
            var patientRequest = {...this.patientRecordRequest};
            patientRequest.data.patientIds = uniquePatientIds;
            patientRequest.header.partnerCode  = this.logedInUserRoles.partnerCode;
            patientRequest.header.userCode     = this.logedInUserRoles.userCode;
            patientRequest.header.functionalityCode     = "TWMA-CNB";
            var patientUrl = environment.API_PATIENT_ENDPOINT + "patientbyids";
            var patient = this.covidService.getPatientRecord(patientUrl,patientRequest).then(getPatientResp =>{
              return getPatientResp;
              // console.log("getPatientResp",getPatientResp);
            }, error=>{
              this.notifier.notify("error","Error while getting patient name.");
              this.ngxLoader.stop();
              resolve();
            })

            const uniqueClientIds = [];
            const map2 = new Map();
            for (const item of allData['data']) {
              if(!map2.has(item.clientId)){
                map2.set(item.clientId, true);    // set any value to Map
                uniqueClientIds.push(item.clientId);
              }
            }

            var clientRequest = {...this.clientRecordRequest};
            clientRequest.data.clientIds = uniqueClientIds;
            clientRequest.header.partnerCode  = this.logedInUserRoles.partnerCode;
            clientRequest.header.userCode     = this.logedInUserRoles.userCode;
            clientRequest.header.functionalityCode     = "TWMA-CNB";
            var clientUrl = environment.API_CLIENT_ENDPOINT + "clientidsnames";
            var client = this.covidService.getClientRecord(clientUrl,clientRequest).then(getClientResp =>{
              return getClientResp;

            }, error=>{
              this.notifier.notify("error","Error while getting client name.");
              this.ngxLoader.stop();
            })
            forkJoin([patient, client]).subscribe(allresults => {
              console.log("allresults",allresults);
              var patient = allresults[0];
              var client = allresults[1];
              for (let l = 0; l < allData['data'].length; l++) {
                //////////patient
                if (patient['data']['length'] > 0) {
                  for (let k = 0; k < patient['data'].length; k++) {
                    if (allData['data'][l]['patientId'] == patient['data'][k]['patientId']) {
                      console.log("patient['data'][k].firstName",patient['data'][k].firstName);
                      console.log("patient['data'][k].firstName",this.encryptDecrypt.decryptString(patient['data'][k].firstName));
                      console.log("patient['data'][k].lastName",patient['data'][k].lastName);
                      console.log("patient['data'][k].lastName",this.encryptDecrypt.decryptString(patient['data'][k].lastName));

                      allData['data'][l]['pFName']      =  this.encryptDecrypt.decryptString(patient['data'][k].firstName);
                      allData['data'][l]['pLName']      =  this.encryptDecrypt.decryptString(patient['data'][k].lastName);
                      allData['data'][l]['pDOB']        =  patient['data'][k].dateOfBirth;
                      // allData['data'][l]['clientName']  =  patient['data'][k].firstName;

                    }

                  }
                }
                if (client['data']['length']) {
                  for (let m = 0; m < client['data'].length; m++) {
                    if (allData['data'][l]['clientId'] == client['data'][m]['clientId']) {
                      allData['data'][l]['clientName']  =  client['data'][m].clientName;
                    }

                  }
                }


                /////////////client
                for (let n = 0; n < this.allWelNo.length; n++) {
                  if (allData['data'][l]['sellectedWell'] == this.allWelNo[n].wellMachineId) {
                    allData['data'][l]['sellectedWellForSorting'] = this.allWelNo[n].wellId;

                  }
                }


              }
              console.log("allData",allData['data']);
              // this.specimenTableData = allData['data'];
              this.specimenTableData = this.sortPipe.transform(allData['data'], "asc", "sellectedWellForSorting");




              // this.populateWells();
              // allData['pFName'] = allresults[0]['data'][0].firstName;
              // allData['pLName'] = allresults[0]['data'][0].lastName;
              // allData['pDOB']    = allresults[0]['data'][0].dateOfBirth;
              // allData['clientName']    = allresults[1]['data'][0].name;
              //
              // allData['sellectedWell'] = null;
              //
              //
              // console.log("allData",allData);
              //
              // this.specimenTableData.push(allData);
              // localStorage.setItem("createCovidBatchData",JSON.stringify(this.specimenTableData));
              //
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                // Destroy the table first
                dtInstance.destroy();
                // Call the dtTrigger to rerender again
                this.dtTrigger.next();
              });
              this.populateWells();
              // this.ngxLoader.stop();
              resolve();

            })


          },error=>{
            this.notifier.notify("error","Error while loading cases");
            this.ngxLoader.stop();
            resolve()
          })


        })


      },error=>{
        this.notifier.notify("error","Error while fetching batch")
        this.ngxLoader.stop();
        resolve();
      })

    })
  }

  populateWells(){
    console.log('this.specimenTableData',this.specimenTableData);
    // console.log('this.specimenTableData',this.specimenTableData.length);
    if (this.specimenTableData.length != 0) {
      // console.log('----',this.allWelNo);
      if (this.allWelNo.length == 0) {
        this.notifier.notify('warning','Well lookup not loaded');
        this.ngxLoader.stop();
        return;
      }


      // this.ngxLoader.stop();
      var size = 0;
      var uniqueWellArrayHolder = {};
      var uniqueWellArray = [];
      for (let i = 0; i < this.allWelNo.length; i++) {
        size = size +1;
        for (let j = 0; j < this.specimenTableData.length; j++) {
          if (this.specimenTableData[j]['sellectedWell'] == this.allWelNo[i].wellMachineId) {
            this.specimenTableData[j]['sellectedWellName'] = this.allWelNo[i].wellNumber
            this.specimenTableData[j]['sellectedWellOrderId'] = this.allWelNo[i].wellId
            // console.log("this.allWelNo[i].wellMachineId",this.allWelNo[i].wellMachineId);

            uniqueWellArrayHolder['well']=(this.specimenTableData[j]['sellectedWell']);
            uniqueWellArrayHolder['caseId']=(this.specimenTableData[j]['caseId']);
            uniqueWellArray.push(uniqueWellArrayHolder);
            uniqueWellArrayHolder ={};
            // var c  = document.getElementsByClassName('WELROW-'+this.specimenTableData[j]['caseId']);
            // var well = this.specimenTableData[j]['sellectedWell'];


            // console.log("c",c);
            // setTimeout(function(){
            //   console.log("c***",c);
            //   c[0]['value'] = well;
            //   console.log("c.val",c[0]['value']);
            // },3000);


            this.allWelNo[i].selected = true;
            this.allWelNo[i].toolTip = "(allready selected)";
          }

        }
        // console.log("size",size);
        // console.log("this.allWelNo",this.allWelNo);

        if (size == this.allWelNo.length) {
          // console.log("uniqueWellArray",uniqueWellArray);
          setTimeout(()=>this.stopUiLoader(uniqueWellArray),3000);
          // this.ngxLoader.stop();

        }


      }
      // console.log("this.allWelNo",this.allWelNo);

      // this.specimenTableData = oldData;
    }
    else{
      this.ngxLoader.stop();
    }
  }

  get f() { return this.batchForm.controls; }
  saveBatch(){
    this.ngxLoader.start();
    // $('#createModal').modal('hide');

    this.submitted  = true;
    if (this.batchForm.invalid) {
      this.ngxLoader.stop();
      // alert('form invalid');
      this.scrollToError();
      return;
    }
    else{
      this.populateSaveBatchData();
    }
    // this.ngxLoader.stop();




  }
  trimBatchID(){
    this.batchId = this.batchId.replace(/[^a-zA-Z0-9]/g, '');
    this.batchId = this.batchId.trim();
  }
  checkBatchId(e){
    // console.log('this.batchId',this.batchId);


    // console.log("batchvalue", e.target.value);
    if(e.target.value == "" || e.target.value == null){
      this.disableBtnCheck = true
      // this.submitted = false;
      this.alreadyExistError = false;
    }
    else{
      if(this.specimenTableData.length == 0) {
        // console.log("batchvalue", this.specimenTableData.length);
        this.disableBtnCheck = true
      }
      else{
        this.disableBtnCheck = false
      }
    }

  }
  addCase(e){
    if (e.ctrlKey == true) {
      // console.log('CTRL');
      return;
    }
    else{
      // this.timeCount = 1
      var regexp = new RegExp(/([a-zA-Z]{2}[0-9]{2}-[0-9]{1})/);
      var reg = regexp.test(this.caseNumber);

      if (reg != false) {
        if(this.timeCount) {
          clearTimeout(this.timeCount);
          // console.log("here",this.timeCount);

          this.timeCount = null;
        }

        // this.timeCount = setTimeout(this.addCaseInternal(e), 5000)
        this.timeCount = setTimeout(()=>{

          console.log("this.sendOnce",this.sendOnce);

          if (this.sendOnce == false) {
            this.addCaseInternal(e);
          }
            // this.addCaseInternal(e);

        }, 400)

      }
    }
  }

  // timeOutCheck(e){
  //   console.log("new function",this.timeCount);
  //
  //   if (this.timeCount == 0) {
  //     this.addCaseInternal(e);
  //   }
  //   else{
  //     return;
  //   }
  // }

  addCaseInternal(e){
    console.log("here", this.caseNumber);
    this.sendOnce = true;
    // console.log("e", e);
    // e.preventDefault()
    // $('#casenumberInput').unbind('keyup');
    // return;


    if (typeof this.caseNumber == "undefined") {
      this.ngxLoader.stop();
      this.batchForm.reset();
      this.sendOnce = false;
      return;
    }
    if (this.caseNumber == null) {
      this.ngxLoader.stop();
      this.batchForm.reset();
      this.sendOnce = false;
      return;
    }
    else{
      console.log("casnumber",this.caseNumber);
      const found = this.specimenTableData.some(el => el.caseNumber === this.caseNumber.toUpperCase());
      // console.log("found",found);
      // console.log("this.caseNumber",this.caseNumber);
      // console.log("this.specimenTableData",this.specimenTableData);

      if (found) {
        this.notifier.notify('info','This Case already exists in the selected batch');
        this.batchForm.reset();
        this.ngxLoader.stop();
        this.sendOnce = false;
        return;
      }
      if (this.specimenTableData.length >= 94) {
        this.notifier.notify('info','Batch Full you cannot add more then 94 cases in one batch');
        this.batchForm.reset();
        this.ngxLoader.stop();
        this.sendOnce = false;
        return;
      }
      // var regexp = new RegExp(/([a-zA-Z]{2}[0-9]{2}-[0-9]{1})/);
      // var reg = regexp.test(this.caseNumber);
      //
      // if (reg == false) {
      //   // this.batchForm.reset();
      //   this.ngxLoader.stop();
      //   return;
      // }
      // else{
        this.ngxLoader.start();

        var CreateRequest = {...this.createBatchRequest};
        CreateRequest.data.caseNumber = this.caseNumber.toUpperCase();
        // console.log("batchId", CreateRequest);
        console.log("CreateRequest", CreateRequest);
        // this.ngxLoader.stop();  return;
        // this.covidService.createBatch;
        var allData =[];
        var batchUrl = environment.API_SPECIMEN_ENDPOINT + "searchbycovidcasenumber";
        CreateRequest.header.partnerCode  = this.logedInUserRoles.partnerCode;
        CreateRequest.header.userCode     = this.logedInUserRoles.userCode;
        CreateRequest.header.functionalityCode     = "SPCA-MC";
        this.covidService.createBatch(batchUrl,CreateRequest).subscribe(getResp =>{
          if (getResp['data'] == null) {
            this.timeCount = 0;
            this.sendOnce = false;
            this.notifier.notify("warning","No Case Found");
            this.batchForm.reset();
            this.ngxLoader.stop();
          }
          else{
            var hold = [];
            var checkTestURL = environment.API_COVID_ENDPOINT + 'checktest';
            var checkTestData = {...this.checktestRequest}
            checkTestData.data.caseIds = [getResp['data'].caseId];
            checkTestData.header.partnerCode  = this.logedInUserRoles.partnerCode;
            checkTestData.header.userCode     = this.logedInUserRoles.userCode;
            checkTestData.header.functionalityCode     = "TWMA-CNB";

            hold.push(getResp['data'])
            // this.sendOnce = false;
            this.checkTestAndLoadCase(hold,checkTestURL,checkTestData,'batch')
            // return;

          }
          // this.batchForm.reset();


        }, error=>{
          this.notifier.notify("error","Error while loading case.");
          this.batchForm.reset();
          this.ngxLoader.stop();
        })
      // }


    }

  }
  scrollTo(el: Element): void {
    if (el) {
      el.scrollIntoView({ behavior: 'smooth', block: 'center' });
    }
  }
  scrollToError(): void {
    const firstElementWithError = document.querySelector('.ng-invalid[formControlName]');
    console.log("aa:", firstElementWithError);

    this.scrollTo(firstElementWithError);
  }

  wellNoFun(event,index){
    // var oldData = JSON.parse(localStorage.getItem('createCovidBatchData'));
    // console.log('oldData',oldData);
    // console.log("Check Well No: ", event);

    for (let i = 0; i < this.allWelNo.length; i++) {
      if (event == this.allWelNo[i].wellMachineId) {
        if (this.specimenTableData[index]['sellectedWell'] == null) {
          // console.log("this.allWelNo[i].wellMachineId",this.allWelNo[i].wellMachineId);

          this.allWelNo[i].selected = true;
          this.specimenTableData[index]['sellectedWell'] = this.allWelNo[i].wellMachineId;
          this.allWelNo[i].toolTip = "(allready selected)";
          ///////////////// for localstorage
          // oldData[index]['sellectedWell'] = this.allWelNo[i].wellMachineId;
          localStorage.setItem("createCovidBatchData",JSON.stringify(this.specimenTableData));
          this.diasbleBack = true;

        }
        else{
          this.allWelNo.some(el =>{
            if (el.wellMachineId === this.specimenTableData[index]['sellectedWell']) {
              el.selected = false;
              el.toolTip = "";
              console.log("el",el);

            }
          });

          this.allWelNo[i].selected = true;
          this.allWelNo[i].toolTip = "(allready selected)";
          this.specimenTableData[index]['sellectedWell'] = this.allWelNo[i].wellMachineId;
          // console.log("this.allWelNo[i].wellMachineId",this.allWelNo[i].wellMachineId);


          ///////////////// for localstorage
          // oldData[index]['sellectedWell'] = this.allWelNo[i].wellMachineId;
          localStorage.setItem("createCovidBatchData",JSON.stringify(this.specimenTableData));
          this.diasbleBack = true;

        }

      }

    }


  }

  populateSaveBatchData(){
    // localStorage.removeItem('createCovidBatchData');
    // this.diasbleBack = false;
    // localStorage.removeItem('createCovidBatchData');
    console.log("this.specimenTableData",this.specimenTableData);
    console.log('this.allWelNo',this.allWelNo);



    var requestData = {...this.saveBtachRequest}
    requestData.data.batchId   = this.batchId.trim();
    requestData.data.createdBy = this.logedInUserRoles.userCode;
    var myDate = new Date();
    var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');
    requestData.data.createdTimestamp = currentDate;

    var uniqueCaseIDs = [];

    // this.ngxLoader.stop(); return;
    var tempHolder = {};
    // console.log('this.allWelNo',this.allWelNo);
    // return;

    for (let i = 0; i < this.specimenTableData.length; i++) {

      // console.log("this.specimenTableData[i]['sellectedWell']",this.specimenTableData[i]['sellectedWell']);

      if (this.specimenTableData[i]['sellectedWell'] == null) {
        var myObj = this.allWelNo.find(obj => obj.selected == false,);
        console.log("myObj",myObj);

        for (let j = 0; j < this.allWelNo.length; j++) {
          // console.log("this.allWelNo[i].selected",this.allWelNo[j].selected);

          if (this.allWelNo[j].selected == false) {
            console.log("this.allWelNo[j]['wellMachineId']",this.allWelNo[j]['wellMachineId']);

            // console.log("this.allWelNo[j]",this.allWelNo[j]);

              // this.specimenTableData[i]['sellectedWell'] = myObj['wellMachineId'];
              tempHolder['wellMachineId'] = this.allWelNo[j]['wellMachineId'];
              this.allWelNo[j].selected = true;
              break;
          }

        }
        // var myObj = this.allWelNo.find(obj => obj.selected == false,);
        //   // this.specimenTableData[i]['sellectedWell'] = myObj['wellMachineId'];
        //   tempHolder['wellMachineId'] = myObj['wellMachineId'];
      }
      else{

        tempHolder['wellMachineId'] = this.specimenTableData[i]['sellectedWell'];
      }
      tempHolder['jobOrderId']    =  this.specimenTableData[i]['jobOrderId'];

      requestData.data.listOfBatchJobOrderIds.push(tempHolder)
      tempHolder = {}

      requestData.data.jobOrderId.push(this.specimenTableData[i]['jobOrderId']);
      // requestData.data.testIds.push(this.specimenTableData[i]['testId']);

      uniqueCaseIDs.push(this.specimenTableData[i]['caseId'])

    }
    console.log("requestData",requestData);
    console.log("uniqueCaseIDs",uniqueCaseIDs);

  // console.log("specimenurl",specimenurl);
  // console.log("specimenreqBody",specimenreqBody);


    // this.ngxLoader.stop(); return;
    requestData.header.partnerCode  = this.logedInUserRoles.partnerCode;
    requestData.header.userCode     = this.logedInUserRoles.userCode;
    requestData.header.functionalityCode     = "TWMA-CNB";
    var saveBatchUrl = environment.API_COVID_ENDPOINT +'createbatch';
    this.covidService.createBatch(saveBatchUrl,requestData).subscribe(createResp=>{
      console.log('createResp',createResp);
      if (createResp['result'].description == 'This batch Id Already Exists') {
        this.notifier.notify("error","Batch ID "+this.batchId+" already exists, Please specify a valid Batch ID");
        this.alreadyExistError = true;
        this.ngxLoader.stop();
      }
      if (createResp['result'].codeType == 'S') {
        var specimenurl = environment.API_SPECIMEN_ENDPOINT + 'updatecasestatusin';

        var specimenreqBody={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "TWMA-CNB",systemHostAddress:"",remoteUserAddress:"",dateTime:""},
        data: {
          caseStatusId:4,
          caseIds:uniqueCaseIDs,
          updatedBy: this.logedInUserRoles.userCode,
          updatedTimestamp:currentDate

        }
      }
      this.covidService.updateCaseStatus(specimenurl,specimenreqBody).subscribe(specimenReps=>{
        if (specimenReps['data']>0) {
          localStorage.removeItem('createCovidBatchData');
          this.diasbleBack = false;

          // setTimeout(function(){
            this.ngxLoader.stop();
            this.alreadyExistError = false;
            this.notifier.notify("success","Batch created successfully");
            localStorage.removeItem('createCovidBatchData');
            this.router.navigate(['manage-batches']);
          // },2000);

        }
        else {
          this.notifier.notify("warning","Batch is created successfully but there was an error while updating case statuses");
          this.ngxLoader.stop();
          this.alreadyExistError = false;
        }
      },error=>{
        this.notifier.notify("warning","Batch is created successfully but there was an error while updating case statuses");
        this.ngxLoader.stop();
        this.alreadyExistError = false;
      })


      }
      else if(createResp['result'].description != 'This batch Id Already Exists'){
        this.notifier.notify("error","Error while creating batch");
        this.ngxLoader.stop();
        this.alreadyExistError = false;
      }

    })
    this.ngxLoader.stop(); return;
  }



  getLookups(){
    // this.ngxLoader.start();
    return new Promise((resolve, reject) => {
      var wellNo = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=BATCHING_WELL_MACHINE_CACHE&partnerCode=sip').then(response =>{
        return response;
      })


      forkJoin([wellNo]).subscribe(allLookups => {
        console.log("allLookups",allLookups);
        this.constantWelNo = this.sortPipe.transform(allLookups[0], "asc", "wellId");

        this.allWelNo      = [...this.constantWelNo];
        console.log("this.constantWelNo",this.allWelNo);

        for (let i = 0; i < this.allWelNo.length; i++) {
          if (this.allWelNo[i].wellNumber == "H12" || this.allWelNo[i].wellNumber == "G12") {
            this.allWelNo[i].selected = true;
            this.allWelNo[i].toolTip = "";
          }
          else{
            this.allWelNo[i].selected = false;
            this.allWelNo[i].toolTip = "";
          }

        }
        if (this.pageType == 'add') {
          var oldData = JSON.parse(localStorage.getItem('createCovidBatchData'));
          if (oldData != null) {
            this.specimenTableData = oldData;
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              // Destroy the table first
              dtInstance.destroy();
              // Call the dtTrigger to rerender again
              this.dtTrigger.next();
            });

          }
        }
        resolve();

      })
    })

  }

  showRemoveModal(item, index){
    console.log("this.specimenTableData",this.specimenTableData);

    if (this.specimenTableData['length'] == 1) {
      if (this.pageType == 'edit') {
        this.notifier.notify('warning',"There must be at least one case in a batch.")
        return;
      }
      else{
        this.removeCaseData  = item;
        this.removeCaseIndex = index;
        $('#cancelModal').modal('show');
      }

    }
    else{
      this.removeCaseData  = item;
      this.removeCaseIndex = index;
      $('#cancelModal').modal('show');
      // return;
    }

  }

  removeCase(){
    var myDate = new Date();
    var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');

    // console.log("this.removeCaseData['batchId']",typeof this.removeCaseData['batchId']);


    if (typeof this.removeCaseData['batchId'] != 'undefined') {
      // console.log('-----------');

      // this.specimenTableData.splice(this.removeCaseIndex,1);
      // for (let i = 0; i < this.allWelNo.length; i++) {
      //   if (this.allWelNo[i].wellMachineId == this.removeCaseData['sellectedWell']) {
      //     this.allWelNo[i].selected = false;
      //   }
      //
      // }
      //
      // this.removeCaseData  = {};
      // this.removeCaseIndex = '';
      // localStorage.setItem("createCovidBatchData",JSON.stringify(this.specimenTableData));
      // this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      //   // Destroy the table first
      //   dtInstance.destroy();
      //   // Call the dtTrigger to rerender again
      //   this.dtTrigger.next();
      // });
      //////////////////Case that is saved with batch
      var removeUrl     = environment.API_COVID_ENDPOINT+'removecase';
      var removeRequest = {...this.removeRequest};
      removeRequest.data.batchId = this.removeCaseData['batchId'];
      if (this.removeCaseData['testStatusId'] == '6') {
        removeRequest.data.testStatusId = this.removeCaseData['testStatusId'];
      }
      else{
        removeRequest.data.testStatusId = "1";
      }
      // removeRequest.data.testStatusId = "1";
      // removeRequest.data.testStatusId = this.removeCaseData['testStatusId'];
      removeRequest.data.updatedBy = this.logedInUserRoles.userCode;
      removeRequest.data.updatedTimestamp = "30-11-2020 20:07:25";
      removeRequest.data.jobOrderId = [this.removeCaseData['jobOrderId']];
      removeRequest.header.partnerCode  = this.logedInUserRoles.partnerCode;
      removeRequest.header.userCode     = this.logedInUserRoles.userCode;
      removeRequest.header.functionalityCode     = "TWMA-CNB";
      this.covidService.removeCase(removeUrl,removeRequest).subscribe(removeResp =>{
        console.log("removeResp",removeResp);
        if (removeResp['result']['codeType'] == 'S') {
          if (this.removeCaseData['testStatusId'] == 6) {
            $('#removeModal').modal('hide');
            this.notifier.notify("success","Case removed successfully");
            this.ngxLoader.stop();
            this.specimenTableData.splice(this.removeCaseIndex,1);
            for (let i = 0; i < this.allWelNo.length; i++) {
              if (this.allWelNo[i].wellMachineId == this.removeCaseData['sellectedWell']) {
                this.allWelNo[i].selected = false;
              }

            }

            this.removeCaseData  = {};
            this.removeCaseIndex = '';
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              // Destroy the table first
              dtInstance.destroy();
              // Call the dtTrigger to rerender again
              this.dtTrigger.next();
            });
            if (this.specimenTableData['length'] == 0) {
              this.disableBtnCheck = true;
            }
            // localStorage.setItem("createCovidBatchData",JSON.stringify(this.specimenTableData));
            // this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            //   dtInstance.ajax.reload();
            // });
          }
          else{
            var specimenurl = environment.API_SPECIMEN_ENDPOINT + 'updatecasestatusin';

            var specimenreqBody={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "TWMA-CNB",systemHostAddress:"",remoteUserAddress:"",dateTime:""},
            data: {
              caseStatusId:3,
              caseIds:[this.removeCaseData['caseId']],
              updatedBy: this.logedInUserRoles.userCode,
              updatedTimestamp:currentDate

            }
          }
          this.covidService.updateCaseStatus(specimenurl,specimenreqBody).subscribe(specimenReps=>{
            if (specimenReps['data']>0) {
              this.ngxLoader.stop();
              this.notifier.notify("success","Case removed successfully");
              // localStorage.removeItem('createCovidBatchData');
              // this.router.navigate(['manage-batches']);
              this.specimenTableData.splice(this.removeCaseIndex,1);
              for (let i = 0; i < this.allWelNo.length; i++) {
                if (this.allWelNo[i].wellMachineId == this.removeCaseData['sellectedWell']) {
                  this.allWelNo[i].selected = false;
                }

              }

              this.removeCaseData  = {};
              this.removeCaseIndex = '';
              // localStorage.setItem("createCovidBatchData",JSON.stringify(this.specimenTableData));
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                // Destroy the table first
                dtInstance.destroy();
                // Call the dtTrigger to rerender again
                this.dtTrigger.next();
              });
              if (this.specimenTableData['length'] == 0) {
                this.disableBtnCheck = true;
              }
            }
            else{
              this.notifier.notify("error","Error while updating case status");
              this.ngxLoader.stop();
            }
          },error=>{
            this.notifier.notify("error","Error while updating case status");
            this.ngxLoader.stop();
          })
          }




          // this.notifier.notify('success',"Case removed successfully");



        }
        else{
          this.notifier.notify('error',"Error while removing case")
        }
      },error=>{
        this.notifier.notify('error',"Error while removing case")
      })
    }
    else{
      //////////////////newly added case from edit or add screen
      // console.log('this.removeCaseData',this.removeCaseData);
      // console.log('this.removeCaseIndex',this.removeCaseIndex);
      // console.log('this.specimenTableData',this.specimenTableData);

      this.specimenTableData.splice(this.removeCaseIndex,1);
      for (let i = 0; i < this.allWelNo.length; i++) {
        if (this.allWelNo[i].wellMachineId == this.removeCaseData['sellectedWell']) {
          this.allWelNo[i].selected = false;
        }

      }

      this.removeCaseData  = {};
      this.removeCaseIndex = '';
      console.log('this.specimenTableData after',this.specimenTableData);
      if (this.specimenTableData['length'] == 0) {
        this.disableBtnCheck = true;
      }

      // if (this.pageType == 'add') {
      //   localStorage.setItem("createCovidBatchData",JSON.stringify(this.specimenTableData));
      // }
      // else{
      //   var oldData = JSON.parse(localStorage.getItem('createCovidBatchData'));
      //   console.log("oldData",oldData);
      //
      //   if (oldData == null) {
      //
      //   }
      // }
      // localStorage.setItem("createCovidBatchData",JSON.stringify(this.specimenTableData));
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        // Destroy the table first
        dtInstance.destroy();
        // Call the dtTrigger to rerender again
        this.dtTrigger.next();
      });
    }

    // var removeUrl     = environment.API_COVID_ENDPOINT+'removecase';
    // var removeRequest = {...this.removeRequest};

    $('#cancelModal').modal('hide');

  }

  updatePatient(e,field,patientId,index, oldValue){

    // console.log('value',value);
    // console.log('field',field);
    // console.log('patientId',patientId);
    // console.log('index',index);
    // console.log('this.specimenTableData',this.specimenTableData);
    // this.ngxLoader.stop()
    // return;
    var myDate = new Date();
    var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');
    var updatePatientRequest = {
      header:{
        uuid:"",
        partnerCode:"",
        userCode:"",
        referenceNumber:"",
        systemCode:"",
        moduleCode:"PNTM",
        functionalityCode: "TWMA-CNB",
        systemHostAddress:"",
        remoteUserAddress:"",
        dateTime:""
      },
      data:{
        patientId: null,
        patientDemographics:{

        },
        userName:""
      }
    }
    // updatePatientRequest = {...this.updatePatientRequest}
    // var updatePatientRequest = () => this.updatePatientRequest;
    updatePatientRequest['data'].patientId = patientId;
    updatePatientRequest['data'].patientDemographics['updatedtedBy'] =  this.logedInUserRoles.userCode;
    updatePatientRequest['data'].userName =  this.logedInUserRoles.userName;
    updatePatientRequest['data'].patientDemographics['updatedtedTimestamp'] =  currentDate;
    updatePatientRequest['data'].patientDemographics['contacts']            =  [];
    updatePatientRequest['header'].partnerCode  = this.logedInUserRoles.partnerCode;
    updatePatientRequest['header'].userCode     = this.logedInUserRoles.userCode;
    updatePatientRequest['header'].functionalityCode     = "TWMA-CNB";
    if (field == 'last') {
      // value = value.trim();
      var regexp = new RegExp(/^((?![\s])[^\$%\^&*()_\-=\+@#!~`{}\[\]|\\\/:;\\'\?\.,<>]+)$/);
      var reg = regexp.test(e.target.value);
      if (reg) {
        updatePatientRequest['data'].patientDemographics['lastName'] =  this.encryptDecrypt.encryptString(e.target.value.trim());
      }
      else{
        e.target.value = oldValue;
        // updatePatientRequest = {};
        alert('special characters not allowed'); return;
      }


    }
    else if (field == 'first') {
      var regexp = new RegExp(/^((?![\s])[^\$%\^&*()_\-=\+@#!~`{}\[\]|\\\/:;\\'\?\.,<>]+)$/);
      var reg = regexp.test(e.target.value);
      if (reg) {
        updatePatientRequest['data'].patientDemographics['firstName'] =  this.encryptDecrypt.encryptString(e.target.value.trim());
      }
      else{
        e.target.value = oldValue;
        // updatePatientRequest = {};
        alert('special characters not allowed'); return;
      }
    }
    else if (field == 'dob') {
      var a= e.target.value.split('/')
      // console.log(parseInt(a[2]));
      // return
      var d = new Date();
      var m =  d.getMonth()+1;
      var dy = d.getDate();
      var yy = d.getFullYear();
      var month;

      var today;
      console.log("dy",dy);
      if (m<10) {
        month = "0"+m
      }
      else{
        month = m;
      }
      if (dy<10) {
        today = "0"+dy
      }
      else{
        today = dy;
      }
      // return

      // var b= a.length-1;
      // var c = a[b]
      // var d = parseInt(c);
      var regexp = new RegExp(/((0[1-9]|1[0-2])\/((0|1)[0-9]|2[0-9]|3[0-1])\/((19|20)\d\d))$/);
      var reg = regexp.test(e.target.value);
      if (reg == true) {
        var date = a[0]+'-'+a[1]+'-'+a[2];
        updatePatientRequest['data'].patientDemographics['dateOfBirth'] =  date;

      }
      else{

        e.target.value = this.datePipe.transform(oldValue, 'MM/dd/yyyy');
        // updatePatientRequest = {};
        // this.specimenTableData[index]['pDOB'] = this.specimenTableData[index]['pDOB'];
        alert('invalid Date Format please enter date in mm/dd/yyyy format')
        return
      }
      if (parseInt(a[2]) > yy) {
        e.target.value = this.datePipe.transform(oldValue, 'MM/dd/yyyy');
        // updatePatientRequest = {};
        // this.specimenTableData[index]['pDOB'] = this.specimenTableData[index]['pDOB'];
        alert('year cannot be greater than current year')
        return
      }
      if (month == a[0] && dy < a[1] && a[2] == yy) {
        e.target.value = this.datePipe.transform(oldValue, 'MM/dd/yyyy');
        // updatePatientRequest = {};
        // this.specimenTableData[index]['pDOB'] = this.specimenTableData[index]['pDOB'];
        alert('date of birth cannot be greater the current date')
        return
      }
      else if (month <= a[0] && dy <= a[1] && a[2] > yy) {
        e.target.value = this.datePipe.transform(oldValue, 'MM/dd/yyyy');
        // updatePatientRequest = {};
        // this.specimenTableData[index]['pDOB'] = this.specimenTableData[index]['pDOB'];
        alert('date of birth cannot be greater the current date')
        return
      }

    }
    console.log("updatePatientRequest",updatePatientRequest);
    // updatePatientRequest = {};
    // return;
    var updatePatientUrl = environment.API_PATIENT_ENDPOINT+'update';
    this.covidService.updatePatient(updatePatientUrl,updatePatientRequest).subscribe(updatePatientResponse=>{
      console.log('updatePatientRequest',updatePatientResponse);
      // updatePatientRequest = {};
      if (updatePatientResponse['result'].codeType == 'S') {
        this.notifier.notify('success',"Patient updated successfully")
        // this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        //   // Destroy the table first
        //   dtInstance.destroy();
        //   // Call the dtTrigger to rerender again
        //   this.dtTrigger.next();
        // });
      }

    },error=>{
      if (field == 'dob') {
        e.target.value = this.datePipe.transform(oldValue, 'MM/dd/yyyy');
      }
      else{
        e.target.value = oldValue;
      }

      // updatePatientRequest = {};
      this.notifier.notify('error',"Error while updating patient")
      // this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      //   // Destroy the table first
      //   dtInstance.destroy();
      //   // Call the dtTrigger to rerender again
      //   this.dtTrigger.next();
      // });
    })


  }

  updateClient(e, clientID, oldName){
    var regexp = new RegExp(/^((?![\s])[^\$%\^&*()_\-=\+@#!~`{}\[\]|\\\/:;\\'\?\.,<>]+)$/);
    var reg = regexp.test(e.target.value);

    var date = new Date();
    var month =  date.getMonth();
    // console.log("month",month);
    // return;


    if (reg == false) {
      e.target.value = oldName;

      alert('invalid name try to avoid special characters')

      return
    }
    else{
      var searchBatchesUrl   = environment.API_CLIENT_ENDPOINT+ 'updateclientname';
      var updateClientRequest = {...this.updateClientRequest}
      updateClientRequest.data.clientId  = clientID;
      updateClientRequest.data.name      = e.target.value.trim();
      updateClientRequest.data.updatedBy = this.logedInUserRoles.userCode;
      updateClientRequest.header.partnerCode  = this.logedInUserRoles.partnerCode;
      updateClientRequest.header.userCode     = this.logedInUserRoles.userCode;
      updateClientRequest.header.functionalityCode     = "TWMA-CNB";
      this.covidService.updateClientName(searchBatchesUrl,updateClientRequest).subscribe(updateClientResp=>{
        console.log("updateClientResp",updateClientResp);
        if (updateClientResp['data'] == 1) {
          this.notifier.notify('success',"Client updated successfully")
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            // Destroy the table first
            dtInstance.destroy();
            // Call the dtTrigger to rerender again
            this.dtTrigger.next();
          });
        }
        else{
          e.target.value = oldName;
        }

      },error=>{
        this.notifier.notify('error',"Error while updating client")
        e.target.value = oldName;
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
      })
    }


  }

  compare_dates(date1,date2){
    // console.log('date1',date1);
    // console.log('date2',date2);

     if (date1>date2) return 1;///////("collectionDate > recievingDate");
   else if (date1<date2) return 2;/////////("recievingDate > collectionDate");
   else return 3;/////////////("collectionDate = recievingDate");
  }
  updateSpecimen(collectedBy,collectionDate, recievingDate, caseId, type, oldValue, original, clientId,index){
    var updateSpecimenRequest = {header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"SPCM",
      functionalityCode: "TWMA-CNB",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      caseId:"",
      collectedBy:null,
      collectionDate:"",
      receivingDate:"",
      updatedBy:"",
      clientId : null

    }}
    var regexp = new RegExp(/((0[1-9]|1[0-2])\/((0|1)[0-9]|2[0-9]|3[0-1])\/((19|20)\d\d)) (\d{1,2}):(\d{2})\s?(?:AM|PM)$/);
    // var regexp = new RegExp(/((0[1-9]|1[0-2])\/((0|1)[0-9]|2[0-9]|3[0-1])\/((19|20)\d\d)) (\d{2}):(\d{2})$/);
    if (type == 'col') {

      var coll= collectionDate.split('/');
      // console.log("coll",coll);
      ///// return;
      var regCollection = regexp.test(collectionDate);
      if (regCollection == true) {
        var recievingToUse   = this.datePipe.transform(recievingDate,'MM/dd/yyyy h:mm')
        var collectionToUse = this.datePipe.transform(collectionDate,'MM/dd/yyyy h:mm')
        // console.log('recievingToUse',recievingToUse);
        // console.log('recievingToUse',recievingToUse);
        // console.log('collectionToUse',collectionToUse);

        var recievingTo   = moment(recievingDate,'MM/DD/YYYY HH:mm:ss')
        var collectionTo = moment(collectionDate,'MM/DD/YYYY HH:mm:ss')
        // console.log('collectionTo',collectionTo);

        // console.log("recievingDate",recievingDate);
        // console.log("collectionDate",collectionDate);


        // console.log(this.compare_dates(new Date(collectionToUse), new Date(recievingToUse)));

        // console.log(this.compare_dates(collectionToUse, recievingToUse));

        const difference = moment.duration(collectionTo.diff(recievingTo));
        // console.log('difference',difference);
        // console.log('difference asMinutes',difference.asMinutes());
        // console.log('difference asDays',difference.asDays());
        // console.log('oldValue.target.value',oldValue.target.value);

        if (this.compare_dates(new Date(collectionToUse), new Date(recievingToUse)) == 1) {
          oldValue.target.value = this.datePipe.transform(original, 'MM/dd/yyyy h:mm a')
          alert("collection date cannot be greater than recieving date")
          return
        }
        // if (collectionTo > recievingTo) {
        //   oldValue.target.value = this.datePipe.transform(original, 'MM/dd/yyyy h:mm a')
        //     alert("collection date cannot be greater than recieving date")
        //     return
        // }
        // return
        // var dateTOSet = coll[0]+'-'+coll[1]+'-'+coll[2];
        var dateTOSet = coll[0]+'-'+coll[1];
        var timesetHolder = coll[2].split(' ');
        // var timeset = '';
        const [time] = timesetHolder[1].split(' ');
        let [hours, minutes] = time.split(':');
        // console.log('hours',hours);
        // console.log('minutes',minutes);
        if (hours == '12') {
          hours = '00';
        }
        if (timesetHolder[2].toUpperCase() == "PM") {
          hours = parseInt(hours, 10) + 12;
          // console.log('herrrr');
        }
        // console.log("hours",hours);
        // console.log("minutes",minutes);

        dateTOSet = dateTOSet+'-'+timesetHolder[0]+' '+hours+':'+minutes+":00";
        // updateSpecimenRequest.data.collectionDate    = dateTOSet;
        // updateSpecimenRequest.data.receivingDate    = dateTOSet;
        updateSpecimenRequest.data.receivingDate   = this.datePipe.transform(recievingDate,'MM-dd-yyyy HH:mm:ss');
        updateSpecimenRequest.data.collectionDate = this.datePipe.transform(collectionDate,'MM-dd-yyyy HH:mm:ss');
        // console.log("updateSpecimenRequest.data.collectionDate",updateSpecimenRequest.data.collectionDate);
        // return;

        // updatePatientRequest.data.patientDemographics['dateOfBirth'] =  date;
      }
      else{
        oldValue.target.value = this.datePipe.transform(original, 'MM/dd/yyyy h:mm a')
        alert('invalid collection date format please enter date in mm/dd/yyyy hh:mm AM/PM format')
        return
      }
      var d = new Date();
      var m =  d.getMonth()+1;
      var dy = d.getDate();
      var yy = d.getFullYear();
      var month;

      var today;
      console.log("dy",dy);
      if (m<10) {
        month = "0"+m
      }
      else{
        month = m;
      }
      if (dy<10) {
        today = "0"+dy
      }
      else{
        today = dy;
      }
      var ytocheck = coll[2].split(' ');
      // console.log('ytocheck',ytocheck);
      // console.log('yy',yy);

      if (parseInt(ytocheck[0]) > yy) {
        // this.specimenTableData[index]['pDOB'] = this.specimenTableData[index]['pDOB'];
        alert('collection date year cannot be greater than current year')
        return
      }
      if (month == coll[0] && dy < coll[1] && ytocheck[0] == yy) {
        // this.specimenTableData[index]['pDOB'] = this.specimenTableData[index]['pDOB'];
        alert('collection date cannot be greater the current date')
        return
      }
      else if (month <= coll[0] && dy <= coll[1] && ytocheck[0] > yy) {
        // this.specimenTableData[index]['pDOB'] = this.specimenTableData[index]['pDOB'];
        alert('collection date cannot be greater the current date')
        return
      }

    }
    else if (type == 'rec') {
      var regRecieving  = regexp.test(recievingDate);
      var rec = recievingDate.split('/');
      if (regRecieving == true) {
        var recievingToUse   = this.datePipe.transform(recievingDate,'MM/dd/yyyy HH:mm:ss')
        var collectionToUse = this.datePipe.transform(collectionDate,'MM/dd/yyyy HH:mm:ss')
        // console.log('recievingToUse',recievingToUse);


        var recievingTo   = moment(recievingDate,'MM/DD/YYYY HH:mm:ss')
        var collectionTo  = moment(collectionDate,'MM/DD/YYYY HH:mm:ss')
        // console.log('recievingTo',recievingTo);
        // console.log('collectionTo',collectionTo);


        // const difference = moment(moment(recievingToUse,"'MM/dd/yyyy HH:mm:ss")).diff(moment(collectionToUse,"'MM/dd/yyyy HH:mm:ss"))
        const difference = moment.duration(collectionTo.diff(recievingTo))
        // console.log('difference',difference);
        // console.log('difference asMinutes',difference.asMinutes());
        // console.log('difference asHours',difference.asHours());
        // console.log('difference asDays',difference.asDays());
        // console.log('oldValue.target.value',oldValue.target.value);


        // if (difference.asMinutes() > 0) {
        //   oldValue.target.value = this.datePipe.transform(original, 'MM/dd/yyyy h:mm a')
        //   alert("collection date cannot be greater than recieving date")
        //   return
        // }
        // if (collectionTo > recievingTo) {
        //   oldValue.target.value = this.datePipe.transform(original, 'MM/dd/yyyy h:mm a')
        //     alert("collection date cannot be greater than recieving date")
        //     return
        // }

        if (this.compare_dates(new Date(collectionToUse), new Date(recievingToUse)) == 1) {
          oldValue.target.value = this.datePipe.transform(original, 'MM/dd/yyyy h:mm a')
          alert("collection date cannot be greater than recieving date")
          return
        }
        // return;


        // var dateTOSet = rec[0]+'-'+rec[1]+'-'+rec[2];
        var dateTOSet = rec[0]+'-'+rec[1];
        var timesetHolder = rec[2].split(' ');
        // var timeset = '';
        const [time] = timesetHolder[1].split(' ');
        let [hours, minutes] = time.split(':');
        // console.log('hours',hours);
        // console.log('minutes',minutes);
        if (hours == '12') {
          hours = '00';
        }
        if (timesetHolder[2].toUpperCase() == "PM") {
          hours = parseInt(hours, 10) + 12;
          // console.log('herrrr');
        }
        // console.log("hours",hours);
        // console.log("minutes",minutes);

        dateTOSet = dateTOSet+'-'+timesetHolder[0]+' '+hours+':'+minutes+":00";
        ////// dateTOSet = dateTOSet+":00";
        // updateSpecimenRequest.data.receivingDate    = dateTOSet;
        // console.log('updateSpecimenRequest',updateSpecimenRequest);return;
        updateSpecimenRequest.data.receivingDate   = this.datePipe.transform(recievingDate,'MM-dd-yyyy HH:mm:ss');
        updateSpecimenRequest.data.collectionDate = this.datePipe.transform(collectionDate,'MM-dd-yyyy HH:mm:ss');

      }
      else{
        oldValue.target.value = this.datePipe.transform(original, 'MM/dd/yyyy h:mm a')
        alert('invalid recieving date format please enter date in mm/dd/yyyy hh:mm AM/PM format')
        return
      }
      var d = new Date();
      var m =  d.getMonth()+1;
      var dy = d.getDate();
      var yy = d.getFullYear();
      var month;

      var today;
      console.log("dy",dy);
      if (m<10) {
        month = "0"+m
      }
      else{
        month = m;
      }
      if (dy<10) {
        today = "0"+dy
      }
      else{
        today = dy;
      }
      var ytocheck = rec[2].split(' ');
      console.log('ytocheck',ytocheck);
      console.log('yy',yy);

      if (parseInt(ytocheck[0]) > yy) {
        // this.specimenTableData[index]['pDOB'] = this.specimenTableData[index]['pDOB'];
        alert('recieving date year cannot be greater than current year')
        return
      }
      if (month == rec[0] && dy < rec[1] && ytocheck[0] == yy) {
        // this.specimenTableData[index]['pDOB'] = this.specimenTableData[index]['pDOB'];
        alert('recieving date cannot be greater the current date')
        return
      }
      else if (month <= rec[0] && dy <= rec[1] && ytocheck[0] > yy) {
        // this.specimenTableData[index]['pDOB'] = this.specimenTableData[index]['pDOB'];
        alert('recieving date cannot be greater the current date')
        return
      }
    }
    else if (type == 'by') {
      updateSpecimenRequest.data.receivingDate   = this.datePipe.transform(recievingDate,'MM-dd-yyyy HH:mm:ss');
      updateSpecimenRequest.data.collectionDate = this.datePipe.transform(collectionDate,'MM-dd-yyyy HH:mm:ss');
    }

    // console.log("rec",rec);
    // return;

      var updateSpecimenUrl   = environment.API_SPECIMEN_ENDPOINT+ 'updatecaseinfo';

      updateSpecimenRequest.data.caseId    = caseId;
      updateSpecimenRequest.data.collectedBy    = collectedBy;
      // updateSpecimenRequest.data.collectionDate    = collectionDate;
      // updateSpecimenRequest.data.receivingDate    = recievingDate;
      updateSpecimenRequest.data.clientId       = clientId;
      updateSpecimenRequest.data.updatedBy = this.logedInUserRoles.userCode;
      console.log('updateSpecimenRequest',updateSpecimenRequest);

      // return
      this.covidService.updateSpecimenIfo(updateSpecimenUrl,updateSpecimenRequest).subscribe(updateClientResp=>{
        console.log("upaetSpecimenResponse",updateClientResp);
        if (updateClientResp['data'] == 1) {
          console.log('updateSpecimenRequest.data.collectionDate',updateSpecimenRequest.data.collectionDate);
          console.log("this.specimenTableData[index]",this.specimenTableData[index])
          console.log("index",index);
          ;


          this.specimenTableData[index]['collectionDate'] = updateSpecimenRequest.data.collectionDate;
          this.specimenTableData[index]['receivingDate']  = updateSpecimenRequest.data.receivingDate;
          this.specimenTableData[index]['collectedBy']    = updateSpecimenRequest.data.collectedBy;
          this.notifier.notify('success',"Case updated successfully");
          // location.reload();
          // this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          //   // dtInstance.ajax.reload(null, false);
          //   // Destroy the table first
          //   dtInstance.destroy();
          //   // // Call the dtTrigger to rerender again
          //   this.dtTrigger.next();
          // });
        }

      },error=>{
        oldValue.target.value = this.datePipe.transform(original, 'MM/dd/yyyy h:mm a')
        this.notifier.notify('error',"Error while updating specimen")
        // this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        //   // Destroy the table first
        //   dtInstance.destroy();
        //   // Call the dtTrigger to rerender again
        //   this.dtTrigger.next();
        // });
      })



  }

  rowMoveArrow(direction,index,data){

    // console.log("direction",direction);
    // console.log("index",index);
    // console.log("data",data);
    var a = [...this.specimenTableData];
    // console.log("start",a);


    if (index == 0 && direction == "up") {
      return;
    }
    else if(index == (this.specimenTableData.length-1) && direction == "down"){
      return;
    }

    if (direction == "down") {
      var temp = {};
      temp = this.specimenTableData[index];
      this.specimenTableData[index]   = this.specimenTableData[index+1];
      this.specimenTableData[index+1] = temp;
      // var origin       = document.getElementsByClassName('WELROW-'+this.specimenTableData[index]['caseId']);
      // var wellOrigin   = this.specimenTableData[index]['well'];
      //
      // var destination     = document.getElementsByClassName('WELROW-'+this.specimenTableData[index+1]['caseId']);
      // var wellDestination = this.specimenTableData[index+1]['well'];
      // // console.log("c",c);
      // // origin[0]['value'] = wellDestination;
      // var arrayCount = 0;
      // for (let i = 0; i < this.allWelNo.length; i++) {
      //   arrayCount = arrayCount +1;
      //   if (wellDestination == null && wellOrigin == null) {
      //     /////// do nothing
      //   }
      //   else{
      //     if (wellOrigin != null) {
      //       if (wellOrigin == this.allWelNo[i].wellMachineId) {
      //         // this.allWelNo[i].selected = false;
      //         // destination[0]['value'] = origin;
      //       }
      //     }
      //
      //   }
      //
      //   if (arrayCount == this.allWelNo.length) {
      //     // origin[0]['value']      = wellDestination;
      //     // const found = this.allWelNo.find(selected => selected == false);
      //     // destination[0]['value'] = origin;
      //   }
      //
      // }
      // // destination[0]['value'] = origin;
    }
    else if(direction == "up"){
      var temp = {};
      temp = this.specimenTableData[index];
      this.specimenTableData[index]   = this.specimenTableData[index-1];
      this.specimenTableData[index-1] = temp;
    }
    // console.log("end",this.specimenTableData);

  }

  populateUpdateBatchData(){
    this.ngxLoader.start();
    var requestData = {...this.updateBtachRequest}
    // console.log("this.selectedBatch",this.selectedBatch);

    requestData.data.batchId          = this.selectedBatch.batchDetails.batchId;
    requestData.data.batchStatusId    = this.selectedBatch.batchDetails.batchStatusId;
    requestData.data.testStatusId     = "2";

    var myDate = new Date();
    var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');
    requestData.data.updatedBy        = this.logedInUserRoles.userCode;
    requestData.data.createdBy        = this.selectedBatch.batchDetails.createdBy;
    requestData.data.updatedTimestamp = currentDate;


    // this.ngxLoader.stop(); return;
    var tempHolder = {};
    var uniqueCaseIDs = [];
    for (let i = 0; i < this.specimenTableData.length; i++) {
      // console.log("this.specimenTableData[i]['sellectedWell']",this.specimenTableData[i]['sellectedWell']);

      if (this.specimenTableData[i]['sellectedWell'] == null) {
        var myObj = this.allWelNo.find(obj => obj.selected == false,);
        console.log("myObj",myObj);

        for (let j = 0; j < this.allWelNo.length; j++) {
          // console.log("this.allWelNo[i].selected",this.allWelNo[j].selected);

          if (this.allWelNo[j].selected == false) {
            // console.log("this.allWelNo[j]",this.allWelNo[j]);

              // this.specimenTableData[i]['sellectedWell'] = myObj['wellMachineId'];
              tempHolder['wellMachineId'] = this.allWelNo[j]['wellMachineId'];
              this.allWelNo[j].selected = true;
              break;
          }

        }
        // var myObj = this.allWelNo.find(obj => obj.selected == false,);
        //   // this.specimenTableData[i]['sellectedWell'] = myObj['wellMachineId'];
        //   tempHolder['wellMachineId'] = myObj['wellMachineId'];
      }
      else{

        tempHolder['wellMachineId'] = this.specimenTableData[i]['sellectedWell'];
      }
      tempHolder['jobOrderId']    =  this.specimenTableData[i]['jobOrderId'];

      requestData.data.listOfBatchJobOrderIds.push(tempHolder)
      tempHolder = {}
      uniqueCaseIDs.push(this.specimenTableData[i]['caseId'])

      // requestData.data.listOfBatchJobOrderIds.push(this.specimenTableData[i]['jobOrderId']);
      // requestData.data.testIds.push(this.specimenTableData[i]['testId']);

    }
    console.log("requestData",requestData);
    requestData.header.partnerCode  = this.logedInUserRoles.partnerCode;
    requestData.header.userCode     = this.logedInUserRoles.userCode;
    requestData.header.functionalityCode     = "TWMA-CNB";
    // this.ngxLoader.stop(); return;
    var saveBatchUrl = environment.API_COVID_ENDPOINT +'updatebatch';
    this.covidService.createBatch(saveBatchUrl,requestData).subscribe(createResp=>{
      console.log('createResp',createResp);
      if (createResp['result'].codeType == 'S') {
        localStorage.removeItem('createCovidBatchData');
        this.diasbleBack = false;
        var specimenurl = environment.API_SPECIMEN_ENDPOINT + 'updatecasestatusin';

        var specimenreqBody={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "TWMA-CNB",systemHostAddress:"",remoteUserAddress:"",dateTime:""},
        data: {
          caseStatusId:4,
          caseIds:uniqueCaseIDs,
          updatedBy: this.logedInUserRoles.userCode,
          updatedTimestamp:currentDate

        }
      }
      this.covidService.updateCaseStatus(specimenurl,specimenreqBody).subscribe(specimenReps=>{
        if (specimenReps['data']>0) {
          localStorage.removeItem('createCovidBatchData');
          this.diasbleBack = false;

          // setTimeout(function(){
          this.ngxLoader.stop();
          this.notifier.notify("success","Batch updated successfully");
          localStorage.removeItem('createCovidBatchData');
          this.router.navigate(['manage-batches']);
          // },2000);

        }
        else {
          this.notifier.notify("warning","Batch is Updated successfully but there was an error while updating case statuses");
          this.ngxLoader.stop();
          this.alreadyExistError = false;
        }
      },error=>{
        this.notifier.notify("warning","Batch is Updated successfully but there was an error while updating case statuses");
        this.ngxLoader.stop();
        this.alreadyExistError = false;
      })

        // setTimeout(function(){
          // this.ngxLoader.stop();
          // this.notifier.notify("success","Batch updated successfully");
          // localStorage.removeItem('createCovidBatchData');
          // this.router.navigate(['manage-batches']);
        // },2000);

      }
      else{
        this.notifier.notify("error","Error while updating batch");
        this.ngxLoader.stop();
      }

    },error =>{
      this.notifier.notify("error","Error while updating batch");
      this.ngxLoader.stop();
    })
    // this.ngxLoader.stop(); return;
  }

  getIntakeCasesRecord(intakeId){
    return new Promise((resolve, reject) => {
      var req = {...this.loadIntakeCaseRequest}
      req.data.intakeId = intakeId;
      req.header.partnerCode = this.logedInUserRoles.partnerCode;
      req.header.userCode = this.logedInUserRoles.userCode;
      req.header.functionalityCode     = "TWMA-CNB";


      var url = environment.API_SPECIMEN_ENDPOINT+'batchintakecases';

      this.covidService.loadCasesFromIntake(url,req).subscribe(intakeResponse=>{
        console.log("intakeResponse",intakeResponse);
        if (intakeResponse['data'] != null) {
          var allData = {...intakeResponse['data']}
          const uniqueCaseIds = [];
          const map2 = new Map();
          for (const item of intakeResponse['data']) {
            if(!map2.has(item.caseId)){
              map2.set(item.caseId, true);    // set any value to Map
              uniqueCaseIds.push(item.caseId);
            }
          }
          // var allData =[];
          if (intakeResponse != null) {
            var checkTestURL = environment.API_COVID_ENDPOINT + 'checktest';
            var checkTestData = {...this.checktestRequest}
            checkTestData.data.caseIds = uniqueCaseIds;
            checkTestData.header.partnerCode  = this.logedInUserRoles.partnerCode;
            checkTestData.header.userCode     = this.logedInUserRoles.userCode;
            checkTestData.header.functionalityCode     = "TWMA-CNB";
            this.checkTestAndLoadCase(intakeResponse['data'],checkTestURL,checkTestData,'intake')


          }
        }


      })
      resolve();
    })
  }
  checkTestAndLoadCase(getResp,checkTestURL,checkTestData, from){
    this.ngxLoader.start();
    console.log("getResp",getResp);

    var allData = [...getResp]
    this.covidService.checkTest(checkTestURL,checkTestData).subscribe(checkStatusresp =>{
      console.log("checkStatusresp",checkStatusresp);
      console.log("checkStatusresp['data']",checkStatusresp['data']);
      console.log("checkStatusresp['data']['length']",checkStatusresp['data']['length']);
      if (checkStatusresp['data']['length'] > 0) {

        for (let i = 0; i < allData.length; i++) {
          for (let index = 0; index < checkStatusresp['data'].length; index++) {
            if (typeof checkStatusresp['data'][index] != 'undefined') {
              if (checkStatusresp['data'][index].caseId ==   allData[i]['caseId']) {
                allData[i]['testId']       = checkStatusresp['data'][index]['testId'];
                allData[i]['testStatusId'] = checkStatusresp['data'][index]['testStatusId'];
                allData[i]['jobOrderId']   = checkStatusresp['data'][index]['jobOrderId'];
              }
            }
          }
          if (allData[i]['jobOrderId'] == null) {
            allData.splice(i,1);
            i--;
          }
        }

        // const found = allData.some(el => el.jobOrderId == null);
        // console.log("fond",found);


        const uniquePatienIds = [];
        const map = new Map();
        for (const item of allData) {
          if(!map.has(item.patientId)){
            map.set(item.patientId, true);    // set any value to Map
            uniquePatienIds.push(item.patientId);
          }
        }
        const uniqueClientIds = [];
        const map2 = new Map();
        for (const item of allData) {
          if(!map2.has(item.clientId)){
            map2.set(item.clientId, true);    // set any value to Map
            uniqueClientIds.push(item.clientId);
          }
        }

            var patientRequest = {...this.patientRecordRequest};
            patientRequest.data.patientIds = uniquePatienIds;
            patientRequest.header.partnerCode  = this.logedInUserRoles.partnerCode;
            patientRequest.header.userCode     = this.logedInUserRoles.userCode;
            patientRequest.header.functionalityCode     = "TWMA-CNB";
            var patientUrl = environment.API_PATIENT_ENDPOINT + "patientbyids";
            var patient = this.covidService.getPatientRecord(patientUrl,patientRequest).then(getPatientResp =>{
              return getPatientResp;
              // console.log("getPatientResp",getPatientResp);
            }, error=>{
              this.sendOnce = false;
              this.notifier.notify("error","Error while getting patient name.");
              this.ngxLoader.stop();
            })

            var clientRequest = {...this.clientRecordRequest};
            clientRequest.data.clientIds = uniqueClientIds;
            clientRequest.header.partnerCode  = this.logedInUserRoles.partnerCode;
            clientRequest.header.userCode     = this.logedInUserRoles.userCode;
            clientRequest.header.functionalityCode     = "TWMA-CNB";
            var clientUrl = environment.API_CLIENT_ENDPOINT + "clientidsnames";
            var Client = this.covidService.getClientRecord(clientUrl,clientRequest).then(getClientResp =>{
              return getClientResp;

            }, error=>{
              this.notifier.notify("error","Error while getting client name.");
              this.ngxLoader.stop();
            })

            forkJoin([patient, Client]).subscribe(allresults => {
              console.log("allresults",allresults);
              for (let i = 0; i < allData.length; i++) {
                if (allresults[0]['data']['length'] > 0) {
                  for (let j = 0; j < allresults[0]['data'].length; j++) {
                    if (allData[i].patientId == allresults[0]['data'][j].patientId) {
                      allData[i]['pFName'] = this.encryptDecrypt.decryptString(allresults[0]['data'][j].firstName);
                      allData[i]['pLName'] = this.encryptDecrypt.decryptString(allresults[0]['data'][j].lastName);
                      allData[i]['pDOB']   = allresults[0]['data'][j].dateOfBirth;
                    }


                  }
                }

                  if (allresults[1]['data']['length'] > 0) {
                    for (let k = 0; k < allresults[1]['data'].length; k++) {
                      if (allData[i].clientId == allresults[1]['data'][k].clientId) {
                        allData[i]['clientName']    = allresults[1]['data'][k].clientName;
                      }


                    }

                  }


                allData[i]['sellectedWell'] = null;
              }

              console.log("allData",allData);
              if (from == 'batch') {
                this.specimenTableData.push(allData[0]);
              }
              else{
                this.specimenTableData=allData;
              }


              // console.log("-----",this.specimenTableData);
              localStorage.setItem("createCovidBatchData",JSON.stringify(this.specimenTableData));
              this.diasbleBack = true;
              // console.log("this.dtTrigger",this.dtTrigger);
              this.notifier.hideAll();

              // this.dtTrigger.next();
              this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                // // Destroy the table first
                dtInstance.destroy();
                // Call the dtTrigger to rerender again
                this.dtTrigger.next();
                // dtInstance.ajax.reload();
              });
              this.timeCount = 0;
              this.ngxLoader.stop();
              this.batchForm.reset();
              this.sendOnce = false;

              // for (let i = 0; i < allData.length; i++) {
              //   if (allData[i]) {

              //   }

              // }
            })





      }
      else{
        this.sendOnce = false;
        this.caseNumber = null;
        this.notifier.notify("warning","The case has already been Batched");
        this.ngxLoader.stop();
      }


    },error=>{
      this.sendOnce = false;
      this.notifier.notify("error","Error while triaging case");
      this.ngxLoader.stop();
    })
  }

  onDrop(event: CdkDragDrop<string[]>) {
    // console.log('event.previousIndex',event.previousIndex);
    // console.log('event.currentIndex',event.currentIndex);
    // console.log('from',this.specimenTableData[event.previousIndex].constWell);
    // console.log('to',this.specimenTableData[event.currentIndex].constWell);

  moveItemInArray(this.specimenTableData, event.previousIndex, event.currentIndex);
  // if (event.previousContainer === event.container) {
  //     moveItemInArray(this.specimenTableData, event.previousIndex, event.currentIndex);
  //   } else {
  //     console.log("here");
  //
  //     transferArrayItem(event.previousContainer.data['data'], event.container.data['data'], event.previousIndex, event.currentIndex);
  //   }



}

editCase(caseNumber){
  console.log("caseNumber",caseNumber);

  if (typeof this.selectedBatch != 'undefined') {
    // console.log("this.selectedBatch",this.selectedBatch);
    var batchId = this.selectedBatch.batchDetails.batchId;
    console.log("batchId",batchId);
    localStorage.setItem("caseFromEditBatch",JSON.stringify(batchId));
    // this.router.navigate(['edit-case/'+caseNumber+'/1']);

    var a = caseNumber+';0';
    var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
    var bx = ax.replace('+','xsarm');
    var cx = bx.replace('/','ydan');
    var ciphertext = cx.replace('=','zhar');
    console.log('ciphertext',ciphertext);
    // this.router.navigateByUrl('/edit-case/'+ciphertext+'/1')
    var ur = location.origin+'/edit-case/'+ciphertext+'/1';
    console.log("ur",ur);

    window.open( ur , '_blank');
  }
  else{
    var a = caseNumber+';0';
    var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
    var bx = ax.replace('+','xsarm');
    var cx = bx.replace('/','ydan');
    var ciphertext = cx.replace('=','zhar');
    console.log('ciphertext',ciphertext);
    // this.router.navigateByUrl('/edit-case/'+ciphertext+'/1')
    var ur = location.origin+'/edit-case/'+ciphertext+'/1';
    console.log("ur",ur);

    window.open( ur , '_blank');
    // this.router.navigate(['edit-case/'+caseNumber+'/1']);
  }


  return


  // console.log('caseNumber',caseNumber);
  // this.router.navigateByUrl('/edit-case/'+caseNumber+'/1');
  // const downloadLink = document.createElement("a");
  // downloadLink.href = reportResp['data']['base64EncodedFile'];
  // downloadLink.download = fileName;
  // downloadLink.click();

  // this.router.navigate(['edit-case/'+caseNumber+'/1']);
}
clicked(e){
  // console.log("-----e ------",e);

}
updateCaseClient(e,oldName,collectedBy,collectionDate,receivingDate,caseId){
  console.log("e",e);
  if (typeof e != 'undefined') {
    let newClientId = e.clientId;

    var updateSpecimenUrl   = environment.API_SPECIMEN_ENDPOINT+ 'updatecaseinfo';
    var updateSpecimenRequest                 = {...this.updateSpecimenRequest}
    updateSpecimenRequest.data.caseId         = caseId;
    updateSpecimenRequest.data.collectedBy    = collectedBy;
    updateSpecimenRequest.data.collectionDate = collectionDate;
    updateSpecimenRequest.data.receivingDate  = receivingDate;
    updateSpecimenRequest.data.clientId       = newClientId;
    updateSpecimenRequest.data.updatedBy      = this.logedInUserRoles.userCode;
    console.log('updateSpecimenRequest',updateSpecimenRequest);
    this.covidService.updateSpecimenIfo(updateSpecimenUrl,updateSpecimenRequest).subscribe(updateClientResp=>{
      console.log("updateClientResp",updateClientResp);
      if (updateClientResp['data'] == 1) {
        this.notifier.notify('success',"Case updated successfully")
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
      }
      else{
        this.notifier.notify('error',"Error while updating specimen")
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
      }

    },error=>{
      this.notifier.notify('error',"Error while updating specimen")
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        // Destroy the table first
        dtInstance.destroy();
        // Call the dtTrigger to rerender again
        this.dtTrigger.next();
      });
    })
  }


}


clientChanged(){

}
onScreenClick(e){
  // console.log('e',e);

}

}
