import { Component, HostListener, OnInit, ElementRef, ViewChild, QueryList } from '@angular/core';
import { environment } from '../../../../../../src/environments/environment';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NotifierService } from 'angular-notifier';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { GlobalApiCallsService } from '../../services/global/global-api-calls.service';
import { CovidApiCallsService } from '../../services/covid/covid-api-calls.service';
import { concat, Observable, of, Subject, forkJoin } from "rxjs";
import { SortPipe } from "../../pipes/sort.pipe";
import { DataTableDirective } from 'angular-datatables';
import { DatePipe, LocationStrategy } from '@angular/common';
import Swal from 'sweetalert2';
declare var $ :any;
import { TableDragService } from "../../services/drag-drop/table-drag.service";
// import { User } from "./user";
import { Subscription } from "rxjs";
import { moveItemInArray, CdkDragDrop } from "@angular/cdk/drag-drop";
// import 'jquery-ui/ui/widgets/selectable.js';
// import 'jquery-ui/ui/widgets/draggable.js';
// import 'jquery-ui/ui/widgets/droppable.js';
// import 'jquery-ui/ui/widgets/sortable.js';
import { GlobalySharedModule } from './../../../../../../src/app/sharedmodules/globaly-shared/globaly-shared.module'
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-worksheet',
  templateUrl: './worksheet.component.html',
  styleUrls: ['./worksheet.component.css'],
  host        : {
    class     :'mainComponentStyle'
  }
})
export class WorksheetComponent implements OnInit {
  @ViewChild(DataTableDirective, {static: false})


  dtElement              : DataTableDirective;
  dtTrigger              : Subject<WorksheetComponent> = new Subject();
  public dtOptions       : any =  {};
  Populate         : any = [];
  allWelNo              : any =  [];
  constantWelNo         : any =  [];
  public viewBatchRequest = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"TWMM",
      functionalityCode: "TWMA-UW",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      page:"0",
      size:"3",
      sortColumn:"batchId",
      sortingOrder:"desc",
      batchId:""
    }
  }
  public usersRequest: any = {
    header              : {
      uuid              : "",
      partnerCode       : "",
      userCode          : "",
      referenceNumber   : "",
      systemCode        : "",
      moduleCode        : "",
      functionalityCode: "TWMA-UW",
      systemHostAddress : "",
      remoteUserAddress : "",
      dateTime          : ""
    },
    data                : {
      userCodes           :[]
    }
  }
  public getCasesRequest = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"TWMM",
      functionalityCode: "TWMA-UW",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
       caseIds:[]
    }
  }
  public patientRecordRequest = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"PMTM",
      functionalityCode: "TWMA-UW",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      username: "danish",
      patientIds: []
    }
  }
  specimenTableData : any = [];
  specimenTableDataforDragDropCheck : any = [];
  // specimenTableData = new Array(94);
  selectedBatch : any;
  swalStyle              : any;
  public logedInUserRoles :any = {};
  removeCaseData = {};
  removeCaseIndex : any;
  allInterpretations: any =  [];
  // allCaseData = new Array(94);
  aGrid : any = [];
  bGrid : any = [];
  cGrid : any = [];
  dGrid : any = [];
  eGrid : any = [];
  fGrid : any = [];
  gGrid : any = [];
  hGrid : any = [];
  public removeRequest = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"TWMM",
      functionalityCode: "TWMA-UW",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      batchId:"9",
      testStatusId:"3",
      updatedBy:"",
      updatedTimestamp: "30-11-2020 20:07:25",
      jobOrderId:[]

    }
  }
  public updateBtachRequest = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"TWMM",
      functionalityCode: "TWMA-UW",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      batchId:"",
      batchStatusId:"",
      testStatusId:"",
      createdBy:"",
      createdTimestamp: "26-11-2020 20:07:22",
      updatedBy:"",
      updatedTimestamp: "3-12-2020 12:07:22",
      listOfBatchJobOrderIds:[
        // {
        //   jobOrderId:5,
        //   wellMachineId:3
        // }
      ]

    }
  }

  public repeatTestRequest = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"TWMM",
      functionalityCode: "TWMA-UW",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      caseId:"",
      previousTestStatusId:"",
      testStatusId:"",
      jobOrderId:[],
      createdBy:"abc",
      createdTimestamp: "1-12-2020 20:07:25",
      updatedBy:"abc",
      updatedTimestamp: "1-12-2020 20:07:25"

    }
  }
  public submitToReportingRequest = {
    header:{
      createdBy: null,
      createdTimestamp: null,
      updatedBy: null,
      updatedTimestamp: null,
      uuid: "67ffaebe-6188-476e-86ae-c5deb2effa80",
      partnerCode: "1",
      userCode: "sip-12588",
      referenceNumber: "TWMM12032020115628",
      systemCode: "NGLIS",
      moduleCode: "TWMM",
      functionalityCode: "TWMA-UR",
      systemHostAddress: "0:0:0:0:0:0:0:1",
      remoteUserAddress: "0:0:0:0:0:0:0:1",
      dateTime: "12/03/2020 11:56:28"
    },
    data:{
      batchId:"",
      submittedDate:""
    }
  }
  SelectedForSubmitId = "";
  public uploadResultsRequest = {
    header:{
      createdBy: null,
      createdTimestamp: null,
      updatedBy: null,
      updatedTimestamp: null,
      uuid: "67ffaebe-6188-476e-86ae-c5deb2effa80",
      partnerCode: "1",
      userCode: "sip-12588",
      referenceNumber: "TWMM12032020115628",
      systemCode: "NGLIS",
      moduleCode: "TWMM",
      functionalityCode: "TWMA-UR",
      systemHostAddress: "0:0:0:0:0:0:0:1",
      remoteUserAddress: "0:0:0:0:0:0:0:1",
      dateTime: "12/03/2020 11:56:28"
    },
    data:{
      batchId:"23423",
      fileName:"COVID-19_ results_sample_V0.1.xlsx",
      fileExtension:".xlsx",
      base64EncodedFile:"",
      updatedDate : ''
    }
  }
  fileName = "";
  fileBase64 ;
  filePayLoad ;
  diasbleBack = false;
  public downloadResultsRequest = {
    header:{
      createdBy: null,
      createdTimestamp: null,
      updatedBy: null,
      updatedTimestamp: null,
      uuid: "67ffaebe-6188-476e-86ae-c5deb2effa80",
      partnerCode: "1",
      userCode: "sip-12588",
      referenceNumber: "TWMM12032020115628",
      systemCode: "NGLIS",
      moduleCode: "TWMM",
      functionalityCode: "TWMA-UR",
      systemHostAddress: "0:0:0:0:0:0:0:1",
      remoteUserAddress: "0:0:0:0:0:0:0:1",
      dateTime: "12/03/2020 11:56:28"
    },
    data:{
      batchId:"23423"
    }
  }
  public exportExcelRequest = {
    header:{
      createdBy: null,
      createdTimestamp: null,
      updatedBy: null,
      updatedTimestamp: null,
      uuid: "",
      partnerCode: "1",
      userCode: "sip-12588",
      referenceNumber: "",
      systemCode: "NGLIS",
      moduleCode: "TWMM",
      functionalityCode: "TWMA-UW",
      systemHostAddress: "",
      remoteUserAddress: "",
      dateTime: ""
    },
    data:{
      batchId:"23423",
      batchActive: true
    }
  }
  public printPDFRequest = {
    header:{
      createdBy: null,
      createdTimestamp: null,
      updatedBy: null,
      updatedTimestamp: null,
      uuid: "",
      partnerCode: "1",
      userCode: "sip-12588",
      referenceNumber: "",
      systemCode: "NGLIS",
      moduleCode: "TWMM",
      functionalityCode: "TWMA-UW",
      systemHostAddress: "",
      remoteUserAddress: "",
      dateTime: ""
    },
    data:{
      batchId:"23423",
      batchActive: true
    }
  }
  testStatusLookup : any;
  allInterpretationStatus: any;
  testBatchToCancel = {};
  public cancelBatchRequest = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"TWMM",
      functionalityCode: "TWMA-UW",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      batchId:"7",
      batchStatusId:"2",
      testStatusId:"3",
      updatedBy:"abc",
      updatedTimestamp: "30-11-2020 20:07:25"

    }
  }
  timeZone;
  controlPressed = false;
  constructor(
    private formBuilder     : FormBuilder,
    private router          : Router,
    private http            : HttpClient,
    private notifier        : NotifierService,
    private ngxLoader       : NgxUiLoaderService,
    private globalService   : GlobalApiCallsService,
    private covidService   : CovidApiCallsService,
    private sortPipe        : SortPipe,
    private elementRef: ElementRef,
    private route        : ActivatedRoute,
    private datePipe: DatePipe,
    private location: LocationStrategy,
    private encryptDecrypt: GlobalySharedModule
  ) {
    this.notifier        = notifier;
    this.swalStyle       =  Swal.mixin({
      customClass : {
        confirmButton : 'btn btn-success my-swal-success',
        cancelButton  : 'btn btn-danger'
      },
      buttonsStyling  : true
    }) }

  ngOnInit(): void {

    console.log("BaseUrl",location.origin)
    var offset = new Date().getTimezoneOffset();
    this.timeZone = offset;
    // console.log("OFFFSSEETTT",offset);
    // console.log(Intl.DateTimeFormat().resolvedOptions().timeZone)
    var flag = 0;
    var num =1
    // var count = 1;
    var tempHold = {};
    var temp     = [];

    for (let i = 0; i<96; i++) {
      for (let j = flag; j < 8; j++) {

          if (j == 0) {
            var name = '';
            if (num <10) { name = 'A0' }else{name = 'A'}
            tempHold['constWell'] = name+(num);
            temp.push(tempHold);
            tempHold = {}
             flag = flag+1;break;
          }
          if (j == 1) {
            if (num <10) { name = 'B0' }else{name = 'B'}
            tempHold['constWell'] = name+(num);
            temp.push(tempHold);
            tempHold = {}
             flag = flag+1;break;
          }
          if (j == 2) {
            if (num <10) { name = 'C0' }else{name = 'C'}
            tempHold['constWell'] = name+(num);
            temp.push(tempHold);
            tempHold = {}
             flag = flag+1;break;
          }
          if (j == 3) {
            if (num <10) { name = 'D0' }else{name = 'D'}
            tempHold['constWell'] = name+(num);
            temp.push(tempHold);
            tempHold = {}
             flag = flag+1;break;
          }
          if (j == 4) {
            if (num <10) { name = 'E0' }else{name = 'E'}
            tempHold['constWell'] = name+(num);
            temp.push(tempHold);
            tempHold = {}
             flag = flag+1;break;
          }
          if (j == 5) {
            if (num <10) { name = 'F0' }else{name = 'F'}
            tempHold['constWell'] = name+(num);
            temp.push(tempHold);
            tempHold = {}
             flag = flag+1;break;
          }
          if (j == 6) {
            if (num <10) { name = 'G0' }else{name = 'G'}
            tempHold['constWell'] = name+(num);
            temp.push(tempHold);
            tempHold = {}
             flag = flag+1;break;
          }
          if (j == 7) {
            if (num <10) { name = 'H0' }else{name = 'H'}
            tempHold['constWell'] = name+(num);
            temp.push(tempHold);
            tempHold = {}
            flag = 0;
            num = num+  1;
            break;
          }

      }

    }


    // console.log("temp",temp);
    this.specimenTableData = [...temp];
    for (let index = 0; index < temp.length; index++) {
      this.specimenTableDataforDragDropCheck.push(temp[index]['constWell'])
    }

    window.addEventListener("beforeunload", function (e) {
      var oldData = JSON.parse(localStorage.getItem('worksheetBatchData'));
      console.log("oldData",oldData);

      if (oldData != null) {
        // alert("hi")
       e.returnValue = ''
       return '';
      }

    });
    localStorage.removeItem('worksheetBatchData');
    this.diasbleBack = false;
    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
    var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
    var originalText = bts.toString(CryptoJS.enc.Utf8);
    this.logedInUserRoles = JSON.parse(originalText)

    if (this.logedInUserRoles['allowedRoles'].indexOf("TWMA-UW") != -1) {
      // this.viewBatchRequest['header'].functionalityCode    = "TWMA-UW";
      // this.usersRequest['header'].functionalityCode    = "TWMA-UW";
      this.getCasesRequest['header'].functionalityCode    = "TWMA-UW";
      this.patientRecordRequest['header'].functionalityCode    = "TWMA-UW";
      this.downloadResultsRequest['header'].functionalityCode    = "TWMA-UW";
      // this.exportExcelRequest['header'].functionalityCode    = "TWMA-UW";
      this.printPDFRequest['header'].functionalityCode    = "TWMA-UW";
    }
    else if (this.logedInUserRoles['allowedRoles'].indexOf("TWMA-VW") != -1){
      // this.viewBatchRequest['header'].functionalityCode    = "TWMA-VW";
      // this.usersRequest['header'].functionalityCode    = "TWMA-VW";
      this.getCasesRequest['header'].functionalityCode    = "TWMA-VW";
      this.patientRecordRequest['header'].functionalityCode    = "TWMA-VW";
      this.downloadResultsRequest['header'].functionalityCode    = "TWMA-VW";
      // this.exportExcelRequest['header'].functionalityCode    = "TWMA-VW";
      this.printPDFRequest['header'].functionalityCode    = "TWMA-VW";
    }

    this.route.params.subscribe(params => {
      console.log("params",params);
      if (typeof params['id'] == "undefined") {
        // this.pageType = 'add';
      }
      else{
        var coverted = params['id'].replace('xsarm','+').replace('ydan','/').replace('zhar','=');
        var bytes  = CryptoJS.AES.decrypt(coverted, 'nglis');
        var originalText = bytes.toString(CryptoJS.enc.Utf8).toString();
        console.log("originalText",originalText);
        this.getLookups().then(lookupsLoaded => {
          var selectURL = environment.API_COVID_ENDPOINT + 'viewbatch';
          var getBatchReq = {...this.viewBatchRequest}
          getBatchReq.header.partnerCode  = this.logedInUserRoles.partnerCode;
          getBatchReq.header.userCode     = this.logedInUserRoles.userCode;
          getBatchReq['header'].functionalityCode    = "TWMA-MBC";
          
          // getBatchReq.header.functionalityCode     = "TWMA-UW";
          getBatchReq.data.batchId = originalText;
          // getBatchReq.data.batchId = params['id'];
          this.ngxLoader.start();
          this.getSelectedRecord(selectURL,getBatchReq).then(selectedPatient => {
            // this.ngxLoader.stop();
          });
        });
        // this.pageType = 'edit';

      }
      // console.log("this.pageType",this.pageType);

    })


    this.getLookups().then(lookupsLoaded => {
    });

    this.dtOptions = {
      paging        :false,
      searching    : false,
      serverSide   : false,
      processing   : false,
      columnDefs: [
        { orderable: true, className: 'reorder', targets: [2,3,4,5,6,7, 'desc'] },
        { orderable: false, targets: ['_all'] }
      ],
    };


  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
    console.log("$('#report_tbody')",$('#report_tbody'));

    // $('#report_tbody').sortable();
     // ($('.selectable') as any).selectable();


  }
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }


  getSelectedRecord(url,data) {
    // this.ngxLoader.start();
    return new Promise((resolve, reject) => {
      this.covidService.getBatchById(url, data).subscribe(getByIdResp=>{
        console.log("getByIdResp",getByIdResp);
        if (typeof getByIdResp['result'] != 'undefined') {
          if (typeof getByIdResp['result']['description'] != 'undefined') {
            if (getByIdResp['result']['description'].indexOf('UnAuthorized') != -1) {
              this.notifier.notify('warning',getByIdResp['result']['description'])
              this.router.navigateByUrl('/page-restrict');
            }
          }
        }
        if (getByIdResp['result']['codeType'] == 'T') {
          this.notifier.notify("error","No Record Found");
          this.ngxLoader.stop();
          this.router.navigate(['manage-batches']);
          resolve()
          return;
        }
        // this.ngxLoader.stop();

        var userRequest = {...this.usersRequest};
        var submittedByhold = null;
        if (typeof getByIdResp['data']['batchDetails']['submittedBy'] != 'undefined' && getByIdResp['data']['batchDetails']['submittedBy'] != null) {
          // console.log("getByIdResp['data']['batchDetails']['submittedBy']",getByIdResp['data']['batchDetails']['submittedBy']);

          // submittedByhold = getByIdResp['data']['batchDetails']['submittedBy'];
          if (typeof getByIdResp['data']['batchDetails']['updatedBy'] != 'undefined' && getByIdResp['data']['batchDetails']['updatedBy'] != null) {
            userRequest.data.userCodes = [getByIdResp['data']['batchDetails']['createdBy'],getByIdResp['data']['batchDetails']['updatedBy'],getByIdResp['data']['batchDetails']['submittedBy']];

          }
          else{
            userRequest.data.userCodes = [getByIdResp['data']['batchDetails']['createdBy']];

          }

        }
        else{
          if (typeof getByIdResp['data']['batchDetails']['updatedBy'] != 'undefined' && getByIdResp['data']['batchDetails']['updatedBy'] != null) {
            userRequest.data.userCodes = [getByIdResp['data']['batchDetails']['createdBy'],getByIdResp['data']['batchDetails']['updatedBy']];
          }
          else{
            userRequest.data.userCodes = [getByIdResp['data']['batchDetails']['createdBy']];

          }

          // submittedByhold = null;
        }


        // userRequest.data.userCodes = [getByIdResp['data']['batchDetails']['createdBy'],getByIdResp['data']['batchDetails']['updatedBy'],submittedByhold];
        var userUrl = environment.API_USER_ENDPOINT + "clientusers";
        userRequest.header.partnerCode  = this.logedInUserRoles.partnerCode;
        userRequest.header.userCode     = this.logedInUserRoles.userCode;
        userRequest.header.functionalityCode     = "CLTA-VC";
        this.covidService.getUserRecord(userUrl,userRequest).then(getUserResp =>{
          // return getUserResp;
          // console.log("getUserResp",getUserResp);
          for (let x = 0; x < getUserResp['data'].length; x++) {
            if (getUserResp['data'][x].userCode == getByIdResp['data']['batchDetails']['createdBy']) {
              getByIdResp['data']['batchDetails'].createdByName = getUserResp['data'][x].fullName;
            }
            if (getUserResp['data'][x].userCode == getByIdResp['data']['batchDetails']['updatedBy']) {
              getByIdResp['data']['batchDetails'].updatedByName = getUserResp['data'][x].fullName;
            }
            if (getUserResp['data'][x].userCode == getByIdResp['data']['batchDetails']['submittedBy']) {
              getByIdResp['data']['batchDetails'].submittedByName = getUserResp['data'][x].fullName;
            }

          }


          this.selectedBatch = getByIdResp['data'];
          console.log("this.selectedBatch",this.selectedBatch);


          const uniqueCaseIds = [];
          const map = new Map();
          for (const item of getByIdResp['data']['jobOrderDetails']) {
            if(!map.has(item.caseId)){
              map.set(item.caseId, true);    // set any value to Map
              uniqueCaseIds.push(item.caseId);
            }
          }

          var casesUrl     = environment.API_SPECIMEN_ENDPOINT + 'findbycaseidin';
          var casesRequest = {...this.getCasesRequest}
          casesRequest.data.caseIds = uniqueCaseIds;
          casesRequest.header.partnerCode  = this.logedInUserRoles.partnerCode;
          casesRequest.header.userCode     = this.logedInUserRoles.userCode;
          // casesRequest.header.functionalityCode     = "TWMA-UW";
          this.covidService.getCasesByIds(casesUrl,casesRequest).subscribe(allData=>{
            for (let i = 0; i < allData['data'].length; i++) {
              if (getByIdResp['data']['jobOrderDetails'].length>0) {
                for (let j = 0; j < getByIdResp['data']['jobOrderDetails'].length; j++) {
                  // console.log("getByIdResp['data']['jobOrderDetails'][j]['testStatusId']",getByIdResp['data']['jobOrderDetails'][j]['testStatusId']);

                  if (getByIdResp['data']['jobOrderDetails'][j]['caseId'] == allData['data'][i]['caseId']) {
                    allData['data'][i]['jobOrderId']    = getByIdResp['data']['jobOrderDetails'][j]['jobOrderId']
                    allData['data'][i]['testId']        = getByIdResp['data']['jobOrderDetails'][j]['testId']
                    allData['data'][i]['testStatusId']  = getByIdResp['data']['jobOrderDetails'][j]['testStatusId']
                    allData['data'][i]['wellMachineId'] = getByIdResp['data']['jobOrderDetails'][j]['wellMachineId']
                    allData['data'][i]['sellectedWell'] = getByIdResp['data']['jobOrderDetails'][j]['wellMachineId']
                    allData['data'][i]['batchId']       = getByIdResp['data']['batchDetails']['batchId']
                    allData['data'][i]['repeatedStatus']       = getByIdResp['data']['jobOrderDetails'][j]['repeatedStatus']
                    // console.log("getByIdResp['data']['jobOrderDetails'][j]['interpretation']",getByIdResp['data']['jobOrderDetails'][j]['interpretation']);
                    // console.log("getByIdResp['data']['jobOrderDetails'][j]['interpretation']",getByIdResp['data']['jobOrderDetails'][j]['interpretation']['length']);
                    if (getByIdResp['data']['jobOrderDetails'][j]['interpretation']['length']>0) {
                      // console.log('-----');


                      allData['data'][i]['InterceptionId']= getByIdResp['data']['jobOrderDetails'][j]['interpretation'][0]['interpretationId'];
                      allData['data'][i]['interpretationStatusId']       = getByIdResp['data']['jobOrderDetails'][j]['interpretation'][0]['interpretationStatusId'];
                      for (let n = 0; n < this.allInterpretations.length; n++) {
                        // if (typeof allData['data'][i]['InterceptionId'] != 'undefined') {
                          if (this.allInterpretations[n]['testInterpretationId'] == allData['data'][i]['InterceptionId']) {
                            allData['data'][i]['InterceptionName'] = this.allInterpretations[n]['value'];
                          }
                        // }
                      }

                      for (let k = 0; k < this.allInterpretationStatus.length; k++) {
                        // if (typeof allData['data'][i]['testStatusId'] != 'undefined') {
                          if (this.allInterpretationStatus[k]['resultStatusId'] == allData['data'][i]['interpretationStatusId']) {
                            allData['data'][i]['testStatusName'] = this.allInterpretationStatus[k]['resultStatusValue'];
                          }
                        // }

                      }

                    }

                    ///////// get well names
                    for (let m = 0; m < this.allWelNo.length; m++) {
                      if (this.allWelNo[m]['wellMachineId'] == allData['data'][i]['sellectedWell']) {
                        allData['data'][i]['sellectedWellName'] = this.allWelNo[m]['wellNumber']
                      }

                    }

                    ///////// get Interception names
                    // console.log("this.allInterpretations",this.allInterpretations);




                    // allData['data'][i]['InterceptionId']= getByIdResp['data']['jobOrderDetails'][j]['interpretation'][0]['interpretationId'];
                  }

                }
              }

              // ///////// get well names
              // for (let m = 0; m < this.allWelNo.length; m++) {
              //   if (this.allWelNo[m]['wellMachineId'] == allData['data'][i]['sellectedWell']) {
              //     allData['data'][i]['sellectedWellName'] = this.allWelNo[m]['wellNumber']
              //   }
              //
              // }
              //
              // ///////// get Interception names
              // // console.log("this.allInterpretations",this.allInterpretations);
              //
              // for (let n = 0; n < this.allInterpretations.length; n++) {
              //   if (typeof allData['data'][i]['InterceptionId'] != 'undefined') {
              //     if (this.allInterpretations[n]['testInterpretationId'] == allData['data'][i]['InterceptionId']) {
              //       allData['data'][i]['InterceptionName'] = this.allInterpretations[n]['value'];
              //     }
              //   }
              // }
              //
              // for (let k = 0; k < this.testStatusLookup.length; k++) {
              //   if (typeof allData['data'][i]['testStatusId'] != 'undefined') {
              //     if (this.testStatusLookup[k]['resultStatusId'] == allData['data'][i]['testStatusId']) {
              //       allData['data'][i]['testStatusName'] = this.testStatusLookup[k]['resultStatusValue'];
              //     }
              //   }
              //
              // }

            }
            // console.log("allData",allData);
            const uniquePatientIds = [];
            const map = new Map();
            for (const item of allData['data']) {
              if(!map.has(item.patientId)){
                map.set(item.patientId, true);    // set any value to Map
                uniquePatientIds.push(item.patientId);
              }
            }
            var patientRequest = {...this.patientRecordRequest};
            patientRequest.header.partnerCode  = this.logedInUserRoles.partnerCode;
            patientRequest.header.userCode     = this.logedInUserRoles.userCode;
            // patientRequest.header.functionalityCode     = "TWMA-UW";
            patientRequest.data.patientIds = uniquePatientIds;
            var patientUrl = environment.API_PATIENT_ENDPOINT + "patientbyids";
            var patient = this.covidService.getPatientRecord(patientUrl,patientRequest).then(getPatientResp =>{
              return getPatientResp;
              // console.log("getPatientResp",getPatientResp);
            }, error=>{
              this.notifier.notify("error","Error while getting patient name.");
              this.ngxLoader.stop();
              resolve();
            })
            forkJoin([patient]).subscribe(allresults => {
              console.log("allresults",allresults);
              var patient = allresults[0];
              // var client = allresults[1];
              for (let l = 0; l < allData['data'].length; l++) {
                //////////patient
                for (let k = 0; k < patient['data'].length; k++) {
                  if (allData['data'][l]['patientId'] == patient['data'][k]['patientId']) {
                    allData['data'][l]['pFName']      =  this.encryptDecrypt.decryptString(patient['data'][k].firstName);
                    allData['data'][l]['pLName']      =  this.encryptDecrypt.decryptString(patient['data'][k].lastName);
                    allData['data'][l]['pDOB']        =  patient['data'][k].dateOfBirth;
                    // allData['data'][l]['clientName']  =  patient['data'][k].firstName;
                    // allData['data'][l]['collectionDate']  =  '02-11-2020';
                    // allData['data'][l]['receivingDate']  =  '02-11-2020';
                    // allData['data'][l]['collectedBy']  =  1;
                  }

                }


              }
              // console.log("allData",allData);
              // this.specimenTableData = allData['data'];
              // console.log("this.specimenTableData.length",this.specimenTableData.length);

              var temp    = [];
              var chek = [];
              var acount = 0
              for (let i = 0; i < this.specimenTableData.length; i++) {
                var tepHold = {}
                acount = acount +1;
                for (let j = 0; j < allData['data'].length; j++) {
                  if (this.specimenTableData[i]['constWell'] == allData['data'][j]['sellectedWellName']) {

                    this.specimenTableData[i]['data']=allData['data'][j];
                  }
                  else{
                    if (typeof this.specimenTableData[i]['data'] == "undefined") {
                      this.specimenTableData[i]['data']={};

                    }
                  }
                  if (i==94) {
                    this.specimenTableData[i]['data']['caseNumber'] = 'NC';
                    // this.specimenTableData[i]['data']['checkVisisble'] = 0;

                  }
                  if (i==95) {
                    this.specimenTableData[i]['data']['caseNumber'] = 'PC';
                    // this.specimenTableData[i]['data']['checkVisisble'] = 0;
                  }


                }

                if (acount ==  this.specimenTableData.length) {
                ////// populate uper table
                this.populateBatchTray(this.specimenTableData);
                }
              }
              // console.log("this.specimenTableData",this.specimenTableData);







              // for (let f = 0; f < this.aGrid.length; f++) {
              //   if (typeof allData['data'][f] != "undefined") {
              //     if (allData['data'][f].sellectedWellName.search('A') != -1) {
              //       this.aGrid[f] = allData['data'][f];
              //     }
              //   }
              //
              // }
              // for (let f = 0; f < this.bGrid.length; f++) {
              //   if (typeof allData['data'][f] != "undefined") {
              //     if (allData['data'][f].sellectedWellName.search('B') != -1) {
              //       this.bGrid[f] = allData['data'][f];
              //     }
              //
              //   }
              //
              // }
              // for (let f = 0; f < this.cGrid.length; f++) {
              //   if (typeof allData['data'][f] != "undefined") {
              //     if (allData['data'][f].sellectedWellName.search('C') != -1) {
              //       this.cGrid[f] = allData['data'][f];
              //     }
              //
              //   }
              //
              // }
              // for (let f = 0; f < this.dGrid.length; f++) {
              //   if (typeof allData['data'][f] != "undefined") {
              //     if (allData['data'][f].sellectedWellName.search('D') != -1) {
              //       this.dGrid[f] = allData['data'][f];
              //     }
              //
              //   }
              // }
              // for (let f = 0; f < this.eGrid.length; f++) {
              //   if (typeof allData['data'][f] != "undefined") {
              //     if (allData['data'][f].sellectedWellName.search('E') != -1) {
              //       this.eGrid[f] = allData['data'][f];
              //     }
              //
              //   }
              //
              // }
              // for (let f = 0; f < this.fGrid.length; f++) {
              //   if (typeof allData['data'][f] != "undefined") {
              //     if (allData['data'][f].sellectedWellName.search('F') != -1) {
              //       this.fGrid[f] = allData['data'][f];
              //     }
              //
              //   }
              // }
              // for (let f = 0; f < this.gGrid.length; f++) {
              //   if (typeof allData['data'][f] != "undefined") {
              //     if (allData['data'][f].sellectedWellName.search('G') != -1) {
              //       this.gGrid[f] = allData['data'][f];
              //     }
              //
              //   }
              //
              // }
              // for (let f = 0; f < this.hGrid.length; f++) {
              //   if (typeof allData['data'][f] != "undefined") {
              //     if (allData['data'][f].sellectedWellName.search('H') != -1) {
              //       this.hGrid[f] = allData['data'][f];
              //     }
              //   }
              //
              // }
              // console.log("this.specimenTableData",this.specimenTableData);

              if (this.selectedBatch.batchDetails.batchStatusId == 1) {
                this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                  // Destroy the table first
                  dtInstance.destroy();
                  // Call the dtTrigger to rerender again
                  this.dtTrigger.next();
                });
              }


              // this.populateWells();
              this.ngxLoader.stop();
              resolve();

            })


          },error=>{
            this.notifier.notify("error","Error while loading cases");
            this.ngxLoader.stop();
            resolve()
          })


        })


      },error=>{
        this.notifier.notify("error","Error while fetching batch")
        this.ngxLoader.stop();
        resolve();
      });
      resolve();
    })
  }



  row_move() {
    // $("#report_tbody").sortable({
    //   update: function (event, ui) {
    //     $(this)
    //       .children()
    //       .each(function (index) {
    //         $(this)
    //           .find("td")
    //           // .last()
    //           // .html(index + 1);
    //       });
    //   }
    // });


  }

  rowMoveArrow(direction,index,data){

    // console.log("data['data']['caseNumber']",data['data']['caseNumber']);
    // console.log('this.specimenTableData',this.specimenTableData);
    // return;
    var a = [...this.specimenTableData];
    // console.log("this.specimenTableData[index]",this.specimenTableData[index]);
    // return;
    if (index == 0 && direction == "up") {
      return;
    }
    else if(index == (this.specimenTableData.length-1) && direction == "down"){
      return;
    }
    if (data['data']['caseNumber'] == "NC" || data['data']['caseNumber'] == "PC") {
      // console.log('here');

      return;
    }
    if (this.specimenTableData[index+1]['data']['caseNumber']== "NC" || this.specimenTableData[index+1]['data']['caseNumber']== "PC") {
      return;
    }

    if (direction == "down") {

      var temp = {};
      var tempConstWell = "";
      var old  = {}
      var oldConstWell = "";
      var souceEmpty        = false;
      var destintationEmpty = false;

      temp = {...this.specimenTableData[index]['data']};
      tempConstWell = this.specimenTableData[index]['constWell'];
      if (Object.keys(temp).length === 0) {
        souceEmpty = true;
      }
      var sourceMachine = temp['wellMachineId'];
      var sourceWell    = temp['wellMachineId'];
      old = {...this.specimenTableData[index+1]['data']};
      oldConstWell = this.specimenTableData[index+1]['constWell'];
      if (Object.keys(old).length === 0) {
        destintationEmpty = true;

      }
      var destMachine = old['wellMachineId'];
      var destWell    = old['wellMachineId'];
      // console.log("tempConstWell",tempConstWell);
      // console.log("oldConstWell",oldConstWell);
      // console.log("source",temp);
      // console.log("dest",old);

      // return
      // temp = this.specimenTableData[index];
      this.specimenTableData[index]['data']   = this.specimenTableData[index+1]['data'];
      //////// wellMachine Id should not be changed
      if (souceEmpty == true) {
        /////// no need to swap wells and machines
        for (let i = 0; i < this.allWelNo.length; i++) {
          if (this.allWelNo[i]['wellNumber'] == tempConstWell) {
            // console.log('-----',this.allWelNo[i]['wellNumber']);
            // console.log('-----',this.allWelNo[i]['wellMachineId']);

            this.specimenTableData[index]['data']['wellMachineId'] = this.allWelNo[i]['wellMachineId'];
            this.specimenTableData[index]['data']['sellectedWell'] = this.allWelNo[i]['wellMachineId'];
          }

        }
      }
      else{
        this.specimenTableData[index]['data']['wellMachineId'] = sourceMachine;
        this.specimenTableData[index]['data']['sellectedWell'] = sourceWell;
      }
      this.specimenTableData[index+1]['data'] = temp;
      if (destintationEmpty == true) {
        /////// no need to swap wells and machines
        for (let i = 0; i < this.allWelNo.length; i++) {
          if (this.allWelNo[i]['wellNumber'] == oldConstWell) {
            // console.log('-----',this.allWelNo[i]['wellNumber']);
            // console.log('-----',this.allWelNo[i]['wellMachineId'])
            this.specimenTableData[index+1]['data']['wellMachineId'] = this.allWelNo[i]['wellMachineId'];
            this.specimenTableData[index+1]['data']['sellectedWell'] = this.allWelNo[i]['wellMachineId'];
          }

        }
      }
      else{
        this.specimenTableData[index+1]['data']['wellMachineId'] = destMachine;
        this.specimenTableData[index+1]['data']['sellectedWell'] = destWell;
      }

      localStorage.setItem('worksheetBatchData',JSON.stringify(this.specimenTableData));
      this.diasbleBack = true;
      // console.log("after source",this.specimenTableData[index]['data']);
      // console.log("after dest",this.specimenTableData[index+1]['data']);
      this.populateBatchTray(this.specimenTableData);

    }
    else if(direction == "up"){
      var temp = {};
      var tempConstWell = "";
      var old  = {}
      var oldConstWell = "";
      var souceEmpty        = false;
      var destintationEmpty = false;

      temp = {...this.specimenTableData[index]['data']};
      tempConstWell = this.specimenTableData[index]['constWell'];
      if (Object.keys(temp).length === 0) {
        souceEmpty = true;
      }
      var sourceMachine = temp['wellMachineId'];
      var sourceWell    = temp['wellMachineId'];
      old = {...this.specimenTableData[index-1]['data']};
      oldConstWell = this.specimenTableData[index-1]['constWell'];
      if (Object.keys(old).length === 0) {
        destintationEmpty = true;

      }
      var destMachine = old['wellMachineId'];
      var destWell    = old['wellMachineId'];
      // console.log("tempConstWell",tempConstWell);
      // console.log("oldConstWell",oldConstWell);
      // console.log("source",temp);
      // console.log("dest",old);

      // return
      // temp = this.specimenTableData[index];
      this.specimenTableData[index]['data']   = this.specimenTableData[index-1]['data'];
      //////// wellMachine Id should not be changed
      if (souceEmpty == true) {
        /////// no need to swap wells and machines
        for (let i = 0; i < this.allWelNo.length; i++) {
          if (this.allWelNo[i]['wellNumber'] == tempConstWell) {
            // console.log('-----',this.allWelNo[i]['wellNumber']);
            // console.log('-----',this.allWelNo[i]['wellMachineId']);

            this.specimenTableData[index]['data']['wellMachineId'] = this.allWelNo[i]['wellMachineId'];
            this.specimenTableData[index]['data']['sellectedWell'] = this.allWelNo[i]['wellMachineId'];
          }

        }
      }
      else{
        this.specimenTableData[index]['data']['wellMachineId'] = sourceMachine;
        this.specimenTableData[index]['data']['sellectedWell'] = sourceWell;
      }
      this.specimenTableData[index-1]['data'] = temp;
      if (destintationEmpty == true) {
        /////// no need to swap wells and machines
        for (let i = 0; i < this.allWelNo.length; i++) {
          if (this.allWelNo[i]['wellNumber'] == oldConstWell) {
            // console.log('-----',this.allWelNo[i]['wellNumber']);
            // console.log('-----',this.allWelNo[i]['wellMachineId'])
            this.specimenTableData[index-1]['data']['wellMachineId'] = this.allWelNo[i]['wellMachineId'];
            this.specimenTableData[index-1]['data']['sellectedWell'] = this.allWelNo[i]['wellMachineId'];
          }

        }
      }
      else{
        this.specimenTableData[index-1]['data']['wellMachineId'] = destMachine;
        this.specimenTableData[index-1]['data']['sellectedWell'] = destWell;
      }

      localStorage.setItem('worksheetBatchData',JSON.stringify(this.specimenTableData));
      this.diasbleBack = true;
      // console.log("after source",this.specimenTableData[index]['data']);
      // console.log("after dest",this.specimenTableData[index-1]['data']);
      this.populateBatchTray(this.specimenTableData);




      // var temp = {};
      // var old  = {}
      //
      // temp = {...this.specimenTableData[index]['data']};
      // var sourceMachine = temp['wellMachineId'];
      // var sourceWell    = temp['wellMachineId'];
      // old = {...this.specimenTableData[index-1]['data']};
      // var destMachine = old['wellMachineId'];
      // var destWell    = old['wellMachineId'];
      // // console.log("before source",temp);
      // // console.log("before dest",old);
      // // return
      // // temp = this.specimenTableData[index];
      // this.specimenTableData[index]['data']   = this.specimenTableData[index-1]['data'];
      // //////// wellMachine Id should not be changed
      // this.specimenTableData[index]['data']['wellMachineId'] = sourceMachine;
      // this.specimenTableData[index]['data']['sellectedWell'] = sourceWell;
      //
      // this.specimenTableData[index-1]['data'] = temp;
      // this.specimenTableData[index-1]['data']['wellMachineId'] = destMachine;
      // this.specimenTableData[index-1]['data']['sellectedWell'] = destWell;
      // localStorage.setItem('worksheetBatchData',JSON.stringify(this.specimenTableData));
      // this.populateBatchTray(this.specimenTableData);
    }

  }

  showRemoveModal(item, index){
    console.log("this.specimenTableData",this.specimenTableData);
    let count = 0;
    for (let i = 0; i < this.specimenTableData.length; i++) {
      if (typeof this.specimenTableData[i]['data']['caseId'] != 'undefined') {
        ++count;
      }

    }

    if (count == 1) {
      this.notifier.notify('warning',"There must be at least one case in a batch.")
      return;
    }
    else{
      if (typeof item['data']['batchId'] == 'undefined') {
        return;
      }
      else{
        console.log("item",item);
        this.removeCaseData  = item['data'];
        this.removeCaseIndex = index;
        $('#removeModal').modal('show');
      }
    }
    // console.log("item['data']['batchId']",item['data']['batchId']);
    // return



  }

  removeCase(){
    this.ngxLoader.start();
    var myDate = new Date();
    var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');


    if (typeof this.removeCaseData['batchId'] !== undefined) {

      var removeUrl     = environment.API_COVID_ENDPOINT+'removecase';
      var removeRequest = {...this.removeRequest};
      removeRequest.header.partnerCode  = this.logedInUserRoles.partnerCode;
      removeRequest.header.userCode     = this.logedInUserRoles.userCode;
      removeRequest.header.functionalityCode     = "TWMA-UW";
      removeRequest.data.batchId = this.removeCaseData['batchId'];
      if (this.removeCaseData['testStatusId'] == '6') {
        removeRequest.data.testStatusId = this.removeCaseData['testStatusId'];
      }
      else{
        removeRequest.data.testStatusId = "1";
      }

      removeRequest.data.updatedBy = this.logedInUserRoles.userCode;
      removeRequest.data.updatedTimestamp = "30-11-2020 20:07:25";
      removeRequest.data.jobOrderId = [this.removeCaseData['jobOrderId']];
      console.log("removeRequest",removeRequest);
      console.log("his.removeCaseData",this.removeCaseData);

      // return;

      this.covidService.removeCase(removeUrl,removeRequest).subscribe(removeResp =>{
        console.log("removeResp",removeResp);
        if (removeResp['result']['codeType'] == 'S') {
          if (this.removeCaseData['testStatusId'] == 6) {
            $('#removeModal').modal('hide');
            this.notifier.notify("success","Case removed successfully");
              this.ngxLoader.stop();
            this.specimenTableData[this.removeCaseIndex]['data'] = {};
            for (let i = 0; i < this.allWelNo.length; i++) {
              if (this.allWelNo[i].wellMachineId == this.removeCaseData['sellectedWell']) {
                this.allWelNo[i].selected = false;
              }

            }

            this.removeCaseData  = {};
            this.removeCaseIndex = '';

            this.populateBatchTray(this.specimenTableData);


          }else{
            var specimenurl = environment.API_SPECIMEN_ENDPOINT + 'updatecasestatusin';

            var specimenreqBody={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "TWMA-UW",systemHostAddress:"",remoteUserAddress:"",dateTime:""},
            data: {
              caseStatusId:3,
              caseIds:[this.removeCaseData['caseId']],
              updatedBy: this.logedInUserRoles.userCode,
              updatedTimestamp:currentDate

            }
          }
          this.covidService.updateCaseStatus(specimenurl,specimenreqBody).subscribe(specimenReps=>{
            if (specimenReps['data']>0) {
              this.ngxLoader.stop();
              $('#removeModal').modal('hide');
              this.notifier.notify("success","Case removed successfully");
              // localStorage.removeItem('createCovidBatchData');
              // this.router.navigate(['manage-batches']);
              // this.specimenTableData.splice(this.removeCaseIndex,1);
              this.specimenTableData[this.removeCaseIndex]['data'] = {};
              for (let i = 0; i < this.allWelNo.length; i++) {
                if (this.allWelNo[i].wellMachineId == this.removeCaseData['sellectedWell']) {
                  this.allWelNo[i].selected = false;
                }

              }

              this.removeCaseData  = {};
              this.removeCaseIndex = '';
              // localStorage.setItem("createCovidBatchData",JSON.stringify(this.specimenTableData));
              // this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              //   // Destroy the table first
              //   dtInstance.destroy();
              //   // Call the dtTrigger to rerender again
              //   this.dtTrigger.next();
              // });
              this.populateBatchTray(this.specimenTableData);
            }
            else{
              $('#removeModal').modal('hide');
              this.notifier.notify("error","Error while updating case status");
              this.ngxLoader.stop();
            }
          },error=>{
            $('#removeModal').modal('hide');
            this.notifier.notify("error","Error while updating case status");
            this.ngxLoader.stop();
          })
          }




          // this.notifier.notify('success',"Case removed successfully");



        }
        else{
          $('#removeModal').modal('hide');
          this.notifier.notify('error',"Error while removing case")
          this.ngxLoader.stop();
        }
      },error=>{
        $('#removeModal').modal('hide');
        this.notifier.notify('error',"Error while removing case")
        this.ngxLoader.stop();
      })
    }
    else if (typeof this.removeCaseData['batchId'] === undefined){
      //////////////////newly added case from edit or add screen
      return;
    }


    $('#removeModal').modal('hide');

  }

  populateBatchTray(specimenTableData){
    var ag = [];
    var bg = [];
    var cg = [];
    var dg = [];
    var eg = [];
    var fg = [];
    var gg = [];
    var hg = [];
    var holder = {};
    console.log('this.specimenTableData',this.specimenTableData);

    for (let k = 0; k < this.specimenTableData.length; k++) {

        if (typeof this.specimenTableData[k]['data']['pFName'] != 'undefined') {

          this.specimenTableData[k]['data']['pFNameShort'] = this.specimenTableData[k]['data'].pFName.substring(0, 1)
        }
        if (this.specimenTableData[k]['constWell'].search('A') != -1) {
              ag.push(this.specimenTableData[k]);
        }
        else if (this.specimenTableData[k]['constWell'].search('B') != -1) {
          bg.push(this.specimenTableData[k]);
        }
        else if (this.specimenTableData[k]['constWell'].search('C') != -1) {
          cg.push(this.specimenTableData[k]);
        }
        else if (this.specimenTableData[k]['constWell'].search('D') != -1) {
          dg.push(this.specimenTableData[k]);
        }
        else if (this.specimenTableData[k]['constWell'].search('E') != -1) {
          eg.push(this.specimenTableData[k]);
        }
        else if (this.specimenTableData[k]['constWell'].search('F') != -1) {
        fg.push(this.specimenTableData[k]);
        }
        else if (this.specimenTableData[k]['constWell'].search('G') != -1) {
          gg.push(this.specimenTableData[k]);


        }
        else if (this.specimenTableData[k]['constWell'].search('H') != -1) {
          hg.push(this.specimenTableData[k]);

        }

      // }
      // if (typeof allData['data'][f] != "undefined") {
      //   // var aname = allData['data'][f].pFName;
      //   allData['data'][f].pFNameShort = allData['data'][f].pFName.substring(0, 1)
      //   if (allData['data'][f].sellectedWellName.search('A') != -1) {
      //     this.aGrid[f] = allData['data'][f];
      //   }
      //   else if (allData['data'][f].sellectedWellName.search('B') != -1) {
      //     this.bGrid[f] = allData['data'][f];
      //   }
      //   else if (allData['data'][f].sellectedWellName.search('C') != -1) {
      //     this.cGrid[f] = allData['data'][f];
      //   }
      //   else if (allData['data'][f].sellectedWellName.search('D') != -1) {
      //     this.dGrid[f] = allData['data'][f];
      //   }
      //   else if (allData['data'][f].sellectedWellName.search('E') != -1) {
      //     this.eGrid[f] = allData['data'][f];
      //   }
      //   else if (allData['data'][f].sellectedWellName.search('F') != -1) {
      //     this.fGrid[f] = allData['data'][f];
      //   }
      //   else if (allData['data'][f].sellectedWellName.search('G') != -1) {
      //     this.gGrid[f] = allData['data'][f];
      //
      //
      //   }
      //   else if (allData['data'][f].sellectedWellName.search('H') != -1) {
      //     this.hGrid[f] = allData['data'][f];
      //
      //   }
      // }

    }
    // console.log("ag",ag);
    // console.log("bg",bg);
    this.aGrid = ag;
    this.bGrid = bg;
    this.cGrid =cg;
    this.dGrid =dg;
    this.eGrid =eg;
    this.fGrid =fg;
    this.gGrid =gg;
    this.hGrid =hg;
    var a = {};
    a['caseNumber'] = 'NC';
    var b = {};
    b['caseNumber'] = 'PC'
    this.gGrid[11]['data'] = a;
    this.hGrid[11]['data'] = b;
    // console.log("gGrid",this.gGrid);

  }
  saveBtch(){
    this.populateUpdateBatchData().then(selectedPatient => {
    this.ngxLoader.stop();
    });
  }

  populateUpdateBatchData(){
    this.ngxLoader.start();

    return new Promise((resolve, reject) => {
      var requestData = {...this.updateBtachRequest}
      // console.log("this.selectedBatch",this.selectedBatch);

      requestData.data.batchId          = this.selectedBatch.batchDetails.batchId;
      requestData.data.batchStatusId    = this.selectedBatch.batchDetails.batchStatusId;
      requestData.data.testStatusId     = "2";

      var myDate = new Date();
      var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');
      requestData.data.updatedBy        = this.logedInUserRoles.userCode;
      requestData.data.createdBy        = this.selectedBatch.batchDetails.createdBy;
      requestData.data.updatedTimestamp = currentDate;


      // this.ngxLoader.stop(); return;
      var tempHolder = {};
      // console.log("");

      for (let i = 0; i < this.specimenTableData.length; i++) {
        if(typeof this.specimenTableData[i]['data']['caseId'] != 'undefined'){
          console.log("this.specimenTableData[i]",this.specimenTableData[i]);

        // console.log("this.specimenTableData[i]['data']['sellectedWell']",this.specimenTableData[i]['data']['sellectedWell']);

          tempHolder['wellMachineId'] = this.specimenTableData[i]['data']['sellectedWell'];

        tempHolder['jobOrderId']    =  this.specimenTableData[i]['data']['jobOrderId'];

        requestData.data.listOfBatchJobOrderIds.push(tempHolder)
        tempHolder = {}

        // requestData.data.listOfBatchJobOrderIds.push(this.specimenTableData[i]['data']['jobOrderId']);
        // requestData.data.testIds.push(this.specimenTableData[i]['data']['testId']);
      }
      }
      console.log("requestData",requestData);
      // this.ngxLoader.stop(); return;
      var saveBatchUrl = environment.API_COVID_ENDPOINT +'updatebatch';
      this.covidService.createBatch(saveBatchUrl,requestData).subscribe(createResp=>{
        console.log('createResp',createResp);
        if (createResp['result'].codeType == 'S') {
          // this.ngxLoader.stop();
          this.notifier.notify("success","Batch changes has been saved successfully");
          localStorage.removeItem('worksheetBatchData');
          this.diasbleBack = false;
          resolve()
          // this.router.navigate(['manage-batches']);
        }
        else{
          this.notifier.notify("error","Error while updating batch");
          this.ngxLoader.stop();
          resolve()
        }

      },error=>{
        this.notifier.notify("error","Error while updating batch");
        this.ngxLoader.stop();
        resolve()
      })
      // this.ngxLoader.stop(); return;
    })

  }

  repeatTest(item,index){
    // console.log("item",item);
    // console.log("index",index);
    if (typeof item['data']['caseId'] == 'undefined') {
      this.ngxLoader.stop()
      return;
    }

    this.ngxLoader.start()
    var myDate = new Date();
    var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');
    var repeatUrl = environment.API_COVID_ENDPOINT + 'repeatcase'
    var repeatRequest = {...this.repeatTestRequest}
    repeatRequest.data.caseId = item.data.caseId;
    repeatRequest.data.previousTestStatusId = '6';
    repeatRequest.data.testStatusId = '1';
    repeatRequest.data.jobOrderId = [item.data.jobOrderId];
    repeatRequest.data.createdBy = this.selectedBatch.batchDetails.createdBy;
    repeatRequest.header.partnerCode  = this.logedInUserRoles.partnerCode;
    repeatRequest.header.userCode     = this.logedInUserRoles.userCode;
    repeatRequest.header.functionalityCode     = "TWMA-UW";
    if (this.selectedBatch.batchDetails.createdTimestamp == null) {
      repeatRequest.data.createdTimestamp = "02-11-2022 20:07:25";
    }
    else{
      repeatRequest.data.createdTimestamp = this.selectedBatch.batchDetails.createdTimestamp;
    }

    repeatRequest.data.updatedBy = this.logedInUserRoles.userCode;
    repeatRequest.data.updatedTimestamp = currentDate;
    // console.log('repeatRequest',repeatRequest);
    this.specimenTableData[index]['data'].testStatusId = 6;

    // this.ngxLoader.stop() ;return;
    this.covidService.repeatTest(repeatUrl,repeatRequest).subscribe(repeatRes=>{
      if (repeatRes['result']['codeType'] == "S") {
        this.notifier.notify("success","Status update Successfully")
        this.specimenTableData[index]['data'].testStatusId = 6;
        var specimenurl = environment.API_SPECIMEN_ENDPOINT + 'updatecasestatusin';

        var specimenreqBody={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "TWMA-UW",systemHostAddress:"",remoteUserAddress:"",dateTime:""},
        data: {
          caseStatusId:3,
          caseIds:[item.data.caseId],
          updatedBy: this.logedInUserRoles.userCode,
          updatedTimestamp:currentDate

        }
      }
      this.covidService.updateCaseStatus(specimenurl,specimenreqBody).subscribe(specimenReps=>{
        if (specimenReps['data']>0) {

        }
        else{
          this.notifier.notify("error","Error while updating case status");
          this.ngxLoader.stop();
        }
      },error=>{
        this.notifier.notify("error","Error while updating case status");
        this.ngxLoader.stop();
      })
        this.ngxLoader.stop()
      }
    },error=>{
      this.notifier.notify("error","Error while updation status")
      this.ngxLoader.stop()
    })
  }

  openSubmitModal(batchId){
    this.SelectedForSubmitId = batchId;
    $('#submitModal').modal('show');
  }

  submitToReporting(){
    this.diasbleBack = false;
    this.ngxLoader.start();
    var myDate = new Date();
    var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');

    var requestUrl = environment.API_COVID_ENDPOINT + 'submitbatch';
    var requestBody = {...this.submitToReportingRequest}
    requestBody.data.batchId = this.SelectedForSubmitId;
    requestBody.data.submittedDate = currentDate;
    requestBody.header.partnerCode = this.logedInUserRoles.partnerCode;
    requestBody.header.userCode = this.logedInUserRoles.userCode;
    requestBody.header.functionalityCode = "TWMA-UW";
    console.log('requestBody',requestBody);
    this.covidService.submitBatch(requestUrl,requestBody).subscribe(submitResponse=>{
      console.log('submitResponse',submitResponse);
      $('#submitModal').modal('hide');
      if (submitResponse['result']['codeType'] == "T") {
        this.ngxLoader.stop();
       this.notifier.notify("warning",submitResponse['result']['description'])
      }
      else{
        this.SelectedForSubmitId = '';
        this.router.navigate(['manage-batches']);
      this.ngxLoader.stop();
      this.notifier.notify("success","Batch submitted successfully")
      }



    },error=>{
      this.ngxLoader.stop();
      this.notifier.notify("error","error while submitting batch")
      $('#submitModal').modal('hide');
      this.SelectedForSubmitId = '';
    })


  }

  triggerUpload(){
    $('#resultUpload').trigger('click');
  }

  uploadResults(input,batchID){
    // console.log('input',input.files[0]);
    // console.log('batchID',batchID);
    // return;
    if (typeof input.files[0] !="undefined") {
      this.fileName = input.files[0].name;
      console.log('this.fileName.length',this.fileName.length);

      if (this.fileName.length < 120) {
        var mb = input.files[0].size/1048576;
        console.log("mb",mb);
        // if (mb <= 1) {
        if (input.files[0].type != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' && input.files[0].type != 'application/vnd.ms-excel') {
           $('#resultUpload').val('');
           alert('invalid file type');
           return;
         }
          if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = (e: any) => {
              // console.log('Got here: ', e.target.result);
              // console.log('input.files[0]: ', input.files[0]);

              this.fileBase64 = e.target.result;
              this.checkToSaveBeforeUpload(batchID,input.files[0].name,input.files[0].type,e.target.result)
              // this.uploadFIleRequest(batchID,input.files[0].name,input.files[0].type,e.target.result)

              // this.obj.photoUrl = e.target.result;
            }
            reader.readAsDataURL(input.files[0]);
          }
          // console.log('this.imageBase64: ', this.imageBase64);
          if(input.files && input.files.length > 0) {
            this.filePayLoad = input.files[0];
          }
        // }
        // else{
        //   alert('File Size must be less then 1MB');
        // }
      }
      else{
        $('#resultUpload').val('');
        alert("File name should contain less then 120 character")
      }


    }


  }
  checkToSaveBeforeUpload(batchID,name,type,base64){
    this.ngxLoader.start();
    var oldData = JSON.parse(localStorage.getItem('worksheetBatchData'));
    if (oldData != null) {
      this.populateUpdateBatchData().then(selectedPatient => {
        this.uploadFIleRequest(batchID,name,type,base64);
      // this.ngxLoader.stop();
      });
    }
    else{
      this.uploadFIleRequest(batchID,name,type,base64);
    }
  }
  uploadFIleRequest(batchID,name,type,base64){
    // console.log('batchID',batchID);
    // console.log('name',name);
    // console.log('type',type);
   // console.log('base64',base64);
   var myDate = new Date();
   var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');

   var a = name.split('.');
   var s = a.length-1;
   var n = a[s];
   // console.log("n",n);
   var uploadURL = environment.API_COVID_ENDPOINT + "importinterpretations";
   var uploadRequest = {...this.uploadResultsRequest}
   uploadRequest.data.batchId = batchID;
   uploadRequest.data.fileName = name;
   uploadRequest.data.fileExtension = '.'+n;
   uploadRequest.data.base64EncodedFile = base64;
   uploadRequest.data.updatedDate = currentDate;
   uploadRequest.header.partnerCode = this.logedInUserRoles.partnerCode;
   uploadRequest.header.userCode    = this.logedInUserRoles.userCode;
   uploadRequest.header.functionalityCode = "TWMA-MBC";

   console.log("uploadRequest",uploadRequest);

   this.covidService.uploadResults(uploadURL,uploadRequest).subscribe(uploadResponse=>{
     console.log('uploadResponse',uploadResponse);
     // return;
     if (uploadResponse['result'].codeType == "S") {
       if (typeof uploadResponse['data'].failedAtRows != undefined) {
         if (uploadResponse['data'].failedAtRows != '') {
           this.notifier.notify("error",uploadResponse['data'].failedAtRows)
         }
         else{
           this.notifier.notify("success","Results uploaded successfully")
         }
       }
       $('#resultUpload').unbind();
       const downloadLink = document.createElement("a");
       var fileName= fileName;

       downloadLink.href = uploadResponse['data']['base64EncodedFile'];
       downloadLink.download = uploadRequest.data.fileName;
       downloadLink.click();
       // window.open(uploadResponse['data']['base64EncodedFile'], "_blank");
       var selectURL = environment.API_COVID_ENDPOINT + 'viewbatch';
       var getBatchReq = {...this.viewBatchRequest}
       getBatchReq.data.batchId = batchID;



       location.reload();

       // this.getSelectedRecord(selectURL,getBatchReq).then(selectedPatient => {
       //   // this.ngxLoader.stop();
       //   // this.poplateTableWithPredefinedRecord(selectURL,getBatchReq)
       //
       // });
       // this.poplateTableWithPredefinedRecord(selectURL,getBatchReq).then(resetRec => {
       //   this.getSelectedRecord(selectURL,getBatchReq).then(selectedPatient => {
       //
       //   });
       //
       // });

     $('#resultUpload').val('');
     }
     else{
       // if (uploadResponse['result'].description == "All cases are from different batch") {
       //   this.notifier.notify("error",uploadResponse['result'].description)
       //
       // }
       // else{
       //   this.notifier.notify("error","error while uploading results")
       // }
       this.notifier.notify("error",uploadResponse['result'].description)
       this.ngxLoader.stop();
       $('#resultUpload').val('');
     }


   },error=>{
     this.ngxLoader.stop();
     this.notifier.notify("error","error while uploading results")

   })

  }

  downloadResults(batchId){
    this.ngxLoader.start();
    var downloadUrl = environment.API_COVID_ENDPOINT +'batchresultfiles';
    var downlaodReq = {...this.downloadResultsRequest}
    downlaodReq.data.batchId = batchId;
    downlaodReq.header.partnerCode = this.logedInUserRoles.partnerCode;
    downlaodReq.header.userCode = this.logedInUserRoles.userCode;
    // downlaodReq.header.functionalityCode = "TWMA-UW";
   this.covidService.downloadResults(downloadUrl,downlaodReq).subscribe(downloadResp=>{
      console.log('downloadResp',downloadResp);
      if (downloadResp['result'].codeType == "S") {
        this.ngxLoader.stop();
       // window.open(downloadResp['data']['base64EncodedOriginalFile'], "_blank");
       var a = downloadResp['data']['base64EncodedOriginalFile'].split(';')
       var filename = ''
       console.log("a",a);

       if (a[0].indexOf('spreadsheetml.sheet') !== -1) {
         filename  = 'result.xlsx';
       }
       else if (a[0].indexOf('zip') !== -1) {
         filename  = 'result.zip';
       }
       else{
         filename  = 'result.csv';
       }
       const downloadLink = document.createElement("a");

       downloadLink.href = downloadResp['data']['base64EncodedOriginalFile'];
       downloadLink.download = filename;
       downloadLink.click();
     }
     else{
       this.ngxLoader.stop();
       this.notifier.notify( "error", downloadResp['result'].description);
     }

    },error=>{
      this.ngxLoader.stop();
      this.notifier.notify("error","error while downloading results")

    })
  }

  exportToExcel(batchId, batchStatus){
    this.ngxLoader.start();
    var downloadUrl = environment.API_COVID_ENDPOINT +'exportbatch';
    var downlaodReq = {...this.exportExcelRequest}
    downlaodReq.data.batchId = batchId;
    if (batchStatus == 1) {
      downlaodReq.data.batchActive = true
    }
    else{
      downlaodReq.data.batchActive = false;
    }
    downlaodReq.header.partnerCode = 'sip';
    // downlaodReq.header.partnerCode = this.logedInUserRoles.partnerCode;
    downlaodReq.header.userCode = this.logedInUserRoles.userCode;
    downlaodReq.header.functionalityCode = "TWMA-MBC";
    this.covidService.exportToExcel(downloadUrl,downlaodReq).subscribe(exportResp=>{
      console.log('exportResp',exportResp);
     //  if (typeof exportResp['result'].codeType == "undefined") {
     //    this.ngxLoader.stop();
     //    this.notifier.notify( "error", "internal serve error");
     //    return;
     // }
      if (exportResp['result'].codeType == "S") {
        this.ngxLoader.stop();
        const downloadLink = document.createElement("a");

        downloadLink.href = exportResp['data']['base64EncodedBatch'];
        downloadLink.download = exportResp['data']['fileName'];
        downloadLink.click();
       // window.open(exportResp['data']['base64EncodedBatch'], "_blank");
     }
     else{
       this.ngxLoader.stop();
       this.notifier.notify( "error", exportResp['result'].description);
     }


    },error=>{
      this.ngxLoader.stop();
      this.notifier.notify("error","error while downloading file")

    })
  }

  printPDF(batchId, batchStatus){
    this.ngxLoader.start();
    var downloadUrl = environment.API_COVID_ENDPOINT +'printworksheet';
    var downlaodReq = {...this.printPDFRequest}
    downlaodReq.data.batchId = batchId;
    if (batchStatus == 1) {
      downlaodReq.data.batchActive = true
    }
    else{
      downlaodReq.data.batchActive = true;
    }

    downlaodReq.header.partnerCode = 'sip';
    // downlaodReq.header.partnerCode = this.logedInUserRoles.partnerCode;
    downlaodReq.header.userCode = this.logedInUserRoles.userCode;
    // downlaodReq.header.functionalityCode = "TWMA-UW";
    this.covidService.printPDF(downloadUrl,downlaodReq).subscribe(exportResp=>{
      console.log('printResp',exportResp);
      if (exportResp['result'].codeType == "S") {
        this.ngxLoader.stop();
       // window.open(exportResp['data']['base64EncodedOriginalFile'], "_blank");
       // const downloadLink = document.createElement("a");
       // const fileName = "result.pdf";
       // var att = document.createAttribute("target");       // Create a "class" attribute
       // att.value = "_blank";
       //
       // downloadLink.href = exportResp['data']['base64EncodedOriginalFile'];
       // downloadLink.download = fileName;
       // downloadLink.click();

       // var winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,'+
       //       'resizable,screenX=50,screenY=50,width=850,height=1050';
       //
       //   var htmlPop = '<embed width=100% height=100%'
       //                    + ' type="application/pdf"'
       //                    + ' src="'
       //                    + escape(exportResp['data']['base64EncodedOriginalFile'])
       //                    + '"></embed>';
       //
       //   var printWindow = window.open ("", "PDF", winparams);
       //   printWindow.document.write (htmlPop);
       //   printWindow.print();

       let pdfWindow = window.open("")
       pdfWindow.document.write(
         "<iframe width='100%' height='100%' src='"+encodeURI(exportResp['data']['base64EncodedOriginalFile'])+"'></iframe>"
       )
     }
     else{
       this.ngxLoader.stop();
       this.notifier.notify( "error", exportResp['result'].description);
     }


    },error=>{
      this.ngxLoader.stop();
      this.notifier.notify("error","error while downloading file")

    })
  }



    getLookups(){
      // this.ngxLoader.start();
      return new Promise((resolve, reject) => {
        var wellNo = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=BATCHING_WELL_MACHINE_CACHE&partnerCode=sip').then(response =>{
          return response;
        })
        var interpretation = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=TEST_INTERPRETATION_CACHE&partnerCode=sip').then(response =>{
          return response;
        })
        var testStatus = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=TEST_STATUS_CACHE&partnerCode=sip').then(response =>{
        return response;
        })
        var interpretationStatus = this.globalService.getLookUps(environment.API_LOOKUP_ENDPOINT+'getlookup?lookupCache=INTERPRETATION_STATUS_CACHE&partnerCode=sip').then(response =>{
        return response;
        })


        forkJoin([wellNo,interpretation,testStatus,interpretationStatus]).subscribe(allLookups => {
          console.log("allLookups",allLookups);
          this.constantWelNo      = this.sortPipe.transform(allLookups[0], "asc", "wellNumber");
          this.allInterpretations = this.sortPipe.transform(allLookups[1], "asc", "value");
          // console.log("this.constantWelNo",this.constantWelNo);
          this.testStatusLookup      = allLookups[2];
          this.allInterpretationStatus      = allLookups[3];

          this.allWelNo      = [...this.constantWelNo];
          for (let i = 0; i < this.allWelNo.length; i++) {
            this.allWelNo[i].selected = false;
            this.allWelNo[i].toolTip = "";
          }
          // if (this.pageType == 'add') {
          //   var oldData = JSON.parse(localStorage.getItem('createCovidBatchData'));
          //   if (oldData != null) {
          //     this.specimenTableData = oldData;
          //     this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          //       // Destroy the table first
          //       dtInstance.destroy();
          //       // Call the dtTrigger to rerender again
          //       this.dtTrigger.next();
          //     });
          //
          //   }
          // }
          resolve();

        })
      })

    }
  //   onDrop(event: CdkDragDrop<string[]>) {
  //     console.log('event.previousIndex',event.previousIndex);
  //     console.log('event.currentIndex',event.currentIndex);
  //     console.log('event',event);
  //
  //   moveItemInArray(this.specimenTableData, event.previousIndex, event.currentIndex);
  //   this.specimenTableData.forEach((user, idx) => {
  //     user.order = idx + 1;
  //   });
  // }
   // array_move(arr, old_index, new_index) {
   //  return new Promise((resolve, reject) =>{
   //    if (new_index >= arr.length) {
   //        var k = new_index - arr.length + 1;
   //        while (k--) {
   //            arr.push(undefined);
   //        }
   //    }
   //    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
   //    console.log("arr",arr);
   //    resolve()
   //  })
   //
   //  }


    // return arr; // for testing

    ctrlDown(e) {
      console.log('e.ctrlKey',e);
        this.controlPressed = e;


    }
    ctrlUp(e) {
      this.controlPressed = false;

    }
    onDrop(event: CdkDragDrop<string[]>) {

      console.log('controlPressed',this.controlPressed);


      // if (e.ctrlKey == true) {
      //   console.log('CTRL');
      //   return;
      // }
      // console.log('event.previousIndex',event.previousIndex);
    console.log('event',event);
    if (event.currentIndex >= 94) {
      return;
    }
    // console.log('from',this.specimenTableData[event.previousIndex].constWell);
    // console.log('to',this.specimenTableData[event.currentIndex].constWell);

  moveItemInArray(this.specimenTableData, event.previousIndex, event.currentIndex);
  // var moveresp = this.array_move(this.specimenTableData, event.previousIndex, event.currentIndex);


  //////////// to keep assigned well same
  this.specimenTableData.forEach((item, idx) => {
    item.order = idx + 1;
    // for (let i = 0; i < this.allWelNo.length; i++) {
    //   if (this.allWelNo[i]['wellNumber'] == item.constWell) {
    //     item.data.wellMachineId = this.allWelNo[i]['wellMachineId'];
    //     item.data.sellectedWell = this.allWelNo[i]['wellMachineId'];
    //   }
    // }


  });
  //
  // // console.log("this.specimenTableDataforDragDropCheck",this.specimenTableDataforDragDropCheck['length']);
  var array = [...this.specimenTableDataforDragDropCheck]
  // console.log("array",array);
  //
  for (let j = 0; j < this.specimenTableData.length; j++) {
    var well = array[j];
    this.specimenTableData[j].constWell = well;
    for (let i = 0; i < this.allWelNo.length; i++) {
      if (this.allWelNo[i]['wellNumber'] == well) {
        this.specimenTableData[j].data.wellMachineId = this.allWelNo[i]['wellMachineId'];
        this.specimenTableData[j].data.sellectedWell = this.allWelNo[i]['wellMachineId'];
      }
    }
     // this.specimenTableData.splice(j, 0, this.specimenTableData.splice(old_index, 1)[0]);
  }
  localStorage.setItem('worksheetBatchData',JSON.stringify(this.specimenTableData));
  this.diasbleBack = true;
  this.populateBatchTray(this.specimenTableData);


}

showCancelModal(item){
  this.testBatchToCancel = item;
  $('#cancelModal').modal('show');


}

cancelBatch(){
  localStorage.removeItem('worksheetBatchData');
  this.diasbleBack = false;
  var myDate = new Date();
  var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');

 // console.log('this.testBatchToCancel',this.testBatchToCancel);
 var selectURL = environment.API_COVID_ENDPOINT + 'viewbatch';
 var getBatchReq = {...this.viewBatchRequest}
 getBatchReq.header.partnerCode  = this.logedInUserRoles.partnerCode;
 getBatchReq.header.userCode     = this.logedInUserRoles.userCode;
 getBatchReq.header.functionalityCode     = "TWMA-MBC";
 getBatchReq.data.batchId = this.testBatchToCancel['batchDetails']['batchId'];
 this.covidService.getBatchById(selectURL, getBatchReq).subscribe(getByIdResp=>{
   console.log("getByIdResp",getByIdResp);
   if (getByIdResp['result']['codeType'] == 'T') {
     if (getByIdResp['result']['description'] == "NO RECORD FOUND") {
       var cancelUrl = environment.API_COVID_ENDPOINT + 'cancelbatch';
       var canceBatchReq = {...this.cancelBatchRequest};
       canceBatchReq.data.batchId = this.testBatchToCancel['batchDetails']['batchId'];
       canceBatchReq.data.batchStatusId = '4';
       canceBatchReq.data.testStatusId = '1';
       canceBatchReq.data.updatedBy = this.logedInUserRoles.userCode;
       canceBatchReq.data.updatedTimestamp = currentDate;
       canceBatchReq.header.partnerCode  = this.logedInUserRoles.partnerCode;
       canceBatchReq.header.userCode     = this.logedInUserRoles.userCode;
       canceBatchReq.header.functionalityCode     = "TWMA-UW";
       this.covidService.cancelBatch(cancelUrl,canceBatchReq).subscribe(cancelResp=>{
         console.log("cancelResp",cancelResp);
         $('#cancelModal').modal('hide');

         if (cancelResp['result']['codeType'] == "S") {
           this.ngxLoader.stop();
           this.notifier.notify("success","Batch cancelled successfully");
           this.router.navigate(['manage-batches']);

         //   this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
         //   dtElement.dtInstance.then((dtInstance: any) => {
         //     dtInstance.destroy();
         //   })
         // })
         this.dtTrigger.next();
           // this.dtTrigger.next();

           $('#cancelModal').modal('show');
           this.testBatchToCancel = {};
         }
       },error=>{
         this.ngxLoader.stop();
         this.notifier.notify("error","Error while canceling Batch");
       })
     }
     else{
       this.notifier.notify("error","No Record Found");
       this.ngxLoader.stop();
     }

   }
   else{
     var uniqeCaseIds = [];
     for (let i = 0; i < getByIdResp['data']['jobOrderDetails'].length; i++) {
       uniqeCaseIds.push(getByIdResp['data']['jobOrderDetails'][i].caseId)
     }
       console.log('uniqeCaseIds',uniqeCaseIds);
       var specimenurl = environment.API_SPECIMEN_ENDPOINT + 'updatecasestatusin';

       var specimenreqBody={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "TWMA-UW",systemHostAddress:"",remoteUserAddress:"",dateTime:""},
       data: {
         caseStatusId:3,
         caseIds:uniqeCaseIds,
         updatedBy: this.logedInUserRoles.userCode,
         updatedTimestamp:currentDate

       }
     }
     // console.log("specimenreqBody",specimenreqBody);
// return;
     this.covidService.updateCaseStatus(specimenurl,specimenreqBody).subscribe(specimenReps=>{
       if (specimenReps['data']>0) {
         var cancelUrl = environment.API_COVID_ENDPOINT + 'cancelbatch';
         var canceBatchReq = {...this.cancelBatchRequest};
         canceBatchReq.data.batchId = this.testBatchToCancel['batchDetails']['batchId'];
         canceBatchReq.data.batchStatusId = '4';
         canceBatchReq.data.testStatusId = '1';
         canceBatchReq.data.updatedBy = this.logedInUserRoles.userCode;
         canceBatchReq.data.updatedTimestamp = currentDate;
         canceBatchReq.header.partnerCode  = this.logedInUserRoles.partnerCode;
         canceBatchReq.header.userCode     = this.logedInUserRoles.userCode;
         canceBatchReq.header.functionalityCode     = "TWMA-UW";
         this.covidService.cancelBatch(cancelUrl,canceBatchReq).subscribe(cancelResp=>{
           console.log("cancelResp",cancelResp);
           $('#cancelModal').modal('hide');

           if (cancelResp['data'] == true) {
             this.ngxLoader.stop();
             this.notifier.notify("success","Batch cancelled successfully");
             this.router.navigate(['manage-batches']);

           //   this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
           //   dtElement.dtInstance.then((dtInstance: any) => {
           //     dtInstance.destroy();
           //   })
           // })
           this.dtTrigger.next();
             // this.dtTrigger.next();

             $('#cancelModal').modal('show');
             this.testBatchToCancel = {};
           }
         },error=>{
           this.ngxLoader.stop();
           this.notifier.notify("error","Error while canceling Batch");
         })
         // this.ngxLoader.stop();
         // this.notifier.notify("success","Batch removed successfully");

         // this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
         //   // Destroy the table first
         //   dtInstance.destroy();
         //   // Call the dtTrigger to rerender again
         //   this.dtTrigger.next();
         // });
       }
       else{
         this.notifier.notify("error","Error while updating case status");
         this.ngxLoader.stop();
       }
     },error=>{
       this.notifier.notify("error","Error while updating case status");
       this.ngxLoader.stop();
     })


     // console.log("getByIdResp",getByIdResp);




   }
 },error=>{
   this.ngxLoader.stop();
   this.notifier.notify("error","Error while getting Batch");
 })


}

poplateTableWithPredefinedRecord(selectURL,getBatchReq){
  return new Promise((resolve, reject) => {
    var flag = 0;
    var num =1
    // var count = 1;
    var tempHold = {};
    var temp     = [];

    for (let i = 0; i<96; i++) {
      for (let j = flag; j < 8; j++) {

          if (j == 0) {
            var name = '';
            if (num <10) { name = 'A0' }else{name = 'A'}
            tempHold['constWell'] = name+(num);
            temp.push(tempHold);
            tempHold = {}
             flag = flag+1;break;
          }
          if (j == 1) {
            if (num <10) { name = 'B0' }else{name = 'B'}
            tempHold['constWell'] = name+(num);
            temp.push(tempHold);
            tempHold = {}
             flag = flag+1;break;
          }
          if (j == 2) {
            if (num <10) { name = 'C0' }else{name = 'C'}
            tempHold['constWell'] = name+(num);
            temp.push(tempHold);
            tempHold = {}
             flag = flag+1;break;
          }
          if (j == 3) {
            if (num <10) { name = 'D0' }else{name = 'D'}
            tempHold['constWell'] = name+(num);
            temp.push(tempHold);
            tempHold = {}
             flag = flag+1;break;
          }
          if (j == 4) {
            if (num <10) { name = 'E0' }else{name = 'E'}
            tempHold['constWell'] = name+(num);
            temp.push(tempHold);
            tempHold = {}
             flag = flag+1;break;
          }
          if (j == 5) {
            if (num <10) { name = 'F0' }else{name = 'F'}
            tempHold['constWell'] = name+(num);
            temp.push(tempHold);
            tempHold = {}
             flag = flag+1;break;
          }
          if (j == 6) {
            if (num <10) { name = 'G0' }else{name = 'G'}
            tempHold['constWell'] = name+(num);
            temp.push(tempHold);
            tempHold = {}
             flag = flag+1;break;
          }
          if (j == 7) {
            if (num <10) { name = 'H0' }else{name = 'H'}
            tempHold['constWell'] = name+(num);
            temp.push(tempHold);
            tempHold = {}
            flag = 0;
            num = num+  1;
            break;
          }

      }

    }


    // console.log("temp",temp);
    this.specimenTableData = [...temp];
    for (let index = 0; index < temp.length; index++) {
      this.specimenTableDataforDragDropCheck.push(temp[index]['constWell'])
    }
    resolve();
  })

  // this.getSelectedRecord(selectURL,getBatchReq).then(selectedPatient => {
  //   // this.ngxLoader.stop();
  // });
}
backTomanage(){
  this.diasbleBack = false;
  this.router.navigate(['manage-batches']);
}

}
