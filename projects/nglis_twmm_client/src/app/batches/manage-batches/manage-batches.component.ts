import { Component, OnInit, ElementRef, ViewChild, QueryList, ViewChildren } from '@angular/core';
import { environment } from '../../../../../../src/environments/environment';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NotifierService } from 'angular-notifier';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { GlobalApiCallsService } from '../../services/global/global-api-calls.service';
import { CovidApiCallsService } from '../../services/covid/covid-api-calls.service';
import { concat, Observable, of, Subject, forkJoin } from "rxjs";
import { SortPipe } from "../../pipes/sort.pipe";
import { DataTableDirective } from 'angular-datatables';
import { DatePipe } from '@angular/common';
import { saveAs } from 'file-saver';
declare var $ :any;
import { GlobalySharedModule } from './../../../../../../src/app/sharedmodules/globaly-shared/globaly-shared.module';
import { UrlGuard } from './../../../../../../src/services/gaurd/url.guard'
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-manage-batches',
  templateUrl: './manage-batches.component.html',
  styleUrls: ['./manage-batches.component.css'],
  host        : {
    class     :'mainComponentStyle'
  }
})
export class ManageBatchesComponent implements OnInit {
  @ViewChildren(DataTableDirective)

  dtElement              : QueryList<any>;
  public dtOptions       : DataTables.Settings[] = [];
  // public dtOptions2      : DataTables.Settings[] = [];
  // public dtOptions3      : DataTables.Settings[] = [];

  dtTrigger              : Subject<ManageBatchesComponent> = new Subject();
  // dtTrigger2             : Subject<ManageBatchesComponent> = new Subject();
  // dtTrigger3             : Subject<ManageBatchesComponent> = new Subject();
  public paginatePage    : any = 0;
  public paginateLength  = 20;
  activeCasesCount   ;
  oldCharacterCount      = 0;

  public paginatePage2    : any = 0;
  public paginateLength2  = 20;
  submitCasesCount   ;
  oldCharacterCount2      = 0;

  public paginatePage3    : any = 0;
  public paginateLength3  = 20;
  cancelledCasesCount  ;
  oldCharacterCount3      = 0;
  updateBatchAction = false;

  public covidCaseRequest: any = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"TWMM",
      functionalityCode: "TWMA-MBC",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data                : {
      page:"0",
      size:"20",
      sortColumn:"createdTimestamp",
      sortingOrder:"desc",
      batchStatusId:"1"

    }
  }
  public covidCaseRequestNew: any = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"TWMM",
      functionalityCode: "TWMA-MBC",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data                : {
      // pageNumber:"0",
      // pageSize:"20",
      // sortColumn:"createdTimestamp",
      // sortType:"desc",
      // batchStatus:"1"

    }
  }
  public usersRequest: any = {
    header              : {
      uuid              : "",
      partnerCode       : "",
      userCode          : "",
      referenceNumber   : "",
      systemCode        : "",
      moduleCode        : "",
      functionalityCode: "TWMA-MBC",
      systemHostAddress : "",
      remoteUserAddress : "",
      dateTime          : ""
    },
    data                : {
      userCodes           :[]
    }
  }
  covidAllActiveData : any = [];
  covidSubmittedData : any = [];
  covidCancelledData : any = [];
  public submitBatchRequest: any = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"TWMM",
      functionalityCode: "TWMA-MBC",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data                : {
      page:"0",
      size:"20",
      sortColumn:"createdTimestamp",
      sortingOrder:"desc",
      batchStatusId:"1"

    }
  }

  public searchBatchRequest: any = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"TWMM",
      functionalityCode: "TWMA-MBC",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data                : {
      page:"0",
      size:"20",
      sortColumn:"createdTimestamp",
      sortingOrder:"desc",
      caseId:"",
      searchString:"",
      // batchId:""
      batchStatusId:"1"


    }
  }
  testBatchToCancel = {};
  public viewBatchRequest = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"TWMM",
      functionalityCode: "TWMA-MBC",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      page:"0",
      size:"3",
      sortColumn:"batchId",
      sortingOrder:"desc",
      batchId:""
    }
  }

  public cancelBatchRequest = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"TWMM",
      functionalityCode: "TWMA-MBC",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      batchId:"7",
      batchStatusId:"2",
      testStatusId:"3",
      updatedBy:"abc",
      updatedTimestamp: "30-11-2020 20:07:25"

    }
  }
  public searchByCaseNumRequest = {
    header:{
      uuid:"",
      partnerCode:"",
      userCode:"",
      referenceNumber:"",
      systemCode:"",
      moduleCode:"SPCM",
      functionalityCode: "TWMA-MBC",
      systemHostAddress:"",
      remoteUserAddress:"",
      dateTime:""
    },
    data:{
      page:"",
      size:"",
      sortColumn:"",
      sortingOrder:"asc",
      searchDate:"",
      caseNumber:"OX20-6",
      caseStatus:"3"
    }
  }
  public submitToReportingRequest = {
    header:{
      createdBy: null,
      createdTimestamp: null,
      updatedBy: null,
      updatedTimestamp: null,
      uuid: "67ffaebe-6188-476e-86ae-c5deb2effa80",
      partnerCode: "1",
      userCode: "sip-12588",
      referenceNumber: "TWMM12032020115628",
      systemCode: "NGLIS",
      moduleCode: "TWMM",
      functionalityCode: "TWMA-UR",
      systemHostAddress: "0:0:0:0:0:0:0:1",
      remoteUserAddress: "0:0:0:0:0:0:0:1",
      dateTime: "12/03/2020 11:56:28"
    },
    data:{
      batchId:"",
      submittedDate:''
    }
  }
  SelectedForSubmitId = "";
  public uploadResultsRequest = {
    header:{
      createdBy: null,
      createdTimestamp: null,
      updatedBy: null,
      updatedTimestamp: null,
      uuid: "67ffaebe-6188-476e-86ae-c5deb2effa80",
      partnerCode: "1",
      userCode: "sip-12588",
      referenceNumber: "TWMM12032020115628",
      systemCode: "NGLIS",
      moduleCode: "TWMM",
      functionalityCode: "TWMA-UR",
      systemHostAddress: "0:0:0:0:0:0:0:1",
      remoteUserAddress: "0:0:0:0:0:0:0:1",
      dateTime: "12/03/2020 11:56:28"
    },
    data:{
      batchId:"23423",
      fileName:"COVID-19_ results_sample_V0.1.xlsx",
      fileExtension:".xlsx",
      base64EncodedFile:"",
      updatedDate : ''
    }
  }
  fileName = "";
  fileBase64 ;
  filePayLoad ;

  public downloadResultsRequest = {
    header:{
      createdBy: null,
      createdTimestamp: null,
      updatedBy: null,
      updatedTimestamp: null,
      uuid: "67ffaebe-6188-476e-86ae-c5deb2effa80",
      partnerCode: "1",
      userCode: "sip-12588",
      referenceNumber: "TWMM12032020115628",
      systemCode: "NGLIS",
      moduleCode: "TWMM",
      functionalityCode: "TWMA-UR",
      systemHostAddress: "0:0:0:0:0:0:0:1",
      remoteUserAddress: "0:0:0:0:0:0:0:1",
      dateTime: "12/03/2020 11:56:28"
    },
    data:{
      batchId:"23423"
    }
  }

  public searchUserRequest = {
    header:{
      createdBy: null,
      createdTimestamp: null,
      updatedBy: null,
      updatedTimestamp: null,
      uuid: "67ffaebe-6188-476e-86ae-c5deb2effa80",
      partnerCode: "1",
      userCode: "sip-12588",
      referenceNumber: "TWMM12032020115628",
      systemCode: "NGLIS",
      moduleCode: "USRM",
      functionalityCode: "TWMA-UR",
      systemHostAddress: "0:0:0:0:0:0:0:1",
      remoteUserAddress: "0:0:0:0:0:0:0:1",
      dateTime: "12/03/2020 11:56:28"
    },
    data:{
      partnerCode:1,
      searchQuery:""
    }
  }

  public exportExcelRequest = {
    header:{
      createdBy: null,
      createdTimestamp: null,
      updatedBy: null,
      updatedTimestamp: null,
      uuid: "",
      partnerCode: "1",
      userCode: "sip-12588",
      referenceNumber: "",
      systemCode: "NGLIS",
      moduleCode: "TWMM",
      functionalityCode: "TWMA-MBC",
      systemHostAddress: "",
      remoteUserAddress: "",
      dateTime: ""
    },
    data:{
      batchId:"23423",
      batchActive: true
    }
  }
  public showbatchesbyusersRequest = {
    header:{
      createdBy: null,
      createdTimestamp: null,
      updatedBy: null,
      updatedTimestamp: null,
      uuid: "",
      partnerCode: "1",
      userCode: "sip-12588",
      referenceNumber: "",
      systemCode: "NGLIS",
      moduleCode: "TWMM",
      functionalityCode: "TWMA-MBC",
      systemHostAddress: "",
      remoteUserAddress: "",
      dateTime: ""
    },
    data:{
      page:"0",
      size:"20",
      sortColumn:"createdTimestamp",
      sortingOrder:"desc",
      batchStatusId:"1",
      userCodes:["sip-125982","sip-12",]

    }
  }

  public printPDFRequest = {
    header:{
      createdBy: null,
      createdTimestamp: null,
      updatedBy: null,
      updatedTimestamp: null,
      uuid: "",
      partnerCode: "1",
      userCode: "sip-12588",
      referenceNumber: "",
      systemCode: "NGLIS",
      moduleCode: "TWMM",
      functionalityCode: "TWMA-MBC",
      systemHostAddress: "",
      remoteUserAddress: "",
      dateTime: ""
    },
    data:{
      batchId:"23423",
      batchActive: true
    }
  }

  public logedInUserRoles :any = {};
  constructor(
    private formBuilder     : FormBuilder,
    private router          : Router,
    private http            : HttpClient,
    private notifier        : NotifierService,
    private ngxLoader       : NgxUiLoaderService,
    private globalService   : GlobalApiCallsService,
    private covidService   : CovidApiCallsService,
    private sortPipe        : SortPipe,
    private datePipe: DatePipe,
    private rbac         : UrlGuard
  ) {

    this.rbac.checkROle('TWMA-MBC');
    this.notifier        = notifier;
  }

  ngOnInit(): void {
    this.ngxLoader.stop();
    let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
var originalText = bts.toString(CryptoJS.enc.Utf8);
this.logedInUserRoles = JSON.parse(originalText)

    if(this.logedInUserRoles['allowedRoles'].indexOf("TWMA-UB") != -1){
      this.updateBatchAction = true;

    }
    var batchUrl = environment.API_ACDM_ENDPOINT + "allbatches";
    this.dtOptions[0] = {
      pagingType   : 'full_numbers',
      pageLength   : this.paginateLength,
      serverSide   : true,
      processing   : true,
      // ordering     : false,
      "order": [[ 1, "desc" ]],
      columnDefs: [
        { orderable: true, className: 'reorder', targets: [0,1,2,3,4, 'desc'] },
        { orderable: false, targets: ['_all'] }
      ],
      // searching    : true,
      "lengthMenu": [20, 50, 75, 100 ],
      // 'columns': [
      //    { data: 'batchId' },
      //    { data: 'createdBy' },
      //    { data: 'updatedBy' },
      //    // { data: 'salary' },
      //    // { data: 'city' },
      // ]

      ajax: (dataTablesParameters: any, callback) => {
        this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {

          dtElement.dtInstance.then((dtInstance: any) => {
            if(dtInstance.table().node().id == 'covid19-active-batch'){
              this.paginatePage           = dtElement['dt'].page.info().page;
              this.paginateLength         = dtElement['dt'].page.len();
              var sortColumn = dataTablesParameters.order[0]['column'];
              var sortOrder  = dataTablesParameters.order[0]['dir'];
              var sortArray  = ['batchId','batchCreationDate','batchCreatedBy','batchUpdationDate','batchUpdatedBy','numberOfCases','batchId']
              var manageRequest = {...this.covidCaseRequestNew};
              if (dataTablesParameters.search.value != "" && (this.oldCharacterCount == 0 || this.oldCharacterCount == 3)) {
                if (this.oldCharacterCount == 3) {
                }
                manageRequest['data']['mainSearch'] = dataTablesParameters.search.value;

              }
              else{

                  $('.dataTables_processing').css('display',"none");
                  if (this.oldCharacterCount == 3) {
                    this.oldCharacterCount     = 3

                  }
                  else{
                    this.oldCharacterCount     = 2;
                  }

              }
                this.oldCharacterCount     = 3;

                manageRequest['data']['batchStatus']= '1';
                manageRequest['data']['sortColumn']    = sortArray[sortColumn];
                manageRequest['data']['sortType']  = sortOrder;
                manageRequest['data']['pageNumber']  = dtElement['dt'].page.info().page;
                manageRequest['data']['pageSize']  = dtElement['dt'].page.len();
                manageRequest.header.partnerCode  = this.logedInUserRoles.partnerCode;
                manageRequest.header.userCode     = this.logedInUserRoles.userCode;
                manageRequest.header.functionalityCode     = "TWMA-MBC";
                this.http.put(batchUrl,manageRequest).subscribe(getActiveResp =>{
                  console.log("getActiveResp",getActiveResp);
                  if (typeof getActiveResp['message'] != 'undefined') {
                    this.notifier.notify("error",getActiveResp['message']);
                    callback({
                      recordsTotal    :  0,
                      recordsFiltered :  0,
                      data: []
                    });
                    this.covidCaseRequestNew.data = {};

                    return;
                  }
                  if (getActiveResp['data'] == null) {
                    this.notifier.notify("error",getActiveResp['result']['description']);
                    callback({
                      recordsTotal    :  0,
                      recordsFiltered :  0,
                      data: []
                    });
                    this.covidCaseRequestNew.data = {};

                    return
                  }
                  if (getActiveResp['data']['batches'] == null) {
                    callback({
                      recordsTotal    :  0,
                      recordsFiltered :  0,
                      data: []
                    });
                    this.covidCaseRequestNew.data = {};
                    return
                  }
                  else{
                    this.covidAllActiveData = getActiveResp['data']['batches'];
                    callback({
                      recordsTotal    :  getActiveResp['data']['totalBatches'],
                      recordsFiltered :  getActiveResp['data']['totalBatches'],
                      data: []
                    });
                    this.covidCaseRequestNew.data = {};
                  }

                  // return;




                },error=>{
                  this.ngxLoader.stop();
                  this.notifier.notify("error","Error while loading Active Batches");
                  this.covidCaseRequestNew.data = {};

                })


            }
          })
        })
        // if(typeof this.dtElement['dt'] != 'undefined'){
        //       this.paginatePage           = this.dtElement['dt'].page.info().page;
        //       this.paginateLength         = this.dtElement['dt'].page.len();
        //   }

      }
    };


    this.dtOptions[1] = {
      pagingType   : 'full_numbers',
      pageLength   : this.paginateLength2,
      serverSide   : true,
      processing   : true,
      // ordering     : false,
      "order": [[ 1, "desc" ]],
      columnDefs: [
        { orderable: true, className: 'reorder', targets: [0,1,2,3,4, 'desc'] },
        { orderable: false, targets: ['_all'] }
      ],
      "lengthMenu": [20, 50, 75, 100 ],
      ajax: (dataTablesParameters: any, callback) => {
        this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {

          dtElement.dtInstance.then((dtInstance: any) => {
            if(dtInstance.table().node().id == 'covid19-submitted-batch'){
              this.paginatePage2          = dtElement['dt'].page.info().page;
              this.paginateLength2         = dtElement['dt'].page.len();
              var sortColumn = dataTablesParameters.order[0]['column'];
              var sortOrder  = dataTablesParameters.order[0]['dir'];
              var sortArray  = ['batchId','batchCreationDate','batchCreatedBy','batchUpdationDate','batchUpdatedBy','numberOfCases','batchId']
              var manageRequest = {...this.covidCaseRequestNew};
              if (dataTablesParameters.search.value != "" && (this.oldCharacterCount2 == 0 || this.oldCharacterCount2 == 3)) {
                if (this.oldCharacterCount2 == 3) {
                }
                manageRequest['data']['mainSearch'] = dataTablesParameters.search.value;

              }
              else{

                  $('.dataTables_processing').css('display',"none");
                  if (this.oldCharacterCount2 == 3) {
                    this.oldCharacterCount2     = 3

                  }
                  else{
                    this.oldCharacterCount2     = 2;
                  }

              }
                this.oldCharacterCount2     = 3;

                manageRequest['data']['batchStatus']= '2';
                manageRequest['data']['sortColumn']    = sortArray[sortColumn];
                manageRequest['data']['sortType']  = sortOrder;
                manageRequest['data']['pageNumber']  = dtElement['dt'].page.info().page;
                manageRequest['data']['pageSize']  = dtElement['dt'].page.len();
                manageRequest.header.partnerCode  = this.logedInUserRoles.partnerCode;
                manageRequest.header.userCode     = this.logedInUserRoles.userCode;
                manageRequest.header.functionalityCode     = "TWMA-MBC";
                this.http.put(batchUrl,manageRequest).subscribe(submittedResp =>{
                  console.log("submittedResp",submittedResp);
                  if (typeof submittedResp['message'] != 'undefined') {
                    this.notifier.notify("error",submittedResp['message']);
                    callback({
                      recordsTotal    :  0,
                      recordsFiltered :  0,
                      data: []
                    });
                    this.covidCaseRequestNew.data = {};

                    return;
                  }
                  if (submittedResp['data'] == null) {
                    this.notifier.notify("error",submittedResp['result']['description']);
                    callback({
                      recordsTotal    :  0,
                      recordsFiltered :  0,
                      data: []
                    });
                    this.covidCaseRequestNew.data = {};

                    return
                  }
                  if (submittedResp['data']['batches'] == null) {
                    callback({
                      recordsTotal    :  0,
                      recordsFiltered :  0,
                      data: []
                    });
                    this.covidCaseRequestNew.data = {};
                    return
                  }
                  else{
                    this.covidSubmittedData = submittedResp['data']['batches'];
                    callback({
                      recordsTotal    :  submittedResp['data']['totalBatches'],
                      recordsFiltered :  submittedResp['data']['totalBatches'],
                      data: []
                    });
                    this.covidCaseRequestNew.data = {};
                  }

                  // return;




                },error=>{
                  this.ngxLoader.stop();
                  this.notifier.notify("error","Error while loading Active Batches")
                  this.covidCaseRequestNew.data = {};
                })

            }
          })
        })
      }
    };



    this.dtOptions[2] = {
      pagingType   : 'full_numbers',
      pageLength   : this.paginateLength3,
      serverSide   : true,
      processing   : true,
      // ordering     : false,
      "order": [[ 1, "desc" ]],
      columnDefs: [
        { orderable: true, className: 'reorder', targets: [0,1,2,3,4, 'desc'] },
        { orderable: false, targets: ['_all'] }
      ],
      "lengthMenu": [20, 50, 75, 100 ],
      ajax: (dataTablesParameters: any, callback) => {
        this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {

          dtElement.dtInstance.then((dtInstance: any) => {
            if(dtInstance.table().node().id == 'covid19-cancelled-batch'){
              var allCancelledData =[];
              this.paginatePage3          = dtElement['dt'].page.info().page;
              this.paginateLength3         = dtElement['dt'].page.len();
              var sortColumn = dataTablesParameters.order[0]['column'];
              var sortOrder  = dataTablesParameters.order[0]['dir'];
              var sortArray  = ['batchId','batchCreationDate','batchCreatedBy','batchUpdationDate','batchUpdatedBy','numberOfCases','batchId']
              var manageRequest = {...this.covidCaseRequestNew};
              if (dataTablesParameters.search.value != "" && (this.oldCharacterCount3 == 0 || this.oldCharacterCount3 == 3)) {
                if (this.oldCharacterCount3 == 3) {
                }
                manageRequest['data']['mainSearch'] = dataTablesParameters.search.value;

              }
              else{

                  $('.dataTables_processing').css('display',"none");
                  if (this.oldCharacterCount3 == 3) {
                    this.oldCharacterCount3     = 3

                  }
                  else{
                    this.oldCharacterCount3     = 2;
                  }

              }
                this.oldCharacterCount3     = 3;

                manageRequest['data']['batchStatus']= '4';
                manageRequest['data']['sortColumn']    = sortArray[sortColumn];
                manageRequest['data']['sortType']  = sortOrder;
                manageRequest['data']['pageNumber']  = dtElement['dt'].page.info().page;
                manageRequest['data']['pageSize']  = dtElement['dt'].page.len();
                manageRequest.header.partnerCode  = this.logedInUserRoles.partnerCode;
                manageRequest.header.userCode     = this.logedInUserRoles.userCode;
                manageRequest.header.functionalityCode     = "TWMA-MBC";
                this.http.put(batchUrl,manageRequest).subscribe(cancelledResp =>{
                  console.log("cancelledResp",cancelledResp);
                  if (typeof cancelledResp['message'] != 'undefined') {
                    this.notifier.notify("error",cancelledResp['message']);
                    callback({
                      recordsTotal    :  0,
                      recordsFiltered :  0,
                      data: []
                    });
                    this.covidCaseRequestNew.data = {};

                    return;
                  }
                  if (cancelledResp['data'] == null) {
                    this.notifier.notify("error",cancelledResp['result']['description']);
                    callback({
                      recordsTotal    :  0,
                      recordsFiltered :  0,
                      data: []
                    });
                    this.covidCaseRequestNew.data = {};

                    return
                  }
                  if (cancelledResp['data']['batches'] == null) {
                    callback({
                      recordsTotal    :  0,
                      recordsFiltered :  0,
                      data: []
                    });
                    this.covidCaseRequestNew.data = {};
                    return
                  }
                  else{
                    this.covidCancelledData = cancelledResp['data']['batches'];
                    callback({
                      recordsTotal    :  cancelledResp['data']['totalBatches'],
                      recordsFiltered :  cancelledResp['data']['totalBatches'],
                      data: []
                    });
                    this.covidCaseRequestNew.data = {};
                  }

                  // return;




                },error=>{
                  this.ngxLoader.stop();
                  this.notifier.notify("error","Error while loading Active Batches")
                  this.covidCaseRequestNew.data = {};
                })
            }
          })
        })
      }
    };



  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  showCancelModal(item){
    this.testBatchToCancel = item;
    $('#cancelModal').modal('show');


  }
  cancelBatch(){
    var myDate = new Date();
    var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');

    // console.log('this.testBatchToCancel',this.testBatchToCancel);
    var selectURL = environment.API_COVID_ENDPOINT + 'viewbatch';
    var getBatchReq = {...this.viewBatchRequest}
    getBatchReq.header.partnerCode  = this.logedInUserRoles.partnerCode;
    getBatchReq.header.userCode     = this.logedInUserRoles.userCode;
    getBatchReq.header.functionalityCode     = "TWMA-MBC";
    getBatchReq.data.batchId = this.testBatchToCancel['batchId'];
    this.covidService.getBatchById(selectURL, getBatchReq).subscribe(getByIdResp=>{
      console.log("getByIdResp",getByIdResp);
      if (getByIdResp['result']['codeType'] == 'T') {
        if (getByIdResp['result']['description'] == "NO RECORD FOUND") {
          var cancelUrl = environment.API_COVID_ENDPOINT + 'cancelbatch';
          var canceBatchReq = {...this.cancelBatchRequest};
          canceBatchReq.data.batchId = this.testBatchToCancel['batchId'];
          canceBatchReq.data.batchStatusId = '4';
          canceBatchReq.data.testStatusId = '1';
          canceBatchReq.data.updatedBy = this.logedInUserRoles.userCode;
          canceBatchReq.data.updatedTimestamp = currentDate;
          canceBatchReq.header.partnerCode  = this.logedInUserRoles.partnerCode;
          canceBatchReq.header.userCode     = this.logedInUserRoles.userCode;
          canceBatchReq.header.functionalityCode     = "TWMA-MBC";
          this.covidService.cancelBatch(cancelUrl,canceBatchReq).subscribe(cancelResp=>{
            console.log("cancelResp",cancelResp);
            $('#cancelModal').modal('hide');

            if (cancelResp['result']['codeType'] == "S") {
              this.ngxLoader.stop();
              this.notifier.notify("success","Batch cancelled successfully");
              this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
                dtElement.dtInstance.then((dtInstance: any) => {
                  dtInstance.destroy();
                })
              })
              this.dtTrigger.next();
              // this.dtTrigger.next();

              $('#cancelModal').modal('show');
              this.testBatchToCancel = {};
            }
          },error=>{
            this.ngxLoader.stop();
            this.notifier.notify("error","Error while canceling Batch");
          })
        }
        else{
          this.notifier.notify("error","No Record Found");
          this.ngxLoader.stop();
        }

      }
      else{
        var uniqeCaseIds = [];
        for (let i = 0; i < getByIdResp['data']['jobOrderDetails'].length; i++) {
          uniqeCaseIds.push(getByIdResp['data']['jobOrderDetails'][i].caseId)
        }
        console.log('uniqeCaseIds',uniqeCaseIds);
        var specimenurl = environment.API_SPECIMEN_ENDPOINT + 'updatecasestatusin';

        var specimenreqBody={header:{uuid:"",partnerCode:this.logedInUserRoles.partnerCode,userCode:this.logedInUserRoles.userCode,referenceNumber:"",systemCode:"",moduleCode:"SPCM",functionalityCode: "TWMA-MBC",systemHostAddress:"",remoteUserAddress:"",dateTime:""},
        data: {
          caseStatusId:3,
          caseIds:uniqeCaseIds,
          updatedBy: this.logedInUserRoles.userCode,
          updatedTimestamp:currentDate

        }
      }
      // console.log("specimenreqBody",specimenreqBody);
      // return;
      this.covidService.updateCaseStatus(specimenurl,specimenreqBody).subscribe(specimenReps=>{
        if (specimenReps['data']>0) {
          var cancelUrl = environment.API_COVID_ENDPOINT + 'cancelbatch';
          var canceBatchReq = {...this.cancelBatchRequest};
          canceBatchReq.data.batchId = this.testBatchToCancel['batchId'];
          canceBatchReq.data.batchStatusId = '4';
          canceBatchReq.data.testStatusId = '1';
          canceBatchReq.data.updatedBy = this.logedInUserRoles.userCode;
          canceBatchReq.data.updatedTimestamp = currentDate;
          canceBatchReq.header.partnerCode  = this.logedInUserRoles.partnerCode;
          canceBatchReq.header.userCode     = this.logedInUserRoles.userCode;
          canceBatchReq.header.functionalityCode     = "TWMA-MBC";
          this.covidService.cancelBatch(cancelUrl,canceBatchReq).subscribe(cancelResp=>{
            console.log("cancelResp",cancelResp);
            $('#cancelModal').modal('hide');

            if (cancelResp['data'] == true) {
              this.ngxLoader.stop();
              this.notifier.notify("success","Batch cancelled successfully");
              this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
                dtElement.dtInstance.then((dtInstance: any) => {
                  dtInstance.destroy();
                })
              })
              this.dtTrigger.next();
              // this.dtTrigger.next();

              $('#cancelModal').modal('show');
              this.testBatchToCancel = {};
            }
          },error=>{
            this.ngxLoader.stop();
            this.notifier.notify("error","Error while canceling Batch");
          })
          // this.ngxLoader.stop();
          // this.notifier.notify("success","Batch removed successfully");

          // this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          //   // Destroy the table first
          //   dtInstance.destroy();
          //   // Call the dtTrigger to rerender again
          //   this.dtTrigger.next();
          // });
        }
        else{
          this.notifier.notify("error","Error while updating case status");
          this.ngxLoader.stop();
        }
      },error=>{
        this.notifier.notify("error","Error while updating case status");
        this.ngxLoader.stop();
      })


      // console.log("getByIdResp",getByIdResp);




    }
  },error=>{
    this.ngxLoader.stop();
    this.notifier.notify("error","Error while getting Batch");
  })


}
PopulateSearchData(data,type){
  return new Promise((resolve, reject) => {
    var allActiveData =[];
    allActiveData = [...data['data']['viewBatchDto']]
    const uniqueUserIds = [];
    const map = new Map();
    for (const item of data['data']['viewBatchDto']) {
      if(!map.has(item.createdBy)){
        map.set(item.createdBy, true);    // set any value to Map
        // uniqueUserIds.push(item.createdBy);
        if (item.createdBy != null) {
          uniqueUserIds.push(item.createdBy);

        }
      }
      if(!map.has(item.updatedBy)){
        map.set(item.updatedBy, true);    // set any value to Map
        // uniqueUserIds.push(item.updatedBy);
        if (item.updatedBy != null) {
          uniqueUserIds.push(item.updatedBy);

        }
      }
      if(!map.has(item.submittedBy)){
        map.set(item.submittedBy, true);    // set any value to Map
        // uniqueUserIds.push(item.submittedBy);
        if (item.submittedBy != null) {
          uniqueUserIds.push(item.submittedBy);

        }
      }
    }
    // console.log("uniqueUserIds",uniqueUserIds);

    var userRequest = {...this.usersRequest};
    userRequest.data.userCodes = uniqueUserIds;
    var userUrl = environment.API_USER_ENDPOINT + "getusersbyusercodes";
    this.covidService.getUserRecord(userUrl,userRequest).then(getUserResp =>{
      // return getUserResp;
      // console.log("getUserResp",getUserResp);
      for (let i = 0; i < allActiveData.length; i++) {
        for (let j = 0; j < getUserResp['data'].length; j++) {
          if(allActiveData[i].createdBy == getUserResp['data'][j].userCode){
            allActiveData[i].createdByName = getUserResp['data'][j].fullName;
          }
          else if(allActiveData[i].createdBy != getUserResp['data'][j].userCode){
            if (typeof allActiveData[i].createdByName == 'undefined') {
              // allActiveData[i].createdByName = "No Name";
            }
          }
          if(allActiveData[i].updatedBy == getUserResp['data'][j].userCode){
            allActiveData[i].updatedByName = getUserResp['data'][j].fullName;
          }
          else if(allActiveData[i].updatedBy != getUserResp['data'][j].userCode){
            if (typeof allActiveData[i].updatedByName == 'undefined') {
              // allActiveData[i].updatedByName = "No Name";
            }
          }
          if(allActiveData[i].submittedBy == getUserResp['data'][j].userCode){
            allActiveData[i].submittedByName = getUserResp['data'][j].fullName;
          }
          else if(allActiveData[i].submittedBy != getUserResp['data'][j].userCode){
            if (typeof allActiveData[i].submittedByName == 'undefined') {
              // allActiveData[i].submittedByName = "No Name";
            }
          }
          if(allActiveData[i].updatedBy == getUserResp['data'][j].userCode){
            allActiveData[i].cancelledByName = getUserResp['data'][j].fullName;
          }
          else if(allActiveData[i].updatedBy != getUserResp['data'][j].userCode){
            if (typeof allActiveData[i].cancelledByName == 'undefined') {
              // allActiveData[i].submittedByName = "No Name";
            }
          }

        }

      }
    },error=>{
      this.ngxLoader.stop();
      this.notifier.notify("error","error While loading Created By")
    })

    //////// updated By
    // console.log('data',data);


    if (type == 'active') {
      console.log("----",allActiveData);
      console.log("---- count",data['data']['batchCount']);

      this.covidAllActiveData = allActiveData;
      this.activeCasesCount = data['data']['batchCount'];
    }
    else if(type == 'submitted'){
      this.covidSubmittedData = allActiveData;
      this.submitCasesCount = data['data']['batchCount'];
    }
    else if(type == 'cancelled'){
      this.covidCancelledData = allActiveData;
      this.cancelledCasesCount = data['data']['batchCount'];
    }


    resolve()
  })
}

openSubmitModal(batchId){
  this.SelectedForSubmitId = batchId;
  $('#submitModal').modal('show');
}

submitToReporting(){
  this.ngxLoader.start();
  var myDate = new Date();
  var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');
  var requestUrl = environment.API_COVID_ENDPOINT + 'submitbatch';
  var requestBody = {...this.submitToReportingRequest}
  requestBody.data.batchId = this.SelectedForSubmitId;
  requestBody.data.submittedDate = currentDate;
  requestBody.header.partnerCode = this.logedInUserRoles.partnerCode;
  requestBody.header.userCode = this.logedInUserRoles.userCode;
  requestBody.header.functionalityCode     = "TWMA-MBC";
  console.log('requestBody',requestBody);
  this.covidService.submitBatch(requestUrl,requestBody).subscribe(submitResponse=>{
    console.log('submitResponse',submitResponse);
    $('#submitModal').modal('hide');
    if (submitResponse['result']['codeType'] == "T") {
      this.ngxLoader.stop();
      this.notifier.notify("warning",submitResponse['result']['description'])
    }
    else{
      this.SelectedForSubmitId = '';
      this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
        dtElement.dtInstance.then((dtInstance: any) => {
          dtInstance.destroy();
        })
      })
      this.dtTrigger.next();
      this.ngxLoader.stop();
      this.notifier.notify("success","Batch submitted successfully")

    }


  },error=>{
    this.ngxLoader.stop();
    this.notifier.notify("error","error while submitting batch")
    $('#submitModal').modal('hide');
    this.SelectedForSubmitId = '';
  })


}

triggerUpload(batchId){
  $('#resultUpload-'+batchId).trigger('click');
}

uploadResults(input,batchID){
  // console.log('input',input.files[0]);
  console.log('batchID',batchID);
  // return;

  this.fileName = input.files[0].name;
  var mb = input.files[0].size/1048576;
  console.log("mb",mb);
  if (this.fileName.length < 120) {
  if (input.files[0].type != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' && input.files[0].type != 'application/vnd.ms-excel') {
    $('#resultUpload-'+batchID).val('');
    alert('invalid file type');
    return;
  }
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = (e: any) => {
      // console.log('Got here: ', e.target.result);
      // console.log('input.files[0]: ', input.files[0]);

      this.fileBase64 = e.target.result;
      this.uploadFIleRequest(batchID,input.files[0].name,input.files[0].type,e.target.result)
      // this.obj.photoUrl = e.target.result;
    }
    reader.readAsDataURL(input.files[0]);
  }
  // console.log('this.imageBase64: ', this.imageBase64);
  if(input.files && input.files.length > 0) {
    this.filePayLoad = input.files[0];
  }
  }
  else{
    $('#resultUpload-'+batchID).val('');
    alert("File name should contain less then 120 character")
  }
}
uploadFIleRequest(batchID,name,type,base64){
  this.ngxLoader.start()
  // console.log('batchID',batchID);
  // console.log('name',name);
  // console.log('type',type);
  // console.log('base64',base64);
  var myDate = new Date();
  var currentDate = this.datePipe.transform(myDate, 'MM-dd-yyyy HH:mm:ss');

  var a = name.split('.');
  var s = a.length-1;
  var n = a[s];
  // console.log("n",n);
  var uploadURL = environment.API_COVID_ENDPOINT + "importinterpretations";
  var uploadRequest = {...this.uploadResultsRequest}
  uploadRequest.data.batchId = batchID;
  uploadRequest.data.fileName = name;
  uploadRequest.data.fileExtension = '.'+n;
  uploadRequest.data.base64EncodedFile = base64;
  uploadRequest.data.updatedDate = currentDate;
  uploadRequest.header.partnerCode = this.logedInUserRoles.partnerCode;
  uploadRequest.header.userCode    = this.logedInUserRoles.userCode;
  uploadRequest.header.functionalityCode     = "TWMA-MBC";

  console.log("uploadRequest",uploadRequest);

  this.covidService.uploadResults(uploadURL,uploadRequest).subscribe(uploadResponse=>{
    console.log('uploadResponse',uploadResponse);
    if (uploadResponse['result'].codeType == "S") {
      if (typeof uploadResponse['data'].failedAtRows != undefined) {
        if (uploadResponse['data'].failedAtRows != '') {
          this.notifier.notify("error",uploadResponse['data'].failedAtRows)
        }
        else{
          this.notifier.notify("success","Results uploaded successfully")
        }
      }
      // window.open(uploadResponse['data']['base64EncodedFile'], "_blank");
      const downloadLink = document.createElement("a");
      var fileName= fileName;

      downloadLink.href = uploadResponse['data']['base64EncodedFile'];
      downloadLink.download = uploadRequest.data.fileName;
      downloadLink.click();
      this.dtElement.forEach((dtElement: DataTableDirective, index: number) => {
        dtElement.dtInstance.then((dtInstance: any) => {
          dtInstance.destroy();
        })
      })
      this.dtTrigger.next();
      this.ngxLoader.stop();
      // this.notifier.notify("success","Results uploaded successfully")
      $('#resultUpload-'+batchID).val('');
    }
    else{
      $('#resultUpload-'+batchID).val('');
      this.ngxLoader.stop();
      // this.notifier.notify("error","error while uploading results")
      this.notifier.notify("error",uploadResponse['result'].description)

    }


  },error=>{
    $('#resultUpload-'+batchID).val('');
    this.ngxLoader.stop();
    this.notifier.notify("error","error while uploading results")

  })




}
downloadResults(batchId){
  this.ngxLoader.start();
  var downloadUrl = environment.API_COVID_ENDPOINT +'batchresultfiles';
  var downlaodReq = {...this.downloadResultsRequest}
  downlaodReq.data.batchId = batchId;
  downlaodReq.header.partnerCode = this.logedInUserRoles.partnerCode;
  downlaodReq.header.userCode = this.logedInUserRoles.userCode;
  downlaodReq.header.functionalityCode = "TWMA-MBC";
  this.covidService.downloadResults(downloadUrl,downlaodReq).subscribe(downloadResp=>{
    console.log('downloadResp',downloadResp);
    if (downloadResp['result'].codeType == "S") {
      this.ngxLoader.stop();
      // window.open(downloadResp['data']['base64EncodedOriginalFile'], "_blank");
      var a = downloadResp['data']['base64EncodedOriginalFile'].split(';')
      var filename = ''
      console.log("a",a);

      if (a[0].indexOf('spreadsheetml.sheet') !== -1) {
        filename  = 'result.xlsx';
      }
      else if (a[0].indexOf('zip') !== -1) {
        filename  = 'result.zip';
      }
      else{
        filename  = 'result.csv';
      }
      const downloadLink = document.createElement("a");

      downloadLink.href = downloadResp['data']['base64EncodedOriginalFile'];
      downloadLink.download = filename;
      downloadLink.click();
    }
    else{
      this.ngxLoader.stop();
      this.notifier.notify( "error", downloadResp['result'].description);
    }


  },error=>{
    this.ngxLoader.stop();
    this.notifier.notify("error","error while downloading results")

  })
}

exportToExcel(batchId, batchStatus){
  this.ngxLoader.start();
  var downloadUrl = environment.API_COVID_ENDPOINT +'exportbatch';
  var downlaodReq = {...this.exportExcelRequest}
  downlaodReq.data.batchId = batchId;
  downlaodReq.data.batchActive = batchStatus;
  downlaodReq.header.partnerCode = 'sip';
  // downlaodReq.header.partnerCode = this.logedInUserRoles.partnerCode;
  downlaodReq.header.userCode = this.logedInUserRoles.userCode;
  downlaodReq.header.functionalityCode = "TWMA-MBC";

  this.covidService.exportToExcel(downloadUrl,downlaodReq).subscribe(exportResp=>{
    console.log('exportResp',exportResp);
    if (typeof exportResp['result'] == "undefined") {
      this.ngxLoader.stop();
      this.notifier.notify('error','internal server error')
      return;
    }
    if (exportResp['result'].codeType == "S") {
      this.ngxLoader.stop();
      const downloadLink = document.createElement("a");

      downloadLink.href = exportResp['data']['base64EncodedBatch'];
      downloadLink.download = exportResp['data']['fileName'];
      downloadLink.click();

      // window.open(exportResp['data']['base64EncodedBatch'], "_blank");
    }
    else{
      this.ngxLoader.stop();
      this.notifier.notify( "error", exportResp['result'].description);
    }


  },error=>{
    this.ngxLoader.stop();
    this.notifier.notify("error","error while downloading file")

  })
}

printPDF(batchId, batchStatus){
  this.ngxLoader.start();
  var downloadUrl = environment.API_COVID_ENDPOINT +'printworksheet';
  var downlaodReq = {...this.printPDFRequest}
  downlaodReq.data.batchId = batchId;
  downlaodReq.data.batchActive = batchStatus;
  downlaodReq.header.partnerCode = 'sip';
  // downlaodReq.header.partnerCode = this.logedInUserRoles.partnerCode;
  downlaodReq.header.userCode = this.logedInUserRoles.userCode;
  downlaodReq.header.functionalityCode = "TWMA-MBC";

  this.covidService.printPDF(downloadUrl,downlaodReq).subscribe(exportResp=>{
    console.log('printResp',exportResp);
    if (exportResp['result'].codeType == "S") {
      this.ngxLoader.stop();
      // window.open(exportResp['data']['base64EncodedOriginalFile'], "_blank");
      let pdfWindow = window.open("")
      pdfWindow.document.write(
        "<iframe width='100%' height='100%' src='"+encodeURI(exportResp['data']['base64EncodedOriginalFile'])+"'></iframe>"
      )
    }
    else{
      this.ngxLoader.stop();
      this.notifier.notify( "error", exportResp['result'].description);
    }


  },error=>{
    this.ngxLoader.stop();
    this.notifier.notify("error","error while downloading file")

  })
}



meagSearch(searchBatchesUrl,searchBatchRequest,userUrl,userRequest,pageNumber,pageSize,column,order,type){
  return new Promise((resolve, reject) => {
    var userResp = this.http.put(userUrl,userRequest).toPromise().then(userResp=>{
      return userResp;
    });
    var batchesResp = this.http.post(searchBatchesUrl,searchBatchRequest).toPromise().then(userResp=>{
      return userResp;
    })
    forkJoin([userResp, batchesResp]).subscribe(bothresults => {
      console.log("bothresults",bothresults);
      var user    = bothresults[0];
      var batches = bothresults[1];
      if (user['data']['users']['length'] == 0 && batches['data']['viewBatchDto']['length'] == 0) {
        if (type == 'active') {
          this.covidAllActiveData = [];
          this.activeCasesCount = 0;
        }
        else if(type == 'submitted'){
          this.covidSubmittedData = [];
          this.submitCasesCount = 0;
        }
        else if(type == 'cancelled'){
          this.covidCancelledData = [];
          this.cancelledCasesCount = 0;
        }
        resolve();

      }
      // console.log("batches",batches);


      if (user['data']['users']['length'] > 0 && batches['data']['viewBatchDto']['length'] == 0) {
        //////////////////// if zero response from COvid end point
        var uniqueUserIds = []
        for (let i = 0; i < user['data']['users']['length']; i++) {
          uniqueUserIds.push(user['data']['users'][i]['userCode']);
        }
        // console.log('uniqueUserIds',uniqueUserIds);
        var searchByUserUrl = environment.API_COVID_ENDPOINT+ 'showbatchesbyusers';
        var searchByUserReq = {...this.showbatchesbyusersRequest};
        if (type == 'active') {
          searchByUserReq.data.batchStatusId = "1";
        }
        else if (type == 'submitted') {
          searchByUserReq.data.batchStatusId = "2";
        }
        else if (type == 'cancelled') {
          searchByUserReq.data.batchStatusId = "4";
        }

        searchByUserReq.data.page       = pageNumber;
        searchByUserReq.data.size       = pageSize;
        searchByUserReq.data.sortColumn = column;
        searchByUserReq.data.sortingOrder = order;
        searchByUserReq.data.userCodes = uniqueUserIds;
        this.covidService.searchByUserIds(searchByUserUrl,searchByUserReq).subscribe(searchByUserResp=>{
          console.log('searchByUserResp',searchByUserResp);
          if (searchByUserResp['data'] == null) {
            if (type == 'active') {
              this.covidAllActiveData = [];
              this.activeCasesCount = 0;
            }
            else if(type == 'submitted'){
              this.covidSubmittedData = [];
              this.submitCasesCount = 0;
            }
            else if(type == 'cancelled'){
              this.covidCancelledData = [];
              this.cancelledCasesCount = 0;
            }
            resolve();

          }
          else{
            this.PopulateSearchData(searchByUserResp,type).then(respOfPopulation=>{
              resolve();
              // callback({
              //     recordsTotal    :  this.activeCasesCount,
              //     recordsFiltered : this.activeCasesCount,
              //     data: []
              //   });
            })
          }


        })
      }
      else if(user['data']['users']['length'] > 0 && batches['data']['viewBatchDto']['length'] > 0){
        ////// when both return record
        var returnedFromCovid = batches;
        var uniqueUserIds = []
        for (let i = 0; i < user['data']['users']['length']; i++) {
          uniqueUserIds.push(user['data']['users'][i]['userCode']);
        }
        // console.log('uniqueUserIds',uniqueUserIds);
        var searchByUserUrl = environment.API_COVID_ENDPOINT+ 'showbatchesbyusers';
        var searchByUserReq = {...this.showbatchesbyusersRequest};
        // searchByUserReq.data.batchStatusId = "1";
        if (type == 'active') {
          searchByUserReq.data.batchStatusId = "1";
        }
        else if (type == 'submitted') {
          searchByUserReq.data.batchStatusId = "2";
        }
        else if (type == 'cancelled') {
          searchByUserReq.data.batchStatusId = "4";
        }
        searchByUserReq.data.page       = pageNumber;
        searchByUserReq.data.size       = pageSize;
        searchByUserReq.data.sortColumn = column;
        searchByUserReq.data.sortingOrder = order;
        searchByUserReq.data.userCodes = uniqueUserIds;
        this.covidService.searchByUserIds(searchByUserUrl,searchByUserReq).subscribe(searchByUserResp=>{
          // console.log('searchByUserResp',searchByUserResp);
          // console.log('uniqueUserIds',uniqueUserIds);
          var searchByUserUrl = environment.API_COVID_ENDPOINT+ 'showbatchesbyusers';
          var searchByUserReq = {...this.showbatchesbyusersRequest};
          // searchByUserReq.data.batchStatusId = "1";
          if (type == 'active') {
            searchByUserReq.data.batchStatusId = "1";
          }
          else if (type == 'submitted') {
            searchByUserReq.data.batchStatusId = "2";
          }
          else if (type == 'cancelled') {
            searchByUserReq.data.batchStatusId = "4";
          }
          searchByUserReq.data.page       = pageNumber;
          searchByUserReq.data.size       = pageSize;
          searchByUserReq.data.sortColumn = column;
          searchByUserReq.data.sortingOrder = order;
          searchByUserReq.data.userCodes = uniqueUserIds;
          this.covidService.searchByUserIds(searchByUserUrl,searchByUserReq).subscribe(searchByUserResp=>{
            // console.log('searchByUserResp',searchByUserResp);
            if (searchByUserResp['data'] == null) {
              /////// then only result from btaches response will be used
              this.PopulateSearchData(returnedFromCovid,type).then(respOfPopulation=>{
                resolve();
                // callback({
                //     recordsTotal    :  this.activeCasesCount,
                //     recordsFiltered : this.activeCasesCount,
                //     data: []
                //   });
              })

            }
            else{
              var uniquefromUser     = [];
              var uniquefromUserTemp ={
                header:{},
                data:{
                  viewBatchDto:[]
                }
              };
              for (let i = 0; i < returnedFromCovid['data']['viewBatchDto']['length']; i++) {
                for (let j = 0; j < searchByUserResp['data']['viewBatchDto'].length; j++) {
                  if (returnedFromCovid['data']['viewBatchDto'][i].batchId == searchByUserResp['data']['viewBatchDto'][j].batchId) {

                  }
                  else{
                    uniquefromUserTemp['data']['viewBatchDto'][i] = returnedFromCovid['data']['viewBatchDto'][i];
                  }

                }

              }
              // console.log('uniquefromUserTemp',uniquefromUserTemp);
              // console.log("searchByUserResp",searchByUserResp);

              searchByUserResp['data']['viewBatchDto'] = [...searchByUserResp['data']['viewBatchDto'], ...uniquefromUserTemp['data']['viewBatchDto']];
              searchByUserResp['data']['batchCount'] = searchByUserResp['data']['viewBatchDto']['length'];
              // console.log("after", searchByUserResp);
              // var formatTemplat ;
              // formatTemplat['data']['viewBatchDto'] = combined;
              // console.log("combined", combined);
              // console.log("formatTemplat", formatTemplat);
              // searchByUserResp['data']['viewBatchDto'].push(uniquefromUserTemp);
              // console.log("combined", searchByUserResp);
              this.PopulateSearchData(searchByUserResp,type).then(respOfPopulation=>{
                resolve();

              })
            }

            //////////////////////sarmad//////////////////////////////////
          })
        })
      }
      else if(user['data']['users']['length'] == 0 && batches['data']['viewBatchDto']['length'] > 0){
        ////// when both return record
        var returnedFromCovid = batches;
        this.PopulateSearchData(returnedFromCovid,type).then(respOfPopulation=>{
          resolve();

        })

      }



    })
    // resolve()
  })
}

goToEditBatch(batchId){
  var a = batchId;

  // var ciphertext = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
  var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
  var bx = ax.replace('+','xsarm');
  var cx = bx.replace('/','ydan');
  var ciphertext = cx.replace('=','zhar');
  console.log('ciphertext',ciphertext);
  this.router.navigate(['edit-batch', ciphertext]);
}
goToWorkshhet(batchId){
  var a = batchId;

  // var ciphertext = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
  var ax = CryptoJS.AES.encrypt(a, 'nglis', { outputLength: 224 }).toString();
  var bx = ax.replace('+','xsarm');
  var cx = bx.replace('/','ydan');
  var ciphertext = cx.replace('=','zhar');
  console.log('ciphertext',ciphertext);
  var url = location.origin +'/worksheet/'+ciphertext;
  window.open( url , '_blank');
}

}
