import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import { BehaviorSubject, Observable, throwError, Subject } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, switchMap, tap, delay, map } from "rxjs/operators";
// import { environment } from '../../../environments/environment';
import { environment } from '../../../../../../src/environments/environment';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class CovidApiCallsService {
  public logedInUserRoles :any = {};

  public searchClientName = {
    header:{
      uuid               :"",
      partnerCode        :"",
      userCode           :"",
      referenceNumber    :"",
      systemCode         :"",
      moduleCode         :"CLIM",
      functionalityCode  :"TWMA-CB",
      systemHostAddress  :"",
      remoteUserAddress  :"",
      dateTime           :""
    },
    data:{
    sortColumn  :"name",
    sortingOrder:"asc",
    searchString:"",
    userCode    : ""
  }
}

  constructor(
    private http: HttpClient,

    ) { }
    search(terms) {
      return terms.pipe(debounceTime(400))
        .pipe(distinctUntilChanged())
        .pipe(switchMap(term => this.searchEntries(term)));
    }

    searchEntries(term) {
      let decData = CryptoJS.enc.Base64.parse(localStorage.getItem('userRolesInfo')).toString(CryptoJS.enc.Utf8)
      var bts  = CryptoJS.AES.decrypt(decData, 'nglisSarmad');
      var originalText = bts.toString(CryptoJS.enc.Utf8);
      this.logedInUserRoles = JSON.parse(originalText)

      // if (term.length >=3 || term.length == 0) {
      this.searchClientName.data.searchString = term;
      if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-UC") != -1) {
        this.searchClientName['header'].functionalityCode    = "CLTA-UC";
      }
      else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-VC") != -1){
        this.searchClientName['header'].functionalityCode    = "CLTA-VC";
      }
      else if (this.logedInUserRoles['allowedRoles'].indexOf("CLTA-CD") != -1){
        this.searchClientName['header'].functionalityCode    = "CLTA-CD";
      }
      this.searchClientName['header'].userCode = this.logedInUserRoles.userCode;
      this.searchClientName['header'].partnerCode = this.logedInUserRoles.partnerCode;
      // if (term.length >=3 || term.length == 0) {
        this.searchClientName.data.searchString = term;
        const searchURL    = environment.API_CLIENT_ENDPOINT + 'searchclientidnamebyname';
        return this.http
        // .get(this.baseUrl + this.queryUrl + term)
            .post(searchURL,this.searchClientName)
            .pipe(map(res => res)).pipe(
              catchError(this.errorHandler)
            )
      // }


    }
    createBatch(url, saveData){
      return this.http.post<any>(url, saveData)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    manageBatch(url, saveData){
      return this.http.post<any>(url, saveData)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    getBatchById(url, saveData){
      return this.http.post<any>(url, saveData)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    updatePatient(url, saveData){
      return this.http.put<any>(url, saveData)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    removeCase(url, saveData){
      return this.http.post<any>(url, saveData)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    getCasesByIds(url, saveData){
      return this.http.post<any>(url, saveData)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    updateCaseStatus(url, saveData){
      return this.http.post<any>(url, saveData)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    updateSpecimenIfo(url, saveData){
      return this.http.post<any>(url, saveData)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    searchBatches(url, saveData){
      return this.http.post<any>(url, saveData)
      .pipe(
        catchError(this.errorHandler)
      )
    }

    cancelBatch(url, saveData){
      return this.http.post<any>(url, saveData)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    repeatTest(url, saveData){
      return this.http.post<any>(url, saveData)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    updateClientName(url, saveData){
      return this.http.post<any>(url, saveData)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    submitBatch(url, saveData){
      return this.http.put<any>(url, saveData)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    uploadResults(url, saveData){
      return this.http.put<any>(url, saveData)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    downloadResults(url, saveData){
      return this.http.put<any>(url, saveData)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    exportToExcel(url, saveData){
      return this.http.post<any>(url, saveData)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    printPDF(url, saveData){
      return this.http.put<any>(url, saveData)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    searchByUserIds(url, saveData){
      return this.http.post<any>(url, saveData)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    getPatientRecord(url, saveData){
      return this.http.put<any>(url, saveData)
      .pipe()
    .toPromise()
    .then( resp => {
      return resp;
    })
    .catch(error => {
      catchError(this.errorHandler)
    });
    }

    getUserRecord(url, saveData){
      return this.http.put<any>(url, saveData)
      .pipe()
    .toPromise()
    .then( resp => {
      return resp;
    })
    .catch(error => {
      catchError(this.errorHandler)
    });
    }
    getClientRecord(url, saveData){
      return this.http.post<any>(url, saveData)
      .pipe()
    .toPromise()
    .then( resp => {
      return resp;
    })
    .catch(error => {
      catchError(this.errorHandler)
    });
    }

    checkTest(url, saveData){
      return this.http.post<any>(url, saveData)
      .pipe(
        catchError(this.errorHandler)
      )
    }
    loadCasesFromIntake(url, saveData){
      return this.http.post<any>(url, saveData)
      .pipe(
        catchError(this.errorHandler)
      )
    }







    errorHandler(error) {
      let errorMessage = '';
      if(error.error instanceof ErrorEvent) {
        // Get client-side error
        errorMessage = error.error.message;
      } else {
        // Get server-side error
        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      }
      console.log(errorMessage);
      return throwError(errorMessage);
    }
}
