import { TestBed } from '@angular/core/testing';

import { CovidApiCallsService } from './covid-api-calls.service';

describe('CovidApiCallsService', () => {
  let service: CovidApiCallsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CovidApiCallsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
