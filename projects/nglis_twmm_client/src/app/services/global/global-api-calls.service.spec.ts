import { TestBed } from '@angular/core/testing';

import { GlobalApiCallsService } from './global-api-calls.service';

describe('GlobalApiCallsService', () => {
  let service: GlobalApiCallsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GlobalApiCallsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
